﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb.Autenticación
{
    public partial class w_ConfirmarEmail : System.Web.UI.Page
    {
        cls_seg_Usuario_Registrar objUsuarioRegistrar = new cls_seg_Usuario_Registrar();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["userid"] != null)
            {
                string[] s_querystring = Request.QueryString["userid"].Split('-');
                string s_codus1 = s_querystring[0];
                string s_rucemp = s_querystring[1];

                clsError objError = objUsuarioRegistrar.f_Usuario_Email(s_rucemp, s_codus1);
                if (!String.IsNullOrEmpty(objError.Mensaje))
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            else
            {
                Response.Redirect("~/w_Login.aspx");
            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
    }
}