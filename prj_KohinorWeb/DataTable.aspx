﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataTable.aspx.cs" Inherits="prj_KohinorWeb.DataTable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    
    <link href="CoreUI/css/style.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css"/>
  
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
    
    <script type="text/javascript">
      $(document).ready(function () {
          $('#<%= grvReporte.ClientID %>').DataTable();
      });
  </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="card-body">
                                    <div class="table table-responsive" style="overflow-x: auto;">
                                        <asp:GridView ID="grvReporte" class="table table-bordered" runat="server" Width="100%" AutoGenerateColumns="False" DataSourceID="sqldsCliente" DataKeyNames="codemp,codus1,codmod,niv001,niv002,niv003">
                                            <Columns>
                                                <asp:BoundField DataField="codemp" HeaderText="codemp" SortExpression="codemp" ReadOnly="True" />
                                                <asp:BoundField DataField="codus1" HeaderText="codus1" SortExpression="codus1" ReadOnly="True" />
                                                <asp:BoundField DataField="codmod" HeaderText="codmod" SortExpression="codmod" ReadOnly="True" />
                                                <asp:BoundField DataField="niv001" HeaderText="niv001" SortExpression="niv001" ReadOnly="True" />
                                                <asp:BoundField DataField="niv002" HeaderText="niv002" SortExpression="niv002" ReadOnly="True" />
                                                <asp:BoundField DataField="niv003" HeaderText="niv003" SortExpression="niv003" ReadOnly="True" />
                                                <asp:BoundField DataField="usuing" HeaderText="usuing" SortExpression="usuing" />
                                                <asp:BoundField DataField="fecing" HeaderText="fecing" SortExpression="fecing" />
                                                <asp:BoundField DataField="codusu" HeaderText="codusu" SortExpression="codusu">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="fecult" HeaderText="fecult" SortExpression="fecult" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
        </div>
        <asp:SqlDataSource ID="sqldsCliente" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codemp], [codus1], [codmod], [niv001], [niv002], [niv003], [usuing], [fecing], [codusu], [fecult] FROM [seg_usuario_menu] WHERE ([codmod] = @codmod)">
        <SelectParameters>
            <asp:SessionParameter Name="codmod" SessionField="gs_Codmod" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
</body>
</html>
