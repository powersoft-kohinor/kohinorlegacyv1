﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace prj_KohinorWeb
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session.Add("gs_Error", "");
            //esta variable (gs_Error) puede causar problemas en Application_Error
            //ver https://stackoverflow.com/questions/6940425/session-state-is-not-available-in-this-context

            Session.Add("gs_PagPre", ""); //para almacenar la pagina previa...usada en error 404
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // An error has occured on a .Net page.
            Exception exception = Server.GetLastError();
            clsError objError = new clsError();
            //var serverError = Server.GetLastError() as HttpException;
            if (exception != null)
            {
                //string borrar = exception.Message;
                HttpContext context = HttpContext.Current;
                if (context != null)
                {
                    if (context.Session != null)
                    {
                        string innerException = "";
                        try
                        {
                            innerException = exception.InnerException.Message;
                            innerException = HttpUtility.UrlEncode(innerException); //para que no de error> redirect uri cannot contain newline characters (reemplaza \n\r)
                        }
                        catch (Exception)
                        {
                            innerException = HttpUtility.UrlEncode(exception.Message);
                        }
                        objError = objError.f_ErrorControlado(exception);
                        if (!string.IsNullOrEmpty(exception.Message))
                        {
                            //Response.Redirect(String.Format("w_Error.aspx?errorMessage={0}&fileName={1}&lineNumber={2}&columnNumber={3}&methodName={4}&className={5}", innerException, FileName, LineNumber, ColumnNumber, MethodName, ClassName));
                            context.Server.ClearError();
                            Response.Redirect(String.Format("w_seg_Error.aspx?errorMessage={0}&fileName={1}&lineNumber={2}&columnNumber={3}&methodName={4}&fecha={5}", innerException + "; exMessage: " + objError.Mensaje, objError.Donde, objError.Linea, objError.Objeto, objError.Evento, objError.Fecha));
                        }
                        
                    }
                    else //para errores en pag aspx (diseño) ó 'Session state is not available in this context.'
                    {
                        objError = objError.f_ErrorControlado(exception);
                        Server.ClearError();
                        Response.Redirect(String.Format("w_seg_Error.aspx?errorMessage={0}&fileName={1}&lineNumber={2}&columnNumber={3}&methodName={4}&fecha={5}", objError.Mensaje, objError.Donde, objError.Linea, objError.Objeto, objError.Evento, objError.Fecha));
                    }
                }
            }
        }
    }
}