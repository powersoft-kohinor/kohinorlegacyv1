﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="prj_KohinorWeb.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no"/>
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template"/>
    <meta name="author" content="Łukasz Holeczek"/>
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard"/>
    <meta name="msapplication-TileColor" content="#ffffff"/>
    <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png"/>
    <meta name="theme-color" content="#ffffff"/>
    <title></title>

    <%--<link href="DataTables/bootstrap.min.css" rel="stylesheet" />--%>
    
    
    
    <%-- Estilo de la pagina Core UI --%>
    <link href="CoreUI/css/style.css" rel="stylesheet" />
 
    <!-- CoreUI and necessary plugins-->    
    <script src="CoreUI/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
    
    <!--[if IE]><!-->
    <script src="CoreUI/vendors/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
    <!-- Plugins and scripts required by this view-->
    <script src="CoreUI/vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js"></script>
    <script src="CoreUI/vendors/@coreui/utils/js/coreui-utils.js"></script>
    <script src="CoreUI/js/main.js"></script>

     <link href="DataTables/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <script src="DataTables/jquery-3.4.1.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%= GridView1.ClientID %>').DataTable();
    });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
        <script type="text/javascript">
            function ShowPopup() {
                $("#btnShowPopup").click();
            }
        </script> 
        <div class="container bg-light">
            
             <asp:LinkButton ID="lkbtnNuevo" runat="server" class="btn btn-danger mb-1" OnClick="lkbtnNuevo_Click">
              <span>Nuevo</span></asp:LinkButton>
            
<button id="btnShowPopup" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#ModalDanger">Danger modal</button>
            <div class="card-body">
              <div class="table-responsive" style="overflow-x:auto;">
                  <asp:GridView ID="GridView1" class="table table-bordered" runat="server" AutoGenerateColumns="False" DataKeyNames="codemp,codus1,codmod,niv001,niv002,niv003" DataSourceID="SqlDataSource1" OnRowDataBound="GridView1_RowDataBound">
                      <Columns>
                          <asp:BoundField DataField="codemp" HeaderText="codemp" ReadOnly="True" SortExpression="codemp" />
                          <asp:BoundField DataField="codus1" HeaderText="codus1" ReadOnly="True" SortExpression="codus1" />
                          <asp:BoundField DataField="codmod" HeaderText="codmod" ReadOnly="True" SortExpression="codmod" />
                          <asp:BoundField DataField="niv001" HeaderText="niv001" ReadOnly="True" SortExpression="niv001" />
                          <asp:BoundField DataField="niv002" HeaderText="niv002" ReadOnly="True" SortExpression="niv002" />
                          <asp:BoundField DataField="niv003" HeaderText="niv003" ReadOnly="True" SortExpression="niv003" />
                          <asp:BoundField DataField="usuing" HeaderText="usuing" SortExpression="usuing" />
                          <asp:BoundField DataField="fecing" HeaderText="fecing" SortExpression="fecing" DataFormatString="{0:d}" />
                          <asp:BoundField DataField="codusu" HeaderText="codusu" SortExpression="codusu" />
                          <asp:BoundField DataField="fecult" HeaderText="fecult" SortExpression="fecult" DataFormatString="{0:d}" />
                      </Columns>
                  </asp:GridView>
              </div>
            </div>
            

        </div>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codemp], [codus1], [codmod], [niv001], [niv002], [niv003], [usuing], [fecing], [codusu], [fecult] FROM [seg_usuario_menu] WHERE ([codmod] = @codmod)">
            <SelectParameters>
                <asp:Parameter DefaultValue="3" Name="codmod" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
          <script src="DataTables/datatables/jquery.dataTables.min.js"></script>
        <script src="DataTables/datatables/dataTables.bootstrap4.min.js"></script>

            <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalDanger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p><asp:Label ID="lblErrorModal" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label></p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

        
      
        

    </form>
</body>
</html>
