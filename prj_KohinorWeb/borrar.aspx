﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="borrar.aspx.cs" Inherits="prj_KohinorWeb.borrar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <a href="borrar.aspx">borrar.aspx</a>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="eddlgrvCodmod" runat="server" DataSourceID="sqldsCodmodNiv001" DataTextField="codmod" DataValueField="codmod"></asp:DropDownList>
            <asp:SqlDataSource ID="sqldsCodmodNiv001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codmod], [nommod] FROM [seg_modulo]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="sqldsCodmodNiv002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codmod], [niv001], [nommen], [nom001] FROM [seg_modulo_niv001] WHERE ([codmod] = @codmod)">
                <SelectParameters>
                    <asp:SessionParameter Name="codmod" SessionField="SSDSS" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
