﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace prj_KohinorWeb.clsCedulaSRI
{
    public class cls_AgregarCed
    {
        //esta cls tiene los metodos para llamar al API q devuelve los datos del SRI
        //http://gextionix.net:8082/api/sri?ruc=1706945944001
        //http://gextionix.net:8082/api/registrocivil?cedula=1706945944
        public cls_RootObjectCedula f_ObtenerDatosCED(string s_cedula)
        {
            //string s_ruc = "0702061730001";
            string s_url = "http://gextionix.net:8082/api/registrocivil?cedula=" + s_cedula;
            return f_download_serialized_json_data<cls_RootObjectCedula>(s_url);

        }

        public cls_RootObjectRUC f_ObtenerDatosRUC(string s_ruc)
        {
            //string s_ruc = "0702061730001";
            string s_url = "http://gextionix.net:8082/api/sri?ruc=" + s_ruc;
            return f_download_serialized_json_data<cls_RootObjectRUC>(s_url);

        }

        private static T f_download_serialized_json_data<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                // if string with JSON data is not empty, deserialize it to class and return its instance 
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }


        public cls_vc_Cliente f_buscarCliSRI(string s_codcli) //metodo para validar cedula y traer info de SRI al abrir ModalClienteNuevo
        {
            //1ro VALIDA CEDULA
            cls_vc_Cliente objCliente = new cls_vc_Cliente();
            if (s_codcli.Length == 10) //si es CEDULA
            {
                objCliente.TipInd = "2";
                objCliente.TipEmp = "3"; // NO ESTABA EN VALIDACION ORIGINAL
                if (s_codcli.Substring(2, 1).Equals("0") || s_codcli.Substring(2, 1).Equals("1") ||
                    s_codcli.Substring(2, 1).Equals("2") || s_codcli.Substring(2, 1).Equals("3") ||
                    s_codcli.Substring(2, 1).Equals("4") || s_codcli.Substring(2, 1).Equals("5"))
                {
                    objCliente.TipEmp = "3";
                }
                else
                {
                    objCliente.TipEmp = "-"; //para mandarle al error en f_ValidarCedula
                    objCliente.ErrorCed = "3er digito de cédula incorrecto.";
                }

                if (String.IsNullOrEmpty(objCliente.ErrorCed))
                {
                    //2do SI ES VALIDA LA BUSCA EN EL SRI
                    cls_AgregarCed objAgregarCed = new cls_AgregarCed();
                    cls_RootObjectCedula objCED = objAgregarCed.f_ObtenerDatosCED(s_codcli);
                    if (objCED.success == true)
                    {
                        cls_Cedula listRoot = objCED.result;
                        objCliente.Codcli = listRoot.Cedula;
                        objCliente.RucCed = listRoot.Cedula;
                        objCliente.Nomcli = listRoot.NombreCiudadano;
                        string[] s_nomcli = objCliente.Nomcli.Split(' ');
                        objCliente.Priape = s_nomcli[0];
                        objCliente.Segape = s_nomcli[1];
                        objCliente.Prinom = s_nomcli[2];
                        if (s_nomcli.Length == 4) objCliente.Segnom = s_nomcli[3];//para saber si tiene 2 nombres
                        try
                        {
                            DateTime d_date = DateTime.Parse(listRoot.FechaNacimiento);
                            string s_Fecfinal = d_date.ToString("yyyy-MM-dd");
                            objCliente.Fecnac = s_Fecfinal;
                        }
                        catch (Exception)
                        {
                            objCliente.Fecnac = DateTime.Now.Date.ToString();
                        }
                        switch (listRoot.EstadoCivil)
                        {
                            case "CASADO":
                                objCliente.Estciv = "1";
                                break;
                            case "SOLTERO":
                                objCliente.Estciv = "2";
                                break;
                            case "DIVORCIADO":
                                objCliente.Estciv = "3";
                                break;
                            case "VIUDO":
                                objCliente.Estciv = "4";
                                break;
                            default:
                                objCliente.Estciv = "1";
                                break;
                        }
                        objCliente.Dircli = listRoot.CallesDomicilio;
                        objCliente.Dirbar = listRoot.NumeroCasa;
                        switch (listRoot.Sexo)
                        {
                            case "M":
                                objCliente.Sexcli = "1";
                                break;
                            case "F":
                                objCliente.Sexcli = "2";
                                break;
                            default:
                                objCliente.Sexcli = "1";
                                break;
                        }                        
                        objCliente.Ciucli = listRoot.DPA_CantonDomicilio;
                        objCliente.DPA_ProvinDomicilio = listRoot.DPA_ProvinDomicilio;
                        objCliente.DPA_ParroqDomicilio = listRoot.DPA_ParroqDomicilio;
                        objCliente.Naccli = listRoot.Nacionalidad;
                    }
                    else
                    {
                        objCliente.ErrorCed = "No se encontro datos en SRI.";
                    }
                }
            }
            else
            {
                if (s_codcli.Length == 13) //si es RUC
                {
                    objCliente.TipInd = "1";
                    if (s_codcli.Substring(2, 1).Equals("9"))
                    {
                        objCliente.TipEmp = "1";
                    }
                    else
                    {
                        if (s_codcli.Substring(2, 1).Equals("6"))
                        {
                            objCliente.TipEmp = "2";
                        }
                        else
                        {
                            objCliente.TipEmp = "3";
                        }
                    }
                    //2do SI ES VALIDA LA BUSCA EN EL SRI
                    cls_AgregarCed objAgregarCed = new cls_AgregarCed();
                    cls_RootObjectRUC objRUC = objAgregarCed.f_ObtenerDatosRUC(s_codcli);
                    if (objRUC.success == true)
                    {
                        List<cls_RUC> listRoot = objRUC.result;
                        objCliente.Codcli = listRoot[0].nroRUC;
                        objCliente.RucCed = listRoot[0].nroRUC;
                        objCliente.Priape = "-";
                        objCliente.Prinom = "-";
                        objCliente.Nomcli = listRoot[0].razonSocial;
                        try
                        {
                            DateTime d_date = DateTime.Parse(listRoot[0].fechaInicioActividades);
                            string s_Fecfinal = d_date.ToString("yyyy-MM-dd");
                            objCliente.Fecnac = s_Fecfinal;
                        }
                        catch (Exception)
                        {
                            objCliente.Fecnac = DateTime.Now.Date.ToString();
                        }
                        objCliente.Sexcli = "1";
                        objCliente.Dircli = listRoot[0].direccionContribuyente;
                        objCliente.Ciucli = listRoot[0].DPA_NombreCanton;
                        objCliente.DPA_ProvinDomicilio = listRoot[0].DPA_NombreProvincia;
                        objCliente.DPA_ParroqDomicilio = listRoot[0].DPA_NombreParroquia;
                    }
                    else
                    {
                        objCliente.ErrorCed = "No se encontro datos en SRI.";
                    }
                }
                else
                {
                    if (s_codcli.Length == 9) //si es PASAPORTE
                    {
                        objCliente.TipInd = "3";
                        objCliente.TipEmp = "3";
                    }
                    else //ERROR, NO ES NI CEDULA NI RUC NI PASAPORTE (tiene digitos de mas o de menos)
                    {                        
                        objCliente.TipInd = "-";
                        objCliente.TipEmp = "-";
                        objCliente.RucCed = "-";
                    }
                    objCliente.ErrorCed = "No se encontro datos en SRI.";
                }
            }
            return objCliente;
        }
    }
}