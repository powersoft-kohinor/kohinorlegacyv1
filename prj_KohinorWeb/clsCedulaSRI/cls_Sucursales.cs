﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb.clsCedulaSRI
{
    public class cls_Sucursales
    {
        public string Establecimiento { get; set; }
        public string NombreComercial { get; set; }
        public string UbicacionEstablecimiento { get; set; }
        public string EstadoEstablecimiento { get; set; }
    }
}