﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class clsCliente
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public string TipInd { get; set; } //para validar cedula
        public string TipEmp { get; set; } //para validar cedula
        public string RucCed { get; set; } //para validar cedula
        public string ErrorCed { get; set; } //para grabar error del metodo  f_ValidacionCedula(string s_codcli, string s_tipemp)

        //***i ATRIBUTOS CLIENTE***//
        public string Codcli { get; set; }
        public string Codcla { get; set; }
        public string Nomcli { get; set; }
        public string Dircli { get; set; }
        public string Telcli { get; set; }
        public string Lispre { get; set; }
        public string Ciucli { get; set; }
        public string Codven { get; set; }
        public string Codpro { get; set; }
        public string Codpar { get; set; }
        public string Email { get; set; }
        public string Prinom { get; set; }
        public string Segnom { get; set; }
        public string Priape { get; set; }
        public string Segape { get; set; }
        public string Sexcli { get; set; }
        public string Estciv { get; set; }
        public string Fecnac { get; set; }
        public string Dirbar { get; set; }
        public string Telcas { get; set; }
        public string Telcel { get; set; }
        public string Naccli { get; set; }
        public string Imgcli { get; set; }
        //***f ATRIBUTOS CLIENTE***//

        public clsError Error { get; set; }


        public static string fecMax { get; set; }

        public static string fecMin { get; set; }

        public string f_ValidacionCedula(string s_codcli, string s_tipemp)
        {
            //string s_codcli = "171939445905";
            string s_CodAsi, s_PriApe, s_SegApe, s_PriNom, s_SegNom, s_CodCli, s_TipEmp;
            s_TipEmp = s_tipemp;
            double l_Fila;
            string s_CedIde, s_Aux;
            double[] r_ArrCed = new double[9];
            double[] r_AuxArr = new double[9];
            double[] r_ArrCe2 = new double[8];
            double[] r_AuxAr2 = new double[8];
            double r_Valor, r_ValDig;

            if (!(s_codcli.Trim().Length == 10 || s_codcli.Trim().Length == 13 || s_codcli.Trim().Length == 9))
            {
                return "Solo se puede ingresar RUC=13 Digitos, CI=10 Digitos, PASAPORTE=9 Digitos";
            }

            if (s_codcli.Trim().Length == 13) //RUC
            {

                if (s_codcli.Substring(12, 1) != "1")
                {
                    return "RUC el digito 13 debe ser 1";
                }

            }

            r_AuxArr[0] = 2;
            r_AuxArr[1] = 1;
            r_AuxArr[2] = 2;
            r_AuxArr[3] = 1;
            r_AuxArr[4] = 2;
            r_AuxArr[5] = 1;
            r_AuxArr[6] = 2;
            r_AuxArr[7] = 1;
            r_AuxArr[8] = 2;

            r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
            r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
            r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
            r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
            r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
            r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
            r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
            r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
            r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));

            r_Valor = 0;
            if (s_codcli.Trim().Length == 13 || s_codcli.Trim().Length == 10) //RUC o CEDULA
            {
                if (s_TipEmp == "1")
                {
                    r_AuxArr[0] = 4;
                    r_AuxArr[1] = 3;
                    r_AuxArr[2] = 2;
                    r_AuxArr[3] = 7;
                    r_AuxArr[4] = 6;
                    r_AuxArr[5] = 5;
                    r_AuxArr[6] = 4;
                    r_AuxArr[7] = 3;
                    r_AuxArr[8] = 2;
                    r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
                    r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
                    r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
                    r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
                    r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
                    r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
                    r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
                    r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
                    r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));


                    for (int i = 0; i <= 8; i++)
                    {
                        r_ValDig = r_ArrCed[i] * r_AuxArr[i];
                        //if (r_ValDig > 9)
                        //    r_ValDig = r_ValDig - 9;
                        r_Valor = r_Valor + r_ValDig;
                    }
                    r_Valor = r_Valor % 11;    //CREO DEVUELVE EL RESIDUO
                    if (r_Valor != 0)
                        r_Valor = 11 - r_Valor;
                    if (double.Parse(s_codcli.Substring(9, 1)) != r_Valor)
                    {
                        return "Dígito Autoverificador Erroneo en el Código";
                    }
                }
                if (s_TipEmp == "2")
                {
                    r_AuxAr2[0] = 3;
                    r_AuxAr2[1] = 2;
                    r_AuxAr2[2] = 7;
                    r_AuxAr2[3] = 6;
                    r_AuxAr2[4] = 5;
                    r_AuxAr2[5] = 4;
                    r_AuxAr2[6] = 3;
                    r_AuxAr2[7] = 2;
                    r_ArrCe2[0] = double.Parse(s_codcli.Substring(0, 1));
                    r_ArrCe2[1] = double.Parse(s_codcli.Substring(1, 1));
                    r_ArrCe2[2] = double.Parse(s_codcli.Substring(2, 1));
                    r_ArrCe2[3] = double.Parse(s_codcli.Substring(3, 1));
                    r_ArrCe2[4] = double.Parse(s_codcli.Substring(4, 1));
                    r_ArrCe2[5] = double.Parse(s_codcli.Substring(5, 1));
                    r_ArrCe2[6] = double.Parse(s_codcli.Substring(6, 1));
                    r_ArrCe2[7] = double.Parse(s_codcli.Substring(7, 1));

                    for (int i = 0; i <= 7; i++)
                    {
                        r_ValDig = r_ArrCe2[i] * r_AuxAr2[i];
                        //if (r_ValDig > 9)
                        //    r_ValDig = r_ValDig - 9;
                        r_Valor = r_Valor + r_ValDig;
                    }
                    r_Valor = r_Valor % 11;    //CREO DEVUELVE EL RESIDUO
                    if (r_Valor != 0)
                        r_Valor = 11 - r_Valor;
                    if (double.Parse(s_codcli.Substring(9, 1)) != r_Valor)
                    {
                        return "Digito Autoverificador Erroneo en el Código";
                    }
                }

                if (s_TipEmp == "3")
                {
                    r_AuxArr[0] = 2;
                    r_AuxArr[1] = 1;
                    r_AuxArr[2] = 2;
                    r_AuxArr[3] = 1;
                    r_AuxArr[4] = 2;
                    r_AuxArr[5] = 1;
                    r_AuxArr[6] = 2;
                    r_AuxArr[7] = 1;
                    r_AuxArr[8] = 2;
                    r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
                    r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
                    r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
                    r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
                    r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
                    r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
                    r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
                    r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
                    r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));

                    for (int i = 0; i <= 8; i++)
                    {
                        r_ValDig = r_ArrCed[i] * r_AuxArr[i];
                        if (r_ValDig > 9)
                            r_ValDig = r_ValDig - 9;
                        r_Valor = r_Valor + r_ValDig;
                    }
                    r_Valor = r_Valor % 10;    //CREO DEVUELVE EL RESIDUO
                    if (r_Valor != 0)
                        r_Valor = 10 - r_Valor;
                    if (double.Parse(s_codcli.Substring(9, 1)) != r_Valor)
                    {
                        return "Dígito Autoverificador Erroneo en el Código";
                    }
                }
            }

            r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
            r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
            r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
            r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
            r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
            r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
            r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
            r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
            r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));

            return "CORRECTO"; //SI NO HAY ERROR
        }

        public clsCliente f_Cliente_Buscar(string gs_Codemp, string gs_codcliSelected)
        {
            clsCliente objCliente = new clsCliente();
            objCliente.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_M_CLIENTE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@CODCLI", gs_codcliSelected);

            cmd.Parameters.Add("@CODCLI", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@CODCLA", SqlDbType.Char, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@NOMCLI", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@RUCCED", SqlDbType.VarChar, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@DIRCLI", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@TELCLI", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@LISPRE", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@CIUCLI", SqlDbType.Char, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@CODVEN", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@CODPRO", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@CODPAR", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@EMAIL", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@PRINOM", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@SEGNOM", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@PRIAPE", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@SEGAPE", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@SEXCLI", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@ESTCIV", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@TIPEMP", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@TIPIND", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@FECNAC", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@DIRBAR", SqlDbType.Char, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@TELCAS", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@TELCEL", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@NACCLI", SqlDbType.Char, 30).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@IMGCLI", SqlDbType.Char, 40).Direction = ParameterDirection.Output;

            try
            {
                cmd.ExecuteNonQuery();
                objCliente.Nomcli = cmd.Parameters["@NOMCLI"].Value.ToString().Trim();
                objCliente.Codcli = cmd.Parameters["@CODCLI"].Value.ToString().Trim();
                objCliente.RucCed = cmd.Parameters["@RUCCED"].Value.ToString().Trim();
                objCliente.Dircli = cmd.Parameters["@DIRCLI"].Value.ToString().Trim();
                objCliente.Telcli = cmd.Parameters["@TELCLI"].Value.ToString().Trim();
                objCliente.Ciucli = cmd.Parameters["@CIUCLI"].Value.ToString().Trim();
                objCliente.Codpro = cmd.Parameters["@CODPRO"].Value.ToString().Trim();
                objCliente.Codpar = cmd.Parameters["@CODPAR"].Value.ToString().Trim();
                objCliente.Email = cmd.Parameters["@EMAIL"].Value.ToString().Trim();
                objCliente.Prinom = cmd.Parameters["@PRINOM"].Value.ToString().Trim();
                objCliente.Segnom = cmd.Parameters["@SEGNOM"].Value.ToString().Trim();
                objCliente.Priape = cmd.Parameters["@PRIAPE"].Value.ToString().Trim();
                objCliente.Segape = cmd.Parameters["@SEGAPE"].Value.ToString().Trim();
                objCliente.Sexcli = cmd.Parameters["@SEXCLI"].Value.ToString().Trim();
                objCliente.Estciv = cmd.Parameters["@ESTCIV"].Value.ToString().Trim();
                objCliente.Lispre = cmd.Parameters["@LISPRE"].Value.ToString().Trim();
                objCliente.Codcla = cmd.Parameters["@CODCLA"].Value.ToString().Trim();
                objCliente.Codven = cmd.Parameters["@CODVEN"].Value.ToString().Trim();
                objCliente.TipEmp = cmd.Parameters["@TIPEMP"].Value.ToString().Trim();
                objCliente.TipInd = cmd.Parameters["@TIPIND"].Value.ToString().Trim();
                objCliente.Fecnac = cmd.Parameters["@FECNAC"].Value.ToString();
                objCliente.Dirbar = cmd.Parameters["@DIRBAR"].Value.ToString().Trim();
                objCliente.Telcas = cmd.Parameters["@TELCAS"].Value.ToString().Trim();
                objCliente.Telcel = cmd.Parameters["@TELCEL"].Value.ToString().Trim();
                objCliente.Naccli = cmd.Parameters["@NACCLI"].Value.ToString().Trim();
                objCliente.Imgcli = cmd.Parameters["@IMGCLI"].Value.ToString().Trim();
                
            }
            catch (Exception ex)
            {
                objCliente.Error = objCliente.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objCliente;
        }

        public clsError f_Cliente_Actualizar(string gs_Codemp, string s_prinom, string s_segnom, string s_priape,
           string s_segape, string s_nomcli, string s_codcli, string s_tipind,
           string s_tipemp, string s_rucced, string s_fecha, string s_sexcli, string s_estciv,
           string s_naccli, string s_dircli, string s_dirbar, string s_codpro,
           string s_ciucli, string s_codpar, string s_telcli, string s_telcas,
           string s_telcel, string s_email, string s_codcla, string s_codven, string s_codusu, string s_usuing, string s_lispre, string s_imgcli, string s_Action)
        {

            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[VC_M_CLIENTE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp); //OJO PONER CODEMP DE VARIABLE GLOBAL
            cmd.Parameters.AddWithValue("@codcli", s_codcli);
            cmd.Parameters.AddWithValue("@codcla", s_codcla);
            cmd.Parameters.AddWithValue("@nomcli", s_nomcli);
            cmd.Parameters.AddWithValue("@rucced", s_rucced);
            cmd.Parameters.AddWithValue("@dircli", s_dircli);
            cmd.Parameters.AddWithValue("@telcli", s_telcli);
            cmd.Parameters.AddWithValue("@lispre", s_lispre);
            cmd.Parameters.AddWithValue("@ciucli", s_ciucli);
            cmd.Parameters.AddWithValue("@codven", s_codven);
            cmd.Parameters.AddWithValue("@codusu", s_codusu);
            cmd.Parameters.AddWithValue("@codpro", s_codpro);
            cmd.Parameters.AddWithValue("@codpar", s_codpar);
            cmd.Parameters.AddWithValue("@email", s_email);
            cmd.Parameters.AddWithValue("@prinom", s_prinom);
            cmd.Parameters.AddWithValue("@segnom", s_segnom);
            cmd.Parameters.AddWithValue("@priape", s_priape);
            cmd.Parameters.AddWithValue("@segape", s_segape);
            cmd.Parameters.AddWithValue("@sexcli", s_sexcli);
            cmd.Parameters.AddWithValue("@estciv", s_estciv);
            cmd.Parameters.AddWithValue("@tipemp", s_tipemp);
            cmd.Parameters.AddWithValue("@fecnac", s_fecha);
            cmd.Parameters.AddWithValue("@dirbar", s_dirbar);
            cmd.Parameters.AddWithValue("@telcas", s_telcas);
            cmd.Parameters.AddWithValue("@telcel", s_telcel);
            cmd.Parameters.AddWithValue("@naccli", s_naccli);
            cmd.Parameters.AddWithValue("@tipind", s_tipind);
            cmd.Parameters.AddWithValue("@usuing", s_usuing);
            cmd.Parameters.AddWithValue("@imgcli", s_imgcli);
            
            try
            {
                cmd.ExecuteNonQuery(); //se inserto correctamente
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;

        }

        public clsError f_Cliente_Eliminar(string gs_codcliSelected)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[VC_M_CLIENTE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Delete");
            cmd.Parameters.AddWithValue("@CODCLI", gs_codcliSelected);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }


        public clsCliente f_Cliente_Principio(string gs_Codemp)
        {
            clsCliente objCliente = new clsCliente();
            objCliente.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_CLIENTE_D_PRIMER]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            //cmd.Parameters.AddWithValue("@CODCLI", Session["sg_codcliSelected"].ToString());

            cmd.Parameters.Add("@s_Codcli", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codcla", SqlDbType.Char, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Nomcli", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Rucced", SqlDbType.VarChar, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dircli", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcli", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Lispre", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Ciucli", SqlDbType.Char, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codven", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codpro", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codpar", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Email", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Prinom", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Segnom", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Priape", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Segape", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Sexcli", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Estciv", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Tipemp", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Tipind", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Fecnac", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dirbar", SqlDbType.Char, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcas", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcel", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Naccli", SqlDbType.Char, 30).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_ImgCli", SqlDbType.Char, 40).Direction = ParameterDirection.Output;

            try
            {
                cmd.ExecuteNonQuery();
                objCliente.Nomcli = cmd.Parameters["@s_Nomcli"].Value.ToString().Trim();
                objCliente.Codcli = cmd.Parameters["@s_Codcli"].Value.ToString().Trim();
                objCliente.RucCed = cmd.Parameters["@s_Rucced"].Value.ToString().Trim();
                objCliente.Dircli = cmd.Parameters["@s_Dircli"].Value.ToString().Trim();
                objCliente.Telcli = cmd.Parameters["@s_Telcli"].Value.ToString().Trim();
                objCliente.Ciucli = cmd.Parameters["@s_Ciucli"].Value.ToString().Trim();
                objCliente.Codpro = cmd.Parameters["@s_Codpro"].Value.ToString().Trim();
                objCliente.Codpar = cmd.Parameters["@s_Codpar"].Value.ToString().Trim();
                objCliente.Email = cmd.Parameters["@s_Email"].Value.ToString().Trim();
                objCliente.Prinom = cmd.Parameters["@s_Prinom"].Value.ToString().Trim();
                objCliente.Segnom = cmd.Parameters["@s_Segnom"].Value.ToString().Trim();
                objCliente.Priape = cmd.Parameters["@s_Priape"].Value.ToString().Trim();
                objCliente.Segape = cmd.Parameters["@s_Segape"].Value.ToString().Trim();
                objCliente.Sexcli = cmd.Parameters["@s_Sexcli"].Value.ToString().Trim();
                objCliente.Estciv = cmd.Parameters["@s_Estciv"].Value.ToString().Trim();
                objCliente.Lispre = cmd.Parameters["@s_Lispre"].Value.ToString().Trim();
                objCliente.Codcla = cmd.Parameters["@s_Codcla"].Value.ToString().Trim();
                objCliente.Codven = cmd.Parameters["@s_Codven"].Value.ToString().Trim();
                objCliente.TipEmp = cmd.Parameters["@s_Tipemp"].Value.ToString().Trim();
                objCliente.TipInd = cmd.Parameters["@s_Tipind"].Value.ToString().Trim();
                objCliente.Fecnac = cmd.Parameters["@s_Fecnac"].Value.ToString();
                objCliente.Dirbar = cmd.Parameters["@s_Dirbar"].Value.ToString().Trim();
                objCliente.Telcas = cmd.Parameters["@s_Telcas"].Value.ToString().Trim();
                objCliente.Telcel = cmd.Parameters["@s_Telcel"].Value.ToString().Trim();
                objCliente.Naccli = cmd.Parameters["@s_Naccli"].Value.ToString().Trim();
                objCliente.Imgcli = cmd.Parameters["@s_ImgCli"].Value.ToString().Trim();
            }
            catch (Exception ex)
            {
                objCliente.Error = objCliente.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objCliente;
        }

        public clsCliente f_Cliente_Siguiente(string gs_Codemp, string gs_Siguiente)
        {
            clsCliente objCliente = new clsCliente();
            objCliente.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_CLIENTE_D_SIGUIENTE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@NOMCLI", gs_Siguiente);

            cmd.Parameters.Add("@s_Codcli", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codcla", SqlDbType.Char, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Nomcli", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Rucced", SqlDbType.VarChar, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dircli", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcli", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Lispre", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Ciucli", SqlDbType.Char, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codven", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codpro", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codpar", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Email", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Prinom", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Segnom", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Priape", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Segape", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Sexcli", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Estciv", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Tipemp", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Tipind", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Fecnac", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dirbar", SqlDbType.Char, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcas", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcel", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Naccli", SqlDbType.Char, 30).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_ImgCli", SqlDbType.Char, 40).Direction = ParameterDirection.Output;

            try
            {
                cmd.ExecuteNonQuery();
                objCliente.Nomcli = cmd.Parameters["@s_Nomcli"].Value.ToString().Trim();
                objCliente.Codcli = cmd.Parameters["@s_Codcli"].Value.ToString().Trim();
                objCliente.RucCed = cmd.Parameters["@s_Rucced"].Value.ToString().Trim();
                objCliente.Dircli = cmd.Parameters["@s_Dircli"].Value.ToString().Trim();
                objCliente.Telcli = cmd.Parameters["@s_Telcli"].Value.ToString().Trim();
                objCliente.Ciucli = cmd.Parameters["@s_Ciucli"].Value.ToString().Trim();
                objCliente.Codpro = cmd.Parameters["@s_Codpro"].Value.ToString().Trim();
                objCliente.Codpar = cmd.Parameters["@s_Codpar"].Value.ToString().Trim();
                objCliente.Email = cmd.Parameters["@s_Email"].Value.ToString().Trim();
                objCliente.Prinom = cmd.Parameters["@s_Prinom"].Value.ToString().Trim();
                objCliente.Segnom = cmd.Parameters["@s_Segnom"].Value.ToString().Trim();
                objCliente.Priape = cmd.Parameters["@s_Priape"].Value.ToString().Trim();
                objCliente.Segape = cmd.Parameters["@s_Segape"].Value.ToString().Trim();
                objCliente.Sexcli = cmd.Parameters["@s_Sexcli"].Value.ToString().Trim();
                objCliente.Estciv = cmd.Parameters["@s_Estciv"].Value.ToString().Trim();
                objCliente.Lispre = cmd.Parameters["@s_Lispre"].Value.ToString().Trim();
                objCliente.Codcla = cmd.Parameters["@s_Codcla"].Value.ToString().Trim();
                objCliente.Codven = cmd.Parameters["@s_Codven"].Value.ToString().Trim();
                objCliente.TipEmp = cmd.Parameters["@s_Tipemp"].Value.ToString().Trim();
                objCliente.TipInd = cmd.Parameters["@s_Tipind"].Value.ToString().Trim();
                objCliente.Fecnac = cmd.Parameters["@s_Fecnac"].Value.ToString();
                objCliente.Dirbar = cmd.Parameters["@s_Dirbar"].Value.ToString().Trim();
                objCliente.Telcas = cmd.Parameters["@s_Telcas"].Value.ToString().Trim();
                objCliente.Telcel = cmd.Parameters["@s_Telcel"].Value.ToString().Trim();
                objCliente.Naccli = cmd.Parameters["@s_Naccli"].Value.ToString().Trim();
                objCliente.Imgcli = cmd.Parameters["@s_ImgCli"].Value.ToString().Trim();
            }
            catch (Exception ex)
            {
                objCliente.Error = objCliente.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objCliente;
        }

        public clsCliente f_Cliente_Final(string gs_Codemp)
        {
            clsCliente objCliente = new clsCliente();
            objCliente.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_CLIENTE_D_ULTIMO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            //cmd.Parameters.AddWithValue("@CODCLI", Session["sg_codcliSelected"].ToString());

            cmd.Parameters.Add("@s_Codcli", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codcla", SqlDbType.Char, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Nomcli", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Rucced", SqlDbType.VarChar, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dircli", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcli", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Lispre", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Ciucli", SqlDbType.Char, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codven", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codpro", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codpar", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Email", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Prinom", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Segnom", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Priape", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Segape", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Sexcli", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Estciv", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Tipemp", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Tipind", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Fecnac", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dirbar", SqlDbType.Char, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcas", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcel", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Naccli", SqlDbType.Char, 30).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_ImgCli", SqlDbType.Char, 40).Direction = ParameterDirection.Output;

            try
            {
                cmd.ExecuteNonQuery();
                objCliente.Nomcli = cmd.Parameters["@s_Nomcli"].Value.ToString().Trim();
                objCliente.Codcli = cmd.Parameters["@s_Codcli"].Value.ToString().Trim();
                objCliente.RucCed = cmd.Parameters["@s_Rucced"].Value.ToString().Trim();
                objCliente.Dircli = cmd.Parameters["@s_Dircli"].Value.ToString().Trim();
                objCliente.Telcli = cmd.Parameters["@s_Telcli"].Value.ToString().Trim();
                objCliente.Ciucli = cmd.Parameters["@s_Ciucli"].Value.ToString().Trim();
                objCliente.Codpro = cmd.Parameters["@s_Codpro"].Value.ToString().Trim();
                objCliente.Codpar = cmd.Parameters["@s_Codpar"].Value.ToString().Trim();
                objCliente.Email = cmd.Parameters["@s_Email"].Value.ToString().Trim();
                objCliente.Prinom = cmd.Parameters["@s_Prinom"].Value.ToString().Trim();
                objCliente.Segnom = cmd.Parameters["@s_Segnom"].Value.ToString().Trim();
                objCliente.Priape = cmd.Parameters["@s_Priape"].Value.ToString().Trim();
                objCliente.Segape = cmd.Parameters["@s_Segape"].Value.ToString().Trim();
                objCliente.Sexcli = cmd.Parameters["@s_Sexcli"].Value.ToString().Trim();
                objCliente.Estciv = cmd.Parameters["@s_Estciv"].Value.ToString().Trim();
                objCliente.Lispre = cmd.Parameters["@s_Lispre"].Value.ToString().Trim();
                objCliente.Codcla = cmd.Parameters["@s_Codcla"].Value.ToString().Trim();
                objCliente.Codven = cmd.Parameters["@s_Codven"].Value.ToString().Trim();
                objCliente.TipEmp = cmd.Parameters["@s_Tipemp"].Value.ToString().Trim();
                objCliente.TipInd = cmd.Parameters["@s_Tipind"].Value.ToString().Trim();
                objCliente.Fecnac = cmd.Parameters["@s_Fecnac"].Value.ToString();
                objCliente.Dirbar = cmd.Parameters["@s_Dirbar"].Value.ToString().Trim();
                objCliente.Telcas = cmd.Parameters["@s_Telcas"].Value.ToString().Trim();
                objCliente.Telcel = cmd.Parameters["@s_Telcel"].Value.ToString().Trim();
                objCliente.Naccli = cmd.Parameters["@s_Naccli"].Value.ToString().Trim();
                objCliente.Imgcli = cmd.Parameters["@s_ImgCli"].Value.ToString().Trim();
            }
            catch (Exception ex)
            {
                objCliente.Error = objCliente.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objCliente;
        }

        public clsCliente f_Cliente_Atras(string gs_Codemp, string gs_Siguiente)
        {
            clsCliente objCliente = new clsCliente();
            objCliente.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_CLIENTE_D_ANTERIOR]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@NOMCLI", gs_Siguiente);

            cmd.Parameters.Add("@s_Codcli", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codcla", SqlDbType.Char, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Nomcli", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Rucced", SqlDbType.VarChar, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dircli", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcli", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Lispre", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Ciucli", SqlDbType.Char, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codven", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codpro", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Codpar", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Email", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Prinom", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Segnom", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Priape", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Segape", SqlDbType.Char, 15).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Sexcli", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Estciv", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Tipemp", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Tipind", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Fecnac", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dirbar", SqlDbType.Char, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcas", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcel", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Naccli", SqlDbType.Char, 30).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_ImgCli", SqlDbType.Char, 40).Direction = ParameterDirection.Output;

            try
            {
                cmd.ExecuteNonQuery();
                objCliente.Nomcli = cmd.Parameters["@s_Nomcli"].Value.ToString().Trim();
                objCliente.Codcli = cmd.Parameters["@s_Codcli"].Value.ToString().Trim();
                objCliente.RucCed = cmd.Parameters["@s_Rucced"].Value.ToString().Trim();
                objCliente.Dircli = cmd.Parameters["@s_Dircli"].Value.ToString().Trim();
                objCliente.Telcli = cmd.Parameters["@s_Telcli"].Value.ToString().Trim();
                objCliente.Ciucli = cmd.Parameters["@s_Ciucli"].Value.ToString().Trim();
                objCliente.Codpro = cmd.Parameters["@s_Codpro"].Value.ToString().Trim();
                objCliente.Codpar = cmd.Parameters["@s_Codpar"].Value.ToString().Trim();
                objCliente.Email = cmd.Parameters["@s_Email"].Value.ToString().Trim();
                objCliente.Prinom = cmd.Parameters["@s_Prinom"].Value.ToString().Trim();
                objCliente.Segnom = cmd.Parameters["@s_Segnom"].Value.ToString().Trim();
                objCliente.Priape = cmd.Parameters["@s_Priape"].Value.ToString().Trim();
                objCliente.Segape = cmd.Parameters["@s_Segape"].Value.ToString().Trim();
                objCliente.Sexcli = cmd.Parameters["@s_Sexcli"].Value.ToString().Trim();
                objCliente.Estciv = cmd.Parameters["@s_Estciv"].Value.ToString().Trim();
                objCliente.Lispre = cmd.Parameters["@s_Lispre"].Value.ToString().Trim();
                objCliente.Codcla = cmd.Parameters["@s_Codcla"].Value.ToString().Trim();
                objCliente.Codven = cmd.Parameters["@s_Codven"].Value.ToString().Trim();
                objCliente.TipEmp = cmd.Parameters["@s_Tipemp"].Value.ToString().Trim();
                objCliente.TipInd = cmd.Parameters["@s_Tipind"].Value.ToString().Trim();
                objCliente.Fecnac = cmd.Parameters["@s_Fecnac"].Value.ToString();
                objCliente.Dirbar = cmd.Parameters["@s_Dirbar"].Value.ToString().Trim();
                objCliente.Telcas = cmd.Parameters["@s_Telcas"].Value.ToString().Trim();
                objCliente.Telcel = cmd.Parameters["@s_Telcel"].Value.ToString().Trim();
                objCliente.Naccli = cmd.Parameters["@s_Naccli"].Value.ToString().Trim();
                objCliente.Imgcli = cmd.Parameters["@s_ImgCli"].Value.ToString().Trim();
            }
            catch (Exception ex)
            {
                objCliente.Error = objCliente.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objCliente;
        }

    }
}