﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class clsGlobalVariablesClass
    {
        public static string ServerIP { get; set; } = ConfigurationManager.AppSettings["serverIP"].ToString(); 
        public static string ImageDirectory { get; set; } = "~\\Icon\\Menu125\\";
        public static string FecAct { get; set; } = DateTime.Now.ToString("yyy-MM-dd");
    }
}