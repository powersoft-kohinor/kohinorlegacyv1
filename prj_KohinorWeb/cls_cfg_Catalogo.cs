﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public class cls_cfg_Catalogo
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public string Nomcat { get; set; }
        public string Codcat { get; set; }
        public string Grumov { get; set; }
        public string Tipcat { get; set; }
        public string Estado { get; set; }
        public decimal Orden { get; set; }
        public string Codref { get; set; }
        public string Nomgru { get; set; }

        public DataTable dtCatalogo { get; set; }
        public DataSet dsCatalogo { get; set; }
        public clsError Error { get; set; }

        public cls_cfg_Catalogo f_Catalogo_Tipo()
        {
            cls_cfg_Catalogo objCatalogo = new cls_cfg_Catalogo();
            objCatalogo.Error = new clsError();
            conn.Open();

            using (conn)
            {

                SqlCommand cmd = new SqlCommand("[dbo].[CFG_M_CATALOGO_TIPO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    ds.Tables[0].TableName = "cfg_catalogo_tipo";

                    objCatalogo.dsCatalogo = ds;

                }
                catch (Exception ex)
                {
                    objCatalogo.Error = objCatalogo.Error.f_ErrorControlado(ex);
                }

                conn.Close();
                return objCatalogo;
            }


        }
        public cls_cfg_Catalogo f_Catalogo_Buscar(string s_Tipcat)
        {
            cls_cfg_Catalogo objCatalogo = new cls_cfg_Catalogo();
            objCatalogo.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CFG_M_CATALOGO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@Tipcat", s_Tipcat);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objCatalogo.dtCatalogo = dt;
            }
            catch (Exception ex)
            {
                objCatalogo.Error = objCatalogo.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objCatalogo;
        }

        public clsError f_Catalogo_Actualizar(cls_cfg_Catalogo objCatalogo, string gs_Codemp, string gs_usuing, string s_fecing, string gs_codusu, string s_fecult, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CFG_M_CATALOGO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@Codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@Codcat", objCatalogo.Codcat);
            cmd.Parameters.AddWithValue("@Nomcat", objCatalogo.Nomcat);
            cmd.Parameters.AddWithValue("@Tipcat", objCatalogo.Tipcat);
            cmd.Parameters.AddWithValue("@Orden", objCatalogo.Orden);
            cmd.Parameters.AddWithValue("@Codref", objCatalogo.Codref);
            cmd.Parameters.AddWithValue("@Nomgru", objCatalogo.Nomgru);
            cmd.Parameters.AddWithValue("@Usuing", gs_usuing);
            cmd.Parameters.AddWithValue("@Fecing", s_fecing);
            cmd.Parameters.AddWithValue("@Codusu", gs_codusu);
            cmd.Parameters.AddWithValue("@Fecult", s_fecult);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public clsError f_Catalogo_Eliminar(cls_cfg_Catalogo objCatalogo, string gs_Codemp, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CFG_M_CATALOGO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the delete task
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@Codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@Tipcat", objCatalogo.Tipcat);
            cmd.Parameters.AddWithValue("@Codcat", objCatalogo.Codcat);

            try
            {
                cmd.ExecuteNonQuery();
                // clear parameter after every delete
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;


        }
    }
}