﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_cfg_Moneda
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);

        public string Codmon { get; set; }
        public string Simmon { get; set; }
        public string Nommon { get; set; }
        public DataTable dtClienteClase { get; set; }
        public clsError Error { get; set; }


        public cls_cfg_Moneda f_Moneda_Buscar()
        {
            cls_cfg_Moneda objMoneda = new cls_cfg_Moneda();
            objMoneda.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CFG_M_MONEDA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objMoneda.dtClienteClase = dt;
            }
            catch (Exception ex)
            {
                objMoneda.Error = objMoneda.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objMoneda;
        }

        public clsError f_Moneda_Actualizar(cls_cfg_Moneda objMoneda, string gs_Codmon, string s_usuing, string s_fecing, string s_codusu, string s_fecult, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CFG_M_MONEDA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@Codmon", objMoneda.Codmon);
            cmd.Parameters.AddWithValue("@Nommon", objMoneda.Nommon);
            cmd.Parameters.AddWithValue("@Simmon", objMoneda.Simmon);
            cmd.Parameters.AddWithValue("@gs_Codmon", gs_Codmon); //campo para Update
            cmd.Parameters.AddWithValue("@Usuing", s_usuing);
            cmd.Parameters.AddWithValue("@Fecing", s_fecing);
            cmd.Parameters.AddWithValue("@Codusu", s_codusu);
            cmd.Parameters.AddWithValue("@Fecult", s_fecult);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public clsError f_Moneda_Eliminar(string gs_Codmon, string s_Action)
        {

            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CFG_M_MONEDA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the delete task
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@Codmon", gs_Codmon);


            try
            {
                cmd.ExecuteNonQuery();
                // clear parameter after every delete
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;



        }
    }
}
