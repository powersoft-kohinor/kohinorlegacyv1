﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_cfg_Secuencia
    {
		protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);

        public string Numero { get; set; }
        public DateTime Fecha { get; set; } = DateTime.Now;
		public clsError Error { get; set; }

		public cls_cfg_Secuencia f_CalcularSecuencia(string gs_Codemp, string codsec, string gs_Sersec)
		{
			cls_cfg_Secuencia objSecuencia = new cls_cfg_Secuencia();
			DataTable dt = new DataTable();
			objSecuencia.Error = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_SEC]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
			cmd.Parameters.AddWithValue("@codsec", codsec);
			cmd.Parameters.AddWithValue("@sersec", gs_Sersec); //del usuario en Login
			try
			{
				cmd.ExecuteNonQuery();
				SqlDataAdapter adapter = new SqlDataAdapter(cmd);
				adapter.Fill(dt);
				string s_numtra = dt.Rows[0].Field<string>("numfac");
				objSecuencia.Numero = s_numtra;
			}
			catch (Exception ex)
			{
				objSecuencia.Error = objSecuencia.Error.f_ErrorControlado(ex);
			}

			conn.Close();
			return objSecuencia;
		}

		public clsError f_ActualizarSecuencia(string gs_Codemp, string codsec, string gs_Sersec)
		{
			clsError objError = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_M_FACTURA_SEC]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
			cmd.Parameters.AddWithValue("@codsec", codsec);
			cmd.Parameters.AddWithValue("@sersec", gs_Sersec); //del usuario en Login

			try
			{
				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				objError = objError.f_ErrorControlado(ex);
			}

			conn.Close();
			return objError;
		}
	}
}