﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_cp_Dasboard
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);

        public string Codpro { get; set; }
        public string Nompro { get; set; }
        public string Codapu { get; set; }
        public string Fecemi { get; set; }
        public string Fecven { get; set; }
        public string Diaven { get; set; }
        public decimal Valtot { get; set; }
        public decimal Valnc { get; set; }
        public decimal Valabo { get; set; }
        public decimal Valsal { get; set; }
        public string Dias { get; set; }
        public string Tipo { get; set; }
        public string Totiva { get; set; }
        public string Totfac{ get; set; }
        public string Totbas { get; set; }
        public string Totiv0 { get; set; }
        public string Totnet { get; set; }
        public string Feccom { get; set; }
        public string Clacon { get; set; }
        public string Numcom { get; set; }
        public string Numser { get; set; }
       
    

        public DataTable dtDashboard { get; set; }
        public clsError Error { get; set; }

        public cls_cp_Dasboard f_Cartera(string gs_Codemp, string gs_Fecfin)
        {
            cls_cp_Dasboard objDashboard = new cls_cp_Dasboard();
            objDashboard.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_I_CARTERA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data

            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@CODPRO", "%");
            cmd.Parameters.AddWithValue("@FECFIN", gs_Fecfin);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();


                dt.Columns.Add(new DataColumn("N°", typeof(string)));
                dt.Columns.Add(new DataColumn("codpro", typeof(string)));
                dt.Columns.Add(new DataColumn("nompro", typeof(string)));
                dt.Columns.Add(new DataColumn("codapu", typeof(string)));
                dt.Columns.Add(new DataColumn("fecemi", typeof(string)));
                dt.Columns.Add(new DataColumn("fecven", typeof(string)));
                dt.Columns.Add(new DataColumn("diaven", typeof(string)));
                dt.Columns.Add(new DataColumn("valtot", typeof(decimal)));
                dt.Columns.Add(new DataColumn("valnc", typeof(decimal)));
                dt.Columns.Add(new DataColumn("valabo", typeof(decimal)));
                dt.Columns.Add(new DataColumn("valsal", typeof(decimal)));
                dt.Columns.Add(new DataColumn("dias", typeof(string)));


                sda.Fill(dt);


                objDashboard.dtDashboard = dt;
            }
            catch (Exception ex)
            {
                objDashboard.Error = objDashboard.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objDashboard;
        }

        public cls_cp_Dasboard f_Compra(string gs_Codemp, string gs_Fecini, string gs_Fecfin)
        {
            cls_cp_Dasboard objDashboard = new cls_cp_Dasboard();
            objDashboard.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_I_COMPRA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data

            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@CODPRO", "%");
            cmd.Parameters.AddWithValue("@FECINI", gs_Fecini);
            cmd.Parameters.AddWithValue("@FECFIN", gs_Fecfin);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();


                dt.Columns.Add(new DataColumn("N°", typeof(string)));
                dt.Columns.Add(new DataColumn("Tipo", typeof(string)));
                dt.Columns.Add(new DataColumn("numfac", typeof(string)));
                dt.Columns.Add(new DataColumn("fecfac", typeof(string)));
                dt.Columns.Add(new DataColumn("codpro", typeof(string)));
                dt.Columns.Add(new DataColumn("nompro", typeof(string)));
                dt.Columns.Add(new DataColumn("numser", typeof(string)));
                dt.Columns.Add(new DataColumn("numcom", typeof(string)));
                dt.Columns.Add(new DataColumn("clacon", typeof(string)));
                dt.Columns.Add(new DataColumn("feccom", typeof(string)));
                dt.Columns.Add(new DataColumn("totnet", typeof(string)));
                dt.Columns.Add(new DataColumn("totiv0", typeof(decimal)));
                dt.Columns.Add(new DataColumn("totbas", typeof(decimal)));
                dt.Columns.Add(new DataColumn("totiva", typeof(decimal)));
                dt.Columns.Add(new DataColumn("totfac", typeof(decimal)));
                


                sda.Fill(dt);


                objDashboard.dtDashboard = dt;
            }
            catch (Exception ex)
            {
                objDashboard.Error = objDashboard.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objDashboard;
        }
        public cls_cp_Dasboard f_Cartera_Tot(string gs_Codemp, string gs_Fecfin)
        {
            cls_cp_Dasboard objDashboard = new cls_cp_Dasboard();
            objDashboard.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_I_CARTERA_TOT]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data

            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@CODPRO", "%");
            cmd.Parameters.AddWithValue("@FECFIN", gs_Fecfin);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();


                dt.Columns.Add(new DataColumn("N°", typeof(string)));
                dt.Columns.Add(new DataColumn("dias", typeof(string)));
                dt.Columns.Add(new DataColumn("valsal", typeof(decimal)));
                


                sda.Fill(dt);


                objDashboard.dtDashboard = dt;
            }
            catch (Exception ex)
            {
                objDashboard.Error = objDashboard.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objDashboard;
        }





    }
}