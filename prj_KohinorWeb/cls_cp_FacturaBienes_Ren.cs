﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
	public class cls_cp_FacturaBienes_Ren
	{
		protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
		public string Codemp { get; set; }
		public string Numfac { get; set; }
		public int Numren { get; set; }
		public int Numite { get; set; }
		public string Codart { get; set; }
		public string Nomart { get; set; }
		public string Codalm { get; set; }
		public string Coduni { get; set; }
		public string Numlot { get; set; }
		public Decimal Cantid { get; set; }
		public Decimal Preuni { get; set; }
		public Decimal Coefic { get; set; }
		public Decimal Desren { get; set; }
		public string Codiva { get; set; }
		public string Codmon { get; set; }
		public string Ubifis { get; set; }
		public Decimal Exiact { get; set; }
		public string Feccad { get; set; }
		public string Tipord { get; set; }
		public string Numord { get; set; }
		public Decimal Valcot { get; set; }
		public Decimal Totext { get; set; }
		public Decimal Prec01 { get; set; }
		public Decimal Poriva { get; set; }
		public Decimal Rening { get; set; }
		public Decimal Renord { get; set; }
		public Decimal Totren { get; set; }
		public string Numing { get; set; }

		public DataTable dtFacturaRen { get; set; }
		public clsError Error { get; set; }



		public cls_cp_FacturaBienes_Ren f_Factura_Ren_Buscar(string gs_Codemp, string gs_numfacSelected)
		{
			cls_cp_FacturaBienes_Ren objFacturaRen = new cls_cp_FacturaBienes_Ren();
			objFacturaRen.Error = new clsError();
			conn.Open();
			SqlCommand cmd = new SqlCommand("[dbo].[CP_M_FACTURA_BIEN_REN]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@Action", "Select");
			cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
			cmd.Parameters.AddWithValue("@numfac", gs_numfacSelected);

			try
			{
				SqlDataAdapter sda = new SqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				dt.Columns.Add(new DataColumn("N°", typeof(string)));
				dt.Columns.Add(new DataColumn("codart", typeof(string)));
				dt.Columns.Add(new DataColumn("nomart", typeof(string)));
				dt.Columns.Add(new DataColumn("codalm", typeof(string)));
				dt.Columns.Add(new DataColumn("coduni", typeof(string)));
				dt.Columns.Add(new DataColumn("cantid", typeof(decimal)));
				dt.Columns.Add(new DataColumn("exiact", typeof(decimal)));
				dt.Columns.Add(new DataColumn("preuni", typeof(decimal)));
				dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
				dt.Columns.Add(new DataColumn("desren", typeof(decimal)));
				dt.Columns.Add(new DataColumn("totren", typeof(decimal)));
				sda.Fill(dt);

				objFacturaRen.dtFacturaRen = dt;
			}
			catch (Exception ex)
			{
				objFacturaRen.Error = objFacturaRen.Error.f_ErrorControlado(ex);
			}

			conn.Close();
			return objFacturaRen;
		}

		public clsError f_Factura_Ren_Actualizar(cls_cp_FacturaBienes_Ren objFacturaRen, string s_Action)
		{

			objFacturaRen.dtFacturaRen = f_dtFacturaRenTableType(objFacturaRen);

			clsError objError = new clsError();
			conn.Open();
			SqlCommand cmd = new SqlCommand("[dbo].[CP_M_FACTURA_BIEN_REN]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@Action", s_Action);
			var param = new SqlParameter("@FacturaRenTableType", objFacturaRen.dtFacturaRen);
			param.SqlDbType = SqlDbType.Structured;
			cmd.Parameters.Add(param);

			try
			{
				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				objError = objError.f_ErrorControlado(ex);
			}
			conn.Close();
			return objError;
		}

		public cls_cp_FacturaBienes_Ren f_CalculoPorIva(string gs_Codemp, string s_codart)
		{
			cls_cp_FacturaBienes_Ren objPedidoRen = new cls_cp_FacturaBienes_Ren();
			objPedidoRen.Error = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_REN_IVA]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
			cmd.Parameters.AddWithValue("@CODART", s_codart);

			//cmd.Parameters.Add("@s_Poriva", SqlDbType.Decimal,(13)).Direction = ParameterDirection.Output;
			SqlParameter parm = new SqlParameter("@s_Poriva", SqlDbType.Decimal);
			parm.Precision = 13;
			parm.Scale = 2;
			parm.Direction = ParameterDirection.Output;
			cmd.Parameters.Add(parm);
			try
			{
				cmd.ExecuteNonQuery();
				objPedidoRen.Poriva = decimal.Parse(cmd.Parameters["@s_Poriva"].Value.ToString().Trim());
			}
			catch (Exception ex)
			{
				objPedidoRen.Poriva = 0;
				objPedidoRen.Error = objPedidoRen.Error.f_ErrorControlado(ex);
			}

			conn.Close();
			return objPedidoRen;
		}
		public cls_cp_FacturaBienes_Ren f_dtFacturaRenToObjFacturaRen(DataTable dt) //para pasar de dt a objFacturaRen los atributos de la base
		{
			cls_cp_FacturaBienes_Ren objFacturaRen = new cls_cp_FacturaBienes_Ren();
			objFacturaRen.Error = new clsError();

			DataTable dtRen = new DataTable();
			dtRen.Columns.Add(new DataColumn("N°", typeof(string)));
			dtRen.Columns.Add(new DataColumn("codart", typeof(string)));
			dtRen.Columns.Add(new DataColumn("nomart", typeof(string)));
			dtRen.Columns.Add(new DataColumn("coduni", typeof(string)));
			dtRen.Columns.Add(new DataColumn("cantid", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("exiact", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("coefic", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("preuni", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("desren", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("prec01", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("poriva", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("totren", typeof(decimal)));


			dt.Merge(dtRen);


			objFacturaRen.dtFacturaRen = dt;
			try //en caso de q los renglones esten vacios se va al catch
			{

				objFacturaRen.Codemp = dt.Rows[0].Field<string>("codemp");
				objFacturaRen.Numfac = dt.Rows[0].Field<string>("numfac");
				objFacturaRen.Numren = dt.Rows[0].Field<int>("numren");
				objFacturaRen.Numite = dt.Rows[0].Field<int>("numite");
				objFacturaRen.Codart = dt.Rows[0].Field<string>("codart");
				objFacturaRen.Nomart = dt.Rows[0].Field<string>("nomart");
				objFacturaRen.Codalm = dt.Rows[0].Field<string>("codalm");
				objFacturaRen.Coduni = dt.Rows[0].Field<string>("coduni");
				objFacturaRen.Cantid = dt.Rows[0].Field<decimal>("cantid");
				objFacturaRen.Preuni = dt.Rows[0].Field<decimal>("preuni");
				objFacturaRen.Coefic = dt.Rows[0].Field<decimal>("coefic");
				objFacturaRen.Exiact = dt.Rows[0].Field<decimal>("exiact");
				objFacturaRen.Desren = dt.Rows[0].Field<decimal>("desren");
				objFacturaRen.Totren = dt.Rows[0].Field<decimal>("totren");
				objFacturaRen.Codiva = dt.Rows[0].Field<string>("codiva");
				objFacturaRen.Codmon = dt.Rows[0].Field<string>("codmon");
				objFacturaRen.Totext = dt.Rows[0].Field<decimal>("totext");
				objFacturaRen.Valcot = dt.Rows[0].Field<decimal?>("valcot") == null ? 0 : dt.Rows[0].Field<decimal>("valcot");
				objFacturaRen.Numlot = dt.Rows[0].Field<string>("numlot");
				objFacturaRen.Poriva = dt.Rows[0].Field<decimal?>("poriva") == null ? 0 : dt.Rows[0].Field<decimal>("poriva");
				objFacturaRen.Prec01 = dt.Rows[0].Field<decimal?>("prec01") == null ? 0 : dt.Rows[0].Field<decimal>("prec01");
				objFacturaRen.Feccad = dt.Rows[0].Field<DateTime?>("feccad") == null ? DateTime.Now.ToString("yyyy-MM-dd") : dt.Rows[0].Field<DateTime>("feccad").ToString("yyyy-MM-dd");
				objFacturaRen.Ubifis = dt.Rows[0].Field<string>("ubifis");
				objFacturaRen.Numing = dt.Rows[0].Field<string>("numing");
				objFacturaRen.Rening = dt.Rows[0].Field<decimal?>("rening") == null ? 0 : dt.Rows[0].Field<decimal>("rening");
				objFacturaRen.Tipord = dt.Rows[0].Field<string>("tipord");
				objFacturaRen.Numord = dt.Rows[0].Field<string>("numord");
				objFacturaRen.Renord = dt.Rows[0].Field<decimal?>("renord") == null ? 0 : dt.Rows[0].Field<decimal>("renord");





			}
			catch (Exception ex)
			{
				objFacturaRen.Error = new clsError();
				objFacturaRen.Error.f_ErrorControlado(ex);
			}

			return objFacturaRen;
		}

		public DataTable f_dtFacturaRenTableType(cls_cp_FacturaBienes_Ren objFacturaRen) //para generar un dataTable que tenga la estructura del TableType en SQL
		{


			DataTable dt = new DataTable();
			dt.Columns.Add(new DataColumn("codemp", typeof(string)));
			dt.Columns.Add(new DataColumn("numfac", typeof(string)));
			dt.Columns.Add(new DataColumn("numren", typeof(int)));
			dt.Columns.Add(new DataColumn("numite", typeof(int)));
			dt.Columns.Add(new DataColumn("codart", typeof(string)));
			dt.Columns.Add(new DataColumn("nomart", typeof(string)));
			dt.Columns.Add(new DataColumn("codalm", typeof(string)));
			dt.Columns.Add(new DataColumn("coduni", typeof(string)));
			dt.Columns.Add(new DataColumn("cantid", typeof(decimal)));
			dt.Columns.Add(new DataColumn("preuni", typeof(decimal)));
			dt.Columns.Add(new DataColumn("coefic", typeof(decimal)));
			dt.Columns.Add(new DataColumn("desren", typeof(decimal)));
			dt.Columns.Add(new DataColumn("totren", typeof(decimal)));
			dt.Columns.Add(new DataColumn("codiva", typeof(string)));
			dt.Columns.Add(new DataColumn("codmon", typeof(string)));
			dt.Columns.Add(new DataColumn("valcot", typeof(decimal)));
			dt.Columns.Add(new DataColumn("totext", typeof(decimal)));
			dt.Columns.Add(new DataColumn("feccad", typeof(DateTime)));
			dt.Columns.Add(new DataColumn("numlot", typeof(string)));
			dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
			dt.Columns.Add(new DataColumn("prec01", typeof(decimal)));
			dt.Columns.Add(new DataColumn("exiact", typeof(decimal)));
			dt.Columns.Add(new DataColumn("ubifis", typeof(string)));
			dt.Columns.Add(new DataColumn("numing", typeof(string)));
			dt.Columns.Add(new DataColumn("rening", typeof(decimal)));
			dt.Columns.Add(new DataColumn("tipord", typeof(string)));
			dt.Columns.Add(new DataColumn("numord", typeof(string)));
			dt.Columns.Add(new DataColumn("renord", typeof(decimal)));


			dt.Merge(objFacturaRen.dtFacturaRen);
			foreach (DataRow row in dt.Rows)
			{
				row["codemp"] = objFacturaRen.Codemp;
				row["numfac"] = objFacturaRen.Numfac;
				row["codalm"] = objFacturaRen.Codalm;
				row["codmon"] = objFacturaRen.Codmon;
				row["totext"] = objFacturaRen.Totext;
				row["numren"] = row["N°"];
				row["numite"] = row["N°"];
				row["valcot"] = objFacturaRen.Valcot;
				row["codiva"] = objFacturaRen.Codiva;
				row["ubifis"] = objFacturaRen.Ubifis;
				row["totext"] = objFacturaRen.Totext;
				row["feccad"] = objFacturaRen.Feccad;
				row["numlot"] = objFacturaRen.Numlot;
				row["numing"] = objFacturaRen.Numing;
				row["rening"] = objFacturaRen.Rening;
				row["tipord"] = objFacturaRen.Tipord;
				row["renord"] = objFacturaRen.Renord;
				row["numord"] = objFacturaRen.Numord;






			}
			dt.Columns.Remove("N°");

			return dt;
		}
	}
}