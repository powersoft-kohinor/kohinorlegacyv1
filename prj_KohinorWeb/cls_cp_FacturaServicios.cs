﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_cp_FacturaServicios
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public static string fecMax { get; set; }
        public static string fecMin { get; set; }

        public string Codemp { get; set; }
        public string Codpro { get; set; }
        public string Codcom { get; set; }
        public string Codgeo { get; set; }
        public string Numdoc { get; set; }
        public string Numser { get; set; }
        public string Numcom { get; set; }
        public int Tipcom { get; set; }
        public int Coddes { get; set; }
        public string Clacon { get; set; }
        public string Nomcxp { get; set; }
        public string Procxp { get; set; }
        public string Codusu { get; set; }
        public string Usuing { get; set; }
        public string Lispre { get; set; }
        public string Deviva { get; set; }
        public string Claimp { get; set; }
        public string Sersec { get; set; }
        public string Observ { get; set; }
        public string Esttri { get; set; }
        public string Estats { get; set; }
        public string Numfac { get; set; }
        public string Fecfac { get; set; }
        public string Estado { get; set; }
        public string Codalm { get; set; }
        public string Feccom { get; set; }
        public string Valaut { get; set; }
        public string Fecven { get; set; }
        public string Conpag { get; set; }
        public string Codmon { get; set; }
        public string Codapu { get; set; }
        public Decimal Valcot { get; set; }
        public Decimal Poriva { get; set; }
        public Decimal Pordes { get; set; }

        public Decimal Totnet { get; set; }
        public Decimal Totbas { get; set; }
        public Decimal Totdes { get; set; }
        public Decimal Totiva { get; set; }
        public Decimal Totiv0 { get; set; }
        public Decimal Totfac { get; set; }
        public DataTable dtFactura { get; set; }
        public cls_cp_FacturaServicios_Ren FacturaRen { get; set; }
        public clsError Error { get; set; }

        public string ClicxcModal { get; set; } //para enviar el codcli al modalNuevo


        public void f_FacturaFecMinFecMax()
        {
            cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
            conn.Open();
            string checkfecmax = "select MAX(fecfac) from cp_compra";
            SqlCommand fecCom = new SqlCommand(checkfecmax, conn);
            string s_fecmax = fecCom.ExecuteScalar().ToString().Trim();
            fecMax = s_fecmax;

            string checkfecmin = "select MIN(fecfac) from cp_compra";
            SqlCommand fecCom2 = new SqlCommand(checkfecmin, conn);
            string s_fecmin = fecCom2.ExecuteScalar().ToString().Trim();
            fecMin = s_fecmin;
            conn.Close();
        }
        public cls_cp_FacturaServicios f_Factura_Buscar(string gs_Codemp, string gs_numfacSelected)
        {

            cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
            objFactura.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_FACTURA_SERVICIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@numfac", gs_numfacSelected);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = f_dtFacturaTableType();
                sda.Fill(dt);


                objFactura = f_dtFacturaToObjFactura(dt);


            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objFactura;
        }
        public clsError f_Factura_Actualizar(cls_cp_FacturaServicios objFactura, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_FACTURA_SERVICIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            var param = new SqlParameter("@FacturaTableType", objFactura.dtFactura);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
        public clsError f_Factura_Eliminar(string gs_numfacSelected, string gs_Codemp)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_FACTURA_SERVICIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Delete");
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@numfac", gs_numfacSelected);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public cls_cp_FacturaServicios f_CalcularSecuencia(string gs_Codemp, string gs_Sersec) // pendiente
        {
            cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
            DataTable dtNumFac = new DataTable();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_SEC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@codsec", "CP_SER");
            cmd.Parameters.AddWithValue("@sersec", gs_Sersec); //del usuario en Login
            try
            {
                cmd.ExecuteNonQuery();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dtNumFac);
                string s_numfac = dtNumFac.Rows[0].Field<string>("numfac");
                objFactura.Numfac = s_numfac;
                objFactura.Fecfac = DateTime.Now.ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objFactura;
        }

        public cls_cp_FacturaServicios f_Factura_Buscar_Cliente(string gs_Codemp, string s_codcli)
        {
            cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_CLIENTE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@CLICXC", s_codcli);
            cmd.Parameters.AddWithValue("@NUMTAR", "");

            cmd.Parameters.Add("@s_Clicxc", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Nomcxc", SqlDbType.Char, 100).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dircli", SqlDbType.Char, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Lispre", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Observ", SqlDbType.Char, 160).Direction = ParameterDirection.Output;
            try
            {
                cmd.ExecuteNonQuery();

                string clicxc = cmd.Parameters["@s_Clicxc"].Value.ToString().Trim();
                if (clicxc.Equals(""))
                {
                    //objFactura.Clicxc = "noEncontro";
                }
                else
                {
                    //objFactura.Clicxc = clicxc;
                }

                //objFactura.Nomcxc = cmd.Parameters["@s_Nomcxc"].Value.ToString().Trim();
                //objFactura.Dircli = cmd.Parameters["@s_Dircli"].Value.ToString().Trim();
                //objFactura.Lispre = cmd.Parameters["@s_Lispre"].Value.ToString().Trim();
                //objFactura.Observ = cmd.Parameters["@s_Observ"].Value.ToString().Trim();
                //objFactura.ClicxcModal = s_codcli;
                //objFactura.Ntardes = "";
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objFactura;
        }

        public cls_cp_FacturaServicios f_Factura_Principio(string gs_Codemp, string gs_Sersec)
        {
            cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[CP_S_FACTURA_BIEN_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "PRIMERO");
            cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                objFactura = f_dtFacturaToObjFactura(ds.Tables[0]);
                objFactura.FacturaRen = new cls_cp_FacturaServicios_Ren();
                objFactura.FacturaRen = objFactura.FacturaRen.f_dtFacturaRenToObjFacturaRen(ds.Tables[1]);
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objFactura;
        }
        public cls_cp_FacturaServicios f_Factura_Siguiente(string gs_Codemp, string gs_Sersec, string gs_Siguiente)
        {
            cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[CP_S_FACTURA_BIEN_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "SIGUIENTE");
            cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);
            cmd.Parameters.AddWithValue("@NUMFAC", gs_Siguiente);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objFactura = f_dtFacturaToObjFactura(ds.Tables[0]);
                    objFactura.FacturaRen = new cls_cp_FacturaServicios_Ren();
                    objFactura.FacturaRen = objFactura.FacturaRen.f_dtFacturaRenToObjFacturaRen(ds.Tables[1]);
                    conn.Close();
                }
                else
                {
                    conn.Close();
                    objFactura = f_Factura_Final(gs_Codemp, gs_Sersec);

                }
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }
            return objFactura;

        }
        public cls_cp_FacturaServicios f_Factura_Final(string gs_Codemp, string gs_Sersec)
        {
            cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[CP_S_FACTURA_BIEN_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "ULTIMO");
            cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                objFactura = f_dtFacturaToObjFactura(ds.Tables[0]);
                objFactura.FacturaRen = new cls_cp_FacturaServicios_Ren();
                objFactura.FacturaRen = objFactura.FacturaRen.f_dtFacturaRenToObjFacturaRen(ds.Tables[1]);
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objFactura;
        }
        public cls_cp_FacturaServicios f_Factura_Atras(string gs_Codemp, string gs_Sersec, string gs_Siguiente)
        {
            cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[CP_S_FACTURA_BIEN_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "ANTERIOR");
            cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);
            cmd.Parameters.AddWithValue("@NUMFAC", gs_Siguiente);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objFactura = f_dtFacturaToObjFactura(ds.Tables[0]);
                    objFactura.FacturaRen = new cls_cp_FacturaServicios_Ren();
                    objFactura.FacturaRen = objFactura.FacturaRen.f_dtFacturaRenToObjFacturaRen(ds.Tables[1]);
                    conn.Close();
                }
                else
                {
                    conn.Close();
                    objFactura = f_Factura_Principio(gs_Codemp, gs_Sersec);

                }
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }
            return objFactura;
        }
        public cls_cp_FacturaServicios f_dtFacturaToObjFactura(DataTable dt) //para pasar de dt a objFactura los atributos de la base
        {
            cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
            objFactura.Error = new clsError();
            objFactura.dtFactura = dt;
            if (objFactura.dtFactura.Rows.Count > 0)
            {


                objFactura.Numfac = String.IsNullOrEmpty(dt.Rows[0].Field<string>("numfac")) ? "0" : dt.Rows[0].Field<string>("numfac");
                objFactura.Nomcxp = dt.Rows[0].Field<string>("nomcxp");
                objFactura.Procxp = dt.Rows[0].Field<string>("procxp");
                objFactura.Codpro = dt.Rows[0].Field<string>("codpro");
                objFactura.Lispre = dt.Rows[0].Field<string>("lispre");
                objFactura.Observ = dt.Rows[0].Field<string>("observ");
                objFactura.Fecfac = dt.Rows[0].Field<DateTime>("fecfac").ToString("yyyy-MM-dd");
                objFactura.Codgeo = dt.Rows[0].Field<string>("codgeo");
                objFactura.Estado = dt.Rows[0].Field<string>("estado");
                objFactura.Codalm = dt.Rows[0].Field<string>("codalm");
                objFactura.Totnet = dt.Rows[0].Field<decimal>("totnet");
                objFactura.Totbas = dt.Rows[0].Field<decimal>("totbas");
                objFactura.Totdes = dt.Rows[0].Field<decimal>("totdes");
                objFactura.Totiva = dt.Rows[0].Field<decimal>("totiva");
                objFactura.Totfac = dt.Rows[0].Field<decimal>("totfac");
                objFactura.Codcom = dt.Rows[0].Field<string>("codcom");
                objFactura.Feccom = dt.Rows[0].Field<DateTime?>("feccom") == null ? DateTime.Now.ToString("yyyy-MM-dd") : dt.Rows[0].Field<DateTime>("feccom").ToString("yyyy-MM-dd");
                objFactura.Valaut = dt.Rows[0].Field<DateTime?>("valaut") == null ? DateTime.Now.ToString("yyyy-MM-dd") : dt.Rows[0].Field<DateTime>("valaut").ToString("yyyy-MM-dd");
                objFactura.Fecven = dt.Rows[0].Field<DateTime?>("fecven") == null ? DateTime.Now.ToString("yyyy-MM-dd") : dt.Rows[0].Field<DateTime>("fecven").ToString("yyyy-MM-dd");
                objFactura.Estats = dt.Rows[0].Field<string>("estats");
                objFactura.Esttri = dt.Rows[0].Field<string>("esttri");
                objFactura.Deviva = dt.Rows[0].Field<string>("deviva");
                objFactura.Claimp = dt.Rows[0].Field<string>("claimp");
                objFactura.Poriva = dt.Rows[0].Field<decimal?>("poriva") == null ? 0 : dt.Rows[0].Field<decimal>("poriva");
                objFactura.Pordes = dt.Rows[0].Field<decimal?>("pordes") == null ? 0 : dt.Rows[0].Field<decimal>("pordes");
                objFactura.Numser = dt.Rows[0].Field<string>("numser");
                objFactura.Numcom = dt.Rows[0].Field<string>("numcom");
                objFactura.Codgeo = dt.Rows[0].Field<string>("codgeo");
                objFactura.Coddes = dt.Rows[0].Field<int>("coddes");
                objFactura.Lispre = dt.Rows[0].Field<string>("lispre");
                objFactura.Codemp = dt.Rows[0].Field<string>("codemp");
                objFactura.Estado = dt.Rows[0].Field<string>("estado");
                objFactura.Clacon = dt.Rows[0].Field<string>("clacon");
                objFactura.Conpag = dt.Rows[0].Field<string>("conpag");
                objFactura.Codmon = dt.Rows[0].Field<string>("codmon");
                objFactura.Codapu = dt.Rows[0].Field<string>("codapu");
                objFactura.Valcot = dt.Rows[0].Field<decimal?>("valcot") == null ? 0 : dt.Rows[0].Field<decimal>("valcot");
                objFactura.Sersec = dt.Rows[0].Field<string>("sersec");
                objFactura.Usuing = dt.Rows[0].Field<string>("usuing");
                objFactura.Codusu = dt.Rows[0].Field<string>("codusu");
            }


            return objFactura;
        }
        public DataTable f_dtFacturaTableType() //para generar un dataTable que tenga la estructura del TableType en SQL
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("codemp", typeof(string)));
            dt.Columns.Add(new DataColumn("numfac", typeof(string)));
            dt.Columns.Add(new DataColumn("fecfac", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("estado", typeof(string)));
            dt.Columns.Add(new DataColumn("procxp", typeof(string)));
            dt.Columns.Add(new DataColumn("nomcxp", typeof(string)));
            dt.Columns.Add(new DataColumn("codpro", typeof(string)));
            dt.Columns.Add(new DataColumn("codgeo", typeof(string)));
            dt.Columns.Add(new DataColumn("codalm", typeof(string)));
            dt.Columns.Add(new DataColumn("lispre", typeof(string)));
            dt.Columns.Add(new DataColumn("codcom", typeof(string)));
            dt.Columns.Add(new DataColumn("numdoc", typeof(string)));
            dt.Columns.Add(new DataColumn("observ", typeof(string)));
            dt.Columns.Add(new DataColumn("numser", typeof(string)));
            dt.Columns.Add(new DataColumn("numcom", typeof(string)));
            dt.Columns.Add(new DataColumn("clacon", typeof(string)));
            dt.Columns.Add(new DataColumn("tipcom", typeof(int)));
            dt.Columns.Add(new DataColumn("coddes", typeof(int)));
            dt.Columns.Add(new DataColumn("feccom", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("valaut", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("estats", typeof(string)));
            dt.Columns.Add(new DataColumn("esttri", typeof(string)));
            dt.Columns.Add(new DataColumn("deviva", typeof(string)));
            dt.Columns.Add(new DataColumn("claimp", typeof(string)));
            dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
            dt.Columns.Add(new DataColumn("pordes", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totnet", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totbas", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totiv0", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totiva", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totdes", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totfac", typeof(decimal)));
            dt.Columns.Add(new DataColumn("fecven", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("conpag", typeof(string)));
            dt.Columns.Add(new DataColumn("codapu", typeof(string)));
            dt.Columns.Add(new DataColumn("codmon", typeof(string)));
            dt.Columns.Add(new DataColumn("valcot", typeof(decimal)));
            dt.Columns.Add(new DataColumn("sersec", typeof(string)));
            dt.Columns.Add(new DataColumn("usuing", typeof(string)));
            dt.Columns.Add(new DataColumn("codusu", typeof(string)));



            return dt;
        }
    }
}