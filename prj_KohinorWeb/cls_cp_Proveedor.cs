﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_cp_Proveedor
    {

        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public string TipInd { get; set; } //para validar cedula
        public string TipEmp { get; set; } //para validar cedula
        public string RucCed { get; set; } //para validar cedula
        public string ErrorCed { get; set; } //para grabar error del metodo  f_ValidacionCedula(string s_codcli, string s_tipemp)

        //***i ATRIBUTOS CLIENTE***//
        public string Codemp { get; set; }
        public string Codpro { get; set; }
        public string Codcla { get; set; }
        public string Codcta { get; set; }
        public string Nompro { get; set; }
        public string Estatus { get; set; }
        public string Contac { get; set; }
        public string Numser { get; set; }
        public string Clacon { get; set; }
        public string Claimp { get; set; }
        public string Codgeo { get; set; }
        public string Codciu { get; set; }
        public string Dirpro { get; set; }
        public string Numcal { get; set; }
        public string Telpro { get; set; }
        public string Faxpro { get; set; }
        public string Apliva { get; set; }
        public decimal Limcre { get; set; }
        public string Estado { get; set; }
        public string Emapro { get; set; }
        public string Emacom { get; set; }
        public string Obspro { get; set; }
        public string Numini { get; set; }
        public string Numfin { get; set; }
        public string Bieret { get; set; }
        public string Bieiva { get; set; }
        public string Serret { get; set; }
        public string Seriva { get; set; }
        public string Bietri { get; set; }
        public string Sertri { get; set; }
        public string Deviva { get; set; }
        public string Valaut { get; set; }
        public int Tipind { get; set; }
        public int Numpag { get; set; }
        public int Plapag { get; set; }
        public decimal Valpro { get; set; }
        public string Estval { get; set; }
        public string Codban { get; set; }
        public string Tipcue { get; set; }
        public string Numcue { get; set; }
        public string Tippro { get; set; }
        public string Estrel { get; set; }
        public string Codart { get; set; }
        public string Nomart { get; set; }
        public string Cedben { get; set; }
        public string Nomben { get; set; }
        public string Usuing { get; set; }
        public string Codusu { get; set; }




        public clsError Error { get; set; }
        public DataTable dtProveedor { get; set; }


        public static string fecMax { get; set; }

        public static string fecMin { get; set; }

        public string f_ValidacionCedula(string s_codcli, string s_tipemp)
        {
            //string s_codcli = "171939445905";
            string s_TipEmp;
            s_TipEmp = s_tipemp;

            double[] r_ArrCed = new double[9];
            double[] r_AuxArr = new double[9];
            double[] r_ArrCe2 = new double[8];
            double[] r_AuxAr2 = new double[8];
            double r_Valor, r_ValDig;

            if (!(s_codcli.Trim().Length == 10 || s_codcli.Trim().Length == 13 || s_codcli.Trim().Length == 9))
            {
                return "Solo se puede ingresar RUC=13 Digitos, CI=10 Digitos, PASAPORTE=9 Digitos";
            }

            if (s_codcli.Trim().Length == 13) //RUC
            {

                if (s_codcli.Substring(12, 1) != "1")
                {
                    return "RUC el digito 13 debe ser 1";
                }

            }

            r_AuxArr[0] = 2;
            r_AuxArr[1] = 1;
            r_AuxArr[2] = 2;
            r_AuxArr[3] = 1;
            r_AuxArr[4] = 2;
            r_AuxArr[5] = 1;
            r_AuxArr[6] = 2;
            r_AuxArr[7] = 1;
            r_AuxArr[8] = 2;

            r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
            r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
            r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
            r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
            r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
            r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
            r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
            r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
            r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));

            r_Valor = 0;
            if (s_codcli.Trim().Length == 13 || s_codcli.Trim().Length == 10) //RUC o CEDULA
            {
                if (s_TipEmp == "1")
                {
                    r_AuxArr[0] = 4;
                    r_AuxArr[1] = 3;
                    r_AuxArr[2] = 2;
                    r_AuxArr[3] = 7;
                    r_AuxArr[4] = 6;
                    r_AuxArr[5] = 5;
                    r_AuxArr[6] = 4;
                    r_AuxArr[7] = 3;
                    r_AuxArr[8] = 2;
                    r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
                    r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
                    r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
                    r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
                    r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
                    r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
                    r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
                    r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
                    r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));


                    for (int i = 0; i <= 8; i++)
                    {
                        r_ValDig = r_ArrCed[i] * r_AuxArr[i];
                        //if (r_ValDig > 9)
                        //    r_ValDig = r_ValDig - 9;
                        r_Valor = r_Valor + r_ValDig;
                    }
                    r_Valor = r_Valor % 11;    //CREO DEVUELVE EL RESIDUO
                    if (r_Valor != 0)
                        r_Valor = 11 - r_Valor;
                    if (double.Parse(s_codcli.Substring(9, 1)) != r_Valor)
                    {
                        return "Dígito Autoverificador Erroneo en el Código";
                    }
                }
                if (s_TipEmp == "2")
                {
                    r_AuxAr2[0] = 3;
                    r_AuxAr2[1] = 2;
                    r_AuxAr2[2] = 7;
                    r_AuxAr2[3] = 6;
                    r_AuxAr2[4] = 5;
                    r_AuxAr2[5] = 4;
                    r_AuxAr2[6] = 3;
                    r_AuxAr2[7] = 2;
                    r_ArrCe2[0] = double.Parse(s_codcli.Substring(0, 1));
                    r_ArrCe2[1] = double.Parse(s_codcli.Substring(1, 1));
                    r_ArrCe2[2] = double.Parse(s_codcli.Substring(2, 1));
                    r_ArrCe2[3] = double.Parse(s_codcli.Substring(3, 1));
                    r_ArrCe2[4] = double.Parse(s_codcli.Substring(4, 1));
                    r_ArrCe2[5] = double.Parse(s_codcli.Substring(5, 1));
                    r_ArrCe2[6] = double.Parse(s_codcli.Substring(6, 1));
                    r_ArrCe2[7] = double.Parse(s_codcli.Substring(7, 1));

                    for (int i = 0; i <= 7; i++)
                    {
                        r_ValDig = r_ArrCe2[i] * r_AuxAr2[i];
                        //if (r_ValDig > 9)
                        //    r_ValDig = r_ValDig - 9;
                        r_Valor = r_Valor + r_ValDig;
                    }
                    r_Valor = r_Valor % 11;    //CREO DEVUELVE EL RESIDUO
                    if (r_Valor != 0)
                        r_Valor = 11 - r_Valor;
                    if (double.Parse(s_codcli.Substring(9, 1)) != r_Valor)
                    {
                        return "Digito Autoverificador Erroneo en el Código";
                    }
                }

                if (s_TipEmp == "3")
                {
                    r_AuxArr[0] = 2;
                    r_AuxArr[1] = 1;
                    r_AuxArr[2] = 2;
                    r_AuxArr[3] = 1;
                    r_AuxArr[4] = 2;
                    r_AuxArr[5] = 1;
                    r_AuxArr[6] = 2;
                    r_AuxArr[7] = 1;
                    r_AuxArr[8] = 2;
                    r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
                    r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
                    r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
                    r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
                    r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
                    r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
                    r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
                    r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
                    r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));

                    for (int i = 0; i <= 8; i++)
                    {
                        r_ValDig = r_ArrCed[i] * r_AuxArr[i];
                        if (r_ValDig > 9)
                            r_ValDig = r_ValDig - 9;
                        r_Valor = r_Valor + r_ValDig;
                    }
                    r_Valor = r_Valor % 10;    //CREO DEVUELVE EL RESIDUO
                    if (r_Valor != 0)
                        r_Valor = 10 - r_Valor;
                    if (double.Parse(s_codcli.Substring(9, 1)) != r_Valor)
                    {
                        return "Dígito Autoverificador Erroneo en el Código";
                    }
                }
            }

            r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
            r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
            r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
            r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
            r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
            r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
            r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
            r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
            r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));

            return "CORRECTO"; //SI NO HAY ERROR
        }

        public cls_cp_Proveedor f_Proveedor_Buscar(string gs_Codemp, string gs_codproSelected) //busca 1 solo cliente
        {
            cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
            objProveedor.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_PROVEEDOR]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@codpro", gs_codproSelected);
            try
            {


                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = f_dtProveedorTableType();
                sda.Fill(dt);
                objProveedor = f_dtProveedorToObjProveedor(dt);


            }
            catch (Exception ex)
            {
                objProveedor.Error = objProveedor.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objProveedor;
        }

        public clsError f_Proveedor_Actualizar(cls_cp_Proveedor objProveedor, string s_Action)
        {

            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_PROVEEDOR]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@codemp", objProveedor.Codemp);
            cmd.Parameters.AddWithValue("@codpro", objProveedor.Codpro);
            cmd.Parameters.AddWithValue("@codcla", objProveedor.Codcla);

            var param = new SqlParameter("@ProveedorTableType", objProveedor.dtProveedor);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;

        }

        public clsError f_Proveedor_Eliminar(string gs_Codemp, string gs_codproSelected)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_PROVEEDOR]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Delete");
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@codpro", gs_codproSelected);


            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public cls_cp_Proveedor f_Proveedor_Principio(string gs_Codemp)
        {
            cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
            objProveedor.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[CP_S_PROVEEDOR_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "PRIMERO");


            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                objProveedor = f_dtProveedorToObjProveedor(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                objProveedor.Error = objProveedor.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objProveedor;
        }
        public cls_cp_Proveedor f_Proveedor_Siguiente(string gs_Codemp, string gs_Siguiente)
        {
            cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
            objProveedor.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[CP_S_PROVEEDOR_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "SIGUIENTE");
            cmd.Parameters.AddWithValue("@nompro", gs_Siguiente);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objProveedor = f_dtProveedorToObjProveedor(ds.Tables[0]);

                    conn.Close();
                }
                else
                {
                    conn.Close();
                    objProveedor = f_Proveedor_Final(gs_Codemp);

                }
            }
            catch (Exception ex)
            {
                objProveedor.Error = objProveedor.Error.f_ErrorControlado(ex);
            }
            return objProveedor;

        }
        public cls_cp_Proveedor f_Proveedor_Final(string gs_Codemp)
        {
            cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
            objProveedor.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[CP_S_PROVEEDOR_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "ULTIMO");


            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                objProveedor = f_dtProveedorToObjProveedor(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                objProveedor.Error = objProveedor.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objProveedor;
        }
        public cls_cp_Proveedor f_Proveedor_Atras(string gs_Codemp, string gs_Siguiente)
        {
            cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
            objProveedor.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[CP_S_PROVEEDOR_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "ANTERIOR");
            cmd.Parameters.AddWithValue("@nompro", gs_Siguiente);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objProveedor = f_dtProveedorToObjProveedor(ds.Tables[0]);

                    conn.Close();
                }
                else
                {
                    conn.Close();
                    objProveedor = f_Proveedor_Principio(gs_Codemp);

                }
            }
            catch (Exception ex)
            {
                objProveedor.Error = objProveedor.Error.f_ErrorControlado(ex);
            }
            return objProveedor;
        }
        public cls_cp_Proveedor f_dtProveedorToObjProveedor(DataTable dt) //para pasar de dt a objFactura los atributos de la base
        {

            cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
            objProveedor.Error = new clsError();
            objProveedor.dtProveedor = dt;
            if (objProveedor.dtProveedor.Rows.Count > 0)
            {
                objProveedor.Codemp = dt.Rows[0].Field<string>("codemp");
                objProveedor.Codpro = dt.Rows[0].Field<string>("codpro").Trim();
                objProveedor.Codcla = dt.Rows[0].Field<string>("codcla");
                objProveedor.Codcta = dt.Rows[0].Field<string>("codcta");
                objProveedor.Nompro = dt.Rows[0].Field<string>("nompro");
                objProveedor.Tipind = dt.Rows[0].Field<int>("tipind");
                objProveedor.RucCed = dt.Rows[0].Field<string>("rucced");
                objProveedor.TipEmp = dt.Rows[0].Field<string>("tipemp");
                objProveedor.Estatus = dt.Rows[0].Field<string>("estatus");
                objProveedor.Contac = dt.Rows[0].Field<string>("contac");
                objProveedor.Numser = dt.Rows[0].Field<string>("numser");
                objProveedor.Clacon = dt.Rows[0].Field<string>("clacon");
                objProveedor.Claimp = dt.Rows[0].Field<string>("claimp");
                objProveedor.Codgeo = dt.Rows[0].Field<string>("codgeo");
                objProveedor.Codciu = dt.Rows[0].Field<string>("codciu");
                objProveedor.Dirpro = dt.Rows[0].Field<string>("dirpro");
                objProveedor.Numcal = dt.Rows[0].Field<string>("numcal");
                objProveedor.Telpro = dt.Rows[0].Field<string>("telpro");
                objProveedor.Faxpro = dt.Rows[0].Field<string>("faxpro");
                objProveedor.Apliva = dt.Rows[0].Field<string>("apliva");
                objProveedor.Limcre = dt.Rows[0].Field<decimal>("limcre");
                objProveedor.Estado = dt.Rows[0].Field<string>("estado");
                objProveedor.Emapro = dt.Rows[0].Field<string>("emapro");
                objProveedor.Emacom = dt.Rows[0].Field<string>("emacom");
                objProveedor.Obspro = dt.Rows[0].Field<string>("obspro");
                objProveedor.Numini = dt.Rows[0].Field<string>("numini");
                objProveedor.Numfin = dt.Rows[0].Field<string>("numfin");
                objProveedor.Bieret = dt.Rows[0].Field<string>("bieret");
                objProveedor.Bieiva = dt.Rows[0].Field<string>("bieiva");
                objProveedor.Serret = dt.Rows[0].Field<string>("serret");
                objProveedor.Seriva = dt.Rows[0].Field<string>("seriva");
                objProveedor.Bietri = dt.Rows[0].Field<string>("bietri");
                objProveedor.Sertri = dt.Rows[0].Field<string>("sertri");
                objProveedor.Deviva = dt.Rows[0].Field<string>("deviva");
                objProveedor.Valaut = dt.Rows[0].Field<string>("valaut") == null ? DateTime.Now.ToString("yyyy-MM-dd") : dt.Rows[0].Field<string>("valaut");
                objProveedor.Numpag = dt.Rows[0].Field<int?>("numpag") == null ? 0 : dt.Rows[0].Field<int>("numpag");
                objProveedor.Plapag = dt.Rows[0].Field<int?>("plapag") == null ? 0 : dt.Rows[0].Field<int>("plapag");
                objProveedor.Valpro = dt.Rows[0].Field<decimal?>("valpro") == null ? 0 : dt.Rows[0].Field<decimal>("valpro");
                objProveedor.Estval = dt.Rows[0].Field<string>("estval");
                objProveedor.Codban = dt.Rows[0].Field<string>("codban");
                objProveedor.Tipcue = dt.Rows[0].Field<string>("tipcue");
                objProveedor.Numcue = dt.Rows[0].Field<string>("numcue");
                objProveedor.Tippro = dt.Rows[0].Field<string>("tippro");
                objProveedor.Estrel = dt.Rows[0].Field<string>("estrel");
                objProveedor.Codart = dt.Rows[0].Field<string>("codart");
                objProveedor.Nomart = dt.Rows[0].Field<string>("nomart");
                objProveedor.Cedben = dt.Rows[0].Field<string>("cedben");
                objProveedor.Nomben = dt.Rows[0].Field<string>("nomben");
                objProveedor.Usuing = dt.Rows[0].Field<string>("usuing");
                objProveedor.Codusu = dt.Rows[0].Field<string>("codusu");

            }
            return objProveedor;
        }
        public DataTable f_dtProveedorTableType() //para generar un dataTable que tenga la estructura del TableType en SQL
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("codemp", typeof(string)));
            dt.Columns.Add(new DataColumn("codpro", typeof(string)));
            dt.Columns.Add(new DataColumn("codcla", typeof(string)));
            dt.Columns.Add(new DataColumn("codcta", typeof(string)));
            dt.Columns.Add(new DataColumn("nompro", typeof(string)));
            dt.Columns.Add(new DataColumn("tipind", typeof(int)));
            dt.Columns.Add(new DataColumn("rucced", typeof(string)));
            dt.Columns.Add(new DataColumn("tipemp", typeof(string)));
            dt.Columns.Add(new DataColumn("estatus", typeof(string)));
            dt.Columns.Add(new DataColumn("contac", typeof(string)));
            dt.Columns.Add(new DataColumn("numser", typeof(string)));
            dt.Columns.Add(new DataColumn("clacon", typeof(string)));
            dt.Columns.Add(new DataColumn("claimp", typeof(string)));
            dt.Columns.Add(new DataColumn("codgeo", typeof(string)));
            dt.Columns.Add(new DataColumn("codciu", typeof(string)));
            dt.Columns.Add(new DataColumn("dirpro", typeof(string)));
            dt.Columns.Add(new DataColumn("numcal", typeof(string)));
            dt.Columns.Add(new DataColumn("telpro", typeof(string)));
            dt.Columns.Add(new DataColumn("faxpro", typeof(string)));
            dt.Columns.Add(new DataColumn("apliva", typeof(string)));
            dt.Columns.Add(new DataColumn("limcre", typeof(decimal)));
            dt.Columns.Add(new DataColumn("estado", typeof(string)));
            dt.Columns.Add(new DataColumn("emapro", typeof(string)));
            dt.Columns.Add(new DataColumn("emacom", typeof(string)));
            dt.Columns.Add(new DataColumn("obspro", typeof(string)));
            dt.Columns.Add(new DataColumn("numini", typeof(string)));
            dt.Columns.Add(new DataColumn("numfin", typeof(string)));
            dt.Columns.Add(new DataColumn("bieret", typeof(string)));
            dt.Columns.Add(new DataColumn("bieiva", typeof(string)));
            dt.Columns.Add(new DataColumn("serret", typeof(string)));
            dt.Columns.Add(new DataColumn("seriva", typeof(string)));
            dt.Columns.Add(new DataColumn("bietri", typeof(string)));
            dt.Columns.Add(new DataColumn("sertri", typeof(string)));
            dt.Columns.Add(new DataColumn("deviva", typeof(string)));
            dt.Columns.Add(new DataColumn("valaut", typeof(string)));
            dt.Columns.Add(new DataColumn("numpag", typeof(int)));
            dt.Columns.Add(new DataColumn("plapag", typeof(int)));
            dt.Columns.Add(new DataColumn("valpro", typeof(decimal)));
            dt.Columns.Add(new DataColumn("estval", typeof(string)));
            dt.Columns.Add(new DataColumn("codban", typeof(string)));
            dt.Columns.Add(new DataColumn("tipcue", typeof(string)));
            dt.Columns.Add(new DataColumn("numcue", typeof(string)));
            dt.Columns.Add(new DataColumn("tippro", typeof(string)));
            dt.Columns.Add(new DataColumn("estrel", typeof(string)));
            dt.Columns.Add(new DataColumn("codart", typeof(string)));
            dt.Columns.Add(new DataColumn("nomart", typeof(string)));
            dt.Columns.Add(new DataColumn("cedben", typeof(string)));
            dt.Columns.Add(new DataColumn("nomben", typeof(string)));
            dt.Columns.Add(new DataColumn("usuing", typeof(string)));
            dt.Columns.Add(new DataColumn("codusu", typeof(string)));


            return dt;
        }


    }
}