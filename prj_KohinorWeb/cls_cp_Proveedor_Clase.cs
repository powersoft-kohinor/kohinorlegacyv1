﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
namespace prj_KohinorWeb
{
    public class cls_cp_Proveedor_Clase
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);

        public string Codemp { get; set; }
        public string Codcla { get; set; }
        public string Nomcla { get; set; }
        public string Codcta { get; set; }
        public string Ctaref { get; set; }
        public DataTable dtProveedorClase { get; set; }
        public clsError Error { get; set; }


        public cls_cp_Proveedor_Clase f_ProveedorClase_Buscar()
        {
            cls_cp_Proveedor_Clase objProveedorClase = new cls_cp_Proveedor_Clase();
            objProveedorClase.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_PROVEEDOR_CLASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objProveedorClase.dtProveedorClase = dt;
            }
            catch (Exception ex)
            {
                objProveedorClase.Error = objProveedorClase.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objProveedorClase;
        }

        public clsError f_ProveedorClase_Actualizar(cls_cp_Proveedor_Clase objClienteClase, string gs_Codcla, string s_usuing, string s_fecing, string s_codusu, string s_fecult, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_PROVEEDOR_CLASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@Codemp", objClienteClase.Codemp);
            cmd.Parameters.AddWithValue("@Nomcla", objClienteClase.Nomcla);
            cmd.Parameters.AddWithValue("@Codcla", objClienteClase.Codcla);
            cmd.Parameters.AddWithValue("@gs_Codcla", gs_Codcla); //campo para Update
            cmd.Parameters.AddWithValue("@Codcta", objClienteClase.Codcta);
            cmd.Parameters.AddWithValue("@Ctaref", objClienteClase.Ctaref);
            cmd.Parameters.AddWithValue("@Usuing", s_usuing);
            cmd.Parameters.AddWithValue("@Fecing", s_fecing);
            cmd.Parameters.AddWithValue("@Codusu", s_codusu);
            cmd.Parameters.AddWithValue("@Fecult", s_fecult);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public clsError f_ProveedorClase_Eliminar(string gs_Codcla, string gs_Codemp, string s_Action)
        {

            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_PROVEEDOR_CLASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the delete task
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@Codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@Codcla", gs_Codcla);


            try
            {
                cmd.ExecuteNonQuery();
                // clear parameter after every delete
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;




        }
    }
}