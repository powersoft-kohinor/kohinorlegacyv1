﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_cp_Proveedor_Pago
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public string Codemp { get; set; }
        public int Codmod { get; set; }
        public string Numcpp { get; set; }
        public string Tipdoc { get; set; }
        public string Codpro { get; set; }
        public string Procxp { get; set; }
        public string Numtra { get; set; }
        public string Fecemi { get; set; }
        public string Fecven { get; set; }
        public string Fectra { get; set; }
        public string Concep { get; set; }
        public decimal Valcob { get; set; }
        public string Numcco { get; set; }
        public string Tiporg { get; set; }
        public string Numorg { get; set; }
        public string Fecimi { get; set; }
        public string Codsuc { get; set; }
        public string Codalm { get; set; }
        public string Sersec { get; set; }
        public string Codcom { get; set; }
        public string Codmon { get; set; }
        public string Numdoc { get; set; }
        public string Tipcco { get; set; }
        public decimal Totcuo { get; set; }
        public string Codapu { get; set; }
        public string Codap1 { get; set; }
        public decimal Valcot { get; set; }
        public decimal Numcuo { get; set; }
        public string Salcob { get; set; }
        public string Estado { get; set; }
        public string Nompro { get; set; }
        
        public string Codusu { get; set; }
      
        public DataTable dtPago { get; set; }
        public clsError Error { get; set; }





        public cls_cp_Proveedor_Pago f_Cuentas_Pagar(String s_Codemp, String s_Codpro)
        {
            cls_cp_Proveedor_Pago objPago = new cls_cp_Proveedor_Pago();
            objPago.Error = new clsError();
            conn.Open(); 
            SqlCommand cmd = new SqlCommand("[dbo].[CP_S_SALDO_CXP]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
         
            cmd.Parameters.AddWithValue("@CODEMP", s_Codemp);
            cmd.Parameters.AddWithValue("@CODPRO", s_Codpro);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                objPago.dtPago = dt;
                
            }
            catch (Exception ex)
            {
                objPago.Error = objPago.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objPago;
        }

        public cls_cp_Proveedor_Pago f_CalcularSecuencia(string gs_Codemp, string gs_Sersec) // pendiente
        {
            cls_cp_Proveedor_Pago objPago = new cls_cp_Proveedor_Pago();
            DataTable dtPago = new DataTable();
            objPago.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_SEC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@codsec", "CP_CPA");
            cmd.Parameters.AddWithValue("@sersec", gs_Sersec); //del usuario en Login
            try
            {
                cmd.ExecuteNonQuery();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dtPago);
                string s_numcco = dtPago.Rows[0].Field<string>("numfac");
                objPago.Numcco = s_numcco;
                objPago.Fecimi = DateTime.Now.ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {
                objPago.Error = objPago.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objPago;
        }

        public cls_cp_Proveedor_Pago f_Proveedor_Pago_Buscar(string gs_Codemp, string gs_numcppSelected)
        {
            cls_cp_Proveedor_Pago objPago = new cls_cp_Proveedor_Pago();
            objPago.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_CUENTAS_POR_PAGAR_FORMA_PAGO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@numcpp", gs_numcppSelected);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("N°", typeof(string)));
                dt.Columns.Add(new DataColumn("codart", typeof(string)));
                dt.Columns.Add(new DataColumn("nomart", typeof(string)));
                dt.Columns.Add(new DataColumn("codalm", typeof(string)));
                dt.Columns.Add(new DataColumn("coduni", typeof(string)));
                dt.Columns.Add(new DataColumn("cantid", typeof(decimal)));
                dt.Columns.Add(new DataColumn("exiact", typeof(decimal)));
                dt.Columns.Add(new DataColumn("preuni", typeof(decimal)));
                dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
                dt.Columns.Add(new DataColumn("desren", typeof(decimal)));
                dt.Columns.Add(new DataColumn("totren", typeof(decimal)));
                sda.Fill(dt);

                objPago.dtPago = dt;
            }
            catch (Exception ex)
            {
                objPago.Error = objPago.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objPago;
        }



        public clsError f_Proveedor_Pago_FormaPago_Actualizar(cls_cp_Proveedor_Pago objPago, string s_Action)
        {
            

            objPago.dtPago = f_dtPagoTableType(objPago);

            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_CUENTAS_POR_PAGAR_FORMA_PAGO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", s_Action);
            var param = new SqlParameter("@cuentasPorPagarFormaPagoType", objPago.dtPago);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }



        public clsError f_Proveedor_Pago_Actualizar(cls_cp_Proveedor_Pago objPago, string s_Action)
        {
           

            objPago.dtPago = f_dtPagoTableType(objPago);

            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_CUENTAS_POR_PAGAR]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", s_Action);
            var param = new SqlParameter("@cuentasPorPagarType", objPago.dtPago);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                 objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public DataTable f_dtPagoTableType(cls_cp_Proveedor_Pago objPago) //para generar un dataTable que tenga la estructura del TableType en SQL
        {


            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("codemp", typeof(string)));
            dt.Columns.Add(new DataColumn("numcpp", typeof(string)));
            dt.Columns.Add(new DataColumn("tipdoc", typeof(string)));
            dt.Columns.Add(new DataColumn("codpro", typeof(string)));
            dt.Columns.Add(new DataColumn("procxp", typeof(string)));
            dt.Columns.Add(new DataColumn("numtra", typeof(string)));
            dt.Columns.Add(new DataColumn("fecemi", typeof(string)));
            dt.Columns.Add(new DataColumn("fecven", typeof(string)));
            dt.Columns.Add(new DataColumn("fectra", typeof(string)));
            dt.Columns.Add(new DataColumn("concep", typeof(string)));
            dt.Columns.Add(new DataColumn("valcob", typeof(decimal)));
            dt.Columns.Add(new DataColumn("tiporg", typeof(string)));
            dt.Columns.Add(new DataColumn("numorg", typeof(string)));
            dt.Columns.Add(new DataColumn("codcom", typeof(string)));
            dt.Columns.Add(new DataColumn("numdoc", typeof(string)));
            dt.Columns.Add(new DataColumn("codapu", typeof(string)));
            dt.Columns.Add(new DataColumn("codap1", typeof(string)));
            dt.Columns.Add(new DataColumn("codmon", typeof(string)));
            dt.Columns.Add(new DataColumn("valcot", typeof(decimal)));
            dt.Columns.Add(new DataColumn("codsuc", typeof(string)));
            dt.Columns.Add(new DataColumn("numlot", typeof(string)));
            dt.Columns.Add(new DataColumn("sersec", typeof(string)));
            dt.Columns.Add(new DataColumn("numcco", typeof(string)));
            dt.Columns.Add(new DataColumn("numcuo", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totcuo", typeof(string)));
            dt.Columns.Add(new DataColumn("codusu", typeof(string)));
    
    
    






            dt.Merge(objPago.dtPago);
            foreach (DataRow row in dt.Rows)
            {
                row["codemp"] = objPago.Codemp;
                row["numcpp"] = objPago.Numcpp;
                row["tipdoc"] = objPago.Tipdoc;
                row["numdoc"] = objPago.Numdoc;
                row["numcco"] = objPago.Numcco;
                row["tipdoc"] = objPago.Tipdoc;
                row["codpro"] = objPago.Codpro;
                row["codusu"] = objPago.Codusu;
                row["fectra"] = objPago.Fectra;
                
               
              


               
         
             
                //row["codmon"] = objPago.Codmon;
                //row["totext"] = objPago.Totext;
                //row["numren"] = row["N°"];
                //row["numite"] = row["N°"];
                //row["valcot"] = objPago.Valcot;
                //row["codiva"] = objPago.Codiva;
                //row["ubifis"] = objPago.Ubifis;
                //row["totext"] = objPago.Totext;
                //row["feccad"] = objPago.Feccad;
                //row["numlot"] = objPago.Numlot;
                //row["numing"] = objPago.Numing;
                //row["rening"] = objPago.Rening;
                //row["tipord"] = objPago.Tipord;
                //row["renord"] = objPago.Renord;
                //row["numord"] = objPago.Numord;






            }
            dt.Columns.Remove("N°");
            dt.Columns.Remove("Tipo");
            return dt;
        }







        public cls_cp_Proveedor_Pago f_dtCuentasPagarToCuentasPagar(DataRow dt) //para pasar de dt a objFactura los atributos de la base
        {
            cls_cp_Proveedor_Pago objPago = new cls_cp_Proveedor_Pago();
            objPago.Error = new clsError();
            objPago.dtPago = dt.Table;

            if (objPago.dtPago.Rows.Count > 0)
            {
                

                objPago.Numtra = String.IsNullOrEmpty(dt.Field<string>("numtra")) ? "0" : dt.Field<string>("numtra");
                objPago.Procxp = dt.Field<string>("procxp");
                objPago.Concep = dt.Field<string>("concep");
                objPago.Valcob = dt.Field<decimal?>("valcob") == null ? 0 : dt.Field<decimal>("valcob");
                objPago.Fecemi = dt.Field<DateTime>("fecemi") == null ? DateTime.Now.ToString("yyyy-MM-dd") : dt.Field<DateTime>("fecemi").ToString("yyyy-MM-dd");
                objPago.Fecven = dt.Field<DateTime>("fecven") == null ? DateTime.Now.ToString("yyyy-MM-dd") : dt.Field<DateTime>("fecven").ToString("yyyy-MM-dd");
            }


            return objPago;
        }

    }
}