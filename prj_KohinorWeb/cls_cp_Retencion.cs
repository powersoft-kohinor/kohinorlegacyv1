﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_cp_Retencion
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);

        public string Codemp { get; set; }
        public string Numfac { get; set; }
        public string Codren { get; set; }
        public string Codret { get; set; }
        public string Debhab { get; set; }
        public decimal Basret { get; set; }
        public decimal Porret { get; set; }
        public decimal Totret { get; set; }
        public string Usuing { get; set; }
        public string Fecing { get; set; }
        public string Codusu { get; set; }

        public DataTable dtRetencion { get; set; }
        public clsError Error { get; set; }

        public cls_cp_Retencion f_RetencionS_Buscar(string gs_Codemp, string gs_numfacSelected)
        {
            cls_cp_Retencion objRetencion = new cls_cp_Retencion();
            objRetencion.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_RETENCION_S]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");

            cmd.Parameters.AddWithValue("@Codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@Numfac", gs_numfacSelected);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();


                dt.Columns.Add(new DataColumn("N°", typeof(string)));
                dt.Columns.Add(new DataColumn("codemp", typeof(string)));
                dt.Columns.Add(new DataColumn("numfac", typeof(string)));
                dt.Columns.Add(new DataColumn("codren", typeof(int)));
                dt.Columns.Add(new DataColumn("codret", typeof(string)));
                dt.Columns.Add(new DataColumn("basret", typeof(decimal)));
                dt.Columns.Add(new DataColumn("porret", typeof(decimal)));
                dt.Columns.Add(new DataColumn("totret", typeof(decimal)));
                dt.Columns.Add(new DataColumn("debhab", typeof(string)));
                dt.Columns.Add(new DataColumn("usuing", typeof(string)));
                dt.Columns.Add(new DataColumn("fecing", typeof(string)));
                dt.Columns.Add(new DataColumn("codusu", typeof(string)));
                sda.Fill(dt);


                objRetencion.dtRetencion = dt;
            }
            catch (Exception ex)
            {
                objRetencion.Error = objRetencion.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objRetencion;
        }

        public clsError f_RetencionS_Actualizar(cls_cp_Retencion objRetencion, string s_Action)
        {

            objRetencion.dtRetencion = f_dtRentencionTableType(objRetencion);
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_RETENCION_S]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            var param = new SqlParameter("@RetencionTableType", objRetencion.dtRetencion);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public clsError f_RetencionS_Eliminar(cls_cp_Retencion objRetencion, string s_Action)
        {

            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_RETENCION_S]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the delete task
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@Codemp", objRetencion.Codemp);
            cmd.Parameters.AddWithValue("@Codren", objRetencion.Codren);
            cmd.Parameters.AddWithValue("@Numfac", objRetencion.Numfac);

            try
            {
                cmd.ExecuteNonQuery();
                // clear parameter after every delete
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;




        }
        public cls_cp_Retencion f_Retencion_Buscar(string gs_Codemp, string gs_numfacSelected)
        {
            cls_cp_Retencion objRetencion = new cls_cp_Retencion();
            objRetencion.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_RETENCION]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            
            cmd.Parameters.AddWithValue("@Codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@Numfac", gs_numfacSelected);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                

                dt.Columns.Add(new DataColumn("N°", typeof(string)));
                dt.Columns.Add(new DataColumn("codemp", typeof(string)));
                dt.Columns.Add(new DataColumn("numfac", typeof(string)));
                dt.Columns.Add(new DataColumn("codren", typeof(int)));
                dt.Columns.Add(new DataColumn("codret", typeof(string)));
                dt.Columns.Add(new DataColumn("basret", typeof(decimal)));
                dt.Columns.Add(new DataColumn("porret", typeof(decimal)));
                dt.Columns.Add(new DataColumn("totret", typeof(decimal)));
                dt.Columns.Add(new DataColumn("debhab", typeof(string)));
                dt.Columns.Add(new DataColumn("usuing", typeof(string)));
                dt.Columns.Add(new DataColumn("fecing", typeof(string)));
                dt.Columns.Add(new DataColumn("codusu", typeof(string)));
                sda.Fill(dt);


                objRetencion.dtRetencion = dt;
            }
            catch (Exception ex)
            {
                objRetencion.Error = objRetencion.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objRetencion;
        }

        public clsError f_Retencion_Actualizar(cls_cp_Retencion objRetencion, string s_Action)
        {

            objRetencion.dtRetencion = f_dtRentencionTableType(objRetencion);
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_RETENCION]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            var param = new SqlParameter("@RetencionTableType", objRetencion.dtRetencion);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            try
            {
                cmd.ExecuteNonQuery();
            } 
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public clsError f_Retencion_Eliminar(cls_cp_Retencion objRetencion, string s_Action)
        {

            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[CP_M_RETENCION]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the delete task
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@Codemp", objRetencion.Codemp);
            cmd.Parameters.AddWithValue("@Codren", objRetencion.Codren);
            cmd.Parameters.AddWithValue("@Numfac", objRetencion.Numfac);

            try
            {
                cmd.ExecuteNonQuery();
                // clear parameter after every delete
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;




        }

        public DataTable f_dtRentencionTableType(cls_cp_Retencion objFacturaRen) //para generar un dataTable que tenga la estructura del TableType en SQL
        {
            
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("codemp", typeof(string)));
            dt.Columns.Add(new DataColumn("numfac", typeof(string)));
            dt.Columns.Add(new DataColumn("codren", typeof(int)));
            dt.Columns.Add(new DataColumn("codret", typeof(string)));
            dt.Columns.Add(new DataColumn("basret", typeof(decimal)));
            dt.Columns.Add(new DataColumn("porret", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totret", typeof(decimal)));
            dt.Columns.Add(new DataColumn("debhab", typeof(string)));
            dt.Columns.Add(new DataColumn("usuing", typeof(string)));
            dt.Columns.Add(new DataColumn("fecing", typeof(string)));
            dt.Columns.Add(new DataColumn("codusu", typeof(string)));


            dt.Merge(objFacturaRen.dtRetencion);
            foreach (DataRow row in dt.Rows)
            {
                row["codemp"] = objFacturaRen.Codemp;
                row["numfac"] = objFacturaRen.Numfac;
                row["usuing"] = objFacturaRen.Usuing;
                row["fecing"] = objFacturaRen.Fecing;
                row["codusu"] = objFacturaRen.Codusu;
                
                row["codren"] = Convert.ToInt64(row["N°"]);
                row["codret"] = row["Retencion"];
                row["debhab"] = "H";
                row["totret"] = Convert.ToDecimal(row["Valor"].ToString());
                row["basret"] = String.IsNullOrEmpty(row["Base"].ToString()) ? 0 : Convert.ToDecimal(row["Base"].ToString());
                row["porret"] = String.IsNullOrEmpty(row["%"].ToString()) ? 0 : Convert.ToDecimal(row["%"].ToString());

                
            }
            dt.Columns.Remove("N°");
            dt.Columns.Remove("Retencion");
            dt.Columns.Remove("Valor");
            dt.Columns.Remove("Base");
            dt.Columns.Remove("%");
            dt.Columns.Remove("fecing");
            return dt;
        }
    }
}