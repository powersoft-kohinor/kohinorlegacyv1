﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_fin_Plancuenta
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);

        public string codcta { get; set; }
        public string nomcta { get; set; }
        public string codmon { get; set; }
        public DataTable dtPlancuenta { get; set; }
        public clsError Error { get; set; }


        public cls_fin_Plancuenta f_PlanCuenta_Buscar()
        {
            cls_fin_Plancuenta objPlanCuenta = new cls_fin_Plancuenta();
            objPlanCuenta.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP(100) codcta, nomcta, codmon FROM fin_plancuenta", conn);


            // call the select task to get all data

            try
            {

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objPlanCuenta.dtPlancuenta = dt;
            }
            catch (Exception ex)
            {
                objPlanCuenta.Error = objPlanCuenta.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objPlanCuenta;
        }


    }
}