﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_inv_Articulo
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public string Codart { get; set; }
        public string Codcla { get; set; }
        public string Codiva { get; set; }
        public string Coduni { get; set; }
        public string Nomart { get; set; }
        public string Codfam { get; set; }
        public decimal Poriva { get; set; }
        public string Codalt { get; set; }
        public string Talart { get; set; }
        public string Colart { get; set; }
        public decimal Prec01 { get; set; }
        public decimal Prec02 { get; set; }
        public decimal Prec03 { get; set; }
        public decimal Prec04 { get; set; }
        public decimal Eximin { get; set; }
        public decimal Eximax { get; set; }
        public decimal Punreo { get; set; }
        public string Ubifis { get; set; }
        public decimal Exiact { get; set; }
        public decimal Ultcos { get; set; }
        public decimal Totcos { get; set; }
        public decimal Cospro { get; set; }
        public decimal Cosfob { get; set; }
        public string Feccad { get; set; }
        public string Observ { get; set; }
        public string Codusu { get; set; }
        public string Estado { get; set; }
        public string Estjue { get; set; }
        public string Codarc { get; set; }
        public string Codfab { get; set; }
        public string Tipart { get; set; }
        public decimal Tieent { get; set; }
        public decimal Desc01 { get; set; }
        public decimal Desc02 { get; set; }
        public decimal Desc03 { get; set; }
        public decimal Desc04 { get; set; }
        public string desart { get; set; }
        public string codmod { get; set; }
        public string numlot { get; set; }
        public string codpro { get; set; }
        public string nompro { get; set; }
        public string nomfam { get; set; }
        public string codgru { get; set; }
        public string codgen { get; set; }
        public string nomtit { get; set; }
        public string marart { get; set; }
        public string usuing { get; set; }
        public decimal numbul { get; set; }
        public decimal pesart { get; set; }
        public decimal larart { get; set; }
        public decimal ancart { get; set; }
        public decimal volart { get; set; }
        public decimal altart { get; set; }
        public decimal canped { get; set; }
        public string nomadi { get; set; }
        public string codcli { get; set; }
        public string nomcli { get; set; }
        public string galmaq { get; set; }
        public string numheb { get; set; }
        public string carpet { get; set; }
        public string fecpro { get; set; }
        public string sersec { get; set; }
        public string colecc { get; set; }
        public decimal prec05 { get; set; }
        public decimal prec06 { get; set; }
        public decimal prec07 { get; set; }
        public string arttip { get; set; }
        public string artman { get; set; }
        public string artdis { get; set; }
        public string artest { get; set; }
        public string artgal { get; set; }
        public string fecdes { get; set; }
        public string estser { get; set; }
        public string estweb { get; set; }
        public string gru001 { get; set; }
        public string gru002 { get; set; }
        public string gru003 { get; set; }
        public string gru004 { get; set; }
        public string gru005 { get; set; }
        public string gru006 { get; set; }
        public string gru007 { get; set; }
        public decimal numpar { get; set; }
        public string modbas { get; set; }
        public string modulo { get; set; }
        public string descri { get; set; }
        public string tipmue { get; set; }
        public decimal numper { get; set; }
        public decimal sectej { get; set; }
        public decimal saccom { get; set; }
        public string desarc { get; set; }
        public string tipcaj { get; set; }
        public string estaut { get; set; }
        public string fecaut { get; set; }
        public string usuaut { get; set; }
        public string codhil { get; set; }
        public string nomhil { get; set; }
        public string codton { get; set; }
        public string marcli { get; set; }
        public decimal canmin { get; set; }
        public string nommat { get; set; }
        public decimal val001 { get; set; }
        public decimal val002 { get; set; }
        public decimal val003 { get; set; }
        public decimal val004 { get; set; }
        public decimal val005 { get; set; }
        public decimal val006 { get; set; }
        public string codcom { get; set; }
        public string nomcom { get; set; }
        public string nomimg { get; set; }
        public string estpre { get; set; }
        public string fecpre { get; set; }
        public string usupre { get; set; }
        public string artcod { get; set; }
        public string estcod { get; set; }
        public string feccod { get; set; }
        public string usucod { get; set; }
        public string numsec { get; set; }
        public float porice { get; set; }
        public decimal prohor { get; set; }
        public string tipmod { get; set; }
        public string coddis { get; set; }
        public decimal nummod { get; set; }
        public string traord { get; set; }
        public decimal pesteo { get; set; }
        public string matpri { get; set; }
        public string largo { get; set; }
        public string forord { get; set; }
        public string espeso { get; set; }
        public string colmat { get; set; }
        public string claimp { get; set; }
        public string calord { get; set; }
        public string bocsup { get; set; }
        public string bocinf { get; set; }
        public string ancord { get; set; }
        public string tipimp { get; set; }
        public string tintas { get; set; }
        public string colore { get; set; }
        public string cyrele { get; set; }
        public string estpun { get; set; }
        public decimal pripro { get; set; }
        public decimal valpun { get; set; }
        public string pesuni { get; set; }
        public decimal desc05 { get; set; }
        public decimal desc06 { get; set; }


        public DataTable dtArticulo { get; set; }
        public clsError Error { get; set; }



        public cls_inv_Articulo f_dtArticuloToObjArticulo(DataTable dt) //para pasar de dt a objFactura los atributos de la base
        {

            cls_inv_Articulo objArticulo = new cls_inv_Articulo();
            objArticulo.Error = new clsError();
            objArticulo.dtArticulo = dt;
            if (objArticulo.dtArticulo.Rows.Count > 0)
            {

                //[codart], A.[nomart], A.[prec01], A.[prec02], A.[prec03], A.[prec04], A.[eximax], A.[codiva], A.[coduni], A.[exiact], A.[ultcos], I.[poriva]


                objArticulo.Codart = dt.Rows[0].Field<string>("codart");

                objArticulo.Nomart = dt.Rows[0].Field<string>("nomart").Trim();
                objArticulo.Coduni = dt.Rows[0].Field<string>("coduni").Trim();
                objArticulo.Prec01 = dt.Rows[0].Field<decimal?>("prec01") == null ? 0 : dt.Rows[0].Field<decimal>("prec01");
                objArticulo.Prec02 = dt.Rows[0].Field<decimal?>("prec02") == null ? 0 : dt.Rows[0].Field<decimal>("prec02");
                objArticulo.Prec03 = dt.Rows[0].Field<decimal?>("prec03") == null ? 0 : dt.Rows[0].Field<decimal>("prec03");
                objArticulo.Prec04 = dt.Rows[0].Field<decimal?>("prec04") == null ? 0 : dt.Rows[0].Field<decimal>("prec04");
                objArticulo.Exiact = dt.Rows[0].Field<decimal?>("exiact") == null ? 0 : dt.Rows[0].Field<decimal>("exiact");
                objArticulo.Ultcos = dt.Rows[0].Field<decimal?>("ultcos") == null ? 0 : dt.Rows[0].Field<decimal>("ultcos");
                objArticulo.Poriva = dt.Rows[0].Field<decimal?>("poriva") == null ? 0 : dt.Rows[0].Field<decimal>("poriva");



            }
            return objArticulo;
        }
        public cls_inv_Articulo f_dtArticuloToObjArticuloServicios(DataTable dt) //para pasar de dt a objFactura los atributos de la base
        {

            cls_inv_Articulo objArticulo = new cls_inv_Articulo();
            objArticulo.Error = new clsError();
            objArticulo.dtArticulo = dt;
            if (objArticulo.dtArticulo.Rows.Count > 0)
            {

                //[codart], A.[nomart], A.[prec01], A.[prec02], A.[prec03], A.[prec04], A.[eximax], A.[codiva], A.[coduni], A.[exiact], A.[ultcos], I.[poriva]


                objArticulo.Codart = dt.Rows[0].Field<string>("codart");

                objArticulo.Nomart = dt.Rows[0].Field<string>("nomart").Trim();
                objArticulo.Coduni = dt.Rows[0].Field<string>("coduni").Trim();
                objArticulo.Prec01 = dt.Rows[0].Field<decimal?>("prec01") == null ? 0 : dt.Rows[0].Field<decimal>("prec01");
                objArticulo.Prec02 = dt.Rows[0].Field<decimal?>("prec02") == null ? 0 : dt.Rows[0].Field<decimal>("prec02");
                objArticulo.Prec03 = dt.Rows[0].Field<decimal?>("prec03") == null ? 0 : dt.Rows[0].Field<decimal>("prec03");
                objArticulo.Prec04 = dt.Rows[0].Field<decimal?>("prec04") == null ? 0 : dt.Rows[0].Field<decimal>("prec04");

                objArticulo.Ultcos = dt.Rows[0].Field<decimal?>("ultcos") == null ? 0 : dt.Rows[0].Field<decimal>("ultcos");
                objArticulo.Poriva = dt.Rows[0].Field<decimal?>("poriva") == null ? 0 : dt.Rows[0].Field<decimal>("poriva");



            }
            return objArticulo;
        }








    }
}