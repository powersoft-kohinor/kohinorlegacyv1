﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_seg_Modulo
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public int Codmod { get; set; }
        public string Nommod { get; set; }
        public string Nomext { get; set; }
        public string Imgmod { get; set; }
        public string Imgdes { get; set; }
        public string Refmod { get; set; }
        public DataTable dtModulo { get; set; }
        public clsError Error { get; set; }

        public cls_seg_Modulo f_Modulo_Buscar()
        {
            cls_seg_Modulo objModulo = new cls_seg_Modulo();
            objModulo.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_MODULO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    row["imgmod"] = clsGlobalVariablesClass.ImageDirectory + row["imgmod"];
                    row["imgdes"] = clsGlobalVariablesClass.ImageDirectory + row["imgdes"];
                }
                objModulo.dtModulo = dt;
            }
            catch (Exception ex)
            {
                objModulo.Error = objModulo.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objModulo;
        }

        public clsError f_Modulo_Actualizar(cls_seg_Modulo objModulo, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_MODULO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@CODMOD", objModulo.Codmod);
            cmd.Parameters.AddWithValue("@NOMMOD", objModulo.Nommod);
            cmd.Parameters.AddWithValue("@NOMEXT", objModulo.Nomext);
            cmd.Parameters.AddWithValue("@IMGMOD", objModulo.Imgmod);
            cmd.Parameters.AddWithValue("@IMGDES", objModulo.Imgdes);
            cmd.Parameters.AddWithValue("@REFMOD", objModulo.Refmod);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
        public clsError f_Modulo_Eliminar(string gs_Codmod, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_MODULO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the delete task
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@CODMOD", gs_Codmod);

            try
            {
                cmd.ExecuteNonQuery();
                // clear parameter after every delete
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
    }
}