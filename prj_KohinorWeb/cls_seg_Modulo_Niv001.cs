﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_seg_Modulo_Niv001
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public int Codmod { get; set; }
        public int Niv001 { get; set; }
        public string Nommen { get; set; }
        public string Nom001 { get; set; }
        public string Ext001 { get; set; }
        public string Img001 { get; set; }
        public string Imgdes { get; set; }
        public string Ref001 { get; set; }
        public string Refext { get; set; }
        public DataTable dtModuloNiv001 { get; set; }
        public clsError Error { get; set; }

        public cls_seg_Modulo_Niv001 f_Niv001_Buscar(string gs_Codmod)
        {
            cls_seg_Modulo_Niv001 objNiv001 = new cls_seg_Modulo_Niv001();
            objNiv001.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_MODULO_NIV001]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@CODMOD", gs_Codmod);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objNiv001.dtModuloNiv001 = dt;
            }
            catch (Exception ex)
            {
                objNiv001.Error = objNiv001.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objNiv001;
        }
        public clsError f_Niv001_Actualizar(cls_seg_Modulo_Niv001 objNiv001, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_MODULO_NIV001]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@Codmod", objNiv001.Codmod);
            cmd.Parameters.AddWithValue("@Niv001", objNiv001.Niv001);
            cmd.Parameters.AddWithValue("@Nommen", objNiv001.Nommen);
            cmd.Parameters.AddWithValue("@Nom001", objNiv001.Nom001);
            cmd.Parameters.AddWithValue("@Ext001", objNiv001.Ext001);
            cmd.Parameters.AddWithValue("@Img001", objNiv001.Img001);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
        public clsError f_Niv001_Eliminar(string gs_Codmod, string s_niv001, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_MODULO_NIV001]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the delete task
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@CODMOD", gs_Codmod);
            cmd.Parameters.AddWithValue("@NIV001", s_niv001);

            try
            {
                cmd.ExecuteNonQuery();
                // clear parameter after every delete
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
    }
}