﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_seg_Modulo_Niv002
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public int Codmod { get; set; }
        public int Niv001 { get; set; }
        public int Niv002 { get; set; }
        public string Nommen { get; set; }
        public string Nom001 { get; set; }
        public string Nom002 { get; set; }
        public string Ext002 { get; set; }
        public string Img002 { get; set; }
        public string Imgdes { get; set; }
        public string Ref002 { get; set; }
        public string Refext { get; set; }
        public DataTable dtModuloNiv002 { get; set; }
        public clsError Error { get; set; }


        public cls_seg_Modulo_Niv002 f_Niv002_Buscar(string gs_Codmod, string s_niv001)
        {
            cls_seg_Modulo_Niv002 objNiv002 = new cls_seg_Modulo_Niv002();
            objNiv002.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_MODULO_NIV002]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@CODMOD", gs_Codmod);
            cmd.Parameters.AddWithValue("@NIV001", s_niv001);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objNiv002.dtModuloNiv002 = dt;
            }
            catch (Exception ex)
            {
                objNiv002.Error = objNiv002.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objNiv002;
        }

        public clsError f_Niv002_Actualizar(cls_seg_Modulo_Niv002 objNiv002, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_MODULO_NIV002]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@Codmod", objNiv002.Codmod);
            cmd.Parameters.AddWithValue("@Niv001", objNiv002.Niv001);
            cmd.Parameters.AddWithValue("@Niv002", objNiv002.Niv002);
            cmd.Parameters.AddWithValue("@Nommen", objNiv002.Nommen);
            cmd.Parameters.AddWithValue("@Nom001", objNiv002.Nom001);
            cmd.Parameters.AddWithValue("@Nom002", objNiv002.Nom002);
            cmd.Parameters.AddWithValue("@Ext002", objNiv002.Ext002);
            cmd.Parameters.AddWithValue("@Ref002", objNiv002.Ref002);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public clsError f_Niv002_Eliminar(string gs_Codmod, string s_niv001, string s_niv002, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_MODULO_NIV002]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the delete task
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@CODMOD", gs_Codmod);
            cmd.Parameters.AddWithValue("@NIV001", s_niv001);
            cmd.Parameters.AddWithValue("@NIV002", s_niv002);

            try
            {
                cmd.ExecuteNonQuery();
                // clear parameter after every delete
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
    }
}