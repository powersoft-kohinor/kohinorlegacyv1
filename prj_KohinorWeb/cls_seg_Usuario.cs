﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace prj_KohinorWeb
{
    public class cls_seg_Usuario
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public string Rucemp { get; set; }
        public string Codemp { get; set; }
        public string Codus1 { get; set; }
        public string Nomusu { get; set; }
        public string Clausu { get; set; }
        public string Codsuc { get; set; }
        public string Sersec { get; set; }
        public string Usmail { get; set; }
        public string Fecexp { get; set; }
        public string Fecing { get; set; }
        public string Estado { get; set; }
        public string Tipacc { get; set; }
        public string Estusu { get; set; }
        public string Pcnom { get; set; }
        public string Pcmac { get; set; }
        public string Pcip { get; set; }
        public string Nomdbm { get; set; }
        public string Nombas { get; set; }
        public string Usubas { get; set; }
        public string Clabas { get; set; }
        public string Ipserv { get; set; }
        public string Tipusu { get; set; }
        public string Gruusu { get; set; }
        public string Asiusu { get; set; }
        public string Codven { get; set; }
        public string Codusu { get; set; }
        public string Codalm { get; set; }
        public string Estcie { get; set; }
        public string Usuing { get; set; }
        public string Codfun { get; set; }
        public Decimal Limcre { get; set; }
        public Decimal Numpag { get; set; }
        public Decimal Plapag { get; set; }
        public string Fotusu { get; set; }
        public string Resnom { get; set; }
        public string Resare { get; set; }
        public string Resmai { get; set; }
        public string Restel { get; set; }
        public string Tecnom { get; set; }
        public string Tecare { get; set; }
        public string Tecmai { get; set; }
        public string Tectel { get; set; }
        public string Codcli { get; set; }
        public string Nomcli { get; set; }
        public string Banord { get; set; }
        public string Banaut { get; set; }
        public string Bancot { get; set; }
        public string Bancor { get; set; }
        public string Banfac { get; set; }
        public string Banped { get; set; }
        public string Banbar { get; set; }
        public string Banche { get; set; }
        public string Bandes { get; set; }
        public Decimal Pordes { get; set; }
        public string Estenc { get; set; }
        public string Claenc { get; set; }
        public string Cedusu { get; set; }
        public string Codmer { get; set; }
        public string Arcfir { get; set; }
        public string Clafir { get; set; }

        public string Fecult { get; set; }

        public string Ultent { get; set; } //? es para que pueda ser null como en la base y no de error
        public string Ultsal { get; set; }



        public DataTable dtUsuario { get; set; }

        public clsError Error { get; set; }


        //i*****************LOGIN****************
        public cls_seg_Usuario f_BuscarUsuarioAdminLogin(string s_codus1, string s_pass, string s_pcip, string s_Action)
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.dtUsuario = new DataTable();
            objUsuario.Error = new clsError();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_V_USUARIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Codus1", s_codus1);
            cmd.Parameters.AddWithValue("@ClaUsu", s_pass);
            cmd.Parameters.AddWithValue("@IP", s_pcip);
            cmd.Parameters.AddWithValue("@Action", s_Action);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(objUsuario.dtUsuario);
            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objUsuario;
        }
        public cls_seg_Usuario f_BuscarUsuarioLogin(string s_rucemp, string s_codus1, string s_pass, string s_pcip, string s_Action)
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.dtUsuario = new DataTable();
            objUsuario.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_V_USUARIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RucEmp", s_rucemp);
            cmd.Parameters.AddWithValue("@Codus1", s_codus1);
            cmd.Parameters.AddWithValue("@ClaUsu", s_pass);
            cmd.Parameters.AddWithValue("@IP", s_pcip);
            cmd.Parameters.AddWithValue("@Action", s_Action);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(objUsuario.dtUsuario);
            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objUsuario;
        }
        //f*****************LOGIN****************

        public clsError f_Usuario_Clave(cls_seg_Usuario objUsuario)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_CLAVE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CodUs1", objUsuario.Codus1);
            cmd.Parameters.AddWithValue("@ClaUsu", objUsuario.Clausu);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }        

        public cls_seg_Usuario f_Usuario_Base_Buscar(string ip) //para usuario inicial
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_INICIAL]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@IP", ip);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                //DataSet ds = new DataSet();
                //sda.Fill(ds);
                //objUsuario = f_dtUsuarioToObjUsuario(ds.Tables[0]);

                objUsuario.Codus1 = dt.Rows[0].Field<string>("codus1");
                objUsuario.Rucemp = dt.Rows[0].Field<string>("rucemp");
                objUsuario.Nomusu = dt.Rows[0].Field<string>("nomusu");
                objUsuario.Codsuc = dt.Rows[0].Field<string>("codsuc");
                objUsuario.Pcip = dt.Rows[0].Field<string>("pcip");
            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
                if (objUsuario.Error.Mensaje.Equals("❗ There is no row at position 0.") || objUsuario.Error.Mensaje.Equals("❗ No hay ninguna fila en la posición 0."))
                {
                    objUsuario.Error.Mensaje = "❗ Máquina no registrada. Digitar RUC manualmente.";
                }
            }

            conn.Close();
            return objUsuario;
        }

        public clsError f_Usuario_Actualizar(cls_seg_Usuario objUsuario, String s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@CODEMP", objUsuario.Codemp);
            cmd.Parameters.AddWithValue("@CODUS1", objUsuario.Codus1);
            cmd.Parameters.AddWithValue("@TIPUSU", objUsuario.Tipusu);
            cmd.Parameters.AddWithValue("@NOMUSU", objUsuario.Nomusu);
            cmd.Parameters.AddWithValue("@FECING", objUsuario.Fecing);
            cmd.Parameters.AddWithValue("@FECEXP", objUsuario.Fecexp);
            cmd.Parameters.AddWithValue("@TIPACC", objUsuario.Tipacc);
            cmd.Parameters.AddWithValue("@GRUUSU", objUsuario.Gruusu);
            cmd.Parameters.AddWithValue("@ASIUSU", objUsuario.Asiusu);
            cmd.Parameters.AddWithValue("@CODVEN", objUsuario.Codven);
            cmd.Parameters.AddWithValue("@CODUSU", objUsuario.Codusu);
            cmd.Parameters.AddWithValue("@FECULT", objUsuario.Fecult);
            cmd.Parameters.AddWithValue("@CODALM", objUsuario.Codalm);
            cmd.Parameters.AddWithValue("@ESTCIE", objUsuario.Estcie);
            cmd.Parameters.AddWithValue("@SERSEC", objUsuario.Sersec);
            cmd.Parameters.AddWithValue("@CODSUC", objUsuario.Codsuc);
            cmd.Parameters.AddWithValue("@USUING", objUsuario.Usuing);
            cmd.Parameters.AddWithValue("@USMAIL", objUsuario.Usmail);
            cmd.Parameters.AddWithValue("@CODFUN", objUsuario.Codfun);
            cmd.Parameters.AddWithValue("@LIMCRE", objUsuario.Limcre);
            cmd.Parameters.AddWithValue("@NUMPAG", objUsuario.Numpag);
            cmd.Parameters.AddWithValue("@PLAPAG", objUsuario.Plapag);
            cmd.Parameters.AddWithValue("@FOTUSU", objUsuario.Fotusu);
            cmd.Parameters.AddWithValue("@RESNOM", objUsuario.Resnom);
            cmd.Parameters.AddWithValue("@RESARE", objUsuario.Resare);
            cmd.Parameters.AddWithValue("@RESMAI", objUsuario.Resmai);
            cmd.Parameters.AddWithValue("@RESTEL", objUsuario.Restel);
            cmd.Parameters.AddWithValue("@TECNOM", objUsuario.Tecnom);
            cmd.Parameters.AddWithValue("@TECARE", objUsuario.Tecare);
            cmd.Parameters.AddWithValue("@TECMAI", objUsuario.Tecmai);
            cmd.Parameters.AddWithValue("@TECTEL", objUsuario.Tectel);
            cmd.Parameters.AddWithValue("@CODCLI", objUsuario.Codcli);
            cmd.Parameters.AddWithValue("@NOMCLI", objUsuario.Nomcli);
            cmd.Parameters.AddWithValue("@BANORD", objUsuario.Banord);
            cmd.Parameters.AddWithValue("@BANAUT", objUsuario.Banaut);
            cmd.Parameters.AddWithValue("@BANCOT", objUsuario.Bancot);
            cmd.Parameters.AddWithValue("@BANCOR", objUsuario.Bancor);
            cmd.Parameters.AddWithValue("@BANFAC", objUsuario.Banfac);
            cmd.Parameters.AddWithValue("@BANPED", objUsuario.Banped);
            cmd.Parameters.AddWithValue("@BANBAR", objUsuario.Banbar);
            cmd.Parameters.AddWithValue("@BANCHE", objUsuario.Banche);
            cmd.Parameters.AddWithValue("@BANDES", objUsuario.Bandes);
            cmd.Parameters.AddWithValue("@PORDES", objUsuario.Pordes);
            cmd.Parameters.AddWithValue("@ESTENC", objUsuario.Estenc);
            cmd.Parameters.AddWithValue("@CLAENC", objUsuario.Claenc);
            cmd.Parameters.AddWithValue("@CEDUSU", objUsuario.Cedusu);
            cmd.Parameters.AddWithValue("@ESTUSU", objUsuario.Estusu);
            cmd.Parameters.AddWithValue("@CODMER", objUsuario.Codmer);
            cmd.Parameters.AddWithValue("@ARCFIR", objUsuario.Arcfir);
            cmd.Parameters.AddWithValue("@CLAFIR", objUsuario.Clafir);
            cmd.Parameters.AddWithValue("@RUCEMP", objUsuario.Rucemp);
            cmd.Parameters.AddWithValue("@ULTENT", objUsuario.Ultent);
            cmd.Parameters.AddWithValue("@ULTSAL", objUsuario.Ultsal);
            cmd.Parameters.AddWithValue("@PCNOM", objUsuario.Pcnom);
            cmd.Parameters.AddWithValue("@PCIP", objUsuario.Pcip);
            cmd.Parameters.AddWithValue("@PCMAC", objUsuario.Pcmac);

            try
            {
                cmd.ExecuteNonQuery(); //se inserto correctamente
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;

        }

        public clsError f_Usuario_Eliminar(string s_codus1, string s_rucemp)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Delete");
            cmd.Parameters.AddWithValue("@CODEMP", s_rucemp);
            cmd.Parameters.AddWithValue("@CODUS1", s_codus1);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public cls_seg_Usuario f_dtUsuarioToObjUsuario(DataTable dt) //para pasar de dt a objUsuario los atributos de la base
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            objUsuario.dtUsuario = dt;
            objUsuario.Rucemp = dt.Rows[0].Field<string>("rucemp");
            objUsuario.Codus1 = dt.Rows[0].Field<string>("codus1");
            objUsuario.Nomusu = dt.Rows[0].Field<string>("nomusu");
            //objUsuario.Clausu = System.Text.Encoding.ASCII.GetString(dt.Rows[0].Field<byte[]>("clausu"));
            objUsuario.Codsuc = dt.Rows[0].Field<string>("codsuc");
            objUsuario.Sersec = dt.Rows[0].Field<string>("sersec");
            objUsuario.Usmail = dt.Rows[0].Field<string>("usmail");
            objUsuario.Fecexp = dt.Rows[0].Field<string>("fecexp");
            objUsuario.Estado = dt.Rows[0].Field<string>("estado");
            objUsuario.Tipacc = dt.Rows[0].Field<string>("tipacc");
            objUsuario.Estusu = dt.Rows[0].Field<string>("estusu");
            objUsuario.Ultent = dt.Rows[0].Field<string>("ultent");
            objUsuario.Ultsal = dt.Rows[0].Field<string>("ultsal");
            objUsuario.Pcnom = dt.Rows[0].Field<string>("pcnom");
            objUsuario.Pcip = dt.Rows[0].Field<string>("pcip");
            objUsuario.Pcmac = dt.Rows[0].Field<string>("pcmac");

            return objUsuario;
        }

        public clsError f_Usuario_Auditoria(string s_rucemp, string s_codus1, string s_usuing, DateTime? s_fecing, string s_codusu, DateTime? s_fecult)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE [seg_usuario] SET [usuing] = @Usuing, [fecing] = @Fecing, [codusu] = @Codusu, [fecult] = @Fecult WHERE [rucemp] = @Rucemp AND [codus1] = @Codus1 ", conn);
            cmd.Parameters.AddWithValue("@Usuing", s_usuing);
            cmd.Parameters.AddWithValue("@Fecing", s_fecing);
            cmd.Parameters.AddWithValue("@Codusu", s_codusu);
            cmd.Parameters.AddWithValue("@Fecult", s_fecult);
            cmd.Parameters.AddWithValue("@Rucemp", s_rucemp);
            cmd.Parameters.AddWithValue("@Codus1", s_codus1);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }


        //-----------------utilizados en este prj--------------------
        

        public cls_seg_Usuario f_Desencriptar_Base(cls_seg_Usuario objUsuario, string s_palcla)
        {
            objUsuario.Error = new clsError();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_V_USUARIO_BASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Codus1", objUsuario.Codus1);
            cmd.Parameters.AddWithValue("@PcIp", objUsuario.Pcip);
            cmd.Parameters.AddWithValue("@PalCla", s_palcla);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                objUsuario.Nomdbm = dt.Rows[0].Field<string>("nomdbm");
                objUsuario.Nombas = dt.Rows[0].Field<string>("nombas");
                objUsuario.Usubas = dt.Rows[0].Field<string>("usubas");
                objUsuario.Clabas = dt.Rows[0].Field<string>("clabas");
                objUsuario.Ipserv = dt.Rows[0].Field<string>("ipserv");
            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            conn.Close();


            return objUsuario;
        }

        public cls_seg_Usuario f_Usuario_EmpresaBase(string s_rucemp, string s_palcla) //f_Usuario_Desencriptar_Base pero esta usando SEG_V_EMPRESA_BASE por q no conocemos la MAC al seleccionar en grv
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_V_EMPRESA_BASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RucEmp", s_rucemp);
            cmd.Parameters.AddWithValue("@PalCla", s_palcla);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                //DataSet ds = new DataSet();
                //sda.Fill(ds);
                //objUsuario = f_dtUsuarioToObjUsuario(ds.Tables[0]);
                objUsuario.Nomdbm = dt.Rows[0].Field<string>("nomdbm");
                objUsuario.Nombas = dt.Rows[0].Field<string>("nombas");
                objUsuario.Usubas = dt.Rows[0].Field<string>("usubas");
                objUsuario.Clabas = dt.Rows[0].Field<string>("clabas");
                objUsuario.Ipserv = dt.Rows[0].Field<string>("ipserv");
            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objUsuario;
        }

        public clsError f_Usuario_EntSal(cls_seg_Usuario objUsuario, string s_entsal)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_V_USUARIO_ENTSAL]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RucEmp", objUsuario.Rucemp);
            cmd.Parameters.AddWithValue("@Codus1", objUsuario.Codus1);
            cmd.Parameters.AddWithValue("@EntSal", s_entsal);
            cmd.Parameters.AddWithValue("@PcNom", objUsuario.Pcnom);
            cmd.Parameters.AddWithValue("@PcIp", objUsuario.Pcip);
            cmd.Parameters.AddWithValue("@PcMac", objUsuario.Pcmac);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public string f_Base_InicialWeb()
        {
            clsError objError = new clsError();
            string ConnectionString = "";
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_V_BASE_INICIAL]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                string name = "SQLCA";
                string dataSource = "181.199.73.144";
                string initialCatalog = dt.Rows[0].Field<string>("Initial_Catalog");
                string userId = dt.Rows[0].Field<string>("UserID");
                string password = dt.Rows[0].Field<string>("PassW");

                ConnectionString = f_Conexion_Base(name, dataSource, initialCatalog, userId, password);
            }
            catch (Exception)
            {
                //objError = objError.f_ErrorControlado(ex);
                ConnectionString = "Error al conectar al servidor. Method - f_Base_InicialWeb(); Page - cls_seg_Usuario;";
            }
            conn.Close();
            return ConnectionString;
        }

        public string f_Conexion_Base(string name, string dataSource, string initialCatalog, string userId, string password)
        {
            Configuration connectionConfiguration = WebConfigurationManager.OpenWebConfiguration("~");
            connectionConfiguration.ConnectionStrings.ConnectionStrings["SQLCA"].Name = name;
            connectionConfiguration.ConnectionStrings.ConnectionStrings["SQLCA"].ConnectionString =
                "Data Source=" + dataSource + ";Initial Catalog=" + initialCatalog + ";User ID=" + userId + ";Password=" + password + "";
            connectionConfiguration.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("connectionStrings");
            connectionConfiguration.Save();
            return connectionConfiguration.ConnectionStrings.ConnectionStrings["SQLCA"].ConnectionString;
        }

        public cls_seg_Usuario f_Usuario_Buscar(string gs_Codemp, string gs_codus1Selected) //busca 1 solo cliente
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp.Trim());
            cmd.Parameters.AddWithValue("@CODUS1", gs_codus1Selected.Trim());
            cmd.Parameters.Add("@s_CODUS1", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_TIPUSU", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_NOMUSU", SqlDbType.VarChar, 30).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_FECING", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_FECEXP", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_TIPACC", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_GRUUSU", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_ASIUSU", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CODVEN", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CODUSU", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_FECULT", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CODALM", SqlDbType.VarChar, 6).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_ESTCIE", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_SERSEC", SqlDbType.VarChar, 6).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CODSUC", SqlDbType.VarChar, 3).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_USUING", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_USMAIL", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CODFUN", SqlDbType.VarChar, 6).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_LIMCRE", SqlDbType.Decimal, 20).Direction = ParameterDirection.Output;
            cmd.Parameters["@s_LIMCRE"].Precision = 20;
            cmd.Parameters["@s_LIMCRE"].Scale = 2;
            cmd.Parameters.Add("@s_NUMPAG", SqlDbType.Decimal, 20).Direction = ParameterDirection.Output;
            cmd.Parameters["@s_LIMCRE"].Precision = 20;
            cmd.Parameters["@s_LIMCRE"].Scale = 2;
            cmd.Parameters.Add("@s_PLAPAG", SqlDbType.Decimal, 20).Direction = ParameterDirection.Output;
            cmd.Parameters["@s_LIMCRE"].Precision = 20;
            cmd.Parameters["@s_LIMCRE"].Scale = 2;
            cmd.Parameters.Add("@s_FOTUSU", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_RESNOM", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_RESARE", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_RESMAI", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_RESTEL", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_TECNOM", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_TECARE", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_TECMAI", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_TECTEL", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CODCLI", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_NOMCLI", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_BANORD", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_BANAUT", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_BANCOT", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_BANCOR", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_BANFAC", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_BANPED", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_BANBAR", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_BANCHE", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_BANDES", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_PORDES", SqlDbType.Decimal, 20).Direction = ParameterDirection.Output;
            cmd.Parameters["@s_LIMCRE"].Precision = 20;
            cmd.Parameters["@s_LIMCRE"].Scale = 5;
            cmd.Parameters.Add("@s_ESTENC", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CLAENC", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CEDUSU", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_ESTUSU", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CODMER", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_ARCFIR", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CLAFIR", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_CLAUSU", SqlDbType.VarBinary, 128).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_RUCEMP", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_ULTENT", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_ULTSAL", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_PCNOM", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_PCIP", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_PCMAC", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;


            try
            {

                cmd.ExecuteNonQuery();
                objUsuario.Rucemp = cmd.Parameters["@s_RUCEMP"].Value.ToString().Trim();
                objUsuario.Codus1 = cmd.Parameters["@s_CODUS1"].Value.ToString().Trim();
                objUsuario.Nomcli = cmd.Parameters["@s_NOMCLI"].Value.ToString().Trim();
                objUsuario.Codcli = cmd.Parameters["@s_CODCLI"].Value.ToString().Trim();
                objUsuario.Tipusu = cmd.Parameters["@s_TIPUSU"].Value.ToString().Trim();
                objUsuario.Nomusu = cmd.Parameters["@s_NOMUSU"].Value.ToString().Trim();
                objUsuario.Fecing = cmd.Parameters["@s_FECING"].Value.ToString();
                objUsuario.Fecexp = cmd.Parameters["@s_FECEXP"].Value.ToString();
                objUsuario.Fecult = cmd.Parameters["@s_FECULT"].Value.ToString();
                objUsuario.Tipacc = cmd.Parameters["@s_TIPACC"].Value.ToString().Trim();
                objUsuario.Gruusu = cmd.Parameters["@s_GRUUSU"].Value.ToString().Trim();
                objUsuario.Asiusu = cmd.Parameters["@s_ASIUSU"].Value.ToString().Trim();
                objUsuario.Codven = cmd.Parameters["@s_CODVEN"].Value.ToString().Trim();
                objUsuario.Codusu = cmd.Parameters["@s_CODUSU"].Value.ToString().Trim();
                objUsuario.Codalm = cmd.Parameters["@s_CODALM"].Value.ToString().Trim();
                objUsuario.Estcie = cmd.Parameters["@s_ESTCIE"].Value.ToString().Trim();
                objUsuario.Sersec = cmd.Parameters["@s_SERSEC"].Value.ToString().Trim();
                objUsuario.Codsuc = cmd.Parameters["@s_CODSUC"].Value.ToString().Trim();
                objUsuario.Usuing = cmd.Parameters["@s_USUING"].Value.ToString().Trim();
                objUsuario.Usmail = cmd.Parameters["@s_USMAIL"].Value.ToString().Trim();
                objUsuario.Codfun = cmd.Parameters["@s_CODFUN"].Value.ToString().Trim();
                objUsuario.Limcre = !String.IsNullOrEmpty(cmd.Parameters["@s_LIMCRE"].Value.ToString().Trim()) ? Convert.ToDecimal(cmd.Parameters["@s_LIMCRE"].Value.ToString().Trim()) : 0;
                objUsuario.Numpag = !String.IsNullOrEmpty(cmd.Parameters["@s_NUMPAG"].Value.ToString().Trim()) ? Convert.ToDecimal(cmd.Parameters["@s_NUMPAG"].Value.ToString().Trim()) : 0;
                objUsuario.Plapag = !String.IsNullOrEmpty(cmd.Parameters["@s_PLAPAG"].Value.ToString().Trim()) ? Convert.ToDecimal(cmd.Parameters["@s_PLAPAG"].Value.ToString().Trim()) : 0;
                objUsuario.Resnom = cmd.Parameters["@s_RESNOM"].Value.ToString().Trim();
                objUsuario.Resare = cmd.Parameters["@s_RESARE"].Value.ToString().Trim();
                objUsuario.Resmai = cmd.Parameters["@s_RESMAI"].Value.ToString().Trim();
                objUsuario.Restel = cmd.Parameters["@s_RESTEL"].Value.ToString().Trim();
                objUsuario.Tecnom = cmd.Parameters["@s_TECNOM"].Value.ToString().Trim();
                objUsuario.Tecare = cmd.Parameters["@s_TECARE"].Value.ToString().Trim();
                objUsuario.Tectel = cmd.Parameters["@s_TECTEL"].Value.ToString().Trim();
                objUsuario.Banord = cmd.Parameters["@s_BANORD"].Value.ToString().Trim();
                objUsuario.Banaut = cmd.Parameters["@s_BANAUT"].Value.ToString().Trim();
                objUsuario.Banfac = cmd.Parameters["@s_BANFAC"].Value.ToString().Trim();
                objUsuario.Banped = cmd.Parameters["@s_BANPED"].Value.ToString().Trim();
                objUsuario.Bancor = cmd.Parameters["@s_BANCOR"].Value.ToString().Trim();
                objUsuario.Banbar = cmd.Parameters["@s_BANBAR"].Value.ToString().Trim();
                objUsuario.Banche = cmd.Parameters["@s_BANCHE"].Value.ToString().Trim();
                objUsuario.Bandes = cmd.Parameters["@s_BANDES"].Value.ToString().Trim();
                objUsuario.Pordes = !String.IsNullOrEmpty(cmd.Parameters["@s_PORDES"].Value.ToString().Trim()) ? Convert.ToDecimal(cmd.Parameters["@s_PORDES"].Value.ToString().Trim()) : 0;
                objUsuario.Estenc = cmd.Parameters["@s_ESTENC"].Value.ToString().Trim();
                objUsuario.Claenc = cmd.Parameters["@s_CLAENC"].Value.ToString().Trim();
                objUsuario.Cedusu = cmd.Parameters["@s_CEDUSU"].Value.ToString().Trim();
                objUsuario.Estusu = cmd.Parameters["@s_ESTUSU"].Value.ToString().Trim();
                objUsuario.Codmer = cmd.Parameters["@s_CODMER"].Value.ToString().Trim();
                objUsuario.Arcfir = cmd.Parameters["@s_ARCFIR"].Value.ToString().Trim();
                objUsuario.Clafir = cmd.Parameters["@s_CLAFIR"].Value.ToString().Trim();
                objUsuario.Clausu = cmd.Parameters["@s_CLAUSU"].Value.ToString().Trim();
                objUsuario.Rucemp = cmd.Parameters["@s_RUCEMP"].Value.ToString().Trim();
                objUsuario.Ultent = cmd.Parameters["@s_ULTENT"].Value.ToString();
                objUsuario.Ultsal = cmd.Parameters["@s_ULTSAL"].Value.ToString();
                objUsuario.Pcnom = cmd.Parameters["@s_PCNOM"].Value.ToString().Trim();
                objUsuario.Pcip = cmd.Parameters["@s_PCIP"].Value.ToString().Trim();
                objUsuario.Pcmac = cmd.Parameters["@s_PCMAC"].Value.ToString().Trim();

            }

            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objUsuario;
        }

        public cls_seg_Usuario f_Usuario_Principio(string gs_Codemp)
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_S_USUARIO_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Action", "PRIMERO");
            //cmd.Parameters.AddWithValue("@CODUS1", gs_Codus1);
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                objUsuario = f_dtUsuarioToObjUsuario_A(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objUsuario;
        }
        public cls_seg_Usuario f_Usuario_Siguiente(string gs_Codemp, string gs_Siguiente)
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_S_USUARIO_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Action", "SIGUIENTE");
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            //cmd.Parameters.AddWithValue("@CODUS1", gs_Codus1);


            cmd.Parameters.AddWithValue("@NOMUSU", gs_Siguiente);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objUsuario = f_dtUsuarioToObjUsuario_A(ds.Tables[0]);

                    conn.Close();
                }
                else
                {
                    conn.Close();
                    objUsuario = f_Usuario_Final(gs_Codemp);

                }
            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            return objUsuario;

        }
        public cls_seg_Usuario f_Usuario_Final(string gs_Codemp)
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_S_USUARIO_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "ULTIMO");
            //cmd.Parameters.AddWithValue("@CODUS1", gs_Codus1);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                objUsuario = f_dtUsuarioToObjUsuario_A(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objUsuario;
        }
        public cls_seg_Usuario f_Usuario_Atras(string gs_Codemp, string gs_Siguiente)
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_S_USUARIO_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "ANTERIOR");
            //cmd.Parameters.AddWithValue("@CODUS1", gs_Codus1);


            cmd.Parameters.AddWithValue("@NOMUSU", gs_Siguiente);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objUsuario = f_dtUsuarioToObjUsuario_A(ds.Tables[0]);

                    conn.Close();
                }
                else
                {
                    conn.Close();
                    objUsuario = f_Usuario_Principio(gs_Codemp);

                }
            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            return objUsuario;
        }
        public cls_seg_Usuario f_dtUsuarioToObjUsuario_A(DataTable dt) //para pasar de dt a objFactura los atributos de la base
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            objUsuario.dtUsuario = dt;
            objUsuario.Codus1 = dt.Rows[0].Field<string>("codus1");
            objUsuario.Nomcli = dt.Rows[0].Field<string>("nomcli");
            objUsuario.Codcli = dt.Rows[0].Field<string>("codcli");
            objUsuario.Tipusu = dt.Rows[0].Field<string>("tipusu");
            objUsuario.Nomusu = dt.Rows[0].Field<string>("nomusu");
            objUsuario.Fecing = Convert.ToString(dt.Rows[0].Field<DateTime>("fecing"));
            objUsuario.Fecexp = Convert.ToString(dt.Rows[0].Field<DateTime>("fecexp"));
            objUsuario.Fecult = Convert.ToString(dt.Rows[0].Field<DateTime>("fecult"));
            objUsuario.Tipacc = dt.Rows[0].Field<string>("tipacc");
            objUsuario.Gruusu = dt.Rows[0].Field<string>("gruusu");
            objUsuario.Asiusu = dt.Rows[0].Field<string>("asiusu");
            objUsuario.Codven = dt.Rows[0].Field<string>("codven");
            objUsuario.Codusu = dt.Rows[0].Field<string>("codusu");
            objUsuario.Codalm = dt.Rows[0].Field<string>("codalm");
            objUsuario.Estcie = dt.Rows[0].Field<string>("estcie");
            objUsuario.Sersec = dt.Rows[0].Field<string>("sersec");
            objUsuario.Codsuc = dt.Rows[0].Field<string>("codsuc");
            objUsuario.Usuing = dt.Rows[0].Field<string>("usuing");
            objUsuario.Usmail = dt.Rows[0].Field<string>("usmail");
            objUsuario.Codfun = dt.Rows[0].Field<string>("codfun");

            objUsuario.Limcre = dt.Rows[0]["limcre"] == DBNull.Value ? 0 : dt.Rows[0].Field<Decimal>("limcre");
            objUsuario.Numpag = dt.Rows[0]["numpag"] == DBNull.Value ? 0 : dt.Rows[0].Field<Decimal>("numpag");
            objUsuario.Plapag = dt.Rows[0]["plapag"] == DBNull.Value ? 0 : dt.Rows[0].Field<Decimal>("plapag");
            objUsuario.Resnom = dt.Rows[0].Field<string>("resnom");
            objUsuario.Resare = dt.Rows[0].Field<string>("resare");
            objUsuario.Resmai = dt.Rows[0].Field<string>("resmai");
            objUsuario.Restel = dt.Rows[0].Field<string>("restel");
            objUsuario.Tecnom = dt.Rows[0].Field<string>("tecnom");
            objUsuario.Tecare = dt.Rows[0].Field<string>("tecare");
            objUsuario.Tectel = dt.Rows[0].Field<string>("tectel");
            objUsuario.Banord = dt.Rows[0].Field<string>("banord");
            objUsuario.Banaut = dt.Rows[0].Field<string>("banaut");
            objUsuario.Banfac = dt.Rows[0].Field<string>("banfac");
            objUsuario.Banped = dt.Rows[0].Field<string>("banped");
            objUsuario.Bancor = dt.Rows[0].Field<string>("bancor");
            objUsuario.Banbar = dt.Rows[0].Field<string>("banbar");
            objUsuario.Banche = dt.Rows[0].Field<string>("banche");
            objUsuario.Bandes = dt.Rows[0].Field<string>("bandes");
            objUsuario.Pordes = dt.Rows[0]["pordes"] == DBNull.Value ? 0 : dt.Rows[0].Field<Decimal>("pordes");
            objUsuario.Estenc = dt.Rows[0].Field<string>("estenc");
            objUsuario.Claenc = dt.Rows[0].Field<string>("claenc");
            objUsuario.Cedusu = dt.Rows[0].Field<string>("cedusu");
            objUsuario.Estusu = dt.Rows[0].Field<string>("estusu");
            objUsuario.Codmer = dt.Rows[0].Field<string>("codmer");
            objUsuario.Arcfir = dt.Rows[0].Field<string>("arcfir");
            objUsuario.Clafir = dt.Rows[0].Field<string>("clafir");
            objUsuario.Clausu = "";
            objUsuario.Rucemp = dt.Rows[0].Field<string>("rucemp");

            DateTime? s_ultent = dt.Rows[0].Field<DateTime?>("ultent");
            objUsuario.Ultent = Convert.ToString(s_ultent);



            DateTime? s_ultsal = dt.Rows[0].Field<DateTime?>("ultsal");
            objUsuario.Ultsal = Convert.ToString(s_ultsal);


            objUsuario.Pcnom = dt.Rows[0].Field<string>("pcnom");
            objUsuario.Pcip = dt.Rows[0].Field<string>("pcip");
            objUsuario.Pcmac = dt.Rows[0].Field<string>("pcmac");



            return objUsuario;
        }
        public DataTable f_dtUsuarioTableType() //para generar un dataTable que tenga la estructura del TableType en SQL
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("codcli", typeof(string)));
            dt.Columns.Add(new DataColumn("tipusu", typeof(string)));
            dt.Columns.Add(new DataColumn("nomusu", typeof(string)));
            dt.Columns.Add(new DataColumn("fecing", typeof(string)));
            dt.Columns.Add(new DataColumn("fecexp", typeof(string)));
            dt.Columns.Add(new DataColumn("fecult", typeof(string)));
            dt.Columns.Add(new DataColumn("tipacc", typeof(string)));
            dt.Columns.Add(new DataColumn("gruusu", typeof(string)));
            dt.Columns.Add(new DataColumn("tipacc", typeof(string)));
            dt.Columns.Add(new DataColumn("codven", typeof(string)));
            dt.Columns.Add(new DataColumn("codusu", typeof(string)));
            dt.Columns.Add(new DataColumn("codalm", typeof(string)));
            dt.Columns.Add(new DataColumn("estcie", typeof(string)));
            dt.Columns.Add(new DataColumn("sersec", typeof(string)));
            dt.Columns.Add(new DataColumn("codsuc", typeof(string)));
            dt.Columns.Add(new DataColumn("usuing", typeof(string)));
            dt.Columns.Add(new DataColumn("usmail", typeof(string)));
            dt.Columns.Add(new DataColumn("codfun", typeof(string)));
            dt.Columns.Add(new DataColumn("limcre", typeof(string)));
            dt.Columns.Add(new DataColumn("numpag", typeof(string)));
            dt.Columns.Add(new DataColumn("plapag", typeof(string)));
            dt.Columns.Add(new DataColumn("resnom", typeof(string)));
            dt.Columns.Add(new DataColumn("resare", typeof(string)));
            dt.Columns.Add(new DataColumn("resmai", typeof(string)));
            dt.Columns.Add(new DataColumn("restel", typeof(string)));
            dt.Columns.Add(new DataColumn("tecnom", typeof(string)));
            dt.Columns.Add(new DataColumn("tecare", typeof(string)));
            dt.Columns.Add(new DataColumn("tectel", typeof(string)));
            dt.Columns.Add(new DataColumn("banord", typeof(string)));
            dt.Columns.Add(new DataColumn("banaut", typeof(string)));
            dt.Columns.Add(new DataColumn("banfac", typeof(string)));
            dt.Columns.Add(new DataColumn("banped", typeof(string)));
            dt.Columns.Add(new DataColumn("bancor", typeof(string)));
            dt.Columns.Add(new DataColumn("banbar", typeof(string)));
            dt.Columns.Add(new DataColumn("banche", typeof(string)));
            dt.Columns.Add(new DataColumn("bandes", typeof(string)));
            dt.Columns.Add(new DataColumn("pordes", typeof(decimal)));
            dt.Columns.Add(new DataColumn("estenc", typeof(string)));
            dt.Columns.Add(new DataColumn("claenc", typeof(string)));
            dt.Columns.Add(new DataColumn("cedusu", typeof(string)));
            dt.Columns.Add(new DataColumn("estusu", typeof(string)));
            dt.Columns.Add(new DataColumn("codmer", typeof(string)));
            dt.Columns.Add(new DataColumn("arcfir", typeof(string)));
            dt.Columns.Add(new DataColumn("clafir", typeof(string)));
            dt.Columns.Add(new DataColumn("clausu", typeof(string)));
            dt.Columns.Add(new DataColumn("rucemp", typeof(string)));
            dt.Columns.Add(new DataColumn("ultent", typeof(string)));
            dt.Columns.Add(new DataColumn("ultsal", typeof(string)));
            dt.Columns.Add(new DataColumn("pcnom", typeof(string)));
            dt.Columns.Add(new DataColumn("pcip", typeof(string)));
            dt.Columns.Add(new DataColumn("pcmac", typeof(string)));

            return dt;
        }

        public clsError f_Usuario_RegistroIP(cls_seg_Usuario objUsuario) //actualizar la IP cuando ingrese de nueva maquina
        {
            clsError objError = new clsError();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_IP]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RUCEMP", objUsuario.Rucemp);
            cmd.Parameters.AddWithValue("@CODUS1", objUsuario.Codus1);
            cmd.Parameters.AddWithValue("@PCIP", objUsuario.Pcip);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
    }
}