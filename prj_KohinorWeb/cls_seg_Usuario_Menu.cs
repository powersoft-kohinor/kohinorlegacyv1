﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_seg_Usuario_Menu
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public string Codemp { get; set; }
        public string Codus1 { get; set; }
        public int Codmod { get; set; }
        public string Niv001 { get; set; }
        public string Niv002 { get; set; }
        public string Banins { get; set; }
        public string Baneli { get; set; }
        public string Bancan { get; set; }
        public string Banbus { get; set; }
        public string Banpre { get; set; }
        public string Banimp { get; set; }
        public string Banotr { get; set; }
        public string Bangra { get; set; }
        public string Menvis { get; set; }
        public string Menhab { get; set; }
        //public string Menbar { get; set; }
        public DataTable dtUsuarioMenu { get; set; }
        public clsError Error { get; set; }

        public cls_seg_Usuario_Menu f_UsuarioMenu_Buscar(string gs_Codemp, string s_codmod, string s_codus1, string s_niv001, string s_niv002)
        {
            cls_seg_Usuario_Menu objUsuarioMenu = new cls_seg_Usuario_Menu();
            objUsuarioMenu.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_MENU]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@CODMOD", s_codmod);
            cmd.Parameters.AddWithValue("@CODUS1", s_codus1);
            cmd.Parameters.AddWithValue("@NIV001", s_niv001);
            cmd.Parameters.AddWithValue("@NIV002", s_niv002);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objUsuarioMenu.dtUsuarioMenu = dt;
            }
            catch (Exception ex)
            {
                objUsuarioMenu.Error = objUsuarioMenu.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objUsuarioMenu;
        }

        public clsError f_UsuarioMenu_Actualizar(cls_seg_Usuario_Menu objUsuarioMenu, string s_usuing, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_MENU]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@CODEMP", objUsuarioMenu.Codemp);
            cmd.Parameters.AddWithValue("@CODUS1", objUsuarioMenu.Codus1);
            cmd.Parameters.AddWithValue("@CODMOD", objUsuarioMenu.Codmod);
            cmd.Parameters.AddWithValue("@NIV001", objUsuarioMenu.Niv001);
            cmd.Parameters.AddWithValue("@NIV002", objUsuarioMenu.Niv002);
            cmd.Parameters.AddWithValue("@BANINS", objUsuarioMenu.Banins);
            cmd.Parameters.AddWithValue("@BANCAN", objUsuarioMenu.Bancan);
            cmd.Parameters.AddWithValue("@BANELI", objUsuarioMenu.Baneli);
            cmd.Parameters.AddWithValue("@BANBUS", objUsuarioMenu.Banbus);
            cmd.Parameters.AddWithValue("@BANPRE", objUsuarioMenu.Banpre);
            cmd.Parameters.AddWithValue("@BANIMP", objUsuarioMenu.Banimp);
            cmd.Parameters.AddWithValue("@BANOTR", objUsuarioMenu.Banotr);
            cmd.Parameters.AddWithValue("@BANGRA", objUsuarioMenu.Bangra);
            cmd.Parameters.AddWithValue("@MENVIS", objUsuarioMenu.Menvis);
            cmd.Parameters.AddWithValue("@MENHAB", objUsuarioMenu.Menhab);
            cmd.Parameters.AddWithValue("@USUING", s_usuing);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

    }
}