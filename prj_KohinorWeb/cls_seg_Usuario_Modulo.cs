﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_seg_Usuario_Modulo
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public string Codemp { get; set; }
        public string Codus1 { get; set; }
        public int Codmod { get; set; }
        public string Estmod { get; set; }
        public clsError Error { get; set; }

        public cls_seg_Usuario_Modulo f_UsuarioModulo_Buscar(string gs_Codemp, string s_codus1, string s_codmod)
        {
            cls_seg_Usuario_Modulo objUsuarioModulo = new cls_seg_Usuario_Modulo();
            objUsuarioModulo.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_MODULO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@CODMOD", s_codmod);
            cmd.Parameters.AddWithValue("@CODUS1", s_codus1);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        objUsuarioModulo.Codus1 = dt.Rows[0].Field<string>("codus1");
                    }
                }
            }
            catch (Exception ex)
            {
                objUsuarioModulo.Error = objUsuarioModulo.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objUsuarioModulo;
        }

        public clsError f_UsuarioModulo_Actualizar(cls_seg_Usuario_Modulo objUsuarioModulo, string s_usuing, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_MODULO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@CODEMP", objUsuarioModulo.Codemp);
            cmd.Parameters.AddWithValue("@CODUS1", objUsuarioModulo.Codus1);
            cmd.Parameters.AddWithValue("@CODMOD", objUsuarioModulo.Codmod);
            cmd.Parameters.AddWithValue("@ESTMOD", objUsuarioModulo.Estmod);
            cmd.Parameters.AddWithValue("@USUING", s_usuing);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }



    }
}