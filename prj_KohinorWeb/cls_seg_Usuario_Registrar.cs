﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_seg_Usuario_Registrar
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ConnectionString);
        public string Rucemp { get; set; }
        public string Codus1 { get; set; }
        public string Nomusu { get; set; }
        public string Usmail { get; set; }
        public string Clausu { get; set; }
        public string Pcnom { get; set; }
        public string Pcip { get; set; }
        public string Pcmac { get; set; }
        public string Estado { get; set; }
        public string Nomdbm { get; set; }
        public string Nombas { get; set; }
        public string Usubas { get; set; }
        public string Clabas { get; set; }
        public string Ipserv { get; set; }

        public int Existe { get; set; } //0=NO EXISTE;1=CODUS1 EXISTE;2=EMAIL EXISTE

        public clsError Error { get; set; }

        public cls_seg_Usuario_Registrar f_Usuario_Existe(cls_seg_Usuario_Registrar objUsuarioRegistrar)
        {
            string checkUser = @"SELECT
                                CASE 
	                                WHEN EXISTS(select codus1 from [dbo].[seg_usuario] WHERE rucemp= @RUCEMP AND codus1= @CODUS1) THEN 1 
	                                WHEN EXISTS(select codus1 from [dbo].[seg_usuario] WHERE rucemp= @RUCEMP AND usmail= @USMAIL) THEN 2 
	                                ELSE 0
                                END";
            objUsuarioRegistrar.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand(checkUser, conn);
            cmd.Parameters.AddWithValue("@RUCEMP", objUsuarioRegistrar.Rucemp);
            cmd.Parameters.AddWithValue("@CODUS1", objUsuarioRegistrar.Codus1);
            cmd.Parameters.AddWithValue("@USMAIL", objUsuarioRegistrar.Usmail);

            try
            {
                objUsuarioRegistrar.Existe = (Int32)cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                objUsuarioRegistrar.Error = objUsuarioRegistrar.Error.f_ErrorControlado(ex);

            }
            conn.Close();
            return objUsuarioRegistrar;
        }
        public clsError f_Usuario_Actualizar(cls_seg_Usuario_Registrar objUsuarioRegistrar, string s_usuing, string s_codusu, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@RUCEMP", objUsuarioRegistrar.Rucemp);
            cmd.Parameters.AddWithValue("@CODUS1", objUsuarioRegistrar.Codus1);
            cmd.Parameters.AddWithValue("@NOMUSU", objUsuarioRegistrar.Nomusu);
            cmd.Parameters.AddWithValue("@USMAIL", objUsuarioRegistrar.Usmail);
            cmd.Parameters.AddWithValue("@PCNOM", objUsuarioRegistrar.Pcnom);
            cmd.Parameters.AddWithValue("@PCIP", objUsuarioRegistrar.Pcip);
            cmd.Parameters.AddWithValue("@PCMAC", objUsuarioRegistrar.Pcmac);
            cmd.Parameters.AddWithValue("@ESTADO", objUsuarioRegistrar.Estado);
            cmd.Parameters.AddWithValue("@USUING", s_usuing);
            cmd.Parameters.AddWithValue("@CODUSU", s_codusu);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
        public clsError f_Usuario_Clave(cls_seg_Usuario_Registrar objUsuarioRegistrar)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_CLAVE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CodUs1", objUsuarioRegistrar.Codus1);
            cmd.Parameters.AddWithValue("@ClaUsu", objUsuarioRegistrar.Clausu);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public clsError f_Usuario_Email(string s_rucemp, string s_codus1)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_MAIL]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CodUs1", s_codus1);
            cmd.Parameters.AddWithValue("@Rucemp", s_rucemp);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public cls_seg_Usuario_Registrar f_Usuario_DatosConexion(cls_seg_Usuario_Registrar objUsuarioRegistrar, string s_palcla)
        {
            objUsuarioRegistrar.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_V_EMPRESA_BASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@RucEmp", objUsuarioRegistrar.Rucemp);
            cmd.Parameters.AddWithValue("@PalCla", s_palcla);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                objUsuarioRegistrar.Nomdbm = dt.Rows[0].Field<string>("nomdbm");
                objUsuarioRegistrar.Nombas = dt.Rows[0].Field<string>("nombas");
                objUsuarioRegistrar.Usubas = dt.Rows[0].Field<string>("usubas");
                objUsuarioRegistrar.Clabas = dt.Rows[0].Field<string>("clabas");
                objUsuarioRegistrar.Ipserv = dt.Rows[0].Field<string>("ipserv");
            }
            catch (Exception ex)
            {
                objUsuarioRegistrar.Error = objUsuarioRegistrar.Error.f_ErrorControlado(ex);
            }
            conn.Close();

            return objUsuarioRegistrar;
        }

        public clsError f_Usuario_Encriptar_Base(cls_seg_Usuario_Registrar objUsuarioRegistrar, string s_palcla)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_BASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@RucEmp", objUsuarioRegistrar.Rucemp);
            cmd.Parameters.AddWithValue("@CodUs1", objUsuarioRegistrar.Codus1);
            cmd.Parameters.AddWithValue("@PalCla", s_palcla);
            cmd.Parameters.AddWithValue("@NomDbm", objUsuarioRegistrar.Nomdbm);
            cmd.Parameters.AddWithValue("@NomBas", objUsuarioRegistrar.Nombas);
            cmd.Parameters.AddWithValue("@UsuBas", objUsuarioRegistrar.Usubas);
            cmd.Parameters.AddWithValue("@ClaBas", objUsuarioRegistrar.Clabas);
            cmd.Parameters.AddWithValue("@IpServ", objUsuarioRegistrar.Ipserv);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
    }
}