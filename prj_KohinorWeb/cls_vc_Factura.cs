﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_vc_Factura
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public static string fecMax { get; set; }
        public static string fecMin { get; set; }

        public string Codemp { get; set; }
        public string Codcli { get; set; } 
        public string Codusu { get; set; } 
        public string Clicxc { get; set; } 
        public string Nomcxc { get; set; } 
        public string Nomcli { get; set; } 
        public string Sersec { get; set; } 
        public string Dircli { get; set; } 
        public string Lispre { get; set; }
        public string Observ { get; set; } 
        public string Ttardes { get; set; }
        public string Ntardes { get; set; }
        public string Numfac { get; set; } 
        public DateTime Fecfac { get; set; } 
        public string Estado { get; set; } 
        public string Codalm { get; set; } 
        public string Codven { get; set; }
        public string Poriva { get; set; }
        public string Pordes { get; set; }

        public Decimal Totnet { get; set; }
        public Decimal Totbas { get; set; }
        public Decimal Totdes { get; set; }
        public Decimal Totiva { get; set; }
        public Decimal Totfac { get; set; }
        public DataTable dtFactura { get; set; }
        public cls_vc_Factura_Ren FacturaRen { get; set; }

        public string ClicxcModal { get; set; } //para enviar el codcli al modalNuevo
        public string Email { get; set; }
        public string Telcli { get; set; }
        public clsError Error { get; set; }


        public void f_FacturaFecMinFecMax()
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            conn.Open();
            string checkfecmax = "select MAX(fecfac) from vc_factura";
            SqlCommand fecCom = new SqlCommand(checkfecmax, conn);
            string s_fecmax = fecCom.ExecuteScalar().ToString().Trim();
            fecMax = s_fecmax;

            string checkfecmin = "select MIN(fecfac) from vc_factura";
            SqlCommand fecCom2 = new SqlCommand(checkfecmin, conn);
            string s_fecmin = fecCom2.ExecuteScalar().ToString().Trim();
            fecMin = s_fecmin;
            conn.Close();
        }
        public cls_vc_Factura f_Factura_Buscar(string gs_Codemp, string gs_numfacSelected)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            objFactura.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[VC_M_FACTURA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@numfac", gs_numfacSelected);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = f_dtFacturaTableType();
                sda.Fill(dt);
                objFactura = f_dtFacturaToObjFactura(dt);

            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objFactura;
        }
        public clsError f_Factura_Actualizar(cls_vc_Factura objFactura, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[VC_M_FACTURA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", s_Action);
            var param = new SqlParameter("@FacturaTableType", objFactura.dtFactura);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
        public clsError f_Factura_Eliminar(string gs_numfacSelected, string gs_Codemp)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[VC_M_FACTURA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Delete");
            cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
            cmd.Parameters.AddWithValue("@numfac", gs_numfacSelected);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        //public cls_vc_Factura f_CalcularSecuencia(string gs_Codemp, string gs_Sersec)
        //{
        //    cls_vc_Factura objFactura = new cls_vc_Factura();
        //    DataTable dtNumFac = new DataTable();
        //    objFactura.Error = new clsError();
        //    conn.Open();

        //    SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_SEC]", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
        //    cmd.Parameters.AddWithValue("@codsec", "VC_FAC");
        //    cmd.Parameters.AddWithValue("@sersec", gs_Sersec); //del usuario en Login
        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        //        adapter.Fill(dtNumFac);
        //        string s_numfac = dtNumFac.Rows[0].Field<string>("numfac");
        //        objFactura.Numfac = s_numfac;
        //        objFactura.Fecfac = DateTime.Now;
        //    }
        //    catch (Exception ex)
        //    {
        //        objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
        //    }            

        //    conn.Close();        
        //    return objFactura;
        //}

        public cls_vc_Factura f_Factura_Buscar_Cliente(string gs_Codemp, string s_codcli)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_CLIENTE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@CLICXC", s_codcli);
            cmd.Parameters.AddWithValue("@NUMTAR", "");

            cmd.Parameters.Add("@s_Clicxc", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Nomcxc", SqlDbType.Char, 100).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dircli", SqlDbType.Char, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Lispre", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Observ", SqlDbType.Char, 160).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Email", SqlDbType.Char, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Telcli", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            try
            {
                cmd.ExecuteNonQuery();

                string clicxc = cmd.Parameters["@s_Clicxc"].Value.ToString().Trim();
                if (clicxc.Equals(""))
                {
                    objFactura.Clicxc = "noEncontro";
                }
                else
                {
                    objFactura.Clicxc = clicxc;
                }

                objFactura.Nomcxc = cmd.Parameters["@s_Nomcxc"].Value.ToString().Trim();
                objFactura.Dircli = cmd.Parameters["@s_Dircli"].Value.ToString().Trim();
                objFactura.Lispre = cmd.Parameters["@s_Lispre"].Value.ToString().Trim();
                objFactura.Observ = cmd.Parameters["@s_Observ"].Value.ToString().Trim();
                objFactura.Email = cmd.Parameters["@s_Email"].Value.ToString().Trim();
                objFactura.Telcli = cmd.Parameters["@s_Telcli"].Value.ToString().Trim();
                objFactura.ClicxcModal = s_codcli;
                objFactura.Ntardes = "";
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objFactura;
        }
        public cls_vc_Factura f_Factura_Principio(string gs_Codemp, string gs_Sersec)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "PRIMERO");
            cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                objFactura = f_dtFacturaToObjFactura(ds.Tables[0]);
                objFactura.FacturaRen = new cls_vc_Factura_Ren();
                objFactura.FacturaRen = objFactura.FacturaRen.f_dtFacturaRenToObjFacturaRen(ds.Tables[1]);
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objFactura;
        }
        public cls_vc_Factura f_Factura_Siguiente(string gs_Codemp, string gs_Sersec, string gs_Siguiente)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "SIGUIENTE");
            cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);
            cmd.Parameters.AddWithValue("@NUMFAC", gs_Siguiente);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objFactura = f_dtFacturaToObjFactura(ds.Tables[0]);
                    objFactura.FacturaRen = new cls_vc_Factura_Ren();
                    objFactura.FacturaRen = objFactura.FacturaRen.f_dtFacturaRenToObjFacturaRen(ds.Tables[1]);
                    conn.Close();
                }                    
                else
                {
                    conn.Close();
                    objFactura = f_Factura_Final(gs_Codemp, gs_Sersec);
                    
                }
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }            
            return objFactura;

        }
        public cls_vc_Factura f_Factura_Final(string gs_Codemp, string gs_Sersec)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "ULTIMO");
            cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                objFactura = f_dtFacturaToObjFactura(ds.Tables[0]);
                objFactura.FacturaRen = new cls_vc_Factura_Ren();
                objFactura.FacturaRen = objFactura.FacturaRen.f_dtFacturaRenToObjFacturaRen(ds.Tables[1]);
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objFactura;
        }
        public cls_vc_Factura f_Factura_Atras(string gs_Codemp, string gs_Sersec, string gs_Siguiente)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            objFactura.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_ORDEN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@Action", "ANTERIOR");
            cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);
            cmd.Parameters.AddWithValue("@NUMFAC", gs_Siguiente);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objFactura = f_dtFacturaToObjFactura(ds.Tables[0]);
                    objFactura.FacturaRen = new cls_vc_Factura_Ren();
                    objFactura.FacturaRen = objFactura.FacturaRen.f_dtFacturaRenToObjFacturaRen(ds.Tables[1]);
                    conn.Close();
                }
                else
                {
                    conn.Close();
                    objFactura = f_Factura_Principio(gs_Codemp, gs_Sersec);

                }
            }
            catch (Exception ex)
            {
                objFactura.Error = objFactura.Error.f_ErrorControlado(ex);
            }
            return objFactura;
        }
        public cls_vc_Factura f_dtFacturaToObjFactura(DataTable dt) //para pasar de dt a objFactura los atributos de la base
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            objFactura.Error = new clsError();
            objFactura.dtFactura = dt;
            if (objFactura.dtFactura.Rows.Count > 0)
            {
                objFactura.Numfac = dt.Rows[0].Field<string>("numfac");
                objFactura.Nomcxc = dt.Rows[0].Field<string>("nomcxc");
                objFactura.Clicxc = dt.Rows[0].Field<string>("clicxc");
                objFactura.Dircli = dt.Rows[0].Field<string>("dircli");
                objFactura.Lispre = dt.Rows[0].Field<string>("lispre");
                objFactura.Observ = dt.Rows[0].Field<string>("observ");
                objFactura.Fecfac = dt.Rows[0].Field<DateTime>("fecfac");
                objFactura.Ntardes = dt.Rows[0].Field<string>("ntardes");
                objFactura.Estado = dt.Rows[0].Field<string>("estado");
                objFactura.Codalm = dt.Rows[0].Field<string>("codalm");
                objFactura.Totnet = dt.Rows[0].Field<decimal>("totnet");
                objFactura.Totbas = dt.Rows[0].Field<decimal>("totbas");
                objFactura.Totdes = dt.Rows[0].Field<decimal>("totdes");
                objFactura.Totiva = dt.Rows[0].Field<decimal>("totiva");
                objFactura.Totfac = dt.Rows[0].Field<decimal>("totfac");
                objFactura.Ttardes = dt.Rows[0].Field<string>("ttardes");
                objFactura.Codven = dt.Rows[0].Field<string>("codven");
            }                     

            return objFactura;
        }
        public DataTable f_dtFacturaTableType() //para generar un dataTable que tenga la estructura del TableType en SQL
        {
            DataTable dt = new DataTable();            
            dt.Columns.Add(new DataColumn("codemp", typeof(string)));
            dt.Columns.Add(new DataColumn("numfac", typeof(string)));
            dt.Columns.Add(new DataColumn("fecfac", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("estado", typeof(string)));
            dt.Columns.Add(new DataColumn("clicxc", typeof(string)));
            dt.Columns.Add(new DataColumn("nomcxc", typeof(string)));
            dt.Columns.Add(new DataColumn("codcli", typeof(string)));
            dt.Columns.Add(new DataColumn("nomcli", typeof(string)));
            dt.Columns.Add(new DataColumn("sersec", typeof(string)));
            dt.Columns.Add(new DataColumn("rucced", typeof(string)));
            dt.Columns.Add(new DataColumn("dircli", typeof(string)));
            dt.Columns.Add(new DataColumn("lispre", typeof(string)));
            dt.Columns.Add(new DataColumn("codven", typeof(string)));
            dt.Columns.Add(new DataColumn("codalm", typeof(string)));
            dt.Columns.Add(new DataColumn("observ", typeof(string)));
            dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
            dt.Columns.Add(new DataColumn("pordes", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totnet", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totdes", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totbas", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totiva", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totfac", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totcxc", typeof(decimal)));
            dt.Columns.Add(new DataColumn("usuing", typeof(string)));
            dt.Columns.Add(new DataColumn("codusu", typeof(string)));

            return dt;
        }
    }
}