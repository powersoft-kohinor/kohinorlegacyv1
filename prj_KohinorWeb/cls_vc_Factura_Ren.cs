﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
    public class cls_vc_Factura_Ren
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public string Codemp { get; set; }
        public string Numfac { get; set; }
        public int Numren { get; set; }
        public int Numite { get; set; }
        public string Codart { get; set; }
        public string Nomart { get; set; }
        public string Coduni { get; set; }
        public Decimal Cantid { get; set; }
        public Decimal Preuni { get; set; }
        public Decimal Desren { get; set; }
        public Decimal Totren { get; set; }
        public string Ubifis { get; set; }
        public string Codiva { get; set; }
        public string Poriva { get; set; }
        public string Codmon { get; set; }
        public Decimal Valcot { get; set; }
        public Decimal Totext { get; set; }
        public string Tiporg { get; set; }
        public string Tiptra { get; set; }
        public string Numtra { get; set; }
        public DataTable dtFacturaRen { get; set; }
        public clsError Error { get; set; }

        public cls_vc_Factura_Ren f_Factura_Ren_Buscar(string gs_Codemp, string gs_numfacSelected)
        {
            cls_vc_Factura_Ren objFacturaRen = new cls_vc_Factura_Ren();
            objFacturaRen.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[VC_M_FACTURA_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@numfac", gs_numfacSelected);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("N°", typeof(string)));
                dt.Columns.Add(new DataColumn("codart", typeof(string)));
                dt.Columns.Add(new DataColumn("nomart", typeof(string)));
                dt.Columns.Add(new DataColumn("cantid", typeof(decimal)));
                dt.Columns.Add(new DataColumn("preuni", typeof(decimal)));
                dt.Columns.Add(new DataColumn("ivacal", typeof(decimal)));
                dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
                dt.Columns.Add(new DataColumn("desren", typeof(decimal)));
                dt.Columns.Add(new DataColumn("coddes", typeof(string)));
                dt.Columns.Add(new DataColumn("totren", typeof(decimal)));
                sda.Fill(dt);

                objFacturaRen.dtFacturaRen = dt;
            }
            catch (Exception ex)
            {
                objFacturaRen.Error = objFacturaRen.Error.f_ErrorControlado(ex);
            }

            conn.Close();
            return objFacturaRen;
        }

        public clsError f_Factura_Ren_Actualizar(cls_vc_Factura_Ren objFacturaRen, string s_Action)
        {
            objFacturaRen.dtFacturaRen = f_dtFacturaRenTableType(objFacturaRen);  
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[VC_M_FACTURA_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", s_Action);
            var param = new SqlParameter("@FacturaRenTableType", objFacturaRen.dtFacturaRen);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public cls_vc_Factura_Ren f_CalculoPorIva(string gs_Codemp, string s_codart)
        {
            cls_vc_Factura_Ren objFacturaRen = new cls_vc_Factura_Ren();
            objFacturaRen.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_REN_IVA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
            cmd.Parameters.AddWithValue("@CODART", s_codart);

            //cmd.Parameters.Add("@s_Poriva", SqlDbType.Decimal,(13)).Direction = ParameterDirection.Output;
            SqlParameter parm = new SqlParameter("@s_Poriva", SqlDbType.Decimal);
            parm.Precision = 13;
            parm.Scale = 2;
            parm.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(parm);
            try
            {
                cmd.ExecuteNonQuery();
                objFacturaRen.Poriva = cmd.Parameters["@s_Poriva"].Value.ToString().Trim();
            }
            catch (Exception ex)
            {
                objFacturaRen.Poriva = "0";
                objFacturaRen.Error = objFacturaRen.Error.f_ErrorControlado(ex);
            }
            
            conn.Close();
            return objFacturaRen;
        }

        public cls_vc_Factura_Ren f_dtFacturaRenToObjFacturaRen(DataTable dt) //para pasar de dt a objFacturaRen los atributos de la base
        {
            cls_vc_Factura_Ren objFacturaRen = new cls_vc_Factura_Ren();
            objFacturaRen.Error = new clsError();

            DataTable dtRen = new DataTable();
            dtRen.Columns.Add(new DataColumn("N°", typeof(string)));
            dtRen.Columns.Add(new DataColumn("codart", typeof(string)));
            dtRen.Columns.Add(new DataColumn("nomart", typeof(string)));
            dtRen.Columns.Add(new DataColumn("cantid", typeof(decimal)));
            dtRen.Columns.Add(new DataColumn("preuni", typeof(decimal)));
            dtRen.Columns.Add(new DataColumn("ivacal", typeof(decimal)));
            dtRen.Columns.Add(new DataColumn("poriva", typeof(decimal)));
            dtRen.Columns.Add(new DataColumn("desren", typeof(decimal)));
            dtRen.Columns.Add(new DataColumn("coddes", typeof(string)));
            dtRen.Columns.Add(new DataColumn("totren", typeof(decimal)));
            dt.Merge(dtRen);
            objFacturaRen.dtFacturaRen = dt;
            try //en caso de q los renglones esten vacios se va al catch
            {
                objFacturaRen.Numfac = dt.Rows[0].Field<string>("numfac");
                objFacturaRen.Numren = dt.Rows[0].Field<int>("numren");
                objFacturaRen.Numite = dt.Rows[0].Field<int>("numite");
                objFacturaRen.Codart = dt.Rows[0].Field<string>("codart");
                objFacturaRen.Nomart = dt.Rows[0].Field<string>("nomart");
                objFacturaRen.Coduni = dt.Rows[0].Field<string>("coduni");
                objFacturaRen.Cantid = dt.Rows[0].Field<decimal>("cantid");
                objFacturaRen.Preuni = dt.Rows[0].Field<decimal>("preuni");
                objFacturaRen.Desren = dt.Rows[0].Field<decimal>("desren");
                objFacturaRen.Totren = dt.Rows[0].Field<decimal>("totren");
                objFacturaRen.Ubifis = dt.Rows[0].Field<string>("ubifis");
                objFacturaRen.Codiva = dt.Rows[0].Field<string>("codiva");
                objFacturaRen.Poriva = dt.Rows[0].Field<decimal>("poriva").ToString();
                objFacturaRen.Codmon = dt.Rows[0].Field<string>("codmon");
                objFacturaRen.Valcot = dt.Rows[0].Field<decimal>("valcot");
                objFacturaRen.Totext = dt.Rows[0].Field<decimal>("totext");
                objFacturaRen.Tiporg = dt.Rows[0].Field<string>("tiporg");
                objFacturaRen.Tiptra = dt.Rows[0].Field<string>("tiptra");
                objFacturaRen.Numtra = dt.Rows[0].Field<string>("numtra");
            }
            catch (Exception ex)
            {
                objFacturaRen.Error = new clsError();
                objFacturaRen.Error.f_ErrorControlado(ex);
            }
            
            return objFacturaRen;
        }

        public DataTable f_dtFacturaRenTableType(cls_vc_Factura_Ren objFacturaRen) //para generar un dataTable que tenga la estructura del TableType en SQL
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("codart", typeof(string)));
            dt.Columns.Add(new DataColumn("nomart", typeof(string)));
            dt.Columns.Add(new DataColumn("cantid", typeof(decimal)));
            dt.Columns.Add(new DataColumn("preuni", typeof(decimal)));
            dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
            dt.Columns.Add(new DataColumn("desren", typeof(decimal)));
            dt.Columns.Add(new DataColumn("coddes", typeof(string)));
            dt.Columns.Add(new DataColumn("totren", typeof(decimal)));
            dt.Columns.Add(new DataColumn("codemp", typeof(string)));
            dt.Columns.Add(new DataColumn("numfac", typeof(string)));
            dt.Columns.Add(new DataColumn("numren", typeof(int)));
            dt.Columns.Add(new DataColumn("numite", typeof(int)));
            dt.Columns.Add(new DataColumn("coduni", typeof(string)));
            dt.Columns.Add(new DataColumn("ubifis", typeof(string)));
            dt.Columns.Add(new DataColumn("codmed", typeof(string)));
            dt.Columns.Add(new DataColumn("nommed", typeof(string)));
            dt.Columns.Add(new DataColumn("codiva", typeof(string)));
            dt.Columns.Add(new DataColumn("codmon", typeof(string)));
            dt.Columns.Add(new DataColumn("valcot", typeof(string)));
            dt.Columns.Add(new DataColumn("totext", typeof(string)));
            dt.Columns.Add(new DataColumn("desesc", typeof(string)));
            dt.Columns.Add(new DataColumn("desedi", typeof(string)));
            dt.Columns.Add(new DataColumn("tiporg", typeof(string)));
            dt.Columns.Add(new DataColumn("tiptra", typeof(string)));
            dt.Columns.Add(new DataColumn("numtra", typeof(string)));
            dt.Columns.Add(new DataColumn("rentra", typeof(string)));
            dt.Columns.Add(new DataColumn("usudes", typeof(string)));
            dt.Columns.Add(new DataColumn("fecdes", typeof(string)));
            
            dt.Merge(objFacturaRen.dtFacturaRen);
            foreach (DataRow row in dt.Rows)
            {
                row["codemp"] = objFacturaRen.Codemp;
                row["numfac"] = objFacturaRen.Numfac;
                row["numren"] = row["N°"];
                row["numite"] = row["N°"];
                row["codiva"] = objFacturaRen.Codiva;
            }
            dt.Columns.Remove("N°");
            dt.Columns.Remove("ivacal");

            return dt;
        }
    }
}