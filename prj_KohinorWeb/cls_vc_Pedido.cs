﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
	public class cls_vc_Pedido
	{
		protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
		public string Codemp { get; set; }
		public string Tiptra { get; set; }
		public string Numtra { get; set; }
		public string Estado { get; set; }
		public string Codalm { get; set; }
		public string Codsuc { get; set; }
		public string Sersec { get; set; }
		public DateTime Fectra { get; set; }
		public DateTime Fecven { get; set; }
		public string Clicxc { get; set; }
		public string Nomcxc { get; set; }
		public string Lispre { get; set; }
		public string Codcen { get; set; }
		public string Nomcen { get; set; }
		public string Codven { get; set; }
		public string Codeje { get; set; }
		public string Nomeje { get; set; }
		public DateTime Fecent { get; set; } = DateTime.Now;
		public string Forpag { get; set; }
		public string Valido { get; set; }
		public string Numpro { get; set; }
		public string Observ { get; set; }
		public Decimal Totnet { get; set; }
		public Decimal Totbas { get; set; }
		public Decimal Totiv0 { get; set; }
		public Decimal Poriva { get; set; }
		public Decimal Totiva { get; set; }
		public Decimal Pordes { get; set; }
		public Decimal Totdes { get; set; }
		public Decimal Totfac { get; set; }
		public string Tiporg { get; set; }
		public string Numorg { get; set; }
		public string Codmon { get; set; }
		public string Valcot { get; set; }
		public decimal Numpag { get; set; }
		public decimal Plapag { get; set; }

		public DataTable dtPedido { get; set; }
		public cls_vc_Pedido_Ren PedidoRen { get; set; }
		public clsError Error { get; set; }

		public cls_vc_Pedido f_Pedido_Buscar(string gs_Codemp, string gs_tiptraSelected, string gs_numtraSelected)
		{
			cls_vc_Pedido objPedido = new cls_vc_Pedido();
			objPedido.Error = new clsError();
			conn.Open();
			SqlCommand cmd = new SqlCommand("[dbo].[VC_M_PEDIDO]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@Action", "Select");
			cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
			cmd.Parameters.AddWithValue("@tiptra", gs_tiptraSelected);
			cmd.Parameters.AddWithValue("@numtra", gs_numtraSelected);
			try
			{
				SqlDataAdapter sda = new SqlDataAdapter(cmd);
				DataTable dt = f_dtPedidoTableType();
				sda.Fill(dt);
				objPedido = f_dtPedidoToObjPedido(dt);
			}
			catch (Exception ex)
			{
				objPedido.Error = objPedido.Error.f_ErrorControlado(ex);
			}
			conn.Close();
			return objPedido;
		}
		public clsError f_Pedido_Actualizar(cls_vc_Pedido objPedido, string s_Action)
		{
			clsError objError = new clsError();
			conn.Open();
			SqlCommand cmd = new SqlCommand("[dbo].[VC_M_PEDIDO]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			// call the select task to get all data
			cmd.Parameters.AddWithValue("@Action", s_Action);
			var param = new SqlParameter("@PedidoTableType", objPedido.dtPedido);
			param.SqlDbType = SqlDbType.Structured;
			cmd.Parameters.Add(param);

			try
			{
				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				objError = objError.f_ErrorControlado(ex);
			}
			conn.Close();
			return objError;
		}
		public clsError f_Pedido_Eliminar(string gs_Codemp, string gs_tiptraSelected, string gs_numtraSelected)
		{
			clsError objError = new clsError();
			conn.Open();
			SqlCommand cmd = new SqlCommand("[dbo].[VC_M_PEDIDO]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@Action", "Delete");
			cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
			cmd.Parameters.AddWithValue("@tiptra", gs_tiptraSelected);
			cmd.Parameters.AddWithValue("@numtra", gs_numtraSelected);

			try
			{
				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				objError = objError.f_ErrorControlado(ex);
			}
			conn.Close();
			return objError;
		}
		public cls_vc_Pedido f_CalcularSecuencia(string gs_Codemp, string gs_Sersec)
		{
			cls_vc_Pedido objPedido = new cls_vc_Pedido();
			DataTable dtPedido = new DataTable();
			objPedido.Error = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_SEC]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
			cmd.Parameters.AddWithValue("@codsec", "VC_PED");
			cmd.Parameters.AddWithValue("@sersec", gs_Sersec); //del usuario en Login
			try
			{
				cmd.ExecuteNonQuery();
				SqlDataAdapter adapter = new SqlDataAdapter(cmd);
				adapter.Fill(dtPedido);
				string s_numtra = dtPedido.Rows[0].Field<string>("numfac");
				objPedido.Numtra = s_numtra;
				objPedido.Fectra = DateTime.Now; //DateTime.Now.ToString("yyyy-MM-dd");
			}
			catch (Exception ex)
			{
				objPedido.Error = objPedido.Error.f_ErrorControlado(ex);
			}

			conn.Close();
			return objPedido;
		}
		public clsError f_ActualizarSecuencia(string gs_Codemp, string gs_Sersec)
		{
			clsError objError = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_M_FACTURA_SEC]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
			cmd.Parameters.AddWithValue("@codsec", "VC_PED");
			cmd.Parameters.AddWithValue("@sersec", gs_Sersec); //del usuario en Login

			try
			{
				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				objError = objError.f_ErrorControlado(ex);
			}

			conn.Close();
			return objError;
		}

		public cls_vc_Pedido f_Pedido_Buscar_Cliente(string gs_Codemp, string s_codcli)
		{
			cls_vc_Pedido objPedido = new cls_vc_Pedido();
			objPedido.Error = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_S_PEDIDO_CLIENTE]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
			cmd.Parameters.AddWithValue("@CLICXC", s_codcli);

			cmd.Parameters.Add("@s_Clicxc", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
			cmd.Parameters.Add("@s_Nomcxc", SqlDbType.Char, 100).Direction = ParameterDirection.Output;
			cmd.Parameters.Add("@s_Lispre", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
			cmd.Parameters.Add("@s_Observ", SqlDbType.Char, 160).Direction = ParameterDirection.Output;
			try
			{
				cmd.ExecuteNonQuery();

				string clicxc = cmd.Parameters["@s_Clicxc"].Value.ToString().Trim();
				if (clicxc.Equals(""))
				{
					objPedido.Clicxc = "noEncontro";
				}
				else
				{
					objPedido.Clicxc = clicxc;
				}

				objPedido.Nomcxc = cmd.Parameters["@s_Nomcxc"].Value.ToString().Trim();
				objPedido.Lispre = cmd.Parameters["@s_Lispre"].Value.ToString().Trim();
				objPedido.Observ = cmd.Parameters["@s_Observ"].Value.ToString().Trim();
			}
			catch (Exception ex)
			{
				objPedido.Error = objPedido.Error.f_ErrorControlado(ex);
			}
			conn.Close();
			return objPedido;
		}
		public cls_vc_Pedido f_Pedido_Principio(string gs_Codemp, string gs_Sersec)
		{
			cls_vc_Pedido objPedido = new cls_vc_Pedido();
			objPedido.Error = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_S_PEDIDO_ORDEN]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
			cmd.Parameters.AddWithValue("@Action", "PRIMERO");
			cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);

			try
			{
				SqlDataAdapter sda = new SqlDataAdapter(cmd);
				DataSet ds = new DataSet();
				sda.Fill(ds);
				objPedido = f_dtPedidoToObjPedido(ds.Tables[0]);
				objPedido.PedidoRen = new cls_vc_Pedido_Ren();
				objPedido.PedidoRen = objPedido.PedidoRen.f_dtPedidoRenToObjPedidoRen(ds.Tables[1]);
			}
			catch (Exception ex)
			{
				objPedido.Error = objPedido.Error.f_ErrorControlado(ex);
			}
			conn.Close();
			return objPedido;
		}
		public cls_vc_Pedido f_Pedido_Siguiente(string gs_Codemp, string gs_Sersec, string gs_Siguiente)
		{
			cls_vc_Pedido objPedido = new cls_vc_Pedido();
			objPedido.Error = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_S_PEDIDO_ORDEN]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
			cmd.Parameters.AddWithValue("@Action", "SIGUIENTE");
			cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);
			cmd.Parameters.AddWithValue("@NUMTRA", gs_Siguiente);

			try
			{
				SqlDataAdapter sda = new SqlDataAdapter(cmd);
				DataSet ds = new DataSet();
				sda.Fill(ds);
				if (ds.Tables[0].Rows.Count > 0)
				{
					objPedido = f_dtPedidoToObjPedido(ds.Tables[0]);
					objPedido.PedidoRen = new cls_vc_Pedido_Ren();
					objPedido.PedidoRen = objPedido.PedidoRen.f_dtPedidoRenToObjPedidoRen(ds.Tables[1]);
					conn.Close();
				}
				else
				{
					conn.Close();
					objPedido = f_Pedido_Final(gs_Codemp, gs_Sersec);

				}
			}
			catch (Exception ex)
			{
				objPedido.Error = objPedido.Error.f_ErrorControlado(ex);
			}
			return objPedido;

		}
		public cls_vc_Pedido f_Pedido_Final(string gs_Codemp, string gs_Sersec)
		{
			cls_vc_Pedido objPedido = new cls_vc_Pedido();
			objPedido.Error = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_S_PEDIDO_ORDEN]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
			cmd.Parameters.AddWithValue("@Action", "ULTIMO");
			cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);

			try
			{
				SqlDataAdapter sda = new SqlDataAdapter(cmd);
				DataSet ds = new DataSet();
				sda.Fill(ds);
				objPedido = f_dtPedidoToObjPedido(ds.Tables[0]);
				objPedido.PedidoRen = new cls_vc_Pedido_Ren();
				objPedido.PedidoRen = objPedido.PedidoRen.f_dtPedidoRenToObjPedidoRen(ds.Tables[1]);
			}
			catch (Exception ex)
			{
				objPedido.Error = objPedido.Error.f_ErrorControlado(ex);
			}
			conn.Close();
			return objPedido;
		}
		public cls_vc_Pedido f_Pedido_Atras(string gs_Codemp, string gs_Sersec, string gs_Siguiente)
		{
			cls_vc_Pedido objPedido = new cls_vc_Pedido();
			objPedido.Error = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_S_PEDIDO_ORDEN]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
			cmd.Parameters.AddWithValue("@Action", "ANTERIOR");
			cmd.Parameters.AddWithValue("@SERSEC", gs_Sersec);
			cmd.Parameters.AddWithValue("@NUMTRA", gs_Siguiente);

			try
			{
				SqlDataAdapter sda = new SqlDataAdapter(cmd);
				DataSet ds = new DataSet();
				sda.Fill(ds);
				if (ds.Tables[0].Rows.Count > 0)
				{
					objPedido = f_dtPedidoToObjPedido(ds.Tables[0]);
					objPedido.PedidoRen = new cls_vc_Pedido_Ren();
					objPedido.PedidoRen = objPedido.PedidoRen.f_dtPedidoRenToObjPedidoRen(ds.Tables[1]);
					conn.Close();
				}
				else
				{
					conn.Close();
					objPedido = f_Pedido_Principio(gs_Codemp, gs_Sersec);

				}
			}
			catch (Exception ex)
			{
				objPedido.Error = objPedido.Error.f_ErrorControlado(ex);
			}
			return objPedido;
		}

		public cls_vc_Pedido f_dtPedidoToObjPedido(DataTable dt) //para pasar de dt a objPedido los atributos de la base
		{
			cls_vc_Pedido objPedido = new cls_vc_Pedido();
			objPedido.Error = new clsError();
			objPedido.dtPedido = dt;
			if (objPedido.dtPedido.Rows.Count > 0)
			{
				objPedido.Tiptra = dt.Rows[0].Field<string>("tiptra");
				objPedido.Numtra = dt.Rows[0].Field<string>("numtra");
				objPedido.Estado = dt.Rows[0].Field<string>("estado");
				objPedido.Codalm = dt.Rows[0].Field<string>("codalm");
				objPedido.Codsuc = dt.Rows[0].Field<string>("codsuc");
				objPedido.Sersec = dt.Rows[0].Field<string>("sersec");
				objPedido.Fectra = dt.Rows[0].Field<DateTime>("fectra");
				objPedido.Fecven = dt.Rows[0].Field<DateTime>("fecven");
				objPedido.Clicxc = dt.Rows[0].Field<string>("clicxc");
				objPedido.Nomcxc = dt.Rows[0].Field<string>("nomcxc");
				objPedido.Lispre = dt.Rows[0].Field<string>("lispre");
				objPedido.Codcen = dt.Rows[0].Field<string>("codcen");
				objPedido.Nomcen = dt.Rows[0].Field<string>("nomcen");
				objPedido.Codven = dt.Rows[0].Field<string>("codven");
				objPedido.Codeje = dt.Rows[0].Field<string>("codeje");
				objPedido.Nomeje = dt.Rows[0].Field<string>("nomeje");
				objPedido.Fecent = dt.Rows[0].Field<DateTime>("fecent");
				objPedido.Forpag = dt.Rows[0].Field<string>("forpag");
				objPedido.Valido = dt.Rows[0].Field<string>("valido");
				objPedido.Numpro = dt.Rows[0].Field<string>("numpro");
				objPedido.Observ = dt.Rows[0].Field<string>("observ");
				objPedido.Totnet = dt.Rows[0].Field<decimal>("totnet");
				objPedido.Totbas = dt.Rows[0].Field<decimal>("totbas");
				objPedido.Totiv0 = dt.Rows[0].Field<decimal>("totiv0");
				objPedido.Poriva = dt.Rows[0].Field<decimal>("poriva");
				objPedido.Totiva = dt.Rows[0].Field<decimal>("totiva");
				objPedido.Pordes = dt.Rows[0].Field<decimal>("pordes");
				objPedido.Totdes = dt.Rows[0].Field<decimal>("totdes");
				objPedido.Totfac = dt.Rows[0].Field<decimal>("totfac");
				objPedido.Tiporg = dt.Rows[0].Field<string>("tiporg");
				objPedido.Numorg = dt.Rows[0].Field<string>("numorg");
				objPedido.Codmon = dt.Rows[0].Field<string>("codmon");
				objPedido.Valcot = dt.Rows[0].Field<decimal>("valcot").ToString();
				objPedido.Numpag = dt.Rows[0].Field<decimal>("numpag");
				objPedido.Plapag = dt.Rows[0].Field<decimal>("plapag");
			}

			return objPedido;
		}
		public DataTable f_dtPedidoTableType() //para generar un dataTable que tenga la estructura del TableType en SQL
		{
			DataTable dt = new DataTable();
			dt.Columns.Add(new DataColumn("codemp", typeof(string)));
			dt.Columns.Add(new DataColumn("tiptra", typeof(string)));
			dt.Columns.Add(new DataColumn("numtra", typeof(string)));
			dt.Columns.Add(new DataColumn("estado", typeof(string)));
			dt.Columns.Add(new DataColumn("codalm", typeof(string)));
			dt.Columns.Add(new DataColumn("codsuc", typeof(string)));
			dt.Columns.Add(new DataColumn("sersec", typeof(string)));
			dt.Columns.Add(new DataColumn("fectra", typeof(DateTime)));
			dt.Columns.Add(new DataColumn("fecven", typeof(DateTime)));
			dt.Columns.Add(new DataColumn("clicxc", typeof(string)));
			dt.Columns.Add(new DataColumn("nomcxc", typeof(string)));
			dt.Columns.Add(new DataColumn("lispre", typeof(string)));
			dt.Columns.Add(new DataColumn("codcen", typeof(string)));
			dt.Columns.Add(new DataColumn("nomcen", typeof(string)));
			dt.Columns.Add(new DataColumn("codven", typeof(string)));
			dt.Columns.Add(new DataColumn("codeje", typeof(string)));
			dt.Columns.Add(new DataColumn("nomeje", typeof(string)));
			dt.Columns.Add(new DataColumn("fecent", typeof(DateTime)));
			dt.Columns.Add(new DataColumn("forpag", typeof(string)));
			dt.Columns.Add(new DataColumn("valido", typeof(string)));
			dt.Columns.Add(new DataColumn("numpro", typeof(string)));
			dt.Columns.Add(new DataColumn("observ", typeof(string)));
			dt.Columns.Add(new DataColumn("totnet", typeof(decimal)));
			dt.Columns.Add(new DataColumn("totbas", typeof(decimal)));
			dt.Columns.Add(new DataColumn("totiv0", typeof(decimal)));
			dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
			dt.Columns.Add(new DataColumn("totiva", typeof(decimal)));
			dt.Columns.Add(new DataColumn("pordes", typeof(decimal)));
			dt.Columns.Add(new DataColumn("totdes", typeof(decimal)));
			dt.Columns.Add(new DataColumn("totfac", typeof(decimal)));
			dt.Columns.Add(new DataColumn("tiporg", typeof(string)));
			dt.Columns.Add(new DataColumn("numorg", typeof(string)));
			dt.Columns.Add(new DataColumn("codmon", typeof(string)));
			dt.Columns.Add(new DataColumn("valcot", typeof(decimal)));
			dt.Columns.Add(new DataColumn("usuing", typeof(string)));
			dt.Columns.Add(new DataColumn("fecing", typeof(DateTime)));
			dt.Columns.Add(new DataColumn("codusu", typeof(string)));
			dt.Columns.Add(new DataColumn("fecult", typeof(DateTime)));
			dt.Columns.Add(new DataColumn("numpag", typeof(decimal)));
			dt.Columns.Add(new DataColumn("plapag", typeof(decimal)));

			return dt;
		}
	}
}