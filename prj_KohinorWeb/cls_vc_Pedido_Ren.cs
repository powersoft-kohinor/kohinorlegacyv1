﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb
{
	public class cls_vc_Pedido_Ren
	{
		protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
		public string Codemp { get; set; }
		public string Tiptra { get; set; }
		public string Numtra { get; set; }
		public int Numren { get; set; }
		public int Numite { get; set; }
		public string Codalm { get; set; }
		public string Codart { get; set; }
		public string Nomart { get; set; }
		public string Coduni { get; set; }
		public Decimal Cantid { get; set; }
		public Decimal Preuni { get; set; }
		public Decimal Desren { get; set; }
		public string Codiva { get; set; }
		public Decimal Poriva { get; set; }
		public Decimal Totren { get; set; }
		public string Ubifis { get; set; }

		public DataTable dtPedidoRen { get; set; }
		public clsError Error { get; set; }


		public cls_vc_Pedido_Ren f_Pedido_Ren_Buscar(string gs_Codemp, string gs_tiptraSelected, string gs_numtraSelected)
		{
			cls_vc_Pedido_Ren objPedidoRen = new cls_vc_Pedido_Ren();
			objPedidoRen.Error = new clsError();
			conn.Open();
			SqlCommand cmd = new SqlCommand("[dbo].[VC_M_PEDIDO_REN]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@Action", "Select");
			cmd.Parameters.AddWithValue("@codemp", gs_Codemp);
			cmd.Parameters.AddWithValue("@tiptra", gs_tiptraSelected);
			cmd.Parameters.AddWithValue("@numtra", gs_numtraSelected);
			try
			{
				SqlDataAdapter sda = new SqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				dt.Columns.Add(new DataColumn("N°", typeof(string)));
				dt.Columns.Add(new DataColumn("codart", typeof(string)));
				dt.Columns.Add(new DataColumn("nomart", typeof(string)));
				dt.Columns.Add(new DataColumn("codalm", typeof(string)));
				dt.Columns.Add(new DataColumn("coduni", typeof(string)));
				dt.Columns.Add(new DataColumn("cantid", typeof(decimal)));
				dt.Columns.Add(new DataColumn("preuni", typeof(decimal)));
				dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
				dt.Columns.Add(new DataColumn("desren", typeof(decimal)));
				dt.Columns.Add(new DataColumn("totren", typeof(decimal)));
				sda.Fill(dt);

				objPedidoRen.dtPedidoRen = dt;
			}
			catch (Exception ex)
			{
				objPedidoRen.Error = objPedidoRen.Error.f_ErrorControlado(ex);
			}

			conn.Close();
			return objPedidoRen;
		}

		public clsError f_Pedido_Ren_Actualizar(cls_vc_Pedido_Ren objPedidoRen, string s_Action)
		{
			objPedidoRen.dtPedidoRen = f_dtPedidoRenTableType(objPedidoRen);
			clsError objError = new clsError();
			conn.Open();
			SqlCommand cmd = new SqlCommand("[dbo].[VC_M_PEDIDO_REN]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@Action", s_Action);
			var param = new SqlParameter("@PedidoRenTableType", objPedidoRen.dtPedidoRen);
			param.SqlDbType = SqlDbType.Structured;
			cmd.Parameters.Add(param);

			try
			{
				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				objError = objError.f_ErrorControlado(ex);
			}
			conn.Close();
			return objError;
		}

		public cls_vc_Pedido_Ren f_CalculoPorIva(string gs_Codemp, string s_codart)
		{
			cls_vc_Pedido_Ren objPedidoRen = new cls_vc_Pedido_Ren();
			objPedidoRen.Error = new clsError();
			conn.Open();

			SqlCommand cmd = new SqlCommand("[dbo].[VC_S_FACTURA_REN_IVA]", conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@CODEMP", gs_Codemp);
			cmd.Parameters.AddWithValue("@CODART", s_codart);

			//cmd.Parameters.Add("@s_Poriva", SqlDbType.Decimal,(13)).Direction = ParameterDirection.Output;
			SqlParameter parm = new SqlParameter("@s_Poriva", SqlDbType.Decimal);
			parm.Precision = 13;
			parm.Scale = 2;
			parm.Direction = ParameterDirection.Output;
			cmd.Parameters.Add(parm);
			try
			{
				cmd.ExecuteNonQuery();
				objPedidoRen.Poriva = decimal.Parse(cmd.Parameters["@s_Poriva"].Value.ToString().Trim());
			}
			catch (Exception ex)
			{
				objPedidoRen.Poriva = 0;
				objPedidoRen.Error = objPedidoRen.Error.f_ErrorControlado(ex);
			}

			conn.Close();
			return objPedidoRen;
		}

		public cls_vc_Pedido_Ren f_dtPedidoRenToObjPedidoRen(DataTable dt) //para pasar de dt a objPedidoRen los atributos de la base
		{
			cls_vc_Pedido_Ren objPedidoRen = new cls_vc_Pedido_Ren();
			objPedidoRen.Error = new clsError();

			DataTable dtRen = new DataTable();
			dtRen.Columns.Add(new DataColumn("N°", typeof(string)));
			dtRen.Columns.Add(new DataColumn("codart", typeof(string)));
			dtRen.Columns.Add(new DataColumn("nomart", typeof(string)));
			dtRen.Columns.Add(new DataColumn("cantid", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("preuni", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("ivacal", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("poriva", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("desren", typeof(decimal)));
			dtRen.Columns.Add(new DataColumn("coddes", typeof(string)));
			dtRen.Columns.Add(new DataColumn("totren", typeof(decimal)));
			dt.Merge(dtRen);
			objPedidoRen.dtPedidoRen = dt;
			try //en caso de q los renglones esten vacios se va al catch
			{
				objPedidoRen.Numtra = dt.Rows[0].Field<string>("numtra");
				objPedidoRen.Numren = dt.Rows[0].Field<int>("numren");
				objPedidoRen.Numite = dt.Rows[0].Field<int>("numite");
				objPedidoRen.Codart = dt.Rows[0].Field<string>("codart");
				objPedidoRen.Nomart = dt.Rows[0].Field<string>("nomart");
				objPedidoRen.Coduni = dt.Rows[0].Field<string>("coduni");
				objPedidoRen.Cantid = dt.Rows[0].Field<decimal>("cantid");
				objPedidoRen.Preuni = dt.Rows[0].Field<decimal>("preuni");
				objPedidoRen.Desren = dt.Rows[0].Field<decimal>("desren");
				objPedidoRen.Totren = dt.Rows[0].Field<decimal>("totren");
				objPedidoRen.Ubifis = dt.Rows[0].Field<string>("ubifis");
				objPedidoRen.Codiva = dt.Rows[0].Field<string>("codiva");
				objPedidoRen.Poriva = dt.Rows[0].Field<decimal>("poriva");
				objPedidoRen.Tiptra = dt.Rows[0].Field<string>("tiptra");
				objPedidoRen.Numtra = dt.Rows[0].Field<string>("numtra");
			}
			catch (Exception ex)
			{
				objPedidoRen.Error = new clsError();
				objPedidoRen.Error.f_ErrorControlado(ex);
			}

			return objPedidoRen;
		}
		public DataTable f_dtPedidoRenTableType(cls_vc_Pedido_Ren objPedidoRen) //para generar un dataTable que tenga la estructura del TableType en SQL
		{
			DataTable dt = new DataTable();
			dt.Columns.Add(new DataColumn("codemp", typeof(string)));
			dt.Columns.Add(new DataColumn("tiptra", typeof(string)));
			dt.Columns.Add(new DataColumn("numtra", typeof(string)));
			dt.Columns.Add(new DataColumn("numren", typeof(int)));
			dt.Columns.Add(new DataColumn("numite", typeof(int)));
			dt.Columns.Add(new DataColumn("codalm", typeof(string)));
			dt.Columns.Add(new DataColumn("codart", typeof(string)));
			dt.Columns.Add(new DataColumn("nomart", typeof(string)));
			dt.Columns.Add(new DataColumn("coduni", typeof(string)));
			dt.Columns.Add(new DataColumn("cantid", typeof(decimal)));
			dt.Columns.Add(new DataColumn("preuni", typeof(decimal)));
			dt.Columns.Add(new DataColumn("desren", typeof(decimal)));
			dt.Columns.Add(new DataColumn("codiva", typeof(string)));
			dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
			dt.Columns.Add(new DataColumn("totren", typeof(decimal)));
			dt.Columns.Add(new DataColumn("ubifis", typeof(string)));
			dt.Merge(objPedidoRen.dtPedidoRen);
			foreach (DataRow row in dt.Rows)
			{
				row["codemp"] = objPedidoRen.Codemp;
				row["tiptra"] = objPedidoRen.Tiptra;
				row["numtra"] = objPedidoRen.Numtra;
				row["numren"] = row["N°"];
				row["numite"] = row["N°"];
				row["codiva"] = objPedidoRen.Codiva;
				row["ubifis"] = objPedidoRen.Ubifis;
			}
			dt.Columns.Remove("N°");
			return dt;
		}
	}
}