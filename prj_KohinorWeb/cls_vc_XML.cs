﻿using sriUtilidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

namespace prj_KohinorWeb
{
    public class cls_vc_XML
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        string is_ArchivoXML = null;
        string s_SerSec = null, s_Codemp = null, s_Codalm = null, s_numfac = null, s_CodCli = null, s_RucCed = null, s_tipind = null, s_NomCli = null, s_AuxClaAcc = null;
        decimal r_tipind = 0;
        DateTime d_fecfac;

        //SELECT TBLEMPRESA
        string is_DirCom = null, is_DirFir = null, is_DirGen = null, is_DirAut = null, is_DirNau = null;
        string is_UbicacionyNombreFirma = null, is_ClaveFirma = null;
        string is_BanSri = null, is_OblSri = null, is_TipSri = null, is_NumRes = null, is_Esqsri = null, ii_NumSeg = null;

        //infoTributaria
        string gs_nomemp = null, gs_RucEmp = null, s_clave = null, s_CodDoc = null, s_estab = null, s_ptoemi = null, s_Numero = null, gs_diremp = null;
        //infoFactura
        string s_DirSuc = null, s_tipid = null, rs_TotNet = null, rs_TotDes = null, rs_TotFac = null;
        decimal r_TotBas = 0, r_TotIv0 = 0, r_TotIva = 0;
        //infoFactura <pagos>
        string s_CodSri = null, s_bancod = null, s_valcob = null, s_CodFor = null, rs_valfpa = null;

        //totalImpuesto
        string s_CodPor = null;
        decimal r_PorIva = 0;

        //Detalles
        string s_codart = null, s_NomArt = null;
        decimal r_cantid = 0, r_preuni = 0, r_DesRen = 0, DESREN = 0, r_totren = 0, r_poriva = 0;

        //infoAdicional
        string s_DirCli = null, s_TelCli = null, s_EmaCli = null;

        DataTable dtEncPV, dt_FormaPago, dt_FacturaRen;

        public cls_vc_XML(string sersec, string codemp, string codalm, DataTable dtEncPV, DataTable dt_FacturaRen, DataTable dt_FormaPago)
        {
            s_SerSec = sersec;
            s_Codemp = codemp;
            s_Codalm = codalm;
            this.dtEncPV = dtEncPV;
            this.dt_FacturaRen = dt_FacturaRen;
            this.dt_FormaPago = dt_FormaPago;
        }
        public clsError f_ProcesoXML_SRI(){
            clsError objError = new clsError();
            objError = f_DatosEmpresa(); 
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                objError = f_ValoresIniciales();
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    objError = f_NombreArchvivoXML();
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        objError = f_create();
                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                            objError = f_FirmaDocumento();
                            if (String.IsNullOrEmpty(objError.Mensaje))
                            {
                                //objError = f_EnviarSRI();
                            }
                        }
                    }
                }
            }            
            return objError;
        }

        protected clsError f_DatosEmpresa()
        {
            clsError objError = new clsError();            
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_S_EMPRESA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "FULL");
            cmd.Parameters.AddWithValue("@CODEMP", s_Codemp);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    gs_nomemp = dt.Rows[0]["nomemp"].ToString();
                    gs_RucEmp = dt.Rows[0]["ruc"].ToString();
                    gs_diremp = dt.Rows[0]["dir01"].ToString();
                    is_DirCom = dt.Rows[0]["dircom"].ToString();
                    is_DirFir = dt.Rows[0]["dirfir"].ToString();
                    is_DirGen = dt.Rows[0]["dirgen"].ToString();
                    is_DirAut = dt.Rows[0]["diraut"].ToString();
                    is_DirNau = dt.Rows[0]["dirnau"].ToString();
                    is_UbicacionyNombreFirma = dt.Rows[0]["arcfir"].ToString();
                    is_ClaveFirma = dt.Rows[0]["clafir"].ToString();
                    is_BanSri = dt.Rows[0]["bansri"].ToString();
                    is_OblSri = dt.Rows[0]["oblsri"].ToString();
                    is_TipSri = dt.Rows[0]["tipsri"].ToString();
                    is_NumRes = dt.Rows[0]["numres"].ToString();
                    is_Esqsri = dt.Rows[0]["esqsri"].ToString();
                    ii_NumSeg = dt.Rows[0]["numseg"].ToString();
                }
            }
            catch (Exception ex) //ERROR EN SP
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
        protected clsError f_ValoresIniciales() //genera xml.txt
        {
            clsError objError = new clsError();
            //s_AuxClaAcc = istr_aux.dw_par01.GetItemString(1, "srie_claacc");  ???
            //s_SerSec = s_sersec;
            d_fecfac = DateTime.Parse(dtEncPV.Rows[0]["fecfac"].ToString());
            //string s_codemp = Session["gs_CodEmp"].ToString();
            //string s_codalm = Session["gs_CodAlm"].ToString();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString))
            {
                conn.Open();
                string sql = "SELECT Isnull(diralm, '') FROM inv_almacen WHERE codemp = @s_codemp and codalm = @s_codalm";
                SqlCommand com = new SqlCommand(sql, conn);
                com.Parameters.AddWithValue("@s_codemp", s_Codemp);
                com.Parameters.AddWithValue("@s_codalm", s_Codalm);
                if (com.ExecuteScalar() != null)
                    s_DirSuc = com.ExecuteScalar().ToString();
                else
                    s_DirSuc = "";
                conn.Close();
            }

            s_CodDoc = "01";
            s_estab = s_SerSec.Substring(0, 3); //OJO EL start DE SUBSTRING EMPIEZA EN 0 NO EN 1, PERO length SI ES EN 1...Substring(start, length)
            s_ptoemi = s_SerSec.Substring(3, 3);

            s_numfac = dtEncPV.Rows[0]["numfac"].ToString();
            s_CodCli = dtEncPV.Rows[0]["codcli"].ToString();
            s_Numero = dtEncPV.Rows[0]["numfac"].ToString();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString))
            {
                conn.Open();
                string sql = "SELECT rucced, tipind, nomcli, dircli, telcli, email FROM vc_cliente WHERE codemp=@s_codemp and codcli=@s_codcli";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@s_codemp", s_Codemp);
                cmd.Parameters.AddWithValue("@s_codcli", s_CodCli);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                conn.Close();

                try
                {
                    s_RucCed = dt.Rows[0]["rucced"].ToString();
                    r_tipind = decimal.Parse(dt.Rows[0]["tipind"].ToString());
                    s_NomCli = dt.Rows[0]["nomcli"].ToString();
                    s_DirCli = dt.Rows[0]["dircli"].ToString();
                    s_TelCli = dt.Rows[0]["telcli"].ToString();
                    s_EmaCli = dt.Rows[0]["email"].ToString();
                }
                catch (Exception ex)
                {
                    objError = objError.f_ErrorControlado(ex);
                    //throw;
                }
            }

            if (r_tipind == 1) s_tipid = "04";
            if (r_tipind == 2) s_tipid = "05";
            if (r_tipind == 3) s_tipid = "06";
            if (r_tipind == 4) s_tipid = "07";
            //if (!Session["gs_txtTotdes"].ToString().Equals(""))
            //    r_TotDes = decimal.Parse(Session["gs_txtTotdes"].ToString());

            if (String.IsNullOrEmpty(s_DirCli))
                objError.Mensaje = "❗ *02. " + DateTime.Now + "No pudo generar archivo XML NO EXISTE DIRECCION PROVEEDOR";
            if (String.IsNullOrEmpty(s_TelCli))
                objError.Mensaje = "❗ *02. " + DateTime.Now + "No pudo generar archivo XML NO EXISTE TELEFONO PROVEEDOR";
            if (String.IsNullOrEmpty(s_EmaCli))
                objError.Mensaje = "❗ *02. " + DateTime.Now + "No pudo generar archivo XML NO EXISTE EMAIL PROVEEDOR";
            if (s_Numero.Equals("000000000000000"))
                objError.Mensaje = "❗ *02. " + DateTime.Now + "No pudo generar archivo XML NUMERO DE RETENCION INCORRECTO";
            s_DirCli = f_CaracteresEspeciales(s_DirCli);

            if (String.IsNullOrEmpty(s_AuxClaAcc)) //???
            {
                s_clave = d_fecfac.ToString("ddMMyyyy") + s_CodDoc + gs_RucEmp + is_BanSri + s_SerSec + '0' + s_Numero.Substring(7, 8) + s_Numero.Substring(7, 8) + is_TipSri;
            }
            return objError;
        }
        protected clsError f_NombreArchvivoXML() //tambien actualiza al dt encabezado PV con campos de sri
        {
            clsError objError = new clsError();
            is_ArchivoXML = Convert.ToString("PVE_" + s_Numero.Substring(0, 6) + '0' + s_Numero.Substring(7, 8) + ".xml");

            //ACTUALIZAR gdt_EncPV
            DataTable dt = dtEncPV;
            if (dt.Rows.Count > 0)
            {
                dt.Rows[0]["srie_claacc"] = s_clave;
                dt.Rows[0]["srie_nomarc"] = is_ArchivoXML;
                //string s_codart = dt.Rows[0].Field<string>("nomcli");
                //string s_codart = dt.Rows[0].Field<string>("nomcli");
            }
            else objError.Mensaje = "❗ *04. " + DateTime.Now + "Error al leer gdt_EncPV. No se generó archivo XML.";

            //grvBorrar.DataSource = dt;
            //grvBorrar.DataBind();
            return objError;
        }

        protected clsError f_create()
        {
            clsError objError = new clsError();
            //Start writer
            //XmlTextWriter writer = new XmlTextWriter(Server.MapPath("~/XMLFiles/" + xmlfile_name + ".xml"), System.Text.Encoding.UTF8);
            //XmlTextWriter writer = new XmlTextWriter(Server.MapPath("~/XMLFiles/" + is_ArchivoXML), Encoding.GetEncoding("ISO-8859-1"));
            XmlTextWriter writer = new XmlTextWriter(HttpContext.Current.Server.MapPath("~/Comprobantes/Generados/" + is_ArchivoXML), Encoding.GetEncoding("ISO-8859-1"));

            //Start XM DOcument
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;

            //ROOT Element
            writer.WriteStartElement("factura");
            writer.WriteAttributeString("id", "comprobante");
            writer.WriteAttributeString("version", "1.0.0");
            //writer.WriteStartElement("\"factura id = \"comprobante\" version = \"1.0.0\"");
            //call create nodes method
            //createNode(book_name, book_price, author1_fname, author1_lname, author2_fname, author2_lname, author1_country, author1_state, author2_country, author2_state, writer);
            f_createNode(writer);

            writer.WriteEndElement();

            //End XML Document
            writer.WriteEndDocument();

            //Close writer
            writer.Close();
            return objError;
        }
        protected string f_createNode(XmlTextWriter writer)
        {
            clsError objError = new clsError();
            //parent node start infoTributaria ****************************
            writer.WriteStartElement("infoTributaria");

            writer.WriteStartElement("ambiente"); //ambiente
            writer.WriteString(is_BanSri);
            writer.WriteEndElement();

            writer.WriteStartElement("tipoEmision"); //tipoEmision
            writer.WriteString(is_TipSri);
            writer.WriteEndElement();

            writer.WriteStartElement("razonSocial"); //razonSocial
            writer.WriteString(gs_nomemp);
            writer.WriteEndElement();

            writer.WriteStartElement("nombreComercial"); //nombreComercial
            writer.WriteString(gs_nomemp);
            writer.WriteEndElement();

            writer.WriteStartElement("ruc"); //ruc
            writer.WriteString(gs_RucEmp);
            writer.WriteEndElement();

            writer.WriteStartElement("claveAcceso"); //claveAcceso
            writer.WriteString(s_clave);
            writer.WriteEndElement();

            writer.WriteStartElement("codDoc"); //codDoc
            writer.WriteString(s_CodDoc);
            writer.WriteEndElement();

            writer.WriteStartElement("estab"); //estab
            writer.WriteString(s_estab);
            writer.WriteEndElement();

            writer.WriteStartElement("ptoEmi"); //ptoEmi
            writer.WriteString(s_ptoemi);
            writer.WriteEndElement();

            writer.WriteStartElement("secuencial"); //secuencial
            writer.WriteString(s_Numero.Substring(7, 8)); //mid(s_Numero,8,8)
            writer.WriteEndElement();

            writer.WriteStartElement("dirMatriz"); //ptoEmi
            writer.WriteString(gs_diremp);
            writer.WriteEndElement();

            writer.WriteEndElement();  //infoTributaria ****************************


            //parent node start infoFactura ****************************
            writer.WriteStartElement("infoFactura");

            writer.WriteStartElement("fechaEmision"); //fechaEmision
            writer.WriteString(DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
            writer.WriteEndElement();

            writer.WriteStartElement("dirEstablecimiento"); //dirEstablecimiento
            writer.WriteString(s_DirSuc);
            writer.WriteEndElement();

            writer.WriteStartElement("contribuyenteEspecial"); //contribuyenteEspecial
            writer.WriteString(is_NumRes);
            writer.WriteEndElement();

            writer.WriteStartElement("obligadoContabilidad"); //obligadoContabilidad
            writer.WriteString(is_OblSri);
            writer.WriteEndElement();

            writer.WriteStartElement("tipoIdentificacionComprador"); //tipoIdentificacionComprador
            writer.WriteString(s_tipid);
            writer.WriteEndElement();

            writer.WriteStartElement("razonSocialComprador"); //razonSocialComprador
            writer.WriteString(f_CaracteresEspeciales(s_NomCli));
            writer.WriteEndElement();

            writer.WriteStartElement("identificacionComprador"); //identificacionComprador
            writer.WriteString(s_RucCed);
            writer.WriteEndElement();

            //TOTAL SIN IMPUESTOS
            rs_TotNet = dtEncPV.Rows[0]["totnet"].ToString();
            rs_TotDes = dtEncPV.Rows[0]["totdes"].ToString();

            writer.WriteStartElement("totalSinImpuestos"); //totalSinImpuestos
            writer.WriteString(rs_TotNet);
            writer.WriteEndElement();

            writer.WriteStartElement("totalDescuento"); //totalDescuento
            writer.WriteString(rs_TotDes);
            writer.WriteEndElement();

            //parent node start totalConImpuestos ****************************
            writer.WriteStartElement("totalConImpuestos");

            r_TotBas = decimal.Parse(dtEncPV.Rows[0]["totbas"].ToString());
            r_TotIv0 = decimal.Parse(rs_TotNet) - r_TotBas;
            r_TotIva = decimal.Parse(rs_TotNet) - r_TotBas;

            if (r_TotIv0 > 0)
            {
                //parent node start totalImpuesto ****************************
                writer.WriteStartElement("totalImpuesto");

                //CODIGO SI ES IVA 2, ICE 3
                writer.WriteStartElement("codigo"); //codigo
                writer.WriteString("2");
                writer.WriteEndElement();
                //CODIGO PORCENTAJE 0=0%, 2=12%, 6=No objeto del Iva
                //CODIGO PORCENTAJE 0=0%, 2=12%, 6=No objeto del Iva
                r_PorIva = 0; // istr_aux.dw_par01.GetItemNumber( 1,"poriva" )
                s_CodPor = "6";
                //select codsri INTO: s_CodPor from iva where poriva = :r_PorIva;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString))
                {
                    conn.Open();
                    string sql = "SELECT TOP 1 codsri FROM cfg_iva WHERE poriva = @r_PorIva";
                    SqlCommand com = new SqlCommand(sql, conn);
                    com.Parameters.AddWithValue("@r_PorIva", r_PorIva);
                    if (com.ExecuteScalar() != null)
                        s_CodPor = com.ExecuteScalar().ToString();
                    else
                        s_CodPor = "";
                    conn.Close();
                }
                if (r_PorIva == 0) s_CodPor = "0";//IF r_PorIva =0 THEN s_CodPor ='0'
                if (r_PorIva == 12) s_CodPor = "2";//IF r_PorIva =12 THEN s_CodPor ='2'
                writer.WriteStartElement("codigoPorcentaje"); //codigoPorcentaje
                writer.WriteString(s_CodPor);
                writer.WriteEndElement();
                //r_TotBas = istr_aux.dw_par01.GetItemNumber(1, "totbas");
                writer.WriteStartElement("baseImponible"); //baseImponible
                writer.WriteString(r_TotIv0.ToString("0.00"));
                writer.WriteEndElement();
                //r_TotIva = istr_aux.dw_par01.GetItemNumber(1, "totiva");
                writer.WriteStartElement("valor"); //valor
                writer.WriteString("0.00");
                writer.WriteEndElement();

                writer.WriteEndElement(); //totalImpuesto ****************************
            }

            if (r_TotBas > 0)
            {
                //parent node start totalImpuesto ****************************
                writer.WriteStartElement("totalImpuesto");

                //CODIGO SI ES IVA 2, ICE 3
                writer.WriteStartElement("codigo"); //codigo
                writer.WriteString("2");
                writer.WriteEndElement();
                //CODIGO PORCENTAJE 0=0%, 2=12%, 6=No objeto del Iva
                //r_PorIva = istr_aux.dw_par01.GetItemNumber(1, "poriva");
                r_PorIva = dtEncPV.Rows[0].Field<decimal>("poriva");
                s_CodPor = "6";
                //select codsri INTO: s_CodPor from iva where poriva = :r_PorIva;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString))
                {
                    conn.Open();
                    string sql = "SELECT TOP 1 codsri FROM cfg_iva WHERE poriva = @r_PorIva";
                    SqlCommand com = new SqlCommand(sql, conn);
                    com.Parameters.AddWithValue("@r_PorIva", r_PorIva);
                    if (com.ExecuteScalar() != null)
                        s_CodPor = com.ExecuteScalar().ToString();
                    else
                        s_CodPor = "";
                    conn.Close();
                }
                if (r_PorIva == 0) s_CodPor = "0";//IF r_PorIva =0 THEN s_CodPor ='0'
                if (r_PorIva == 12) s_CodPor = "2";//IF r_PorIva =12 THEN s_CodPor ='2'
                writer.WriteStartElement("codigoPorcentaje"); //codigoPorcentaje
                writer.WriteString(s_CodPor);
                writer.WriteEndElement();
                //r_TotBas = istr_aux.dw_par01.GetItemNumber(1, "totbas");
                writer.WriteStartElement("baseImponible"); //baseImponible
                writer.WriteString(r_TotBas.ToString("0.00"));
                writer.WriteEndElement();
                //r_TotIva = istr_aux.dw_par01.GetItemNumber(1, "totiva");
                writer.WriteStartElement("valor"); //valor
                writer.WriteString(r_TotIva.ToString("0.00"));
                writer.WriteEndElement();

                writer.WriteEndElement(); //totalImpuesto ****************************
            }

            if (r_TotBas == 0 && r_TotIv0 == 0)
            {
                //parent node start totalImpuesto ****************************
                writer.WriteStartElement("totalImpuesto");

                //CODIGO SI ES IVA 2, ICE 3
                writer.WriteStartElement("codigo"); //codigo
                writer.WriteString("2");
                writer.WriteEndElement();
                //CODIGO PORCENTAJE 0=0%, 2=12%, 6=No objeto del Iva
                //r_PorIva = istr_aux.dw_par01.GetItemNumber(1, "poriva");
                r_PorIva = dtEncPV.Rows[0].Field<decimal>("poriva");
                s_CodPor = "6";
                //select codsri INTO: s_CodPor from iva where poriva = :r_PorIva;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString))
                {
                    conn.Open();
                    string sql = "SELECT TOP 1 codsri FROM cfg_iva WHERE poriva = @r_PorIva";
                    SqlCommand com = new SqlCommand(sql, conn);
                    com.Parameters.AddWithValue("@r_PorIva", r_PorIva);
                    if (com.ExecuteScalar() != null)
                        s_CodPor = com.ExecuteScalar().ToString();
                    else
                        s_CodPor = "";
                    conn.Close();
                }
                if (r_PorIva == 0) s_CodPor = "0";//IF r_PorIva =0 THEN s_CodPor ='0'
                if (r_PorIva == 12) s_CodPor = "2";//IF r_PorIva =12 THEN s_CodPor ='2'
                writer.WriteStartElement("codigoPorcentaje"); //codigoPorcentaje
                writer.WriteString(s_CodPor);
                writer.WriteEndElement();
                //r_TotBas = istr_aux.dw_par01.GetItemNumber(1, "totbas");
                writer.WriteStartElement("baseImponible"); //baseImponible
                writer.WriteString(r_TotBas.ToString("0.00"));
                writer.WriteEndElement();
                //r_TotIva = istr_aux.dw_par01.GetItemNumber(1, "totiva");
                writer.WriteStartElement("valor"); //valor
                writer.WriteString(r_TotIva.ToString("0.00"));
                writer.WriteEndElement();

                writer.WriteEndElement(); //totalImpuesto ****************************
            }

            writer.WriteEndElement(); //totalConImpuestos ****************************

            writer.WriteStartElement("propina"); //propina
            writer.WriteString("0.00");
            writer.WriteEndElement();

            rs_TotFac = dtEncPV.Rows[0]["totfac"].ToString();
            writer.WriteStartElement("importeTotal"); //importeTotal
            writer.WriteString(rs_TotFac);
            writer.WriteEndElement();

            writer.WriteStartElement("moneda"); //moneda
            writer.WriteString("DOLAR");
            writer.WriteEndElement();

            //parent node start pagos ****************************
            writer.WriteStartElement("pagos");

            DataTable dtRenglonesFP = dt_FormaPago;
            foreach (DataRow row in dtRenglonesFP.Rows)
            {
                //parent node start pago ****************************
                writer.WriteStartElement("pago");

                s_CodFor = row.Field<string>("Forma de Pago");
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString))
                {
                    conn.Open();
                    string sql = "SELECT isnull(codsri,'') FROM cfg_formapago WHERE codemp =  @s_codemp and codfor = @s_CodFor";
                    SqlCommand com = new SqlCommand(sql, conn);
                    com.Parameters.AddWithValue("@s_codemp", s_Codemp);
                    com.Parameters.AddWithValue("@s_CodFor", s_CodFor);
                    if (com.ExecuteScalar() != null)
                        s_CodSri = com.ExecuteScalar().ToString();
                    else
                        s_CodSri = "";
                    conn.Close();
                }

                if (String.IsNullOrEmpty(s_CodSri))
                {
                    objError.Mensaje = "¡Ingrese forma de pago en el modulo CONFIGURACION, Formas de Pago!";
                    //DataTable dt = (DataTable)Session["gdt_EncPV"];
                    if (dtEncPV.Rows.Count > 0)
                        dtEncPV.Rows[0]["srie_mensaj"] = "Ingrese forma de pago en el modulo COFIGURACION formas de pago";
                }

                writer.WriteStartElement("formaPago"); //formaPago
                writer.WriteString(s_CodSri);
                writer.WriteEndElement();

                rs_valfpa = row.Field<string>("Valor");
                writer.WriteStartElement("total"); //total
                writer.WriteString(rs_valfpa);
                writer.WriteEndElement();

                writer.WriteStartElement("plazo"); //plazo
                writer.WriteString("0");
                writer.WriteEndElement();

                writer.WriteStartElement("unidadTiempo"); //unidadTiempo
                writer.WriteString("dias");
                writer.WriteEndElement();

                writer.WriteEndElement(); //pago ****************************
            }

            //DataTable dtRenglones2 = (DataTable)Session["gdt_Renglones2"]; //wladi dijo no hace aun
            //foreach (DataRow row in dtRenglones2.Rows)
            //{
            //    //parent node start pago ****************************
            //    writer.WriteStartElement("pago");

            //    //if (String.IsNullOrEmpty(s_bancod))
            //    //{
            //    //    Response.Write("<script>alert('¡Ingrese forma de pago en el boton PAGOS!');</script>");
            //    //}

            //    writer.WriteStartElement("formaPago"); //formaPago
            //    writer.WriteString(s_bancod);
            //    writer.WriteEndElement();

            //    writer.WriteStartElement("total"); //total
            //    writer.WriteString(s_valcob);
            //    writer.WriteEndElement();

            //    writer.WriteStartElement("plazo"); //plazo
            //    //s_Linea = "<plazo>" + string(DaysAfter(date(istr_aux.dw_par05.GetItemDatetime(i, "fecemi")), Date(istr_aux.dw_par05.GetItemDatetime(i, "fectra"))), "###0") + "</plazo>"
            //    writer.WriteString(""); //???
            //    writer.WriteEndElement();

            //    writer.WriteStartElement("unidadTiempo"); //unidadTiempo
            //    writer.WriteString("dias");
            //    writer.WriteEndElement();

            //    writer.WriteEndElement(); //pago ****************************
            //}

            writer.WriteEndElement(); //pagos ****************************

            writer.WriteEndElement(); //infoFactura ****************************

            //parent node start detalles ****************************
            writer.WriteStartElement("detalles");

            DataTable dtRenglones = dt_FacturaRen;
            foreach (DataRow row in dtRenglones.Rows)
            {
                //IF TRIM(istr_aux.dw_par02.GetItemString(i,"codart") ) <> '*' THEN
                //parent node start detalle ****************************
                writer.WriteStartElement("detalle");

                s_codart = row.Field<string>("codart").Trim();
                writer.WriteStartElement("codigoPrincipal"); //codigoPrincipal
                writer.WriteString(s_codart);
                writer.WriteEndElement();

                writer.WriteStartElement("codigoAuxiliar"); //codigoAuxiliar
                writer.WriteString(s_codart);
                writer.WriteEndElement();

                s_NomArt = row.Field<string>("nomart");
                writer.WriteStartElement("descripcion"); //descripcion
                writer.WriteString(f_CaracteresEspeciales(s_NomArt));
                writer.WriteEndElement();

                r_cantid = row.Field<decimal>("cantid");
                writer.WriteStartElement("cantidad"); //cantidad
                writer.WriteString(r_cantid.ToString("0.00"));
                writer.WriteEndElement();

                r_preuni = row.Field<decimal>("preuni");
                writer.WriteStartElement("precioUnitario"); //precioUnitario
                writer.WriteString(r_preuni.ToString("0.00"));
                writer.WriteEndElement();

                r_totren = row.Field<decimal>("totren");
                //r_DesRen = Round(istr_aux.dw_par02.GetItemNumber(i, "cantid") * istr_aux.dw_par02.GetItemNumber(i, "preuni"), 2)
                //r_DesRen = Round(r_DesRen - istr_aux.dw_par02.GetItemNumber(i, "totren"), 2)
                r_DesRen = r_cantid * r_preuni;
                r_DesRen = r_DesRen - r_totren;

                //IF istr_aux.dw_par02.GetItemNumber(i, "DESREN") = 0  THEN r_DesRen = 0
                if (row.Field<decimal>("desren") == 0) r_DesRen = 0;

                writer.WriteStartElement("descuento"); //descuento
                writer.WriteString(r_DesRen.ToString("0.00"));
                writer.WriteEndElement();

                writer.WriteStartElement("precioTotalSinImpuesto"); //precioTotalSinImpuesto
                writer.WriteString(r_totren.ToString("0.00"));
                writer.WriteEndElement();

                //parent node start impuestos ****************************
                writer.WriteStartElement("impuestos");
                //parent node start impuesto ****************************
                writer.WriteStartElement("impuesto");
                r_PorIva = row.Field<decimal>("poriva");
                s_CodPor = "6";

                //select codsri INTO: s_CodPor from iva where poriva = :r_PorIva;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString))
                {
                    conn.Open();
                    string sql = "SELECT TOP 1 codsri FROM cfg_iva WHERE poriva = @r_PorIva";
                    SqlCommand com = new SqlCommand(sql, conn);
                    com.Parameters.AddWithValue("@r_PorIva", r_PorIva);
                    if (com.ExecuteScalar() != null)
                        s_CodPor = com.ExecuteScalar().ToString();
                    else
                        s_CodPor = "";
                    conn.Close();
                }
                if (r_PorIva == 0) s_CodPor = "0";//IF r_PorIva =0 THEN s_CodPor ='0'
                if (r_PorIva == 12) s_CodPor = "2";//IF r_PorIva =12 THEN s_CodPor ='2'
                writer.WriteStartElement("codigo"); //codigo
                writer.WriteString("2");
                writer.WriteEndElement();

                writer.WriteStartElement("codigoPorcentaje"); //codigoPorcentaje
                writer.WriteString(s_CodPor);
                writer.WriteEndElement();

                writer.WriteStartElement("tarifa"); //tarifa
                writer.WriteString(r_PorIva.ToString("0.00"));
                writer.WriteEndElement();

                writer.WriteStartElement("baseImponible"); //baseImponible
                writer.WriteString(r_totren.ToString("0.00"));
                writer.WriteEndElement();

                writer.WriteStartElement("valor"); //valor
                //s_Linea = "<valor>" + string(Round(istr_aux.dw_par02.GetItemNumber(i, "totren") * r_poriva / 100, 2), "###0.00") + "</valor>"
                writer.WriteString((r_totren * r_poriva / 100).ToString("0.00"));
                writer.WriteEndElement();

                writer.WriteEndElement(); //impuesto ****************************
                writer.WriteEndElement(); //impuestos ****************************
                writer.WriteEndElement(); //detalle ****************************
            }

            writer.WriteEndElement(); //detalles ****************************

            //parent node start infoAdicional ****************************
            writer.WriteStartElement("infoAdicional");

            writer.WriteStartElement("campoAdicional"); //campoAdicional
            writer.WriteAttributeString("nombre", "Dirección");
            writer.WriteString(s_DirCli);
            writer.WriteEndElement();

            writer.WriteStartElement("campoAdicional"); //campoAdicional
            writer.WriteAttributeString("nombre", "Teléfono");
            writer.WriteString(s_TelCli);
            writer.WriteEndElement();

            writer.WriteStartElement("campoAdicional"); //campoAdicional
            writer.WriteAttributeString("nombre", "Email");
            writer.WriteString(s_EmaCli);
            writer.WriteEndElement();

            writer.WriteEndElement(); //infoAdicional ****************************
                                      //writer.WriteEndElement(); //factura ****************************

            return "XML creado!";
        }
        protected string f_CaracteresEspeciales(string cadena)
        {
            try
            {
                cadena = cadena.Replace("&", "");
                cadena = cadena.Replace("/", "");
                cadena = cadena.Replace("-", "");
                cadena = cadena.Replace("ň", "n");
                cadena = cadena.Replace("á", "a");
                cadena = cadena.Replace("é", "e");
                cadena = cadena.Replace("í", "i");
                cadena = cadena.Replace("ó", "o");
                cadena = cadena.Replace("ú", "u");
                cadena = cadena.Replace("Ň", "N");
                cadena = cadena.Replace(".", "");
            }
            catch (Exception ex)
            {
                //lblResult
                //throw;
            }

            return cadena;
        }

        protected clsError f_FirmaDocumento()
        {
            clsError objError = new clsError();
            sriUtilidades.main io_dllSRI = new main();//OJO para q funcione sriUtilidades se debe isntalar paquete PM> Install-Package IKVM.OpenJDK.XML.Parse -Version 7.2.4630.5

            //io_dllSRI.PathFirmados = is_DirFir; // Carpeta donde se van a guardar los archivos firmados
            io_dllSRI.PathFirmados = HttpContext.Current.Server.MapPath("~/Comprobantes/Firmados/");
            //io_dllSRI.ArchivoXMLSinFirmar = is_DirGen + is_ArchivoXML;//Archivo XML generado, el cual se va a firmar
            io_dllSRI.ArchivoXMLSinFirmar = HttpContext.Current.Server.MapPath(Path.Combine("~/Comprobantes/Generados/", is_ArchivoXML));
            //io_dllSRI.FirmaElectronica = is_UbicacionyNombreFirma; // Archivo de firma electrónica
            io_dllSRI.FirmaElectronica = HttpContext.Current.Server.MapPath("~/Comprobantes/Firma/ruth_janeth_sandoval_arcos2018.p12");
            io_dllSRI.ClaveToken = is_ClaveFirma;   // Clave del archivo
            io_dllSRI.estadoFirmado = "";           // Inicializa para comentarios devueltos de la librería
            io_dllSRI.ArchivoXMLFirmado = ""; 		//Ruta y nombre del archivo que ha sido firmado
            try
            {
                bool ib_Respuesta = io_dllSRI.FirmaComprobanteElectronico(); //ESTE FIRMA EL DOCUMENTO
                string sle_temp; //variable para guardar lo que debe ir en control sle_firxml
                if (ib_Respuesta)
                {
                    //sle_firxml.Text = is_DirFir + is_ArchivoXML
                    //txtSle_firxml.Text = io_dllSRI.ArchivoXMLFirmado;
                    sle_temp = is_DirFir + is_ArchivoXML;
                    sle_temp = io_dllSRI.ArchivoXMLFirmado;
                }
                else
                {
                    //txtSle_firxml.Text = "ERROR archivo no pudo ser firmado";
                    sle_temp = "ERROR archivo no pudo ser firmado";
                    objError.Mensaje = "❗ *03. " + DateTime.Now + " No pudo ser firmado el archivo. Intentelo nuevamente.";
                }
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            return objError;
        }
        protected clsError f_EnviarSRI()
        {
            clsError objError = new clsError();
            return objError;
            String s_SriMsj, s_EstEnv, s_ClaAcc, s_ArcXml;
            String s_CarpetayNombreArchivoXML, s_Estado, s_numaut, s_MenArc;
            Boolean b_BanPru, b_BanPro, b_File;
            Double l, li, posi, i, r_NumCom;
            String s_NomArc, s_Linea, s_AuxLin;
            DateTime d_fecaut;
            Boolean b_ResSer;

            b_BanPru = false;
            b_BanPro = true;

            if (is_BanSri.Equals("1"))
            {
                b_BanPru = true;
                b_BanPro = false;
            }

            DataTable dt = dtEncPV;
            s_ClaAcc = dt.Rows[0].Field<string>("srie_claacc");
            s_CarpetayNombreArchivoXML = is_DirFir + is_ArchivoXML;

            s_Estado = dt.Rows[0].Field<string>("srie_estado");
            if (String.IsNullOrEmpty(s_Estado)) s_Estado = "";
            //sle_envxml.Text = s_Estado ???
            r_NumCom = 0;

            if (s_Estado.Equals("RECIBIDA"))
            {
                s_EstEnv = dt.Rows[0].Field<string>("srie_estenv");
                sriUtilidades.main io_dllSRI = new main();
                //long il_ResDllSri = io_dllSRI.ConnectToNewObject("sriUtilidades.ComprobantesElectronicos")
                if (String.IsNullOrEmpty(s_EstEnv)) s_EstEnv = "";
                //VALIDA QUE LA LIBRERIA ESTE ACTIVADA
                //IF il_ResDllSri<> 0 THEN
                //MessageBox("...... E R R O R  !!!!!!", "LIBRERIA NO AUTORIZADA", StopSign!)
                //commit;
                //destroy io_dllSRI;
                //return 0;
                //END IF
                if (is_BanSri.Equals("1")) io_dllSRI.webServiceURL = "https://celcer.sri.gob.ec";
                if (is_BanSri.Equals("1")) io_dllSRI.webServiceURL = "https://cel.sri.gob.ec";
                if (is_Esqsri.Equals("1"))
                {
                    io_dllSRI.webMethodAutorizacion = "comprobantes-electronicos-ws/AutorizacionComprobantes";
                    io_dllSRI.webMethodRecepcion = "comprobantes-electronicos-ws/RecepcionComprobantes";
                }
                else
                {
                    io_dllSRI.webMethodAutorizacion = "comprobantes-electronicos-ws/AutorizacionComprobantesOffline";
                    io_dllSRI.webMethodRecepcion = "comprobantes-electronicos-ws/RecepcionComprobantesOffline";
                }

                io_dllSRI.loteMasivo = false; // Si se envia en lote masivo
                io_dllSRI.Usa_Proxy = false; // Si utiliza proxy para el acceso al internet
                io_dllSRI.HTTP_Proxy = "";	// Proxy (Ej. 192.168.0.4)
                io_dllSRI.HTTP_Proxy_Puerto = "";	// Puerto del Proxy (Ej. 3128)
                io_dllSRI.rootComprobantesElectronicos = is_DirCom + "\"";  // "c:\sriComprobantes\"

                //CONSULTA SI EXISTE ARCHIVO ENVIADO CON LA CLAVE ACCESO
                io_dllSRI.ClaveAcceso = s_ClaAcc; //Clave de acceso a consultar
                io_dllSRI.ArchivoXMLAutorizado = "";
                io_dllSRI.estadoConsultado = "";
                io_dllSRI.ConsultaClaveAcceso();
                Double r_AuxCom;
                String s_AuxEst;

                r_AuxCom = io_dllSRI.numeroComprobantesRespuestaAutorizacion;  // Devuelve cuantos comprobantes existen subidos con la clave de acceso, por lo general 1
                s_AuxEst = io_dllSRI.estadoConsultado;                        // Estado de la clave de acceso (AUTORIZADO, NO AUTORIZADO, ERROR CONEXION)
                s_numaut = io_dllSRI.numeroAutorizacion;                      // Si fue autorizado, devuelve el numero de autorizacion
                d_fecaut = DateTime.Parse(io_dllSRI.fechaAutorizacion);					  // Si fue autorizado, devuelve la fecha de autorizacion

                if (s_AuxEst.Equals("AUTORIZADO"))
                {
                    dt.Rows[0]["srir_estado"] = "AUTORIZADO";
                    dt.Rows[0]["srir_MENSAJ"] = "Pendiente ya autorizada";
                    dt.Rows[0]["srir_fecaut"] = d_fecaut.ToString();
                    dt.Rows[0]["srir_numaut"] = s_numaut;
                    dt.Rows[0]["srie_estado"] = "RECIBIDA";
                    dt.Rows[0]["srie_MENSAJ"] = "Pendiente ya recibida";
                    //istr_aux.dw_par03.Update() ???
                    //sle_envxml.Text = 'RECIBIDA'
                }

                io_dllSRI.ArchivoXMLFirmado = s_CarpetayNombreArchivoXML; //"c:\sriComprobantes\firmados\FAC_001-003-000000013.xml"
                io_dllSRI.ArchivoXMLMensajesRecibidos = "";
                s_SriMsj = io_dllSRI.WSRecepcionComprobantes(); // Devuelve RECIBIDA, DEVUELTA, ERROR CONEXION

                //IF il_ResDllSri = 0 THEN
                dt.Rows[0]["srie_estenv"] = "ENVIADO";
                dt.Rows[0]["srie_fecenv"] = DateTime.Now.ToString();
                dt.Rows[0]["srie_usuenv"] = dtEncPV.Rows[0]["codusu"].ToString();

                s_SriMsj = s_SriMsj.Trim();
                if (s_SriMsj.Equals("RECIBIDA"))
                {
                    //sle_envxml.Text = s_SriMsj
                    dt.Rows[0]["srie_estado"] = s_SriMsj;
                }
                else
                {
                    //sle_envxml.Text = s_SriMsj
                    dt.Rows[0]["srie_estado"] = s_SriMsj;
                    s_NomArc = is_DirCom + "'\recibidos\'" + is_ArchivoXML;
                    b_File = File.Exists(s_NomArc); //OJO HAY QUE PROBAR
                    if (b_File)
                    {
                        //l = FileLength64(s_NomArc) ???

                        //li = FileOpen(s_NomArc, LineMode!)

                        //IF li< 32767 THEN

                        //          FileRead(li, s_Linea)

                        //END IF

                        //s_AuxLin = s_Linea

                        //DO WHILE FileRead(li, s_Linea) <> -100

                        //    s_AuxLin = trim(s_AuxLin) + trim(s_Linea)

                        //LOOP

                        //FileClose(li)
                    }
                }
            }
        }
    }
}