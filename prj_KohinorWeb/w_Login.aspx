﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_Login.aspx.cs" Inherits="prj_KohinorWeb.w_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login - Kohinor Web</title>
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
     <%-- Estilo de la pagina Core UI --%>
    <link href="CoreUI/css/style.css" rel="stylesheet" />

    <%-- Estilo del side nav extra --%>
    <link href="CoreUI/css/style2.css" rel="stylesheet" />

    <%-- Core Ui iconos --%>
    <link href="CoreUI/vendors/@coreui/icons/css/brand.min.css" rel="stylesheet" />
    <link href="CoreUI/vendors/@coreui/icons/css/flag.min.css" rel="stylesheet" />
    <link href="CoreUI/vendors/@coreui/icons/css/free.min.css" rel="stylesheet" />


    <!-- CoreUI and necessary plugins-->
    <script src="CoreUI/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
    <script src="DataTables/jquery-3.4.1.min.js"></script> <!--OJO!!! ESTE ES PARA Q METODO .blur() FUNCIONE-->
    <!--[if IE]><!-->
    <script src="CoreUI/vendors/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
    <!-- Plugins and scripts required by this view-->
    <script src="CoreUI/vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js"></script>
    <script src="CoreUI/vendors/@coreui/utils/js/coreui-utils.js"></script>
    <script src="CoreUI/js/main.js"></script>

    <%-- Core Ui script --%>
     <script>
         window.dataLayer = window.dataLayer || [];
         function gtag() {
             dataLayer.push(arguments);
         }
         gtag('js', new Date());
         // Shared ID
         gtag('config', 'UA-118965717-3');
         // Bootstrap ID
         gtag('config', 'UA-118965717-5');
    </script>
   
    
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
        <script type="text/javascript">
            function f_ModalAutenticacion() {
                $("#btnModalAutenticacion").click();
            }
            function f_ModalError() {
                $("#btnModalError").click();
            }
        </script>
        <!--Boton AUTENTICACION -->
        <button id="btnModalAutenticacion" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#ModalAutenticacion"></button>
        <!--Boton ERROR -->
        <button id="btnModalError" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#ModalError"></button>
        <div class="c-app flex-row align-items-center fondoLogin">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card-group">
                            <div class="card p-4">
                                <div class="card-body">
                                    <div class="text-center">
                                        <asp:Image ID="Image1" src="Icon\Menu125\kohinor.png" runat="server" Width="75" />
                                    </div>
                                    <hr />

                                    <h2>
                                        <asp:Label ID="lblIncioSesion" runat="server" Text=""></asp:Label></h2>
                                    <p class="text-muted">
                                        <asp:Label ID="lblIngresacuenta" runat="server" Text=""></asp:Label></p>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <svg class="c-icon">
                                                    <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-layers"></use>
                                                </svg></span>
                                        </div>
                                        <asp:TextBox ID="txtRucemp" class="form-control form-control-user" placeholder="RUC" runat="server" AutoPostBack="true" OnTextChanged="txtRucemp_TextChanged" TextMode="Number"></asp:TextBox>
                                    </div>
                                    <div class="input-group mb-3">
                                        <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="input-group mb-3">
                                        <%--<asp:DropDownList ID="ddlSerie" runat="server" CssClass="form-control" DataSourceID="sqldsSerie" DataTextField="sersec" DataValueField="sersec"></asp:DropDownList>--%>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <svg class="c-icon">
                                                    <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-user"></use>
                                                </svg></span>
                                        </div>
                                        <asp:TextBox ID="txtUsuario" runat="server" class="form-control form-control-user"></asp:TextBox>

                                    </div>
                                    <div class="input-group mb-4">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <svg class="c-icon">
                                                    <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-lock-locked"></use>
                                                </svg></span>
                                        </div>
                                        <input type="password" class="form-control form-control-user" id="txtPassword" placeholder="" runat="server" />
                                    </div>
                                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                                    <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green"></asp:Label>
                                    <div class="row">
                                        <div class="col-6">
                                            <asp:Button ID="btnLogin" runat="server" Text="" class="btn btn-primary px-4" OnClick="btnLogin_Click" />
                                        </div>
                                        <div class="col-6 text-right">
                                            <asp:Button class="btn btn-outline-success" ID="btnRegistrarse" type="button" runat="server" Text="Registrarse" PostBackUrl="~/w_Registrar.aspx" />
                                            <asp:Button class="btn btn-link px-0" ID="btnRecuperar" type="button" runat="server" Text="" PostBackUrl="~/w_OlvidoClave.aspx" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card text-white bg-primary d-md-down-none">
                                <asp:Image ID="imgLogo" Style="width: 100%; height: 100%;" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--i SQL DATASOURCES-->
        <asp:SqlDataSource ID="sqldsPalabraClave" runat="server" ConnectionString="<%$ ConnectionStrings:Default %>" SelectCommand="SELECT [palcla] FROM [seg_configuracion]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsConfiguracion" runat="server" ConnectionString="<%$ ConnectionStrings:Default %>" SelectCommand="SELECT [usudes], [usupro], [usuadm] FROM [seg_configuracion]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsEmpresa" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_EMPRESA" SelectCommandType="StoredProcedure">
            <%--<SelectParameters>
              <asp:Parameter DefaultValue="Select" Name="Action" Type="String" />
              <asp:SessionParameter DefaultValue="" Name="RucEmp" SessionField="gs_Rucemp" Type="String" />
          </SelectParameters>--%>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsSerie" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [sersec] FROM [seg_usuario_serie]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsPaginaInicial" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT ref002 FROM [seg_modulo_niv002] WHERE (([codmod] = @codmod) AND ([niv001] = @niv001) AND ([niv002] = @niv002))">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="3" Name="codmod" SessionField="gs_Codmod" Type="Int32" />
                <asp:Parameter DefaultValue="2" Name="niv001" Type="Int32" />
                <asp:Parameter DefaultValue="1" Name="niv002" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <!--f SQL DATASOURCES-->

        <!-- Modal Autenticacion -->
        <div class="modal fade" id="ModalAutenticacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-primary modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Registro de Computador</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-secondary" role="alert">
                            <h4 class="alert-heading">Mensaje</h4>
                            
                            <ul>
                                <li>Detectamos que ingresaste a nuestra página web desde una dirección IP diferente a la utilizada anteriormente.</li>
                                <li>Para continuar responde la pregunta de autenticación y presiona Aceptar, al hacerlo, aceptas expresamente el registro de la presente dirección IP.</li>
                                <li>Te recomendamos hacer este proceso en el computador con el que realizas transacciones habitualmente.</li>
                                <li>Por tu seguridad no se permite el acceso desde dispositivos no registrados.</li>
                            </ul>
                            <p></p>
                            <hr/>
                            <p class="mb-0">
                                <strong>Fecha: </strong>
                                <asp:Label ID="lblModalAutFecha" runat="server"></asp:Label>
                            </p>
                        </div>
                        <p>Nombre de tu primera mascota.</p>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <svg class="c-icon">
                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-lock-locked"></use>
                                    </svg></span>
                            </div>
                            <asp:TextBox ID="txtRespuestaAutenticacion" runat="server" class="form-control"></asp:TextBox>
                        </div>                       
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                        <asp:Button ID="btnAceptarModal" class="btn btn-primary" runat="server" Text="Aceptar" OnClick="btnAceptarModal_Click" />
                    </div>
                </div>
                <!-- /.modal-content-->
            </div>
            <!-- /.modal-dialog-->
        </div>

        <!-- Modal Danger (Error) -->
        <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-danger modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Error</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">N° Error</span>
                            </div>
                            <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                        </div>

                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Mensaje</h4>
                            <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                            <p>
                                <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                            </p>
                            <hr>
                            <p class="mb-0">
                                <strong>Donde: </strong>
                                <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                            </p>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Objeto</span>
                            </div>
                            <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Evento</span>
                            </div>
                            <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Línea</span>
                            </div>
                            <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                        <button class="btn btn-danger" type="button">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content-->
            </div>
            <!-- /.modal-dialog-->
        </div>

    </form>
</body>
</html>
