﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_Login : System.Web.UI.Page
    {
        protected cls_seg_Usuario objUsuario = new cls_seg_Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Clear();
                try
                {
                    DataSourceSelectArguments args1 = new DataSourceSelectArguments();
                    DataView view1 = (DataView)sqldsPalabraClave.Select(args1);
                    DataTable dt1 = view1.ToTable();
                    string s_palcla = dt1.Rows[0]["palcla"].ToString();
                    objUsuario = objUsuario.f_Usuario_Base_Buscar(f_GetIp());
                    //objUsuario = objUsuario.f_Usuario_Base_Buscar("192.168.100.8");
                    if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
                    {
                        Session.Add("gs_Rucemp", objUsuario.Rucemp);
                        f_Conexion_Inicial(objUsuario, s_palcla);
                    }
                    else
                    {
                        lblError.Text = objUsuario.Error.Mensaje;
                    }
                }
                catch (Exception ex)
                {
                    clsError objError = new clsError();
                    objError = objError.f_ErrorControlado(ex);
                    Server.ClearError();
                    Response.Redirect(String.Format("w_seg_Error500.aspx?errorMessage={0}&fileName={1}&lineNumber={2}&columnNumber={3}&methodName={4}&fecha={5}", objError.Mensaje, objError.Donde, objError.Linea, objError.Objeto, objError.Evento, objError.Fecha));
                }                

                //Aqui se asigna el nombre de cada uno de los elementos segun el idioma base de datos 
                lblIncioSesion.Text = "Iniciar Sesión";
                lblIngresacuenta.Text = "Ingresa a tu cuenta";
                txtUsuario.Attributes.Add("placeholder", "Usuario");
                txtPassword.Attributes.Add("placeholder", "Contraseña");
                btnLogin.Text = "Login";
                btnRecuperar.Text = "Olvidaste tu contraseña?";
            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected string f_GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                ip = "HTTP_X_FORWARDED_FOR IS NULL";
            }
            return ip;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session.Add("gs_SuperAdmin", "N");
            string s_pcip = f_GetIp();
            objUsuario =  objUsuario.f_BuscarUsuarioLogin(txtRucemp.Text, txtUsuario.Text, txtPassword.Value, s_pcip, "");
            if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
            {
                DataTable dt = objUsuario.dtUsuario;
                switch (dt.Rows[0]["existe"].ToString().Trim())
                {
                    case "3": //no coincide Clausu
                        lblError.Text = "*Credenciales no concuerdan.";
                        lblError.Visible = true;
                        break;
                    case "2": //no coincide el Codus1
                        if (f_AdminSearch())
                        {
                            Session["gs_SuperAdmin"] = "Y";
                            f_AdminLogin(txtUsuario.Text, txtPassword.Value, s_pcip); //con base WEB
                        }
                        else
                        {
                            lblError.Text = "*Credenciales no concuerdan.";
                            lblError.Visible = true;
                        }
                        break;
                    case "1": //Coincide usu y clausu...igual busca si es SuperAdmin
                        if (f_AdminSearch())
                        {
                            Session["gs_SuperAdmin"] = "Y";
                            f_AdminLogin(txtUsuario.Text, txtPassword.Value, s_pcip); //con base WEB
                        }
                        else
                        {
                            f_generarObjUsuario(dt); //con base regular
                        }                        
                        break;
                    case "0": //LANAZA MODAL DE REGISTRO DE NUEVA IP
                        lblError.Text = "❗ Máquina no registrada.";
                        lblError.Visible = true;

                        lblModalAutFecha.Text = DateTime.Now.ToString();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalAutenticacion();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        break;
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "❗ *01. " + objUsuario.Error.Mensaje;
                f_ErrorNuevo(objUsuario.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

            if (String.IsNullOrEmpty(lblError.Text))
            {
                f_Redirect(); //para ingresar la APP       
            }
        }
        protected void f_Conexion_Inicial(cls_seg_Usuario objUsuario, string s_palclaEncrip)
        {
            cls_seg_Encriptacion objEncriptacion = new cls_seg_Encriptacion();
            string s_PalCla = objEncriptacion.f_seg_Encriptar(s_palclaEncrip);
            objUsuario = objUsuario.f_Desencriptar_Base(objUsuario, s_PalCla);
            f_Variables_Incial(objUsuario);
        }
        protected void f_Variables_Incial(cls_seg_Usuario objUsuario) //conecta con la base y asigna textos iniciales de Login
        {
            string name = "SQLCA";
            string dataSource = clsGlobalVariablesClass.ServerIP;
            string initialCatalog = objUsuario.Nombas;
            string userId = objUsuario.Usubas;
            string password = objUsuario.Clabas;
            string ConnectionString = objUsuario.f_Conexion_Base(name, dataSource, initialCatalog, userId, password);

            Session.Clear();
            try
            {
                Session.Add("gs_Rucemp", objUsuario.Rucemp);
                //Pasar info de sql data source a una tabla
                sqldsEmpresa.ConnectionString = ConnectionString;
                sqldsEmpresa.DataBind();
                DataSourceSelectArguments args = new DataSourceSelectArguments();
                DataView view = (DataView)sqldsEmpresa.Select(args); //procedimiento almacenado
                DataTable dt = view.ToTable();

                //llenar ddl con procedimiento almacenado
                dt.Columns.Add("ColumnaConTodo", typeof(string), "nomemp");
                Session.Add("gs_Nomemp", dt.Rows[0]["ColumnaConTodo"]); //para Nombre Completo
                ddlEmpresa.DataSource = dt;
                ddlEmpresa.DataTextField = "ColumnaConTodo";
                //ddlEmpresa.DataValueField = "codemp";
                ddlEmpresa.DataBind();

                imgLogo.ImageUrl = clsGlobalVariablesClass.ImageDirectory + dt.Rows[0].Field<string>("logsis").Trim();
                imgLogo.DataBind();
                lblError.Visible = false;
                txtRucemp.Text = objUsuario.Rucemp;
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = "❗ *02. " + ex.Message;
            }
        }
        protected void txtRucemp_TextChanged(object sender, EventArgs e)
        {
            Session.Clear();
            DataSourceSelectArguments args1 = new DataSourceSelectArguments();
            DataView view1 = (DataView)sqldsPalabraClave.Select(args1);
            DataTable dt1 = view1.ToTable();

            cls_seg_Encriptacion objEncriptacion = new cls_seg_Encriptacion();
            string s_PalCla = objEncriptacion.f_seg_Encriptar(dt1.Rows[0]["palcla"].ToString());

            Session.Add("gs_Ip", f_GetIp());
            objUsuario = objUsuario.f_Usuario_EmpresaBase(txtRucemp.Text, s_PalCla);
            objUsuario.Rucemp = txtRucemp.Text;
            if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
            {
                lblError.Visible = false;
                f_Variables_Incial(objUsuario);
            }
            else
            {
                ddlEmpresa.Items.Clear();
                lblError.Visible = true;
                lblError.Text = "❗ Empresa no encontrada.";
            }
        }
        protected bool f_AdminSearch()//Busca si el usuario puede ingresar con WEB
        {
            DataSourceSelectArguments args1 = new DataSourceSelectArguments();
            DataView view1 = (DataView)sqldsConfiguracion.Select(args1);
            DataTable dt1 = view1.ToTable();
            string s_usudes = dt1.Rows[0]["usudes"].ToString();
            string s_usupro = dt1.Rows[0]["usupro"].ToString();
            string s_usuadm = dt1.Rows[0]["usuadm"].ToString();
            if (txtUsuario.Text.Equals(s_usupro) || txtUsuario.Text.Equals(s_usuadm) || txtUsuario.Text.Equals(s_usudes)) return true;
            else return false;
        }
        protected void f_AdminLogin(string s_usuario, string s_password, string s_pcip)
        {
            lblError.Text = "";
            objUsuario = objUsuario.f_BuscarUsuarioAdminLogin(txtUsuario.Text, txtPassword.Value, s_pcip, "");
            if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
            {
                DataTable dt = objUsuario.dtUsuario;
                switch (dt.Rows[0]["existe"].ToString().Trim())
                {
                    case "2":
                        lblError.Text = "**Credenciales no concuerdan.";
                        lblError.Visible = true;
                        break;
                    case "1":
                        f_generarObjUsuario(dt);
                        break;
                    case "0":
                        lblError.Text = "❗ **Máquina no registrada.";
                        lblError.Visible = true;

                        lblModalAutFecha.Text = DateTime.Now.ToString();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalAutenticacion();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        break;
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "❗ *01. " + objUsuario.Error.Mensaje;
                f_ErrorNuevo(objUsuario.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void f_Redirect()
        {
            clsError objError = new clsError();
            objError = objUsuario.f_Usuario_EntSal(objUsuario, "0");
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                Session.Add("gs_Modo", "L"); //Para light/dark mode
                Session.Add("gs_CodEmp", "01");
                Session.Add("gs_Error", "");
                Session.Add("gs_Nomemp", ddlEmpresa.SelectedValue.ToString()); //para Nombre Completo
                Session.Add("gs_objUsuario", objUsuario);
                Session.Add("gs_Niv001", 0);
                Session.Add("gs_Codmod", 3); //Ventas
                Session.Add("gs_Nommod", "Ventas ( Cuentas por Cobrar )"); //Ventas
                sqldsPaginaInicial.DataBind();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsPaginaInicial.Select(args2); //procedimiento almacenado
                DataTable dt2 = view2.ToTable();
                //Response.Redirect(dt2.Rows[0].Field<string>("ref002"));
                Response.Redirect("w_vc_Pedido.aspx");
                //Response.Redirect("w_Escritorio.aspx");
                //Response.Redirect("w_testform.aspx");

            }
            else
            {
                lblError.Text = objError.Mensaje;
                //f_ErrorNuevo(objError);
                //ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

        }
        protected void f_generarObjUsuario(DataTable dt)
        {
            objUsuario.Rucemp = dt.Rows[0]["rucemp"].ToString().Trim();
            objUsuario.Codus1 = dt.Rows[0]["codus1"].ToString().Trim();
            objUsuario.Pcnom = dt.Rows[0]["pcnom"].ToString().Trim();
            objUsuario.Pcip = dt.Rows[0]["pcip"].ToString().Trim();
            objUsuario.Pcmac = dt.Rows[0]["pcmac"].ToString().Trim();
            Session.Add("gs_CodUs1", dt.Rows[0]["codus1"].ToString().Trim());
            Session.Add("gs_UsuIng", dt.Rows[0]["codus1"].ToString().Trim());
            Session.Add("gs_SerSec", dt.Rows[0]["sersec"].ToString().Trim());
            Session.Add("gs_CodVen", dt.Rows[0]["codven"].ToString().Trim());
            Session.Add("gs_CodAlm", dt.Rows[0]["codalm"].ToString().Trim());
            Session.Add("gs_CodSuc", dt.Rows[0]["codsuc"].ToString().Trim());
            Session.Add("gs_Rucemp", dt.Rows[0]["rucemp"].ToString().Trim());
            Session.Add("gs_Ip", dt.Rows[0]["pcip"].ToString().Trim());
        }
        protected void btnAceptarModal_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtRucemp.Text) || String.IsNullOrEmpty(txtUsuario.Text))
            {
                lblError.Text = "❗ Debe Llenar RUC y Usuario para cambiar de IP.";
            }
            else
            {
                lblError.Text = "";
                lblError.Visible = false;
                objUsuario.Rucemp = txtRucemp.Text;
                objUsuario.Codus1 = txtUsuario.Text;
                objUsuario.Pcip = f_GetIp();

                clsError objError = objUsuario.f_Usuario_RegistroIP(objUsuario);
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    if (Session["gs_SuperAdmin"].ToString().Equals("Y")) //usu admin de WEB
                    {
                        objUsuario = objUsuario.f_BuscarUsuarioAdminLogin(txtUsuario.Text, "", objUsuario.Pcip, "RegistroIP");
                        if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
                        {
                            DataTable dt = objUsuario.dtUsuario;
                            f_generarObjUsuario(dt);
                        }
                        else
                        {
                            f_ErrorNuevo(objUsuario.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                    else //usu comun
                    {
                        objUsuario = objUsuario.f_BuscarUsuarioLogin(txtRucemp.Text, txtUsuario.Text, "", objUsuario.Pcip, "RegistroIP");
                        if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
                        {
                            DataTable dt = objUsuario.dtUsuario;
                            f_generarObjUsuario(dt);
                        }
                        else
                        {
                            f_ErrorNuevo(objUsuario.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                    f_Redirect(); //para ingresar la APP
                }                    
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
        }
    }
}