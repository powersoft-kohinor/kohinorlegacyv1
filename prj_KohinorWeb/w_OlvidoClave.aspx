﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_OlvidoClave.aspx.cs" Inherits="prj_KohinorWeb.w_OlvidoClave" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Olvido Contraseña - Kohinor Web</title>
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
     <%-- Estilo de la pagina Core UI --%>
    <link href="CoreUI/css/style.css" rel="stylesheet" />

    <%-- Estilo del side nav extra --%>
    <link href="CoreUI/css/style2.css" rel="stylesheet" />

    <%-- Core Ui iconos --%>
    <link href="CoreUI/vendors/@coreui/icons/css/brand.min.css" rel="stylesheet" />
    <link href="CoreUI/vendors/@coreui/icons/css/flag.min.css" rel="stylesheet" />
    <link href="CoreUI/vendors/@coreui/icons/css/free.min.css" rel="stylesheet" />


    <!-- CoreUI and necessary plugins-->
    <script src="CoreUI/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
    <script src="DataTables/jquery-3.4.1.min.js"></script> <!--OJO!!! ESTE ES PARA Q METODO .blur() FUNCIONE-->
    <!--[if IE]><!-->
    <script src="CoreUI/vendors/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
    <!-- Plugins and scripts required by this view-->
    <script src="CoreUI/vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js"></script>
    <script src="CoreUI/vendors/@coreui/utils/js/coreui-utils.js"></script>
    <script src="CoreUI/js/main.js"></script>

    <%-- Core Ui script --%>
     <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        // Shared ID
        gtag('config', 'UA-118965717-3');
        // Bootstrap ID
        gtag('config', 'UA-118965717-5');
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
        <script type="text/javascript">
            $(document).ready(function () {
                SetButtonStatus();
            });
            function f_ModalError() {
                $("#btnModalError").click();
            }
            //bloquear btnAceptar si no escribe en email o ruc
             function SetButtonStatus() {
                 var email = document.getElementById('<%=txtEmail.ClientID%>').value;           
                 var rucemp = document.getElementById('<%=txtRucemp.ClientID%>').value;           
                 if (email && rucemp)
                     document.getElementById('<%=btnAceptar.ClientID%>').disabled = false;
                   else
                     document.getElementById('<%=btnAceptar.ClientID%>').disabled = true;
             }

        </script>

        <!--Boton ERROR -->
        <button id="btnModalError" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#ModalError"></button>
        <div class="c-app flex-row align-items-center fondoLogin">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="card mx-4">
                            <div class="card-body p-4">
                                <h1>Contraseña Olvidada</h1>
                                <p class="text-muted">Si olvidó su contraseña es necesario que se identifique para asignarle una nueva.</p>                                
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <svg class="c-icon">
                                                <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-layers"></use>
                                            </svg></span>
                                    </div>
                                    <asp:TextBox ID="txtRucemp" class="form-control form-control-user" placeholder="RUC" runat="server" AutoPostBack="true" OnTextChanged="txtRucemp_TextChanged" TextMode="Number"></asp:TextBox>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control form-control-user" id="txtNomemp" placeholder="Nombre Empresa" runat="server" readonly="true" />
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <svg class="c-icon">
                                                <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-envelope-closed"></use>
                                            </svg></span>
                                    </div>
                                    <asp:TextBox ID="txtEmail" class="form-control form-control-user" placeholder="Email" runat="server" onkeyup="SetButtonStatus();"></asp:TextBox>
                                </div>
                                
                                <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green"></asp:Label>
                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                            </div>

                            <div class="card-footer p-4">
                                <div class="row">
                                    <div class="col-6">
                                        <asp:Button ID="btnLogin" runat="server" class="btn btn-block btn-outline-dark" Text="Regresar" PostBackUrl="~/w_Login.aspx" formnovalidate="true" />
                                    </div>
                                    <div class="col-6">
                                        <asp:Button ID="btnAceptar" class="btn btn-block btn-primary" runat="server" Text="Aceptar" OnClick="btnAceptar_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      <!--i SQL DATASOURCES-->
        <asp:SqlDataSource ID="sqldsPalabraClave" runat="server" ConnectionString="<%$ ConnectionStrings:Default %>" SelectCommand="SELECT [palcla] FROM [seg_configuracion]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsEmpresa" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_EMPRESA" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsExisteEmail" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codus1],[usmail] FROM [seg_usuario] WHERE (([rucemp] = @rucemp) AND ([usmail] = @usmail))">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtRucemp" Name="rucemp" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtEmail" Name="usmail" PropertyName="Text" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
      <!--f SQL DATASOURCES-->

         <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    </form>
</body>
</html>


