﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection.Emit;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_OlvidoClave : System.Web.UI.Page
    {
		//https://www.youtube.com/watch?v=JVAuGygtWYM
		protected cls_seg_Usuario objUsuario = new cls_seg_Usuario();
        protected void Page_Load(object sender, EventArgs e)
        {

		}

		protected void f_ErrorNuevo(clsError objError)
		{
			lblModalFecha.Text = objError.Fecha.ToString();
			txtModalNum.Text = objError.NoError;
			lblModalError.Text = objError.Mensaje;
			lblModalPagina.Text = objError.Donde;
			txtModalObjeto.Text = objError.Objeto;
			txtModalEvento.Text = objError.Evento;
			txtModalLinea.Text = objError.Linea;
		}
		protected string f_GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                ip = "HTTP_X_FORWARDED_FOR IS NULL";
            }
            return ip;
        }

        protected string f_Variables_Incial() //conecta con la base y asigna textos iniciales de Login
        {
            string ConnectionString = "";
            Session.Clear();
            DataSourceSelectArguments args1 = new DataSourceSelectArguments();
            DataView view1 = (DataView)sqldsPalabraClave.Select(args1);
            DataTable dt1 = view1.ToTable();

            cls_seg_Encriptacion objEncriptacion = new cls_seg_Encriptacion();
            string s_PalCla = objEncriptacion.f_seg_Encriptar(dt1.Rows[0]["palcla"].ToString());

            Session.Add("gs_Ip", f_GetIp());
            objUsuario = objUsuario.f_Usuario_EmpresaBase(txtRucemp.Text, s_PalCla);
            objUsuario.Rucemp = txtRucemp.Text;
            if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
            {
                lblError.Visible = false;
                string name = "SQLCA";
                string dataSource = clsGlobalVariablesClass.ServerIP;
                string initialCatalog = objUsuario.Nombas;
                string userId = objUsuario.Usubas;
                string password = objUsuario.Clabas;
                ConnectionString = objUsuario.f_Conexion_Base(name, dataSource, initialCatalog, userId, password);

                Session.Clear();
                try
                {
                    //Pasar info de sql data source a una tabla
                    sqldsEmpresa.ConnectionString = ConnectionString;
                    sqldsEmpresa.DataBind();
                    DataSourceSelectArguments args = new DataSourceSelectArguments();
                    DataView view = (DataView)sqldsEmpresa.Select(args); //procedimiento almacenado
                    DataTable dt = view.ToTable();

                    //llenar txt con procedimiento almacenado
                    txtNomemp.Value = dt.Rows[0].Field<string>("nomemp").Trim();
                    txtRucemp.Text = objUsuario.Rucemp;
                    lblError.Visible = false;                    
                }
                catch (Exception ex)
                {
                    lblError.Visible = true;
                    lblError.Text = "❗ *02. " + ex.Message;
                }
            }
            else
            {
                txtNomemp.Value = "";
                lblError.Visible = true;
                lblError.Text = "❗ Empresa no encontrada.";
            }
            return ConnectionString;
        }

        protected void txtRucemp_TextChanged(object sender, EventArgs e)
        {
            f_Variables_Incial();
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
			string ConnectionString = f_Variables_Incial();
			sqldsExisteEmail.ConnectionString = ConnectionString;
			sqldsExisteEmail.DataBind();
			DataSourceSelectArguments args = new DataSourceSelectArguments();
			DataView view = (DataView)sqldsExisteEmail.Select(args); //procedimiento almacenado
			DataTable dt = view.ToTable();
			if (dt.Rows.Count > 0)
			{
				objUsuario.Codus1 = dt.Rows[0].Field<string>("codus1").ToString();
                objUsuario.Clausu = "123"; // GeneratePassword.Run();
				f_CambioPassword(objUsuario);
			}
			else
			{
				lblError.Visible = true;
				lblError.Text = "❗ Cuenta no registrada.";
			}
		}

		protected void f_CambioPassword(cls_seg_Usuario objUsuario)
        {
			clsError objError = objUsuario.f_Usuario_Clave(objUsuario); //guarda pass encriptado
			if (String.IsNullOrEmpty(objError.Mensaje))
			{
				lblError.Text = "";
				//SendEmail(txtEmail.Text, objUsuario.Codus1, objUsuario.Clausu);
			}
			else
			{
				f_ErrorNuevo(objError);
				ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
			}
		}

		private void SendEmail(string s_usmail, string codus1, string clausu)
		{			
			//Fetching Settings from WEB.CONFIG file.
			string emailSender = ConfigurationManager.AppSettings["emailsender"].ToString();
			string emailSenderPassword = ConfigurationManager.AppSettings["password"].ToString();
			string emailSenderHost = ConfigurationManager.AppSettings["smtpserver"].ToString();
			int emailSenderPort = Convert.ToInt16(ConfigurationManager.AppSettings["portnumber"]);
			Boolean emailIsSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSSL"]);
			string html = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
                <html xmlns=""http://www.w3.org/1999/xhtml"" lang=""en-GB"">
                <head>
                  <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
                  <title>Facturación Electrónica - Kohinor Web</title>
                  <meta name=""viewport"" content=""width=device-width, initial-scale=1.0""/>
                  <style type=""text/css"">
                    a[x-apple-data-detectors] {color: inherit !important;}
                  </style>
                </head>
                <body style=""margin: 0; padding: 0;"">
                  <table role=""presentation"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
                    <tr>
                      <td style=""padding: 20px 0 30px 0;"">

                <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"" style=""border-collapse: collapse; border: 1px solid #cccccc;"">
                  <tr>
                    <td align=""center"" bgcolor=""#00445e"" style=""padding: 40px 0 30px 0;"">
                      <img src=""https://www.kohinorec.com/wp-content/uploads/2020/08/Logo-Kohinor-2.png"" alt=""Kohinor Web."" width=""300"" height=""95"" style=""display: block;"" />
                    </td>
                  </tr>
                  <tr>
                    <td bgcolor=""#ffffff"" style=""padding: 40px 30px 40px 30px;"">
                      <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;"">
                        <tr>
                          <td style=""color: #153643; font-family: Arial, sans-serif; text-align:center;"">
                            <h1 style=""font-size: 24px; margin: 0;"">Actualización de contraseña</h1>
                          </td>
                        </tr>
                        <tr>
                          <td style=""color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;"">
                            <p style=""margin: 0;"">Se ha recibido la solicitud de actualización de contraseña de <strong>"+ codus1 + @"</strong>. A continuación se encuentra la nueva credencial para acceder a tu cuenta.</p>
                          </td>
                        </tr>
                        <tr>
                          <td bgcolor=""#00445e"" style=""color: #ffffff; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 25px 25px 25px 25px; text-align:center;"">
                            <p style=""margin: 0;""><strong>" + clausu + @"</strong></p>
                          </td>
                        </tr>
                        <tr>
                          <td style=""color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;"">
                            <p style=""margin: 0;"">
                                Fecha y Hora de Proceso: " + DateTime.Now + @"
                                <br>
                                Si no solicitó una actualización de contraseña puede ingnorar este correo. Solo la persona con acceso a este correo podrá visualizar la contraseña.
                            </p>
                          </td>
                        </tr>     
                        <tr>
                          <td class=""x_px-xs-20""
                            style=""padding-top:0px; padding-right:40px; padding-bottom:0; padding-left:40px; text-align:center"">
                            <span
                              style=""font-family: Arial; font-size: 8px; font-weight: normal; font-style: normal; font-stretch: normal; line-height: normal; letter-spacing: normal; text-align: center; color: rgb(204, 204, 204) !important;""
                              data-ogsc=""rgb(74, 74, 74)"">
                              Nota: La información y adjuntos contenidos en este mensaje electrónico son confidenciales y reservados. Esta información no debe ser usada, reproducida o divulgada por otras personas distintas a su(s) destinatario(s). Si Ud. no es el destinatario de este email, le solicitamos comedidamente eliminarlo. La organización no asume responsabilidad sobre información, opiniones o criterios contenidos en este e-mail que no esté relacionada con negocios oficiales de nuestra institución.
                              <br>
                              <br>
                              Disclaimer: The information and attachments contained in this electronic message are confidential and proprietary. This information should not be used, reproduced, or disclosed by anyone other than the addressee. If you are not the addressee of this email, we kindly ask you to delete it. The organization assumes no responsibility for information, opinions or opinions contained in this e-mail that are not related to official business of our institution.
                            </span>              
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td bgcolor=""#7a7a7a"" style=""padding: 30px 30px;"">
                        <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;"">
                        <tr>
                          <td style=""color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;"">
                            <p style=""margin: 0;"">© 2021 Kohinor. Todos los derechos reservados.<br/>
                            Powered by <a href=""#"" style=""color: #ffffff;"">POWERSOFT</a></p>
                          </td>
                          <td align=""right"">
                            <table border=""0"" cellpadding=""0"" cellspacing=""0"" style=""border-collapse: collapse;"">
                              <tr>
                                <td>
                                  <a href=""http://www.twitter.com/"">
                                    <img src=""https://assets.codepen.io/210284/tw.gif"" alt=""Twitter."" width=""38"" height=""38"" style=""display: block;"" border=""0"" />
                                  </a>
                                </td>
                                <td style=""font-size: 0; line-height: 0;"" width=""20"">&nbsp;</td>
                                <td>
                                  <a href=""http://www.twitter.com/"">
                                    <img src=""https://assets.codepen.io/210284/fb.gif"" alt=""Facebook."" width=""38"" height=""38"" style=""display: block;"" border=""0"" />
                                  </a>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                      </td>
                    </tr>
                  </table>
                </body>
                </html>";

			try
			{
				MailMessage objMailMsg = new MailMessage();
				objMailMsg.From = new MailAddress(emailSender, "KohinorWeb");
				objMailMsg.To.Add(s_usmail);
				objMailMsg.Subject = "Actualización de Contraseña";
				objMailMsg.Body = html;
				objMailMsg.IsBodyHtml = true;
				objMailMsg.Priority = MailPriority.Normal;

				SmtpClient sc = new SmtpClient(emailSenderHost, emailSenderPort); //Try port 587 instead of 465. Port 465 is technically deprecated.
				sc.Credentials = new NetworkCredential(emailSender, emailSenderPassword);
				sc.EnableSsl = emailIsSSL;
				sc.Send(objMailMsg);
				lblExito.Text = "✔️ Cambio de contraseña realizado. Verificar correo electrónico.";
			}
			catch (Exception ex)
			{
				clsError objError = new clsError();
				objError = objError.f_ErrorControlado(ex);
				f_ErrorNuevo(objError);
				ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
			}
		}
	}

	public class GeneratePassword
	{
		public static string Run()
		{
			var lettersUpper =
				Enumerable.Range('A', 'Z' - 'A' + 1)
				.Select(x => (char)x);

			var lettersLower =
				Enumerable.Range('a', 'z' - 'a' + 1)
				.Select(x => (char)x);

			var numbers =
				Enumerable.Range('0', '9' - '0' + 1)
				.Select(x => (char)x);

			//var characters =
			//	Enumerable.Range('!', '?' - '!' + 1)
			//	.Select(x => (char)x);

			var alphabet =
				lettersUpper
				.Union(lettersLower)
				.Union(numbers)
				//.Union(characters)
				.ToArray();

			Random rand = new Random();
			var password = string.Empty;

			for (int i = 0; i < 15; i++)
			{
				var idx = rand.Next(alphabet.Count());
				password += alphabet[idx];
			}

			return password;
		}
	}
}