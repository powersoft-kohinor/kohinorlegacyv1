﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_Principal : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            f_Seg_Usuario_Modulo();
            f_Seg_Usuario_Modulo_Config(); //para modulos Configuracion y Seguridades
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Para light/dark mode
                string s_modo;
                try
                {
                    s_modo = Session["gs_Modo"].ToString();
                }
                catch (Exception)
                {
                    s_modo = "L";
                }


                if (s_modo == "L")
                {
                    body.Attributes.Add("class", "c-app");
                    btnligth.Attributes.Add("style", "display: none;");
                    mySidenav.Attributes.Add("class", "sidenavImg");

                }
                else if (s_modo == "D")
                {
                    body.Attributes.Add("class", "c-app c-dark-theme");
                    btndark.Attributes.Add("style", "display: none;");
                    mySidenav.Attributes.Add("class", "sidenavImg dark-theme");

                }

                try
                {
                    lblEmp.Text = Session["gs_Nomemp"].ToString();
                    lblCodus1.Text = Session["gs_CodUs1"].ToString();
                    lblSersec.Text = Session["gs_SerSec"].ToString();
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblNomemp.Text = Session["gs_Nomemp"].ToString();
                }
                catch (Exception ex)
                {
                    lblError.Visible = true;
                    lblError.Text = "❗ *01. " + ex.Message;
                }
            }
        }


        protected void f_Seg_Usuario_Modulo()
        {
            try
            {
                DataSourceSelectArguments args = new DataSourceSelectArguments();
                DataView view = (DataView)sqldsModuloInicialMaster.Select(args); //procedimiento almacenado
                DataTable dtInicial = view.ToTable(); //contiene todos los modulos con imgdes

                view = (DataView)sqldsModuloMaster.Select(args); //procedimiento almacenado
                DataTable dt = view.ToTable();
                int rowindex = 0;
                foreach (DataRow row in dt.Rows)
                {
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow row2 in dtInicial.Rows)
                            {
                                if (row["codmod"].Equals(row2["codmod"]))
                                {
                                    row2["imagen"] = row["imagen"];
                                }
                            }
                        }
                    }
                    rowindex++;
                }
                dt = dtInicial;

                foreach (DataRow row in dt.Rows)
                {
                    string s_imagen = row["imagen"].ToString().Trim();
                    int i_codmod = dt.Rows.IndexOf(row) + 1;
                    string display = "display:normal";
                    ImageButton imgModulo = new ImageButton();
                    imgModulo.ImageUrl = clsGlobalVariablesClass.ImageDirectory + s_imagen;
                    if (s_imagen.Equals("3")) //si retorna 3
                    {
                        imgModulo.Visible = false;
                        imgModulo.Enabled = false;
                        display = "display:none";
                    }
                    else
                    {
                        if (s_imagen.Equals("d" + (dt.Rows.IndexOf(row) + 1) + ".png"))
                        {
                            imgModulo.Enabled = false;
                            if (i_codmod == 9 || i_codmod == 10) //Configuracion y Seguridad son <a href="">
                                imgModulo.ImageUrl = "2";
                        }
                    }

                    switch (i_codmod)
                    {
                        case 1:
                            imgInventarios.ImageUrl = imgModulo.ImageUrl;
                            imgInventarios.Enabled = imgModulo.Enabled;
                            imgInventarios.Visible = imgModulo.Visible;
                            divInventarios.Attributes["style"] = display;
                            imgInventarios.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            break;
                        case 2:
                            imgCompras.ImageUrl = imgModulo.ImageUrl;
                            imgCompras.Enabled = imgModulo.Enabled;
                            imgCompras.Visible = imgModulo.Visible;
                            divCompras.Attributes["style"] = display;
                            imgCompras.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            break;
                        case 3:
                            imgVentas.ImageUrl = imgModulo.ImageUrl;
                            imgVentas.Enabled = imgModulo.Enabled;
                            imgVentas.Visible = imgModulo.Visible;
                            divVentas.Attributes["style"] = display;
                            imgVentas.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            break;
                        case 4:
                            imgContabilidad.ImageUrl = imgModulo.ImageUrl;
                            imgContabilidad.Enabled = imgModulo.Enabled;
                            imgContabilidad.Visible = imgModulo.Visible;
                            divContabilidad.Attributes["style"] = display;
                            imgContabilidad.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            break;
                        case 5:
                            imgNomina.ImageUrl = imgModulo.ImageUrl;
                            imgNomina.Enabled = imgModulo.Enabled;
                            imgNomina.Visible = imgModulo.Visible;
                            divNomina.Attributes["style"] = display;
                            imgNomina.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            break;
                        case 6:
                            imgActivosFijos.ImageUrl = imgModulo.ImageUrl;
                            imgActivosFijos.Enabled = imgModulo.Enabled;
                            imgActivosFijos.Visible = imgModulo.Visible;
                            divActivosFijos.Attributes["style"] = display;
                            imgActivosFijos.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            break;
                        case 7:
                            imgImportaciones.ImageUrl = imgModulo.ImageUrl;
                            imgImportaciones.Enabled = imgModulo.Enabled;
                            imgImportaciones.Visible = imgModulo.Visible;
                            divImportaciones.Attributes["style"] = display;
                            imgImportaciones.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            break;
                        case 8:
                            imgProduccion.ImageUrl = imgModulo.ImageUrl;
                            imgProduccion.Enabled = imgModulo.Enabled;
                            imgProduccion.Visible = imgModulo.Visible;
                            divProduccion.Attributes["style"] = display;
                            imgProduccion.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            break;
                        case 9:
                            Session.Add("gs_EstmodConfig", imgModulo.ImageUrl); //1.png,d1.png,3
                            lblModuloConf.Text = row["nommod"].ToString().Trim();
                            lkbtnConfig.HRef = "w_Escritorio.aspx?gs_Modulo=" + row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            break;
                        case 10:
                            Session.Add("gs_EstmodSeg", imgModulo.ImageUrl); //1.png,d1.png,3
                            lblModuloSeg.Text = row["nommod"].ToString().Trim();
                            lkbtnSeg.HRef = "w_Escritorio.aspx?gs_Modulo=" + row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = "❗ *01. " + ex.Message;
            }
        }
        protected void f_Seg_Usuario_Modulo_Config()
        {
            if (Session["gs_EstmodConfig"] != null && Session["gs_EstmodSeg"] != null)
            {
                if (!String.IsNullOrEmpty(Session["gs_EstmodConfig"].ToString()) && !String.IsNullOrEmpty(Session["gs_EstmodSeg"].ToString()))
                {
                    if (Session["gs_EstmodConfig"].ToString().Equals("2")) //deshabilitado
                    {
                        lkbtnConfig.Disabled = true;
                        lkbtnConfig.Attributes.Remove("href");
                    }
                    else if (Session["gs_EstmodConfig"].ToString().Equals("3"))
                        lkbtnConfig.Visible = false;

                    if (Session["gs_EstmodSeg"].ToString().Equals("2")) //deshabilitado
                    {
                        lkbtnSeg.Disabled = true;
                        lkbtnSeg.Attributes.Remove("href");
                    }
                    else if (Session["gs_EstmodSeg"].ToString().Equals("3"))
                        lkbtnSeg.Visible = false;
                }
            }
            else
            {
                lkbtnConfig.Visible = false;
                lkbtnSeg.Visible = false;
            }
        }
        protected void btndark_Click(object sender, EventArgs e)
        {
            body.Attributes.Add("class", "c-app c-dark-theme");
            btnligth.Attributes.Add("style", "display: inline;");
            btndark.Attributes.Add("style", "display: none;");
            mySidenav.Attributes.Add("class", "sidenavImg dark-theme");
            Session["gs_Modo"] = "D";
        }
        protected void btnligth_Click(object sender, EventArgs e)
        {
            body.Attributes.Add("class", "c-app");
            btndark.Attributes.Add("style", "display: inline;");
            btnligth.Attributes.Add("style", "display: none;");
            mySidenav.Attributes.Add("class", "sidenavImg");
            Session["gs_Modo"] = "L";
        }

        protected void imgModulos_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imgBtn = (ImageButton)sender;
            Response.Redirect("w_Escritorio.aspx?gs_Modulo=" + imgBtn.CommandArgument.ToString());
        }
    }
}

