﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection.Emit;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_Registrar : System.Web.UI.Page
    {
        cls_seg_Usuario_Registrar objUsuarioRegistrar = new cls_seg_Usuario_Registrar();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Add("gs_Error", "");
                Session.Add("gd_FecAct", DateTime.Now.ToString("yyy-MM-dd"));
                Session.Add("gs_Modo", "L"); //Para light/dark mode
                Session.Add("gs_RucempSelected", "");
            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod()]
        public static String f_UnirNombreCompleto(string s_nombre1, string s_apellido1)
        {
            string s_nombreCompleto = s_nombre1 + " " +  s_apellido1;
            return s_nombreCompleto;
        }

        protected void btnCrearCuenta_Click(object sender, EventArgs e)
        {    
            lblError.Text = "";
            lblExito.Text = "";

            //string s_ClaUsu, s_ClaCon;
            //s_ClaUsu = txtPassword.Value.Trim();
            //s_ClaCon = txtPassConfirm.Value.Trim();
            //if (!s_ClaUsu.Equals(s_ClaCon))
            //{
            //    lblError.Text = "* Las contraseñas no coinciden.";
            //}
            //else
            //{
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsEmpresaExiste.Select(args);
            DataTable dt = view.ToTable();
            if (dt != null)
            {
                if (dt.Rows.Count > 0) //validar si RUC existe en seg_empresa
                {
                    objUsuarioRegistrar.Rucemp = txtRucemp.Text.Trim();
                    objUsuarioRegistrar.Codus1 = txtCodus1.Value.Trim();
                    objUsuarioRegistrar.Nomusu = txtNomusu.Value.Trim();
                    objUsuarioRegistrar.Usmail = txtEmail.Value.Trim();
                    //objUsuarioRegistrar.Clausu = txtPassword.Value.Trim();
                    string ip = f_GetIp().Trim();
                    objUsuarioRegistrar.Pcnom = ip;
                    objUsuarioRegistrar.Pcip = ip;
                    objUsuarioRegistrar.Pcmac = ip;
                    objUsuarioRegistrar.Estado = "2";

                    //i - Datos de Conexión ----------------------------------------------------
                    //OBTENER PALCLA
                    DataSourceSelectArguments args1 = new DataSourceSelectArguments();
                    DataView view1 = (DataView)sqldsPalabraClave.Select(args1);
                    DataTable dt1 = view1.ToTable();
                    string s_palcla = dt1.Rows[0]["palcla"].ToString();
                    cls_seg_Encriptacion objEncriptacion = new cls_seg_Encriptacion();
                    string s_PalCla = objEncriptacion.f_seg_Encriptar(s_palcla);
                    objUsuarioRegistrar = objUsuarioRegistrar.f_Usuario_DatosConexion(objUsuarioRegistrar, s_PalCla);

                    //f - Datos de Conexión ----------------------------------------------------
                    if (String.IsNullOrEmpty(objUsuarioRegistrar.Error.Mensaje))
                    {
                        objUsuarioRegistrar = objUsuarioRegistrar.f_Usuario_Existe(objUsuarioRegistrar);
                        if (String.IsNullOrEmpty(objUsuarioRegistrar.Error.Mensaje))
                        {
                            if (objUsuarioRegistrar.Existe == 1) lblError.Text = "* Código de usuario no disponible.";
                            else if (objUsuarioRegistrar.Existe == 2) lblError.Text = "* Email no disponible.";
                            else
                            {
                                clsError objError = objUsuarioRegistrar.f_Usuario_Actualizar(objUsuarioRegistrar, "ADMIN", "ADMIN", "Add");

                                if (String.IsNullOrEmpty(objError.Mensaje))
                                {
                                    //objError = objUsuarioRegistrar.f_Usuario_Clave(objUsuarioRegistrar); //guarda pass encriptado
                                    //if (String.IsNullOrEmpty(objError.Mensaje))
                                    //{
                                    lblError.Text = "";
                                    lblExito.Text = "✔️ Cuenta creada. ";

                                    //DATOS DE CONEXION 
                                    objError = objUsuarioRegistrar.f_Usuario_Encriptar_Base(objUsuarioRegistrar, s_PalCla);
                                    if (String.IsNullOrEmpty(objError.Mensaje))
                                    {
                                        f_VerificacionEmail(objUsuarioRegistrar.Usmail, objUsuarioRegistrar.Rucemp, objUsuarioRegistrar.Codus1);
                                        lblExito.Text = lblExito.Text + " - Verificar correo electronico para completar resgistro.";
                                    }
                                    else
                                    {
                                        lblExito.Text = lblExito.Text + " - Datos de Conexión NO Registrados.";
                                        f_ErrorNuevo(objError);
                                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                                    }
                                    //}
                                    //else
                                    //{
                                    //    f_ErrorNuevo(objError);
                                    //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                                    //}
                                }
                                else
                                {
                                    f_ErrorNuevo(objError);
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                                }
                            }
                        }
                        else
                        {
                            f_ErrorNuevo(objUsuarioRegistrar.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                    else
                    {
                        f_ErrorNuevo(objUsuarioRegistrar.Error);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
                else
                {
                    lblError.Text = "* No existe la empresa.";
                }
            }
            //} 
        }
                
        protected string f_Credenciasles_Usuario()
        {
            string s_ClaUsu, s_ClaCon, s_CodUs1;
            DateTime dt_FecAct;

            string ip = f_GetIp();
            string s0 = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
            string s1 = System.Web.HttpContext.Current.Request.ServerVariables["AUTH_USER"];
            string s2 = System.Web.HttpContext.Current.Request.ServerVariables["AUTH_TYPE"];
            string s3 = System.Web.HttpContext.Current.Request.ServerVariables["LOGON_USER"];
            string s4 = System.Web.HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
            string s5 = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
            string s6 = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_USER"];
            string s7 = System.Web.HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
            string s8 = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            string s9 = System.Security.Principal.WindowsIdentity.GetCurrent().User.Value;
            string s10 = System.Security.Principal.WindowsIdentity.GetCurrent().Owner.Value;
            string s11 = System.Security.Principal.WindowsIdentity.GetCurrent().Token.ToString();
            string s12 = System.Security.Principal.WindowsIdentity.GetCurrent().AccessToken.ToString();
            string s13 = System.Web.HttpContext.Current.Request.ServerVariables["CACHE_URL"];
            string s14 = System.Web.HttpContext.Current.Request.ServerVariables["CERT_COOKIE"];
            string s15 = System.Web.HttpContext.Current.Request.ServerVariables["CERT_FLAGS"];
            string s16 = System.Web.HttpContext.Current.Request.ServerVariables["CERT_ISSUER"];
            string s17 = System.Web.HttpContext.Current.Request.ServerVariables["CERT_KEYSIZE"];
            string s18 = System.Web.HttpContext.Current.Request.ServerVariables["CERT_SECRETKEYSIZE"];
            string s19 = System.Web.HttpContext.Current.Request.ServerVariables["CERT_SERIALNUMBER"];
            string s20 = System.Web.HttpContext.Current.Request.ServerVariables["CERT_SERVER_ISSUER"];
            string s21 = System.Web.HttpContext.Current.Request.ServerVariables["APP_POOL_ID"];
            string s22 = System.Web.HttpContext.Current.Request.ServerVariables["CERT_SUBJECT"];
            string s23 = System.Web.HttpContext.Current.Request.ServerVariables["CONTENT_LENGTH"];
            string s24 = System.Web.HttpContext.Current.Request.ServerVariables["CONTENT_TYPE"];
            string s25 = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_COOKIE"];
            string s26 = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_URL"];
            string s27 = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_VERSION"];
            string s28 = System.Web.HttpContext.Current.Request.ServerVariables["INSTANCE_ID"];
            string s29 = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            string s30 = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_PORT"];
            string s31 = System.Web.HttpContext.Current.Request.ServerVariables["UNMAPPED_REMOTE_USER"];

            string PRUEBA = "REMOTE_ADDR: " + ip +
                "\n HTTP_USER_AGENT: " + s0 +
                "\n AUTH_USER: " + s1 +
                "\n AUTH_TYPE: " + s2 +
                "\n LOGON_USER: " + s3 +
                "\n LOCAL_ADDR: " + s4 +
                "\n REMOTE_HOST: " + s5 +
                "\n REMOTE_USER: " + s6 +
                "\n LOCAL_ADDR: " + s7 +
                "\n Name: " + s8 +
                "\n User: " + s9 +
                "\n Owner: " + s10 +
                "\n Token: " + s11 +
                "\n AccessToken: " + s12 +
                "\n CACHE_URL: " + s13 +
                "\n CERT_COOKIE: " + s14 +
                "\n CERT_FLAGS: " + s15 +
                "\n CERT_ISSUER: " + s16 +
                "\n CERT_KEYSIZE: " + s17 +
                "\n CERT_SECRETKEYSIZE: " + s18 +
                "\n CERT_SERIALNUMBER: " + s19 +
                "\n CERT_SERVER_ISSUER: " + s20 +
                "\n APP_POOL_ID: " + s21 +
                "\n CERT_SUBJECT: " + s22 +
                "\n CONTENT_LENGTH: " + s23 +
                "\n CONTENT_TYPE: " + s24 +
                "\n HTTP_COOKIE: " + s25 +
                "\n HTTP_URL: " + s26 +
                "\n HTTP_VERSION: " + s27 +
                "\n INSTANCE_ID: " + s28 +
                "\n REMOTE_ADDR: " + s29 +
                "\n REMOTE_PORT: " + s30 +
                "\n UNMAPPED_REMOTE_USER: " + s31;
            return PRUEBA;
        }

        public string f_GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                ip = "HTTP_X_FORWARDED_FOR IS NULL";
            }
            return ip;
        }

        private void f_VerificacionEmail(string s_usmail, string rucemp, string codus1)
        {
            string url = Request.Url.GetLeftPart(UriPartial.Authority) + "/Autenticación/w_ConfirmarEmail.aspx?userid=" + codus1 + "-" + rucemp;
            //Fetching Settings from WEB.CONFIG file.
            string emailSender = ConfigurationManager.AppSettings["emailsender"].ToString();
            string emailSenderPassword = ConfigurationManager.AppSettings["password"].ToString();
            string emailSenderHost = ConfigurationManager.AppSettings["smtpserver"].ToString();
            int emailSenderPort = Convert.ToInt16(ConfigurationManager.AppSettings["portnumber"]);
            Boolean emailIsSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSSL"]);
            string html = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
                <html xmlns=""http://www.w3.org/1999/xhtml"" lang=""en-GB"">
                <head>
                  <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
                  <title>Facturación Electrónica - Kohinor Web</title>
                  <meta name=""viewport"" content=""width=device-width, initial-scale=1.0""/>
                  <style type=""text/css"">
                    a[x-apple-data-detectors] {color: inherit !important;}
                  </style>
                </head>
                <body style=""margin: 0; padding: 0;"">
                  <table role=""presentation"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
                    <tr>
                      <td style=""padding: 20px 0 30px 0;"">

                <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"" style=""border-collapse: collapse; border: 1px solid #cccccc;"">
                  <tr>
                    <td align=""center"" bgcolor=""#00445e"" style=""padding: 40px 0 30px 0;"">
                      <img src=""https://www.kohinorec.com/wp-content/uploads/2020/08/Logo-Kohinor-2.png"" alt=""Kohinor Web."" width=""300"" height=""95"" style=""display: block;"" />
                    </td>
                  </tr>
                  <tr>
                    <td bgcolor=""#ffffff"" style=""padding: 40px 30px 40px 30px;"">
                      <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;"">
                        <tr>
                          <td style=""color: #153643; font-family: Arial, sans-serif; text-align:center;"">
                            <h1 style=""font-size: 24px; margin: 0;"">Verificación de cuenta</h1>
                          </td>
                        </tr>
                        <tr>
                          <td style=""color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;"">
                            <p style=""margin: 0;"">Se ha recibido la solicitud de registro de <strong>" + codus1 + @"</strong>. Por favor, verifique su dirección de correo electrónico haciendo clic en este enlace:</p>
                          </td>
                        </tr>
                        <tr>
                          <td>                            
                            <table width=""500"" align=""center""
                                style=""background-color:#ffffff;padding:10px;border:0;margin:auto;font-size:14px;line-height:125%"" cellspacing=""0""
                                cellpadding=""0"">
                                <tbody>
                                    <tr>
                                        <td align=""center"">
                                            <table border=""0"" cellpadding=""10"" cellspacing=""0""
                                                style=""background-color:#00445e;border:1px solid #0089be;border-radius:4px"">
                                                <tbody>
                                                    <tr>
                                                        <td align=""center"" valign=""middle"" style=""padding:10px;border-collapse:collapse"">
                                                            <a href=""" + url + @"""
                                                                style=""text-decoration:none"">
                                                                <div
                                                                    style=""text-align:center;color:white;font-size:15px;font-weight:500;line-height:15px"">
                                                                    Verificar</div>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style=""color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;"">
                            <p style=""margin: 0;"">
                                Fecha y Hora de Proceso: " + DateTime.Now + @"
                                <br>
                                Si no solicitó registrar una nueva cuenta puede ingnorar este correo. Solo la persona con acceso a este correo podrá verificar la cuenta.
                            </p>
                          </td>
                        </tr>     
                        <tr>
                          <td class=""x_px-xs-20""
                            style=""padding-top:0px; padding-right:40px; padding-bottom:0; padding-left:40px; text-align:center"">
                            <span
                              style=""font-family: Arial; font-size: 8px; font-weight: normal; font-style: normal; font-stretch: normal; line-height: normal; letter-spacing: normal; text-align: center; color: rgb(204, 204, 204) !important;""
                              data-ogsc=""rgb(74, 74, 74)"">
                              Nota: La información y adjuntos contenidos en este mensaje electrónico son confidenciales y reservados. Esta información no debe ser usada, reproducida o divulgada por otras personas distintas a su(s) destinatario(s). Si Ud. no es el destinatario de este email, le solicitamos comedidamente eliminarlo. La organización no asume responsabilidad sobre información, opiniones o criterios contenidos en este e-mail que no esté relacionada con negocios oficiales de nuestra institución.
                              <br>
                              <br>
                              Disclaimer: The information and attachments contained in this electronic message are confidential and proprietary. This information should not be used, reproduced, or disclosed by anyone other than the addressee. If you are not the addressee of this email, we kindly ask you to delete it. The organization assumes no responsibility for information, opinions or opinions contained in this e-mail that are not related to official business of our institution.
                            </span>              
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td bgcolor=""#7a7a7a"" style=""padding: 30px 30px;"">
                        <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;"">
                        <tr>
                          <td style=""color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;"">
                            <p style=""margin: 0;"">© 2021 Kohinor. Todos los derechos reservados.<br/>
                            Powered by <a href=""#"" style=""color: #ffffff;"">POWERSOFT</a></p>
                          </td>
                          <td align=""right"">
                            <table border=""0"" cellpadding=""0"" cellspacing=""0"" style=""border-collapse: collapse;"">
                              <tr>
                                <td>
                                  <a href=""http://www.twitter.com/"">
                                    <img src=""https://assets.codepen.io/210284/tw.gif"" alt=""Twitter."" width=""38"" height=""38"" style=""display: block;"" border=""0"" />
                                  </a>
                                </td>
                                <td style=""font-size: 0; line-height: 0;"" width=""20"">&nbsp;</td>
                                <td>
                                  <a href=""http://www.twitter.com/"">
                                    <img src=""https://assets.codepen.io/210284/fb.gif"" alt=""Facebook."" width=""38"" height=""38"" style=""display: block;"" border=""0"" />
                                  </a>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                      </td>
                    </tr>
                  </table>
                </body>
                </html>";

            

            try
            {
                MailMessage objMailMsg = new MailMessage();
                objMailMsg.From = new MailAddress(emailSender, "KohinorWeb");
                objMailMsg.To.Add(s_usmail);
                objMailMsg.Subject = "Verificación de cuenta";
                objMailMsg.Body = html;
                objMailMsg.IsBodyHtml = true;
                objMailMsg.Priority = MailPriority.Normal;

                SmtpClient sc = new SmtpClient(emailSenderHost, emailSenderPort); //Try port 587 instead of 465. Port 465 is technically deprecated.
                sc.Credentials = new NetworkCredential(emailSender, emailSenderPassword);
                sc.EnableSsl = emailIsSSL;
                sc.Send(objMailMsg);
            }
            catch (Exception ex)
            {
                clsError objError = new clsError();
                objError = objError.f_ErrorControlado(ex);
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
    }
}