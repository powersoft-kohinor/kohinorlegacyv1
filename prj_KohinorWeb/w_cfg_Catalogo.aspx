﻿<%@ Page Title="Catálogo" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_cfg_Catalogo.aspx.cs" Inherits="prj_KohinorWeb.w_cfg_Catalogo" %>

<asp:Content ID="Content4" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item active">Monedas</li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <a class="c-subheader-nav-link">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary" type="button" id="">
                            <i class="cil-filter"></i>
                        </button>
                    </div>

                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
                </div>
                <asp:LinkButton ID="lkbtnSearch" runat="server" OnClick="f_GridBuscar" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
            </a>

            <a class="c-subheader-nav-link">
                <i class="c-icon cil-speech"></i>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-graph"></i>&nbsp;
                            Escritorio
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-settings"></i>
            </a>
        </div>
    </div>
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item active">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <asp:LinkButton class="btn btn-outline-primary" ID="btnAgregar" runat="server" OnClick="btnAgregar_Click"><span class="cil-file"></span> Agregar</asp:LinkButton>
                    <asp:LinkButton class="btn btn-outline-dark" ID="btnEditar" runat="server" OnClick="btnEditar_Click"><span class="cil-folder-open"></span> Editar</asp:LinkButton>
                    <asp:LinkButton class="btn btn-outline-dark" ID="btnGuardar" runat="server" OnClick="btnGuardar_Click"><span class="cil-save"></span> Guardar</asp:LinkButton>
                    <asp:LinkButton class="btn btn-outline-dark" ID="btnCancelar" runat="server" OnClick="btnCancelar_Click"><span class="cil-grid-slash"></span> Cancelar</asp:LinkButton>
                    <asp:LinkButton class="btn btn-outline-danger" ID="btnEliminar" runat="server" OnClick="btnDelete_Click"><span class="cil-trash"></span> Eliminar</asp:LinkButton>
                </div>
                <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>

            </li>
        </ol>

    </div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">

    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="row">
                    <div class="col-sm-2">

                        <div class="card text-white bg-gradient-primary">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i><b>Filtros</b>
                            </div>
                            <div class="card-body">

                                <asp:TreeView ID="tvCatalogo" runat="server" NodeStyle-CssClass="treeNode" RootNodeStyle-CssClass="rootNode" LeafNodeStyle-CssClass="leafNode" SelectedNodeStyle-CssClass="selectNode" ExpandDepth="1" NodeStyle-NodeSpacing="4px" HoverNodeStyle-CssClass="HoverNodeStyle" ImageSet="Arrows" OnSelectedNodeChanged="tvCatalogo_SelectedNodeChanged" DataValueField="tipcat">
                                </asp:TreeView>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-->
                    <div class="col-sm-10">
                        <div class="card text-white bg-gradient-primary">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i><b>Catalogo</b>
                            </div>
                            <div class="card-body">




                                <div class="table table-responsive" style="overflow-x: auto;">
                                    <div class="thead-dark">
                                        <!-- Begin Page Content -->
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:GridView ID="grvCatalogo" runat="server" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" class="table table-bordered table-active text-black-50 table-active table-hover table-striped"
                                                    ShowFooter="True" AllowPaging="True" PageSize="7" OnPageIndexChanging="grvCatalogo_PageIndexChanging" DataKeyNames="codcat">

                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkRow" runat="server"></asp:CheckBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex + 1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Codigo">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvCodcat" runat="server" Text='<%#Eval("codcat")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="etxtgrvCodcat" class="form-control form-control-sm" runat="server" Text='<%#Eval("codcat")%>'> </asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="ftxtgrvCodcat" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Orden">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvOrden" runat="server" Text='<%#Eval("orden")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="etxtgrvOrden" class="form-control form-control-sm" runat="server" Text='<%#Eval("orden")%>'> </asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="ftxtgrvOrden" class="form-control form-control-sm" runat="server"> </asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Descripcion">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvNomcat" runat="server" Text='<%#Eval("nomcat")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="etxtgrvNomcat" runat="server" class="form-control form-control-sm" Text='<%#Eval("nomcat")%>'> </asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="ftxtgrvNomcat" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Referencia">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvCodref" runat="server" Text='<%#Eval("codref")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="etxtgrvCodref" runat="server" class="form-control form-control-sm" Text='<%#Eval("codref")%>'> </asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="ftxtgrvCodref" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Grupo">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvNomgru" runat="server" Text='<%#Eval("nomgru")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="etxtgrvNomgru" runat="server" class="form-control form-control-sm" Text='<%#Eval("nomgru")%>'> </asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="ftxtgrvNomgru" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Usuario">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvUsuing" runat="server" Text='<%#Eval("usuing")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="flblgrvUsuing" runat="server"> </asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Código Usuario">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvCodusu" runat="server" Text='<%#Eval("codusu")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="flblgrvCodusu" runat="server"> </asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="Fecha Ingreso">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvFecing" runat="server" Text='<%#Eval("fecing")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="flblgrvFecing" runat="server"> </asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Fecha Ultimo">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvFecult" runat="server" Text='<%#Eval("fecult")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="flblgrvFecult" runat="server"> </asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>




                                                    </Columns>
                                                    <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                                </asp:GridView>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
            <!-- /.container-fluid -->
            <!-- End of Main Content -->

        </div>
    </main>




    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCatalogoTipo" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [tipcat], [nomcat] FROM [cfg_catalogo_tipo]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCatalogo" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT * FROM [cfg_catalogo] WHERE ([tipcat] = @tipcat)">
        <%-- <SelectParameters>
            <asp:ControlParameter ControlID="ddlCatalogoTipo" Name="tipcat" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>--%>
    </asp:SqlDataSource>

    <!--f SQL DATASOURCES-->



    <!-- Modal Eliminar -->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Cliente</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar la(s) clase(s) '<asp:Label ID="lblEliCli" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminar_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <style>
        .treeNode {
            color: #0c232b;
            width: 140px;
            font-size: 11px;
            display: block;
            text-decoration: none;
            border: solid 1px Transparent;
            padding: 3px 3px 3px 3px;
        }

            .treeNode:hover,
            a.treeNode:hover,
            .treeNode:link:hover,
            a.treeNode:link:hover {
                font-size: 11px;
                display: block;
                text-decoration: none;
                color: #003687;
                border: solid 1px #71a9ff;
                background-color: #c6ddff;
                padding: 3px 3px 3px 3px;
            }

        .rootNode {
            font-weight: bold;
            color: #4f5d73;
            width: 140px;
            font-size: 11px;
            display: block;
            text-decoration: none;
            border: solid 1px Transparent;
            padding: 3px 3px 3px 3px;
            background-color: #d8dbe0;
            border-radius: 10px;
        }

        .HoverNodeStyle {
            font-size: 11px;
            display: block;
            text-decoration: none;
            color: #4f5d73;
            border: solid 1px Transparent;
            padding: 3px 3px 3px 3px;
        }

        .leafNode {
            font-size: 11px;
            padding: 3px 3px 3px 3px;
            color: #4f5d73;
            background-color: #d8dbe0;
            border-radius: 10px;
        }

        .selectNode {
            font-weight: bold;
            background-color: Black;
            color: White;
            border-radius: 10px;
            font-size: 11px;
            display: block;
            text-decoration: none;
            color: #ffffff;
            border: solid 1px Transparent;
            padding: 3px 3px 3px 3px;
        }

            .selectNode:hover,
            a.selectNode:hover,
            .selectNode:link:hover,
            a.selectNode:link:hover {
                font-size: 11px;
                display: block;
                text-decoration: none;
                color: #003687;
                border: solid 1px #71a9ff;
                background-color: #c6ddff;
                padding: 3px 3px 3px 3px;
            }
    </style>
</asp:Content>
