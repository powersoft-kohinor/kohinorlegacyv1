﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_cfg_Catalogo : System.Web.UI.Page
    {
        protected cls_cfg_Catalogo objCatalogo = new cls_cfg_Catalogo();
        int editindex = -1;
        protected string codmod = "9", niv001 = "2", niv002 = "19";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                f_Seg_Usuario_Menu();
                // hide Update button on page load
                GetTreeViewItems();
                btnGuardar.Visible = false;
                f_BindGrid_Inicial();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAgregar.Attributes.Add("class", "btn btn-dark");
                btnEditar.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-secondary disabled");
                btnEliminar.Attributes.Add("class", "btn btn-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");
                grvCatalogo.PagerStyle.CssClass = "";
            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAgregar.Attributes.Add("class", "btn btn-outline-dark");
                btnEditar.Attributes.Add("class", "btn btn-outline-primary");
                btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");
                grvCatalogo.PagerStyle.CssClass = "pagination-ys justify-content-center align-items-center";
            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente Clase
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnAgregar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEditar.Attributes.Add("class", "btn btn-outline-secondary disabled");

                    }
                    if (row["banbus"].Equals("N")) //Abrir O Buscar
                    {
                        txtSearch.Enabled = false;
                        lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 disabled");
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                    }
                }
            }
            return dt;
        }

        private void GetTreeViewItems()
        {
            objCatalogo = objCatalogo.f_Catalogo_Tipo();
            if (String.IsNullOrEmpty(objCatalogo.Error.Mensaje))
            {
                DataSet ds = objCatalogo.dsCatalogo;
                Session.Add("gds_Catalogo", ds);
                TreeNode tn_Catalogo = new TreeNode();
                tn_Catalogo.Text = "TIPOS CATALOGOS";

                tvCatalogo.Nodes.Add(tn_Catalogo);
                foreach (DataRow drCatalogo in ds.Tables["cfg_catalogo_tipo"].Rows)
                {
                    TreeNode tn_Catalogo_Tipo = new TreeNode();
                    tn_Catalogo_Tipo.Text = drCatalogo["tipcat"].ToString() + drCatalogo["nomcat"].ToString();
                    tn_Catalogo_Tipo.Value = drCatalogo["tipcat"].ToString();
                    tn_Catalogo.ChildNodes.Add(tn_Catalogo_Tipo);
                }
            }
            else
            {
                f_ErrorNuevo(objCatalogo.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }


            //CODIFGO EN CASO DE HABER TABLAS ENLAZADAS KDSABHKDHGBSKHADSBKDSABKBHDSAHBJKDSABHKBHDASBHKHBKDASHKBDASHBKHKBDASHKBDASHBKDSAHBKDASHKBDBHKSAHBKDASBHKDSA

            //string cs = ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString;
            //SqlConnection con = new SqlConnection(cs);
            //SqlDataAdapter da = new SqlDataAdapter("[dbo].[CFG_M_CATALOGO_TIPO]", con);
            //DataSet ds = new DataSet();
            //da.Fill(ds);

            //ds.Relations.Add("ChildRows", ds.Tables[0].Columns["tipcat"],
            //    ds.Tables[0].Columns["nomcat"]);

            //foreach (DataRow level1DataRow in ds.Tables[0].Rows)
            //{

            //        TreeNode treeNode = new TreeNode();
            //        treeNode.Text = level1DataRow["nomcat"].ToString();
            //        //treeNode.NavigateUrl = level1DataRow["NavigateURL"].ToString();

            //        //DataRow[] level2DataRows = level1DataRow.GetChildRows("ChildRows");
            //        //foreach (DataRow level2DataRow in level2DataRows)
            //        //{
            //        //    TreeNode childTreeNode = new TreeNode();
            //        //    childTreeNode.Text = level2DataRow["tipcat"].ToString();
            //        //    //childTreeNode.NavigateUrl = level2DataRow[""].ToString();
            //        //    treeNode.ChildNodes.Add(childTreeNode);
            //        //}
            //        TreeView1.Nodes.Add(treeNode);

            //}
        }

        private void f_BindGrid_Inicial()
        {
            string s_Tipcat = "";
            TreeNodeCollection nodes = tvCatalogo.Nodes;
            //string s_Tipcat = ddlCatalogoTipo.SelectedValue;
            if (tvCatalogo.SelectedNode == null)
            {
                nodes[0].Selected = true;
            }
            else
            {
                s_Tipcat = tvCatalogo.SelectedNode.Value;

            }


            objCatalogo = objCatalogo.f_Catalogo_Buscar(s_Tipcat);
            if (String.IsNullOrEmpty(objCatalogo.Error.Mensaje))
            {
                DataTable dt = objCatalogo.dtCatalogo;
                Session.Add("gdt_Catalogo", dt);
                if (dt.Rows.Count > 0)
                {
                    grvCatalogo.DataSource = dt;
                    grvCatalogo.DataBind();
                }
                else
                {
                    // add new row when the dataset is having zero record
                    dt.Rows.Add(dt.NewRow());
                    grvCatalogo.DataSource = dt;
                    grvCatalogo.DataBind();
                    grvCatalogo.Rows[0].Visible = false;
                }
            }
            else
            {
                f_ErrorNuevo(objCatalogo.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        private void f_BindGrid_Tree()
        {
            string s_Tipcat = tvCatalogo.SelectedNode.Value;

            objCatalogo = objCatalogo.f_Catalogo_Buscar(s_Tipcat);
            if (String.IsNullOrEmpty(objCatalogo.Error.Mensaje))
            {
                DataTable dt = objCatalogo.dtCatalogo;
                Session.Add("gdt_Catalogo", dt);
                if (dt.Rows.Count > 0)
                {
                    grvCatalogo.DataSource = dt;
                    grvCatalogo.DataBind();
                }
                else
                {
                    // add new row when the dataset is having zero record
                    dt.Rows.Add(dt.NewRow());
                    grvCatalogo.DataSource = dt;
                    grvCatalogo.DataBind();
                    grvCatalogo.Rows[0].Visible = false;
                }
            }
            else
            {
                f_ErrorNuevo(objCatalogo.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        private void f_BindGrid(string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Catalogo"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "codcat LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "nomgru LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codref LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "nomcat LIKE " + "'%" + searchText.Trim() + "%'";

                    grvCatalogo.DataSource = dv;
                }
                grvCatalogo.DataBind();
            }
        }

        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(txtSearch.Text);
        }

        protected void grvCatalogo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCatalogo.PageIndex = e.NewPageIndex;
            f_BindGrid(txtSearch.Text);
            lblError.Text = lblExito.Text = "";
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            TextBox ftxtgrvCodcat = (TextBox)(grvCatalogo.FooterRow.FindControl("ftxtgrvCodcat"));
            TextBox ftxtgrvOrden = (TextBox)(grvCatalogo.FooterRow.FindControl("ftxtgrvOrden"));
            TextBox ftxtgrvNomcat = (TextBox)(grvCatalogo.FooterRow.FindControl("ftxtgrvNomcat"));
            TextBox ftxtgrvCodref = (TextBox)(grvCatalogo.FooterRow.FindControl("ftxtgrvCodref"));
            TextBox ftxtgrvNomgru = (TextBox)(grvCatalogo.FooterRow.FindControl("ftxtgrvNomgru"));




            if (ftxtgrvCodcat.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un codigo.";
                ftxtgrvCodcat.Focus();
                return;
            }
            if (ftxtgrvOrden.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese el orden.";
                ftxtgrvOrden.Focus();
                return;
            }
            if (ftxtgrvNomcat.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese el nombre.";
                ftxtgrvNomcat.Focus();
                return;
            }
            if (ftxtgrvCodref.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese el codigo de refrencia.";
                ftxtgrvCodref.Focus();
                return;
            }

            if (ftxtgrvNomgru.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un grupo.";
                ftxtgrvNomgru.Focus();
                return;
            }

            objCatalogo.Codcat = ftxtgrvCodcat.Text.Trim();
            objCatalogo.Orden = Decimal.Parse(ftxtgrvOrden.Text.Trim());
            objCatalogo.Nomcat = ftxtgrvNomcat.Text.Trim();
            objCatalogo.Codref = ftxtgrvCodref.Text.Trim();
            objCatalogo.Nomgru = ftxtgrvNomgru.Text.Trim();

            objCatalogo.Tipcat = tvCatalogo.SelectedNode.Value;



            clsError objError = objCatalogo.f_Catalogo_Actualizar(objCatalogo, Session["gs_Codemp"].ToString(), Session["gs_UsuIng"].ToString(), DateTime.Now.ToString(), Session["gs_CodUs1"].ToString(), DateTime.Now.ToString(), "Add");


            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                f_BindGrid_Inicial();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblExito.Text = "";
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvCatalogo.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for edit
                    editindex = gvrow.RowIndex;

                    rowCount++;
                    if (rowCount > 1)
                    {
                        break;
                    }
                }
            }

            if (rowCount > 1)
            {
                lblError.Text = "Seleccione solo una fila para editar";
            }
            else if (rowCount == 0)
            {
                lblError.Text = "Seleccione una fila para editar";
            }
            else
            {
                ViewState["editindex"] = editindex;
                Label lblgrvCodcat = (Label)grvCatalogo.Rows[editindex].FindControl("lblgrvCodcat");
                Session.Add("gs_grvCodcat", lblgrvCodcat.Text.Trim());

                grvCatalogo.EditIndex = editindex;
                f_BindGrid_Inicial();



                // disable all row checkboxes while editing
                foreach (GridViewRow gvrow in grvCatalogo.Rows)
                {
                    CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                    chkRow.Enabled = false;
                }

                //hide footer row while editing
                grvCatalogo.FooterRow.Visible = false;

                // hide and disable respective buttons
                btnAgregar.Enabled = false;
                btnAgregar.Visible = false;
                btnEditar.Visible = false;
                btnGuardar.Visible = true;
                btnEliminar.Enabled = false;
                btnEliminar.Visible = false;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            // retrieving current row edit index from viewstate
            editindex = Convert.ToInt32(ViewState["editindex"].ToString());

            TextBox etxtgrvCodcat = (TextBox)grvCatalogo.Rows[editindex].FindControl("etxtgrvCodcat");
            TextBox etxtgrvOrden = (TextBox)grvCatalogo.Rows[editindex].FindControl("etxtgrvOrden");
            TextBox etxtgrvNomcat = (TextBox)grvCatalogo.Rows[editindex].FindControl("etxtgrvNomcat");
            TextBox etxtgrvCodref = (TextBox)grvCatalogo.Rows[editindex].FindControl("etxtgrvCodref");
            TextBox etxtgrvNomgru = (TextBox)grvCatalogo.Rows[editindex].FindControl("etxtgrvNomgru");


            if (etxtgrvCodcat.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un codigo.";
                etxtgrvCodcat.Focus();
                return;
            }
            if (etxtgrvOrden.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese el orden.";
                etxtgrvOrden.Focus();
                return;
            }
            if (etxtgrvNomcat.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese el nombre.";
                etxtgrvNomcat.Focus();
                return;
            }
            if (etxtgrvCodref.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese el codigo de refrencia.";
                etxtgrvCodref.Focus();
                return;
            }

            if (etxtgrvNomgru.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un grupo.";
                etxtgrvNomgru.Focus();
                return;
            }

            objCatalogo.Codcat = Session["gs_grvCodcat"].ToString();
            objCatalogo.Orden = Decimal.Parse(etxtgrvOrden.Text.Trim());
            objCatalogo.Nomcat = etxtgrvNomcat.Text.Trim();
            objCatalogo.Codref = etxtgrvCodref.Text.Trim();
            objCatalogo.Nomgru = etxtgrvNomgru.Text.Trim();
            objCatalogo.Tipcat = tvCatalogo.SelectedNode.Value;


            clsError objError = objCatalogo.f_Catalogo_Actualizar(objCatalogo, Session["gs_Codemp"].ToString(), Session["gs_UsuIng"].ToString(), DateTime.Now.ToString(), Session["gs_CodUs1"].ToString(), DateTime.Now.ToString(), "Update");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                grvCatalogo.EditIndex = -1;
                f_BindGrid_Inicial();
                btnAgregar.Enabled = true;
                btnAgregar.Visible = true;
                btnEditar.Visible = true;
                btnGuardar.Visible = false;
                btnEliminar.Enabled = true;
                btnEliminar.Visible = true;
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvCatalogo.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;
                    Label lblgrvCodcat = (Label)grvCatalogo.Rows[editindex].FindControl("lblgrvCodcat");
                    lblEliCli.Text += " / " + lblgrvCodcat.Text;
                    rowCount++;
                }
            }

            if (rowCount > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar una clase para eliminar.";
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            int rowCount = 0;

            clsError objError = new clsError();

            foreach (GridViewRow gvrow in grvCatalogo.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;

                    // retrieve Id from DataKeys to delete record

                    Label lblgrvCodcat = (Label)grvCatalogo.Rows[editindex].FindControl("lblgrvCodcat");

                    objCatalogo.Codcat = lblgrvCodcat.Text;
                    objCatalogo.Tipcat = tvCatalogo.SelectedNode.Value;


                    objError = objCatalogo.f_Catalogo_Eliminar(objCatalogo, Session["gs_Codemp"].ToString(), "Delete");
                    if (!String.IsNullOrEmpty(objError.Mensaje))
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        break;
                    }

                    rowCount++;

                }
            }



            if (rowCount > 0 && String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblEliCli.Text = "";
                lblExito.Text = "✔️ Registro eliminado.";

                f_BindGrid_Inicial();
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "Selecciona una fila para eliminar";
            }
        }

        protected void ddlCatalogoTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            f_BindGrid_Inicial();
        }

        protected void tvCatalogo_SelectedNodeChanged(object sender, EventArgs e)
        {
            f_BindGrid_Inicial();
            //f_BindGrid_Tree();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            grvCatalogo.EditIndex = -1;
            f_BindGrid_Inicial();

            btnAgregar.Enabled = true;
            btnAgregar.Visible = true;
            btnEditar.Visible = true;
            btnGuardar.Visible = false;
            btnEliminar.Enabled = true;
            btnEliminar.Visible = true;
            lblError.Text = lblExito.Text = "";
        }
    }
}