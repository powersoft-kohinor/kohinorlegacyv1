﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_cfg_Moneda : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "9", niv001 = "2", niv002 = "6";
        protected cls_cfg_Moneda objMoneda = new cls_cfg_Moneda();
        int editindex = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // hide Update button on page load                
                btnGuardar.Visible = false;
                f_Seg_Usuario_Menu();   
                f_BindGrid_Inicial();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAgregar.Attributes.Add("class", "btn btn-dark");
                btnEditar.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-secondary disabled");
                btnEliminar.Attributes.Add("class", "btn btn-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");
                grvRenglonesMoneda.PagerStyle.CssClass = "";
            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAgregar.Attributes.Add("class", "btn btn-outline-dark");
                btnEditar.Attributes.Add("class", "btn btn-outline-primary");
                btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");
                grvRenglonesMoneda.PagerStyle.CssClass = "pagination-ys justify-content-center align-items-center";
            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Monedas
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnAgregar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEditar.Attributes.Add("class", "btn btn-outline-secondary disabled");

                    }
                    if (row["banbus"].Equals("N")) //Abrir O Buscar
                    {
                        txtSearch.Enabled = false;
                        lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 disabled");
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                    }
                }
            }
            return dt;
        }

        private void f_BindGrid_Inicial()
        {
            objMoneda = objMoneda.f_Moneda_Buscar();
            if (String.IsNullOrEmpty(objMoneda.Error.Mensaje))
            {
                DataTable dt = objMoneda.dtClienteClase;
                Session.Add("gdt_ClienteClase", dt);
                if (dt.Rows.Count > 0)
                {
                    grvRenglonesMoneda.DataSource = dt;
                    grvRenglonesMoneda.DataBind();
                }
                else
                {
                    // add new row when the dataset is having zero record
                    dt.Rows.Add(dt.NewRow());
                    grvRenglonesMoneda.DataSource = dt;
                    grvRenglonesMoneda.DataBind();
                    grvRenglonesMoneda.Rows[0].Visible = false;
                }
            }
            else
            {
                f_ErrorNuevo(objMoneda.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        private void f_BindGrid(string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_ClienteClase"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "nommon LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codmon LIKE " + "'%" + searchText.Trim() + "%'";
                    grvRenglonesMoneda.DataSource = dv;
                }
                grvRenglonesMoneda.DataBind();
            }
        }

        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(txtSearch.Text);
        }

        protected void grvRenglonesClienteClase_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRenglonesMoneda.PageIndex = e.NewPageIndex;
            f_BindGrid(txtSearch.Text);
            lblError.Text = lblExito.Text = "";
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            TextBox ftxtgrvCodmon = (TextBox)(grvRenglonesMoneda.FooterRow.FindControl("ftxtgrvCodmon"));
            TextBox ftxtgrvNommon = (TextBox)(grvRenglonesMoneda.FooterRow.FindControl("ftxtgrvNommon"));
            TextBox ftxtgrvSimmon = (TextBox)(grvRenglonesMoneda.FooterRow.FindControl("ftxtgrvSimmon"));

            if (ftxtgrvCodmon.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un codigo de la moneda.";
                ftxtgrvCodmon.Focus();
                return;
            }
            if (ftxtgrvNommon.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un nombre.";
                ftxtgrvNommon.Focus();
                return;
            }
            if (ftxtgrvSimmon.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese el simbolo de la moneda.";
                ftxtgrvSimmon.Focus();
                return;
            }


            objMoneda.Codmon = ftxtgrvCodmon.Text.Trim();
            objMoneda.Simmon = ftxtgrvSimmon.Text.Trim();
            objMoneda.Nommon = ftxtgrvNommon.Text.Trim();


            clsError objError = objMoneda.f_Moneda_Actualizar(objMoneda, null, Session["gs_UsuIng"].ToString(), DateTime.Now.ToString(), Session["gs_CodUs1"].ToString(), DateTime.Now.ToString(), "Add");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                f_BindGrid_Inicial();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblExito.Text = "";
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvRenglonesMoneda.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for edit
                    editindex = gvrow.RowIndex;

                    rowCount++;
                    if (rowCount > 1)
                    {
                        break;
                    }
                }
            }

            if (rowCount > 1)
            {
                lblError.Text = "Seleccione solo una fila para editar";
            }
            else if (rowCount == 0)
            {
                lblError.Text = "Seleccione una fila para editar";
            }
            else
            {
                ViewState["editindex"] = editindex;
                Label lblgrvCodmon = (Label)grvRenglonesMoneda.Rows[editindex].FindControl("lblgrvCodmon");
                Session.Add("gs_grvCodmon", lblgrvCodmon.Text.Trim());

                grvRenglonesMoneda.EditIndex = editindex;
                f_BindGrid_Inicial();



                // disable all row checkboxes while editing
                foreach (GridViewRow gvrow in grvRenglonesMoneda.Rows)
                {
                    CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                    chkRow.Enabled = false;
                }

                //hide footer row while editing
                grvRenglonesMoneda.FooterRow.Visible = false;

                // hide and disable respective buttons
                btnAgregar.Enabled = false;
                btnAgregar.Visible = false;
                btnEditar.Visible = false;
                btnGuardar.Visible = true;
                btnEliminar.Enabled = false;
                btnEliminar.Visible = false;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            // retrieving current row edit index from viewstate
            editindex = Convert.ToInt32(ViewState["editindex"].ToString());



            TextBox etxtgrvNommon = (TextBox)grvRenglonesMoneda.Rows[editindex].FindControl("etxtgrvNommon");
            TextBox etxtgrvCodmon = (TextBox)grvRenglonesMoneda.Rows[editindex].FindControl("etxtgrvCodmon");
            TextBox etxtgrvSimmon = (TextBox)grvRenglonesMoneda.Rows[editindex].FindControl("etxtgrvSimmon");


            if (etxtgrvNommon.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un nombre.";
                etxtgrvNommon.Focus();
                return;
            }
            if (etxtgrvCodmon.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un codigo de moneda.";
                etxtgrvCodmon.Focus();
                return;
            }
            if (etxtgrvSimmon.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese el simbolo de la moneda.";
                etxtgrvSimmon.Focus();
                return;
            }



            objMoneda.Codmon = etxtgrvCodmon.Text.Trim();
            objMoneda.Simmon = etxtgrvSimmon.Text.Trim();
            objMoneda.Nommon = etxtgrvNommon.Text.Trim();

            clsError objError = objMoneda.f_Moneda_Actualizar(objMoneda, etxtgrvCodmon.Text.Trim(), Session["gs_UsuIng"].ToString(), DateTime.Now.ToString(), Session["gs_CodUs1"].ToString(), DateTime.Now.ToString(), "Update");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                grvRenglonesMoneda.EditIndex = -1;
                f_BindGrid_Inicial();
                btnAgregar.Enabled = true;
                btnAgregar.Visible = true;
                btnEditar.Visible = true;
                btnGuardar.Visible = false;
                btnEliminar.Enabled = true;
                btnEliminar.Visible = true;
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvRenglonesMoneda.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;
                    Label lblgrvCodmon = (Label)grvRenglonesMoneda.Rows[editindex].FindControl("lblgrvCodmon");
                    lblEliCli.Text += " / " + lblgrvCodmon.Text;
                    rowCount++;
                }
            }

            if (rowCount > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar una clase para eliminar.";
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            int rowCount = 0;

            clsError objError = new clsError();

            foreach (GridViewRow gvrow in grvRenglonesMoneda.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;

                    // retrieve Id from DataKeys to delete record

                    Label lblgrvCodmon = (Label)grvRenglonesMoneda.Rows[editindex].FindControl("lblgrvCodmon");

                    objError = objMoneda.f_Moneda_Eliminar(lblgrvCodmon.Text, "Delete");
                    if (!String.IsNullOrEmpty(objError.Mensaje))
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        break;
                    }

                    rowCount++;

                }
            }



            if (rowCount > 0 && String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblEliCli.Text = "";
                lblExito.Text = "✔️ Registro eliminado.";

                f_BindGrid_Inicial();
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "Selecciona una fila para eliminar";
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            grvRenglonesMoneda.EditIndex = -1;
            f_BindGrid_Inicial();

            btnAgregar.Enabled = true;
            btnAgregar.Visible = true;
            btnEditar.Visible = true;
            btnGuardar.Visible = false;
            btnEliminar.Enabled = true;
            btnEliminar.Visible = true;
            lblError.Text = lblExito.Text = "";

        }


    }
}