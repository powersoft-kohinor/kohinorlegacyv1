﻿<%@ Page Title="" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_cp_Dashboard.aspx.cs" Inherits="prj_KohinorWeb.w_cp_Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item active">Escritorio</li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <asp:Label ID="lblError" runat="server" ForeColor="Maroon" align="right"></asp:Label>
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <%--<a class="c-subheader-nav-link">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary" type="button" id="">
                            <i class="cil-filter"></i>
                        </button>
                    </div>

                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
                </div>
                <asp:LinkButton ID="lkbtnSearch" runat="server" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-speech"></i>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-graph"></i>&nbsp;
                                    Escritorio
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-settings"></i>
            </a>--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
      <div class="container-fluid">
            <%--  <div id="ui-view">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-gradient-primary">
                                <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                    <div>
                                        <div class="text-value-lg"></div>
                                        <div>Ventas del Dia</div>
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <svg class="c-icon">
                                                <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-settings"></use>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a></div>
                                    </div>
                                </div>
                                <div class="c-chart-wrapper mt-3 mx-3" style="height: 70px;">
                                    <canvas class="chart" id="card-chart1" height="70"></canvas>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-gradient-info">
                                <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                    <div>
                                        <div class="text-value-lg"></div>
                                        <div>Ventas Total</div>
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <svg class="c-icon">
                                                <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-settings"></use>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a></div>
                                    </div>
                                </div>
                                <div class="c-chart-wrapper mt-3 mx-3" style="height: 70px;">
                                    <canvas class="chart" id="card-chart2" height="70"></canvas>



                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-gradient-warning">
                                <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                    <div>
                                        <div class="text-value-lg"></div>
                                        <div>Ventas Parcial</div>
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <svg class="c-icon">
                                                <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-settings"></use>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a></div>
                                    </div>
                                </div>
                                <div class="c-chart-wrapper mt-3" style="height: 70px;">
                                    <canvas class="chart" id="card-chart3" height="70"></canvas>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-gradient-danger">
                                <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                    <div>
                                        <div class="text-value-lg"></div>
                                        <div>Saldo</div>
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <svg class="c-icon">
                                                <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-settings"></use>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a></div>
                                    </div>
                                </div>
                          
                                <div class="c-chart-wrapper mt-3 mx-3" style="height: 70px;">
                                    <canvas class="chart" id="card-chart4" height="70"></canvas>

                                   



                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                    </div>--%>
                        <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <h4 class="card-title mb-0">Compras</h4>
                                    <div class="small text-muted">Modelo Tabla</div>
                                </div>
                                  </div>
                                <asp:UpdatePanel ID="updDashboard" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                 <asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
                                <div class="table table-responsive" style="overflow-x: auto;">
                                    <div class="thead-dark">
                                        <asp:GridView ID="grvDashboard" class="table table-bordered table-active table-active table-hover table-striped" runat="server" AutoGenerateColumns="False" AllowPaging="true" ShowHeaderWhenEmpty="True" AllowSorting="true" OnSorting="OnSorting" OnPageIndexChanging="OnPageIndexChanging" PageSize="10" OnRowDataBound="grvDashboard_RowDataBound" OnRowCreated="grvDashboard_RowCreated" OnSelectedIndexChanged="grvDashboard_SelectedIndexChanged">
                                 



                                            <Columns>
                                               
                                                <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="1%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                                <asp:BoundField DataField="numfac" HeaderText="Factura" SortExpression="numfac" />
                                                 <asp:BoundField DataField="fecfac" HeaderText="Fecha" SortExpression="fecfac" />
                                                <asp:BoundField DataField="codpro" HeaderText="Cod." SortExpression="codpro" />
                                                <asp:BoundField DataField="nompro" HeaderText="Nombre" SortExpression="nompro" />
                                                <asp:BoundField DataField="numser" HeaderText="Serie" SortExpression="numser" />
                                                <asp:BoundField DataField="numcom" HeaderText="numcom" SortExpression="numcom" />
                                                <asp:BoundField DataField="clacon" HeaderText="Clave" SortExpression="clacon" />
                                                <asp:BoundField DataField="feccom" HeaderText="Fecha Com." SortExpression="feccom" />
                                                <asp:BoundField DataField="totnet" HeaderText="Total Neto" SortExpression="totnet" />
                                                <asp:BoundField DataField="totiv0" HeaderText="IVA 0" SortExpression="totiv0" />
                                                <asp:BoundField DataField="totbas" HeaderText=" Base" SortExpression="totbas" />
                                                <asp:BoundField DataField="totiva" HeaderText="IVA" SortExpression="totiva" />
                                                <asp:BoundField DataField="totfac" HeaderText="Total Factura" SortExpression="totfac" />
                                          

                                               
                                            </Columns>
                                            <HeaderStyle Font-Bold="True" ForeColor="White" />
                                            <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />

                                        </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>


                              </div>
                              </div>

                      </div>

                    <!-- /.row-->
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <h4 class="card-title mb-0">Cartera Vencida</h4>
                                    <div class="small text-muted">Datos por Fecha</div>
                                </div>

                                 <div class="form-group col-md-2">
                                        <asp:TextBox ID="txtFecini" runat="server" class="form-control form-control-sm" TextMode="Date" OnTextChanged="txtFecini_TextChanged" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <asp:TextBox ID="txtFecfin" runat="server" class="form-control form-control-sm" TextMode="Date" OnTextChanged="txtFecfin_TextChanged" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                <div class="btn-toolbar d-none d-md-block" role="toolbar" aria-label="Toolbar with buttons">
                                    
                                   
                                  
                                    <button type="button" class="btn btn-outline-dark" id="lkbtnFinal" runat="server" onserverclick="lkbtnDescargar_Click"><i class="cil-cloud-download"></i></button>

                                   
                                </div>
                            </div>
                            <%--<div class="c-chart-wrapper" style="height: 700px; margin-top: 40px;">--%>
                                 <asp:Literal ID="ltChart" runat="server"></asp:Literal>
                                <%--<canvas class="chart" id="hojacentral" height="700"></canvas>--%>
                               
                            


                            



                                <%--<asp:Label ID="prueba" runat="server" Text="Label"></asp:Label>--%>
                               <%-- <script>
                                    for (var i = 0; i < player.length; i++) {
                                        var t1 = player[i];
                                    }
                                    

                                    var hojacentral = new Chart(document.getElementById('hojacentral'), {
                                        type: 'line',
                                        data: {
                                            labels: ['MAYOR 120', 'MAYOR 120','90-120', 'MAYOR 120', 'MAYOR 120', 'MAYOR 120'],
                                            datasets: [{
                                                label: 'Ventas',
                                                backgroundColor: coreui.Utils.hexToRgba(coreui.Utils.getStyle('--info'), 10),
                                                borderColor: coreui.Utils.getStyle('--info'),
                                                pointHoverBackgroundColor: '#fff',
                                                borderWidth: 2,
                                                data: t1
                                            
                                            }]
                                        },
                                        options: {
                                            maintainAspectRatio: false,
                                            legend: {
                                                display: false
                                            },
                                            scales: {
                                                xAxes: [{
                                                    gridLines: {
                                                        drawOnChartArea: false
                                                    }
                                                }],
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero: true,
                                                        maxTicksLimit: 5,
                                                        stepSize: Math.ceil(1000 / 5),
                                                        max: 800
                                                    }
                                                }]
                                            },
                                            elements: {
                                                point: {
                                                    radius: 0,
                                                    hitRadius: 10,
                                                    hoverRadius: 4,
                                                    hoverBorderWidth: 3
                                                }
                                            }
                                        }
                                    });


                                </script>--%>




                            <%--</div>--%>
                        </div>
                <%--        <div class="card-footer">
                            <div class="row text-center">
                                <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                    <div class="text-muted">Visits</div>
                                    <strong>29.703 Users (40%)</strong>
                                    <div class="progress progress-xs mt-2">
                                        <div class="progress-bar bg-gradient-success" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                    <div class="text-muted">Unique</div>
                                    <strong>24.093 Users (20%)</strong>
                                    <div class="progress progress-xs mt-2">
                                        <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                    <div class="text-muted">Pageviews</div>
                                    <strong>78.706 Views (60%)</strong>
                                    <div class="progress progress-xs mt-2">
                                        <div class="progress-bar bg-gradient-warning" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                    <div class="text-muted">New Users</div>
                                    <strong>22.123 Users (80%)</strong>
                                    <div class="progress progress-xs mt-2">
                                        <div class="progress-bar bg-gradient-danger" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>


                                <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                    <div class="text-muted">Bounce Rate</div>
                                    <strong>40.15%</strong>
                                    <div class="progress progress-xs mt-2">
                                        <div class="progress-bar" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                    </div>
                    <!-- /.card-->

                    <%--<div class="row">
                        <div class="col-sm-6 col-lg-4">
                            <div class="card">
                                <div class="card-header bg-facebook content-center">
                                    <svg class="c-icon c-icon-3xl text-white my-4">
                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-facebook-f"></use>
                                    </svg>
                                </div>
                                <div class="card-body row text-center">
                                    <div class="col">
                                        <div class="text-value-xl">89k</div>
                                        <div class="text-uppercase text-muted small">friends</div>
                                    </div>
                                    <div class="c-vr"></div>
                                    <div class="col">
                                        <div class="text-value-xl">459</div>
                                        <div class="text-uppercase text-muted small">feeds</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                        <div class="col-sm-6 col-lg-4">
                            <div class="card">
                                <div class="card-header bg-twitter content-center">
                                    <svg class="c-icon c-icon-3xl text-white my-4">
                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-twitter"></use>
                                    </svg>
                                </div>
                                <div class="card-body row text-center">
                                    <div class="col">
                                        <div class="text-value-xl">973k</div>
                                        <div class="text-uppercase text-muted small">followers</div>
                                    </div>
                                    <div class="c-vr"></div>
                                    <div class="col">
                                        <div class="text-value-xl">1.792</div>
                                        <div class="text-uppercase text-muted small">tweets</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                        <div class="col-sm-6 col-lg-4">
                            <div class="card">
                                <div class="card-header bg-linkedin content-center">
                                    <svg class="c-icon c-icon-3xl text-white my-4">
                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-linkedin"></use>
                                    </svg>
                                </div>
                                <div class="card-body row text-center">
                                    <div class="col">
                                        <div class="text-value-xl">500+</div>
                                        <div class="text-uppercase text-muted small">contacts</div>
                                    </div>
                                    <div class="c-vr"></div>
                                    <div class="col">
                                        <div class="text-value-xl">292</div>
                                        <div class="text-uppercase text-muted small">feeds</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                    </div>--%>
                    <!-- /.row-->
                   <%-- <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">Traffic &amp; Sales</div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="c-callout c-callout-info">
                                                        <small class="text-muted">New Clients</small>
                                                        <div class="text-value-lg">9,123</div>
                                                    </div>
                                                </div>
                                                <!-- /.col-->
                                                <div class="col-6">
                                                    <div class="c-callout c-callout-danger">
                                                        <small class="text-muted">Recuring Clients</small>
                                                        <div class="text-value-lg">22,643</div>
                                                    </div>
                                                </div>
                                                <!-- /.col-->
                                            </div>
                                            <!-- /.row-->
                                            <hr class="mt-0">
                                            <div class="progress-group mb-4">
                                                <div class="progress-group-prepend"><span class="progress-group-text">Monday</span></div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-danger" role="progressbar" style="width: 78%" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group mb-4">
                                                <div class="progress-group-prepend"><span class="progress-group-text">Tuesday</span></div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 56%" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-danger" role="progressbar" style="width: 94%" aria-valuenow="94" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group mb-4">
                                                <div class="progress-group-prepend"><span class="progress-group-text">Wednesday</span></div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 12%" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-danger" role="progressbar" style="width: 67%" aria-valuenow="67" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group mb-4">
                                                <div class="progress-group-prepend"><span class="progress-group-text">Thursday</span></div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 43%" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-danger" role="progressbar" style="width: 91%" aria-valuenow="91" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group mb-4">
                                                <div class="progress-group-prepend"><span class="progress-group-text">Friday</span></div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 22%" aria-valuenow="22" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-danger" role="progressbar" style="width: 73%" aria-valuenow="73" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group mb-4">
                                                <div class="progress-group-prepend"><span class="progress-group-text">Saturday</span></div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 53%" aria-valuenow="53" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-danger" role="progressbar" style="width: 82%" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group mb-4">
                                                <div class="progress-group-prepend"><span class="progress-group-text">Sunday</span></div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 9%" aria-valuenow="9" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-danger" role="progressbar" style="width: 69%" aria-valuenow="69" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col-->
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="c-callout c-callout-warning">
                                                        <small class="text-muted">Pageviews</small>
                                                        <div class="text-value-lg">78,623</div>
                                                    </div>
                                                </div>
                                                <!-- /.col-->
                                                <div class="col-6">
                                                    <div class="c-callout c-callout-success">
                                                        <small class="text-muted">Organic</small>
                                                        <div class="text-value-lg">49,123</div>
                                                    </div>
                                                </div>
                                                <!-- /.col-->
                                            </div>
                                            <!-- /.row-->
                                            <hr class="mt-0">
                                            <div class="progress-group">
                                                <div class="progress-group-header">
                                                    <svg class="c-icon progress-group-icon">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-user"></use>
                                                    </svg>
                                                    <div>Male</div>
                                                    <div class="mfs-auto font-weight-bold">43%</div>
                                                </div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-warning" role="progressbar" style="width: 43%" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group mb-5">
                                                <div class="progress-group-header">
                                                    <svg class="c-icon progress-group-icon">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-user-female"></use>
                                                    </svg>
                                                    <div>Female</div>
                                                    <div class="mfs-auto font-weight-bold">37%</div>
                                                </div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-warning" role="progressbar" style="width: 43%" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group">
                                                <div class="progress-group-header align-items-end">
                                                    <svg class="c-icon progress-group-icon">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-google"></use>
                                                    </svg>
                                                    <div>Organic Search</div>
                                                    <div class="mfs-auto font-weight-bold mfe-2">191.235</div>
                                                    <div class="text-muted small">(56%)</div>
                                                </div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-success" role="progressbar" style="width: 56%" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group">
                                                <div class="progress-group-header align-items-end">
                                                    <svg class="c-icon progress-group-icon">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-facebook-f"></use>
                                                    </svg>
                                                    <div>Facebook</div>
                                                    <div class="mfs-auto font-weight-bold mfe-2">51.223</div>
                                                    <div class="text-muted small">(15%)</div>
                                                </div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-success" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group">
                                                <div class="progress-group-header align-items-end">
                                                    <svg class="c-icon progress-group-icon">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-twitter"></use>
                                                    </svg>
                                                    <div>Twitter</div>
                                                    <div class="mfs-auto font-weight-bold mfe-2">37.564</div>
                                                    <div class="text-muted small">(11%)</div>
                                                </div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-success" role="progressbar" style="width: 11%" aria-valuenow="11" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-group">
                                                <div class="progress-group-header align-items-end">
                                                    <svg class="c-icon progress-group-icon">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-linkedin"></use>
                                                    </svg>
                                                    <div>LinkedIn</div>
                                                    <div class="mfs-auto font-weight-bold mfe-2">27.319</div>
                                                    <div class="text-muted small">(8%)</div>
                                                </div>
                                                <div class="progress-group-bars">
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-success" role="progressbar" style="width: 8%" aria-valuenow="8" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col-->
                                    </div>
                                    <!-- /.row-->
                                    <br>
                                    <table class="table table-responsive-sm table-hover table-outline mb-0">
                                        <thead class="thead-light">
                                            <tr>
                                                <th class="text-center">
                                                    <svg class="c-icon">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-people"></use>
                                                    </svg>
                                                </th>
                                                <th>User</th>
                                                <th class="text-center">Country</th>
                                                <th>Usage</th>
                                                <th class="text-center">Payment Method</th>
                                                <th>Activity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="c-avatar">
                                                        <img class="c-avatar-img" src="CoreUI/assets/img/avatars/1.jpg" alt="user@email.com"><span class="c-avatar-status bg-success"></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>Yiorgos Avraamu</div>
                                                    <div class="small text-muted"><span>New</span> | Registered: Jan 1, 2015</div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/flag.svg#cif-us"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="clearfix">
                                                        <div class="float-left"><strong>50%</strong></div>
                                                        <div class="float-right"><small class="text-muted">Jun 11, 2015 - Jul 10, 2015</small></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-cc-mastercard"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="small text-muted">Last login</div>
                                                    <strong>10 sec ago</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="c-avatar">
                                                        <img class="c-avatar-img" src="CoreUI/assets/img/avatars/2.jpg" alt="user@email.com"><span class="c-avatar-status bg-danger"></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>Avram Tarasios</div>
                                                    <div class="small text-muted"><span>Recurring</span> | Registered: Jan 1, 2015</div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/flag.svg#cif-br"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="clearfix">
                                                        <div class="float-left"><strong>10%</strong></div>
                                                        <div class="float-right"><small class="text-muted">Jun 11, 2015 - Jul 10, 2015</small></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-cc-visa"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="small text-muted">Last login</div>
                                                    <strong>5 minutes ago</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="c-avatar">
                                                        <img class="c-avatar-img" src="CoreUI/assets/img/avatars/3.jpg" alt="user@email.com"><span class="c-avatar-status bg-warning"></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>Quintin Ed</div>
                                                    <div class="small text-muted"><span>New</span> | Registered: Jan 1, 2015</div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/flag.svg#cif-in"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="clearfix">
                                                        <div class="float-left"><strong>74%</strong></div>
                                                        <div class="float-right"><small class="text-muted">Jun 11, 2015 - Jul 10, 2015</small></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-warning" role="progressbar" style="width: 74%" aria-valuenow="74" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-cc-stripe"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="small text-muted">Last login</div>
                                                    <strong>1 hour ago</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="c-avatar">
                                                        <img class="c-avatar-img" src="CoreUI/assets/img/avatars/4.jpg" alt="user@email.com"><span class="c-avatar-status bg-secondary"></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>Enéas Kwadwo</div>
                                                    <div class="small text-muted"><span>New</span> | Registered: Jan 1, 2015</div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/flag.svg#cif-fr"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="clearfix">
                                                        <div class="float-left"><strong>98%</strong></div>
                                                        <div class="float-right"><small class="text-muted">Jun 11, 2015 - Jul 10, 2015</small></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-danger" role="progressbar" style="width: 98%" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-cc-paypal"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="small text-muted">Last login</div>
                                                    <strong>Last month</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="c-avatar">
                                                        <img class="c-avatar-img" src="CoreUI/assets/img/avatars/5.jpg" alt="user@email.com"><span class="c-avatar-status bg-success"></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>Agapetus Tadeáš</div>
                                                    <div class="small text-muted"><span>New</span> | Registered: Jan 1, 2015</div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/flag.svg#cif-es"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="clearfix">
                                                        <div class="float-left"><strong>22%</strong></div>
                                                        <div class="float-right"><small class="text-muted">Jun 11, 2015 - Jul 10, 2015</small></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-info" role="progressbar" style="width: 22%" aria-valuenow="22" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-cc-apple-pay"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="small text-muted">Last login</div>
                                                    <strong>Last week</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="c-avatar">
                                                        <img class="c-avatar-img" src="CoreUI/assets/img/avatars/6.jpg" alt="user@email.com"><span class="c-avatar-status bg-danger"></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>Friderik Dávid</div>
                                                    <div class="small text-muted"><span>New</span> | Registered: Jan 1, 2015</div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/flag.svg#cif-pl"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="clearfix">
                                                        <div class="float-left"><strong>43%</strong></div>
                                                        <div class="float-right"><small class="text-muted">Jun 11, 2015 - Jul 10, 2015</small></div>
                                                    </div>
                                                    <div class="progress progress-xs">
                                                        <div class="progress-bar bg-gradient-success" role="progressbar" style="width: 43%" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/brand.svg#cib-cc-amex"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div class="small text-muted">Last login</div>
                                                    <strong>Yesterday</strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                    </div>
                </div>--%>
                <!-- /.row-->
                     </div>
            </div>
        </div>
    </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <!--f SQL DATASOURCES-->

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>
</asp:Content>
