﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace prj_KohinorWeb
{
    public partial class w_cp_Dashboard : System.Web.UI.Page
    {
        protected cls_cp_Dasboard objDashboard = new cls_cp_Dasboard();

        protected void Page_Init(object sender, EventArgs e)
        {
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["gs_Modulo"] != null) //al cambiar de modulo
                {
                    string[] s_modulo = Request.QueryString["gs_Modulo"].Split('@');
                    Session["gs_Codmod"] = s_modulo[0];
                    Session["gs_Nommod"] = s_modulo[1];
                }
                if (Request.QueryString["gs_RedirectError"] != null) //al cambiar a pag en otro modulo por URL
                {
                    lblError.Text = Request.QueryString["gs_RedirectError"];
                }
                f_Seg_Usuario_Menu();
            }

            f_BindGrid_Inicial();


            f_Dashboard_Fac(DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));


        }


       
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }

        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
        }

        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }

        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                //if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente
                //{
                //    if (row["banins"].Equals("N")) //Nuevo
                //    {
                //        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnNuevo.Disabled = true;
                //    }
                //    if (row["banbus"].Equals("N")) //Abrir
                //    {
                //        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnAbrir.Disabled = true;
                //    }
                //    if (row["bangra"].Equals("N")) //Guardar
                //    {
                //        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnGuardar.Disabled = true;
                //    }
                //    if (row["bancan"].Equals("N")) //Cancelar
                //    {
                //        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnCancelar.Disabled = true;

                //    }
                //    if (row["baneli"].Equals("N")) //Eliminiar
                //    {
                //        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnEliminar.Disabled = true;
                //        Session["baneli"] = "N";
                //    }
                //}
            }
            return dt;
        }

        protected void grvDashboard_RowDataBound(object sender, GridViewRowEventArgs e) //only fires when the GridView's data changes during the postback
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }       

        protected void grvDashboard_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }
        }

        protected void grvDashboard_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvDashboard.SelectedRow;
            string s_numfac = fila.Cells[2].Text.Trim();

            Session["gs_numfacBienSelected"] = s_numfac; //variable para que al seleccionar uno en el grv se obtenga el codcli (sg_codcliSELECTED ->Editar)
            Response.Redirect("w_cp_FacturaBienesAdministrar.aspx");

        }

        private void f_BindGrid_Inicial()
        {
            objDashboard = objDashboard.f_Compra(Session["gs_Codemp"].ToString(), DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
            
            DataTable dt = objDashboard.dtDashboard;

            grvDashboard.DataSource = dt;
            f_BindGrid();
        }
        private void f_BindGrid(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Factura"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "codpro LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "nompro LIKE " + "'%" + searchText.Trim() + "%'";
                    grvDashboard.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirection;
                    grvDashboard.DataSource = dv;
                }
                grvDashboard.DataBind();
            }
        }
        private string SortDirection
        {
            get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }
        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(null, txtSearch.Text);
        }
        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvDashboard.PageIndex = e.NewPageIndex;
            f_BindGrid(null, txtSearch.Text);
        }
        protected void OnSorting(object sender, GridViewSortEventArgs e)
        {
            f_BindGrid(e.SortExpression, txtSearch.Text);
        }


        private void f_Dashboard(string s_fecing )
        {

            objDashboard = objDashboard.f_Cartera(Session["gs_Codemp"].ToString(), s_fecing);
            if (String.IsNullOrEmpty(objDashboard.Error.Mensaje))
            {
                DataTable tb = objDashboard.dtDashboard;

                if (tb != null)
                {
                    String chart = "";

                    chart = "<div class=\"c-chart-wrapper\" style =\"height: 700px; margin-top: 40px;\"> <canvas id=\"line-chart\" class=\"chart\" height=\"700\"></canvas></div>";
                    chart += "<script>";

                    chart += "new Chart(document.getElementById(\"line-chart\"), {" +
                        " type: 'line', " +
                        "data: " +
                        "{" +
                            "labels: [";
                    // get data from database and add to chart
                    String label = "";
                    for (int i = 0; i < tb.Rows.Count; i++)
                        label += "'" + tb.Rows[i]["dias"].ToString() + "',";
                    label = label.Substring(0, label.Length - 1);
                    chart += label;

                    chart += "],";
                    chart += "datasets: [{label: 'Ventas',backgroundColor: coreui.Utils.hexToRgba(coreui.Utils.getStyle('--info'), 10),borderColor: coreui.Utils.getStyle('--info'),pointHoverBackgroundColor: '#fff',borderWidth: 2,data:[ ";

                    // get data from database and add to chart
                    String value = "";
                    for (int i = 0; i < tb.Rows.Count; i++)
                        value += tb.Rows[i]["valsal"].ToString() + ",";
                    value = value.Substring(0, value.Length - 1);
                    chart += value;


                    chart += "]}]},";


                    chart += "options: {maintainAspectRatio: false, legend: {display: false},scales:{xAxes:[{gridLines:{drawOnChartArea: false}}],yAxes:[{ticks:{beginAtZero: true,maxTicksLimit: 5,stepSize: Math.ceil(1000 / 5),max: 800}}]},elements:{point:{radius: 0,hitRadius: 10,hoverRadius: 4,hoverBorderWidth: 3}}}";

                    chart += "});";
                    chart += "</script>";

                    ltChart.Text = chart;



                    //List<Decimal> valsal_Tot = new List<Decimal>();

                    //for (int i = 0; i < tb.Rows.Count; i++)
                    //    valsal_Tot.Add(Convert.ToDecimal(tb.Rows[i]["valsal"]));




                    //JavaScriptSerializer jss1 = new JavaScriptSerializer();
                    //string _myJSONstring = jss1.Serialize(valsal_Tot);
                    //string player = "var player=" + _myJSONstring + ";";
                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "player123", player, true);


                }

            }
        }
        private void f_Dashboard_Fac(DataTable dt)
        {

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "Customers");

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=ReporteFacturaBienes.xlsx");
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }


        }

        protected void lkbtnDescargar_Click(object sender, EventArgs e)
        {
            DataTable tb = objDashboard.dtDashboard;
            f_Dashboard_Fac(tb);
        }
        

        private void f_Dashboard_Fac(string s_fecing, string s_fecfin)
        {

            objDashboard = objDashboard.f_Compra(Session["gs_Codemp"].ToString(), s_fecing, s_fecfin);
            if (String.IsNullOrEmpty(objDashboard.Error.Mensaje))
            {
                DataTable tb = objDashboard.dtDashboard;

                if (tb.Rows.Count > 0)
                {
                    String chart = "";

                    chart = "<div class=\"c-chart-wrapper\" style =\"height: 700px; margin-top: 40px;\"> <canvas id=\"line-chart\" class=\"chart\" height=\"700\"></canvas></div>";
                    chart += "<script>";

                    chart += "new Chart(document.getElementById(\"line-chart\"), {" +
                        " type: 'line', " +
                        "data: " +
                        "{" +
                            "labels: [";
                    // get data from database and add to chart
                    String label = "";
                    for (int i = 0; i < tb.Rows.Count; i++)
                        label += "'" + tb.Rows[i]["numfac"].ToString() + "',";
                    label = label.Substring(0, label.Length - 1);
                    chart += label;

                    chart += "],";
                    chart += "datasets: [{label: 'Ventas',backgroundColor: coreui.Utils.hexToRgba(coreui.Utils.getStyle('--info'), 10),borderColor: coreui.Utils.getStyle('--info'),pointHoverBackgroundColor: '#fff',borderWidth: 2,data:[ ";

                    // get data from database and add to chart
                    String value = "";
                    for (int i = 0; i < tb.Rows.Count; i++)
                        value += tb.Rows[i]["totfac"].ToString() + ",";
                    value = value.Substring(0, value.Length - 1);
                    chart += value;


                    chart += "]}]},";


                    chart += "options: {maintainAspectRatio: false, legend: {display: false},scales:{xAxes:[{gridLines:{drawOnChartArea: false}}],yAxes:[{ticks:{beginAtZero: true,maxTicksLimit: 5,stepSize: Math.ceil(1500 / 5),max: 1000}}]},elements:{point:{radius: 0,hitRadius: 10,hoverRadius: 4,hoverBorderWidth: 3}}}";

                    chart += "});";
                    chart += "</script>";

                    ltChart.Text = chart;



                    //List<Decimal> valsal_Tot = new List<Decimal>();

                    //for (int i = 0; i < tb.Rows.Count; i++)
                    //    valsal_Tot.Add(Convert.ToDecimal(tb.Rows[i]["valsal"]));




                    //JavaScriptSerializer jss1 = new JavaScriptSerializer();
                    //string _myJSONstring = jss1.Serialize(valsal_Tot);
                    //string player = "var player=" + _myJSONstring + ";";
                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "player123", player, true);


                }

            }
        }

        protected void txtFecini_TextChanged(object sender, EventArgs e)
        {
            string fecimi = txtFecini.Text;
            f_Dashboard_Fac(fecimi, DateTime.Now.ToString("yyyy-MM-dd"));
        }

        protected void txtFecfin_TextChanged(object sender, EventArgs e)
        {
            string fecfin = txtFecfin.Text;
            f_Dashboard_Fac(DateTime.Now.ToString("yyyy-MM-dd"), fecfin);
        }
    }
}