﻿<%@ Page Title="Devolucion Administrar" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_cp_DevolucionAdministrar.aspx.cs" Inherits="prj_KohinorWeb.w_cp_DevolucionAdministrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function f_ModalProveedor() {
            $("#btnShowProveedorModal").click();
        }
        function f_ModalProveedorNuevo() {
            $("#btnShowProveedorNuevoModal").click();
            <%--$("#<%=txtCodcli.ClientID%>").focus();--%>
            <%--$('#ClienteNuevoModal').on('shown.bs.modal', function () {
                $("#<%=txtCodcli.ClientID%>").focus();
            })--%>
        }

        function f_ModalFacturaArticulos() {
            $("#btnShowArticulosModal").click();
        }
     

       <%-- $(document).ready(function () {
            $("#<%=txtCodpro.ClientID%>").blur(function () {
                f_Validar();
            });
        });

        function f_Validar() {
            var s_cod = $("#<%=txtCodpro.ClientID%>").val();
            PageMethods.f_Validar(s_cod, f_SuccessValidacionCod, f_FailValidacionCod);
        }
        function f_SuccessValidacionCod(data) {
            if (data.TipInd == "-" || data.TipEmp == "-") {
                //alert("Solo se puede ingresar RUC = 13 Digitos, CI=10 Digitos, PASAPORTE=9 Digitos");
                alert(data.ErrorCed);
                $("#<%=txtTipind.ClientID%>").val("-");
                $("#<%=txtTipemp.ClientID%>").val("-");
                $("#<%=txtRucced.ClientID%>").val("-");
            }
            else {
                $("#<%=txtTipind.ClientID%>").val(data.TipInd);
                $("#<%=txtTipemp.ClientID%>").val(data.TipEmp);
                $("#<%=txtRucced.ClientID%>").val(data.RucCed);
            }
        }
        function f_FailValidacionCod() {
            alert("ERROR : Método getValues_Success(data) fallo.");
        }--%>





        //Tab en txtClicxc
        function buscarClicxc() {
            var text1 = $("#<%=txtProcxp.ClientID%>").val();
            PageMethods.f_buscarCli(text1, buscarClicxc_Success, buscarClicxc_Fail);
        }
        function buscarClicxc_Success(data) {
            if (data.Clicxc == "noEncontro") {
<%--                $("#<%=txtCodpro.ClientID%>").val(data.ClicxcModal);
                --%>
                f_ModalClienteNuevo();
                <%--$('#ClienteNuevoModal').modal('show');
                $('#ClienteNuevoModal').on('shown.bs.modal', function () {
                    $("#<%=txtCodcli.ClientID%>").focus();
                })--%>
            }
            else {
                $("#<%=txtProcxp.ClientID%>").val(data.Clicxc);
                $("#<%=txtNomcxp.ClientID%>").val(data.Nomcxc);
                $("#<%=txtLispre.ClientID%>").val(data.Lispre);

                $("#<%=txtObserv.ClientID%>").val(data.Observ);

            }
        }
        function buscarClicxc_Fail() {
            alert("ERROR : Método buscarClicxc_Success(data) fallo.");
        }






    </script>
</asp:Content>
<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>

<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item"><a href="w_cp_DevolucionAdministrar.aspx">Devoluciones y Nota Crédito</a></li>
            <li class="breadcrumb-item active">Administrar Devoluciones</li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <a class="c-subheader-nav-link">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary" type="button" id="">
                            <i class="cil-filter"></i>
                        </button>
                    </div>

                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
                </div>
                <asp:LinkButton ID="lkbtnSearch" runat="server" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
            </a>

            <a class="c-subheader-nav-link">
                <i class="c-icon cil-speech"></i>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-graph"></i>&nbsp;
                            Escritorio
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-settings"></i>
            </a>
        </div>
    </div>
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item active">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click"><i class="cil-file"></i>Nuevo</button>
                    <button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" onserverclick="btnAbrir_Click"><i class="cil-folder-open"></i>Abrir</button>
                    <button type="button" class="btn btn-outline-dark" id="btnGuardar" runat="server" onserverclick="btnGuardarFactura_Click"><i class="cil-save"></i>Guardar</button>
                    <button type="button" class="btn btn-outline-dark" id="btnCancelar" runat="server" onserverclick="btnCancelar_Click"><i class="cil-grid-slash"></i>Cancelar</button>
                    <button type="button" class="btn btn-outline-danger" id="btnEliminar" runat="server" onserverclick="lkbtnDelete_Click"><i class="cil-trash"></i>Eliminar</button>
                </div>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-dark" id="lkbtnPrincipio" runat="server" onserverclick="lkbtnPrincipio_Click"><i class="cil-media-step-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnAtras" runat="server" onserverclick="lkbtnAtras_Click"><i class="cil-media-skip-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnSiguiente" runat="server" onserverclick="lkbtnSiguiente_Click"><i class="cil-media-skip-forward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnFinal" runat="server" onserverclick="lkbtnFinal_Click"><i class="cil-media-step-forward"></i></button>
                </div>

                <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
            </li>
        </ol>

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
            
                <main class="c-main">
                    <div class="container-fluid">
                        <div class="fade-in">
                            <div id="div_Factura" runat="server" class="card bg-gradient-secondary">
                                <div class="card-header">
                                    <i class="c-icon cil-justify-center"></i><b>Devoluciones y Nota Crédito</b>
                                </div>

                                <div class="card-body">
                                    <div class="form-row">
                                        <div class="form-group col-md-9">
                                            <div class="form-row">
                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label2" runat="server" Text="Ruc/C.I."></asp:Label>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <div class="input-group">
                                                        <input type="text" id="txtProcxp" class="form-control form-control-sm" runat="server" />
                                                        <div class="input-group-append">
                                                            <asp:LinkButton ID="lkbtnBuscarProveedor" data-toggle="modal" data-target="#ProveedorModal" runat="server" class="btn btn-sm btn-primary">
                                                            <i class="cil-search"></i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <asp:TextBox ID="txtNomcxp" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                </div>

                                                 <div class="form-group col-md-1">
                                                      <asp:Label ID="Label10" runat="server" Text="Factura"></asp:Label>
                                                     </div>
                                                <div class="form-group col-md-2">
                                                    <div class="dropdown">
                                                      <button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Escoge Tipo Factura <span class="caret"></span></button>
                                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                        <li><a href="#" data-toggle="modal" data-target="#FacturaBienesModal">Bienes</a></li>
                                                        <li><a href="#" data-toggle="modal" data-target="#FacturaServiciosModal">Servicios</a></li>
                                                      </ul>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label5" runat="server" Text="Observ."></asp:Label>
                                                </div>

                                                <div class="form-group col-md-5">
                                                    <asp:TextBox ID="txtObserv" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <asp:DropDownList ID="ddlCodgeo" runat="server" class="form-control form-control-sm" DataSourceID="sqldsCodgeo" DataTextField="nomgeo" DataValueField="codgeo">
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <asp:DropDownList ID="ddlCodalm" runat="server" class="form-control form-control-sm" DataSourceID="sqldsCodalm" DataTextField="nomalm" DataValueField="codalm">
                                                    </asp:DropDownList>


                                                </div>
                                            </div>
                                            <div class="form-row">

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label1" runat="server" Text="Serie"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:TextBox ID="txtNumser" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label20" runat="server" Text="N°"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <asp:TextBox ID="txtNumcom" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label21" runat="server" Text="N° Aut."></asp:Label>
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <asp:TextBox ID="txtClacon" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label4" runat="server" Text="Lista"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <asp:TextBox ID="txtLispre" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                </div>

                                            </div>


                                            <div class="form-row">

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label22" runat="server" Text="Tipo"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <asp:DropDownList ID="ddlTipcom" runat="server" class="form-control form-control-sm" DataSourceID="sqldsTipcom" DataTextField="nomdoc" DataValueField="codsri">
                                                    </asp:DropDownList>
                                                </div>


                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label25" runat="server" Text="Fecha Factura"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <asp:TextBox ID="txtFeccom" runat="server" class="form-control form-control-sm" TextMode="Date"></asp:TextBox>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label26" runat="server" Text="ATS"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:DropDownList ID="ddlEstats" runat="server" class="form-control form-control-sm">
                                                        <asp:ListItem Value="S">SI</asp:ListItem>
                                                        <asp:ListItem Value="N">NO</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label27" runat="server" Text="Rise"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:DropDownList ID="ddlDeviva" runat="server" class="form-control form-control-sm">
                                                        <asp:ListItem Value="S">Si</asp:ListItem>
                                                        <asp:ListItem Value="N">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>


                                            </div>


                                            <div class="form-row">

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label23" runat="server" Text="Destino"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <asp:DropDownList ID="ddlCoddes" runat="server" class="form-control form-control-sm" DataSourceID="sqldsCoddes" DataTextField="nomdes" DataValueField="coddes">
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label24" runat="server" Text="Fecha Validez"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <asp:TextBox ID="txtValaut" runat="server" class="form-control form-control-sm" TextMode="Date"></asp:TextBox>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label28" runat="server" Text="Cre"></asp:Label>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:DropDownList ID="ddlEsttri" runat="server" class="form-control form-control-sm" DataSourceID="sqldsEsttri" DataTextField="nomtri" DataValueField="codtri">
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:Label ID="Label29" runat="server" Text="N° Imp."></asp:Label>
                                                </div>

                                                <div class="form-group col-md-1">
                                                    <asp:TextBox ID="txtClaimp" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                </div>



                                            </div>




                                            <div class="form-row">
                                                <div class="table table-responsive scroll1 text-black-50" style="overflow-x: auto; height: 70%">
                                                    <div class="thead-dark">
                                                        <asp:UpdatePanel ID="updFacturaBienesRen" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="grvFacturaRen" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" class="table table-bordered table-dark table-hover table-striped">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <%# Container.DataItemIndex + 1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Código" ItemStyle-Width="3%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updFacturaBienesRenPuntitos" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <div class="input-group mb-1">
                                                                                            <asp:TextBox ID="txtgrvCodart" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                                            <div class="input-group-append">
                                                                                                <asp:Button ID="btnPuntitosRen" runat="server" Text="..." class="btn btn-primary btn-sm" OnClick="btnPuntitosRen_Click" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="btnPuntitosRen" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>


                                                                        <asp:TemplateField HeaderText="Descripción" ItemStyle-Width="7%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtgrvNomart" runat="server" htmlencode="false" class="form-control form-control-sm"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Uni." ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updFacturaBienesRenCoduni" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtgrvCoduni" runat="server" class="form-control form-control-sm" AutoPostBack="True"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="txtgrvCoduni" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updFacturaBienesRenCantid" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtgrvCantid" runat="server" class="form-control form-control-sm" AutoPostBack="True" TextMode="Number" OnTextChanged="txtgrvCantid_TextChanged"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="txtgrvCantid" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>


                                                                        <asp:TemplateField HeaderText="Ext." ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updFacturaBienesRenExiact" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtgrvExiact" runat="server" class="form-control form-control-sm" AutoPostBack="True" TextMode="Number" ReadOnly="True"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="txtgrvExiact" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>


                                                                        <asp:TemplateField HeaderText="Coef." ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updFacturaBienesRenCoefic" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtgrvCoefic" runat="server" class="form-control form-control-sm" AutoPostBack="True" TextMode="Number"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="txtgrvCoefic" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Costo Uni." ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updFacturaBienesRenPreuni" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtgrvPreuni" class="form-control form-control-sm" runat="server" DataFormatString="{0:n}" AutoPostBack="True" TextMode="Number" OnTextChanged="txtgrvPreuni_TextChanged"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="txtgrvPreuni" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Dsco" ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtgrvDesren" class="form-control form-control-sm" runat="server" ReadOnly="True"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Precio" ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtgrvPrec01" class="form-control form-control-sm" runat="server" TextMode="Number"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="IVA" ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtgrvPoriva" runat="server" class="form-control form-control-sm" DataFormatString="{0:n}" ReadOnly="True"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Total" ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtgrvTotren" class="form-control form-control-sm" runat="server" ReadOnly="True"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-Width="1%" ItemStyle-HorizontalAlign="center">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updFacturaBienesRenBorrar" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:ImageButton ID="lkgrvBorrar" runat="server" OnClick="lkgrvBorrar_Click" ImageUrl="~/Icon/Menu125/delete-icon.png" Width="20px" />
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lkgrvBorrar" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <asp:Button ID="btnAgregar" runat="server" class="btn btn-dark" Text="➕ Agregar" OnClick="btnAgregar_Click" />
                                                    <asp:Button ID="btnEliminarTodo" runat="server" class="btn btn-dark" OnClick="btnEliminarTodo_Click" Text="Eliminar Todo" />
                                                    <asp:Button ID="btn2x1" runat="server" class="btn btn-dark" Text="2X1" />
                                                    <asp:Button ID="btnFormaDePago" runat="server" class="btn btn-facebook" Text="Forma de Pago" OnClick="btnFormaDePago_Click" />
                                                </div>
                                                <div class="form-group col-md-4">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <div class="card text-white bg-gradient-primary">
                                                <div class="card-body">
                                                    <asp:UpdatePanel ID="updEncabezado" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="form-row">

                                                                <div class="form-group col-md-3">
                                                                    <asp:Label ID="Label7" runat="server" Text="Label">N°</asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-9">
                                                                    <asp:TextBox ID="txtNumfac" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-row">
                                                                <div class="form-group col-md-3">
                                                                    <asp:Label ID="Label8" runat="server" Text="Label">Emisión</asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-9">
                                                                    <asp:TextBox ID="txtFecfac" runat="server" class="form-control form-control-sm" TextMode="Date"></asp:TextBox>
                                                                </div>
                                                            </div>


                                                            <div class="form-row">
                                                                <div class="form-group col-md-3">
                                                                    <asp:Label ID="Label3" runat="server" Text="Label">Comp</asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-9">
                                                                    <asp:TextBox ID="txtCodcom" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                </div>

                                                            </div>


                                                            <div class="form-row">
                                                                <div class="form-group col-md-3">
                                                                    <asp:Label ID="Label6" runat="server" Text="Label">Estado</asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-9">
                                                                    <asp:DropDownList ID="ddlEstado" runat="server" class="form-control form-control-sm">
                                                                        <asp:ListItem Value="P">OK</asp:ListItem>
                                                                        <asp:ListItem Value="A">ANULADO</asp:ListItem>
                                                                        <asp:ListItem Value="C">CANCELADO</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                            </div>





                                                            <div class="form-row">
                                                                <div class="form-group col-md-3">
                                                                    <asp:Label ID="Label12" runat="server" Text="Label">N°</asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-9">
                                                                    <asp:TextBox ID="txtNumdoc" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>


                                            <div class="card text-white bg-primary">
                                                <div class="card-body">
                                                    <asp:UpdatePanel ID="updTotales" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="form-row">
                                                                <div class="form-group col-md-3">
                                                                    <asp:Label ID="Label13" runat="server" Text="Label">Base 0</asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-9">
                                                                    <asp:TextBox ID="txtTotiv0" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-row">
                                                                <div class="form-group col-md-3">
                                                                    <asp:Label ID="Label14" runat="server" Text="Label">Base</asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-9">
                                                                    <asp:TextBox ID="txtTotbas" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                </div>
                                                            </div>


                                                            <div class="form-row">
                                                                <div class="form-group col-md-3">
                                                                    <asp:Label ID="Label9" runat="server" Text="Label">Subtotal</asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-9">
                                                                    <asp:TextBox ID="txtTotnet" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                </div>

                                                            </div>
                                                            <asp:UpdatePanel ID="upPoriva" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <div class="form-row">
                                                                        <div class="form-group col-md-3">
                                                                            <asp:Label ID="Label16" runat="server" Text="Label">IVA. </asp:Label>
                                                                        </div>
                                                                        <div class="form-group col-md-3">


                                                                            <div class="input-group">
                                                                                <asp:DropDownList ID="ddlPoriva" runat="server" class="form-control form-control-sm" AutoPostBack="True" OnSelectedIndexChanged="ddlPoriva_SelectedIndexChanged">
                                                                                    <asp:ListItem Value="12.00">12</asp:ListItem>
                                                                                    <asp:ListItem Value="0.00">0</asp:ListItem>

                                                                                </asp:DropDownList>
                                                                                <div class="input-group-append">
                                                                                    <button class="btn btn-light btn-sm" style="cursor: default;" disabled><strong>%</strong></button>
                                                                                </div>
                                                                            </div>




                                                                        </div>


                                                                        <div class="form-group col-md-6">

                                                                            <asp:TextBox ID="txtTotiva" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="ddlPoriva" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>






                                                            <div class="form-row">
                                                                <div class="form-group col-md-3">
                                                                    <asp:Label ID="Label17" runat="server" Text="Label">TOTAL </asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-9">
                                                                    <asp:TextBox ID="txtTotfac" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                    <!-- End of Main Content -->
                </main>
            



    <!--i SQL DATASOURCES-->

   

    <asp:SqlDataSource ID="sqldsDevolucionExiste" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [numfac] FROM [cp_devolucion] WHERE (([codemp] = @codemp) AND ([numfac] = @numfac))">
        <SelectParameters>
            <asp:SessionParameter Name="codemp" SessionField="gs_Codemp" Type="String" />
            <asp:SessionParameter Name="numfac" SessionField="gs_numfacTSelected" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>


      <asp:SqlDataSource ID="sqldsFacturaBienes" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [numfac], [fecfac], [procxp], [nomcxp], [totfac], [srir_estado], [srie_estado] FROM [cp_compra] WHERE (([codemp] = @codemp) AND ([nomcxp] LIKE '%' + @nomcxp + '%') AND ([procxp] LIKE '%' + @procxp + '%') AND ([fecfac] &gt;= @fecfac) AND ([fecfac] &lt;= @fecfac2))">
        <SelectParameters>
            <asp:SessionParameter Name="codemp" SessionField="gs_Codemp" Type="String" />
            <asp:SessionParameter Name="nomcxp" SessionField="gs_Nomcxp" Type="String" />
            <asp:SessionParameter Name="procxp" SessionField="gs_Procxp" Type="String" />
            <asp:SessionParameter Name="fecfac" SessionField="gs_FeciniB" Type="DateTime" />
            <asp:SessionParameter Name="fecfac2" SessionField="gs_FecfinB" Type="DateTime" />
            
            
        </SelectParameters>
    </asp:SqlDataSource>

      <asp:SqlDataSource ID="sqldsFacturaServicios" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [numfac], [fecfac], [totfac], [procxp], [nomcxp], [srie_estado], [srir_estado] FROM [cp_servicio] WHERE (([codemp] = @codemp) AND ([nomcxp] LIKE '%' + @nomcxp + '%') AND ([procxp] LIKE '%' + @procxp + '%') AND ([fecfac] &gt;= @fecfac) AND ([fecfac] &lt;= @fecfac2))">
        <SelectParameters>
            <asp:SessionParameter Name="codemp" SessionField="gs_Codemp" Type="String" />
            <asp:SessionParameter Name="nomcxp" SessionField="gs_Nomcxp" Type="String" />
            <asp:SessionParameter Name="procxp" SessionField="gs_Procxp" Type="String" />
            <asp:SessionParameter Name="fecfac" SessionField="gs_FeciniS" Type="DateTime" />
            <asp:SessionParameter Name="fecfac2" SessionField="gs_FecfinS" Type="DateTime" />
            
            
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsArticulo" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA  %>" SelectCommand="SELECT TOP 50 A.[codart], A.[nomart], A.[prec01], A.[prec02], A.[prec03], A.[prec04], A.[eximax], A.[codiva], A.[coduni], A.[exiact], A.[ultcos], I.[poriva] FROM [inv_articulo] A, [cfg_iva] I WHERE ([nomart] LIKE '%' + @nomart + '%' AND A.[codiva] = I.[CODIVA] )">
        <SelectParameters>
            <asp:SessionParameter Name="nomart" SessionField="gs_BuscarNomart" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>




    <asp:SqlDataSource ID="sqldsProveedorExiste" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codpro] FROM [cp_proveedor] WHERE (([codemp] = @codemp) AND ([codpro] = @codpro))">
        <SelectParameters>
            <asp:SessionParameter Name="codemp" SessionField="gs_Codemp" Type="String" />
            <asp:SessionParameter Name="codpro" SessionField="gs_codproSelected" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsProveedor" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codpro], [codcla], [codcta], [nompro], [rucced], [telpro], [dirpro] FROM [cp_proveedor]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsClaseProveedor" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codcla])) AS codcla, [nomcla] FROM [vc_cliente_clase] "></asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsCodcla" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codcla], [nomcla] FROM [cp_proveedor_clase]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsEstatus" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [nomest], [estatus] FROM [cfg_estatus]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsApliva" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codiva], [nomiva] FROM [cfg_iva]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodban" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codban], [nomban] FROM [cfg_banco]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodcta" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codcta], [nomcta] FROM [fin_plancuenta]"></asp:SqlDataSource>


    <asp:SqlDataSource ID="sqldsTipcom" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codsri], [nomdoc] FROM [cfg_tipo_doc]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCoddes" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [nomdes], [coddes] FROM [cfg_destino]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodgeo" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codgeo], [nomgeo] FROM [cfg_locgeo]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodalm" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codalm], [nomalm] FROM [inv_almacen]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsEsttri" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [nomtri], [codtri] FROM [cfg_tributario]"></asp:SqlDataSource>

    <!--f SQL DATASOURCES-->
    <button id="btnShowFacturaBienesModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#FacturaBienesModal"></button>
    <div id="FacturaBienesModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccione Artículos</h4>
                    <button class="close" id="btnCloseFacturaBienesModal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <!-- CONTENIDO-->
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="f_GridBuscarFacturaBienes" class="btn btn-primary"> <span class="cil-search"></span></asp:LinkButton>
                                </div>
                                <asp:TextBox ID="TextBox1" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" Width="50%"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="form-group col-md-12">
                            <div class="table table-responsive" style="overflow-x: auto;">
                                                <div class="thead-dark">
                            <asp:UpdatePanel ID="updFacturaBienes" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            
                                                    <asp:GridView ID="grvFacturaBienes" class="table table-bordered table-active table-active table-hover table-striped" runat="server" AutoGenerateColumns="False" AllowPaging="true" ShowHeaderWhenEmpty="True" AllowSorting="true" OnSorting="OnSorting" OnPageIndexChanging="OnPageIndexChanging" PageSize="10" OnRowDataBound="grvFacturaBienes_RowDataBound" OnRowCreated="grvFacturaBienes_RowCreated" OnSelectedIndexChanged="grvFacturaBienes_SelectedIndexChanged">

                                                        <Columns>


                                                            <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updFacturaBienesSelect" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnSelectFacturaBienes" CausesValidation="True" ClientIDMode="Inherit" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/Select-Hand.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>

                                                                    <asp:PostBackTrigger ControlID="btnSelectFacturaBienes" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>



                                                            <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex + 1 %>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="1%"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="numfac" HeaderText="Factura" SortExpression="numfac" />
                                                            <asp:BoundField DataField="nomcxp" HeaderText="Nombre" SortExpression="nomcxp" />
                                                            <asp:BoundField DataField="procxp" HeaderText="Nombre" SortExpression="procxp" />
                                                            <asp:BoundField DataField="fecfac" HeaderText="Fecha" SortExpression="fecfac" />
                                                            <asp:BoundField DataField="totfac" HeaderText="Total" SortExpression="totfac" />
                                                            <asp:BoundField DataField="srie_estado" HeaderText="SRI Recibida" SortExpression="srie_estado" />
                                                            <asp:BoundField DataField="srir_estado" HeaderText=" SRI Autorizado" SortExpression="srir_estado" />
                                                      
                                                        </Columns>
                                                      <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />

                                                    </asp:GridView>

                                         
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                          </div>
                                        </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


     <button id="btnShowFacturaServiciosModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#FacturaServiciosModal"></button>
    <div id="FacturaServiciosModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccione Artículos</h4>
                    <button class="close" id="btnCloseFacturaServiciosModal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <!-- CONTENIDO-->
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="LinkButton2" runat="server" OnClick="f_GridBuscarFacturaServicios" class="btn btn-primary"> <span class="cil-search"></span></asp:LinkButton>
                                </div>
                                <asp:TextBox ID="TextBox2" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" Width="50%"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="form-group col-md-12">
                            <div class="table table-responsive" style="overflow-x: auto;">
                                                <div class="thead-dark">
                            <asp:UpdatePanel ID="updFacturaServicios" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="grvFacturaServicios" class="table table-bordered table-active table-active table-hover table-striped" runat="server" AutoGenerateColumns="False" AllowPaging="true" ShowHeaderWhenEmpty="True" AllowSorting="true" OnSorting="OnSorting" OnPageIndexChanging="OnPageIndexChanging" PageSize="10" OnRowDataBound="grvFacturaServicios_RowDataBound" OnRowCreated="grvFacturaServicios_RowCreated" OnSelectedIndexChanged="grvFacturaServicios_SelectedIndexChanged">

                                                        <Columns>
                                                              <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updFacturaServiciosSelect" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnSelectFacturaServicios" CausesValidation="True" ClientIDMode="Inherit" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/Select-Hand.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>

                                                                    <asp:PostBackTrigger ControlID="btnSelectFacturaServicios" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex + 1 %>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="1%"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="numfac" HeaderText="Factura" SortExpression="numfac" />
                                                            <asp:BoundField DataField="nomcxp" HeaderText="Factura" SortExpression="numfac" />
                                                            <asp:BoundField DataField="procxp" HeaderText="Nombre" SortExpression="procxp" />
                                                            <asp:BoundField DataField="fecfac" HeaderText="Fecha" SortExpression="fecfac" />
                                                            <asp:BoundField DataField="totfac" HeaderText="Total" SortExpression="totfac" />
                                                            <asp:BoundField DataField="srie_estado" HeaderText="SRI Recibida" SortExpression="srie_estado" />
                                                            <asp:BoundField DataField="srir_estado" HeaderText=" SRI Autorizado" SortExpression="srir_estado" />
                                                          
                                                        </Columns>
                                                        <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />

                                                    </asp:GridView>
                                            
                                                 

                                         
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                          </div>
                                        </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>









   

    <!-- Modal grvProveedor -->
    <button id="btnShowProveedorModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#ProveedorModal"></button>
    <div class="modal fade" id="ProveedorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccionar Proveedor</h4>
                    <button class="close" id="btnCloseProveedorModal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">

                    <div class="form-row">

                        <div class="form-group col-md-12">

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="lkbtnSearchProveedor" runat="server" OnClick="f_GridBuscar" class="btn btn-primary"> <span class="cil-search"></span></asp:LinkButton>
                                </div>
                                <asp:TextBox ID="txtSearchProveedor" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" Width="50%"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group col-md-12">

                            <div class="table table-responsive" style="overflow-x: auto;">
                                <div class="thead-dark">
                                    <asp:UpdatePanel ID="updProveedor" runat="server" UpdateMode="Conditional">

                                        <ContentTemplate>



                                            <asp:GridView ID="grvProveedor" class="table table-bordered table-active table-active table-hover table-striped"
                                                runat="server" AutoGenerateColumns="False" OnRowDataBound="grvProveedor_RowDataBound" OnRowCreated="grvProveedor_RowCreated" OnSelectedIndexChanged="grvProveedor_SelectedIndexChanged"
                                                AllowSorting="true" OnSorting="OnSorting" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging" PageSize="10">
                                                <Columns>


                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updProveedorSelect" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnSelectProveedor" CausesValidation="True" ClientIDMode="Inherit" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/Select-Hand.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>

                                                                    <asp:PostBackTrigger ControlID="btnSelectProveedor" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%"></ItemStyle>


                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="codpro" HeaderText="Código" SortExpression="codpro" />
                                                    <asp:BoundField DataField="codcla" HeaderText="Clase" SortExpression="codcla" />
                                                    <asp:BoundField DataField="nompro" HeaderText="Nombres" SortExpression="nompro" />
                                                    <asp:BoundField DataField="rucced" HeaderText="RUC/Cédula" SortExpression="rucced" />
                                                    <asp:BoundField DataField="telpro" HeaderText="Teléfono" SortExpression="telpro" />
                                                    <asp:BoundField DataField="dirpro" HeaderText="Dirección" SortExpression="dirpro" />

                                                </Columns>
                                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                            </asp:GridView>






                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Articulos Popup -->
    <button id="btnShowArticulosModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#modalPuntitosArt"></button>
    <div id="modalPuntitosArt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccione Artículos</h4>
                    <button class="close" id="btnCloseArticulosModal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <!-- CONTENIDO-->
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="lkbtnBuscarNomart" runat="server" OnClick="f_GridBuscarArt" class="btn btn-primary"> <span class="cil-search"></span></asp:LinkButton>
                                </div>
                                <asp:TextBox ID="txtSearchArt" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" Width="50%"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="form-group col-md-12">
                            <div class="table table-responsive" style="overflow-x: auto;">
                                <div class="thead-dark">
                                    <asp:UpdatePanel ID="updArticulo" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="grvArticulo" runat="server" class="table table-bordered table-active table-active table-hover table-striped"
                                                AutoGenerateColumns="False" OnSelectedIndexChanged="grvArticulo_SelectedIndexChanged" OnRowCreated="grvArticulo_RowCreated"
                                                AllowSorting="true" OnSorting="OnSortingArt" AllowPaging="true" OnPageIndexChanging="OnPageIndexChangingArt" PageSize="10">

                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updArticuloSelect" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnSelectArticulo" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/Select-Hand.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnSelectArticulo" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="codart" HeaderText="codart" SortExpression="codart" />
                                                    <asp:BoundField DataField="nomart" HeaderText="nomart" SortExpression="nomart" />
                                                    <asp:BoundField DataField="prec01" HeaderText="prec01" SortExpression="prec01" />
                                                    <asp:BoundField DataField="prec02" HeaderText="prec02" SortExpression="prec02" />
                                                    <asp:BoundField DataField="prec03" HeaderText="prec03" SortExpression="prec03" />
                                                    <asp:BoundField DataField="prec04" HeaderText="prec04" SortExpression="prec04" />
                                                    <asp:BoundField DataField="eximax" HeaderText="eximax" SortExpression="eximax" />
                                                </Columns>

                                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Eliminar -->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Factura</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar la factura '<asp:Label ID="lblEliFac" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarFactura_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

</asp:Content>

<%--  --%>
