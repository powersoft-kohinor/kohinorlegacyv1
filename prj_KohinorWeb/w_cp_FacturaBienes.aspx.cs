﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_cp_FacturaBienes : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "2", niv001 = "2", niv002 = "1";
        protected cls_cp_FacturaBienes objFactura = new cls_cp_FacturaBienes();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Add("baneli", "");
                Session.Add("gs_Procxp", "%"); //codigo de factura
                Session.Add("gs_Nomcxp", "%"); //nombre de factura
                Session.Add("gs_numfacBienSelected", ""); //el selecccionado en el grv
                cls_seg_Usuario objUsuario = (cls_seg_Usuario)Session["gs_objUsuario"];

                objFactura.f_FacturaFecMinFecMax();
                string s_fecmin = cls_cp_FacturaBienes.fecMin;
                string s_fecmax = cls_cp_FacturaBienes.fecMax;

                Session.Add("gs_Fecini", s_fecmin); //fecha inicio
                Session.Add("gs_Fecfin", s_fecmax); //fecha fin
                f_Seg_Usuario_Menu();
                f_BindGrid_Inicial();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAbrir.Attributes.Add("class", "btn btn-dark");
                btnNuevo.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-secondary disabled");
                btnEliminar.Attributes.Add("class", "btn btn-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");
                grvFactura.PagerStyle.CssClass = "";
            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
                btnNuevo.Attributes.Add("class", "btn btn-outline-primary");
                btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");
                grvFactura.PagerStyle.CssClass = "pagination-ys justify-content-center align-items-center";
            }

        }

        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=:exclamation: Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Facturas
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnNuevo.Disabled = true;
                    }
                    if (row["banbus"].Equals("N")) //Abrir
                    {
                        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnAbrir.Disabled = true;
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnGuardar.Disabled = true;
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnCancelar.Disabled = true;

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEliminar.Disabled = true;
                        Session["baneli"] = "N";
                    }
                }
            }
            return dt;
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_cp_FacturaBienesAdministrar.aspx");
        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_cp_FacturaBienes.aspx");
        }

        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)btn.NamingContainer;
            string s_numfac = gvRow.Cells[2].Text.Trim();
            lblEliFac.Text = s_numfac;
            Session["gs_numfacBienSelected"] = s_numfac;
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }

        protected void btnEliminarFactura_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_numfacBienSelected"].ToString()))
            {
                cls_cp_FacturaBienes objFactura = new cls_cp_FacturaBienes(); //eliminar
                clsError objError = objFactura.f_Factura_Eliminar(Session["gs_numfacBienSelected"].ToString(), Session["gs_Codemp"].ToString());
                if (String.IsNullOrEmpty(objError.Mensaje))
                    lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar un cliente para eliminar.";
            }

            lblEliFac.Text = "";
            grvFactura.SelectedIndex = -1;
            f_BindGrid_Inicial();
        }

        protected void grvFactura_RowDataBound(object sender, GridViewRowEventArgs e) //only fires when the GridView's data changes during the postback
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void grvFactura_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }
        }

        protected void grvFactura_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvFactura.SelectedRow;
            string s_numfac = fila.Cells[2].Text.Trim();

            Session["gs_numfacBienSelected"] = s_numfac; //variable para que al seleccionar uno en el grv se obtenga el codcli (sg_codcliSELECTED ->Editar)
            Response.Redirect("w_cp_FacturaBienesAdministrar.aspx");

        }

        private void f_BindGrid_Inicial()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsFactura.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();
            Session.Add("gdt_Factura", dt);
            if (!Session["baneli"].ToString().Equals("")) //en caso de q no se le permita Eliminar
                grvFactura.Columns[8].Visible = false;
            grvFactura.DataSource = dt;
            f_BindGrid();
        }
        private void f_BindGrid(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Factura"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "nomcxp LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "numfac LIKE " + "'%" + searchText.Trim() + "%'";
                    grvFactura.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirection;
                    grvFactura.DataSource = dv;
                }
                grvFactura.DataBind();
            }
        }
        private string SortDirection
        {
            get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }
        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(null, txtSearch.Text);
        }
        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvFactura.PageIndex = e.NewPageIndex;
            f_BindGrid(null, txtSearch.Text);
        }
        protected void OnSorting(object sender, GridViewSortEventArgs e)
        {
            f_BindGrid(e.SortExpression, txtSearch.Text);
        }
    }
}