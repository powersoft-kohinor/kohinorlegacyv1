﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_cp_FacturaBienesAdministrar : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "2", niv001 = "2", niv002 = "1";
        protected cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
        protected cls_inv_Articulo objArticulo = new cls_inv_Articulo();
        protected cls_cp_FacturaBienes objFactura = new cls_cp_FacturaBienes();
        protected cls_cp_FacturaBienes_Ren objFacturaRen = new cls_cp_FacturaBienes_Ren();
        protected cls_cfg_Secuencia objSecuencia = new cls_cfg_Secuencia();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //***i RENGLONES***//
                Session.Add("gs_BuscarNomart", "%"); //variable para buscar el articulo en btnPuntitosArticulos
                f_SetInitialRow();
                Session.Add("gs_Siguiente", ""); //para el lkbtnSiguiente Y lkbtnAtras

                f_Seg_Usuario_Menu();
                f_BindGrid_Inicial(); //grvProveedor
                f_BindGrid_InicialArt();//grvArticulo


                if (Session["gs_numfacBienSelected"] == null) Response.Redirect(" w_cp_FacturaBienes.aspx");


               
                if (String.IsNullOrEmpty(Session["gs_numfacBienSelected"].ToString()))
                    Session.Add("gs_Action", "Add");
                else
                    Session.Add("gs_Action", "Update");


                ddlCoddes.DataBind();
                ddlCodgeo.DataBind();
                ddlDeviva.DataBind();
                ddlEstado.DataBind();
                ddlCodalm.DataBind();
                ddlEsttri.DataBind();
                ddlEstats.DataBind();
                ddlTipcom.DataBind();
                ddlPoriva.DataBind();
                ddlEstado.DataBind();
                ddlDeviva.DataBind();


                objFactura = objFactura.f_Factura_Buscar(Session["gs_Codemp"].ToString(), Session["gs_numfacBienSelected"].ToString());
                if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
                {
                    if (!String.IsNullOrEmpty(objFactura.Numfac))//si seleccionó una Factura
                    {
                        Session["gs_numfacBienSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                        Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                        f_llenarCamposFacturaEnc(objFactura);

                        objFacturaRen = objFacturaRen.f_Factura_Ren_Buscar(Session["gs_Codemp"].ToString(), Session["gs_numfacBienSelected"].ToString());
                        if (String.IsNullOrEmpty(objFacturaRen.Error.Mensaje))
                        {
                            ViewState["CurrentTable"] = objFacturaRen.dtFacturaRen;
                            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                            if (dtCurrentTable.Rows.Count > 0)
                            {
                                grvFacturaRen.DataSource = dtCurrentTable;
                                grvFacturaRen.DataBind();
                                f_SetPreviousData();
                            }
                            else
                            {
                                f_SetInitialRow();
                                f_SetPreviousData();
                                lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
                            }
                        }
                        else
                        {
                            f_ErrorNuevo(objFacturaRen.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                    else //crear factura desde cero
                    {
                        //objFactura = objFactura.f_CalcularSecuencia(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
                        objSecuencia = objSecuencia.f_CalcularSecuencia(Session["gs_Codemp"].ToString(), "CP_FAC", Session["gs_Sersec"].ToString());
                        objFactura.Numfac = objSecuencia.Numero;
                        objFactura.Fecfac = objSecuencia.Fecha.ToString();
                        if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
                        {
                            f_llenarCamposFacturaEnc(objFactura);
                        }
                        else
                        {
                            f_ErrorNuevo(objFactura.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                }
                else
                {
                    f_ErrorNuevo(objFactura.Error);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
                //***f RENGLONES***//
            }

        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAbrir.Attributes.Add("class", "btn btn-dark");
                btnNuevo.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-dark");
                btnCancelar.Attributes.Add("class", "btn btn-dark");
                btnEliminar.Attributes.Add("class", "btn btn-danger");
                div_Factura.Attributes.Add("class", "card");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");

            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
                btnNuevo.Attributes.Add("class", "btn btn-outline-primary");
                div_Factura.Attributes.Add("class", "card bg-gradient-secondary");
                btnGuardar.Attributes.Add("class", "btn btn-outline-dark");
                btnCancelar.Attributes.Add("class", "btn btn-outline-dark");
                btnEliminar.Attributes.Add("class", "btn btn-outline-danger ");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");

            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=:exclamation: Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones 
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnNuevo.Disabled = true;
                    }
                    if (row["banbus"].Equals("N")) //Abrir
                    {
                        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnAbrir.Disabled = true;
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnGuardar.Disabled = true;
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnCancelar.Disabled = true;

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEliminar.Disabled = true;
                        Session["baneli"] = "N";
                    }
                }
            }
            return dt;
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_numfacBienSelected"] = "";

            f_limpiarCampos();
        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_cp_FacturaBienes.aspx");
        }
        protected void btnGuardarFactura_Click(object sender, EventArgs e)
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsFacturaExiste.Select(args);

            
                if (view != null) //actualiza
                {
                    objFactura = f_generarObjFactura();
                    clsError objError = objFactura.f_Factura_Actualizar(objFactura, "Update");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        //ACTUALIZA RENGLON
                        DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
                        objFacturaRen = f_generarObjFacturaRen();
                        objFacturaRen.dtFacturaRen = dtRenglones;
                        objError = objFacturaRen.f_Factura_Ren_Actualizar(objFacturaRen, "Update");
                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                            lblExito.Text = "✔️ Guardado Exitoso.";
                        }
                        else //error en renglones
                        {
                            f_ErrorNuevo(objError);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }

                    
                    }
                    else
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
                else
                {
                    objFactura = f_generarObjFactura();
                    clsError objError = objFactura.f_Factura_Actualizar(objFactura, "Add");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        //INSERTA RENGLON
                        DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
                        objFacturaRen = f_generarObjFacturaRen();
                        objFacturaRen.dtFacturaRen = dtRenglones;
                        objError = objFacturaRen.f_Factura_Ren_Actualizar(objFacturaRen, "Add");
                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                        lblExito.Text = "✔️ Guardado Exitoso.";
                        objError = objSecuencia.f_ActualizarSecuencia(Session["gs_Codemp"].ToString(), "CP_FAC", Session["gs_Sersec"].ToString());
                            if (String.IsNullOrEmpty(objError.Mensaje))
                            {
                                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                                Session["gs_numfacBienSelected"] = objFactura.Numfac;
                                //DATOS DE RENGLONES FORMA PAGO
                                Session.Add("gdt_FormaPago", (DataTable)ViewState["CurrentTable"]);
                                //Response.Redirect("w_vc_FacturaVistaPrevia.aspx");
                                //Response.Redirect("w_xml.aspx");
                            }
                            else
                            {
                                objFactura.f_Factura_Eliminar(objFactura.Numfac, objFactura.Codemp);
                                f_ErrorNuevo(objFactura.Error);
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                            }

                        }
                    }
                    else
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                

                }

        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_numfacBienSelected"] = "";
            f_limpiarCampos();
        }
        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_numfacBienSelected"].ToString()))
            {
                Session["gs_numfacBienSelected"] = txtNumfac.Text;
                lblEliFac.Text = Session["gs_numfacBienSelected"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar una factura para eliminar.";
            }
        }
        protected void btnEliminarFactura_Click(object sender, EventArgs e)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura(); //eliminar
            clsError objError = objFactura.f_Factura_Eliminar(Session["gs_numfacBienSelected"].ToString(), Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblExito.Text = "✔️ Guardado Exitoso.";
                f_limpiarCampos();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void lkbtnPrincipio_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objFactura = objFactura.f_Factura_Principio(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
            if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            {
                Session["gs_numfacBienSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposFacturaEnc(objFactura);
                ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objFactura.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnSiguiente_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objFactura = objFactura.f_Factura_Siguiente(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString(), Session["gs_Siguiente"].ToString());
            if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            {
                Session["gs_numfacBienSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposFacturaEnc(objFactura);
                ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    f_SetInitialRow();
                    f_SetPreviousData();
                    lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objFactura.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnFinal_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objFactura = objFactura.f_Factura_Final(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
            if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            {
                Session["gs_numfacBienSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposFacturaEnc(objFactura);
                ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objFactura.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnAtras_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objFactura = objFactura.f_Factura_Atras(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString(), Session["gs_Siguiente"].ToString());
            if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            {
                Session["gs_numfacBienSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposFacturaEnc(objFactura);
                ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    f_SetInitialRow();
                    f_SetPreviousData();
                    lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objFactura.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }




        //***i CLIENTE ***//
        [WebMethod(enableSession: true)]
        public static cls_vc_Factura f_buscarCli(string s_codcli) //tab en txtClicxc
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            if (s_codcli.Equals("")) //tab en txtClicxc y esta vacio
            {
                objFactura.Clicxc = "";
                objFactura.Nomcxc = "";
                objFactura.Dircli = "";
                objFactura.Lispre = "";
                objFactura.Observ = "";
                objFactura.ClicxcModal = "";
                objFactura.Ntardes = "";

                return objFactura;
            }
            return objFactura.f_Factura_Buscar_Cliente(HttpContext.Current.Session["gs_Codemp"].ToString(), s_codcli);


        }


        protected void btnGuardarProveedor_Click(object sender, EventArgs e)
        {
            //frmVerificar.Attributes.Add("class", "needs-validation was-validated");

            string s_imgcli = "";
            //if (imgCliente.PostedFile != null)
            //{
            //    //si hay una archivo.
            //    string nombreArchivo = imgCliente.PostedFile.FileName;
            //    s_imgcli = "~/Modulos/Ventas/ImgCliente/" + nombreArchivo;
            //    imgCliente.PostedFile.SaveAs(Server.MapPath(s_imgcli));
            //}

            //string s_estrel = "";
            //if (rbtSiRelacionado.Checked)
            //    s_estrel = rbtSiRelacionado.Text;
            //else
            //    s_estrel = rbtNoRelacionado.Text;

            //string s_estado = "";
            //if (rbtSiRelacionado.Checked)
            //    s_estado = rbtSiContribuyente.Text;
            //else
            //    s_estado = rbtNoContribuyente.Text;



            //if (txtCodpro.Value.Equals("") || txtTipemp.Text.Equals("") || txtTipind.Text.Equals("") || txtRucced.Text.Equals("") || txtNompro.Value.Equals("") ||
            //txtNumser.Text.Equals("") || txtNumini.Text.Equals("") || txtNumfin.Text.Equals("") || txtClacon.Text.Equals("") || txtClaimp.Text.Equals("") || txtContac.Text.Equals("") || txtNomciu.Text.Equals("") || txtEmapro.Text.Equals("") &&
            //txtEmacom.Text.Equals("") || txtTelpro.Text.Equals("") || txtDirpro.Text.Equals("") || txtNumcal.Text.Equals("") || txtCedben.Text.Equals("") || txtNomben.Text.Equals("") || txtNumcue.Text.Equals("") || txtLimcre.Text.Equals("") &&
            //txtNumpag.Text.Equals("") || txtPlapag.Text.Equals("") || txtCodart.Text.Equals("") || txtNomart.Text.Equals("") || txtObserv.Text.Equals(""))
            //{

            //    DataSourceSelectArguments args = new DataSourceSelectArguments();
            //    DataView view = (DataView)sqldsProveedorExiste.Select(args);
            //    if (view != null) //ya existe el pedido (Actualizar)
            //    {
            //        objProveedor = f_generarObjProveedor();
            //        clsError objError = objProveedor.f_Proveedor_Actualizar(objProveedor, "Update");
            //        if (String.IsNullOrEmpty(objError.Mensaje))
            //        {

            //            lblExito.Text = "✔️ Guardado Exitoso.";


            //        }
            //        else
            //        {
            //            f_ErrorNuevo(objError);
            //            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            //        }
            //    }
            //    else //no existe el pedido (Insertar)
            //    {
            //        objProveedor = f_generarObjProveedor();
            //        clsError objError = objProveedor.f_Proveedor_Actualizar(objProveedor, "Add");
            //        if (String.IsNullOrEmpty(objError.Mensaje))
            //        {
            //            //INSERTA RENGLON

            //            lblExito.Text = "✔️ Guardado Exitoso.";

            //        }
            //        else
            //        {
            //            f_ErrorNuevo(objError);
            //            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            //        }
            //    }
            //}
            //else
            //{
            //    lblError.Text = "❗ *01. " + DateTime.Now + " Debe llenar todos los campos para crear un Proveedor.";
            //}

        }




        protected void grvProveedor_RowDataBound(object sender, GridViewRowEventArgs e) //only fires when the GridView's data changes during the postback
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }


        protected void grvProveedor_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }


        }


        protected void grvProveedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvProveedor.SelectedRow;
            string s_codpro = fila.Cells[2].Text.Trim();

            Session["gs_codproSelected"] = s_codpro; //variable para que al seleccionar uno en el grv se obtenga el codcli (sg_codcliSELECTED ->Editar)
            objProveedor = objProveedor.f_Proveedor_Buscar(Session["gs_Codemp"].ToString(), s_codpro);
            if (String.IsNullOrEmpty(objProveedor.Error.Mensaje))
            {
                f_llenarCamposEditarProveedor(objProveedor);

            }
            else
            {
                f_ErrorNuevo(objProveedor.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

        }

        private void f_BindGrid_Inicial()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsProveedor.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();
            Session.Add("gdt_Proveedor", dt);

            grvProveedor.DataSource = dt;
            f_BindGrid();
        }

        private void f_BindGrid(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Proveedor"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "nompro LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codpro LIKE " + "'%" + searchText.Trim() + "%'";
                    grvProveedor.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirection;
                    grvProveedor.DataSource = dv;
                }
                grvProveedor.DataBind();
            }
        }

        private string SortDirection
        {
            get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }

        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(null, txtSearchProveedor.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalProveedor();", true);
        }
        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProveedor.PageIndex = e.NewPageIndex;
            f_BindGrid(null, txtSearchProveedor.Text);
        }
        protected void OnSorting(object sender, GridViewSortEventArgs e)
        {
            f_BindGrid(e.SortExpression, txtSearchProveedor.Text);
        }




        public void f_llenarCamposEditarProveedor(cls_cp_Proveedor objProveedor)
        {


            txtProcxp.Value = objProveedor.Codpro;
            txtNomcxp.Text = objProveedor.Nompro;
            txtNumser.Text = objProveedor.Numser;
            txtClacon.Text = objProveedor.Clacon;
            txtClacon.Text = objProveedor.Clacon;
            txtClaimp.Text = objProveedor.Claimp;



            string s_Codgeo = objProveedor.Codgeo;
            string s_Deviva = objProveedor.Deviva;


            if (String.IsNullOrEmpty(s_Codgeo))
                ddlCodgeo.SelectedValue = ddlCodgeo.Items.FindByValue("01      ").Value;
            else
                ddlCodgeo.SelectedValue = ddlCodgeo.Items.FindByValue(s_Codgeo).Value;

            if (String.IsNullOrEmpty(s_Deviva))
                ddlDeviva.SelectedValue = ddlDeviva.Items.FindByValue("N").Value;
            else
                ddlDeviva.SelectedValue = ddlDeviva.Items.FindByValue(s_Deviva).Value;


            string s_fecnac = objProveedor.Valaut;
            try
            {
                DateTime d_date = DateTime.Parse(s_fecnac);
                string s_Fecfinal = d_date.ToString("yyyy-MM-dd");
                txtValaut.Text = s_Fecfinal;
            }
            catch (Exception) //si tiene fecnac en NULL
            {
                txtValaut.Text = DateTime.Now.Date.ToString("yyyy-MM-dd");
                throw;
            }


        }

        //***f GRVCLIENTE ***//


        //***i CODIGO DE RENGLONES ***//
        private void f_SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("N°", typeof(string)));
            dt.Columns.Add(new DataColumn("codart", typeof(string)));
            dt.Columns.Add(new DataColumn("nomart", typeof(string)));
            dt.Columns.Add(new DataColumn("coduni", typeof(string)));
            dt.Columns.Add(new DataColumn("cantid", typeof(decimal)));
            dt.Columns.Add(new DataColumn("exiact", typeof(decimal)));
            dt.Columns.Add(new DataColumn("coefic", typeof(decimal)));
            dt.Columns.Add(new DataColumn("preuni", typeof(decimal)));
            dt.Columns.Add(new DataColumn("desren", typeof(decimal)));
            dt.Columns.Add(new DataColumn("prec01", typeof(decimal)));
            dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totren", typeof(decimal)));



            dr = dt.NewRow();
            dr["N°"] = 1;
            dr["codart"] = string.Empty;
            dr["nomart"] = string.Empty;
            dr["coduni"] = string.Empty;
            dr["cantid"] = 0.00;
            dr["coefic"] = 1;
            dr["exiact"] = 0.00;
            dr["preuni"] = 0.00;
            dr["desren"] = 0.00;
            dr["prec01"] = 0.00;
            dr["poriva"] = 0.00;
            dr["totren"] = 0.00;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            grvFacturaRen.DataSource = dt;
            grvFacturaRen.DataBind();
        }
        private void f_AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {

                        //extract the TextBox values (OJO el .Cells[2] no importa porq esta buscando el control con rowIndex)
                        TextBox codart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        TextBox coduni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvCoduni");
                        TextBox cantid = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvCantid");
                        TextBox exiact = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvExiact");
                        TextBox coefic = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvCoefic");
                        TextBox preuni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[8].FindControl("txtgrvPreuni");
                        TextBox desren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[9].FindControl("txtgrvDesren");
                        TextBox prec01 = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[10].FindControl("txtgrvPrec01");
                        TextBox poriva = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[11].FindControl("txtgrvPoriva");
                        TextBox totren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[12].FindControl("txtgrvTotren");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["codart"] = codart.Text;
                        dtCurrentTable.Rows[i - 1]["nomart"] = nomart.Text;

                        if (cantid.Text == "" && preuni.Text == "" && poriva.Text == "" && desren.Text == "" && totren.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["coduni"] = "UND";
                            dtCurrentTable.Rows[i - 1]["cantid"] = 0.00;

                            dtCurrentTable.Rows[i - 1]["exiact"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["coefic"] = 1;
                            dtCurrentTable.Rows[i - 1]["preuni"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["desren"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["prec01"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["poriva"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["totren"] = 0.00;
                        }
                        else
                        {
                            dtCurrentTable.Rows[i - 1]["coduni"] = String.IsNullOrEmpty(coduni.Text) ? "UND" : coduni.Text;
                            dtCurrentTable.Rows[i - 1]["cantid"] = String.IsNullOrEmpty(cantid.Text) ? 0 : Convert.ToDecimal(cantid.Text);
                            dtCurrentTable.Rows[i - 1]["exiact"] = String.IsNullOrEmpty(exiact.Text) ? 0 : Convert.ToDecimal(exiact.Text);
                            dtCurrentTable.Rows[i - 1]["coefic"] = String.IsNullOrEmpty(coefic.Text) ? 1 : Convert.ToDecimal(coefic.Text);

                            dtCurrentTable.Rows[i - 1]["preuni"] = String.IsNullOrEmpty(preuni.Text) ? 0 : Convert.ToDecimal(preuni.Text);
                            dtCurrentTable.Rows[i - 1]["desren"] = String.IsNullOrEmpty(desren.Text) ? 0 : Convert.ToDecimal(desren.Text);
                            dtCurrentTable.Rows[i - 1]["prec01"] = String.IsNullOrEmpty(prec01.Text) ? 0 : Convert.ToDecimal(prec01.Text);

                            dtCurrentTable.Rows[i - 1]["poriva"] = String.IsNullOrEmpty(poriva.Text) ? Convert.ToDecimal("0.00") : Convert.ToDecimal(poriva.Text);

                            dtCurrentTable.Rows[i - 1]["totren"] = String.IsNullOrEmpty(totren.Text) ? 0 : Convert.ToDecimal(totren.Text);
                        }
                        rowIndex++;
                    }

                    drCurrentRow["coduni"] = "";
                    drCurrentRow["cantid"] = 0.00;
                    drCurrentRow["exiact"] = 0.00;
                    drCurrentRow["coefic"] = 1;
                    drCurrentRow["preuni"] = 0.00;
                    drCurrentRow["desren"] = 0.00;
                    drCurrentRow["prec01"] = 0.00;

                    drCurrentRow["poriva"] = 0.00;

                    drCurrentRow["totren"] = 0.00;
                    dtCurrentTable.Rows.Add(drCurrentRow);

                    ViewState["CurrentTable"] = dtCurrentTable;

                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            f_SetPreviousData();
        }
        private void f_SetPreviousData() //para actualizar el grvFacturaRen con CurrentTable (llenar los textbox del grv)
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        TextBox codart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        TextBox coduni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvCoduni");
                        TextBox cantid = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvCantid");
                        TextBox exiact = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvExiact");
                        TextBox coefic = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvCoefic");
                        TextBox preuni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[8].FindControl("txtgrvPreuni");
                        TextBox desren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[9].FindControl("txtgrvDesren");
                        TextBox prec01 = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[10].FindControl("txtgrvPrec01");
                        TextBox poriva = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[11].FindControl("txtgrvPoriva");
                        TextBox totren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[12].FindControl("txtgrvTotren");


                        codart.Text = dt.Rows[i]["codart"].ToString();
                        nomart.Text = dt.Rows[i]["nomart"].ToString();
                        coduni.Text = dt.Rows[i]["coduni"].ToString();
                        cantid.Text = String.Format("{0:0.00}", (Decimal)dt.Rows[i]["cantid"]);

                        exiact.Text = String.IsNullOrEmpty(dt.Rows[i]["exiact"].ToString()) ? "0.00" : String.Format("{0:0.00}", (Decimal)dt.Rows[i]["exiact"]);
                        coefic.Text = String.IsNullOrEmpty(dt.Rows[i]["coefic"].ToString()) ? "0.00" : String.Format("{0:0.00}", (Decimal)dt.Rows[i]["coefic"]);
                        preuni.Text = String.IsNullOrEmpty(dt.Rows[i]["preuni"].ToString()) ? "0.00" : String.Format("{0:0.00}", (Decimal)dt.Rows[i]["preuni"]);
                        desren.Text = String.IsNullOrEmpty(dt.Rows[i]["desren"].ToString()) ? "0.00" : String.Format("{0:0.00}", (Decimal)dt.Rows[i]["desren"]);
                        prec01.Text = String.IsNullOrEmpty(dt.Rows[i]["prec01"].ToString()) ? "0.00" : String.Format("{0:0.00}", (Decimal)dt.Rows[i]["prec01"]);
                        poriva.Text = String.IsNullOrEmpty(dt.Rows[i]["poriva"].ToString()) ? "0.00" : String.Format("{0:0.00}", (Decimal)dt.Rows[i]["poriva"]);
                        totren.Text = String.IsNullOrEmpty(dt.Rows[i]["totren"].ToString()) ? "0.00" : String.Format("{0:0.00}", (Decimal)dt.Rows[i]["totren"]);

                        if (String.IsNullOrEmpty(dt.Rows[i]["N°"].ToString()))
                        {
                            dt.Rows[i]["N°"] = i + 1;
                        }




                        rowIndex++;
                    }
                }
            }
        }

        //*i BOTONES INFERIOR *//
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            f_AddNewRowToGrid();
        }
        protected void btnEliminarTodo_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];

            for (int i = 0; i < grvFacturaRen.Rows.Count; i++)
            {
                int rowID = grvFacturaRen.SelectedIndex + 1;
                if (ViewState["CurrentTable"] != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (grvFacturaRen.SelectedIndex < dt.Rows.Count - 1)
                        {
                            //Remove the Selected Row data
                            dt.Rows.RemoveAt(i);
                        }
                    }

                    //Store the current data in ViewState for future reference
                    ViewState["CurrentTable"] = dt;
                    //Re bind the GridView for the updated data
                    grvFacturaRen.DataSource = dt;
                    grvFacturaRen.DataBind();
                    i = 0;
                }
            }
            //para borrar los datos de la primera fila....sobra despues de que se ha eliminado todos los demas renglones
            dt.Rows[0]["codart"] = "";
            dt.Rows[0]["nomart"] = "";
            dt.Rows[0]["coduni"] = "";
            dt.Rows[0]["cantid"] = 0.00;
            dt.Rows[0]["exiact"] = 0.00;
            dt.Rows[0]["coefic"] = 1;
            dt.Rows[0]["preuni"] = 0.00;
            dt.Rows[0]["desren"] = 0.00;
            dt.Rows[0]["prec01"] = 0.00;
            dt.Rows[0]["poriva"] = 0.00;
            dt.Rows[0]["totren"] = 0.00;

            //Set Previous Data on Postbacks
            f_SetPreviousData();

            f_calcularTblTotales();
        }
        protected void lkgrvBorrar_Click(object sender, EventArgs e)
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox codart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        TextBox coduni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvCoduni");
                        TextBox cantid = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvCantid");
                        TextBox exiact = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvExiact");
                        TextBox coefic = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvCoefic");
                        TextBox preuni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[8].FindControl("txtgrvPreuni");
                        TextBox desren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[9].FindControl("txtgrvDesren");
                        TextBox prec01 = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[10].FindControl("txtgrvPrec01");
                        TextBox poriva = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[11].FindControl("txtgrvPoriva");
                        TextBox totren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[12].FindControl("txtgrvTotren");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["codart"] = codart.Text;
                        dtCurrentTable.Rows[i - 1]["nomart"] = nomart.Text;


                        if (cantid.Text == "" && preuni.Text == "" && poriva.Text == "" && desren.Text == "" && totren.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["coduni"] = "UND";
                            dtCurrentTable.Rows[i - 1]["cantid"] = 0.00;

                            dtCurrentTable.Rows[i - 1]["exiact"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["coefic"] = 1;
                            dtCurrentTable.Rows[i - 1]["preuni"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["desren"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["prec01"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["poriva"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["totren"] = 0.00;
                        }
                        else
                        {
                            dtCurrentTable.Rows[i - 1]["coduni"] = String.IsNullOrEmpty(coduni.Text) ? "UND" : coduni.Text;
                            dtCurrentTable.Rows[i - 1]["cantid"] = String.IsNullOrEmpty(cantid.Text) ? 0 : Convert.ToDecimal(cantid.Text);
                            dtCurrentTable.Rows[i - 1]["exiact"] = String.IsNullOrEmpty(exiact.Text) ? 0 : Convert.ToDecimal(exiact.Text);
                            dtCurrentTable.Rows[i - 1]["coefic"] = String.IsNullOrEmpty(coefic.Text) ? 1 : Convert.ToDecimal(coefic.Text);

                            dtCurrentTable.Rows[i - 1]["preuni"] = String.IsNullOrEmpty(preuni.Text) ? 0 : Convert.ToDecimal(preuni.Text);
                            dtCurrentTable.Rows[i - 1]["desren"] = String.IsNullOrEmpty(desren.Text) ? 0 : Convert.ToDecimal(desren.Text);
                            dtCurrentTable.Rows[i - 1]["prec01"] = String.IsNullOrEmpty(prec01.Text) ? 0 : Convert.ToDecimal(prec01.Text);

                            dtCurrentTable.Rows[i - 1]["poriva"] = String.IsNullOrEmpty(poriva.Text) ? Convert.ToDecimal("0.00") : Convert.ToDecimal(poriva.Text);

                            dtCurrentTable.Rows[i - 1]["totren"] = String.IsNullOrEmpty(totren.Text) ? 0 : Convert.ToDecimal(totren.Text);
                        }




                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }

            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (!(gvRow.RowIndex == 0 && dt.Rows.Count == 1)) //no permite borrar la primera fila
                    {
                        //Remove the Selected Row data
                        dt.Rows.Remove(dt.Rows[rowID]);
                    }
                }
                else //si selecciona la primera fila
                {
                    if (gvRow.RowIndex == 0 && dt.Rows.Count == 1) //para borrar los datos de la primera fila
                    {
                        dt.Rows[0]["codart"] = "";
                        dt.Rows[0]["nomart"] = "";
                        dt.Rows[0]["coduni"] = "";
                        dt.Rows[0]["cantid"] = 0.00;
                        dt.Rows[0]["exiact"] = 0.00;
                        dt.Rows[0]["coefic"] = 1;
                        dt.Rows[0]["preuni"] = 0.00;
                        dt.Rows[0]["desren"] = 0.00;
                        dt.Rows[0]["prec01"] = 0.00;
                        dt.Rows[0]["poriva"] = 0.00;
                        dt.Rows[0]["totren"] = 0.00;
                    }
                }
                //Store the current data in ViewState for future reference
                ViewState["CurrentTable"] = dt;
                //Re bind the GridView for the updated data
                grvFacturaRen.DataSource = dt;
                grvFacturaRen.DataBind();
            }
            //Set Previous Data on Postbacks
            f_SetPreviousData();
            f_calcularTblTotales();
        }
        //*f BOTONES INFERIOR *//

        //*i SELECCION ARTICULOS *//
        protected void btnPuntitosRen_Click(object sender, EventArgs e) //Abrir Modal Articulos
        {
            Button btn = (Button)sender;
            GridViewRow gvRow = (GridViewRow)btn.NamingContainer;
            int rowIndex = gvRow.RowIndex; //para saber la fila seleccionada
            Session.Add("gs_renSelecxtedRow", rowIndex); //para saber en donde poner el articulo seleccionado del grvArticulos

            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalFacturaArticulos();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void grvArticulo_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }
        }
        protected void grvArticulo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = grvArticulo.SelectedRow;
            string s_codart = row.Cells[2].Text.Trim();
            string s_nomart = Server.HtmlDecode(row.Cells[3].Text.Trim()); //para que acepte caracteres especiales (Ñ)
            string s_preuni = row.Cells[4].Text.Trim();

            objFacturaRen.Codart = s_codart;
            objFacturaRen.Nomart = s_nomart;
            objFacturaRen.Preuni = decimal.Parse(s_preuni);
            objFacturaRen.Cantid = 1;

            Session["gs_BuscarNomart"] = s_nomart;

            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsArticulo.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();

            Session["gdt_Articulo"] = dt;

            int rowIndex = int.Parse(Session["gs_renSelecxtedRow"].ToString());
            f_llenarCamporFacturasRen(objFacturaRen, rowIndex);
        }
        private void f_BindGrid_InicialArt()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsArticulo.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();
            Session.Add("gdt_Articulo", dt);

            grvArticulo.DataSource = dt;
            f_BindGridArt();
        }
        private void f_BindGridArt(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Articulo"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "nomart LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codart LIKE " + "'%" + searchText.Trim() + "%'";
                    grvArticulo.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirectionArt = this.SortDirectionArt == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirectionArt;
                    grvArticulo.DataSource = dv;
                }
                grvArticulo.DataBind();
            }
        }
        private string SortDirectionArt
        {
            get { return ViewState["SortDirectionArt"] != null ? ViewState["SortDirectionArt"].ToString() : "ASC"; }
            set { ViewState["SortDirectionArt"] = value; }
        }
        protected void f_GridBuscarArt(object sender, EventArgs e)
        {
            f_BindGridArt(null, txtSearchArt.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalFacturaArticulos();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void OnPageIndexChangingArt(object sender, GridViewPageEventArgs e)
        {
            grvArticulo.PageIndex = e.NewPageIndex;
            f_BindGridArt(null, txtSearchArt.Text);
        }
        protected void OnSortingArt(object sender, GridViewSortEventArgs e)
        {
            f_BindGridArt(e.SortExpression, txtSearchArt.Text);
        }
        //*f SELECCION ARTICULOS *//


        //*i CALCULOS RENGLON*//
        public void f_recalcularRenglon(int i_rowID)
        {
            GridViewRow row = grvFacturaRen.Rows[i_rowID];

            try
            {
                TextBox txtgrvCantid = (TextBox)row.FindControl("txtgrvCantid");
                if (String.IsNullOrEmpty(txtgrvCantid.Text))
                {
                    txtgrvCantid.Text = "1";
                }
                objFacturaRen.Cantid = decimal.Parse(txtgrvCantid.Text);

                TextBox txtgrvPreuni = (TextBox)row.FindControl("txtgrvPreuni");
                if (String.IsNullOrEmpty(txtgrvPreuni.Text))
                {
                    txtgrvPreuni.Text = "0.00";
                }
                objFacturaRen.Preuni = decimal.Parse(txtgrvPreuni.Text);

                TextBox txtgrvCodart = (TextBox)row.FindControl("txtgrvCodart");
                objFacturaRen.Codart = txtgrvCodart.Text;

                TextBox txtgrvNomart = (TextBox)row.FindControl("txtgrvNomart");
                objFacturaRen.Nomart = txtgrvNomart.Text;

                f_llenarCamporFacturasRen(objFacturaRen, i_rowID);

            }
            catch (Exception ex)
            {
                objFacturaRen.Error = new clsError();
                objFacturaRen.Error = objFacturaRen.Error.f_ErrorControlado(ex);
                f_ErrorNuevo(objFacturaRen.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void txtgrvCantid_TextChanged(object sender, EventArgs e)
        {
            TextBox tx = (TextBox)sender;
            GridViewRow gvRow = (GridViewRow)tx.NamingContainer;
            int i_rowID = gvRow.RowIndex;//para saber la fila seleccionada
            f_recalcularRenglon(i_rowID);
        }
        protected void txtgrvPreuni_TextChanged(object sender, EventArgs e)
        {
            TextBox tx = (TextBox)sender;
            GridViewRow gvRow = (GridViewRow)tx.NamingContainer;
            int i_rowID = gvRow.RowIndex;//para saber la fila seleccionada
            f_recalcularRenglon(i_rowID);
        }

        public string f_descuentoArticulo(string s_codart)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_PV_M_AUT_DESCUENTO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", "01"); //OJO CODEMP SE ESTA MANDANDO QUEMADO 
            cmd.Parameters.AddWithValue("@CODART", s_codart);
            cmd.Parameters.AddWithValue("@FECHA", "2016-08-08"); //para validar si el decuento aplica

            cmd.Parameters.Add("@s_Coddes", SqlDbType.Char, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Aplica", SqlDbType.Char, 15).Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string descuento = cmd.Parameters["@s_Coddes"].Value.ToString().Trim();
            conn.Close();

            return descuento;



        }
        //*f CALCULOS RENGLON*//


        //*i CALCULOS DERECHA*//
        public void f_calcularTblTotales() //tblTotales es la tabla de la derecha.
        {
            f_renglonesActuales();


            double r_totNeto = f_cantTotal();
            double r_totIva0 = f_cantBaseImponible0();
            double r_totBas = f_cantBaseImponible();

            double r_iva = Convert.ToDouble(ddlPoriva.SelectedValue);
            double r_totIva = r_totBas * (r_iva / 100);
            double r_totFac = r_totNeto + r_totIva;




            double r_Subtotal = r_totIva0 + r_totBas;
            //txtTotnet.Text = r_totNeto.ToString("N2");
            txtTotiv0.Text = r_totIva0.ToString("N2");
            txtTotnet.Text = r_Subtotal.ToString("N2");
            txtTotbas.Text = r_totBas.ToString("N2");
            txtTotiva.Text = r_totIva.ToString("N2");
            txtTotfac.Text = r_totFac.ToString("N2");

        }
        public double f_cantBaseImponible0() //recorre todos los datos de la columna total, sumando solo los del 12% IVA
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            int nrow = dt.Rows.Count;
            int i = 0;
            double r_ctotal = 0;
            try
            {
                while (i < nrow)
                {
                    if (!dt.Rows[i]["totren"].ToString().Equals("") && dt.Rows[i]["poriva"].ToString().Equals("0.00")) //si txtgrvTotal NO esta vacio suma
                    {
                        r_ctotal = r_ctotal + double.Parse(dt.Rows[i]["totren"].ToString());
                    }
                    i = i + 1;
                }
            }
            catch (Exception)
            {
                Response.Write("<script>alert('Todos los totales deben estar llenos.');</script>");
            }

            return r_ctotal;
        }
        public double f_cantBaseImponible() //recorre todos los datos de la columna total, sumando solo los del 12% IVA
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            int nrow = dt.Rows.Count;
            int i = 0;
            double r_ctotal = 0;
            try
            {
                while (i < nrow)
                {
                    if (!dt.Rows[i]["totren"].ToString().Equals("") && dt.Rows[i]["poriva"].ToString().Equals("12.00")) //si txtgrvTotal NO esta vacio suma
                    {
                        r_ctotal = r_ctotal + double.Parse(dt.Rows[i]["totren"].ToString());
                    }
                    i = i + 1;
                }
            }
            catch (Exception)
            {
                Response.Write("<script>alert('Todos los totales deben estar llenos.');</script>");
            }

            return r_ctotal;
        }
        public double f_cantTotal()//para txtTotnet ...recorre todos los datos de la columna total, sumando los num para sacar la cantidad total
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            int nrow = dt.Rows.Count;
            int i = 0;
            double r_ctotal = 0;
            try
            {
                while (i < nrow)
                {
                    if (!dt.Rows[i]["totren"].ToString().Equals("")) //si txtgrvTotal NO esta vacio suma
                    {
                        r_ctotal = r_ctotal + double.Parse(dt.Rows[i]["totren"].ToString());
                    }
                    i = i + 1;
                }
            }
            catch (Exception)
            {
                Response.Write("<script>alert('Todos los totales deben estar llenos.');</script>");
            }

            return r_ctotal;
        }
        public void f_renglonesActuales() //para guardar en ViewState["CurrentTable"] los renglones mostrados en pantalla
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox codart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        TextBox coduni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvCoduni");
                        TextBox cantid = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvCantid");
                        TextBox exiact = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvExiact");
                        TextBox coefic = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvCoefic");
                        TextBox preuni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[8].FindControl("txtgrvPreuni");
                        TextBox desren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[9].FindControl("txtgrvDesren");
                        TextBox prec01 = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[10].FindControl("txtgrvPrec01");
                        TextBox poriva = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[11].FindControl("txtgrvPoriva");
                        TextBox totren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[12].FindControl("txtgrvTotren");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["codart"] = codart.Text;
                        dtCurrentTable.Rows[i - 1]["nomart"] = nomart.Text;

                        if (cantid.Text == "" && preuni.Text == "" && poriva.Text == "" && desren.Text == "" && totren.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["coduni"] = "UND";
                            dtCurrentTable.Rows[i - 1]["cantid"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["exiact"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["coefic"] = 1;
                            dtCurrentTable.Rows[i - 1]["preuni"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["desren"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["prec01"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["poriva"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["totren"] = 0.00;
                        }
                        else
                        {
                            dtCurrentTable.Rows[i - 1]["coduni"] = String.IsNullOrEmpty(coduni.Text) ? "UND" : coduni.Text;
                            dtCurrentTable.Rows[i - 1]["cantid"] = String.IsNullOrEmpty(cantid.Text) ? 0 : Convert.ToDecimal(cantid.Text);
                            dtCurrentTable.Rows[i - 1]["exiact"] = String.IsNullOrEmpty(exiact.Text) ? 0 : Convert.ToDecimal(exiact.Text);
                            dtCurrentTable.Rows[i - 1]["coefic"] = String.IsNullOrEmpty(coefic.Text) ? 1 : Convert.ToDecimal(coefic.Text);
                            dtCurrentTable.Rows[i - 1]["preuni"] = String.IsNullOrEmpty(preuni.Text) ? 0 : Convert.ToDecimal(preuni.Text);
                            dtCurrentTable.Rows[i - 1]["desren"] = String.IsNullOrEmpty(desren.Text) ? 0 : Convert.ToDecimal(desren.Text);
                            dtCurrentTable.Rows[i - 1]["prec01"] = String.IsNullOrEmpty(prec01.Text) ? 0 : Convert.ToDecimal(prec01.Text);
                            dtCurrentTable.Rows[i - 1]["poriva"] = String.IsNullOrEmpty(poriva.Text) ? Convert.ToDecimal("0.00") : Convert.ToDecimal(poriva.Text);
                            dtCurrentTable.Rows[i - 1]["totren"] = String.IsNullOrEmpty(totren.Text) ? 0 : Convert.ToDecimal(totren.Text);
                        }




                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }
        }
        //*f CALCULOS DERECHA*//

        //***f CODIGO DE RENGLONES ***//

        public void f_llenarCamposFacturaEnc(cls_cp_FacturaBienes objFactura)
        {


            txtProcxp.Value = objFactura.Procxp;
            txtNomcxp.Text = objFactura.Nomcxp;
            txtNumser.Text = objFactura.Numser;
            txtNumcom.Text = objFactura.Numcom;
            txtClacon.Text = objFactura.Clacon;
            txtLispre.Text = objFactura.Lispre;
            txtObserv.Text = objFactura.Observ;
            txtClaimp.Text = objFactura.Claimp;
            txtNumfac.Text = objFactura.Numfac;
            txtFecfac.Text = objFactura.Fecfac;
            txtCodcom.Text = objFactura.Codcom;
            txtNumdoc.Text = objFactura.Numdoc;
            txtTotiv0.Text = objFactura.Totiv0.ToString();
            txtTotbas.Text = objFactura.Totbas.ToString();
            txtTotiva.Text = objFactura.Totiva.ToString();
            txtTotfac.Text = objFactura.Totfac.ToString();





            string s_codalm = objFactura.Codalm;
            string s_tipcom = objFactura.Tipcom.ToString();
            string s_coddes = objFactura.Coddes.ToString();
            string s_codgeo = objFactura.Codgeo;
            string s_estado = objFactura.Estado;
            string s_deviva = objFactura.Deviva;
            string s_esttri = objFactura.Esttri;
            decimal d_poriva = objFactura.Poriva;
            string s_poriva = String.Format("{0:0.00}", d_poriva);
            string s_estats = objFactura.Estats;






            if (String.IsNullOrEmpty(s_codalm))
                ddlCodalm.SelectedValue = ddlCodalm.Items.FindByValue("001.01").Value;
            else
                ddlCodalm.SelectedValue = ddlCodalm.Items.FindByValue(s_codalm).Value;

            if (String.IsNullOrEmpty(s_estado))
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue("C").Value;
            else
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue(s_estado).Value;

            if (String.IsNullOrEmpty(s_estats))
                ddlEstats.SelectedValue = ddlEstats.Items.FindByValue("S").Value;
            else
                ddlEstats.SelectedValue = ddlEstats.Items.FindByValue(s_estats).Value;




            if (String.IsNullOrEmpty(s_esttri))
                ddlEsttri.SelectedValue = ddlEsttri.Items.FindByValue("1").Value;
            else
                ddlEsttri.SelectedValue = ddlEsttri.Items.FindByValue(s_esttri).Value.Trim();




            //if (String.IsNullOrEmpty(s_tipcom))
            //    ddlTipcom.SelectedValue = ddlTipcom.Items.FindByValue("1").Value;
            //else
            //    ddlTipcom.SelectedValue = ddlTipcom.Items.FindByValue(s_tipcom).Value;

            if (String.IsNullOrEmpty(s_coddes) || s_coddes.Equals("0"))
                ddlCoddes.SelectedValue = ddlCoddes.Items.FindByValue("1").Value;
            else
                ddlCoddes.SelectedValue = ddlCoddes.Items.FindByValue(s_coddes).Value;

            if (String.IsNullOrEmpty(s_poriva))
                ddlPoriva.SelectedValue = ddlPoriva.Items.FindByValue("12.00").Value;
            else
                ddlPoriva.SelectedValue = ddlPoriva.Items.FindByValue(s_poriva).Value;


            if (String.IsNullOrEmpty(s_codgeo))
                ddlCodgeo.SelectedValue = ddlCodgeo.Items.FindByValue("01      ").Value;
            else
                ddlCodgeo.SelectedValue = ddlCodgeo.Items.FindByValue(s_codgeo).Value;

            if (String.IsNullOrEmpty(s_deviva))
                ddlDeviva.SelectedValue = ddlDeviva.Items.FindByValue("N").Value;
            else
                ddlDeviva.SelectedValue = ddlDeviva.Items.FindByValue(s_deviva).Value;



            string s_valaut = "";
            string s_feccom = "";


            if (String.IsNullOrEmpty(objFactura.Valaut) || String.IsNullOrEmpty(objFactura.Feccom))
            {
                s_valaut = DateTime.Now.ToString("yyyy-MM-dd");
                s_feccom = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                s_valaut = objFactura.Feccom.ToString();
                s_feccom = objFactura.Valaut.ToString();

            }


            try
            {
                DateTime d_valaut = DateTime.Parse(s_valaut);
                string s_FecfinalValaut = d_valaut.ToString("yyyy-MM-dd");
                txtValaut.Text = s_FecfinalValaut;
            }
            catch (Exception) //si tiene fecnac en NULL
            {
                txtValaut.Text = DateTime.Now.Date.ToString("yyyy-MM-dd");
                throw;
            }



            try
            {
                DateTime d_feccom = DateTime.Parse(s_feccom);
                string s_FecfinalFeccom = d_feccom.ToString("yyyy-MM-dd");
                txtFeccom.Text = s_FecfinalFeccom;
            }
            catch (Exception) //si tiene fecnac en NULL
            {
                txtFeccom.Text = DateTime.Now.Date.ToString("yyyy-MM-dd");
                throw;
            }

            string s_fecfac = objFactura.Fecfac;
            try
            {
                DateTime d_date = DateTime.Parse(s_fecfac);
                string s_Fecfinal = d_date.ToString("yyyy-MM-dd");
                txtFecfac.Text = s_Fecfinal;
            }
            catch (Exception ex) //si tiene fecnac en NULL
            {
                txtFecfac.Text = DateTime.Now.Date.ToString();
                throw;
            }
        }
        public void f_llenarCamporFacturasRen(cls_cp_FacturaBienes_Ren objFacturaRen, int rowIndex)
        {
            GridViewRow row = grvFacturaRen.Rows[rowIndex];

            TextBox txtgrvCodart = (TextBox)row.FindControl("txtgrvCodart");
            TextBox txtgrvNomart = (TextBox)row.FindControl("txtgrvNomart");
            TextBox txtgrvCoduni = (TextBox)row.FindControl("txtgrvCoduni");
            TextBox txtgrvCantid = (TextBox)row.FindControl("txtgrvCantid");
            TextBox txtgrvExiact = (TextBox)row.FindControl("txtgrvExiact");
            TextBox txtgrvCoefic = (TextBox)row.FindControl("txtgrvCoefic");
            TextBox txtgrvPreuni = (TextBox)row.FindControl("txtgrvPreuni");
            TextBox txtgrvDesren = (TextBox)row.FindControl("txtgrvDesren");
            TextBox txtgrvPrec01 = (TextBox)row.FindControl("txtgrvPrec01");
            TextBox txtgrvPoriva = (TextBox)row.FindControl("txtgrvPoriva");
            TextBox txtgrvTotren = (TextBox)row.FindControl("txtgrvTotren");

            objArticulo = objArticulo.f_dtArticuloToObjArticulo((DataTable)Session["gdt_Articulo"]);



            decimal d_poriva = objArticulo.Poriva;

            double r_preuni = (double)objFacturaRen.Preuni; //para solo mostrar 2 decimales
            double s_coefic = 1;
            double r_poriva = double.Parse(d_poriva.ToString());
            double r_totIva = r_preuni + r_preuni * (r_poriva / 100);
            double r_desren = 0.00;

            string s_coddes = "1"; //OJO borra esto y descomentar ^^^ al programar lo de decuentos
            string s_poriva = String.Format("{0:0.00}", d_poriva);

            txtgrvCodart.Text = objFacturaRen.Codart;
            txtgrvNomart.Text = objFacturaRen.Nomart;
            txtgrvCoduni.Text = objArticulo.Coduni;
            txtgrvCantid.Text = objFacturaRen.Cantid.ToString();
            txtgrvExiact.Text = objArticulo.Exiact.ToString();
            txtgrvCoefic.Text = s_coefic.ToString();
            txtgrvPrec01.Text = objArticulo.Prec01.ToString();
            txtgrvPoriva.Text = s_poriva;



            if (!s_coddes.Equals("3X1") && !s_coddes.Equals("-")) //EN Equals("") PONER TODOS LOS DESCUENTOS QUE NO SEAN % Y SE GUARDEN EN LA TABLA....ejm: 3X1
            {
                s_coddes = new string(s_coddes.Where(c => char.IsDigit(c)).ToArray()); //si es 20% devuelve 20
                r_desren = double.Parse(s_coddes);
            }
            //para aplicar descuento de tarjeta
            if (Session["gs_Destar"] != null)
            {
                r_desren = r_desren + double.Parse(Session["gs_Destar"].ToString());
            }


            double d_Preuni = r_preuni - (r_preuni * (r_desren / 100));
            double r_totRen = (double)objFacturaRen.Cantid * r_preuni * s_coefic;

            r_totRen = r_totRen - (r_totRen * (r_desren / 100)); //resta el % de descuento 



            txtgrvPreuni.Text = d_Preuni.ToString("N2");
            txtgrvDesren.Text = r_desren.ToString("N2");
            txtgrvTotren.Text = r_totRen.ToString("N2");



            if (!objFacturaRen.Codart.Trim().Equals("*"))
                f_calcularTblTotales();

        }
        protected void btnFormaDePago_Click(object sender, EventArgs e)
        {
            Session.Add("g_objFacturaBien", f_generarObjFactura());
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            Session.Add("gdt_FacturaBienRen", dt);

         
            Response.Redirect("w_cp_FacturaBienesFormaPago.aspx");
        } 
        protected void lkbtnRetencion_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session.Add("g_objFacturaBien", f_generarObjFactura());
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            Session.Add("gdt_FacturaBienRen", dt);
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsFacturaExiste.Select(args);
            if (view != null) //ya existe la factura (Actualizar)
                Session.Add("gs_Existe", "Update");
            else //no existe la factura (Insertar)
                Session.Add("gs_Existe", "Add");


            if (String.IsNullOrEmpty(txtProcxp.Value)) lblError.Text = "❗ Debe seleccionar un Proveedor.";
            
            if (String.IsNullOrEmpty(lblError.Text)) Response.Redirect("w_cp_RetencionesBienes.aspx");

        }
        public void f_limpiarCampos()
        {
            //Image2.ImageUrl = "~/imgusu/user.png";

            txtNomcxp.Text = txtProcxp.Value = txtNumser.Text = txtNumcom.Text = txtClacon.Text = txtClaimp.Text = txtLispre.Text = txtObserv.Text = "";
            objSecuencia = objSecuencia.f_CalcularSecuencia(Session["gs_Codemp"].ToString(), "CP_FAC", Session["gs_Sersec"].ToString());
            objFactura.Numfac = objSecuencia.Numero;
            objFactura.Fecfac = objSecuencia.Fecha.ToString();
            objFactura = objFactura.f_CalcularSecuencia(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
            if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            {
                f_llenarCamposFacturaEnc(objFactura);
            }
            else
            {
                f_ErrorNuevo(objFactura.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

            DataTable dt = (DataTable)ViewState["CurrentTable"];
            for (int i = 0; i < grvFacturaRen.Rows.Count; i++)
            {
                int rowID = grvFacturaRen.SelectedIndex + 1;
                if (ViewState["CurrentTable"] != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (grvFacturaRen.SelectedIndex < dt.Rows.Count - 1)
                        {
                            //Remove the Selected Row data
                            dt.Rows.RemoveAt(i);
                        }
                    }

                    //Store the current data in ViewState for future reference
                    ViewState["CurrentTable"] = dt;
                    //Re bind the GridView for the updated data
                    grvFacturaRen.DataSource = dt;
                    grvFacturaRen.DataBind();
                    i = 0;
                }
            }

            //para borrar los datos de la primera fila....sobra despues de que se ha eliminado todos los demas renglones
            if (grvFacturaRen.Rows.Count == 0)
            {
                f_AddNewRowToGrid();
            }
            else
            {
                dt.Rows[0]["codart"] = "";
                dt.Rows[0]["nomart"] = "";
                dt.Rows[0]["coduni"] = "";
                dt.Rows[0]["cantid"] = 0.00;
                dt.Rows[0]["exiact"] = 0.00;
                dt.Rows[0]["coefic"] = 1;
                dt.Rows[0]["preuni"] = 0.00;
                dt.Rows[0]["desren"] = 0.00;
                dt.Rows[0]["prec01"] = 0.00;
                dt.Rows[0]["poriva"] = 0.00;
                dt.Rows[0]["totren"] = 0.00;

            }
            //Set Previous Data on Postbacks
            f_SetPreviousData();
            f_calcularTblTotales();

            lblError.Text = lblError.Text = "";

        }

        protected void ddlPoriva_SelectedIndexChanged(object sender, EventArgs e)
        {
            f_calcularTblTotales();
        }

        public cls_cp_FacturaBienes f_generarObjFactura()
        {

            string fecha_final = DateTime.Now.ToString("yyyy-MM-dd");

            //CAMPOS NORMALES
            objFactura.Codemp = Session["gs_Codemp"].ToString();
            objFactura.Codusu = Session["gs_CodUs1"].ToString();
            objFactura.Procxp = txtProcxp.Value;
            objFactura.Codpro = txtProcxp.Value;
            objFactura.Nomcxp = txtNomcxp.Text;
            objFactura.Codgeo = String.IsNullOrEmpty(ddlCodgeo.Text) ? "01" : ddlCodgeo.Text;
            objFactura.Codalm = String.IsNullOrEmpty(ddlCodalm.Text) ? "001.01" : ddlCodalm.Text;
            objFactura.Numser = txtNumser.Text;
            objFactura.Numcom = txtNumcom.Text;
            objFactura.Clacon = txtClacon.Text;
            objFactura.Lispre = txtLispre.Text;
            objFactura.Observ = txtObserv.Text;
            objFactura.Tipcom = String.IsNullOrEmpty(ddlTipcom.SelectedValue) ? 0 : int.Parse(ddlTipcom.SelectedValue);
            objFactura.Coddes = String.IsNullOrEmpty(ddlCoddes.SelectedValue) ? 1 : int.Parse(ddlCoddes.Text);
            objFactura.Estats = String.IsNullOrEmpty(ddlEstats.SelectedValue) ? "S" : ddlEstats.SelectedValue;
            objFactura.Feccom = String.IsNullOrEmpty(txtFeccom.Text) ? fecha_final : txtFeccom.Text;
            objFactura.Deviva = String.IsNullOrEmpty(ddlDeviva.SelectedValue) ? "N" : ddlDeviva.SelectedValue;
            objFactura.Estado = String.IsNullOrEmpty(ddlEstado.SelectedValue) ? "P" : ddlEstado.SelectedValue;
            objFactura.Fecfac = String.IsNullOrEmpty(txtFecfac.Text) ? fecha_final : txtFecfac.Text;
            objFactura.Estado = ddlEstado.Text;
            objFactura.Deviva = ddlDeviva.Text;
            objFactura.Esttri = String.IsNullOrEmpty(ddlEsttri.Text) ? "1" : ddlEsttri.Text;
            objFactura.Claimp = txtClaimp.Text;
            objFactura.Valaut = String.IsNullOrEmpty(txtValaut.Text) ? fecha_final : txtValaut.Text;
            objFactura.Numfac = txtNumfac.Text;
            objFactura.Fecfac = txtFecfac.Text;
            objFactura.Codcom = txtCodcom.Text;
            objFactura.Numdoc = txtNumdoc.Text;
            objFactura.Totiv0 = String.IsNullOrEmpty(txtTotiv0.Text) ? 0 : decimal.Parse(txtTotiv0.Text);
            objFactura.Pordes = 0;
            objFactura.Totdes = 0;
            objFactura.Totbas = String.IsNullOrEmpty(txtTotbas.Text) ? 0 : decimal.Parse(txtTotbas.Text);
            objFactura.Poriva = String.IsNullOrEmpty(ddlPoriva.SelectedValue) ? 0 : decimal.Parse(ddlPoriva.SelectedValue);
            objFactura.Totiva = String.IsNullOrEmpty(txtTotiva.Text) ? 0 : decimal.Parse(txtTotiva.Text);
            objFactura.Totfac = String.IsNullOrEmpty(txtTotfac.Text) ? 0 : decimal.Parse(txtTotfac.Text);
            objFactura.Totnet = String.IsNullOrEmpty(txtTotnet.Text) ? 0 : decimal.Parse(txtTotnet.Text);
            objFactura.Fecven = String.IsNullOrEmpty(txtFecfac.Text) ? fecha_final : txtFecfac.Text;
            objFactura.Conpag = "C";
            objFactura.Codapu = "FC" + txtNumfac.Text;
            objFactura.Codmon = "01";
            objFactura.Valcot = 01;
            objFactura.Sersec = Session["gs_SerSec"].ToString();

            //DATATABLE FACTURA
            DataTable dt = objFactura.f_dtFacturaTableType();
            DataRow dr = null;
            dr = dt.NewRow();
            dr["codemp"] = Session["gs_Codemp"].ToString();
            dr["numfac"] = txtNumfac.Text;
            dr["fecfac"] = String.IsNullOrEmpty(txtFecfac.Text) ? fecha_final : txtFecfac.Text;
            dr["procxp"] = txtProcxp.Value;
            dr["codpro"] = txtProcxp.Value;
            dr["nomcxp"] = txtNomcxp.Text;
            dr["codgeo"] = String.IsNullOrEmpty(ddlCodgeo.SelectedValue) ? "01" : ddlCodgeo.SelectedValue;
            dr["codalm"] = String.IsNullOrEmpty(ddlCodalm.SelectedValue) ? "001.01" : ddlCodalm.SelectedValue;
            dr["tipcom"] = String.IsNullOrEmpty(ddlTipcom.SelectedValue) ? 0 : int.Parse(ddlTipcom.SelectedValue);
            dr["coddes"] = String.IsNullOrEmpty(ddlCoddes.SelectedValue) ? 1 : int.Parse(ddlCoddes.SelectedValue);
            dr["estats"] = String.IsNullOrEmpty(ddlEstats.SelectedValue) ? "S" : ddlEstats.SelectedValue;
            dr["deviva"] = String.IsNullOrEmpty(ddlDeviva.SelectedValue) ? "N" : ddlDeviva.SelectedValue;
            dr["estado"] = String.IsNullOrEmpty(ddlEstado.SelectedValue) ? "P" : ddlEstado.SelectedValue;
            dr["numser"] = txtNumser.Text;
            dr["numcom"] = txtNumcom.Text;
            dr["clacon"] = txtClacon.Text;
            dr["lispre"] = txtLispre.Text;
            dr["observ"] = txtObserv.Text;
            dr["tipcom"] = String.IsNullOrEmpty(ddlTipcom.SelectedValue) ? 0 : int.Parse(ddlTipcom.SelectedValue);
            dr["feccom"] = String.IsNullOrEmpty(txtFeccom.Text) ? fecha_final : txtFeccom.Text;
            dr["esttri"] = String.IsNullOrEmpty(ddlEsttri.SelectedValue) ? "1" : ddlEsttri.SelectedValue;
            dr["claimp"] = txtClaimp.Text;
            dr["valaut"] = String.IsNullOrEmpty(txtValaut.Text) ? fecha_final : txtValaut.Text;
            dr["numfac"] = txtNumfac.Text;
            dr["codcom"] = txtCodcom.Text;
            dr["numdoc"] = txtNumdoc.Text;
            dr["totiv0"] = String.IsNullOrEmpty(txtTotiv0.Text) ? 0 : decimal.Parse(txtTotiv0.Text);
            dr["pordes"] = 0;
            dr["totdes"] = 0;
            dr["totbas"] = String.IsNullOrEmpty(txtTotbas.Text) ? 0 : decimal.Parse(txtTotbas.Text);
            dr["poriva"] = String.IsNullOrEmpty(ddlPoriva.SelectedValue) ? Convert.ToDecimal("0.00") : decimal.Parse(ddlPoriva.SelectedValue);
            dr["totiva"] = String.IsNullOrEmpty(txtTotiva.Text) ? 0 : decimal.Parse(txtTotiva.Text);
            dr["totfac"] = String.IsNullOrEmpty(txtTotfac.Text) ? 0 : decimal.Parse(txtTotfac.Text);
            dr["fecven"] = String.IsNullOrEmpty(txtFecfac.Text) ? fecha_final : txtFecfac.Text;
            dr["totnet"] = String.IsNullOrEmpty(txtTotnet.Text) ? 0 : decimal.Parse(txtTotnet.Text);
            dr["conpag"] = "C";
            dr["codapu"] = "FC" + txtNumfac.Text;
            dr["codmon"] = "01";
            dr["valcot"] = 01;
            dr["sersec"] = Session["gs_SerSec"].ToString();
            dr["usuing"] = Session["gs_UsuIng"].ToString();
            dr["codusu"] = Session["gs_CodUs1"].ToString();


            dt.Rows.Add(dr);
            objFactura.dtFactura = dt;

            return objFactura;
        }


        public cls_cp_FacturaBienes_Ren f_generarObjFacturaRen()
        {
            objFacturaRen.Codemp = Session["gs_Codemp"].ToString();
            objFacturaRen.Numfac = txtNumfac.Text;
            objFacturaRen.Codalm = String.IsNullOrEmpty(ddlCodalm.Text) ? "001.01" : ddlCodalm.Text;
            objFacturaRen.Codmon = "01";
            objFacturaRen.Valcot = 01;
            objFacturaRen.Codiva = "1";
            objFacturaRen.Ubifis = "?";
            objFacturaRen.Totext = 700;
            objFacturaRen.Feccad = String.IsNullOrEmpty(txtFecfac.Text) ? DateTime.Now.ToString("yyyy-MM-dd") : txtFecfac.Text;
            objFacturaRen.Numlot = "1";
            objFacturaRen.Numing = "1";
            objFacturaRen.Rening = 1;
            objFacturaRen.Tipord = "1";
            objFacturaRen.Renord = 1;
            objFacturaRen.Numord = "1";


            return objFacturaRen;
        }
    }
}