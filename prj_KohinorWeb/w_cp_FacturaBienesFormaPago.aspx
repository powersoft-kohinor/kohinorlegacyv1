﻿<%@ Page Title="Factura Bien Forma Pago" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_cp_FacturaBienesFormaPago.aspx.cs" Inherits="prj_KohinorWeb.w_cp_FacturaBienesFormaPago" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
	<%-- Modulos Kohinor Web --%>
	<li class="c-sidebar-nav-title">Menu</li>
	<asp:Repeater ID="rptNivel001" runat="server">
		<ItemTemplate>
			<li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
				<a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
				<ul class="c-sidebar-nav-dropdown-items">
					<asp:Repeater ID="rptNivel002" runat="server">
						<ItemTemplate>
							<li class="c-sidebar-nav-item">
								<asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
						</ItemTemplate>
					</asp:Repeater>
				</ul>
			</li>
		</ItemTemplate>
	</asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
	<div class="c-subheader justify-content-between px-3">
		<!-- Breadcrumb-->
		<ol class="breadcrumb border-0 m-0">
			<li class="breadcrumb-item"><asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
			<li class="breadcrumb-item"><a href="w_vc_Factura.aspx">Facturas</a></li>
			<li class="breadcrumb-item"><a href="w_vc_FacturaAdministrar.aspx">Administrar Factura</a></li>
			<li class="breadcrumb-item active">Forma de Pago</li>
			<!-- Breadcrumb Menu-->
		</ol>
		<div class="c-subheader-nav d-md-down-none mfe-2">
			<div id="spinner" class="spinner-border" role="status">
				<span class="sr-only">Loading...</span>
			</div>
			<a class="c-subheader-nav-link">
				<div class="input-group">
					<div class="input-group-prepend">
						<button class="btn btn-primary" type="button" id="">
							<i class="cil-filter"></i>
						</button>
					</div>

					<asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
				</div>
				<asp:LinkButton ID="lkbtnSearch" runat="server" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
			</a>			

			<a class="c-subheader-nav-link">
				<i class="c-icon cil-speech"></i>
			</a>
			<a class="c-subheader-nav-link">
				<i class="c-icon cil-graph"></i>&nbsp;
							Escritorio
			</a>
			<a class="c-subheader-nav-link">
				<i class="c-icon cil-settings"></i>
			</a>
		</div>
	</div>
	<div class="c-subheader px-3">
		<ol class="breadcrumb border-0 m-0">
			<li class="breadcrumb-item active">
				<div class="btn-group" role="group" aria-label="Basic example">
					<button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click"><i class="cil-file"></i>Nuevo</button>
					<button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" onserverclick="btnAbrir_Click"><i class="cil-folder-open"></i>Abrir</button>
					<button type="button" class="btn btn-outline-dark" id="btnGuardar" runat="server"><i class="cil-save"></i>Guardar</button>
					<button type="button" class="btn btn-outline-dark" id="btnCancelar" runat="server" onserverclick="btnCancelar_Click"><i class="cil-grid-slash"></i>Cancelar</button>
					<button type="button" class="btn btn-outline-danger" id="btnEliminar" runat="server"><i class="cil-trash"></i>Eliminar</button>
				</div>
				<div class="btn-group" role="group" aria-label="Basic example">
					<button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-step-backward"></i></button>
					<button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-skip-backward"></i></button>
					<button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-skip-forward"></i></button>
					<button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-step-forward"></i></button>
				</div>

				<asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
				<asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
			</li>
		</ol>

	</div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
				<main class="c-main">
					<div class="container-fluid">
						<div class="fade-in">
							<div id="div_FormaPago" runat="server" class="card bg-gradient-secondary">
								<div class="card-header">
									<i class="c-icon cil-justify-center"></i><b> Forma de Pago</b>
								</div>                                
									
								<div class="card-body">
									<div class="form-row">
										<div class="form-group col-md-9">  
											<div class="form-row">
												<div class="table table-responsive scroll1 text-black-50" style="overflow-x: auto; height:70%">
												<div class="thead-dark">
													<asp:UpdatePanel ID="updFacturaRen" runat="server" UpdateMode="Conditional">
														<ContentTemplate>
															<asp:GridView ID="grvFormaPago" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" class="table table-bordered table-dark table-hover table-striped">
																<Columns>
																	<asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
																		<ItemTemplate>
																			<%# Container.DataItemIndex + 1 %>
																		</ItemTemplate>
																	</asp:TemplateField>																	                                                                                  

																	<asp:TemplateField HeaderText="Forma Pago">
																		<ItemTemplate>
																			<div class="form-row">
																				  <asp:DropDownList ID="ddlgrvCodfor" class="form-control form-control-sm" runat="server" DataSourceID="sqldsCodfor" DataTextField="nomfor" DataValueField="codfor" Width="120px">
																				  </asp:DropDownList>
																				  <asp:SqlDataSource ID="sqldsCodfor" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codfor], [nomfor] FROM [cfg_formapago]"></asp:SqlDataSource>
																			  </div>
																		</ItemTemplate>
																	</asp:TemplateField>

																	<asp:TemplateField HeaderText="Banco">
																		  <ItemTemplate>
																			  <asp:TextBox ID="txtgrvCodban" runat="server" class="form-control form-control-sm"></asp:TextBox>
																		  </ItemTemplate>
																	  </asp:TemplateField>

																	  <asp:TemplateField HeaderText="Lote">
																		  <ItemTemplate>
																			  <asp:TextBox ID="txtgrvDocfpa" runat="server" class="form-control form-control-sm"></asp:TextBox>
																		  </ItemTemplate>
																	  </asp:TemplateField>

																	  <asp:TemplateField HeaderText="Referencia">
																		  <ItemTemplate>
																			  <asp:TextBox ID="txtgrvBaufpa" class="form-control form-control-sm" runat="server"></asp:TextBox>
																		  </ItemTemplate>

																	  </asp:TemplateField>

																	  <asp:TemplateField HeaderText="N° Tarjeta/NC">
																		  <ItemTemplate>
																			  <asp:TextBox ID="txtgrvNumtar" runat="server" class="form-control form-control-sm"></asp:TextBox>
																		  </ItemTemplate>
																	  </asp:TemplateField>

																	  <asp:TemplateField HeaderText="Valor">
																		  <ItemTemplate>
																			  <asp:UpdatePanel ID="updFacturaRenCantid" runat="server" UpdateMode="Conditional">
																				<ContentTemplate>
																					<asp:TextBox ID="txtgrvValfpa" runat="server" class="form-control form-control-sm" AutoPostBack="True" TextMode="Number" OnTextChanged="txtgrvValfpa_TextChanged"></asp:TextBox>
																				</ContentTemplate>
																				<Triggers>
																					<asp:PostBackTrigger ControlID="txtgrvValfpa" />
																				</Triggers>
																			</asp:UpdatePanel>                                                                              
																		  </ItemTemplate>                                                                          
																	  </asp:TemplateField>

																	<asp:TemplateField ItemStyle-Width="1%" ItemStyle-HorizontalAlign="center">
																		<ItemTemplate>
																			<asp:UpdatePanel ID="updFacturaRenBorrar" runat="server" UpdateMode="Conditional">
																				<ContentTemplate>
																					<asp:ImageButton ID="lkgrvBorrar" runat="server" OnClick="lkgrvBorrar_Click" ImageUrl="~/Icon/Menu125/delete-icon.png" Width="20px" />
																				</ContentTemplate>
																				<Triggers>
																					<asp:PostBackTrigger ControlID="lkgrvBorrar" />
																				</Triggers>
																			</asp:UpdatePanel>
																		</ItemTemplate>
																	</asp:TemplateField>
																</Columns>
															</asp:GridView>
														</ContentTemplate>
													</asp:UpdatePanel>
												</div>
											</div>
											</div>
											<hr />
											<div class="form-row">
												<div class="form-group col-md-3">
												</div>
												<div class="form-group col-md-6">
													<div class="card shadow mb-4">
														<div class="card-body">
															<div class="row">
																<div class="col">
																	<asp:Button ID="btnAgregar" runat="server" CssClass="btn btn-dark" OnClick="btnAgregar_Click" Text="➕ Agregar" />
																</div>
																<div class="col">
																	<div class="input-group">
																		<div class="input-group-preppend">
																			<span class="input-group-text">
																				<strong>Falta</strong></span>
																		</div>
																		<asp:TextBox ID="txtFalta" runat="server" class="form-control" ReadOnly="True"></asp:TextBox>
																		<div class="input-group-append">
																			<asp:Button ID="btnConfirmar" runat="server" CssClass="btn btn-primary" OnClick="btnConfirmar_Click" Text="🗸 Confirmar" />
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>

												</div>
												<div class="form-group col-md-3">
												</div>
											</div>
										</div>

										<div class="form-group col-md-3">
											<div class="card text-white bg-gradient-primary ">
												<asp:UpdatePanel ID="updEncabezado" runat="server" UpdateMode="Conditional">
													<ContentTemplate>
														<div class="card-header text-center">
															<div class="row">
																<div class="col">
																	<h4><label><strong>N° </strong></label></h4>
																	<h4><asp:Label ID="txtNumfac" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label></h4>
																</div>
															</div>
														</div>
														<div class="card-body">
															<div class="form-row">
																<div class="form-group col-md-3">
																	<asp:Label ID="Label8" runat="server" Text="Label">Emisión</asp:Label>
																</div>
																<div class="form-group col-md-9">
																	<asp:TextBox ID="txtFecfac" runat="server" class="form-control form-control-sm" TextMode="Date"></asp:TextBox>
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<div class="input-group input-group-sm">
																		<div class="input-group-prepend">
																			<span class="input-group-text"><strong>Estado</strong></span>
																		</div>
																		<asp:DropDownList ID="ddlEstado" class="form-control" runat="server">
																			<asp:ListItem Value="P">OK</asp:ListItem>
																			<asp:ListItem Value="A">ANULADO</asp:ListItem>
																			<asp:ListItem Value="C">CANCELADO</asp:ListItem>
																		</asp:DropDownList>
																	</div>
																</div>
																<div class="form-group col-md-6">
																	<div class="input-group input-group-sm">
																		<div class="input-group-prepend">
																			<span class="input-group-text"><strong>Almacén</strong></span>
																		</div>
																		<asp:TextBox ID="txtCodalm" runat="server" class="form-control form-control-sm"></asp:TextBox>
																	</div>                                                                    
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-3">
																	<asp:Label ID="Label11" runat="server" Text="Label">Vendedor</asp:Label>
																</div>
																<div class="form-group col-md-9">
																	<asp:DropDownList ID="ddlCodven" runat="server" class="form-control form-control-sm" DataSourceID="sqldsVendedor" DataTextField="nomven" DataValueField="codven"></asp:DropDownList>
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-3">
																	<asp:Label ID="Label12" runat="server" Text="Label">AUT.SRI</asp:Label>
																</div>
																<div class="form-group col-md-9">
																	<asp:TextBox ID="txtSriNumaut" runat="server" class="form-control form-control-sm"></asp:TextBox>
																</div>
															</div>
														</div>
													</ContentTemplate>
												</asp:UpdatePanel>
											</div>
											
											<div class="card text-white bg-primary text-center">
												<div class="card-body">
													<asp:UpdatePanel ID="updTotales" runat="server" UpdateMode="Conditional">
														<ContentTemplate>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<asp:Label ID="Label13" runat="server" Text="Label">Total Neto</asp:Label>
																</div>
																<div class="form-group col-md-6">
																	<asp:Label ID="txtTotnet" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
																</div>
															</div>

															<div class="form-row">
																<div class="form-group col-md-6">
																	<asp:Label ID="Label14" runat="server" Text="Label">Base</asp:Label>
																</div>
																<div class="form-group col-md-6">
																	<asp:Label ID="txtTotbas" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
																</div>
															</div>

															<div class="form-row">
																<div class="form-group col-md-6">
																	<asp:Label ID="Label15" runat="server" Text="Label">Descto</asp:Label>
																</div>
																<div class="form-group col-md-6">
																	<asp:Label ID="txtTotdes" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
																</div>
															</div>

															<div class="form-row">

																<div class="form-group col-md-6">
																	<asp:Label ID="Label16" runat="server" Text="Label">IVA.</asp:Label>
																</div>
																<div class="form-group col-md-6">
																	<asp:Label ID="txtTotiva" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
																</div>
															</div>

															<hr />

															<div class="form-row">
																<div class="form-group col-md-6">
																	<h3><label><strong>TOTAL</strong></label></h3>
																</div>
																<div class="form-group col-md-6">
																	<h3><asp:Label ID="txtTotfac" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label></h3>
																</div>
															</div>

															<div class="form-row">
																<div class="form-group col-md-6">
																	<div class="input-group input-group-sm">
																		<div class="input-group-prepend">
																			<span class="input-group-text"><strong>Recibido</strong></span>
																		</div>
																		<asp:TextBox ID="txtTotrec" runat="server" class="form-control form-control-sm"></asp:TextBox>
																	</div>
																</div>
																<div class="form-group col-md-6">
																	<div class="input-group input-group-sm">
																		<div class="input-group-prepend">
																			<span class="input-group-text"><strong>Cambio</strong></span>
																		</div>
																		<asp:TextBox ID="txtCambio" runat="server" class="form-control form-control-sm"></asp:TextBox>
																	</div>                                                                    
																</div>
															</div>

															<div class="form-row">
																
																<div class="form-row">
																	Clave Acceso
&nbsp;<asp:Label ID="lblUsuing" runat="server" Text="Label"></asp:Label><asp:Label ID="lblFecing" runat="server" Text="Label"></asp:Label><asp:Label ID="lblCodusu" runat="server" Text="Label"></asp:Label><asp:Label ID="lblFecutl" runat="server" Text="Label"></asp:Label>

																</div>
															</div>
														</ContentTemplate>
													</asp:UpdatePanel>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.container-fluid -->
					<!-- End of Main Content -->
				</main>

	<!--i SQL DATASOURCES-->
	<asp:SqlDataSource ID="sqldsFacturaExiste" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [numfac] FROM [vc_factura] WHERE (([codemp] = @codemp) AND ([numfac] = @numfac))">
		<SelectParameters>
			<asp:SessionParameter Name="codemp" SessionField="gs_Codemp" Type="String" />
			<asp:SessionParameter Name="numfac" SessionField="gs_numfacSelected" Type="String" />
		</SelectParameters>
	</asp:SqlDataSource>
	
	<asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
		<SelectParameters>
			<asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
			<asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
			<asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
			<asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
		</SelectParameters>
	</asp:SqlDataSource>
	<asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
		<SelectParameters>
			<asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
			<asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
			<asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
			<asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
			<asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
		</SelectParameters>
	</asp:SqlDataSource>
	<asp:SqlDataSource ID="sqldsVendedor" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codven])) AS codven, [nomven] FROM [vc_vendedor]"></asp:SqlDataSource>
	<asp:SqlDataSource ID="sqldsTtardes" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT * FROM [vc_tarjeta]"></asp:SqlDataSource>
	<asp:SqlDataSource ID="sqldsCliente" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codcli], [codcla], [nomcli], [rucced], [telcli], [email], [dircli] FROM [vc_cliente]"></asp:SqlDataSource>

	<asp:SqlDataSource ID="sqldsArticulo" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA  %>" SelectCommand="SELECT TOP 50 [codart], [nomart], [prec01], [prec02], [prec03], [prec04], [eximax], [codiva] FROM [inv_articulo] WHERE ([nomart] LIKE '%' + @nomart + '%')">
		<SelectParameters>
			<asp:SessionParameter Name="nomart" SessionField="gs_BuscarNomart" Type="String" />
		</SelectParameters>
	</asp:SqlDataSource>

	<asp:SqlDataSource ID="sqldsClaseCliente" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codcla])) AS codcla, [nomcla] FROM [vc_cliente_clase] "></asp:SqlDataSource>

	<!--f SQL DATASOURCES-->

	<!-- Modal Eliminar -->
	<button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
	<div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-primary" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Eliminar Factura</h4>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				</div>
				<div class="modal-body">
					<p>¿Desea eliminar la factura '<asp:Label ID="lblEliFac" runat="server" Font-Bold="True"></asp:Label>' ?</p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
					<button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarFactura_Click">Eliminar</button>
				</div>
			</div>
			<!-- /.modal-content-->
		</div>
		<!-- /.modal-dialog-->
	</div>

	<!-- Modal Danger (Error) -->
	<div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-danger modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Error</h4>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				</div>
				<div class="modal-body">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">N° Error</span>
						</div>
						<asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
					</div>

					<div class="alert alert-danger" role="alert">
						<h4 class="alert-heading">Mensaje</h4>
						<asp:Label ID="lblModalFecha" runat="server"></asp:Label>
						<p>
							<asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
						</p>
						<hr>
						<p class="mb-0">
							<strong>Donde: </strong>
							<asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
						</p>
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">Objeto</span>
						</div>
						<asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">Evento</span>
						</div>
						<asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">Línea</span>
						</div>
						<asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
					<button class="btn btn-danger" type="button">Save changes</button>
				</div>
			</div>
			<!-- /.modal-content-->
		</div>
		<!-- /.modal-dialog-->
	</div>
</asp:Content>
