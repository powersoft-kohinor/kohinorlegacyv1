﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace prj_KohinorWeb
{
    public partial class w_cp_FacturaServiciosFormaPago : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "2", niv001 = "2", niv002 = "1";
        protected cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
        protected cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
        protected cls_cp_FacturaServicios_Ren objFacturaRen = new cls_cp_FacturaServicios_Ren();
        protected cls_cfg_Secuencia objSencuencia = new cls_cfg_Secuencia();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                f_Seg_Usuario_Menu();
                if (Session["gs_numfacServicioSelected"] == null) Response.Redirect("w_vc_Factura.aspx");
                btnConfirmar.Enabled = false;
                ddlCodven.DataBind(); //OJO DEBE IR ANTES DE f_llenarCampos
                ddlEstado.DataBind();
                ddlEstado.DataBind();
                objFactura = (cls_cp_FacturaServicios)Session["g_objFacturaServicio"];
                //objFacturaRen = (cls_vc_Factura_Ren)Session["g_objFacturaRen"];
                f_llenarCamposFacturaEnc(objFactura);
                f_SetInitialRow(objFactura);
                txtFalta.Text = "0.00";
                f_renglonesActuales();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAbrir.Attributes.Add("class", "btn btn-dark");
                btnNuevo.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-secondary");

                div_FormaPago.Attributes.Add("class", "card");

                btnEliminar.Attributes.Add("class", "btn btn-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");
                grvFormaPago.PagerStyle.CssClass = "";
            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
                btnNuevo.Attributes.Add("class", "btn btn-outline-primary");
                div_FormaPago.Attributes.Add("class", "card bg-gradient-secondary");
                btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-outline-dark");
                btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");
                grvFormaPago.PagerStyle.CssClass = "pagination-ys justify-content-center align-items-center";
            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnNuevo.Disabled = true;
                    }
                    if (row["banbus"].Equals("N")) //Abrir
                    {
                        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnAbrir.Disabled = true;
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnGuardar.Disabled = true;
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnCancelar.Disabled = true;

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEliminar.Disabled = true;
                        Session["baneli"] = "N";
                    }
                }
            }
            return dt;
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_numfacServicioSelected"] = "";
            f_limpiarCampos();
        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_vc_Factura.aspx");
        }
        protected void btnGuardarFactura_Click(object sender, EventArgs e)
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsFacturaExiste.Select(args);
            if (view != null) //ya existe la factura (Actualizar)
            {
                objFactura = (cls_cp_FacturaServicios)Session["g_objFacturaServicio"];
                clsError objError = objFactura.f_Factura_Actualizar(objFactura, "Update");
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    //ACTUALIZA RENGLON
                    DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
                    objFacturaRen.dtFacturaRen = dtRenglones;
                    objError = objFacturaRen.f_Factura_Ren_Actualizar(objFacturaRen, "Update");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                    }
                    else //error en renglones
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            else //no existe la factura (Insertar)
            {
                objFactura = (cls_cp_FacturaServicios)Session["g_objFacturaServicio"];
                clsError objError = objFactura.f_Factura_Actualizar(objFactura, "Add");
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    //INSERTA RENGLON
                    DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
                    objFacturaRen.dtFacturaRen = dtRenglones;
                    objError = objFacturaRen.f_Factura_Ren_Actualizar(objFacturaRen, "Add");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                    }
                    else //error en renglones
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_vc_FacturaAdministrar.aspx");
        }
        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_numfacServicioSelected"].ToString()))
            {
                Session["gs_numfacServicioSelected"] = txtNumfac.Text;
                lblEliFac.Text = Session["gs_numfacServicioSelected"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar una factura para eliminar.";
            }
        }
        protected void btnEliminarFactura_Click(object sender, EventArgs e)
        {
            cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios(); //eliminar
            clsError objError = objFactura.f_Factura_Eliminar(Session["gs_numfacServicioSelected"].ToString(), Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                f_limpiarCampos();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        public void f_llenarCamposFacturaEnc(cls_cp_FacturaServicios objFactura)
        {


            txtNumfac.Text = objFactura.Numfac;
            txtFecfac.Text = objFactura.Fecfac;
            txtTotnet.Text = objFactura.Totnet.ToString();
            txtTotbas.Text = objFactura.Totbas.ToString();
            txtTotdes.Text = objFactura.Totdes.ToString();
            txtTotiva.Text = objFactura.Totiva.ToString();
            txtTotfac.Text = objFactura.Totfac.ToString();


            string s_codalm = objFactura.Codalm;
            string s_estado = objFactura.Estado;


            if (String.IsNullOrEmpty(s_estado))
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue("C").Value;
            else
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue(s_estado).Value;

            if (String.IsNullOrEmpty(s_codalm))
                txtCodalm.Text = Session["gs_CodAlm"].ToString();
            else
                txtCodalm.Text = objFactura.Codalm;
            if (String.IsNullOrEmpty(s_estado))
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue("P").Value;
            else
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue(s_estado).Value;


            string s_fecfac = objFactura.Fecfac;
            try
            {
                DateTime d_date = DateTime.Parse(s_fecfac);
                string s_Fecfinal = d_date.ToString("yyyy-MM-dd");
                txtFecfac.Text = s_Fecfinal;
            }
            catch (Exception ex) //si tiene fecnac en NULL
            {
                txtFecfac.Text = DateTime.Now.Date.ToString();
                throw;
            }

        }
        public void f_limpiarCampos()
        {
            lblError.Text = lblError.Text = "";
        }


        //NUEVOS METODOS...DE PRJ PUNTOVENTA ---------------------------------------------------------------------------------

        private void f_SetInitialRow(cls_cp_FacturaServicios objFactura)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("N°", typeof(string)));
            dt.Columns.Add(new DataColumn("Forma de Pago", typeof(string)));
            dt.Columns.Add(new DataColumn("Banco", typeof(string)));
            dt.Columns.Add(new DataColumn("Lote", typeof(string)));
            dt.Columns.Add(new DataColumn("Referencia", typeof(string)));
            dt.Columns.Add(new DataColumn("N° Tarjeta/NC", typeof(string)));
            dt.Columns.Add(new DataColumn("Valor", typeof(string)));

            dr = dt.NewRow();
            dr["N°"] = 1;
            dr["Forma de Pago"] = string.Empty;
            dr["Banco"] = "EFECTIVO";
            dr["Lote"] = string.Empty;
            dr["Referencia"] = string.Empty;
            dr["N° Tarjeta/NC"] = string.Empty;
            dr["Valor"] = objFactura.Totfac.ToString();

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;
            grvFormaPago.DataSource = dt;
            grvFormaPago.DataBind();

            //para llenar el Banco de la primera fila al recien entrar a forma de pago
            int rowIndex = 0;
            TextBox txtgrvCodban = (TextBox)grvFormaPago.Rows[rowIndex].Cells[2].FindControl("txtgrvCodban");
            txtgrvCodban.Text = "EFECTIVO";
            TextBox txtgrvValfpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[6].FindControl("txtgrvValfpa");
            txtgrvValfpa.Text = objFactura.Totfac.ToString();
            f_calcularFalta();
        }
        private void f_AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    decimal r_valfpa = 0, r_valfpaAux = 0;
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values (OJO el .Cells[2] no importa porq esta buscando el control con rowIndex)
                        DropDownList codfor = (DropDownList)grvFormaPago.Rows[rowIndex].Cells[2].FindControl("ddlgrvCodfor");
                        TextBox codban = (TextBox)grvFormaPago.Rows[rowIndex].Cells[3].FindControl("txtgrvCodban");
                        TextBox docfpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[4].FindControl("txtgrvDocfpa");
                        TextBox baufpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[4].FindControl("txtgrvBaufpa");
                        TextBox numtar = (TextBox)grvFormaPago.Rows[rowIndex].Cells[5].FindControl("txtgrvNumtar");
                        TextBox valfpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[6].FindControl("txtgrvValfpa");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;
                        drCurrentRow["Banco"] = "EFECTIVO";
                        r_valfpaAux = String.IsNullOrEmpty(valfpa.Text) ? 0 : decimal.Parse(valfpa.Text);
                        r_valfpa = r_valfpa + r_valfpaAux;
                        drCurrentRow["Valor"] = (decimal.Parse(txtTotfac.Text) - r_valfpa).ToString("N2");

                        dtCurrentTable.Rows[i - 1]["Forma de Pago"] = codfor.Text;
                        dtCurrentTable.Rows[i - 1]["Banco"] = codban.Text;
                        dtCurrentTable.Rows[i - 1]["Lote"] = docfpa.Text;
                        dtCurrentTable.Rows[i - 1]["Referencia"] = baufpa.Text;
                        dtCurrentTable.Rows[i - 1]["N° Tarjeta/NC"] = numtar.Text;
                        dtCurrentTable.Rows[i - 1]["Valor"] = String.IsNullOrEmpty(valfpa.Text) ? "0" : valfpa.Text;

                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);

                    ViewState["CurrentTable"] = dtCurrentTable;

                    grvFormaPago.DataSource = dtCurrentTable;
                    grvFormaPago.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            f_SetPreviousData();
        }
        private void f_SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList codfor = (DropDownList)grvFormaPago.Rows[rowIndex].Cells[2].FindControl("ddlgrvCodfor");
                        TextBox codban = (TextBox)grvFormaPago.Rows[rowIndex].Cells[3].FindControl("txtgrvCodban");
                        TextBox docfpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[4].FindControl("txtgrvDocfpa");
                        TextBox baufpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[4].FindControl("txtgrvBaufpa");
                        TextBox numtar = (TextBox)grvFormaPago.Rows[rowIndex].Cells[5].FindControl("txtgrvNumtar");
                        TextBox valfpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[6].FindControl("txtgrvValfpa");

                        codfor.Text = dt.Rows[i]["Forma de Pago"].ToString();
                        codban.Text = dt.Rows[i]["Banco"].ToString();
                        docfpa.Text = dt.Rows[i]["Lote"].ToString();
                        baufpa.Text = dt.Rows[i]["Referencia"].ToString();
                        numtar.Text = dt.Rows[i]["N° Tarjeta/NC"].ToString();
                        valfpa.Text = dt.Rows[i]["Valor"].ToString();


                        rowIndex++;
                    }
                }
            }
            f_calcularFalta();
        }
        protected void ddlgrvCodfor_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            GridViewRow gvRow = (GridViewRow)ddl.NamingContainer;
            int rowIndex = gvRow.RowIndex; //para saber la fila seleccionada

            //int rowIndex = int.Parse(Session["gs_renSelecxtedRow"].ToString());
            //int rowIndex = 0;
            GridViewRow row = grvFormaPago.Rows[rowIndex];
            TextBox txtgrvCodban = (TextBox)row.FindControl("txtgrvCodban");
            DropDownList ddlgrvCodfor = (DropDownList)row.FindControl("ddlgrvCodfor");
            string s_codfor = ddlgrvCodfor.SelectedValue.ToString().Trim();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();
            string s_checktar = "SELECT B.nomban FROM bancos B, tab_formapago T where B.codban = T.codban AND T.codfor = @s_codfor"; //OJO codfor se debe filtrar con length
            //cfg_formapago
            //cfg_banco
            SqlCommand com = new SqlCommand(s_checktar, conn);
            com.Parameters.AddWithValue("@s_codfor", s_codfor);
            if (com.ExecuteScalar() != null)
                txtgrvCodban.Text = com.ExecuteScalar().ToString();
            else
                txtgrvCodban.Text = "";
            conn.Close();
        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            f_AddNewRowToGrid();
        }

        //PARA PODER BORRAR UN RENGLON
        protected void lkgrvBorrar_Click(object sender, EventArgs e)
        {
            //antes de borrar se actualizar el CurrentTable con grvRenglones
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        DropDownList codfor = (DropDownList)grvFormaPago.Rows[rowIndex].Cells[2].FindControl("ddlgrvCodfor");
                        TextBox codban = (TextBox)grvFormaPago.Rows[rowIndex].Cells[3].FindControl("txtgrvCodban");
                        TextBox docfpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[4].FindControl("txtgrvDocfpa");
                        TextBox baufpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[4].FindControl("txtgrvBaufpa");
                        TextBox numtar = (TextBox)grvFormaPago.Rows[rowIndex].Cells[5].FindControl("txtgrvNumtar");
                        TextBox valfpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[6].FindControl("txtgrvValfpa");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["Forma de Pago"] = codfor.Text;
                        dtCurrentTable.Rows[i - 1]["Banco"] = codban.Text;
                        dtCurrentTable.Rows[i - 1]["Lote"] = docfpa.Text;
                        dtCurrentTable.Rows[i - 1]["Referencia"] = baufpa.Text;
                        dtCurrentTable.Rows[i - 1]["N° Tarjeta/NC"] = numtar.Text;
                        dtCurrentTable.Rows[i - 1]["Valor"] = valfpa.Text;

                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }

            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1) //si selecciona una fila que NO sea la primera
                {
                    if (!(gvRow.RowIndex == 0 && dt.Rows.Count == 1)) //no permite borrar la primera fila
                    {
                        //Remove the Selected Row data
                        dt.Rows.Remove(dt.Rows[rowID]);
                    }
                }
                else //si selecciona la primera fila
                {
                    if (gvRow.RowIndex == 0 && dt.Rows.Count == 1) //para borrar los datos de la primera fila
                    {
                        dt.Rows[0]["Forma de Pago"] = "";
                        dt.Rows[0]["Banco"] = "";
                        dt.Rows[0]["Lote"] = "";
                        dt.Rows[0]["Referencia"] = "";
                        dt.Rows[0]["N° Tarjeta/NC"] = "";
                        dt.Rows[0]["Valor"] = "";
                    }
                }

                //Store the current data in ViewState for future reference
                ViewState["CurrentTable"] = dt;
                //Re bind the GridView for the updated data
                grvFormaPago.DataSource = dt;
                grvFormaPago.DataBind();
            }
            //Set Previous Data on Postbacks
            f_SetPreviousData();
            f_calcularFalta(); //para recalcular txtValfpa en caso de borrar una fila con valor
        }

        public void f_renglonesActuales() //para guardar en ViewState["CurrentTable"] los renglones mostrados en pantalla
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        DropDownList codfor = (DropDownList)grvFormaPago.Rows[rowIndex].Cells[2].FindControl("ddlgrvCodfor");
                        TextBox codban = (TextBox)grvFormaPago.Rows[rowIndex].Cells[3].FindControl("txtgrvCodban");
                        TextBox docfpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[4].FindControl("txtgrvDocfpa");
                        TextBox baufpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[4].FindControl("txtgrvBaufpa");
                        TextBox numtar = (TextBox)grvFormaPago.Rows[rowIndex].Cells[5].FindControl("txtgrvNumtar");
                        TextBox valfpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[6].FindControl("txtgrvValfpa");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["Forma de Pago"] = codfor.Text;
                        dtCurrentTable.Rows[i - 1]["Banco"] = codban.Text;
                        dtCurrentTable.Rows[i - 1]["Lote"] = docfpa.Text;
                        dtCurrentTable.Rows[i - 1]["Referencia"] = baufpa.Text;
                        dtCurrentTable.Rows[i - 1]["N° Tarjeta/NC"] = numtar.Text;
                        dtCurrentTable.Rows[i - 1]["Valor"] = valfpa.Text;

                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }
        }

        public void f_calcularFalta()
        {
            double r_valFpa = 0;
            double r_valTotal = 0;

            int rowIndex = 0;
            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            if (dtCurrentTable.Rows.Count > 0)
            {
                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                {
                    TextBox valfpa = (TextBox)grvFormaPago.Rows[rowIndex].Cells[6].FindControl("txtgrvValfpa");
                    if (String.IsNullOrEmpty(valfpa.Text))
                    {
                        valfpa.Text = "0.00";
                    }
                    r_valFpa = r_valFpa + double.Parse(valfpa.Text);
                    rowIndex++;
                }
            }

            try
            {
                //r_valFpa = double.Parse(txtgrvValfpa.Text);
                r_valTotal = double.Parse(txtTotfac.Text) - r_valFpa;
                if (r_valTotal <= 0)
                    btnConfirmar.Enabled = true;
                else
                    btnConfirmar.Enabled = false;
            }
            catch (Exception ex)
            {
                lblError.Text = "❗ *01. " + DateTime.Now + ex.Message;
            }
            txtTotrec.Text = r_valFpa.ToString("N2");
            txtCambio.Text = r_valTotal < 0 ? (r_valTotal * (-1)).ToString("N2") : "0.00";
            txtFalta.Text = r_valTotal.ToString("N2");
        }
        protected void txtgrvValfpa_TextChanged(object sender, EventArgs e)
        {
            TextBox tx = (TextBox)sender;
            GridViewRow gvRow = (GridViewRow)tx.NamingContainer;
            TextBox txtgrvValfpa = (TextBox)gvRow.FindControl("txtgrvValfpa");

            if (String.IsNullOrEmpty(txtgrvValfpa.Text))
            {
                txtgrvValfpa.Text = "0.00";
            }

            f_calcularFalta();
            f_renglonesActuales();

            //if (r_valTotal > 0)
            //{
            //    f_AddNewRowToGrid();
            //}
            //else
            //{
            //    btnAgregar.Enabled = false;
            //    btnConfirmar.Enabled = true; //para habilitar btnConfirmar si ya pago la cantidad necesaria
            //    //btnConfirmar.CssClass = "btn btn-primary btn-sm";
            //}
        }

        protected void btnConfirmar_Click(object sender, EventArgs e)
        {
            //***i GUARDAR EN dt el GRV forma de pago***//
            DataTable dt = new DataTable();
            dt.Columns.Add("Código");
            dt.Columns.Add("Forma de Pago");
            dt.Columns.Add("Valor");
            foreach (GridViewRow row in grvFormaPago.Rows)
            {
                DataRow dr = dt.NewRow();
                DropDownList codfor = (DropDownList)row.Cells[0].FindControl("ddlgrvCodfor");
                dr["Código"] = codfor.SelectedValue.ToString();
                dr["Forma de Pago"] = codfor.SelectedItem.ToString();
                TextBox valfpa = (TextBox)row.Cells[0].FindControl("txtgrvValfpa");
                dr["Valor"] = valfpa.Text;

                dt.Rows.Add(dr);
            }
            Session.Add("gdt_FormaPagoImprimir", dt);
            //***f GUARDAR EN dt el GRV forma de pago***//

            if (Session["gs_Existe"].ToString().Equals("Add")) //SI VA A INSERTAR
            {
                //***INSERTAR ENCABEZADO***//
                objFactura = (cls_cp_FacturaServicios)Session["g_objFacturaServico"];
                clsError objError = objFactura.f_Factura_Actualizar(objFactura, Session["gs_Existe"].ToString());
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    //INSERTA RENGLON
                    DataTable dtRenglones = (DataTable)Session["gdt_FacturaServicioRen"];
                    objFacturaRen = f_generarObjFacturaRen();
                    objFacturaRen.dtFacturaRen = dtRenglones;
                    objError = objFacturaRen.f_Factura_Ren_Actualizar(objFacturaRen, Session["gs_Existe"].ToString());
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        objError = objSencuencia.f_ActualizarSecuencia(Session["gs_Codemp"].ToString(),"CP_FAC", Session["gs_Sersec"].ToString());
                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                            lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                            Session["gs_numfacServicioSelected"] = objFactura.Numfac;
                            //DATOS DE RENGLONES FORMA PAGO
                            Session.Add("gdt_FormaPago", (DataTable)ViewState["CurrentTable"]);
                            //Response.Redirect("w_vc_FacturaVistaPrevia.aspx");
                            //Response.Redirect("w_xml.aspx");
                        }
                        else
                        {
                            objFactura.f_Factura_Eliminar(objFactura.Numfac, objFactura.Codemp);
                            f_ErrorNuevo(objFactura.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }

                    }
                    else //error en renglones
                    {
                        objFactura.f_Factura_Eliminar(objFactura.Numfac, objFactura.Codemp);
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
                else //error en el encabezado
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            else //YA ESTABA INSERTADO ...gs_Existe = Update
            {
                Session.Add("gdt_FormaPago", (DataTable)ViewState["CurrentTable"]);
                Response.Redirect("w_vc_FacturaVistaPrevia.aspx");
                //Response.Redirect("w_xml.aspx");
            }
        }

        public cls_cp_FacturaServicios_Ren f_generarObjFacturaRen()
        {
            objFacturaRen.Codemp = Session["gs_Codemp"].ToString();
            //objFacturaRen.Tiptra = "1";
            objFacturaRen.Numfac = txtNumfac.Text;
            objFacturaRen.Codiva = "1";
            //objFacturaRen.Ubifis = "?";

            return objFacturaRen;
        }
    }
}