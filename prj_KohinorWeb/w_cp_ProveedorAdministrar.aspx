﻿<%@ Page Title="Proveedor Administrar" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_cp_ProveedorAdministrar.aspx.cs" Inherits="prj_KohinorWeb.w_cp_ProveedorAdministrar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtCodpro.ClientID%>").blur(function () {
                f_Validar();
            });
        });


    </script>

    <script type="text/javascript">
        function UnCheckOtherCheckBox(chkbox1, chkbox2) {
            if (chkbox1.checked) {
                //Uncheck the other checkbox
                document.getElementById(chkbox2).checked = false;
            }
        }
    </script>

    <script type="text/javascript"> 
        function f_Validar() {
            var s_cod = $("#<%=txtCodpro.ClientID%>").val();
            PageMethods.f_Validar(s_cod, f_SuccessValidacionCod, f_FailValidacionCod);
        }
        function f_SuccessValidacionCod(data) {
            if (data.TipInd == "-" || data.TipEmp == "-") {
                //alert("Solo se puede ingresar RUC = 13 Digitos, CI=10 Digitos, PASAPORTE=9 Digitos");
                alert(data.ErrorCed);
                $("#<%=txtTipind.ClientID%>").val("-");
                $("#<%=txtTipemp.ClientID%>").val("-");
                $("#<%=txtRucced.ClientID%>").val("-");
            }
            else {
                $("#<%=txtTipind.ClientID%>").val(data.TipInd);
                $("#<%=txtTipemp.ClientID%>").val(data.TipEmp);
                $("#<%=txtRucced.ClientID%>").val(data.RucCed);
            }
        }
        function f_FailValidacionCod() {
            alert("ERROR : Método getValues_Success(data) fallo.");
        }



        function f_vistaPreviaImg() {
            var preview = document.querySelector('#<%=imgFotoProveedor.ClientID %>');
            var file = document.querySelector('#<%=imgProveedor.ClientID %>').files[0];
            var reader = new FileReader();

            reader.onloadend = function () {
                preview.src = reader.result;
            }

            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        }
    </script>

</asp:Content>
<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item"><a href="w_cp_Proveedor.aspx">Proveedores</a></li>
            <li class="breadcrumb-item active">Administrar Proveedor</li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <a class="c-subheader-nav-link">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary" type="button" id="">
                            <i class="cil-filter"></i>
                        </button>
                    </div>

                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
                </div>
                <asp:LinkButton ID="lkbtnSearch" runat="server" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
            </a>

            <a class="c-subheader-nav-link">
                <i class="c-icon cil-speech"></i>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-graph"></i>&nbsp;
                            Escritorio
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-settings"></i>
            </a>
        </div>
    </div>
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item active">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click"><i class="cil-file"></i>Nuevo</button>
                    <button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" onserverclick="btnAbrir_Click"><i class="cil-folder-open"></i>Abrir</button>
                    <button type="button" class="btn btn-outline-dark" id="btnGuardar" runat="server" onserverclick="btnGuardarProveedor_Click"><i class="cil-save"></i>Guardar</button>
                    <button type="button" class="btn btn-outline-dark" id="btnCancelar" runat="server" onserverclick="btnCancelar_Click"><i class="cil-grid-slash"></i>Cancelar</button>
                    <button type="button" class="btn btn-outline-danger" id="btnEliminar" runat="server" onserverclick="lkbtnDelete_Click"><i class="cil-trash"></i>Eliminar</button>
                </div>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-dark" id="lkbtnPrincipio" runat="server" onserverclick="lkbtnPrincipio_Click"><i class="cil-media-step-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnAtras" runat="server" onserverclick="lkbtnAtras_Click"><i class="cil-media-skip-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnSiguiente" runat="server" onserverclick="lkbtnSiguiente_Click"><i class="cil-media-skip-forward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnFinal" runat="server" onserverclick="lkbtnFinal_Click"><i class="cil-media-step-forward"></i></button>
                </div>

                <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>

            </li>
        </ol>

    </div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <!-- Begin Page Content -->
                <div id="frmVerificar" class="needs-validation" runat="server" novalidate>
                    <!-- Page Heading -->
                    <div class="card text-white bg-secondary text-black-50">
                        <div class="card-body">

                            <div class="form-row">
                                <div class="col-md-2">
                                    <h2><strong>Gestión Proveedores</strong></h2>
                                </div>

                                <div class="form-group col-md-2">
                                    <input type="text" id="txtCodpro" class="form-control" runat="server" required />
                                    <label for="txtCodpro">Código</label>
                                </div>
                                <div class="form-group col-md-3">
                                    <asp:TextBox ID="txtTipemp" runat="server" class="form-control" placeholder="Empresa" required></asp:TextBox>
                                </div>
                                <div class="form-group col-md-2">
                                    <asp:TextBox ID="txtTipind" runat="server" class="form-control" placeholder="Indentificación" required></asp:TextBox>

                                </div>

                                <div class="form-group col-md-3">
                                    <asp:TextBox ID="txtRucced" runat="server" class="form-control" placeholder="Número"></asp:TextBox>
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-md-2 align-items-center justify-content-center">
                                    <div class="form-row align-items-center justify-content-center">
                                        <asp:Image ID="imgFotoProveedor" class="img-thumbnail" runat="server" Width="110px" Height="140px" />
                                        <div class="input-group mb-3 mt-3">
                                            <div class="custom-file">
                                                <input id="imgProveedor" type="file" class="custom-file-input" onchange="f_vistaPreviaImg()" runat="server">
                                                <label class="custom-file-label" for="imgProveedor" aria-describedby="inputGroupFileAddon02">Foto</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <input type="text" id="txtNompro" class="form-control" runat="server" required />
                                            <label for="txtNompro">Proveedor</label>
                                            <div class="valid-feedback">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <asp:DropDownList ID="ddlCodcla" runat="server" class="form-control" DataSourceID="sqldsCodcla" DataTextField="nomcla" DataValueField="codcla"></asp:DropDownList>
                                            <label for="ddlCodcla">Clase Proveedor</label>
                                        </div>
                                        <div class="form-group col-md-3">

                                            <asp:DropDownList ID="ddlCodcta" runat="server" class="form-control" DataSourceID="sqldsCodcta" DataTextField="nomcta" DataValueField="codcta"></asp:DropDownList>

                                            <label for="txtCodcta">Codigo Cuenta</label>
                                        </div>


                                        <div class="form-group col-md-2">
                                            <asp:DropDownList ID="ddlEstatus" class="form-control" runat="server" DataSourceID="sqldsEstatus" DataTextField="nomest" DataValueField="estatus" required></asp:DropDownList>
                                            <label for="txtCodcta">Estatus</label>
                                        </div>

                                    </div>



                                    <div class="form-row">

                                        <div class="form-group col-md-4">
                                            <div class="card-body">

                                                <div class="form-row">

                                                    <h5>Tipo de Proveedor </h5>
                                                    <div class="form-group col-md-9">
                                                        <asp:DropDownList ID="ddlTippro" class="form-control" runat="server">
                                                            <asp:ListItem Value="01">Local</asp:ListItem>
                                                            <asp:ListItem Value="02">Exterior</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <label for="ddlTippro">Tipo</label>
                                                    </div>

                                                    <div class="form-group col-md-3">
                                                        <asp:DropDownList ID="ddlApliva" class="form-control" runat="server" DataSourceID="sqldsApliva" DataTextField="nomiva" DataValueField="codiva"></asp:DropDownList>
                                                        <label for="ddlApliva">IVA</label>
                                                    </div>

                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label>Relacionado</label>
                                                    </div>



                                                    <div class="form-group col-md-2">


                                                        <asp:RadioButton ID="rbtSiRelacionado" GroupName="op1" runat="server" />
                                                        <label for="rbtSiRelacionado">Si</label>

                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <asp:RadioButton ID="rbtNoRelacionado" GroupName="op1" runat="server" />
                                                        <label for="rbtNoRelacionado">No</label>



                                                    </div>



                                                </div>
                                                <div class="form-row">

                                                    <div class="form-group col-md-6">
                                                        <label>Contibuyente Especial</label>
                                                    </div>
                                                    <div class="form-group col-md-2">



                                                        <asp:RadioButton ID="rbtSiContribuyente" runat="server" GroupName="op2" />
                                                        <label for="rbtSiContribuyente">Si</label>

                                                    </div>
                                                    <div class="form-group col-md-2">

                                                        <asp:RadioButton ID="rbtNoContribuyente" runat="server" GroupName="op2" />
                                                        <label for="rbtNoContribuyente">No</label>

                                                    </div>



                                                </div>


                                            </div>


                                        </div>


                                        <div class="form-group col-md-8">
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <h5>Datos ATS </h5>
                                                </div>
                                                <div class="form-row">


                                                    <div class="form-group col-md-3">
                                                        <asp:TextBox ID="txtNumser" runat="server" class="form-control"></asp:TextBox>

                                                        <label for="txtNumser">Serie</label>
                                                    </div>

                                                    <div class="form-group col-md-3">
                                                        <asp:TextBox ID="txtNumini" runat="server" class="form-control"></asp:TextBox>
                                                        <label for="txtNumini">N° Desde</label>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <asp:TextBox ID="txtNumfin" runat="server" class="form-control"></asp:TextBox>
                                                        <label for="txtNumfin">N° Hasta</label>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <asp:TextBox ID="TextBox1" runat="server" class="form-control" TextMode="Date"></asp:TextBox>
                                                        <label for="txtNumfin">Valido Hasta</label>
                                                    </div>

                                                </div>

                                                <div class="form-row">

                                                    <div class="form-group col-md-6">
                                                        <asp:TextBox ID="txtClacon" runat="server" class="form-control"></asp:TextBox>
                                                        <label for="txtClacon">N° Aut.Contribuyente</label>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <asp:TextBox ID="txtClaimp" runat="server" class="form-control"></asp:TextBox>
                                                        <label for="txtClaimp">N° Aut.Imprenta</label>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>



                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="card shadow ">
                        <div class="card-body text-white bg-secondary text-black-50">

                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <div class="card-body">

                                        <div class="form-row">

                                            <h5>Localizacion Geografica</h5>
                                            <div class="form-group col-md-12">
                                                <asp:TextBox ID="txtContac" runat="server" class="form-control"></asp:TextBox>

                                                <label for="txtContac">Contacto</label>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <asp:DropDownList ID="ddlCodgeo" class="form-control" runat="server" DataSourceID="sqldsCodgeo" DataTextField="nomgeo" DataValueField="codgeo"></asp:DropDownList>
                                                <label for="ddlCodgeo">Provincia</label>
                                            </div>

                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <asp:TextBox ID="txtNomciu" runat="server" class="form-control"></asp:TextBox>
                                                <label for="txtNomciu">Ciudad</label>
                                            </div>

                                        </div>

                                    </div>

                                </div>




                                <div class="form-group col-md-4">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <h5>Email/Telefonos</h5>
                                        </div>
                                        <div class="form-row">


                                            <div class="form-group col-md-12">
                                                <asp:TextBox ID="txtEmapro" runat="server" class="form-control"></asp:TextBox>

                                                <label for="txtEmapro">Retenciones</label>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <asp:TextBox ID="txtEmacom" runat="server" class="form-control"></asp:TextBox>
                                                <label for="txtEmacom">Compras</label>
                                            </div>
                                        </div>

                                        <div class="form-row">


                                            <div class="form-group col-md-12">
                                                <asp:TextBox ID="txtTelpro" runat="server" class="form-control"></asp:TextBox>
                                                <label for="txtTelpro">Telefono</label>
                                            </div>


                                        </div>




                                    </div>

                                </div>

                                <div class="form-group col-md-4">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <h5>Direccion</h5>
                                        </div>
                                        <div class="form-row">


                                            <div class="form-group col-md-10">
                                                <asp:TextBox ID="txtDirpro" runat="server" class="form-control"></asp:TextBox>

                                                <label for="txtDirpro">Calles</label>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <asp:TextBox ID="txtNumcal" runat="server" class="form-control"></asp:TextBox>
                                                <label for="txtNumcal">N° Calle</label>
                                            </div>


                                        </div>




                                    </div>

                                </div>



                            </div>


                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <h5>Datos de Transferencia </h5>
                                        </div>
                                        <div class="form-row">


                                            <div class="form-group col-md-5">
                                                <asp:TextBox ID="txtCedben" runat="server" class="form-control"></asp:TextBox>

                                                <label for="txtCedben">Beneficiario</label>
                                            </div>

                                            <div class="form-group col-md-7">
                                                <asp:TextBox ID="txtNomben" runat="server" class="form-control"></asp:TextBox>

                                                <label for="txtNomben">Nombre</label>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-5">
                                                <asp:DropDownList ID="ddlTipcue" runat="server" class="form-control">
                                                    <asp:ListItem Value="01">Corriente</asp:ListItem>
                                                    <asp:ListItem Value="02">Ahorro</asp:ListItem>
                                                    <asp:ListItem Value="03">Efectivo</asp:ListItem>
                                                    <asp:ListItem Value="04">Virtual</asp:ListItem>
                                                </asp:DropDownList>
                                                <label for="ddlTipcue">Tipo Cuenta</label>
                                            </div>

                                            <div class="form-group col-md-7">
                                                <asp:DropDownList ID="ddlCodban" runat="server" class="form-control" DataSourceID="sqldsCodban" DataTextField="nomban" DataValueField="codban"></asp:DropDownList>
                                                <label for="ddlCodban">Banco</label>
                                            </div>

                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <asp:TextBox ID="txtNumcue" runat="server" class="form-control"></asp:TextBox>
                                                <label for="txtNumcue">Numero Cuenta</label>
                                            </div>
                                        </div>

                                    </div>

                                </div>


                                <div class="form-group col-md-6">
                                    <div class="card-body">

                                        <div class="form-row">

                                            <h5>Datos Contables</h5>
                                            <div class="form-group col-md-12">
                                                <asp:TextBox ID="txtLimcre" runat="server" class="form-control"></asp:TextBox>

                                                <label for="txtLimcre">Devolucion IVA</label>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-1">
                                                <label>N° de Pagos</label>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <asp:TextBox ID="txtNumpag" runat="server" class="form-control"></asp:TextBox>
                                                <label for="txtNumpag">Cuotas</label>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <asp:TextBox ID="txtPlapag" runat="server" class="form-control"></asp:TextBox>
                                                <label for="txtPlapag">Plazos</label>
                                            </div>

                                        </div>


                                    </div>

                                </div>


                            </div>



                            <div class="form-row">

                                <div class="form-group col-md-12">
                                    <div class="card-body">

                                        <div class="form-row">

                                            <h5>Datos de Credito</h5>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-5">
                                                <asp:DropDownList ID="ddlDeviva" runat="server" class="form-control">
                                                    <asp:ListItem Value="S">Si</asp:ListItem>
                                                    <asp:ListItem Value="N">No</asp:ListItem>
                                                </asp:DropDownList>

                                                <label for="ddlDeviva">Devolucion IVA</label>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <asp:DropDownList ID="ddlBieiva" runat="server" class="form-control"></asp:DropDownList>

                                                <label for="ddlBieiva">Retencion IVA</label>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <asp:DropDownList ID="ddlBietri" runat="server" class="form-control"></asp:DropDownList>

                                                <label for="ddlBieiva">Cre. T.</label>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-5">
                                                <asp:DropDownList ID="ddlSerret" runat="server" class="form-control"></asp:DropDownList>
                                                <label for="ddlSerret">Servicios</label>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <asp:DropDownList ID="ddlSeriva" runat="server" class="form-control"></asp:DropDownList>

                                            </div>
                                            <div class="form-group col-md-2">
                                                <asp:DropDownList ID="ddlSertri" runat="server" class="form-control"></asp:DropDownList>

                                            </div>

                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <asp:TextBox ID="txtCodart" runat="server" class="form-control"></asp:TextBox>
                                                <label for="txtCodart">Codigo</label>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <asp:TextBox ID="txtNomart" runat="server" class="form-control"></asp:TextBox>
                                                <label for="txtNomart">Nombre</label>
                                            </div>


                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <asp:TextBox ID="txtObserv" runat="server" class="form-control"></asp:TextBox>
                                                <label for="txtObserv">Observaciones</label>
                                            </div>



                                        </div>



                                    </div>

                                </div>



                            </div>








                            <button id="btnGuardarProveedor" type="submit" value="button" runat="server" class="btn btn-primary float-right" onserverclick="btnGuardarProveedor_Click"><i class="cil-save"></i>Guardar</button>

                        </div>

                    </div>

                    <!-- Crear Nuevo Proveedor -->
                </div>
            </div>

            <!-- /.container-fluid -->
            <!-- End of Main Content -->
        </div>
    </main>
    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsProveedorExiste" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codpro] FROM [cp_proveedor] WHERE (([codemp] = @codemp) AND ([codpro] = @codpro))">
        <SelectParameters>
            <asp:SessionParameter Name="codemp" SessionField="gs_Codemp" Type="String" />
            <asp:SessionParameter Name="codpro" SessionField="gs_codproSelected" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsCodcla" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codcla], [nomcla] FROM [cp_proveedor_clase]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsEstatus" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [nomest], [estatus] FROM [cfg_estatus]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsApliva" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codiva], [nomiva] FROM [cfg_iva]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodgeo" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codgeo], [nomgeo] FROM [cfg_locgeo]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodban" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codban], [nomban] FROM [cfg_banco]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodcta" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codcta], [nomcta] FROM [fin_plancuenta]"></asp:SqlDataSource>


    <asp:SqlDataSource ID="sqldsClaseProveedor" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codcla])) AS codcla, [nomcla] FROM [vc_cliente_clase] "></asp:SqlDataSource>


    <!--f SQL DATASOURCES-->


    <!-- Modal Eliminar -->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Proveedor</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar al Proveedor '<asp:Label ID="lblEliCli" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarProveedor_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>
</asp:Content>
