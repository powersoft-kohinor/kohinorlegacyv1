﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_cp_ProveedorAdministrar : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "2", niv001 = "2", niv002 = "11";
        protected cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Add("gs_Siguiente", ""); //para el lkbtnSiguiente Y lkbtnAtras

                f_Seg_Usuario_Menu();

                if (String.IsNullOrEmpty(Session["gs_codproSelected"].ToString()))
                    Session.Add("gs_Action", "Add");
                else
                    Session.Add("gs_Action", "Update");

                ddlCodcla.DataBind(); //OJO DEBE IR ANTES DE f_llenarCamposEditar
                ddlCodcta.DataBind();
                ddlEstatus.DataBind();
                ddlTipcue.DataBind();
                ddlApliva.DataBind();
                ddlCodgeo.DataBind();
                ddlTipcue.DataBind();
                ddlCodban.DataBind();
                ddlDeviva.DataBind();

                objProveedor = objProveedor.f_Proveedor_Buscar(Session["gs_Codemp"].ToString(), Session["gs_codproSelected"].ToString());

                if (String.IsNullOrEmpty(objProveedor.Error.Mensaje))
                {
                    f_llenarCamposEditar(objProveedor);
                }
                else
                {
                    f_ErrorNuevo(objProveedor.Error);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            if (IsPostBack)
            {
                lblExito.Text = "";
                lblError.Text = "";
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAbrir.Attributes.Add("class", "btn btn-dark");
                btnNuevo.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-dark");
                btnCancelar.Attributes.Add("class", "btn btn-dark");
                btnEliminar.Attributes.Add("class", "btn btn-danger");

                lkbtnPrincipio.Attributes.Add("class", "btn btn-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");

            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
                btnNuevo.Attributes.Add("class", "btn btn-outline-primary");

                btnGuardar.Attributes.Add("class", "btn btn-outline-dark");
                btnCancelar.Attributes.Add("class", "btn btn-outline-dark");
                btnEliminar.Attributes.Add("class", "btn btn-outline-danger ");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");

            }
        }

        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }

        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=:exclamation: Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");


                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnNuevo.Disabled = true;
                    }
                    if (row["banbus"].Equals("N")) //Abrir
                    {
                        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnAbrir.Disabled = true;
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnGuardar.Disabled = true;
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnCancelar.Disabled = true;

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEliminar.Disabled = true;
                        Session["baneli"] = "N";
                    }
                }
            }
            return dt;
        }


        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_codproSelected"] = "";
            f_limpiarCamposEditar();

        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_cp_Proveedor.aspx");
        }
        protected void btnGuardarProveedor_Click(object sender, EventArgs e)
        {
            frmVerificar.Attributes.Add("class", "needs-validation was-validated");

            string s_imgcli = "";
            //if (imgCliente.PostedFile != null)
            //{
            //    //si hay una archivo.
            //    string nombreArchivo = imgCliente.PostedFile.FileName;
            //    s_imgcli = "~/Modulos/Ventas/ImgCliente/" + nombreArchivo;
            //    imgCliente.PostedFile.SaveAs(Server.MapPath(s_imgcli));
            //}


            if (txtCodpro.Value.Equals("") || txtTipemp.Text.Equals("") || txtTipind.Text.Equals("") || txtRucced.Text.Equals("") || txtNompro.Value.Equals("") ||
            txtNumser.Text.Equals("") || txtNumini.Text.Equals("") || txtNumfin.Text.Equals("") || txtClacon.Text.Equals("") || txtClaimp.Text.Equals("") || txtContac.Text.Equals("") || txtNomciu.Text.Equals("") || txtEmapro.Text.Equals("") &&
            txtEmacom.Text.Equals("") || txtTelpro.Text.Equals("") || txtDirpro.Text.Equals("") || txtNumcal.Text.Equals("") || txtCedben.Text.Equals("") || txtNomben.Text.Equals("") || txtNumcue.Text.Equals("") || txtLimcre.Text.Equals("") &&
            txtNumpag.Text.Equals("") || txtPlapag.Text.Equals("") || txtCodart.Text.Equals("") || txtNomart.Text.Equals("") || txtObserv.Text.Equals(""))
            {

                DataSourceSelectArguments args = new DataSourceSelectArguments();
                DataView view = (DataView)sqldsProveedorExiste.Select(args);
                if (view != null) //ya existe el pedido (Actualizar)
                {
                    objProveedor = f_generarObjProveedor();
                    clsError objError = objProveedor.f_Proveedor_Actualizar(objProveedor, "Update");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {

                        lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;


                    }
                    else
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
                else //no existe el pedido (Insertar)
                {
                    objProveedor = f_generarObjProveedor();
                    clsError objError = objProveedor.f_Proveedor_Actualizar(objProveedor, "Add");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        //INSERTA RENGLON

                        lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;

                    }
                    else
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
            }
            else
            {
                lblError.Text = "❗ *01. " + DateTime.Now + " Debe llenar todos los campos para crear un cliente.";
            }

        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_codproSelected"] = "";
            f_limpiarCamposEditar();
        }

        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_codproSelected"].ToString()))
            {
                //Session["gs_codproSelected"] = txtCodcli.Value;
                lblEliCli.Text = Session["gs_codproSelected"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar un proveedor para eliminar.";
            }
        }
        protected void btnEliminarProveedor_Click(object sender, EventArgs e)
        {
            cls_cp_Proveedor objProveedor = new cls_cp_Proveedor(); //eliminar
            clsError objError = objProveedor.f_Proveedor_Eliminar(Session["gs_Codemp"].ToString(), Session["gs_codproSelected"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                f_limpiarCamposEditar();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

        }


        protected void lkbtnPrincipio_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objProveedor = objProveedor.f_Proveedor_Principio(Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objProveedor.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codproSelected"] = objProveedor.Codpro;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objProveedor.Nompro;

                f_llenarCamposEditar(objProveedor);
            }
            else
            {
                f_ErrorNuevo(objProveedor.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

        }

        protected void lkbtnSiguiente_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objProveedor = objProveedor.f_Proveedor_Siguiente(Session["gs_Codemp"].ToString().Trim(), Session["gs_Siguiente"].ToString().Trim());
            if (String.IsNullOrEmpty(objProveedor.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codproSelected"] = objProveedor.Codpro;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objProveedor.Nompro;

                f_llenarCamposEditar(objProveedor);
            }
            else
            {
                f_ErrorNuevo(objProveedor.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void lkbtnFinal_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objProveedor = objProveedor.f_Proveedor_Final(Session["gs_Codemp"].ToString().Trim());
            if (String.IsNullOrEmpty(objProveedor.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codproSelected"] = objProveedor.Codpro;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objProveedor.Nompro;

                f_llenarCamposEditar(objProveedor);
            }
            else
            {
                f_ErrorNuevo(objProveedor.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void lkbtnAtras_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objProveedor = objProveedor.f_Proveedor_Atras(Session["gs_Codemp"].ToString().Trim(), Session["gs_Siguiente"].ToString().Trim());
            if (String.IsNullOrEmpty(objProveedor.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codproSelected"] = objProveedor.Codpro;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objProveedor.Nompro;

                f_llenarCamposEditar(objProveedor);
            }
            else
            {
                f_ErrorNuevo(objProveedor.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }



        [WebMethod]
        public static cls_cp_Proveedor f_Validar(string s_codcli)
        {
            cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
            objProveedor.RucCed = s_codcli;

            if (s_codcli.Length == 10) //si es CEDULA
            {
                objProveedor.TipInd = "2";
                objProveedor.TipEmp = "3"; // NO ESTABA EN VALIDACION ORIGINAL
                if (s_codcli.Substring(2, 1).Equals("0") || s_codcli.Substring(2, 1).Equals("1") ||
                    s_codcli.Substring(2, 1).Equals("2") || s_codcli.Substring(2, 1).Equals("3") ||
                    s_codcli.Substring(2, 1).Equals("4") || s_codcli.Substring(2, 1).Equals("5"))
                {
                    objProveedor.TipEmp = "3";
                }
                else
                {
                    objProveedor.TipEmp = "-"; //para mandarle al error en f_ValidarCedula
                    objProveedor.ErrorCed = "3er digito de cédula incorrecto.";
                }
            }
            else
            {
                if (s_codcli.Length == 13) //si es RUC
                {
                    objProveedor.TipInd = "1";
                    if (s_codcli.Substring(2, 1).Equals("9"))
                    {
                        objProveedor.TipEmp = "1";
                    }
                    else
                    {
                        if (s_codcli.Substring(2, 1).Equals("6"))
                        {
                            objProveedor.TipEmp = "2";
                        }
                        else
                        {
                            objProveedor.TipEmp = "3";
                        }
                    }
                }
                else
                {
                    if (s_codcli.Length == 9) //si es PASAPORTE
                    {
                        objProveedor.TipInd = "3";
                        objProveedor.TipEmp = "3";
                    }
                    else //ERROR, NO ES NI CEDULA NI RUC NI PASAPORTE (tiene digitos de mas o de menos)
                    {
                        objProveedor.TipInd = "-";
                        objProveedor.TipEmp = "-";
                        objProveedor.RucCed = "-";
                    }
                }
            }

            string s_tipemp = objProveedor.TipEmp.ToString();

            string s_error = objProveedor.f_ValidacionCedula(s_codcli, s_tipemp);
            if (!s_error.Equals("CORRECTO"))
            {
                objProveedor.ErrorCed = objProveedor.f_ValidacionCedula(s_codcli, s_tipemp);
                objProveedor.TipInd = "-";
                objProveedor.TipEmp = "-";
                objProveedor.RucCed = "-";
            }

            return objProveedor;
        }




        public void f_limpiarCamposEditar()
        {
            frmVerificar.Attributes.Add("class", "needs-validation");
            //Image2.ImageUrl = "~/imgusu/user.png";
            txtCodpro.Value = txtTipemp.Text = txtTipind.Text = txtRucced.Text = txtNompro.Value = txtTipemp.Text = txtRucced.Text =
            txtNumser.Text = txtNumini.Text = txtNumfin.Text = txtClacon.Text = txtClaimp.Text = txtContac.Text = txtNomciu.Text = txtEmapro.Text =
            txtEmacom.Text = txtTelpro.Text = txtDirpro.Text = txtNumcal.Text = txtCedben.Text = txtNomben.Text = txtNumcue.Text = txtLimcre.Text =
            txtNumpag.Text = txtPlapag.Text = txtCodart.Text = txtNomart.Text = txtObserv.Text = "";

            lblError.Text = lblError.Text = "";
        }

        public void f_llenarCamposEditar(cls_cp_Proveedor objProveedor)
        {
            //string tmpImgCli = objProveedor.Imgcli;
            //if (!tmpImgCli.Equals(""))
            //{
            //    imgFotoCliente.Attributes.Add("src", tmpImgCli);
            //}
            //else
            //{
            //    imgFotoCliente.Attributes.Add("src", "~/Modulos/Ventas/ImgCliente/user.png");
            //}


            txtCodpro.Value = objProveedor.Codpro;
            txtTipemp.Text = objProveedor.TipEmp;
            txtTipind.Text = objProveedor.TipInd;
            txtRucced.Text = objProveedor.RucCed;
            txtNompro.Value = objProveedor.Nompro;
            txtNumser.Text = objProveedor.Numser;
            txtNumini.Text = objProveedor.Numini;
            txtNumfin.Text = objProveedor.Numfin;
            txtClacon.Text = objProveedor.Clacon;
            txtClaimp.Text = objProveedor.Claimp;
            txtContac.Text = objProveedor.Contac;
            txtNomciu.Text = objProveedor.Codciu;
            txtEmapro.Text = objProveedor.Emapro;
            txtEmacom.Text = objProveedor.Emacom;
            txtTelpro.Text = objProveedor.Telpro;
            txtDirpro.Text = objProveedor.Dirpro;
            txtNumcal.Text = objProveedor.Numcal;
            txtCedben.Text = objProveedor.Cedben;
            txtNomben.Text = objProveedor.Nomben;
            txtNumcue.Text = objProveedor.Numcue;
            txtLimcre.Text = objProveedor.Limcre.ToString();
            txtNumpag.Text = objProveedor.Numpag.ToString();
            txtPlapag.Text = objProveedor.Plapag.ToString();
            txtCodart.Text = objProveedor.Codart;
            txtNomart.Text = objProveedor.Nomart;
            txtObserv.Text = objProveedor.Obspro;

            string s_codcla = objProveedor.Codcla;
            string s_codcta = objProveedor.Codcta;
            string s_estatus = objProveedor.Estatus;
            string s_tippro = objProveedor.Tippro;
            string s_apliva = objProveedor.Apliva;
            string s_codgeo = objProveedor.Codgeo;
            string s_tipcue = objProveedor.Tipcue;
            string s_codban = objProveedor.Codban;
            string s_deviva = objProveedor.Deviva;






            if (String.IsNullOrEmpty(s_codcla))
                ddlCodcla.SelectedValue = ddlCodcla.Items.FindByValue("01   ").Value;
            else
                ddlCodcla.SelectedValue = ddlCodcla.Items.FindByValue(s_codcla).Value;

            if (String.IsNullOrEmpty(s_codcta))
                ddlCodcta.SelectedValue = ddlCodcta.Items.FindByValue("2.1.01.001          ").Value;
            else
                ddlCodcta.SelectedValue = ddlCodcta.Items.FindByValue(s_codcta).Value;

            if (String.IsNullOrEmpty(s_estatus))
                ddlEstatus.SelectedValue = ddlEstatus.Items.FindByValue("A").Value;
            else
                ddlEstatus.SelectedValue = ddlEstatus.Items.FindByValue(s_estatus).Value.Trim();

            if (String.IsNullOrEmpty(s_tippro))
                ddlTippro.SelectedValue = ddlTippro.Items.FindByValue("01").Value;
            else
                ddlTippro.SelectedValue = ddlTippro.Items.FindByValue(s_tippro).Value.Trim();

            if (String.IsNullOrEmpty(s_apliva))
                ddlApliva.SelectedValue = ddlApliva.Items.FindByValue("1").Value;
            else
                ddlApliva.SelectedValue = ddlApliva.Items.FindByValue(s_apliva).Value.Trim();

            if (String.IsNullOrEmpty(s_codgeo))
                ddlCodgeo.SelectedValue = ddlCodgeo.Items.FindByValue("01      ").Value;
            else
                ddlCodgeo.SelectedValue = ddlCodgeo.Items.FindByValue(s_codgeo).Value;

            if (String.IsNullOrEmpty(s_tipcue))
                ddlTipcue.SelectedValue = ddlTipcue.Items.FindByValue("01").Value;
            else
                ddlTipcue.SelectedValue = ddlTipcue.Items.FindByValue(s_tipcue).Value.Trim();

            if (String.IsNullOrEmpty(s_codban))
                ddlCodban.SelectedValue = ddlCodban.Items.FindByValue("10   ").Value;
            else
                ddlCodban.SelectedValue = ddlCodban.Items.FindByValue(s_codban).Value.Trim();

            if (String.IsNullOrEmpty(s_deviva))
                ddlDeviva.SelectedValue = ddlDeviva.Items.FindByValue("N").Value;
            else
                ddlDeviva.SelectedValue = ddlDeviva.Items.FindByValue(s_deviva).Value;





        }

        public cls_cp_Proveedor f_generarObjProveedor()
        {




            string s_estrel = "";
            if (rbtSiRelacionado.Checked)
                s_estrel = "S";
            else
                s_estrel = "N";

            string s_estado = "";
            if (rbtSiRelacionado.Checked)
                s_estado = "S";
            else
                s_estado = "N";


            string fecha_final = DateTime.Now.ToString("yyyy-MM-dd");



            //CAMPOS NORMALES
            objProveedor.Codemp = Session["gs_Codemp"].ToString();
            objProveedor.Codusu = Session["gs_CodUs1"].ToString();

            objProveedor.Estrel = s_estrel;
            objProveedor.Estado = s_estado;

            objProveedor.Codpro = txtCodpro.Value;
            objProveedor.Codcla = String.IsNullOrEmpty(ddlCodcla.SelectedValue) ? "0" : ddlCodcla.SelectedValue;
            objProveedor.Codcta = String.IsNullOrEmpty(ddlCodcta.SelectedValue) ? "0" : ddlCodcta.SelectedValue;
            objProveedor.TipEmp = txtTipemp.Text;
            objProveedor.Tipind = String.IsNullOrEmpty(txtTipind.Text) ? 0 : int.Parse(txtTipind.Text);
            objProveedor.RucCed = txtRucced.Text;
            objProveedor.Nompro = txtNompro.Value;
            objProveedor.Estatus = String.IsNullOrEmpty(ddlEstatus.SelectedValue) ? "0" : ddlEstatus.SelectedValue;
            objProveedor.Numser = txtNumser.Text;
            objProveedor.Numini = txtNumini.Text;
            objProveedor.Numfin = txtNumfin.Text;
            objProveedor.Codgeo = String.IsNullOrEmpty(ddlCodgeo.Text) ? "0" : ddlCodgeo.Text;
            objProveedor.Clacon = txtClacon.Text;
            objProveedor.Claimp = txtClaimp.Text;
            objProveedor.Contac = txtContac.Text;
            objProveedor.Emapro = txtEmapro.Text;
            objProveedor.Emacom = txtEmacom.Text;
            objProveedor.Telpro = txtTelpro.Text;
            objProveedor.Dirpro = txtDirpro.Text;
            objProveedor.Numcal = txtNumcal.Text;
            objProveedor.Cedben = txtCedben.Text;
            objProveedor.Nomben = txtNomben.Text;
            objProveedor.Plapag = String.IsNullOrEmpty(txtPlapag.Text) ? 0 : int.Parse(txtPlapag.Text);
            objProveedor.Codart = txtCodart.Text;
            objProveedor.Nomart = txtNomart.Text;
            objProveedor.Valpro = decimal.Parse("0.00");

            objProveedor.Numcue = txtNumcue.Text;
            objProveedor.Limcre = String.IsNullOrEmpty(txtLimcre.Text) ? decimal.Parse("0.00") : decimal.Parse(txtLimcre.Text);
            objProveedor.Numpag = String.IsNullOrEmpty(txtNumpag.Text) ? 0 : int.Parse(txtNumpag.Text);
            objProveedor.Obspro = txtObserv.Text;
            objProveedor.Tippro = String.IsNullOrEmpty(ddlTippro.SelectedValue) ? "0" : ddlTippro.SelectedValue;
            objProveedor.Apliva = String.IsNullOrEmpty(ddlApliva.SelectedValue) ? "0" : ddlApliva.SelectedValue;
            objProveedor.Tipcue = String.IsNullOrEmpty(ddlTipcue.SelectedValue) ? "0" : ddlTipcue.SelectedValue;
            objProveedor.Codban = String.IsNullOrEmpty(ddlCodban.SelectedValue) ? "0" : ddlCodban.SelectedValue;
            objProveedor.Deviva = String.IsNullOrEmpty(ddlDeviva.SelectedValue) ? "0" : ddlDeviva.SelectedValue;
            objProveedor.Bieiva = String.IsNullOrEmpty(ddlBieiva.SelectedValue) ? "0" : ddlBieiva.SelectedValue;
            objProveedor.Bietri = String.IsNullOrEmpty(ddlBietri.SelectedValue) ? "0" : ddlBietri.SelectedValue;
            objProveedor.Serret = String.IsNullOrEmpty(ddlSerret.SelectedValue) ? "0" : ddlSerret.SelectedValue;
            objProveedor.Seriva = String.IsNullOrEmpty(ddlSeriva.SelectedValue) ? "0" : ddlSeriva.SelectedValue;
            objProveedor.Sertri = String.IsNullOrEmpty(ddlSertri.SelectedValue) ? "0" : ddlSertri.SelectedValue;



            //DATATABLE FACTURA
            DataTable dt = objProveedor.f_dtProveedorTableType();
            DataRow dr = null;
            dr = dt.NewRow();
            dr["codemp"] = Session["gs_Codemp"].ToString();
            dr["codpro"] = txtCodpro.Value;
            dr["codcla"] = String.IsNullOrEmpty(ddlCodcla.SelectedValue) ? "0" : ddlCodcla.SelectedValue;
            dr["codcta"] = String.IsNullOrEmpty(ddlCodcta.SelectedValue) ? "0" : ddlCodcta.SelectedValue;
            dr["nompro"] = txtNompro.Value;
            dr["tipind"] = String.IsNullOrEmpty(txtTipind.Text) ? 0 : int.Parse(txtTipind.Text);
            dr["rucced"] = txtRucced.Text;
            dr["tipemp"] = txtTipemp.Text;
            dr["estatus"] = String.IsNullOrEmpty(ddlEstatus.SelectedValue) ? "0" : ddlEstatus.SelectedValue;
            dr["contac"] = txtContac.Text;
            dr["numser"] = txtNumser.Text;
            dr["clacon"] = txtClacon.Text;
            dr["claimp"] = txtClaimp.Text;
            dr["codgeo"] = String.IsNullOrEmpty(ddlCodgeo.SelectedValue) ? "0" : ddlCodgeo.SelectedValue;
            dr["codciu"] = "";
            dr["dirpro"] = txtDirpro.Text;
            dr["numcal"] = txtNumcal.Text;
            dr["telpro"] = txtTelpro.Text;
            dr["faxpro"] = "";
            dr["apliva"] = String.IsNullOrEmpty(ddlApliva.SelectedValue) ? "0" : ddlApliva.SelectedValue;
            dr["limcre"] = String.IsNullOrEmpty(txtLimcre.Text) ? 0 : decimal.Parse(txtLimcre.Text);
            dr["estado"] = s_estado;
            dr["emapro"] = txtEmapro.Text;
            dr["emacom"] = txtEmacom.Text;
            dr["obspro"] = txtObserv.Text;
            dr["numini"] = txtNumini.Text;
            dr["numfin"] = txtNumfin.Text;
            dr["bieret"] = "";
            dr["bieiva"] = String.IsNullOrEmpty(ddlBieiva.SelectedValue) ? "0" : ddlBieiva.SelectedValue;
            dr["serret"] = String.IsNullOrEmpty(ddlSerret.SelectedValue) ? "0" : ddlSerret.SelectedValue;
            dr["seriva"] = String.IsNullOrEmpty(ddlSeriva.SelectedValue) ? "0" : ddlSeriva.SelectedValue;
            dr["bietri"] = String.IsNullOrEmpty(ddlBietri.SelectedValue) ? "0" : ddlBietri.SelectedValue;
            dr["sertri"] = String.IsNullOrEmpty(ddlSertri.SelectedValue) ? "0" : ddlSertri.SelectedValue;
            dr["deviva"] = String.IsNullOrEmpty(ddlDeviva.SelectedValue) ? "0" : ddlDeviva.SelectedValue;
            dr["valaut"] = fecha_final;
            dr["numpag"] = txtNumpag.Text;
            dr["plapag"] = txtPlapag.Text;
            dr["valpro"] = 0;
            dr["estval"] = "";
            dr["codban"] = "";
            dr["tipcue"] = "";
            dr["numcue"] = "";
            dr["tippro"] = "";
            dr["estrel"] = s_estrel;
            dr["codart"] = txtCodart.Text;
            dr["nomart"] = txtNomart.Text;
            dr["cedben"] = txtCedben.Text;
            dr["nomben"] = txtNomben.Text;
            dr["usuing"] = Session["gs_CodUs1"].ToString();
            dr["codusu"] = Session["gs_CodUs1"].ToString();








            dt.Rows.Add(dr);
            objProveedor.dtProveedor = dt;

            return objProveedor;
        }


    }
}