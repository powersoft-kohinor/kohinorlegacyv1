﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_cp_ProveedorClase : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "2", niv001 = "2", niv002 = "12";
        protected cls_cp_Proveedor_Clase objProveedorClase = new cls_cp_Proveedor_Clase();
        protected cls_fin_Plancuenta objPlanCuenta = new cls_fin_Plancuenta();

        int editindex = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // hide Update button on page load
                btnGuardar.Visible = false;
                f_Seg_Usuario_Menu();
                f_BindGrid_Inicial();
            }
        }

        void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {

                btnEditar.Attributes.Add("class", "btn btn-dark");
                btnAgregar.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-secondary disabled");
                btnEliminar.Attributes.Add("class", "btn btn-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");
                grvRenglonesProveedorClase.PagerStyle.CssClass = "";
            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnEditar.Attributes.Add("class", "btn btn-outline-dark");
                btnAgregar.Attributes.Add("class", "btn btn-outline-primary");
                btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");
                grvRenglonesProveedorClase.PagerStyle.CssClass = "pagination-ys justify-content-center align-items-center";
            }
        }

        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente Clase
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnAgregar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEditar.Attributes.Add("class", "btn btn-outline-secondary disabled");

                    }
                    if (row["banbus"].Equals("N")) //Abrir O Buscar
                    {
                        txtSearch.Enabled = false;
                        lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 disabled");
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                    }
                }
            }
            return dt;
        }


        private void f_BindGrid_Inicial()
        {
            objProveedorClase = objProveedorClase.f_ProveedorClase_Buscar();
            if (String.IsNullOrEmpty(objProveedorClase.Error.Mensaje))
            {
                DataTable dt = objProveedorClase.dtProveedorClase;
                Session.Add("gdt_ProveedorClase", dt);
                if (dt.Rows.Count > 0)
                {
                    grvRenglonesProveedorClase.DataSource = dt;
                    grvRenglonesProveedorClase.DataBind();
                }
                else
                {
                    // add new row when the dataset is having zero record
                    dt.Rows.Add(dt.NewRow());
                    grvRenglonesProveedorClase.DataSource = dt;
                    grvRenglonesProveedorClase.DataBind();
                    grvRenglonesProveedorClase.Rows[0].Visible = false;
                }
            }
            else
            {
                f_ErrorNuevo(objProveedorClase.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        private void f_BindGrid(string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_ProveedorClase"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "nomcla LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codcla LIKE " + "'%" + searchText.Trim() + "%'";
                    grvRenglonesProveedorClase.DataSource = dv;
                }
                grvRenglonesProveedorClase.DataBind();
            }
        }
        private DataTable GetData(string searchText = null)
        {
            DataTable dt = new DataTable();
            objPlanCuenta = objPlanCuenta.f_PlanCuenta_Buscar();
            if (String.IsNullOrEmpty(objPlanCuenta.Error.Mensaje))
            {
                dt = objPlanCuenta.dtPlancuenta;
                Session.Add("gdt_Plancuenta", dt);
                using (dt)
                {
                    DataView dv = new DataView(dt);
                    if (searchText != null)
                    {
                        if (searchText.Equals(""))
                            searchText = "";
                        dv.RowFilter = "codcta LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "nomcta LIKE " + "'%" + searchText.Trim() + "%'";


                        return dv.ToTable();
                    }


                }


                return dt;

            }
            else
            {
                f_ErrorNuevo(objProveedorClase.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            return dt;

        }
        private void f_BindGrid2()
        {
            TextBox ftxtgrvCodcta = (TextBox)(grvRenglonesProveedorClase.FooterRow.FindControl("ftxtgrvCodcta"));
            DataTable dt = this.GetData(ftxtgrvCodcta.Text); ;
            //if (ftxtgrvCodcta.Text == null)
            //{
            //    dt = this.GetData();
            //}
            //else
            //{
            //    dt = this.GetData(ftxtgrvCodcta.Text);

            //}





            Repeater rptBuscador = (Repeater)(grvRenglonesProveedorClase.FooterRow.FindControl("rptBuscador"));
            HtmlControl ddlContenedorBuscador = (HtmlControl)(grvRenglonesProveedorClase.FooterRow.FindControl("ddlContenedorBuscador"));
            HtmlControl ddlBuscador = (HtmlControl)(grvRenglonesProveedorClase.FooterRow.FindControl("ddlBuscador"));

            rptBuscador.DataSource = dt;
            rptBuscador.DataBind();

            ddlContenedorBuscador.Attributes.Add("class", "c-header-nav-item dropdown d-md-down-none mx-2 ajuste");
            ddlBuscador.Attributes.Add("class", "dropdown-menu dropdown-menu-left dropdown-menu-lg pt-0 scroll1");

        }

        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(txtSearch.Text);
        }

        protected void grvRenglonesProveedorClase_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRenglonesProveedorClase.PageIndex = e.NewPageIndex;
            f_BindGrid(txtSearch.Text);
            lblError.Text = lblExito.Text = "";
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            TextBox ftxtgrvNomcla = (TextBox)(grvRenglonesProveedorClase.FooterRow.FindControl("ftxtgrvNomcla"));
            TextBox ftxtgrvCodcla = (TextBox)(grvRenglonesProveedorClase.FooterRow.FindControl("ftxtgrvCodcla"));
            TextBox ftxtgrvCodcta = (TextBox)(grvRenglonesProveedorClase.FooterRow.FindControl("ftxtgrvCodcta"));
            TextBox ftxtgrvCtaref = (TextBox)(grvRenglonesProveedorClase.FooterRow.FindControl("ftxtgrvCtaref"));

            if (ftxtgrvNomcla.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un nombre.";
                ftxtgrvNomcla.Focus();
                return;
            }
            if (ftxtgrvCodcla.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un codigo de clase.";
                ftxtgrvCodcla.Focus();
                return;
            }
            if (ftxtgrvCodcta.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese una cuenta.";
                ftxtgrvCodcta.Focus();
                return;
            }
            if (ftxtgrvCtaref.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese una referencia.";
                ftxtgrvCtaref.Focus();
                return;
            }

            objProveedorClase.Codemp = Session["gs_CodEmp"].ToString();
            objProveedorClase.Codcla = ftxtgrvCodcla.Text.Trim();
            objProveedorClase.Nomcla = ftxtgrvNomcla.Text.Trim();
            objProveedorClase.Codcta = ftxtgrvCodcta.Text.Trim();
            objProveedorClase.Ctaref = ftxtgrvCtaref.Text.Trim();

            clsError objError = objProveedorClase.f_ProveedorClase_Actualizar(objProveedorClase, null, Session["gs_UsuIng"].ToString(), DateTime.Now.ToString(), Session["gs_CodUs1"].ToString(), DateTime.Now.ToString(), "Add");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ Guardado Exitoso.";
                f_BindGrid_Inicial();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblExito.Text = "";
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvRenglonesProveedorClase.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for edit
                    editindex = gvrow.RowIndex;

                    rowCount++;
                    if (rowCount > 1)
                    {
                        break;
                    }
                }
            }

            if (rowCount > 1)
            {
                lblError.Text = "Seleccione solo una fila para editar";
            }
            else if (rowCount == 0)
            {
                lblError.Text = "Seleccione una fila para editar";
            }
            else
            {
                ViewState["editindex"] = editindex;
                Label lblgrvCodcla = (Label)grvRenglonesProveedorClase.Rows[editindex].FindControl("lblgrvCodcla");
                Session.Add("gs_grvCodcla", lblgrvCodcla.Text.Trim());

                grvRenglonesProveedorClase.EditIndex = editindex;
                f_BindGrid_Inicial();



                // disable all row checkboxes while editing
                foreach (GridViewRow gvrow in grvRenglonesProveedorClase.Rows)
                {
                    CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                    chkRow.Enabled = false;
                }

                //hide footer row while editing
                grvRenglonesProveedorClase.FooterRow.Visible = false;

                // hide and disable respective buttons
                btnAgregar.Enabled = false;
                btnAgregar.Visible = false;
                btnEditar.Visible = false;
                btnGuardar.Visible = true;
                //btnEliminar.Enabled = false;
                //btnEliminar.Visible = false;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            // retrieving current row edit index from viewstate
            editindex = Convert.ToInt32(ViewState["editindex"].ToString());

            int Id = Convert.ToInt32(grvRenglonesProveedorClase.DataKeys[editindex].Values["codcla"].ToString());

            TextBox etxtgrvNomcla = (TextBox)grvRenglonesProveedorClase.Rows[editindex].FindControl("etxtgrvNomcla");
            TextBox etxtgrvCodcla = (TextBox)grvRenglonesProveedorClase.Rows[editindex].FindControl("etxtgrvCodcla");
            TextBox etxtgrvCodcta = (TextBox)grvRenglonesProveedorClase.Rows[editindex].FindControl("etxtgrvCodcta");
            TextBox etxtgrvCtaref = (TextBox)grvRenglonesProveedorClase.Rows[editindex].FindControl("etxtgrvCtaref");

            if (etxtgrvNomcla.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un nombre.";
                etxtgrvNomcla.Focus();
                return;
            }
            if (etxtgrvCodcla.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese un codigo de clase.";
                etxtgrvCodcla.Focus();
                return;
            }
            if (etxtgrvCodcta.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese una cuenta.";
                etxtgrvCodcta.Focus();
                return;
            }
            if (etxtgrvCtaref.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese una referencia.";
                etxtgrvCtaref.Focus();
                return;
            }

            objProveedorClase.Codemp = Session["gs_CodEmp"].ToString();
            objProveedorClase.Codcla = Session["gs_grvCodcla"].ToString();
            objProveedorClase.Nomcla = etxtgrvNomcla.Text.Trim();
            objProveedorClase.Codcta = etxtgrvCodcta.Text.Trim();
            objProveedorClase.Ctaref = etxtgrvCtaref.Text.Trim();

            clsError objError = objProveedorClase.f_ProveedorClase_Actualizar(objProveedorClase, etxtgrvCodcla.Text.Trim(), Session["gs_UsuIng"].ToString(), DateTime.Now.ToString(), Session["gs_CodUs1"].ToString(), DateTime.Now.ToString(), "Update");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ Guardado Exitoso.";
                grvRenglonesProveedorClase.EditIndex = -1;
                f_BindGrid_Inicial();
                btnAgregar.Enabled = true;
                btnAgregar.Visible = true;
                btnEditar.Visible = true;
                btnGuardar.Visible = false;
                btnEliminar.Enabled = true;
                btnEliminar.Visible = true;
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvRenglonesProveedorClase.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;
                    Label lblgrvCodcla = (Label)grvRenglonesProveedorClase.Rows[editindex].FindControl("lblgrvCodcla");
                    lblEliCli.Text += " / " + lblgrvCodcla.Text;
                    rowCount++;
                }
            }

            if (rowCount > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar una clase para eliminar.";
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            int rowCount = 0;

            clsError objError = new clsError();

            foreach (GridViewRow gvrow in grvRenglonesProveedorClase.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;

                    // retrieve Id from DataKeys to delete record
                    int Id = Convert.ToInt32(grvRenglonesProveedorClase.DataKeys[editindex].Values["codcla"].ToString());
                    Label lblgrvCodcla = (Label)grvRenglonesProveedorClase.Rows[editindex].FindControl("lblgrvCodcla");

                    objError = objProveedorClase.f_ProveedorClase_Eliminar(lblgrvCodcla.Text, Session["gs_CodEmp"].ToString(), "Delete");
                    if (!String.IsNullOrEmpty(objError.Mensaje))
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        break;
                    }

                    rowCount++;

                }
            }



            if (rowCount > 0 && String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblEliCli.Text = "";
                lblExito.Text = "✔️ Registro eliminado.";

                f_BindGrid_Inicial();
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "Selecciona una fila para eliminar";
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            grvRenglonesProveedorClase.EditIndex = -1;
            f_BindGrid_Inicial();

            btnAgregar.Enabled = true;
            btnAgregar.Visible = true;
            btnEditar.Visible = true;
            btnGuardar.Visible = false;
            btnEliminar.Enabled = true;
            btnEliminar.Visible = true;
            lblError.Text = lblExito.Text = "";

        }



        protected void btnSearch_Click(object sender, EventArgs e)
        {
            f_BindGrid2();

            HtmlControl ddlContenedorBuscador = (HtmlControl)(grvRenglonesProveedorClase.FooterRow.FindControl("ddlContenedorBuscador"));
            HtmlControl ddlBuscador = (HtmlControl)(grvRenglonesProveedorClase.FooterRow.FindControl("ddlBuscador"));
            ddlContenedorBuscador.Attributes.Add("class", "c-header-nav-item dropdown d-md-down-none mx-2 ajuste show");
            ddlBuscador.Attributes.Add("class", "dropdown-menu dropdown-menu-left dropdown-menu-lg pt-0 scroll1 show");
        }

        protected void btnBuscador_Click(object sender, EventArgs e)
        {

            HtmlControl lkbtnBuscador = (HtmlControl)grvRenglonesProveedorClase.FooterRow.FindControl("lkbtnBuscador");
            TextBox ftxtgrvCodcta = (TextBox)(grvRenglonesProveedorClase.FooterRow.FindControl("ftxtgrvCodcta"));

            lkbtnBuscador = (HtmlControl)sender;
            ftxtgrvCodcta.Text = lkbtnBuscador.Attributes["data-val"];
            f_BindGrid2();

        }
    }
}