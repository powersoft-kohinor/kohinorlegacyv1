﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_cp_ProveedorPago : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "2", niv001 = "2", niv002 = "15";
        protected cls_cp_Proveedor objProveedor = new cls_cp_Proveedor();
        protected cls_cp_Proveedor_Pago objPago = new cls_cp_Proveedor_Pago();
   
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //***i RENGLONES***//
                Session.Add("gs_BuscarNomart", "%"); //variable para buscar el articulo en btnPuntitosArticulos
                f_SetInitialRow();
                Session.Add("gs_Siguiente", ""); //para el lkbtnSiguiente Y lkbtnAtras
               
                f_Seg_Usuario_Menu();
                f_BindGrid_Inicial(); //grvProveedor



                objPago = objPago.f_CalcularSecuencia(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
                if (String.IsNullOrEmpty(objPago.Error.Mensaje))
                {
                    txtNumcco.Text = objPago.Numcco;
                    Session.Add("gs_numcpplected", txtNumcco.Text);
                    txtFecemi.Text = objPago.Fecemi;

                }
                else
                {
                    f_ErrorNuevo(objPago.Error);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }










            }

        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAbrir.Attributes.Add("class", "btn btn-dark");
                btnNuevo.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-dark");
                btnCancelar.Attributes.Add("class", "btn btn-dark");
                btnEliminar.Attributes.Add("class", "btn btn-danger");
                div_Factura.Attributes.Add("class", "card");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");

            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
                btnNuevo.Attributes.Add("class", "btn btn-outline-primary");
                div_Factura.Attributes.Add("class", "card bg-gradient-secondary");
                btnGuardar.Attributes.Add("class", "btn btn-outline-dark");
                btnCancelar.Attributes.Add("class", "btn btn-outline-dark");
                btnEliminar.Attributes.Add("class", "btn btn-outline-danger ");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");

            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=:exclamation: Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones 
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnNuevo.Disabled = true;
                    }
                    if (row["banbus"].Equals("N")) //Abrir
                    {
                        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnAbrir.Disabled = true;
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnGuardar.Disabled = true;
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnCancelar.Disabled = true;

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEliminar.Disabled = true;
                        Session["baneli"] = "N";
                    }
                }
            }
            return dt;
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_numcppSelected"] = "";
            f_limpiarCampos();
        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_numcppSelected"] = "";
            f_limpiarCampos();
        }
        protected void btnGuardarFactura_Click(object sender, EventArgs e)
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsProveedorReglonPagoExiste.Select(args);

            if (view != null) //actualiza
            {
                DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
                objPago = f_generarObjPago();
                objPago.dtPago = dtRenglones;
                clsError objError = objPago.f_Proveedor_Pago_Actualizar(objPago, "Update");
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                }
                else //error en renglones
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            else
            {
                DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
                objPago = f_generarObjPago();
                objPago.dtPago = dtRenglones;
                clsError objError = objPago.f_Proveedor_Pago_Actualizar(objPago, "Add");
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                }
                else //error en renglones
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }


        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_numfacBienSelected"] = "";
            f_limpiarCampos();
        }
        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_numfacBienSelected"].ToString()))
            {
                //Session["gs_numfacBienSelected"] = txtNumfac.Text;
                lblEliFac.Text = Session["gs_numfacBienSelected"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar una factura para eliminar.";
            }
        }
        protected void btnEliminarFactura_Click(object sender, EventArgs e)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura(); //eliminar
            clsError objError = objFactura.f_Factura_Eliminar(Session["gs_numfacBienSelected"].ToString(), Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblExito.Text = "✔️ Guardado Exitoso.";
                f_limpiarCampos();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void lkbtnPrincipio_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            //objFactura = objFactura.f_Factura_Principio(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
            //if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            //{
            //    Session["gs_numfacBienSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
            //    Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
            //    //f_llenarCamposFacturaEnc(objFactura);
            //    ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
            //    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            //    if (dtCurrentTable.Rows.Count > 0)
            //    {
            //        grvFacturaRen.DataSource = dtCurrentTable;
            //        grvFacturaRen.DataBind();
            //        f_SetPreviousData();
            //    }
            //    else
            //    {
            //        lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
            //    }
            //}
            //else
            //{
            //    f_ErrorNuevo(objFactura.Error);
            //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            //}
        }
        protected void lkbtnSiguiente_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            //objFactura = objFactura.f_Factura_Siguiente(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString(), Session["gs_Siguiente"].ToString());
            //if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            //{
            //    Session["gs_numfacBienSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
            //    Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
            //    //f_llenarCamposFacturaEnc(objFactura);
            //    ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
            //    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            //    if (dtCurrentTable.Rows.Count > 0)
            //    {
            //        grvFacturaRen.DataSource = dtCurrentTable;
            //        grvFacturaRen.DataBind();
            //        f_SetPreviousData();
            //    }
            //    else
            //    {
            //        f_SetInitialRow();
            //        f_SetPreviousData();
            //        lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
            //    }
            //}
            //else
            //{
            //    f_ErrorNuevo(objFactura.Error);
            //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            //}
        }
        protected void lkbtnFinal_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            //objFactura = objFactura.f_Factura_Final(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
            //if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            //{
            //    Session["gs_numfacBienSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
            //    Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
            //    //f_llenarCamposFacturaEnc(objFactura);
            //    ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
            //    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            //    if (dtCurrentTable.Rows.Count > 0)
            //    {
            //        grvFacturaRen.DataSource = dtCurrentTable;
            //        grvFacturaRen.DataBind();
            //        f_SetPreviousData();
            //    }
            //    else
            //    {
            //        lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
            //    }
            //}
            //else
            //{
            //    f_ErrorNuevo(objFactura.Error);
            //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            //}
        }
        protected void lkbtnAtras_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            //objFactura = objFactura.f_Factura_Atras(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString(), Session["gs_Siguiente"].ToString());
            //if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            //{
            //    Session["gs_numfacBienSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
            //    Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
            //    //f_llenarCamposFacturaEnc(objFactura);
            //    ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
            //    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            //    if (dtCurrentTable.Rows.Count > 0)
            //    {
            //        grvFacturaRen.DataSource = dtCurrentTable;
            //        grvFacturaRen.DataBind();
            //        f_SetPreviousData();
            //    }
            //    else
            //    {
            //        f_SetInitialRow();
            //        f_SetPreviousData();
            //        lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
            //    }
            //}
            //else
            //{
            //    f_ErrorNuevo(objFactura.Error);
            //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            //}
        }




        //***i CLIENTE ***//
        [WebMethod(enableSession: true)]
        public static cls_vc_Factura f_buscarCli(string s_codcli) //tab en txtClicxc
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            if (s_codcli.Equals("")) //tab en txtClicxc y esta vacio
            {
                objFactura.Clicxc = "";
                objFactura.Nomcxc = "";
                objFactura.Dircli = "";
                objFactura.Lispre = "";
                objFactura.Observ = "";
                objFactura.ClicxcModal = "";
                objFactura.Ntardes = "";

                return objFactura;
            }
            return objFactura.f_Factura_Buscar_Cliente(HttpContext.Current.Session["gs_Codemp"].ToString(), s_codcli);


        }






        protected void grvProveedor_RowDataBound(object sender, GridViewRowEventArgs e) //only fires when the GridView's data changes during the postback
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }


        protected void grvProveedor_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }


        }


        protected void grvProveedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvProveedor.SelectedRow;
            string s_codpro = fila.Cells[2].Text.Trim();

            Session["gs_codproSelected"] = s_codpro; //variable para que al seleccionar uno en el grv se obtenga el codcli (sg_codcliSELECTED ->Editar)
            objProveedor = objProveedor.f_Proveedor_Buscar(Session["gs_Codemp"].ToString(), s_codpro);
            if (String.IsNullOrEmpty(objProveedor.Error.Mensaje))
            {
                f_llenarCamposEditarProveedor(objProveedor);

            }
            else
            {
                f_ErrorNuevo(objProveedor.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

        }

        private void f_BindGrid_Inicial()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsProveedor.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();
            Session.Add("gdt_Proveedor", dt);

            grvProveedor.DataSource = dt;
            f_BindGrid();
        }

        private void f_BindGrid(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Proveedor"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "nompro LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codpro LIKE " + "'%" + searchText.Trim() + "%'";
                    grvProveedor.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirection;
                    grvProveedor.DataSource = dv;
                }
                grvProveedor.DataBind();
            }
        }

        private string SortDirection
        {
            get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }

        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(null, txtSearchProveedor.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalProveedor();", true);
        }
        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProveedor.PageIndex = e.NewPageIndex;
            f_BindGrid(null, txtSearchProveedor.Text);
        }
        protected void OnSorting(object sender, GridViewSortEventArgs e)
        {
            f_BindGrid(e.SortExpression, txtSearchProveedor.Text);
        }




        public void f_llenarCamposEditarProveedor(cls_cp_Proveedor objProveedor)
        {


            txtProcxp.Value = objProveedor.Codpro;
            txtNomcxp.Text = objProveedor.Nompro;
          
           



            string s_Codgeo = objProveedor.Codgeo;
            string s_Deviva = objProveedor.Deviva;


            


        }

        //***f GRVCLIENTE ***//


        //***i CODIGO DE RENGLONES ***//
        private void f_SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("N°", typeof(string)));
            dt.Columns.Add(new DataColumn("tipo", typeof(string)));
            dt.Columns.Add(new DataColumn("numtra", typeof(string)));
            dt.Columns.Add(new DataColumn("procxp", typeof(string)));
            dt.Columns.Add(new DataColumn("concep", typeof(string)));
            dt.Columns.Add(new DataColumn("valcob", typeof(decimal)));
            dt.Columns.Add(new DataColumn("fecemi", typeof(string)));
            dt.Columns.Add(new DataColumn("fecven", typeof(string)));
           



            dr = dt.NewRow();
            dr["N°"] = 1;
            dr["tipo"] = string.Empty;
            dr["numtra"] = string.Empty;
            dr["procxp"] = string.Empty;
            dr["concep"] = string.Empty;
            dr["valcob"] = 0.00;
            dr["fecemi"] = string.Empty;
            dr["fecven"] = string.Empty;
            

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            grvFacturaRen.DataSource = dt;
            grvFacturaRen.DataBind();
        }
        
        private void f_AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {

                        //extract the TextBox values (OJO el .Cells[2] no importa porq esta buscando el control con rowIndex)
                        TextBox tipo = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[1].FindControl("txtgrvTipo");
                        TextBox numtra = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvNumtra");
                        TextBox procxp = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvProcxp");
                        TextBox concep = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvConcep");
                        TextBox valcob = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvValcob");
                        TextBox fecemi = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvFecemi");
                        TextBox fecven = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvFecven");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;

                        //dtCurrentTable.Rows[i - 1]["tipo"] = tipo.Text;
                        dtCurrentTable.Rows[i - 1]["numtra"] = numtra.Text;
                        dtCurrentTable.Rows[i - 1]["procxp"] = procxp.Text;
                        dtCurrentTable.Rows[i - 1]["concep"] = concep.Text;
                        dtCurrentTable.Rows[i - 1]["fecemi"] = fecemi.Text;
                        dtCurrentTable.Rows[i - 1]["fecven"] = fecven.Text;

                        if (valcob.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["valcob"] = 0.00;

                      
                        }
                        else
                        {
                          
                            dtCurrentTable.Rows[i - 1]["valcob"] = String.IsNullOrEmpty(valcob.Text) ? 0 : Convert.ToDecimal(valcob.Text);
                           
                        }
                        rowIndex++;
                    }

                    drCurrentRow["valcob"] = 0.00;
                  
                    dtCurrentTable.Rows.Add(drCurrentRow);

                    ViewState["CurrentTable"] = dtCurrentTable;

                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            f_SetPreviousData();
        }
        private void f_SetPreviousData() //para actualizar el grvFacturaRen con CurrentTable (llenar los textbox del grv)
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        TextBox tipo = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[1].FindControl("txtgrvTipo");
                        TextBox numtra = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvNumtra");
                        TextBox procxp = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvProcxp");
                        TextBox concep = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvConcep");
                        TextBox valcob = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvValcob");
                        TextBox fecemi = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvFecemi");
                        TextBox fecven = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvFecven");

                        tipo.Text = dt.Rows[i]["tipo"].ToString();
                        numtra.Text = dt.Rows[i]["numtra"].ToString();
                        procxp.Text = dt.Rows[i]["procxp"].ToString();
                        concep.Text = dt.Rows[i]["concep"].ToString();
                        fecemi.Text = dt.Rows[i]["fecemi"].ToString();
                        fecven.Text = dt.Rows[i]["fecven"].ToString();

                        valcob.Text = String.Format("{0:0.00}", (Decimal)dt.Rows[i]["valcob"]);

                        
                        if (String.IsNullOrEmpty(dt.Rows[i]["N°"].ToString()))
                        {
                            dt.Rows[i]["N°"] = i + 1;
                        }




                        rowIndex++;
                    }
                }
            }
        }

        //*i BOTONES INFERIOR *//
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            f_AddNewRowToGrid();
        }
        protected void btnEliminarTodo_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];

            for (int i = 0; i < grvFacturaRen.Rows.Count; i++)
            {
                int rowID = grvFacturaRen.SelectedIndex + 1;
                if (ViewState["CurrentTable"] != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (grvFacturaRen.SelectedIndex < dt.Rows.Count - 1)
                        {
                            //Remove the Selected Row data
                            dt.Rows.RemoveAt(i);
                        }
                    }

                    //Store the current data in ViewState for future reference
                    ViewState["CurrentTable"] = dt;
                    //Re bind the GridView for the updated data
                    grvFacturaRen.DataSource = dt;
                    grvFacturaRen.DataBind();
                    i = 0;
                }
            }
            //para borrar los datos de la primera fila....sobra despues de que se ha eliminado todos los demas renglones
            dt.Rows[0]["tipo"] = "";
            dt.Rows[0]["numtra"] = "";
            dt.Rows[0]["procxp"] = "";
            dt.Rows[0]["concep"] = "";
            dt.Rows[0]["valcob"] = 0.00;
           

            //Set Previous Data on Postbacks
            f_SetPreviousData();

        }
        protected void lkgrvBorrar_Click(object sender, EventArgs e)
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values

                        TextBox tipo = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[1].FindControl("txtgrvTipo");
                        TextBox numtra = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvNumtra");
                        TextBox procxp = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvProcxp");
                        TextBox concep = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvConcep");
                        TextBox valcob = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvValcob");
                        TextBox fecemi = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvFecemi");
                        TextBox fecven = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvFecven");


                       
                        


                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["tipo"] = tipo.Text;
                        dtCurrentTable.Rows[i - 1]["numtra"] = numtra.Text;
                        dtCurrentTable.Rows[i - 1]["procxp"] = procxp.Text;
                       
                        dtCurrentTable.Rows[i - 1]["concep"] = concep.Text;
                        dtCurrentTable.Rows[i - 1]["fecemi"] = fecemi.Text;
                        dtCurrentTable.Rows[i - 1]["fecven"] = fecven.Text;

                        if (valcob.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["valcob"] = 0.00;
                            
                        }
                        else
                        {
                            dtCurrentTable.Rows[i - 1]["valcob"] = String.IsNullOrEmpty(valcob.Text) ? 0 : Convert.ToDecimal(valcob.Text);
                            
                        }




                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }

            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (!(gvRow.RowIndex == 0 && dt.Rows.Count == 1)) //no permite borrar la primera fila
                    {
                        //Remove the Selected Row data
                        dt.Rows.Remove(dt.Rows[rowID]);
                    }
                }
                else //si selecciona la primera fila
                {
                    if (gvRow.RowIndex == 0 && dt.Rows.Count == 1) //para borrar los datos de la primera fila
                    {
                      

                        dt.Rows[0]["tipo"] = "";
                        dt.Rows[0]["numtra"] = "";
                        dt.Rows[0]["procxp"] = "";
                        dt.Rows[0]["concep"] = "";
                        dt.Rows[0]["valcob"] = 0.00;
                        dt.Rows[0]["fecemi"] = "";
                        dt.Rows[0]["fecven"] = "";

                    }
                }
                //Store the current data in ViewState for future reference
                ViewState["CurrentTable"] = dt;
                //Re bind the GridView for the updated data
                grvFacturaRen.DataSource = dt;
                grvFacturaRen.DataBind();
            }
            //Set Previous Data on Postbacks
            f_SetPreviousData();
          
        }
        //*f BOTONES INFERIOR *//

        //*i SELECCION Cuentas por pagar *//
   
        
      
        protected void grvCuentaPagar_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }
        }
        
        private void f_BindGrid_InicialCuentaPagar()
        {
        }
        private void f_BindGridCuentaPagar(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_CuentaPagar"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "numtra LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "concep LIKE " + "'%" + searchText.Trim() + "%'";
                    grvCuentaPagar.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirectionCuentaPagar = this.SortDirectionCuentaPagar == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirectionCuentaPagar;
                    grvCuentaPagar.DataSource = dv;
                }
                grvCuentaPagar.DataBind();
            }
        }
        private string SortDirectionCuentaPagar
        {
            get { return ViewState["SortDirectionCuentaPagar"] != null ? ViewState["SortDirectionCuentaPagar"].ToString() : "ASC"; }
            set { ViewState["SortDirectionCuentaPagar"] = value; }
        }
        protected void f_GridBuscarCuentaPagar(object sender, EventArgs e)
        {
            f_BindGridCuentaPagar(null, txtSearchCuentaPagar.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalCuentaPagar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void OnPageIndexChangingCuentaPagar(object sender, GridViewPageEventArgs e)
        {
            grvCuentaPagar.PageIndex = e.NewPageIndex;
            f_BindGridCuentaPagar(null, txtSearchCuentaPagar.Text);
        }
        protected void OnSortingCuentaPagar(object sender, GridViewSortEventArgs e)
        {
            f_BindGridCuentaPagar(e.SortExpression, txtSearchCuentaPagar.Text);
        }

       

        //*i CALCULOS DERECHA*//
        public void f_calcularTblTotales() //tblTotales es la tabla de la derecha.
        {
            f_renglonesActuales();


            double r_tot = f_cantTotal();

            txtTotfac.Text = r_tot.ToString("N2");

        }
       
        public double f_cantTotal()//para txtTotnet ...recorre todos los datos de la columna total, sumando los num para sacar la cantidad total
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            int nrow = dt.Rows.Count;
            int i = 0;
            double r_ctotal = 0;
            try
            {
                while (i < nrow)
                {
                    if (!dt.Rows[i]["valcob"].ToString().Equals("")) //si txtgrvTotal NO esta vacio suma
                    {
                        r_ctotal = r_ctotal + double.Parse(dt.Rows[i]["valcob"].ToString());
                    }
                    i = i + 1;
                }
            }
            catch (Exception)
            {
                Response.Write("<script>alert('Todos los totales deben estar llenos.');</script>");
            }

            return r_ctotal;
        }
        public void f_renglonesActuales() //para guardar en ViewState["CurrentTable"] los renglones mostrados en pantalla
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox tipo = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[1].FindControl("txtgrvTipo");
                        TextBox numtra = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvNumtra");
                        TextBox procxp = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvProcxp");
                        TextBox concep = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvConcep");
                        TextBox valcob = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvValcob");
                        TextBox fecemi = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvFecemi");
                        TextBox fecven = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvFecven");


                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["tipo"] = tipo.Text;
                        dtCurrentTable.Rows[i - 1]["numtra"] = numtra.Text;
                        dtCurrentTable.Rows[i - 1]["procxp"] = procxp.Text;
                        dtCurrentTable.Rows[i - 1]["concep"] = concep.Text;
                        dtCurrentTable.Rows[i - 1]["fecemi"] = fecemi.Text;
                        dtCurrentTable.Rows[i - 1]["fecven"] = fecven.Text;

                        if (valcob.Text == "" )
                        {
                       
                            dtCurrentTable.Rows[i - 1]["valcob"] = 0.00;
                     
                        }
                        else
                        {
                           
                            dtCurrentTable.Rows[i - 1]["valcob"] = String.IsNullOrEmpty(valcob.Text) ? 0 : Convert.ToDecimal(valcob.Text);
                            
                        }




                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }
        }
        //*f CALCULOS DERECHA*//

        //***f CODIGO DE RENGLONES ***//


        public void f_llenarCamporCuentasPagarRen(cls_cp_Proveedor_Pago objPagoRen, int rowIndex)
        {
            GridViewRow row = grvFacturaRen.Rows[rowIndex];

            TextBox txtgrvTipo = (TextBox)row.FindControl("txtgrvTipo");
            TextBox txtgrvNumtra = (TextBox)row.FindControl("txtgrvNumtra");
            TextBox txtgrvProcxp = (TextBox)row.FindControl("txtgrvProcxp");
            TextBox txtgrvConcep = (TextBox)row.FindControl("txtgrvConcep");
            TextBox txtgrvValcob = (TextBox)row.FindControl("txtgrvValcob");
            TextBox txtgrvFecemi = (TextBox)row.FindControl("txtgrvFecemi");
            TextBox txtgrvFecven = (TextBox)row.FindControl("txtgrvFecven");


            txtgrvTipo.Text = ddlTipdoc.SelectedValue;
            txtgrvNumtra.Text = objPagoRen.Numtra;
            txtgrvProcxp.Text = objPagoRen.Procxp;
            txtgrvConcep.Text = objPagoRen.Concep;
            txtgrvValcob.Text = objPagoRen.Valcob.ToString();
            txtgrvFecemi.Text = objPagoRen.Fecemi;
            txtgrvFecven.Text = objPagoRen.Fecven;

        }

        protected void btnCuentasPorPagar_Click(object sender, EventArgs e)
        {
            

            if (String.IsNullOrEmpty(txtProcxp.Value))
            {
                lblError.Text = "*Debe seleccionar un proveedor*";
            }
            else
            {
                lblError.Text = "";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalCuentaPagar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                objPago = objPago.f_Cuentas_Pagar(Session["gs_Codemp"].ToString(), txtProcxp.Value);
                if (String.IsNullOrEmpty(objPago.Error.Mensaje))
                {

                    ViewState["CurrentTableCuentaPagar"] = objPago.dtPago;
                    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTableCuentaPagar"];
                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        grvCuentaPagar.DataSource = dtCurrentTable;
                        grvCuentaPagar.DataBind();
                       
                    }
                    else
                    {
                     
                        lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
                    }


                }
                else
                {
                    f_ErrorNuevo(objPago.Error);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }

        }
        protected void btnFormaDePago_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session.Add("g_objPago", f_generarObjPago());
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            Session.Add("gdt_ProveedorFormaPagoRen", dt);
            Session.Add("gs_Existe", "Add");

            //DataSourceSelectArguments args = new DataSourceSelectArguments();
            //DataView view = (DataView)sqldsProveedorReglonPagoExiste.Select(args);


            //DataTable dt2 = view.ToTable();
            //if (dt2 != null)
            //{
            //    if (dt2.Rows.Count > 0) //validar si RUC existe en seg_empresa
            //    {
                   
            //        Session.Add("gs_Existe", "Update");
                    
                        
            //    }
            //    else
            //    {
            //        Session.Add("gs_Existe", "Add");
            //    }
            //}

        
            if (String.IsNullOrEmpty(txtProcxp.Value)) lblError.Text = "❗ Debe seleccionar un Proveedor.";
            if (String.IsNullOrEmpty(lblError.Text)) Response.Redirect("w_cp_ProveedorFormaPago.aspx");
        }
        protected void btnGetPago_Click(object sender, EventArgs e)
        {
            int index = 0;
         
            foreach (GridViewRow gvrow in grvCuentaPagar.Rows)
            {
                
                CheckBox chk = (CheckBox)gvrow.FindControl("chkRow");
                if (chk != null & chk.Checked)
                {
                    

                    objPago = objPago.f_Cuentas_Pagar(Session["gs_Codemp"].ToString(), txtProcxp.Value);
                    if (String.IsNullOrEmpty(objPago.Error.Mensaje))
                    {

                        ViewState["CurrentTableCuentaPagar"] = objPago.dtPago;
                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTableCuentaPagar"];

                        if (dtCurrentTable.Rows.Count > 0)
                        {
                            objPago = objPago.f_dtCuentasPagarToCuentasPagar(dtCurrentTable.Rows[index]);
                            int rowIndex = gvrow.RowIndex;
                            f_llenarCamporCuentasPagarRen(objPago, rowIndex);
                            if (index < grvCuentaPagar.Rows.Count -1 )
                            {
                                f_AddNewRowToGrid();
                            }
                            
                            f_calcularTblTotales();
                            index++;
                        }
                        else
                        {

                            lblError.Text = "❗ *01. " + DateTime.Now + "No tiene renglones.";
                        }


                    }
                    else
                    {
                        f_ErrorNuevo(objPago.Error);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
            }
           
        }



        public void f_limpiarCampos()
        {
            //Image2.ImageUrl = "~/imgusu/user.png";
         


            txtNomcxp.Text = txtProcxp.Value = "";
            objPago = objPago.f_CalcularSecuencia(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
            if (String.IsNullOrEmpty(objPago.Error.Mensaje))
            {
                txtNumcco.Text = objPago.Numcco;
                txtFecemi.Text = objPago.Fecemi;
            }
            else
            {
                f_ErrorNuevo(objPago.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

            DataTable dt = (DataTable)ViewState["CurrentTable"];
            for (int i = 0; i < grvFacturaRen.Rows.Count; i++)
            {
                int rowID = grvFacturaRen.SelectedIndex + 1;
                if (ViewState["CurrentTable"] != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (grvFacturaRen.SelectedIndex < dt.Rows.Count - 1)
                        {
                            //Remove the Selected Row data
                            dt.Rows.RemoveAt(i);
                        }
                    }

                    //Store the current data in ViewState for future reference
                    ViewState["CurrentTable"] = dt;
                    //Re bind the GridView for the updated data
                    grvFacturaRen.DataSource = dt;
                    grvFacturaRen.DataBind();
                    i = 0;
                }
            }

            //para borrar los datos de la primera fila....sobra despues de que se ha eliminado todos los demas renglones
            if (grvFacturaRen.Rows.Count == 0)
            {
                f_AddNewRowToGrid();
            }
            else
            {

                dt.Rows[0]["tipo"] = "";
                dt.Rows[0]["numtra"] = "";
                dt.Rows[0]["procxp"] = "";
                dt.Rows[0]["concep"] = "";
                dt.Rows[0]["valcob"] = 0.00;
                dt.Rows[0]["fecemi"] = "";
                dt.Rows[0]["fecven"] = "";


            }
            //Set Previous Data on Postbacks
            f_SetPreviousData();
            f_calcularTblTotales();

            lblError.Text = lblError.Text = "";

        }

        protected void ddlPoriva_SelectedIndexChanged(object sender, EventArgs e)
        {
            f_calcularTblTotales();
        }

        public cls_cp_Proveedor_Pago f_generarObjPago()
        {

            string fecha_final = DateTime.Now.ToString("yyyy-MM-dd");
            objPago.Codemp = Session["gs_Codemp"].ToString();
            objPago.Numcpp = txtNumcco.Text;
            objPago.Codpro = txtProcxp.Value;
            objPago.Tipdoc = ddlTipdoc.SelectedValue;
            objPago.Numdoc = txtNumdoc.Text;
            objPago.Numcco = txtNumcco.Text;
            objPago.Tipdoc = ddlTipdoc.SelectedValue;
            objPago.Procxp = txtProcxp.Value;
            objPago.Nompro = txtNomcxp.Text;
            objPago.Concep = txtConcep.Text;
            objPago.Codusu = Session["gs_CodUs1"].ToString();
            objPago.Fectra = String.IsNullOrEmpty(txtFecemi.Text) ? DateTime.Now.ToString("yyyy-MM-dd") : txtFecemi.Text;
            objPago.Valcot = String.IsNullOrEmpty(txtValcot.Text) ? 0 : decimal.Parse(txtValcot.Text);
          
            objPago.Totcuo = String.IsNullOrEmpty(txtTotfac.Text) ? 0 : decimal.Parse(txtTotfac.Text);

            return objPago;
        }


        
    }
}