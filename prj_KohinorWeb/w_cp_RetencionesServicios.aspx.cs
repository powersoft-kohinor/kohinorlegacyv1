﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_cp_RetencionesServicios : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "2", niv001 = "2", niv002 = "1";
        protected cls_vc_Cliente objCliente = new cls_vc_Cliente();
        protected cls_cp_FacturaServicios objFactura = new cls_cp_FacturaServicios();
        protected cls_cp_FacturaServicios_Ren objFacturaRen = new cls_cp_FacturaServicios_Ren();
        protected cls_cp_Retencion objFacturaRetencion = new cls_cp_Retencion();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                f_Seg_Usuario_Menu();
                if (Session["gs_numfacServicioSelected"] == null) Response.Redirect("w_cp_FacturaServicios.aspx");
                btnConfirmar.Enabled = false;

                objFactura = (cls_cp_FacturaServicios)Session["g_objFacturaServicio"];

                objFacturaRetencion = objFacturaRetencion.f_RetencionS_Buscar(Session["gs_Codemp"].ToString(), Session["gs_numfacServicioSelected"].ToString());

                if (String.IsNullOrEmpty(objFacturaRetencion.Error.Mensaje))
                {
                    Session["gs_numfacServicioSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                    Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                    f_llenarCamposFacturaEnc(objFactura);
                    if (!String.IsNullOrEmpty(objFactura.Numfac))//si seleccionó una Factura
                    {


                        ViewState["CurrentTableGrvRetencion"] = objFacturaRetencion.dtRetencion;
                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTableGrvRetencion"];
                        if (dtCurrentTable.Rows.Count > 0)
                        {
                            grvRetencion.DataSource = dtCurrentTable;
                            grvRetencion.DataBind();
                            f_SetPreviousDataGrvRetencion();
                            f_SetInitialRowGrvRetencion(objFactura);


                        }
                        else
                        {
                            f_SetInitialRow(objFactura);
                            f_SetPreviousData();
                            lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
                        }


                    }
                    else //crear factura desde cero
                    {

                        if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
                        {
                            f_llenarCamposFacturaEnc(objFactura);
                        }
                        else
                        {
                            f_ErrorNuevo(objFactura.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                }
                else
                {
                    f_ErrorNuevo(objFactura.Error);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
                //***f RENGLONES***//




                txtFalta.Text = "0.00";
                //f_renglonesActuales();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAbrir.Attributes.Add("class", "btn btn-dark");
                btnNuevo.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-secondary disabled");

                div_FormaPago.Attributes.Add("class", "card");

                btnEliminar.Attributes.Add("class", "btn btn-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");
                grvRetencion.PagerStyle.CssClass = "";
            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
                btnNuevo.Attributes.Add("class", "btn btn-outline-primary");
                div_FormaPago.Attributes.Add("class", "card bg-gradient-secondary");
                btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");
                grvRetencion.PagerStyle.CssClass = "pagination-ys justify-content-center align-items-center";
            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnNuevo.Disabled = true;
                    }
                    if (row["banbus"].Equals("N")) //Abrir
                    {
                        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnAbrir.Disabled = true;
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnGuardar.Disabled = true;
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnCancelar.Disabled = true;

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEliminar.Disabled = true;
                        Session["baneli"] = "N";
                    }
                }
            }
            return dt;
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_numfacServicioSelected"] = "";
            f_limpiarCampos();
        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_cp_FacturaBienes.aspx");
        }
        protected void btnGuardarFactura_Click(object sender, EventArgs e)
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsFacturaExiste.Select(args);
            if (view != null) //ya existe la factura (Actualizar)
            {
                objFactura = (cls_cp_FacturaServicios)Session["g_objFacturaServicio"];
                clsError objError = objFactura.f_Factura_Actualizar(objFactura, "Update");
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    //ACTUALIZA RENGLON
                    DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
                    objFacturaRen.dtFacturaRen = dtRenglones;
                    objError = objFacturaRen.f_Factura_Ren_Actualizar(objFacturaRen, "Update");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                    }
                    else //error en renglones
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            else //no existe la factura (Insertar)
            {
                objFactura = (cls_cp_FacturaServicios)Session["g_objFacturaServicio"];
                clsError objError = objFactura.f_Factura_Actualizar(objFactura, "Add");
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    //INSERTA RENGLON
                    DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
                    objFacturaRen.dtFacturaRen = dtRenglones;
                    objError = objFacturaRen.f_Factura_Ren_Actualizar(objFacturaRen, "Add");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                    }
                    else //error en renglones
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_numfacServicioSelected"] = "";
            f_limpiarCampos();
        }
        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_numfacServicioSelected"].ToString()))
            {
                Session["gs_numfacServicioSelected"] = txtNumfac.Text;
                lblEliFac.Text = Session["gs_numfacServicioSelected"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar una factura para eliminar.";
            }
        }
        protected void btnEliminarFactura_Click(object sender, EventArgs e)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura(); //eliminar
            clsError objError = objFactura.f_Factura_Eliminar(Session["gs_numfacServicioSelected"].ToString(), Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                f_limpiarCampos();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        public void f_llenarCamposFacturaEnc(cls_cp_FacturaServicios objFactura)
        {

            txtNumfac.Text = objFactura.Numfac;
            txtFecfac.Text = objFactura.Fecfac;
            txtCodcom.Text = objFactura.Codcom;
            txtNumdoc.Text = objFactura.Numdoc;
            txtTotiv0.Text = objFactura.Totiv0.ToString();
            txtTotbas.Text = objFactura.Totbas.ToString();
            txtTotiva.Text = objFactura.Totiva.ToString();
            txtTotfac.Text = objFactura.Totfac.ToString();


            string s_estado = objFactura.Estado;

            decimal d_poriva = objFactura.Poriva;
            string s_poriva = String.Format("{0:0.00}", d_poriva);







            if (String.IsNullOrEmpty(s_estado))
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue("C").Value;
            else
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue(s_estado).Value;












            if (String.IsNullOrEmpty(s_poriva))
                ddlPoriva.SelectedValue = ddlPoriva.Items.FindByValue("12.00").Value;
            else
                ddlPoriva.SelectedValue = ddlPoriva.Items.FindByValue(s_poriva).Value;






            string s_valaut = "";
            string s_feccom = "";


            if (String.IsNullOrEmpty(objFactura.Valaut) || String.IsNullOrEmpty(objFactura.Feccom))
            {
                s_valaut = DateTime.Now.ToString("yyyy-MM-dd");
                s_feccom = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                s_valaut = objFactura.Feccom.ToString();
                s_feccom = objFactura.Valaut.ToString();

            }




            string s_fecfac = objFactura.Fecfac;
            try
            {
                DateTime d_date = DateTime.Parse(s_fecfac);
                string s_Fecfinal = d_date.ToString("yyyy-MM-dd");
                txtFecfac.Text = s_Fecfinal;
            }
            catch (Exception ex) //si tiene fecnac en NULL
            {
                txtFecfac.Text = DateTime.Now.Date.ToString();
                throw;
            }

            //txtFecfac.Text = String.IsNullOrEmpty(objFactura.Fecfac.ToString()) ? DateTime.Now.Date.ToString() : objFactura.Fecfac.ToString("yyyy-MM-dd");
        }
        public void f_limpiarCampos()
        {
            lblError.Text = lblError.Text = "";
        }


        //NUEVOS METODOS...DE PRJ PUNTOVENTA ---------------------------------------------------------------------------------
        private void f_SetPreviousDataGrvRetencion() //para actualizar el grvFacturaRen con CurrentTable (llenar los textbox del grv)
        {
            int rowIndex = 0;
            if (ViewState["CurrentTableGrvRetencion"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTableGrvRetencion"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {


                        DropDownList codret = (DropDownList)grvRetencion.Rows[rowIndex].Cells[1].FindControl("ddlgrvCodret");
                        TextBox basret = (TextBox)grvRetencion.Rows[rowIndex].Cells[2].FindControl("txtgrvBasret");
                        TextBox porret = (TextBox)grvRetencion.Rows[rowIndex].Cells[3].FindControl("txtgrvPorret");
                        TextBox totret = (TextBox)grvRetencion.Rows[rowIndex].Cells[4].FindControl("txtgrvTotret");

                        codret.SelectedValue = codret.Items.FindByValue(dt.Rows[i]["codret"].ToString()).Value;


                        basret.Text = dt.Rows[i]["basret"].ToString();
                        porret.Text = dt.Rows[i]["porret"].ToString();
                        totret.Text = String.IsNullOrEmpty(dt.Rows[i]["totret"].ToString()) ? "0.00" : String.Format("{0:0.00}", (Decimal)dt.Rows[i]["totret"]);



                        if (String.IsNullOrEmpty(dt.Rows[i]["N°"].ToString()))
                        {
                            dt.Rows[i]["N°"] = i + 1;
                        }




                        rowIndex++;
                    }
                }



            }
        }
        private void f_SetInitialRowGrvRetencion(cls_cp_FacturaServicios objFactura)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("N°", typeof(string)));
            dt.Columns.Add(new DataColumn("Retencion", typeof(string)));
            dt.Columns.Add(new DataColumn("Base", typeof(string)));
            dt.Columns.Add(new DataColumn("%", typeof(string)));
            dt.Columns.Add(new DataColumn("Valor", typeof(string)));


            int rowIndex = 0;
            if (ViewState["CurrentTableGrvRetencion"] != null)
            {
                DataTable dt2 = (DataTable)ViewState["CurrentTableGrvRetencion"];
                if (dt2.Rows.Count > 0)
                {
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {



                        DropDownList codret = (DropDownList)grvRetencion.Rows[rowIndex].Cells[1].FindControl("ddlgrvCodret");
                        TextBox basret = (TextBox)grvRetencion.Rows[rowIndex].Cells[2].FindControl("txtgrvBasret");
                        TextBox porret = (TextBox)grvRetencion.Rows[rowIndex].Cells[3].FindControl("txtgrvPorret");
                        TextBox totret = (TextBox)grvRetencion.Rows[rowIndex].Cells[4].FindControl("txtgrvTotret");

                        codret.SelectedValue = codret.Items.FindByValue(dt2.Rows[i]["codret"].ToString()).Value;

                        dr = dt.NewRow();

                        dr["Retencion"] = codret.SelectedValue;
                        dr["Base"] = basret.Text;
                        dr["%"] = porret.Text;
                        dr["Valor"] = totret.Text;











                        dt2.Rows[i]["N°"] = i + 1;
                        dr["N°"] = dt2.Rows[i]["N°"];


                        dt.Rows.Add(dr);


                        rowIndex++;
                    }
                }



            }











            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

        }
        private void f_SetInitialRow(cls_cp_FacturaServicios objFactura)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("N°", typeof(string)));
            dt.Columns.Add(new DataColumn("Retencion", typeof(string)));
            dt.Columns.Add(new DataColumn("Base", typeof(string)));
            dt.Columns.Add(new DataColumn("%", typeof(string)));
            dt.Columns.Add(new DataColumn("Valor", typeof(string)));

            dr = dt.NewRow();
            dr["N°"] = 1;
            dr["Retencion"] = string.Empty;
            dr["Base"] = objFactura.Totfac.ToString();
            dr["%"] = string.Empty;
            dr["Valor"] = "";

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;
            grvRetencion.DataSource = dt;
            grvRetencion.DataBind();

            //para llenar el Banco de la primera fila al recien entrar a forma de pago
            int rowIndex = 0;
            //TextBox txtgrvCodban = (TextBox)grvFormaPago.Rows[rowIndex].Cells[2].FindControl("txtgrvCodban");
            //txtgrvCodban.Text = "EFECTIVO";
            TextBox txtgrvBasret = (TextBox)grvRetencion.Rows[rowIndex].Cells[2].FindControl("txtgrvBasret");
            txtgrvBasret.Text = objFactura.Totfac.ToString();
            f_calcularFalta();
        }
        private void f_AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    decimal r_valfpa = 0, r_valfpaAux = 0;
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values (OJO el .Cells[2] no importa porq esta buscando el control con rowIndex)
                        DropDownList codret = (DropDownList)grvRetencion.Rows[rowIndex].Cells[1].FindControl("ddlgrvCodret");
                        TextBox basret = (TextBox)grvRetencion.Rows[rowIndex].Cells[2].FindControl("txtgrvBasret");
                        TextBox porret = (TextBox)grvRetencion.Rows[rowIndex].Cells[3].FindControl("txtgrvPorret");
                        TextBox totret = (TextBox)grvRetencion.Rows[rowIndex].Cells[4].FindControl("txtgrvTotret");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;
                        //drCurrentRow["Banco"] = "EFECTIVO";

                        r_valfpaAux = String.IsNullOrEmpty(basret.Text) ? 0 : decimal.Parse(basret.Text);
                        r_valfpa = r_valfpa + r_valfpaAux;
                        drCurrentRow["Base"] = (decimal.Parse(txtTotfac.Text) - r_valfpa).ToString("N2");


                        dtCurrentTable.Rows[i - 1]["Retencion"] = codret.Text;
                        dtCurrentTable.Rows[i - 1]["Base"] = String.IsNullOrEmpty(basret.Text) ? "0" : basret.Text; ;
                        dtCurrentTable.Rows[i - 1]["%"] = porret.Text;
                        dtCurrentTable.Rows[i - 1]["Valor"] = String.IsNullOrEmpty(totret.Text) ? "0" : totret.Text;


                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);

                    ViewState["CurrentTable"] = dtCurrentTable;

                    grvRetencion.DataSource = dtCurrentTable;
                    grvRetencion.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            f_SetPreviousData();
        }
        private void f_SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList codret = (DropDownList)grvRetencion.Rows[rowIndex].Cells[1].FindControl("ddlgrvCodret");
                        TextBox basret = (TextBox)grvRetencion.Rows[rowIndex].Cells[2].FindControl("txtgrvBasret");
                        TextBox porret = (TextBox)grvRetencion.Rows[rowIndex].Cells[3].FindControl("txtgrvPorret");
                        TextBox totret = (TextBox)grvRetencion.Rows[rowIndex].Cells[4].FindControl("txtgrvTotret");

                        codret.Text = dt.Rows[i]["Retencion"].ToString();
                        basret.Text = dt.Rows[i]["Base"].ToString();
                        porret.Text = dt.Rows[i]["%"].ToString();
                        totret.Text = dt.Rows[i]["Valor"].ToString();


                        rowIndex++;
                    }
                }
            }
            f_calcularFalta();
        }

        protected void ddlgrvCodret_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            GridViewRow gvRow = (GridViewRow)ddl.NamingContainer;
            int rowIndex = gvRow.RowIndex; //para saber la fila seleccionada
            GridViewRow row = grvRetencion.Rows[rowIndex];
            DropDownList ddlgrvCodret = (DropDownList)row.FindControl("ddlgrvCodret");
            TextBox txtgrvPorret = (TextBox)row.FindControl("txtgrvPorret");
            string s_codret = ddlgrvCodret.SelectedValue.ToString().Trim();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
            conn.Open();
            string s_checktar = "SELECT [codret], [nomret], [porret] FROM [fin_retencion] where [codret] = @s_codret"; //OJO codfor se debe filtrar con length
            //cfg_formapago
            //cfg_banco
            SqlCommand com = new SqlCommand(s_checktar, conn);
            com.Parameters.AddWithValue("@s_codret", s_codret);
            if (com.ExecuteScalar() != null)
                txtgrvPorret.Text = com.ExecuteScalar().ToString();
            else
                txtgrvPorret.Text = "";
            conn.Close();



            TextBox txtgrvBasret = (TextBox)row.FindControl("txtgrvBasret");
            txtgrvPorret = (TextBox)row.FindControl("txtgrvPorret");
            TextBox txtgrvTotret = (TextBox)row.FindControl("txtgrvTotret");

            double r_total = (double)(Convert.ToDecimal(txtgrvBasret.Text) * Convert.ToDecimal(txtgrvPorret.Text));
            txtgrvTotret.Text = r_total.ToString();

            f_renglonesActuales();



        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            f_AddNewRowToGrid();
        }

        //PARA PODER BORRAR UN RENGLON
        protected void lkgrvBorrar_Click(object sender, EventArgs e)
        {
            //antes de borrar se actualizar el CurrentTable con grvRenglones
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {




                        //extract the TextBox values
                        DropDownList codret = (DropDownList)grvRetencion.Rows[rowIndex].Cells[1].FindControl("ddlgrvCodret");
                        TextBox basret = (TextBox)grvRetencion.Rows[rowIndex].Cells[2].FindControl("txtgrvBasret");
                        TextBox porret = (TextBox)grvRetencion.Rows[rowIndex].Cells[3].FindControl("txtgrvPorret");
                        TextBox totret = (TextBox)grvRetencion.Rows[rowIndex].Cells[4].FindControl("txtgrvTotret");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["Retencion"] = codret.Text;
                        dtCurrentTable.Rows[i - 1]["Base"] = basret.Text;
                        dtCurrentTable.Rows[i - 1]["%"] = porret.Text;
                        dtCurrentTable.Rows[i - 1]["Valor"] = totret.Text;


                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }

            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1) //si selecciona una fila que NO sea la primera
                {
                    if (!(gvRow.RowIndex == 0 && dt.Rows.Count == 1)) //no permite borrar la primera fila
                    {
                        //Remove the Selected Row data
                        dt.Rows.Remove(dt.Rows[rowID]);
                    }
                }
                else //si selecciona la primera fila
                {
                    if (gvRow.RowIndex == 0 && dt.Rows.Count == 1) //para borrar los datos de la primera fila
                    {
                        dt.Rows[0]["Retencion"] = "";
                        dt.Rows[0]["Base"] = "";
                        dt.Rows[0]["%"] = "";
                        dt.Rows[0]["Valor"] = "";
                    }
                }

                //Store the current data in ViewState for future reference
                ViewState["CurrentTable"] = dt;
                //Re bind the GridView for the updated data
                grvRetencion.DataSource = dt;
                grvRetencion.DataBind();
            }
            //Set Previous Data on Postbacks
            f_SetPreviousData();
            f_calcularFalta(); //para recalcular txtValfpa en caso de borrar una fila con valor
        }

        public void f_renglonesActuales() //para guardar en ViewState["CurrentTable"] los renglones mostrados en pantalla
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {

                        DropDownList codret = (DropDownList)grvRetencion.Rows[rowIndex].Cells[1].FindControl("ddlgrvCodret");
                        TextBox basret = (TextBox)grvRetencion.Rows[rowIndex].Cells[2].FindControl("txtgrvBasret");
                        TextBox porret = (TextBox)grvRetencion.Rows[rowIndex].Cells[3].FindControl("txtgrvPorret");
                        TextBox totret = (TextBox)grvRetencion.Rows[rowIndex].Cells[4].FindControl("txtgrvTotret");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["Retencion"] = codret.Text;
                        dtCurrentTable.Rows[i - 1]["Base"] = basret.Text;
                        dtCurrentTable.Rows[i - 1]["%"] = porret.Text;
                        dtCurrentTable.Rows[i - 1]["Valor"] = totret.Text;


                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }
        }

        public void f_calcularFalta()
        {
            double r_valFpa = 0;
            double r_valTotal = 0;

            int rowIndex = 0;
            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            if (dtCurrentTable.Rows.Count > 0)
            {
                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                {
                    TextBox basret = (TextBox)grvRetencion.Rows[rowIndex].Cells[2].FindControl("txtgrvBasret");
                    if (String.IsNullOrEmpty(basret.Text))
                    {
                        basret.Text = "0.00";
                    }
                    r_valFpa = r_valFpa + double.Parse(basret.Text);
                    rowIndex++;
                }
            }

            try
            {
                //r_valFpa = double.Parse(txtgrvValfpa.Text);
                r_valTotal = double.Parse(txtTotfac.Text) - r_valFpa;
                if (r_valTotal <= 0)
                    btnConfirmar.Enabled = true;
                else
                    btnConfirmar.Enabled = false;
            }
            catch (Exception ex)
            {
                lblError.Text = "❗ *01. " + DateTime.Now + ex.Message;
            }
            //txtTotrec.Text = r_valFpa.ToString("N2");
            //txtCambio.Text = r_valTotal < 0 ? (r_valTotal * (-1)).ToString("N2") : "0.00";
            txtFalta.Text = r_valTotal.ToString("N2");
        }


        protected void txtgrvValfpa_TextChanged(object sender, EventArgs e)
        {
            TextBox tx = (TextBox)sender;
            GridViewRow gvRow = (GridViewRow)tx.NamingContainer;
            TextBox txtgrvBasret = (TextBox)gvRow.FindControl("txtgrvBasret");

            if (String.IsNullOrEmpty(txtgrvBasret.Text))
            {
                txtgrvBasret.Text = "0.00";
            }

            f_calcularFalta();
            f_renglonesActuales();


        }

        protected void btnEliminarRetencion_Click(object sender, EventArgs e)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura(); //eliminar
            clsError objError = objFactura.f_Factura_Eliminar(Session["gs_numfacSelected"].ToString(), Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                f_limpiarCampos();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }


        protected void btnConfirmar_Click(object sender, EventArgs e)
        {




            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsFacturaExiste.Select(args);
            DataTable dt = view.ToTable();
            if (dt != null)
            {
                if (dt.Rows.Count > 0) //actualiza
                {
                    objFactura = (cls_cp_FacturaServicios)Session["g_objFacturaServicio"];
                    //objFacturaRen = (cls_cp_FacturaBienes_Ren)Session["gdt_FacturaBienRen"];
                    DataTable dtRenglonesRetencion = (DataTable)ViewState["CurrentTable"];
                    objFacturaRetencion = f_generarObjRentencion();

                    objFacturaRetencion.dtRetencion = dtRenglonesRetencion;
                    clsError objError = objFacturaRetencion.f_RetencionS_Actualizar(objFacturaRetencion, "Add");


                }
                else
                {
                    //objFactura = f_generarObjFactura();
                    clsError objError = objFactura.f_Factura_Actualizar(objFactura, "Add");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        //INSERTA RENGLON

                    }
                    else
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }


            }
        }


        public cls_cp_Retencion f_generarObjRentencion()
        {


            objFacturaRetencion.Codemp = Session["gs_Codemp"].ToString();
            objFacturaRetencion.Numfac = txtNumfac.Text;
            objFacturaRetencion.Codret = "01";
            objFacturaRetencion.Basret = 01;
            objFacturaRetencion.Porret = 01;
            objFacturaRetencion.Totret = 01;
            objFacturaRetencion.Debhab = "";
            objFacturaRetencion.Usuing = Session["gs_CodUs1"].ToString();
            objFacturaRetencion.Fecing = String.IsNullOrEmpty(txtFecfac.Text) ? DateTime.Now.ToString("yyyy-MM-dd") : txtFecfac.Text;
            objFacturaRetencion.Codusu = Session["gs_CodUs1"].ToString();



            return objFacturaRetencion;
        }
    }
}