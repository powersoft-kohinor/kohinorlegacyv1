﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_seg_Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    f_Seg_Usuario_Menu();
                }
                catch (Exception)
                {
                    lblError.Text = "❗ Error al cargar menú de navegación. Volver a Login.";
                }
                
                try
                {
                    if (Request.UrlReferrer != null)
                        Session["gs_PagPre"] = Request.UrlReferrer.ToString(); //PARA OBTENER PAGINA PREVIA
                    string s_errorDesign = "errorMessage= " + Request.QueryString["errorMessage"] + "<br/>" +
                        " fileName= " + Request.QueryString["fileName"] + "<br/>" +
                        " lineNumber= " + Request.QueryString["lineNumber"] + "<br/>" +
                        " columnNumber = " + Request.QueryString["columnNumber"] + "<br/>" +
                        " methodName = " + Request.QueryString["methodName"] + "<br/>" +
                        " fecha = " + Request.QueryString["fecha"]
                        ;
                    lblError.Text = s_errorDesign;
                    //if (Session["gs_Error"]==null)
                    //{
                    //    string s_errorDesign = Request.QueryString["errorMessage"];
                    //    lblError.Text = s_errorDesign;  
                    //    //lblError.Text = "Unknown Error. Check Session State in Global";
                    //}
                    //else
                    //{
                    //    lblError.Text = Session["gs_Error"].ToString();
                    //}
                }
                catch (Exception ex)
                {
                    lblError.Text = "❗ Error al leer excepción. " + ex.Message;
                }
            }
        }

        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                //if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente
                //{
                //    if (row["banins"].Equals("N")) //Nuevo
                //    {
                //        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnNuevo.Disabled = true;
                //    }
                //    if (row["banbus"].Equals("N")) //Abrir
                //    {
                //        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnAbrir.Disabled = true;
                //    }
                //    if (row["bangra"].Equals("N")) //Guardar
                //    {
                //        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnGuardar.Disabled = true;
                //    }
                //    if (row["bancan"].Equals("N")) //Cancelar
                //    {
                //        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnCancelar.Disabled = true;

                //    }
                //    if (row["baneli"].Equals("N")) //Eliminiar
                //    {
                //        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnEliminar.Disabled = true;
                //        Session["baneli"] = "N";
                //    }
                //}
            }
            return dt;
        }
    }
}