﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_seg_Error404.aspx.cs" Inherits="prj_KohinorWeb.w_seg_Error404" %>

<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v3.0.0
* @link https://coreui.io
* Copyright (c) 2020 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->
<html lang="en">
<head runat="server">
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Kohinor Web - Enterprise Resource Planning Software">
    <meta name="author" content="David Abad | Kevin Díaz">
    <meta name="keyword" content="ERP,Bootstrap,Admin,Template,jQuery,CSS,HTML,Dashboard">
    <title>404 - Kohinor Web</title>
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <%--  <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">--%>
    <!-- Main styles for this application-->
    <link href="CoreUI/css/style.css" rel="stylesheet" />
    <link href="CoreUI/vendors/@coreui/icons/css/free.min.css" rel="stylesheet" />
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <%--<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        // Shared ID
        gtag('config', 'UA-118965717-3');
        // Bootstrap ID
        gtag('config', 'UA-118965717-5');
    </script>--%>
</head>
<body>
    <form id="form1" runat="server">
        <div class="c-app flex-row align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="clearfix">
                            <h1 class="float-left display-3 mr-4">404</h1>
                            <h4 class="pt-3">¡Uy! Te has perdido.</h4>
                            <p class="text-muted">No se ha encontrado la página que busca.</p>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <%--<asp:LinkButton ID="lkbtnVolver" runat="server" OnClick="lkbtnVolver_Click"><i class="cil-arrow-circle-left"></i> <b>Volver</b></asp:LinkButton>--%>
                                <a href='javascript:history.go(-1)'><i class="cil-arrow-circle-left"></i> Volver a la página anterior</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- CoreUI and necessary plugins-->
    <script src="CoreUI/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
    <!--[if IE]><!-->
    <script src="CoreUI/vendors/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
</body>
</html>
