﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_seg_Error500 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (Request.UrlReferrer != null)
                        Session["gs_PagPre"] = Request.UrlReferrer.ToString(); //PARA OBTENER PAGINA PREVIA
                    string s_errorDesign = "errorMessage= " + Request.QueryString["errorMessage"] + "<br/>" +
                        " fileName= " + Request.QueryString["fileName"] + "<br/>" +
                        " lineNumber= " + Request.QueryString["lineNumber"] + "<br/>" +
                        " columnNumber = " + Request.QueryString["columnNumber"] + "<br/>" +
                        " methodName = " + Request.QueryString["methodName"] + "<br/>" +
                        " fecha = " + Request.QueryString["fecha"]
                        ;
                    lblError.Text = s_errorDesign;
                    //if (Session["gs_Error"]==null)
                    //{
                    //    string s_errorDesign = Request.QueryString["errorMessage"];
                    //    lblError.Text = s_errorDesign;  
                    //    //lblError.Text = "Unknown Error. Check Session State in Global";
                    //}
                    //else
                    //{
                    //    lblError.Text = Session["gs_Error"].ToString();
                    //}
                }
                catch (Exception ex)
                {
                    lblError.Text = "❗ Error al leer excepción. " + ex.Message;
                }
            }
        }
    }
}