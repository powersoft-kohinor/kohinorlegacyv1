﻿<%@ Page Title="Módulo Menú" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_seg_ModuloMenu.aspx.cs" Inherits="prj_KohinorWeb.w_seg_ModuloMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function f_EliminarNiv001() {
            $("#btnShowWarningNiv001").click();
        }
        function f_EliminarNiv002() {
            $("#btnShowWarningNiv002").click();
        }
    </script>
</asp:Content>
<asp:Content ID="ContentSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item active">Modulos - Menú</li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <asp:UpdatePanel ID="updError" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">

    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>MODULOS</b></span>
                                    </div>
                                    <input id="txtSearch" type="text" class="form-control" placeholder="Buscar..." runat="server">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" id="btnSearch" runat="server" onserverclick="f_GridBuscarModulo">
                                            <i class="cil-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="btn-group float-right" role="group" aria-label="Basic example">
                                    <asp:LinkButton class="btn btn-outline-primary" ID="btnAgregar" runat="server" OnClick="btnAgregar_Click"><span class="cil-file"></span> Agregar</asp:LinkButton>
                                    <asp:LinkButton class="btn btn-outline-dark" ID="btnEditar" runat="server" OnClick="btnEditar_Click"><span class="cil-folder-open"></span> Editar</asp:LinkButton>
                                    <asp:LinkButton class="btn btn-outline-dark" ID="btnGuardar" runat="server" OnClick="btnGuardar_Click"><span class="cil-save"></span> Guardar</asp:LinkButton>
                                    <asp:LinkButton class="btn btn-outline-dark" ID="btnCancelar" runat="server" OnClick="btnCancelar_Click"><span class="cil-grid-slash"></span> Cancelar</asp:LinkButton>
                                    <asp:LinkButton class="btn btn-outline-danger" ID="btnEliminar" runat="server" OnClick="btnDelete_Click"><span class="cil-trash"></span> Eliminar</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <asp:UpdatePanel ID="updModulos" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="grvModulos" runat="server" ShowHeaderWhenEmpty="True" AutoGenerateColumns="false" class="table table-bordered table-hover table-striped"
                                            ShowFooter="true" AllowPaging="true" PageSize="5" OnPageIndexChanging="grvModulo_PageIndexChanging" DataKeyNames="codmod" OnSelectedIndexChanged="grvModulo_SelectedIndexChanged">

                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRow" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:CommandField ButtonType="Image" SelectImageUrl="~/Icon/Menu125/Select-Hand.png" ShowSelectButton="True">
                                                    <ControlStyle Width="30px" />
                                                </asp:CommandField>

                                                <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Codmod" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblgrvCodmod" runat="server" Text='<%#Eval("codmod")%>'> </asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label ID="elblgrvCodmod" runat="server" Text='<%#Eval("codmod")%>'> </asp:Label>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="ftxtgrvCodmod" class="form-control form-control-sm" runat="server" TextMode="Number"> </asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Nommod" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblgrvNommod" runat="server" Text='<%#Eval("nommod")%>'> </asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="etxtgrvNommod" class="form-control form-control-sm" runat="server" Text='<%#Eval("nommod")%>'> </asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="ftxtgrvNommod" class="form-control form-control-sm" runat="server"> </asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Nomext" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblgrvNomext" runat="server" Text='<%#Eval("nomext")%>'> </asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="etxtgrvNomext" runat="server" class="form-control form-control-sm" Text='<%#Eval("nomext")%>'> </asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="ftxtgrvNomext" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Imgmod" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgImgmod" runat="server" ImageUrl='<%#Eval("imgmod")%>' Width="80px" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Image ID="eimgImgmod" runat="server" ImageUrl='<%#Eval("imgmod")%>' Width="80px" />
                                                        <asp:FileUpload ID="efuImgmod" runat="server" />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:FileUpload ID="ffuImgmod" runat="server" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Imgdes" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgImgdes" runat="server" ImageUrl='<%#Eval("imgdes")%>' Width="80px" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Image ID="eimgImgdes" runat="server" ImageUrl='<%#Eval("imgdes")%>' Width="80px" />
                                                        <asp:FileUpload ID="efuImgdes" runat="server" />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:FileUpload ID="ffuImgdes" runat="server" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Refmod" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblgrvRefmod" runat="server" Text='<%#Eval("refmod")%>'> </asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="etxtgrvRefmod" runat="server" class="form-control form-control-sm" Text='<%#Eval("refmod")%>'> </asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="ftxtgrvRefmod" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <FooterStyle BackColor="#c0c0c0" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-xl-6">
                        <div class="fade-in">
                            <div class="card card-accent-primary bg-light">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b>Menú Nivel 1</b></span>
                                                </div>
                                                <input id="txtSearchNiv001" type="text" class="form-control" placeholder="Buscar..." runat="server">
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-primary" type="button" id="btnSearchNiv001" runat="server" onserverclick="f_GridBuscarNiv001">
                                                        <i class="cil-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="btn-group float-right" role="group" aria-label="Basic example">
                                                <asp:LinkButton class="btn btn-outline-primary" ID="btnAgregarNiv001" runat="server" OnClick="btnAgregarNiv001_Click"><span class="cil-file"></span> Agregar</asp:LinkButton>
                                                <asp:LinkButton class="btn btn-outline-dark" ID="btnEditarNiv001" runat="server" OnClick="btnEditarNiv001_Click"><span class="cil-folder-open"></span> Editar</asp:LinkButton>
                                                <asp:LinkButton class="btn btn-outline-dark" ID="btnGuardarNiv001" runat="server" OnClick="btnGuardarNiv001_Click"><span class="cil-save"></span> Guardar</asp:LinkButton>
                                                <asp:LinkButton class="btn btn-outline-dark" ID="btnCancelarNiv001" runat="server" OnClick="btnCancelarNiv001_Click"><span class="cil-grid-slash"></span> Cancelar</asp:LinkButton>
                                                <asp:LinkButton class="btn btn-outline-danger" ID="btnEliminarNiv001" runat="server" OnClick="btnDeleteNiv001_Click"><span class="cil-trash"></span> Eliminar</asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <asp:UpdatePanel ID="updNiv001" runat="server">
                                        <ContentTemplate>
                                            <div class="row" style="overflow-x: auto;">

                                                <asp:GridView ID="grvNiv001" runat="server" ShowHeaderWhenEmpty="True" AutoGenerateColumns="false" class="table table-bordered table-hover table-striped"
                                                    ShowFooter="true" AllowPaging="true" PageSize="5" OnPageIndexChanging="grvNiv001_PageIndexChanging" DataKeyNames="niv001" OnSelectedIndexChanged="grvNiv001_SelectedIndexChanged">

                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkRow" runat="server"></asp:CheckBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:CommandField ButtonType="Image" SelectImageUrl="~/Icon/Menu125/Select-Hand.png" ShowSelectButton="True">
                                                            <ControlStyle Width="30px" />
                                                        </asp:CommandField>

                                                        <asp:TemplateField HeaderText="Codmod" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ItemStyle-Width="1%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvCodmod" runat="server" Text='<%#Eval("codmod")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:Label ID="elblgrvCodmod" runat="server" Text='<%#Eval("codmod")%>'> </asp:Label>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList ID="fddlgrvCodmod" class="form-control form-control-sm" runat="server" DataSourceID="sqldsCodmodNiv001" DataTextField="codmod" DataValueField="codmod"></asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Niv001" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ItemStyle-Width="1%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvNiv001" runat="server" Text='<%#Eval("niv001")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:Label ID="elblgrvNiv001" runat="server" Text='<%#Eval("niv001")%>'> </asp:Label>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="ftxtgrvNiv001" class="form-control form-control-sm" runat="server" TextMode="Number"> </asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Nommen" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvNommen" runat="server" Text='<%#Eval("nommen")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="etxtgrvNommen" runat="server" class="form-control form-control-sm" Text='<%#Eval("nommen")%>'> </asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="ftxtgrvNommen" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Nom001" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvNom001" runat="server" Text='<%#Eval("nom001")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="etxtgrvNom001" runat="server" class="form-control form-control-sm" Text='<%#Eval("nom001")%>'> </asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="ftxtgrvNom001" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Ext001" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvExt001" runat="server" Text='<%#Eval("ext001")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="etxtgrvExt001" runat="server" class="form-control form-control-sm" Text='<%#Eval("ext001")%>'> </asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="ftxtgrvExt001" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Img001" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrvImg001" runat="server" Text='<%#Eval("img001")%>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="etxtgrvImg001" runat="server" class="form-control form-control-sm" Text='<%#Eval("img001")%>'> </asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="ftxtgrvImg001" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                    <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                                    <FooterStyle BackColor="#c0c0c0" />
                                                </asp:GridView>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xl-6">
                        <div class="card card-accent-dark bg-light">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Menú Nivel 2</b></span>
                                            </div>
                                            <input id="txtSearchNiv002" type="text" class="form-control" placeholder="Buscar..." runat="server">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-primary" type="button" id="btnSearchNiv002" runat="server" onserverclick="f_GridBuscarNiv002">
                                                    <i class="cil-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="btn-group float-right" role="group" aria-label="Basic example">
                                            <asp:LinkButton class="btn btn-outline-primary" ID="btnAgregarNiv002" runat="server" OnClick="btnAgregarNiv002_Click"><span class="cil-file"></span> Agregar</asp:LinkButton>
                                            <asp:LinkButton class="btn btn-outline-dark" ID="btnEditarNiv002" runat="server" OnClick="btnEditarNiv002_Click"><span class="cil-folder-open"></span> Editar</asp:LinkButton>
                                            <asp:LinkButton class="btn btn-outline-dark" ID="btnGuardarNiv002" runat="server" OnClick="btnGuardarNiv002_Click"><span class="cil-save"></span> Guardar</asp:LinkButton>
                                            <asp:LinkButton class="btn btn-outline-dark" ID="btnCancelarNiv002" runat="server" OnClick="btnCancelarNiv002_Click"><span class="cil-grid-slash"></span> Cancelar</asp:LinkButton>
                                            <asp:LinkButton class="btn btn-outline-danger" ID="btnEliminarNiv002" runat="server" OnClick="btnDeleteNiv002_Click"><span class="cil-trash"></span> Eliminar</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <asp:UpdatePanel ID="updNiv002" runat="server">
                                    <ContentTemplate>
                                        <div class="row" style="overflow-x: auto;">
                                            <asp:GridView ID="grvNiv002" runat="server" ShowHeaderWhenEmpty="True" AutoGenerateColumns="false" class="table table-bordered table-hover table-striped"
                                                ShowFooter="true" AllowPaging="true" PageSize="5" OnPageIndexChanging="grvNiv002_PageIndexChanging" DataKeyNames="niv002">

                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRow" runat="server"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Codmod" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgrvCodmod" runat="server" Text='<%#Eval("codmod")%>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="elblgrvCodmod" runat="server" Text='<%#Eval("codmod")%>'> </asp:Label>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="fddlgrvCodmod" class="form-control form-control-sm" runat="server" DataSourceID="sqldsCodmodNiv001" DataTextField="codmod" DataValueField="codmod"></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Niv001" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgrvNiv001" runat="server" Text='<%#Eval("niv001")%>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="elblgrvNiv001" runat="server" Text='<%#Eval("niv001")%>'> </asp:Label>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="fddlgrvNiv001" class="form-control form-control-sm" runat="server" DataSourceID="sqldsCodmodNiv002" DataTextField="niv001" DataValueField="niv001"></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Niv002" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgrvNiv002" runat="server" Text='<%#Eval("niv002")%>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="elblgrvNiv002" runat="server" Text='<%#Eval("niv002")%>'> </asp:Label>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="ftxtgrvNiv002" class="form-control form-control-sm" runat="server" TextMode="Number"> </asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Nommen" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgrvNommen" runat="server" Text='<%#Eval("nommen")%>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="etxtgrvNommen" runat="server" class="form-control form-control-sm" Text='<%#Eval("nommen")%>'> </asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="ftxtgrvNommen" runat="server" class="form-control form-control-sm" Text='<%#Eval("nommen")%>'> </asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Nom001" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgrvNom001" runat="server" Text='<%#Eval("nom001")%>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="etxtgrvNom001" runat="server" class="form-control form-control-sm" Text='<%#Eval("nom001")%>'> </asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="ftxtgrvNom001" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Nom002" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgrvNom002" runat="server" Text='<%#Eval("nom002")%>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="etxtgrvNom002" runat="server" class="form-control form-control-sm" Text='<%#Eval("nom002")%>'> </asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="ftxtgrvNom002" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Ext002" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgrvExt002" runat="server" Text='<%#Eval("ext002")%>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="etxtgrvExt002" runat="server" class="form-control form-control-sm" Text='<%#Eval("ext002")%>'> </asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="ftxtgrvExt002" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Ref002" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgrvRef002" runat="server" Text='<%#Eval("ref002")%>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="etxtgrvRef002" runat="server" class="form-control form-control-sm" Text='<%#Eval("ref002")%>'> </asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="ftxtgrvRef002" runat="server" class="form-control form-control-sm"> </asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                                <FooterStyle BackColor="#c0c0c0" />
                                            </asp:GridView>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>

                <small>* No usar el filtro de busqueda al "Editar" una fila.</small>

            </div>
        </div>
    </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsModulo" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_USUARIO_MODULO" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsCodmodNiv001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codmod], [nommod] FROM [seg_modulo]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodmodNiv002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codmod], [niv001], [nommen], [nom001] FROM [seg_modulo_niv001] WHERE ([codmod] = @codmod)">
        <SelectParameters>
            <asp:SessionParameter Name="codmod" SessionField="gs_codmodSelected" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <!--f SQL DATASOURCES-->


    <!-- Modal Eliminar Modulo-->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Modulo</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar el Modulo '<asp:Label ID="lblEliCodMod" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                    <asp:Image ID="imgEliImgmod" runat="server" Width="50px" />
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminar_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Eliminar Niv001-->
    <button id="btnShowWarningNiv001" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModalNiv001">Eliminar Niv001 modal</button>
    <div class="modal fade" id="warningModalNiv001" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Niv001</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    Modulo:
                    <asp:Label ID="lblModuloNiv001" runat="server" Font-Bold="True"></asp:Label>
                    <p>¿Desea eliminar el Niv001 '<asp:Label ID="lblEliNiv001" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarNiv001_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Eliminar Niv002-->
    <button id="btnShowWarningNiv002" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModalNiv002">Eliminar Niv002 modal</button>
    <div class="modal fade" id="warningModalNiv002" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Niv002</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    Modulo:
                    <asp:Label ID="lblModuloNiv002" runat="server" Font-Bold="True"></asp:Label><br />
                    Niv001:
                    <asp:Label ID="lblNiv001Niv002" runat="server" Font-Bold="True"></asp:Label>
                    <p>¿Desea eliminar el Niv002 '<asp:Label ID="lblEliNiv002" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarNiv002_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>


    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>


</asp:Content>
