﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_seg_ModuloMenu : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "10", niv001 = "2", niv002 = "11";
        protected cls_seg_Modulo objModulo = new cls_seg_Modulo();
        protected cls_seg_Modulo_Niv001 objNiv001 = new cls_seg_Modulo_Niv001();
        protected cls_seg_Modulo_Niv002 objNiv002 = new cls_seg_Modulo_Niv002();
        int editindex = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // hide Update button on page load
                btnGuardar.Visible = false;
                btnGuardarNiv001.Visible = false;
                btnGuardarNiv002.Visible = false;
                Session.Add("gs_codmodSelected", ""); //para niv001 y 2
                Session.Add("gs_niv001Selected", ""); //para niv001 y 2
                Session.Add("gs_nommenSelected", ""); //para niv002
                Session.Add("gs_nom001Selected", ""); //para niv002

                f_Seg_Usuario_Menu();
                f_BindGridModulo_Inicial();
                f_BindGridNiv001_Inicial();
                f_BindGridNiv002_Inicial();

            }
        }

        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                //if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente
                //{
                //    if (row["banins"].Equals("N")) //Nuevo
                //    {
                //        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnNuevo.Disabled = true;
                //    }
                //    if (row["banbus"].Equals("N")) //Abrir
                //    {
                //        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnAbrir.Disabled = true;
                //    }
                //    if (row["bangra"].Equals("N")) //Guardar
                //    {
                //        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnGuardar.Disabled = true;
                //    }
                //    if (row["bancan"].Equals("N")) //Cancelar
                //    {
                //        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnCancelar.Disabled = true;

                //    }
                //    if (row["baneli"].Equals("N")) //Eliminiar
                //    {
                //        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnEliminar.Disabled = true;
                //        Session["baneli"] = "N";
                //    }
                //}
            }
            return dt;
        }

        //***i MODULOS ***//
        private void f_BindGridModulo_Inicial()
        {
            objModulo = objModulo.f_Modulo_Buscar();
            if (String.IsNullOrEmpty(objModulo.Error.Mensaje))
            {
                DataTable dt = objModulo.dtModulo;
                Session.Add("gdt_Modulo", dt);
                if (dt.Rows.Count > 0)
                {
                    grvModulos.DataSource = dt;
                    grvModulos.DataBind();                    
                }
                else
                {
                    // add new row when the dataset is having zero record
                    dt.Rows.Add(dt.NewRow());
                    grvModulos.DataSource = dt;
                    grvModulos.DataBind();
                    grvModulos.Rows[0].Visible = false;
                }
                TextBox ftxtgrvCodmod = (TextBox)(grvModulos.FooterRow.FindControl("ftxtgrvCodmod"));
                ftxtgrvCodmod.Text = (dt.Rows.Count + 1).ToString();
            }
            else
            {
                f_ErrorNuevo(objModulo.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        private void f_BindGridModulo(string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Modulo"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    //dv.RowFilter = "codmod LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "nommod LIKE " + "'%" + searchText.Trim() + "%'";
                    dv.RowFilter = "nommod LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + string.Format("CONVERT({0}, System.String) like '%{1}%'", "codmod", searchText.Trim());
                    grvModulos.DataSource = dv;
                }
                grvModulos.DataBind();
                TextBox ftxtgrvCodmod = (TextBox)(grvModulos.FooterRow.FindControl("ftxtgrvCodmod"));
                ftxtgrvCodmod.Text = (dt.Rows.Count + 1).ToString();
            }
        }
        protected void f_GridBuscarModulo(object sender, EventArgs e)
        {
            f_BindGridModulo(txtSearch.Value);
        }
        protected void grvModulo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvModulos.PageIndex = e.NewPageIndex;
            f_BindGridModulo(txtSearch.Value);
            lblError.Text = lblExito.Text = "";
        }
        protected void grvModulo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label lblgrvCodmod = (Label)grvModulos.SelectedRow.FindControl("lblgrvCodmod");
            string s_codmod = lblgrvCodmod.Text.Trim();
            Session["gs_codmodSelected"] = s_codmod;
            Session["gs_niv001Selected"] = "";
            f_BindGridNiv001_Inicial();
            f_BindGridNiv002_Inicial();

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            TextBox ftxtgrvCodmod = (TextBox)(grvModulos.FooterRow.FindControl("ftxtgrvCodmod"));
            TextBox ftxtgrvNommod = (TextBox)(grvModulos.FooterRow.FindControl("ftxtgrvNommod"));
            TextBox ftxtgrvNomext = (TextBox)(grvModulos.FooterRow.FindControl("ftxtgrvNomext"));
            FileUpload ffuImgmod = (FileUpload)(grvModulos.FooterRow.FindControl("ffuImgmod"));
            FileUpload ffuImgdes = (FileUpload)(grvModulos.FooterRow.FindControl("ffuImgdes"));
            TextBox ftxtgrvRefmod = (TextBox)(grvModulos.FooterRow.FindControl("ftxtgrvRefmod"));

            if (ftxtgrvCodmod.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Codmod.";
                ftxtgrvCodmod.Focus();
                return;
            }
            if (ftxtgrvNommod.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nommod.";
                ftxtgrvNommod.Focus();
                return;
            }
            if (ftxtgrvNomext.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nomext.";
                ftxtgrvNomext.Focus();
                return;
            }
            string s_pathImgmod = "../Icon/Menu125/";
            if (!ffuImgmod.HasFile)
            {
                lblError.Text = "❗ Seleccionar Imgmod.";
                return;
            }
            else
            {
                try
                {
                    ffuImgmod.SaveAs(Server.MapPath("~/Icon/Menu125/") + Path.GetFileName(ffuImgmod.FileName));
                    s_pathImgmod = s_pathImgmod + Path.GetFileName(ffuImgmod.FileName);
                }
                catch (Exception ex)
                {
                    clsError objErrorimgMod = new clsError();
                    objErrorimgMod.f_ErrorControlado(ex);
                    f_ErrorNuevo(objErrorimgMod);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }                
            }

            string s_pathImgdes = "../Icon/Menu125/";
            if (!ffuImgdes.HasFile)
            {
                lblError.Text = "❗ Seleccionar Imgdes.";
                return;
            }
            else
            {
                try
                {
                    ffuImgdes.SaveAs(Server.MapPath("~/Icon/Menu125/") + Path.GetFileName(ffuImgdes.FileName));
                    s_pathImgdes = s_pathImgdes + Path.GetFileName(ffuImgdes.FileName);
                }
                catch (Exception ex)
                {
                    clsError objErrorimgMod = new clsError();
                    objErrorimgMod.f_ErrorControlado(ex);
                    f_ErrorNuevo(objErrorimgMod);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }

            if (ftxtgrvRefmod.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Refmod.";
                ftxtgrvRefmod.Focus();
                return;
            }

            objModulo.Codmod = int.Parse(ftxtgrvCodmod.Text.Trim());
            objModulo.Nommod = ftxtgrvNommod.Text.Trim();
            objModulo.Nomext = ftxtgrvNomext.Text.Trim();
            objModulo.Imgmod = s_pathImgmod.Trim();
            objModulo.Imgdes = s_pathImgdes.Trim();
            objModulo.Refmod = ftxtgrvRefmod.Text.Trim();

            clsError objError = objModulo.f_Modulo_Actualizar(objModulo, "Add");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ " + DateTime.Now + " Guardado Exitoso.";
                f_BindGridModulo_Inicial();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void btnEditar_Click(object sender, EventArgs e)
        {            
            lblError.Text = "";
            lblExito.Text = "";
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvModulos.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for edit
                    editindex = gvrow.RowIndex;

                    rowCount++;
                    if (rowCount > 1)
                    {
                        break;
                    }
                }
            }

            if (rowCount > 1)
            {
                lblError.Text = "Seleccione solo una fila para editar.";
            }
            else if (rowCount == 0)
            {
                lblError.Text = "Seleccione una fila para editar.";
            }
            else
            {
                ViewState["editindex"] = editindex;

                grvModulos.EditIndex = editindex;
                f_BindGridModulo_Inicial();

                // disable all row checkboxes while editing
                foreach (GridViewRow gvrow in grvModulos.Rows)
                {
                    CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                    chkRow.Enabled = false;
                }

                //hide footer row while editing
                grvModulos.FooterRow.Visible = false;

                // hide and disable respective buttons
                f_DisableBtnNiv001();
                f_DisableBtnNiv002();
                btnAgregar.Enabled = false;
                btnAgregar.Visible = false;
                btnEditar.Visible = false;
                btnGuardar.Visible = true;
                btnEliminar.Enabled = false;
                btnEliminar.Visible = false;
            }
        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            // retrieving current row edit index from viewstate
            editindex = Convert.ToInt32(ViewState["editindex"].ToString());

            int Id = Convert.ToInt32(grvModulos.DataKeys[editindex].Values["codmod"].ToString());

            Label elblgrvCodmod = (Label)grvModulos.Rows[editindex].FindControl("elblgrvCodmod");
            TextBox etxtgrvNommod = (TextBox)(grvModulos.Rows[editindex].FindControl("etxtgrvNommod"));
            TextBox etxtgrvNomext = (TextBox)(grvModulos.Rows[editindex].FindControl("etxtgrvNomext"));
            Image eimgImgmod = (Image)grvModulos.Rows[editindex].FindControl("eimgImgmod");
            FileUpload efuImgmod = (FileUpload)(grvModulos.Rows[editindex].FindControl("efuImgmod"));
            Image eimgImgdes = (Image)grvModulos.Rows[editindex].FindControl("eimgImgdes");
            FileUpload efuImgdes = (FileUpload)(grvModulos.Rows[editindex].FindControl("efuImgdes"));
            TextBox etxtgrvRefmod = (TextBox)(grvModulos.Rows[editindex].FindControl("etxtgrvRefmod"));

            if (etxtgrvNommod.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nommod.";
                etxtgrvNommod.Focus();
                return;
            }
            if (etxtgrvNomext.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nomext.";
                etxtgrvNomext.Focus();
                return;
            }
            string s_pathImgmod = "../Icon/Menu125/";
            if (!efuImgmod.HasFile)
            {
                s_pathImgmod = eimgImgmod.ImageUrl;
            }
            else
            {
                try
                {
                    efuImgmod.SaveAs(Server.MapPath("~/Icon/Menu125/") + Path.GetFileName(efuImgmod.FileName));
                    s_pathImgmod = s_pathImgmod + Path.GetFileName(efuImgmod.FileName);
                }
                catch (Exception ex)
                {
                    clsError objErrorimgMod = new clsError();
                    objErrorimgMod.f_ErrorControlado(ex);
                    f_ErrorNuevo(objErrorimgMod);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }

            string s_pathImgdes = "../Icon/Menu125/";
            if (!efuImgdes.HasFile)
            {
                s_pathImgdes = eimgImgdes.ImageUrl;
            }
            else
            {
                try
                {
                    efuImgdes.SaveAs(Server.MapPath("~/Icon/Menu125/") + Path.GetFileName(efuImgdes.FileName));
                    s_pathImgdes = s_pathImgdes + Path.GetFileName(efuImgdes.FileName);
                }
                catch (Exception ex)
                {
                    clsError objErrorimgMod = new clsError();
                    objErrorimgMod.f_ErrorControlado(ex);
                    f_ErrorNuevo(objErrorimgMod);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }

            if (etxtgrvRefmod.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Refmod.";
                etxtgrvRefmod.Focus();
                return;
            }

            objModulo.Codmod = int.Parse(elblgrvCodmod.Text.Trim());
            objModulo.Nommod = etxtgrvNommod.Text.Trim();
            objModulo.Nomext = etxtgrvNomext.Text.Trim();
            objModulo.Imgmod = s_pathImgmod.Trim();
            objModulo.Imgdes = s_pathImgdes.Trim();
            objModulo.Refmod = etxtgrvRefmod.Text.Trim();

            clsError objError = objModulo.f_Modulo_Actualizar(objModulo, "Update");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ " + DateTime.Now + " Guardado Exitoso.";
                grvModulos.EditIndex = -1;
                f_BindGridModulo_Inicial();
                btnAgregar.Enabled = true;
                btnAgregar.Visible = true;
                btnEditar.Visible = true;
                btnGuardar.Visible = false;
                btnEliminar.Enabled = true;
                btnEliminar.Visible = true;
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            f_EnableBtnNiv001();
            f_EnableBtnNiv002();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvModulos.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;
                    Label lblgrvCodmod = (Label)grvModulos.Rows[editindex].FindControl("lblgrvCodmod");
                    Label lblgrvNommod = (Label)grvModulos.Rows[editindex].FindControl("lblgrvNommod");
                    Image imgImgmod = (Image)grvModulos.Rows[editindex].FindControl("imgImgmod");
                    lblEliCodMod.Text += " / " + lblgrvCodmod.Text + " - " + lblgrvNommod.Text + " / ";
                    imgEliImgmod.ImageUrl = imgImgmod.ImageUrl;
                    rowCount++;
                }
            }

            if (rowCount > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar un Modulo para eliminar.";
            }
        }
        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            int rowCount = 0;

            clsError objError = new clsError();

            foreach (GridViewRow gvrow in grvModulos.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;

                    // retrieve Id from DataKeys to delete record
                    int Id = Convert.ToInt32(grvModulos.DataKeys[editindex].Values["codmod"].ToString());
                    Label lblgrvCodmod = (Label)grvModulos.Rows[editindex].FindControl("lblgrvCodmod");

                    objError = objModulo.f_Modulo_Eliminar(lblgrvCodmod.Text, "Delete");
                    if (!String.IsNullOrEmpty(objError.Mensaje))
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        break;
                    }

                    rowCount++;
                }
            }

            if (rowCount > 0 && String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblEliCodMod.Text = "";
                lblExito.Text = "✔️ Registro eliminado.";

                f_BindGridModulo_Inicial();
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "Seleccione una fila para eliminar.";
            }
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            grvModulos.EditIndex = -1;
            f_BindGridModulo_Inicial();

            f_EnableBtnNiv001();
            btnAgregar.Enabled = true;
            btnAgregar.Visible = true;
            btnEditar.Visible = true;
            btnGuardar.Visible = false;
            btnEliminar.Enabled = true;
            btnEliminar.Visible = true;
            lblError.Text = lblExito.Text = "";

        }
        protected void f_DisableBtnModulo()
        {
            btnAgregar.Enabled = btnEditar.Enabled = btnGuardar.Enabled = btnCancelar.Enabled = btnEliminar.Enabled = false;
            btnAgregar.Visible = btnEditar.Visible = btnGuardar.Visible = btnCancelar.Visible = btnEliminar.Visible = false;
        }
        protected void f_EnableBtnModulo()
        {
            btnAgregar.Enabled = btnEditar.Enabled = btnGuardar.Enabled = btnCancelar.Enabled = btnEliminar.Enabled = true;
            btnAgregar.Visible = btnEditar.Visible = btnCancelar.Visible = btnEliminar.Visible = true;
        }

        //***f MODULOS ***//

        //***i NIV001 ***//
        private void f_BindGridNiv001_Inicial()
        {
            objNiv001 = objNiv001.f_Niv001_Buscar(Session["gs_codmodSelected"].ToString());
            if (String.IsNullOrEmpty(objNiv001.Error.Mensaje))
            {
                DataTable dt = objNiv001.dtModuloNiv001;
                Session.Add("gdt_Niv001", dt);
                if (dt.Rows.Count > 0)
                {
                    grvNiv001.DataSource = dt;
                    grvNiv001.DataBind();
                }
                else
                {
                    // add new row when the dataset is having zero record
                    dt.Rows.Add(dt.NewRow());
                    grvNiv001.DataSource = dt;
                    grvNiv001.DataBind();
                    grvNiv001.Rows[0].Visible = false;
                }
                if (!Session["gs_codmodSelected"].ToString().Equals(""))
                {
                    DropDownList fddlgrvCodmod = (DropDownList)(grvNiv001.FooterRow.FindControl("fddlgrvCodmod"));
                    fddlgrvCodmod.SelectedValue = fddlgrvCodmod.Items.FindByValue(Session["gs_codmodSelected"].ToString()).Value;
                    TextBox ftxtgrvNiv001 = (TextBox)(grvNiv001.FooterRow.FindControl("ftxtgrvNiv001"));
                    ftxtgrvNiv001.Text = (dt.Rows.Count + 1).ToString();                    
                }                
            }
            else
            {
                f_ErrorNuevo(objModulo.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        private void f_BindGridNiv001(string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Niv001"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    //dv.RowFilter = "codmod LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "nommod LIKE " + "'%" + searchText.Trim() + "%'";
                    dv.RowFilter = "nom001 LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + string.Format("CONVERT({0}, System.String) like '%{1}%'", "niv001", searchText.Trim());
                    grvNiv001.DataSource = dv;
                }
                grvNiv001.DataBind();
                DropDownList fddlgrvCodmod = (DropDownList)(grvNiv001.FooterRow.FindControl("fddlgrvCodmod"));
                fddlgrvCodmod.SelectedValue = fddlgrvCodmod.Items.FindByValue(Session["gs_codmodSelected"].ToString()).Value;
                TextBox ftxtgrvNiv001 = (TextBox)(grvNiv001.FooterRow.FindControl("ftxtgrvNiv001"));
                ftxtgrvNiv001.Text = (dt.Rows.Count + 1).ToString();
            }
        }
        protected void f_GridBuscarNiv001(object sender, EventArgs e)
        {
            f_BindGridNiv001(txtSearchNiv001.Value);
        }
        protected void grvNiv001_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvNiv001.PageIndex = e.NewPageIndex;
            f_BindGridNiv001(txtSearchNiv001.Value);
            lblError.Text = lblExito.Text = "";
        }
        protected void grvNiv001_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label lblgrvCodmod = (Label)grvNiv001.SelectedRow.FindControl("lblgrvCodmod");
            Label lblgrvNiv001 = (Label)grvNiv001.SelectedRow.FindControl("lblgrvNiv001");
            Label lblgrvNommen = (Label)grvNiv001.SelectedRow.FindControl("lblgrvNommen");
            Label lblgrvNom001 = (Label)grvNiv001.SelectedRow.FindControl("lblgrvNom001");
            Session["gs_codmodSelected"] = lblgrvCodmod.Text.Trim();
            Session["gs_niv001Selected"] = lblgrvNiv001.Text.Trim();
            Session["gs_nommenSelected"] = lblgrvNommen.Text.Trim();
            Session["gs_nom001Selected"] = lblgrvNom001.Text.Trim();

            f_BindGridNiv002_Inicial();

        }

        protected void btnAgregarNiv001_Click(object sender, EventArgs e)
        {
            DropDownList fddlgrvCodmod = (DropDownList)(grvNiv001.FooterRow.FindControl("fddlgrvCodmod"));
            TextBox ftxtgrvNiv001 = (TextBox)(grvNiv001.FooterRow.FindControl("ftxtgrvNiv001"));
            TextBox ftxtgrvNommen = (TextBox)(grvNiv001.FooterRow.FindControl("ftxtgrvNommen"));
            TextBox ftxtgrvNom001 = (TextBox)(grvNiv001.FooterRow.FindControl("ftxtgrvNom001"));
            TextBox ftxtgrvExt001 = (TextBox)(grvNiv001.FooterRow.FindControl("ftxtgrvExt001"));
            TextBox ftxtgrvImg001 = (TextBox)(grvNiv001.FooterRow.FindControl("ftxtgrvImg001"));

            if (ftxtgrvNiv001.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Niv001.";
                ftxtgrvNiv001.Focus();
                return;
            }
            if (ftxtgrvNommen.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nommen.";
                ftxtgrvNommen.Focus();
                return;
            }
            if (ftxtgrvNom001.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nom001.";
                ftxtgrvNom001.Focus();
                return;
            }     
            if (ftxtgrvExt001.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Ext001.";
                ftxtgrvExt001.Focus();
                return;
            }
            if (ftxtgrvImg001.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Img001.";
                ftxtgrvImg001.Focus();
                return;
            }

            objNiv001.Codmod = int.Parse(fddlgrvCodmod.SelectedValue.Trim());
            objNiv001.Niv001 = int.Parse(ftxtgrvNiv001.Text.Trim());
            objNiv001.Nommen = ftxtgrvNommen.Text.Trim();
            objNiv001.Nom001 = ftxtgrvNom001.Text.Trim();
            objNiv001.Ext001 = ftxtgrvExt001.Text.Trim();
            objNiv001.Img001 = ftxtgrvImg001.Text.Trim();

            clsError objError = objNiv001.f_Niv001_Actualizar(objNiv001, "Add");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ " + DateTime.Now + " Guardado Exitoso.";
                f_BindGridNiv001_Inicial();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void btnEditarNiv001_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblExito.Text = "";
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvNiv001.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for edit
                    editindex = gvrow.RowIndex;

                    rowCount++;
                    if (rowCount > 1)
                    {
                        break;
                    }
                }
            }

            if (rowCount > 1)
            {
                lblError.Text = "Seleccione solo una fila para editar.";
            }
            else if (rowCount == 0)
            {
                lblError.Text = "Seleccione una fila para editar.";
            }
            else
            {
                ViewState["editindex"] = editindex;

                grvNiv001.EditIndex = editindex;
                f_BindGridNiv001_Inicial();

                // disable all row checkboxes while editing
                foreach (GridViewRow gvrow in grvNiv001.Rows)
                {
                    CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                    chkRow.Enabled = false;
                }

                //hide footer row while editing
                grvNiv001.FooterRow.Visible = false;

                // hide and disable respective buttons
                f_DisableBtnModulo();
                f_DisableBtnNiv002();
                btnAgregarNiv001.Enabled = false;
                btnAgregarNiv001.Visible = false;
                btnEditarNiv001.Visible = false;
                btnGuardarNiv001.Visible = true;
                btnEliminarNiv001.Enabled = false;
                btnEliminarNiv001.Visible = false;
            }
        }
        protected void btnGuardarNiv001_Click(object sender, EventArgs e)
        {
            // retrieving current row edit index from viewstate
            editindex = Convert.ToInt32(ViewState["editindex"].ToString());

            int Id = Convert.ToInt32(grvNiv001.DataKeys[editindex].Values["niv001"].ToString());

            Label elblgrvCodmod = (Label)(grvNiv001.Rows[editindex].FindControl("elblgrvCodmod"));
            Label elblgrvNiv001 = (Label)(grvNiv001.Rows[editindex].FindControl("elblgrvNiv001"));
            TextBox etxtgrvNommen = (TextBox)(grvNiv001.Rows[editindex].FindControl("etxtgrvNommen"));
            TextBox etxtgrvNom001 = (TextBox)(grvNiv001.Rows[editindex].FindControl("etxtgrvNom001"));
            TextBox etxtgrvExt001 = (TextBox)(grvNiv001.Rows[editindex].FindControl("etxtgrvExt001"));
            TextBox etxtgrvImg001 = (TextBox)(grvNiv001.Rows[editindex].FindControl("etxtgrvImg001"));

            if (etxtgrvNommen.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nommen.";
                etxtgrvNommen.Focus();
                return;
            }
            if (etxtgrvNom001.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nom001.";
                etxtgrvNom001.Focus();
                return;
            }
            if (etxtgrvExt001.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Ext001.";
                etxtgrvExt001.Focus();
                return;
            }
            if (etxtgrvImg001.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Img001.";
                etxtgrvImg001.Focus();
                return;
            }

            objNiv001.Codmod = int.Parse(elblgrvCodmod.Text.Trim());
            objNiv001.Niv001 = int.Parse(elblgrvNiv001.Text.Trim());
            objNiv001.Nommen = etxtgrvNommen.Text.Trim();
            objNiv001.Nom001 = etxtgrvNom001.Text.Trim();
            objNiv001.Ext001 = etxtgrvExt001.Text.Trim();
            objNiv001.Img001 = etxtgrvImg001.Text.Trim();

            clsError objError = objNiv001.f_Niv001_Actualizar(objNiv001, "Update");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ " + DateTime.Now + " Guardado Exitoso.";
                grvNiv001.EditIndex = -1;
                f_BindGridNiv001_Inicial();
                btnAgregarNiv001.Enabled = true;
                btnAgregarNiv001.Visible = true;
                btnEditarNiv001.Visible = true;
                btnGuardarNiv001.Visible = false;
                btnEliminarNiv001.Enabled = true;
                btnEliminarNiv001.Visible = true;
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            f_EnableBtnModulo();
            f_EnableBtnNiv002();
        }
        protected void btnDeleteNiv001_Click(object sender, EventArgs e)
        {
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvNiv001.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;
                    Label lblgrvCodmod = (Label)grvNiv001.Rows[editindex].FindControl("lblgrvCodmod");
                    Label lblgrvNiv001 = (Label)grvNiv001.Rows[editindex].FindControl("lblgrvNiv001");
                    Label lblgrvNom001 = (Label)grvNiv001.Rows[editindex].FindControl("lblgrvNom001");
                    lblModuloNiv001.Text = lblgrvCodmod.Text;
                    lblEliNiv001.Text += " / " + lblgrvNiv001.Text + " - " + lblgrvNom001.Text + " / ";
                    rowCount++;
                }
            }

            if (rowCount > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_EliminarNiv001();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar un Nivel para eliminar.";
            }
        }
        protected void btnEliminarNiv001_Click(object sender, EventArgs e)
        {
            int rowCount = 0;

            clsError objError = new clsError();

            foreach (GridViewRow gvrow in grvNiv001.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;

                    // retrieve Id from DataKeys to delete record
                    int Id = Convert.ToInt32(grvNiv001.DataKeys[editindex].Values["niv001"].ToString());
                    Label lblgrvCodmod = (Label)grvNiv001.Rows[editindex].FindControl("lblgrvCodmod");
                    Label lblgrvNiv001 = (Label)grvNiv001.Rows[editindex].FindControl("lblgrvNiv001");

                    objError = objNiv001.f_Niv001_Eliminar(lblgrvCodmod.Text, lblgrvNiv001.Text, "Delete");
                    if (!String.IsNullOrEmpty(objError.Mensaje))
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        break;
                    }

                    rowCount++;
                }
            }

            if (rowCount > 0 && String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblEliNiv001.Text = "";
                lblExito.Text = "✔️ Registro eliminado.";

                f_BindGridNiv001_Inicial();
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "Seleccione una fila para eliminar.";
            }
        }
        protected void btnCancelarNiv001_Click(object sender, EventArgs e)
        {
            grvNiv001.EditIndex = -1;
            f_BindGridNiv001_Inicial();

            f_EnableBtnModulo();
            btnAgregarNiv001.Enabled = true;
            btnAgregarNiv001.Visible = true;
            btnEditarNiv001.Visible = true;
            btnGuardarNiv001.Visible = false;
            btnEliminarNiv001.Enabled = true;
            btnEliminarNiv001.Visible = true;
            lblError.Text = lblExito.Text = "";

        }
        protected void f_DisableBtnNiv001()
        {
            btnAgregarNiv001.Enabled = btnEditarNiv001.Enabled = btnGuardarNiv001.Enabled = btnCancelarNiv001.Enabled = btnEliminarNiv001.Enabled = false;
            btnAgregarNiv001.Visible = btnEditarNiv001.Visible = btnGuardarNiv001.Visible = btnCancelarNiv001.Visible = btnEliminarNiv001.Visible = false;
        }
        protected void f_EnableBtnNiv001()
        {
            btnAgregarNiv001.Enabled = btnEditarNiv001.Enabled = btnGuardarNiv001.Enabled = btnCancelarNiv001.Enabled = btnEliminarNiv001.Enabled = true;
            btnAgregarNiv001.Visible = btnEditarNiv001.Visible = btnCancelarNiv001.Visible = btnEliminarNiv001.Visible = true;
        }

        //***f NIV001 ***//

        //***i NIV002 ***//
        private void f_BindGridNiv002_Inicial()
        {
            objNiv002 = objNiv002.f_Niv002_Buscar(Session["gs_codmodSelected"].ToString(), Session["gs_niv001Selected"].ToString());
            if (String.IsNullOrEmpty(objNiv002.Error.Mensaje))
            {
                DataTable dt = objNiv002.dtModuloNiv002;
                Session.Add("gdt_Niv002", dt);
                if (dt.Rows.Count > 0)
                {
                    grvNiv002.DataSource = dt;
                    grvNiv002.DataBind();
                }
                else
                {
                    // add new row when the dataset is having zero record
                    dt.Rows.Add(dt.NewRow());
                    grvNiv002.DataSource = dt;
                    grvNiv002.DataBind();
                    grvNiv002.Rows[0].Visible = false;
                }
                
                if (!Session["gs_codmodSelected"].ToString().Equals("") && !Session["gs_niv001Selected"].ToString().Equals(""))
                {                    
                    DropDownList fddlgrvCodmod = (DropDownList)(grvNiv002.FooterRow.FindControl("fddlgrvCodmod"));
                    fddlgrvCodmod.SelectedValue = fddlgrvCodmod.Items.FindByValue(Session["gs_codmodSelected"].ToString()).Value;
                    DropDownList fddlgrvNiv001 = (DropDownList)(grvNiv002.FooterRow.FindControl("fddlgrvNiv001"));
                    fddlgrvNiv001.SelectedValue = fddlgrvNiv001.Items.FindByValue(Session["gs_niv001Selected"].ToString()).Value;
                    TextBox ftxtgrvNiv002 = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvNiv002"));
                    ftxtgrvNiv002.Text = (dt.Rows.Count + 1).ToString();
                    TextBox ftxtgrvNommen = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvNommen"));
                    ftxtgrvNommen.Text = Session["gs_nommenSelected"].ToString();
                    TextBox ftxtgrvNom001 = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvNom001"));
                    ftxtgrvNom001.Text = Session["gs_nom001Selected"].ToString();                    
                }
            }
            else
            {
                f_ErrorNuevo(objNiv002.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        private void f_BindGridNiv002(string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Niv002"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    //dv.RowFilter = "codmod LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "nommod LIKE " + "'%" + searchText.Trim() + "%'";
                    dv.RowFilter = "nom002 LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + string.Format("CONVERT({0}, System.String) like '%{1}%'", "niv002", searchText.Trim());
                    grvNiv002.DataSource = dv;
                }
                grvNiv002.DataBind();

                DropDownList fddlgrvCodmod = (DropDownList)(grvNiv002.FooterRow.FindControl("fddlgrvCodmod"));
                fddlgrvCodmod.SelectedValue = fddlgrvCodmod.Items.FindByValue(Session["gs_codmodSelected"].ToString()).Value;
                DropDownList fddlgrvNiv001 = (DropDownList)(grvNiv002.FooterRow.FindControl("fddlgrvNiv001"));
                fddlgrvNiv001.SelectedValue = fddlgrvNiv001.Items.FindByValue(Session["gs_niv001Selected"].ToString()).Value;
                TextBox ftxtgrvNiv002 = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvNiv002"));
                ftxtgrvNiv002.Text = (dt.Rows.Count + 1).ToString();
                TextBox ftxtgrvNommen = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvNommen"));
                ftxtgrvNommen.Text = Session["gs_nommenSelected"].ToString();
                TextBox ftxtgrvNom001 = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvNom001"));
                ftxtgrvNom001.Text = Session["gs_nom001Selected"].ToString();
            }
        }
        protected void f_GridBuscarNiv002(object sender, EventArgs e)
        {
            f_BindGridNiv002(txtSearchNiv002.Value);
        }
        protected void grvNiv002_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvNiv002.PageIndex = e.NewPageIndex;
            f_BindGridNiv002(txtSearchNiv002.Value);
            lblError.Text = lblExito.Text = "";
        }

        protected void btnAgregarNiv002_Click(object sender, EventArgs e)
        {
            DropDownList fddlgrvCodmod = (DropDownList)(grvNiv002.FooterRow.FindControl("fddlgrvCodmod"));
            DropDownList fddlgrvNiv001 = (DropDownList)(grvNiv002.FooterRow.FindControl("fddlgrvNiv001"));
            TextBox ftxtgrvNiv002 = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvNiv002"));
            TextBox ftxtgrvNommen = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvNommen"));
            TextBox ftxtgrvNom001 = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvNom001"));
            TextBox ftxtgrvNom002 = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvNom002"));
            TextBox ftxtgrvExt002 = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvExt002"));
            TextBox ftxtgrvRef002 = (TextBox)(grvNiv002.FooterRow.FindControl("ftxtgrvRef002"));

            if (ftxtgrvNiv002.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Niv002.";
                ftxtgrvNiv002.Focus();
                return;
            }
            if (ftxtgrvNommen.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nommen.";
                ftxtgrvNommen.Focus();
                return;
            }
            if (ftxtgrvNom001.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nom001.";
                ftxtgrvNom001.Focus();
                return;
            }
            if (ftxtgrvNom002.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nom002.";
                ftxtgrvNom002.Focus();
                return;
            }
            if (ftxtgrvExt002.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Ext002.";
                ftxtgrvExt002.Focus();
                return;
            }
            if (ftxtgrvRef002.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Ref002.";
                ftxtgrvRef002.Focus();
                return;
            }

            objNiv002.Codmod = int.Parse(fddlgrvCodmod.SelectedValue.Trim());
            objNiv002.Niv001 = int.Parse(fddlgrvNiv001.SelectedValue.Trim());
            objNiv002.Niv002 = int.Parse(ftxtgrvNiv002.Text.Trim());
            objNiv002.Nommen = ftxtgrvNommen.Text.Trim();
            objNiv002.Nom001 = ftxtgrvNom001.Text.Trim();
            objNiv002.Nom002 = ftxtgrvNom002.Text.Trim();
            objNiv002.Ext002 = ftxtgrvExt002.Text.Trim();
            objNiv002.Ref002 = ftxtgrvRef002.Text.Trim();

            clsError objError = objNiv002.f_Niv002_Actualizar(objNiv002, "Add");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ " + DateTime.Now + " Guardado Exitoso.";
                f_BindGridNiv002_Inicial();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void btnEditarNiv002_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblExito.Text = "";
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvNiv002.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for edit
                    editindex = gvrow.RowIndex;

                    rowCount++;
                    if (rowCount > 1)
                    {
                        break;
                    }
                }
            }

            if (rowCount > 1)
            {
                lblError.Text = "Seleccione solo una fila para editar.";
            }
            else if (rowCount == 0)
            {
                lblError.Text = "Seleccione una fila para editar.";
            }
            else
            {
                ViewState["editindex"] = editindex;

                grvNiv002.EditIndex = editindex;
                f_BindGridNiv002_Inicial();

                // disable all row checkboxes while editing
                foreach (GridViewRow gvrow in grvNiv002.Rows)
                {
                    CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                    chkRow.Enabled = false;
                }

                //hide footer row while editing
                grvNiv002.FooterRow.Visible = false;

                // hide and disable respective buttons
                f_DisableBtnModulo();
                f_DisableBtnNiv001();
                btnAgregarNiv002.Enabled = false;
                btnAgregarNiv002.Visible = false;
                btnEditarNiv002.Visible = false;
                btnGuardarNiv002.Visible = true;
                btnEliminarNiv002.Enabled = false;
                btnEliminarNiv002.Visible = false;
            }
        }
        protected void btnGuardarNiv002_Click(object sender, EventArgs e)
        {
            // retrieving current row edit index from viewstate
            editindex = Convert.ToInt32(ViewState["editindex"].ToString());

            int Id = Convert.ToInt32(grvNiv002.DataKeys[editindex].Values["niv002"].ToString());

            Label elblgrvCodmod = (Label)(grvNiv002.Rows[editindex].FindControl("elblgrvCodmod"));
            Label elblgrvNiv001 = (Label)(grvNiv002.Rows[editindex].FindControl("elblgrvNiv001"));
            Label elblgrvNiv002 = (Label)(grvNiv002.Rows[editindex].FindControl("elblgrvNiv002"));
            TextBox etxtgrvNommen = (TextBox)(grvNiv002.Rows[editindex].FindControl("etxtgrvNommen"));
            TextBox etxtgrvNom001 = (TextBox)(grvNiv002.Rows[editindex].FindControl("etxtgrvNom001"));
            TextBox etxtgrvNom002 = (TextBox)(grvNiv002.Rows[editindex].FindControl("etxtgrvNom002"));
            TextBox etxtgrvExt002 = (TextBox)(grvNiv002.Rows[editindex].FindControl("etxtgrvExt002"));
            TextBox etxtgrvRef002 = (TextBox)(grvNiv002.Rows[editindex].FindControl("etxtgrvRef002"));

            if (etxtgrvNommen.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nommen.";
                etxtgrvNommen.Focus();
                return;
            }
            if (etxtgrvNom001.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nom001.";
                etxtgrvNom001.Focus();
                return;
            }
            if (etxtgrvNom002.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Nom002.";
                etxtgrvNom002.Focus();
                return;
            }
            if (etxtgrvExt002.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Ext002.";
                etxtgrvExt002.Focus();
                return;
            }
            if (etxtgrvRef002.Text.Trim() == string.Empty)
            {
                lblError.Text = "❗ Ingrese Ref002.";
                etxtgrvRef002.Focus();
                return;
            }

            objNiv002.Codmod = int.Parse(elblgrvCodmod.Text.Trim());
            objNiv002.Niv001 = int.Parse(elblgrvNiv001.Text.Trim());
            objNiv002.Niv002 = int.Parse(elblgrvNiv002.Text.Trim());
            objNiv002.Nommen = etxtgrvNommen.Text.Trim();
            objNiv002.Nom001 = etxtgrvNom001.Text.Trim();
            objNiv002.Nom002 = etxtgrvNom002.Text.Trim();
            objNiv002.Ext002 = etxtgrvExt002.Text.Trim();
            objNiv002.Ref002 = etxtgrvRef002.Text.Trim();

            clsError objError = objNiv002.f_Niv002_Actualizar(objNiv002, "Update");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ " + DateTime.Now + " Guardado Exitoso.";
                grvNiv002.EditIndex = -1;
                f_BindGridNiv002_Inicial();
                btnAgregarNiv002.Enabled = true;
                btnAgregarNiv002.Visible = true;
                btnEditarNiv002.Visible = true;
                btnGuardarNiv002.Visible = false;
                btnEliminarNiv002.Enabled = true;
                btnEliminarNiv002.Visible = true;
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            f_EnableBtnModulo();
            f_EnableBtnNiv001();
        }
        protected void btnDeleteNiv002_Click(object sender, EventArgs e)
        {
            int rowCount = 0;
            foreach (GridViewRow gvrow in grvNiv002.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;
                    Label lblgrvCodmod = (Label)grvNiv002.Rows[editindex].FindControl("lblgrvCodmod");
                    Label lblgrvNiv001 = (Label)grvNiv002.Rows[editindex].FindControl("lblgrvNiv001");
                    Label lblgrvNom001 = (Label)grvNiv002.Rows[editindex].FindControl("lblgrvNom001");
                    Label lblgrvNiv002 = (Label)grvNiv002.Rows[editindex].FindControl("lblgrvNiv002");
                    Label lblgrvNom002 = (Label)grvNiv002.Rows[editindex].FindControl("lblgrvNom002");
                    lblModuloNiv002.Text = lblgrvCodmod.Text;
                    lblNiv001Niv002.Text = lblgrvNiv001.Text + " - " + lblgrvNom001.Text;
                    lblEliNiv002.Text += " / " + lblgrvNiv002.Text + " - " + lblgrvNom002.Text + " / ";
                    rowCount++;
                }
            }

            if (rowCount > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_EliminarNiv002();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar un Nivel para eliminar.";
            }
        }
        protected void btnEliminarNiv002_Click(object sender, EventArgs e)
        {
            int rowCount = 0;

            clsError objError = new clsError();

            foreach (GridViewRow gvrow in grvNiv002.Rows)
            {
                CheckBox chkRow = (CheckBox)gvrow.FindControl("chkRow");
                if (chkRow.Checked && chkRow != null)
                {
                    //finding rowindex for delete
                    editindex = gvrow.RowIndex;

                    // retrieve Id from DataKeys to delete record
                    int Id = Convert.ToInt32(grvNiv002.DataKeys[editindex].Values["niv002"].ToString());
                    Label lblgrvCodmod = (Label)grvNiv002.Rows[editindex].FindControl("lblgrvCodmod");
                    Label lblgrvNiv001 = (Label)grvNiv002.Rows[editindex].FindControl("lblgrvNiv001");
                    Label lblgrvNiv002 = (Label)grvNiv002.Rows[editindex].FindControl("lblgrvNiv002");

                    objError = objNiv002.f_Niv002_Eliminar(lblgrvCodmod.Text, lblgrvNiv001.Text, lblgrvNiv002.Text, "Delete");
                    if (!String.IsNullOrEmpty(objError.Mensaje))
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        break;
                    }

                    rowCount++;
                }
            }

            if (rowCount > 0 && String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblEliNiv002.Text = "";
                lblExito.Text = "✔️ Registro eliminado.";

                f_BindGridNiv002_Inicial();
            }
            else
            {
                lblExito.Text = "";
                lblError.Text = "Seleccione una fila para eliminar.";
            }
        }
        protected void btnCancelarNiv002_Click(object sender, EventArgs e)
        {
            grvNiv002.EditIndex = -1;
            f_BindGridNiv002_Inicial();

            f_EnableBtnModulo();
            f_EnableBtnNiv001();
            btnAgregarNiv002.Enabled = true;
            btnAgregarNiv002.Visible = true;
            btnEditarNiv002.Visible = true;
            btnGuardarNiv002.Visible = false;
            btnEliminarNiv002.Enabled = true;
            btnEliminarNiv002.Visible = true;
            lblError.Text = lblExito.Text = "";

        }
        protected void f_DisableBtnNiv002()
        {
            btnAgregarNiv002.Enabled = btnEditarNiv002.Enabled = btnGuardarNiv002.Enabled = btnCancelarNiv002.Enabled = btnEliminarNiv002.Enabled = false;
            btnAgregarNiv002.Visible = btnEditarNiv002.Visible = btnGuardarNiv002.Visible = btnCancelarNiv002.Visible = btnEliminarNiv002.Visible = false;
        }
        protected void f_EnableBtnNiv002()
        {
            btnAgregarNiv002.Enabled = btnEditarNiv002.Enabled = btnGuardarNiv002.Enabled = btnCancelarNiv002.Enabled = btnEliminarNiv002.Enabled = true;
            btnAgregarNiv002.Visible = btnEditarNiv002.Visible = btnCancelarNiv002.Visible = btnEliminarNiv002.Visible = true;
        }

        //***f NIV002 ***//
    }
}