﻿<%@ Page Title="Usuario Administrar" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_seg_UsuarioAdministrar.aspx.cs" Inherits="prj_KohinorWeb.w_seg_UsuarioAdministrar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item"><a href="w_seg_Usuario.aspx">Usuario</a></li>
            <li class="breadcrumb-item active">Administrar Usuario</li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <a class="c-subheader-nav-link">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary" type="button" id="">
                            <i class="cil-filter"></i>
                        </button>
                    </div>

                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
                </div>
                <asp:LinkButton ID="lkbtnSearch" runat="server" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
            </a>

            <%--<a class="c-subheader-nav-link">
                <i class="c-icon cil-speech"></i>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-graph"></i>&nbsp;
                            Escritorio
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-settings"></i>
            </a>--%>
        </div>
    </div>
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item active">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click"><i class="cil-file"></i>Nuevo</button>
                    <button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" onserverclick="btnAbrir_Click"><i class="cil-folder-open"></i>Abrir</button>
                    <button type="button" class="btn btn-outline-dark" id="btnGuardar" runat="server" onserverclick="btnGuardarUsuario_Click"><i class="cil-save"></i>Guardar</button>
                    <button type="button" class="btn btn-outline-dark" id="btnCancelar" runat="server" onserverclick="btnCancelar_Click"><i class="cil-grid-slash"></i>Cancelar</button>
                    <button type="button" class="btn btn-outline-danger" id="btnEliminar" runat="server" onserverclick="lkbtnDelete_Click"><i class="cil-trash"></i>Eliminar</button>
                </div>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-dark" id="lkbtnPrincipio" runat="server" onserverclick="lkbtnPrincipio_Click"><i class="cil-media-step-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnAtras" runat="server" onserverclick="lkbtnAtras_Click"><i class="cil-media-skip-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnSiguiente" runat="server" onserverclick="lkbtnSiguiente_Click"><i class="cil-media-skip-forward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnFinal" runat="server" onserverclick="lkbtnFinal_Click"><i class="cil-media-step-forward"></i></button>

                </div>
                <div class="btn-group" role="group" aria-label="Usuario Modulos">
                    <button class="btn btn-primary" type="button" runat="server" id="btnUsuarioModulo" onserverclick="btnUsuarioModulo_Click">
                        <i class="cil-3d"></i>&nbsp;Usuario Módulos
                    </button>
                </div>
                <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
            </li>
        </ol>

    </div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <!-- Begin Page Content -->
                <div id="frmVerificar" class="needs-validation" runat="server" novalidate>
                    <!-- Page Heading -->
                    <div class="card text-white bg-secondary text-black-50">
                        <div class="card-body">

                            <div class="form-row">
                                <div class="col-md-2">
                                    <h4><strong>Creación de Usuarios</strong></h4>
                                </div>

                                <div class="form-group col-md-2">
                                    <input type="text" id="txtCodus1" class="form-control" runat="server" required />
                                    <label for="txtCodus1">Usuario</label>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" id="txtClausu" class="form-control" runat="server" required />
                                    <label for="txtClausu">Clave</label>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" id="txtNomusu" class="form-control" runat="server" required />
                                    <label for="txtNomusu">Nombre</label>
                                </div>

                                <div class="form-group col-md-4">

                                    <input type="text" id="txtUsmail" class="form-control" runat="server" required />
                                    <label for="txtUsmail">Mail</label>
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-md-2 align-items-center justify-content-center">
                                    <div class="form-row align-items-center justify-content-center">
                                        <asp:Image ID="imgFotoUsuario" class="img-thumbnail" runat="server" Width="110px" Height="140px" />
                                        <div class="input-group mb-3 mt-3">
                                            <div class="custom-file">
                                                <input id="imgUsuario" type="file" class="custom-file-input" onchange="f_vistaPreviaImg()" runat="server">
                                                <label class="custom-file-label" for="imgUsuario" aria-describedby="inputGroupFileAddon02">Foto</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <input type="text" id="txtCodmer" class="form-control" runat="server" required />
                                            <label for="txtCodmer">Linea de Mercado</label>
                                            <div class="valid-feedback">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="text" id="txtCedusu" class="form-control" runat="server" required />
                                            <label for="txtCedusu">Cedula</label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input type="text" id="txtCodfun" class="form-control" runat="server" required />
                                            <label for="txtCodfun">Funcionario</label>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <asp:DropDownList ID="ddlCodsuc" class="form-control" runat="server" DataSourceID="sqldsCodsuc" DataTextField="nomsuc" DataValueField="codsuc"></asp:DropDownList>
                                            <label for="ddlCodsuc">Sucursal</label>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <asp:DropDownList ID="ddlCodalm" class="form-control" runat="server" DataSourceID="sqldsCodalm" DataTextField="nomalm" DataValueField="codalm"></asp:DropDownList>
                                            <label for="ddlCodalm">Almacen</label>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="text" id="txtSersec" class="form-control" runat="server" />
                                            <label for="txtSersec">Serie</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <div class="card text-white bg-secondary text-black-50">
                                <div class="card-body">
                                    <h5>Datos Basicos</h5>

                                    <hr />
                                    <br />
                                    <div class="form-row">
                                        <div class="form-group col-md-1">
                                            <label>Tipo</label>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <asp:DropDownList ID="ddlTipo" class="form-control" runat="server">
                                                <asp:ListItem Value="1">Grupo</asp:ListItem>
                                                <asp:ListItem Value="2">Usuario</asp:ListItem>
                                            </asp:DropDownList>
                                            <label for="ddlTipo"></label>
                                        </div>

                                        <div class="form-group col-md-1">
                                            <label>Cierre</label>
                                        </div>
                                        <div class="form-group col-md-5">

                                            <asp:DropDownList ID="ddlOpcion" class="form-control" runat="server">
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                                <asp:ListItem Value="2">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <asp:DropDownList ID="ddlBancor" class="form-control" runat="server"></asp:DropDownList>
                                            <label for="ddlBancor">Expiración</label>

                                        </div>
                                        <div class="form-group col-md-6">
                                            <asp:DropDownList ID="ddlCodven" class="form-control" runat="server" DataSourceID="sqldsCodven" DataTextField="nomven" DataValueField="codven"></asp:DropDownList>
                                            <label for="ddlCodven">Vendedor</label>

                                        </div>
                                    </div>
                                    <div class="form-row">

                                        <div class="form-group col-md-6">
                                            <asp:TextBox ID="txtFecing" runat="server" class="form-control" TextMode="DateTime" ReadOnly="True"></asp:TextBox>
                                            <label for="txtFecing">Fecha Inicio</label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input type="date" id="txtFecexp" class="form-control" runat="server" required />
                                            <label for="txtFecexp">Fecha Expiración</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <div class="card text-white bg-secondary text-black-50">
                                <div class="card-body">
                                    <h5>Accesos</h5>
                                    <hr />
                                    <br />
                                    <div class="form-row">

                                        <div class="form-group col-md-1">
                                            <label>Tipo</label>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <asp:DropDownList ID="ddlTipusu" runat="server" class="form-control" required>
                                                <asp:ListItem Value="1">Super Administrador</asp:ListItem>
                                                <asp:ListItem Value="2">Administrador</asp:ListItem>
                                                <asp:ListItem Value="3">Usuario</asp:ListItem>
                                                <asp:ListItem Value="4">Cliente</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <input type="text" id="txtLimcre" class="form-control" runat="server" required />
                                            <label for="txtLimcre">Credito</label>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" id="txtPordes" class="form-control" runat="server" required />
                                            <label for="txtPordes">% Descto</label>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" id="txtNumpag" class="form-control" runat="server" required />
                                            <label for="txtFecexp">Cuota</label>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" id="txtPlapag" class="form-control" runat="server" required />
                                            <label for="txtFecexp">Plazo</label>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <asp:DropDownList ID="ddlAsiusu" class="form-control" runat="server" DataSourceID="sqldsAcessoUsuario" DataTextField="codus1" DataValueField="codus1"></asp:DropDownList>
                                            <label for="ddlAsiusu">Acceso</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.container-fluid -->
            <!-- End of Main Content -->
        </div>
    </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsClaseCliente" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codcla])) AS codcla, [nomcla] FROM [vc_cliente_clase] "></asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsVendedor" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codven], [nomven] FROM [vc_vendedor]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodsuc" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codsuc], [nomsuc] FROM [seg_sucursal]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodven" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codven], [nomven] FROM [vc_vendedor]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodalm" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codalm], [nomalm] FROM [inv_almacen]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsUsuarioExiste" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codus1] FROM [seg_usuario] WHERE (([codemp] = @codemp) AND ([codus1] = @codus1))">
        <SelectParameters>
            <asp:SessionParameter Name="codemp" SessionField="gs_Codemp" Type="String" DefaultValue=" " />
            <asp:SessionParameter Name="codus1" SessionField="gs_codus1Selected" Type="String" DefaultValue=" " />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsUsuarioExisteWEB" runat="server" ConnectionString="<%$ ConnectionStrings:Default %>" SelectCommand="SELECT [codus1], [pcip] FROM [seg_usuario] WHERE (([rucemp] = @rucemp) AND ([codus1] = @codus1))">
        <SelectParameters>
            <asp:SessionParameter Name="rucemp" SessionField="gs_Rucemp" Type="String" DefaultValue=" " />
            <asp:SessionParameter Name="codus1" SessionField="gs_codus1Selected" Type="String" DefaultValue=" " />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsAcessoUsuario" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codus1] FROM [seg_usuario]"></asp:SqlDataSource>
    <!--f SQL DATASOURCES-->


    <!-- Modal Eliminar -->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Usuario</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar al usuario '<asp:Label ID="lblEliCli" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarUsuario_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>
</asp:Content>
