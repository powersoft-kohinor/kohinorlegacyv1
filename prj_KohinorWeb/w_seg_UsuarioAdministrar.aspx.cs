﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_seg_UsuarioAdministrar : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "10", niv001 = "2", niv002 = "12";
        protected cls_seg_Usuario objUsuario = new cls_seg_Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Add("gs_Siguiente", ""); //para el lkbtnSiguiente Y lkbtnAtras

                f_Seg_Usuario_Menu();
                if (String.IsNullOrEmpty(Session["gs_codus1Selected"].ToString()))
                    Session.Add("gs_Action", "Add");
                else
                    Session.Add("gs_Action", "Update");

                ddlCodalm.DataBind();
                ddlCodsuc.DataBind();
                ddlCodven.DataBind();
                ddlBancor.DataBind();
                ddlAsiusu.DataBind();
                txtFecing.Text = DateTime.Now.ToString("yyyy-MM-dd");
                //ddlEstadoCivil.DataBind();
                //ddlCodcla.DataBind(); //OJO DEBE IR ANTES DE f_llenarCamposEditar

                objUsuario = objUsuario.f_Usuario_Buscar(Session["gs_Codemp"].ToString(), Session["gs_codus1Selected"].ToString());
                if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
                {
                    f_llenarCamposEditar(objUsuario);
                }
                else
                {
                    f_ErrorNuevo(objUsuario.Error);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            if (IsPostBack)
            {
                lblExito.Text = "";
                lblError.Text = "";
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAbrir.Attributes.Add("class", "btn btn-dark");
                btnNuevo.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-dark");
                btnCancelar.Attributes.Add("class", "btn btn-dark");
                btnEliminar.Attributes.Add("class", "btn btn-danger");

                lkbtnPrincipio.Attributes.Add("class", "btn btn-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");

            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
                btnNuevo.Attributes.Add("class", "btn btn-outline-primary");

                btnGuardar.Attributes.Add("class", "btn btn-outline-dark");
                btnCancelar.Attributes.Add("class", "btn btn-outline-dark");
                btnEliminar.Attributes.Add("class", "btn btn-outline-danger ");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");
            }
        }

        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Usuario
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnNuevo.Disabled = true;
                    }
                    if (row["banbus"].Equals("N")) //Abrir
                    {
                        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnAbrir.Disabled = true;
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnGuardar.Disabled = true;
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnCancelar.Disabled = true;

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEliminar.Disabled = true;
                        Session["baneli"] = "N";
                    }
                }
            }
            return dt;
        }


        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_codus1Selected"] = "";
            f_limpiarCamposEditar();
        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_seg_Usuario.aspx");
        }
        protected void btnGuardarUsuario_Click(object sender, EventArgs e)
        {
            Session["gs_codus1Selected"] = txtCodus1.Value;
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsUsuarioExisteWEB.Select(args); //Revisa si usu existe en WEB para poder extraer la pcip, pcnom, pcmac
            DataTable dt1 = view.ToTable();
            if (dt1 != null)
            {
                if (dt1.Rows.Count > 0)
                {
                    string s_pcip = dt1.Rows[0]["pcip"].ToString();
                    DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                    DataView view2 = (DataView)sqldsUsuarioExiste.Select(args2);
                    DataTable dt2 = view2.ToTable();
                    if (dt2 != null)
                    {
                        if (dt2.Rows.Count > 0) //Actualizar
                        {
                            //DataTable dtUsuarioExiste = view.ToTable();
                            if (!(txtCodus1.Value.Equals("") || txtNomusu.Value.Equals("") || txtUsmail.Value.Equals("") || txtCodfun.Value.Equals("") || txtUsmail.Value.Equals("") || txtPordes.Value.Equals("") || txtLimcre.Value.Equals("") || txtNumpag.Value.Equals("") || txtPlapag.Value.Equals("") || txtCedusu.Value.Equals("") || txtCodmer.Value.Equals("")))
                            {
                                objUsuario.Codemp = Session["gs_Codemp"].ToString();
                                objUsuario.Codus1 = txtCodus1.Value;
                                objUsuario.Tipusu = ddlTipusu.Text;
                                objUsuario.Nomusu = txtNomusu.Value;
                                objUsuario.Fecing = DateTime.Now.ToString("yyyy-MM-dd");
                                objUsuario.Fecult = DateTime.Now.ToString("yyyy-MM-dd");
                                objUsuario.Fecexp = txtFecexp.Value;
                                objUsuario.Asiusu = ddlAsiusu.Text;
                                objUsuario.Codven = ddlCodven.Text;
                                objUsuario.Codalm = ddlCodalm.Text;
                                objUsuario.Sersec = txtSersec.Value;
                                objUsuario.Codsuc = ddlCodsuc.Text;
                                objUsuario.Usuing = Session["gs_UsuIng"].ToString();
                                objUsuario.Codusu = Session["gs_UsuIng"].ToString();
                                objUsuario.Usmail = txtUsmail.Value;
                                objUsuario.Codfun = txtCodfun.Value;
                                objUsuario.Limcre = Convert.ToDecimal(txtLimcre.Value);
                                objUsuario.Numpag = Convert.ToDecimal(txtNumpag.Value);
                                objUsuario.Plapag = Convert.ToDecimal(txtPlapag.Value);
                                objUsuario.Bancor = ddlBancor.Text;
                                objUsuario.Pordes = Convert.ToDecimal(txtPordes.Value);
                                objUsuario.Cedusu = txtCedusu.Value;
                                objUsuario.Codmer = txtCodmer.Value;
                                objUsuario.Rucemp = Session["gs_Rucemp"].ToString();
                                objUsuario.Clausu = txtClausu.Value.Trim();
                                objUsuario.Pcnom = s_pcip;
                                objUsuario.Pcip = s_pcip;
                                objUsuario.Pcmac = s_pcip;

                                //DataTable dt = objUsuario.f_dtUsuarioTableType();
                                //DataRow dr = null;
                                //dr = dt.NewRow();
                                //dr["codemp"] = Session["gs_Codemp"].ToString();
                                //dr["codus1"] = Session["gs_CodUs1"].ToString();
                                //dr["tipusu"] = ddlTipusu.Text;
                                //dr["nomusu"] = txtNomusu.Value;
                                //dr["fecing"] = txtFecing.Value;
                                //dr["fecexp"] = txtFecexp.Value;
                                //dr["asiusu"] = ddlAsiusu.Text;
                                //dr["codven"] = ddlCodven.Text;
                                //dr["codalm"] = ddlCodalm.Text;
                                //dr["sersec"] = txtSersec.Value;
                                //dr["codsuc"] = ddlCodsuc.Text;
                                //dr["usuing"] = Session["gs_UsuIng"].ToString();
                                //dr["usmail"] = txtUsmail.Value;
                                //dr["codfun"] = txtCodfun.Value;
                                //dr["limcre"] = txtLimcre.Value;
                                //dr["numpag"] = txtNumpag.Value;
                                //dr["plapag"] = txtPlapag.Value;
                                //dr["bancor"] = ddlBancor.Text;
                                //dr["pordes"] = txtPordes.Value;
                                //dr["cedusu"] = txtCedusu.Value;
                                //dr["codmer"] = txtCodmer.Value;
                                //dr["clausu"] = txtClausu.Value;
                                //dt.Rows.Add(dr);
                                //objUsuario.dtUsuario = dt;

                                clsError objError = objUsuario.f_Usuario_Actualizar(objUsuario, "Update");
                                if (String.IsNullOrEmpty(objError.Mensaje))
                                {
                                    objError = objUsuario.f_Usuario_Clave(objUsuario); //guarda pass encriptado
                                    if (String.IsNullOrEmpty(objError.Mensaje))
                                    {
                                        lblError.Text = "";
                                        lblExito.Text = "✔️ Guardado Exitoso. Usuario " + Session["gs_codus1Selected"].ToString() + " actualizado.";
                                    }
                                    else
                                    {
                                        f_ErrorNuevo(objError);
                                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                                    }
                                }
                                else
                                {
                                    f_ErrorNuevo(objError);
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                                }
                            }
                            else
                            {
                                lblError.Text = "❗ *01. " + DateTime.Now + " Debe llenar todos los campos para crear un Usuario.";
                            }
                        }
                        else //Insertar
                        {
                            frmVerificar.Attributes.Add("class", "needs-validation was-validated");

                            if (!(txtCodus1.Value.Equals("") || txtNomusu.Value.Equals("") || txtUsmail.Value.Equals("") || txtCodfun.Value.Equals("") || txtUsmail.Value.Equals("") || txtPordes.Value.Equals("") || txtLimcre.Value.Equals("") || txtNumpag.Value.Equals("") || txtPlapag.Value.Equals("") || txtCedusu.Value.Equals("") || txtCodmer.Value.Equals("")))
                            {
                                objUsuario.Codemp = Session["gs_Codemp"].ToString();
                                objUsuario.Codus1 = txtCodus1.Value;
                                objUsuario.Tipusu = ddlTipusu.Text;
                                objUsuario.Nomusu = txtNomusu.Value;
                                objUsuario.Fecing = DateTime.Now.ToString("yyyy-MM-dd");
                                objUsuario.Fecult = DateTime.Now.ToString("yyyy-MM-dd");
                                objUsuario.Fecexp = txtFecexp.Value;
                                objUsuario.Asiusu = ddlAsiusu.Text;
                                objUsuario.Codven = ddlCodven.Text;
                                objUsuario.Codalm = ddlCodalm.Text;
                                objUsuario.Sersec = txtSersec.Value;
                                objUsuario.Codsuc = ddlCodsuc.Text;
                                objUsuario.Usuing = Session["gs_UsuIng"].ToString();
                                objUsuario.Codusu = Session["gs_UsuIng"].ToString();
                                objUsuario.Usmail = txtUsmail.Value;
                                objUsuario.Codfun = txtCodfun.Value;
                                objUsuario.Limcre = Convert.ToDecimal(txtLimcre.Value);
                                objUsuario.Numpag = Convert.ToDecimal(txtNumpag.Value);
                                objUsuario.Plapag = Convert.ToDecimal(txtPlapag.Value);
                                objUsuario.Bancor = ddlBancor.Text;
                                objUsuario.Pordes = Convert.ToDecimal(txtPordes.Value);
                                objUsuario.Cedusu = txtCedusu.Value;
                                objUsuario.Codmer = txtCodmer.Value;
                                objUsuario.Rucemp = Session["gs_Rucemp"].ToString();
                                objUsuario.Clausu = txtClausu.Value.Trim();
                                objUsuario.Pcnom = s_pcip;
                                objUsuario.Pcip = s_pcip;
                                objUsuario.Pcmac = s_pcip;

                                clsError objError = objUsuario.f_Usuario_Actualizar(objUsuario, "Add");
                                if (String.IsNullOrEmpty(objError.Mensaje))
                                {


                                    objError = objUsuario.f_Usuario_Clave(objUsuario); //guarda pass encriptado
                                    if (String.IsNullOrEmpty(objError.Mensaje))
                                    {
                                        lblError.Text = "";
                                        lblExito.Text = "✔️ Guardado Exitoso. Usuario " + Session["gs_codus1Selected"].ToString() + " creado.";
                                    }
                                    else
                                    {
                                        f_ErrorNuevo(objError);
                                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                                    }
                                }
                                else
                                {
                                    f_ErrorNuevo(objError);
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                                }
                            }
                            else
                            {
                                lblError.Text = "❗ *01. " + DateTime.Now + " Debe llenar todos los campos para crear un Usuario.";
                            }

                            //DataTable dt = objUsuario.f_dtUsuarioTableType();
                            //DataRow dr = null;
                            //dr = dt.NewRow();
                            //dr["codemp"] = Session["gs_Codemp"].ToString();
                            //dr["codus1"] = txtCodus1.Value;
                            //dr["tipusu"] = ddlTipusu.Text;
                            //dr["nomusu"] = txtNomusu.Value;
                            //dr["fecing"] = txtFecing.Value;
                            //dr["fecexp"] = txtFecexp.Value;
                            //dr["asiusu"] = ddlAsiusu.Text;
                            //dr["codven"] = ddlCodven.Text;
                            //dr["codalm"] = ddlCodalm.Text;
                            //dr["sersec"] = txtSersec.Value;
                            //dr["codsuc"] = ddlCodsuc.Text;
                            //dr["usuing"] = Session["gs_UsuIng"].ToString();
                            //dr["usmail"] = txtUsmail.Value;
                            //dr["codfun"] = txtCodfun.Value;
                            //dr["limcre"] = txtLimcre.Value;
                            //dr["numpag"] = txtNumpag.Value;
                            //dr["plapag"] = txtPlapag.Value;
                            //dr["bancor"] = ddlBancor.Text;
                            //dr["pordes"] = txtPordes.Value;
                            //dr["cedusu"] = txtCedusu.Value;
                            //dr["codmer"] = txtCodmer.Value;
                            //dr["clausu"] = txtClausu.Value;
                            //dt.Rows.Add(dr);
                            //objUsuario.dtUsuario = dt;
                        }
                    }                   
                }
                else
                {
                    lblError.Text = "❗ *04. " + DateTime.Now + " Código de usuario no registrado.";
                }
            }           
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_codus1Selected"] = "";
            f_limpiarCamposEditar();
        }
        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_codus1Selected"].ToString()))
            {
                //Session["gs_codus1Selected"] = txtCodcli.Value;
                lblEliCli.Text = Session["gs_codus1Selected"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar un usuario para eliminar.";
            }

        }
        protected void btnEliminarUsuario_Click(object sender, EventArgs e)
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario(); //eliminar
            clsError objError = objUsuario.f_Usuario_Eliminar(Session["gs_codus1Selected"].ToString(), Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblExito.Text = "✔️ " + DateTime.Now + " Guardado Exitoso.";
                f_limpiarCamposEditar();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

        }
        protected void lkbtnPrincipio_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objUsuario = objUsuario.f_Usuario_Principio(Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codus1Selected"] = objUsuario.Codus1;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objUsuario.Nomusu;

                f_llenarCamposEditar(objUsuario);
            }
            else
            {
                f_ErrorNuevo(objUsuario.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

        }
        protected void lkbtnSiguiente_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            string p = Session["gs_Codemp"].ToString();
            string p2 = Session["gs_Siguiente"].ToString();


            objUsuario = objUsuario.f_Usuario_Siguiente(Session["gs_Codemp"].ToString(), Session["gs_Siguiente"].ToString());


            if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codus1Selected"] = objUsuario.Codus1;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objUsuario.Nomusu;

                f_llenarCamposEditar(objUsuario);
            }
            else
            {
                f_ErrorNuevo(objUsuario.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnFinal_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objUsuario = objUsuario.f_Usuario_Final(Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codus1Selected"] = objUsuario.Codus1;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objUsuario.Nomusu;

                f_llenarCamposEditar(objUsuario);
            }
            else
            {
                f_ErrorNuevo(objUsuario.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnAtras_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objUsuario = objUsuario.f_Usuario_Atras(Session["gs_Codemp"].ToString(), Session["gs_Siguiente"].ToString());
            if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codus1Selected"] = objUsuario.Codus1;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objUsuario.Nomusu;

                f_llenarCamposEditar(objUsuario);
            }
            else
            {
                f_ErrorNuevo(objUsuario.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        public void f_limpiarCamposEditar()
        {
            frmVerificar.Attributes.Add("class", "needs-validation");
            txtCodus1.Value = txtClausu.Value = txtNomusu.Value = txtUsmail.Value = txtCodmer.Value = txtCedusu.Value = txtCodfun.Value = txtFecexp.Value = "";
            txtFecing.Text = DateTime.Now.ToString("dd/MM/yyyy");
            lblError.Text = lblError.Text = "";
        }
        public void f_llenarCamposEditar(cls_seg_Usuario objUsuario)
        {
            //string tmpImgCli = objCliente.Imgcli;
            //if (!tmpImgCli.Equals(""))
            //{
            //    imgFotoCliente.Attributes.Add("src", tmpImgCli);
            //}
            //else
            //{
            //    imgFotoCliente.Attributes.Add("src", "~/Modulos/Ventas/ImgCliente/user.png");
            //}

            txtCodus1.Value = objUsuario.Codus1;
            //Session["gs_Siguiente"] = objUsuario.Nomcli;
            txtClausu.Value = objUsuario.Clausu;
            txtNomusu.Value = objUsuario.Nomusu;
            txtCodmer.Value = objUsuario.Codmer;
            txtCedusu.Value = objUsuario.Cedusu;
            txtCodfun.Value = objUsuario.Codfun;
            txtFecing.Text = objUsuario.Fecing;
            txtFecexp.Value = objUsuario.Fecexp;

            //if (objUsuario.Sexcli.Equals("1"))
            //    rbtGrupo.Checked = true;
            //else
            //    rbtGrupo.Checked = true;

            string s_codsuc = objUsuario.Codsuc;
            string s_codalm = objUsuario.Codalm;
            string s_bancor = objUsuario.Bancor;
            string s_asiusu = objUsuario.Asiusu;
            string s_codven = objUsuario.Codven;


            //if (String.IsNullOrEmpty(s_codsuc))
            //    ddlCodsuc.SelectedValue = ddlCodsuc.Items.FindByValue("001").Value;
            //else
            //    ddlCodsuc.SelectedValue = ddlCodsuc.Items.FindByValue(s_codsuc).Value;

            //if (String.IsNullOrEmpty(s_codven))
            //    ddlCodven.SelectedValue = ddlCodven.Items.FindByValue("01").Value;
            //else
            //    ddlCodven.SelectedValue = ddlCodven.Items.FindByValue(s_codven).Value;

            //if (String.IsNullOrEmpty(s_bancor))
            //    ddlBancor.SelectedValue = ddlCodven.Items.FindByValue("N").Value;
            //else
            //    ddlBancor.SelectedValue = ddlCodven.Items.FindByValue(s_bancor).Value;


            //if (String.IsNullOrEmpty(s_asiusu))
            //    ddlAsiusu.SelectedValue = ddlCodven.Items.FindByValue("").Value;
            //else
            //    ddlAsiusu.SelectedValue = ddlCodven.Items.FindByValue(s_asiusu).Value;


            //if (String.IsNullOrEmpty(s_codalm))
            //    ddlCodalm.SelectedValue = ddlCodven.Items.FindByValue("001").Value;
            //else
            //    ddlBancor.SelectedValue = ddlCodven.Items.FindByValue(s_codalm).Value;



            string s_fecing = objUsuario.Fecing;
            try
            {
                DateTime d_date = DateTime.Parse(s_fecing);
                string s_Fecfinal = d_date.ToString("yyyy-MM-dd");
                txtFecing.Text = s_Fecfinal;
            }
            catch (Exception ex) //si tiene fecnac en NULL
            {
                txtFecing.Text = DateTime.Now.Date.ToString();
                //throw;
            }

            string s_fecexp = objUsuario.Fecexp;
            try
            {
                DateTime d_date = DateTime.Parse(s_fecexp);
                string s_Fecfinal = d_date.ToString("yyyy-MM-dd");
                txtFecexp.Value = s_Fecfinal;
            }
            catch (Exception ex) //si tiene fecnac en NULL
            {
                txtFecexp.Value = DateTime.Now.Date.ToString();
                //throw;
            }


            txtLimcre.Value = Convert.ToString(objUsuario.Limcre);
            txtPordes.Value = Convert.ToString(objUsuario.Pordes);
            txtNumpag.Value = Convert.ToString(objUsuario.Numpag);
            txtPlapag.Value = Convert.ToString(objUsuario.Plapag);
            txtUsmail.Value = objUsuario.Usmail;

        }

        protected void btnUsuarioModulo_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_codus1Selected"].ToString()))
            {
                Response.Redirect("w_seg_UsuarioModulo.aspx");
            }
            else
            {
                lblError.Text = "❗ *03. " + DateTime.Now + " Debe seleccionar un usuario para administrar sus módulos.";
            }
        }
        
    }
}