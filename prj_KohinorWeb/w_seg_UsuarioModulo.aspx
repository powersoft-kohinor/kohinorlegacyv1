﻿<%@ Page Title="Usuario Módulo" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_seg_UsuarioModulo.aspx.cs" Inherits="prj_KohinorWeb.w_seg_UsuarioModulo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ContentSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item"><a href="w_seg_Usuario.aspx">Usuario</a></li>
            <li class="breadcrumb-item"><a href="w_seg_UsuarioAdministrar.aspx">Administrar Usuario</a></li>
            <li class="breadcrumb-item active">Usuario Módulo -
                <asp:Label ID="lblCodus1" runat="server" Text=""></asp:Label></li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <asp:UpdatePanel ID="updError" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <asp:UpdatePanel ID="updModulos" runat="server">
                    <ContentTemplate>
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i><b>MODULOS -
                                    <asp:Label ID="lblCodus1_2" runat="server" Text=""></asp:Label></b>
                                <div class="card-header-actions">
                                    <button class="btn btn-primary" type="button" runat="server" id="btnGuardar" onserverclick="btnGuardar_Click">
                                        <i class="cil-save"></i>&nbsp;Guardar
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-2 mb-2 mt-1 mx-2 alert border" id="divInventarios" runat="server">
                                            <div class="pl-2">
                                                <div class="row">
                                                    <div class="col">
                                                        <asp:ImageButton ID="imgInventarios" Width="75" Height="75" runat="server" OnClick="imgModulos_Click" />
                                                    </div>
                                                    <div class="col">
                                                        <label class="c-switch c-switch-label c-switch-primary c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select2_onserverchange" enableviewstate="true" id="chkInventarios3">
                                                            <span class="c-switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>
                                                        </label>
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select1_onserverchange" enableviewstate="true" id="chkInventarios">
                                                            <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col"><b>1 - Inventarios </b></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 mb-2 mt-1 mx-2 alert border" id="divCompras" runat="server">
                                            <div class="pl-2">
                                                <div class="row">
                                                    <div class="col">
                                                        <asp:ImageButton ID="imgCompras" Width="75" Height="75" runat="server" OnClick="imgModulos_Click" />
                                                    </div>
                                                    <div class="col">
                                                        <label class="c-switch c-switch-label c-switch-primary c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select2_onserverchange" enableviewstate="true" id="chkCompras3">
                                                            <span class="c-switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>
                                                        </label>
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select1_onserverchange" enableviewstate="true" id="chkCompras">
                                                            <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col">
                                                        <b>2 - Compras</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 mb-2 mt-1 mx-2 alert border" id="divVentas" runat="server">
                                            <div class="pl-2">
                                                <div class="row">
                                                    <div class="col">
                                                        <asp:ImageButton ID="imgVentas" Width="75" Height="75" runat="server" OnClick="imgModulos_Click" />
                                                    </div>
                                                    <div class="col">
                                                        <label class="c-switch c-switch-label c-switch-primary c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select2_onserverchange" enableviewstate="true" id="chkVentas3">
                                                            <span class="c-switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>
                                                        </label>
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select1_onserverchange" enableviewstate="true" id="chkVentas">
                                                            <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col">
                                                        <b>3 - Ventas</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 mb-2 mt-1 mx-2 alert border" id="divContabilidad" runat="server">
                                            <div class="pl-2">
                                                <div class="row">
                                                    <div class="col">
                                                        <asp:ImageButton ID="imgContabilidad" Width="75" Height="75" runat="server" OnClick="imgModulos_Click" />
                                                    </div>
                                                    <div class="col">
                                                        <label class="c-switch c-switch-label c-switch-primary c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select2_onserverchange" enableviewstate="true" id="chkContabilidad3">
                                                            <span class="c-switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>
                                                        </label>
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select1_onserverchange" enableviewstate="true" id="chkContabilidad">
                                                            <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col">
                                                        <b>4 - Contabilidad</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 mb-2 mt-1 mx-2 alert border" id="divNomina" runat="server">
                                            <div class="pl-2">
                                                <div class="row">
                                                    <div class="col">
                                                        <asp:ImageButton ID="imgNomina" Width="75" Height="75" runat="server" OnClick="imgModulos_Click" />
                                                    </div>
                                                    <div class="col">
                                                        <label class="c-switch c-switch-label c-switch-primary c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select2_onserverchange" enableviewstate="true" id="chkNomina3">
                                                            <span class="c-switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>
                                                        </label>
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select1_onserverchange" enableviewstate="true" id="chkNomina">
                                                            <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col">
                                                        <b>5 - Nómina</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 mb-2 mt-1 mx-2 alert border" id="divActivosFijos" runat="server">
                                            <div class="pl-2">
                                                <div class="row">
                                                    <div class="col">
                                                        <asp:ImageButton ID="imgActivosFijos" Width="75" Height="75" runat="server" OnClick="imgModulos_Click" />
                                                    </div>
                                                    <div class="col">
                                                        <label class="c-switch c-switch-label c-switch-primary c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select2_onserverchange" enableviewstate="true" id="chkActivosFijos3">
                                                            <span class="c-switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>
                                                        </label>
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select1_onserverchange" enableviewstate="true" id="chkActivosFijos">
                                                            <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col">
                                                        <b>6 - Activos Fijos</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 mb-2 mt-1 mx-2 alert border" id="divImportaciones" runat="server">
                                            <div class="pl-2">
                                                <div class="row">
                                                    <div class="col">
                                                        <asp:ImageButton ID="imgImportaciones" Width="75" Height="75" runat="server" OnClick="imgModulos_Click" />
                                                    </div>
                                                    <div class="col">
                                                        <label class="c-switch c-switch-label c-switch-primary c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select2_onserverchange" enableviewstate="true" id="chkImportaciones3">
                                                            <span class="c-switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>
                                                        </label>
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select1_onserverchange" enableviewstate="true" id="chkImportaciones">
                                                            <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col">
                                                        <b>7 - Importaciones</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 mb-2 mt-1 mx-2 alert border" id="divProduccion" runat="server">
                                            <div class="pl-2">
                                                <div class="row">
                                                    <div class="col">
                                                        <asp:ImageButton ID="imgProduccion" Width="75" Height="75" runat="server" OnClick="imgModulos_Click" />
                                                    </div>
                                                    <div class="col">
                                                        <label class="c-switch c-switch-label c-switch-primary c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select2_onserverchange" enableviewstate="true" id="chkProduccion3">
                                                            <span class="c-switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>
                                                        </label>
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select1_onserverchange" enableviewstate="true" id="chkProduccion">
                                                            <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col">
                                                        <b>8 - Producción</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 mb-2 mt-1 mx-2 alert border" id="divConfiguracion" runat="server">
                                            <div class="pl-2">
                                                <div class="row">
                                                    <div class="col">
                                                        <asp:ImageButton ID="imgConfiguracion" Width="75" Height="75" runat="server" OnClick="imgModulos_Click" />
                                                    </div>
                                                    <div class="col">
                                                        <label class="c-switch c-switch-label c-switch-primary c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select2_onserverchange" enableviewstate="true" id="chkConfiguracion3">
                                                            <span class="c-switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>
                                                        </label>
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select1_onserverchange" enableviewstate="true" id="chkConfiguracion">
                                                            <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col">
                                                        <b>9 - Configuración</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2 mb-2 mt-1 mx-2 alert border" id="divSeguridades" runat="server">
                                            <div class="pl-2">
                                                <div class="row">
                                                    <div class="col">
                                                        <asp:ImageButton ID="imgSeguridades" Width="75" Height="75" runat="server" OnClick="imgModulos_Click" />
                                                    </div>
                                                    <div class="col">
                                                        <label class="c-switch c-switch-label c-switch-primary c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select2_onserverchange" enableviewstate="true" id="chkSeguridades3">
                                                            <span class="c-switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>
                                                        </label>
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success c-switch-sm ">
                                                            <input class="c-switch-input" type="checkbox" checked="" runat="server" onclick="javascript: form1.submit();" onserverchange="Select1_onserverchange" enableviewstate="true" id="chkSeguridades">
                                                            <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col">
                                                        <b>10 - Seguridades</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="row">
                    <div class="col-sm-12 col-xl-7">
                        <div class="fade-in">
                            <div class="card card-accent-primary">
                                <div class="card-header">Opciones del Menú</div>
                                <div class="card-body">
                                    <asp:UpdatePanel ID="updNiv" runat="server">
                                        <ContentTemplate>
                                            <div class="row">
                                                <div class="col-sm-12 col-xl-6">
                                                    <asp:ListBox ID="lbxNiv001" runat="server" Width="100%" Height="125px" DataSourceID="sqldsModuloNiv001" DataTextField="nom001" DataValueField="niv001" OnSelectedIndexChanged="lbxNiv001_SelectedIndexChanged" AutoPostBack="true" CssClass="list-group-item"></asp:ListBox>
                                                </div>
                                                <div class="col-sm-12 col-xl-6">
                                                    <asp:ListBox ID="lbxNiv002" runat="server" Width="100%" Height="125px" DataSourceID="sqldsModuloNiv002" DataTextField="nom002" DataValueField="niv002" OnSelectedIndexChanged="lbxNiv002_SelectedIndexChanged" AutoPostBack="true" CssClass="list-group-item"></asp:ListBox>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xl-5">
                        <asp:UpdatePanel ID="updBanderas" runat="server">
                            <ContentTemplate>
                                <div class="card card-accent-dark bg-light">
                                    <div class="card-header">
                                        Accesos opciones
                                                    <div class="card-header-actions">
                                                        <a class="card-header-action btn-setting" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                            <i class="c-icon cil-chevron-circle-down-alt"></i>
                                                        </a>
                                                    </div>
                                    </div>
                                    <div class="card-body">
                                        <%-- <div class="collapse" id="collapseExample">--%>
                                        <div class="row">
                                            <div class="col-6">
                                                <asp:CheckBox ID="chkMenvis" runat="server" Text=" Menú Visible" />
                                            </div>
                                            <div class="col-6">
                                                <asp:CheckBox ID="chkMenhab" runat="server" Text=" Menú Habilitado" />
                                            </div>
                                        </div>
                                        <%--  </div>--%>
                                    </div>
                                </div>
                                <div class="card card-accent-dark bg-light">
                                    <div class="card-header">
                                        Tipo Accesos opciones
                                                    <div class="card-header-actions">
                                                        <a class="card-header-action btn-setting" data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">
                                                            <i class="c-icon cil-chevron-circle-down-alt"></i>
                                                        </a>
                                                    </div>
                                    </div>
                                    <div class="card-body">
                                        <%-- <div class="collapse" id="collapseExample2">--%>
                                        <%--<div class="card card-body bg-light">--%>
                                        <div class="row">
                                            <div class="col-4">
                                                <asp:CheckBox ID="chkBanins" runat="server" Text="Nuevo" /><br />
                                                <asp:CheckBox ID="chkBancan" runat="server" Text="Cancelar" /><br />
                                                <asp:CheckBox ID="chkBaneli" runat="server" Text="Eliminar" />
                                            </div>
                                            <div class="col-4">
                                                <asp:CheckBox ID="chkBanbus" runat="server" Text="Abrir" /><br />
                                                <asp:CheckBox ID="chkBangra" runat="server" Text="Abrir y Grabar" /><br />
                                                <asp:CheckBox ID="chkBanpre" runat="server" Text="Presentación Preliminar" />
                                            </div>
                                            <div class="col-4">
                                                <asp:CheckBox ID="chkBanimp" runat="server" Text="Imprimir" /><br />
                                                <asp:CheckBox ID="chkBanotr" runat="server" Text="Otros Procesos" />
                                            </div>
                                        </div>
                                        <%--</div>--%>
                                        <%--</div>--%>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsModuloInicial" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT codmod, nommod, imgdes AS imagen FROM seg_modulo"></asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsModulo" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_USUARIO_MODULO" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="CODUS1" SessionField="gs_codus1Selected" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <!--i Listbox DATASOURCES-->
    <asp:SqlDataSource ID="sqldsModuloNiv001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codmod], [niv001], [nom001] FROM [seg_modulo_niv001] WHERE ([codmod] = @codmod)">
        <SelectParameters>
            <asp:SessionParameter Name="codmod" SessionField="gs_CodmodSelected" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsModuloNiv002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codmod], [niv001], [niv002], [nom002] FROM [seg_modulo_niv002] WHERE (([codmod] = @codmod) AND ([niv001] = @niv001))">
        <SelectParameters>
            <asp:SessionParameter Name="codmod" SessionField="gs_CodmodSelected" Type="Int32" />
            <asp:ControlParameter ControlID="lbxNiv001" Name="niv001" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <!--f Listbox DATASOURCES-->

    <!--f SQL DATASOURCES-->

    <!--i Scripts -->
    <%--        <script src="DataTables/datatables/jquery.dataTables.min.js"></script>
        <script src="DataTables/datatables/dataTables.bootstrap4.min.js"></script>--%>
    <!--f Scripts -->

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Errorn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>


</asp:Content>

