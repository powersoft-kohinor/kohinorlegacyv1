﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_seg_UsuarioModulo : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "10", niv001 = "2", niv002 = "3";
        protected cls_seg_Usuario_Modulo objUsuarioModulo = new cls_seg_Usuario_Modulo();
        protected cls_seg_Usuario_Menu objUsuarioMenu = new cls_seg_Usuario_Menu();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                f_Seg_Usuario_Menu();
                f_Seg_Usuario_Modulo();
                f_BloquearCheckbox();
                
                if (Session["gs_codus1Selected"] != null)
                {
                    if (!String.IsNullOrEmpty(Session["gs_codus1Selected"].ToString()))
                    {
                        lblCodus1.Text = Session["gs_codus1Selected"].ToString();
                        lblCodus1_2.Text = Session["gs_codus1Selected"].ToString();
                    }
                    else
                    {
                        Response.Redirect("w_seg_Usuario.aspx?gs_ErrorModulo=" + "❗ *02. " + DateTime.Now + " Vuelva a seleccionar el usuario para administrar sus módulos.");
                    }
                }
                else
                {
                    Response.Redirect("w_seg_Usuario.aspx?gs_ErrorModulo=" + "❗ *02. " + DateTime.Now + " Vuelva a seleccionar el usuario para administrar sus módulos.");
                }
            }
        }

        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                //if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente
                //{
                //    if (row["banins"].Equals("N")) //Nuevo
                //    {
                //        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnNuevo.Disabled = true;
                //    }
                //    if (row["banbus"].Equals("N")) //Abrir
                //    {
                //        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnAbrir.Disabled = true;
                //    }
                //    if (row["bangra"].Equals("N")) //Guardar
                //    {
                //        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnGuardar.Disabled = true;
                //    }
                //    if (row["bancan"].Equals("N")) //Cancelar
                //    {
                //        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnCancelar.Disabled = true;

                //    }
                //    if (row["baneli"].Equals("N")) //Eliminiar
                //    {
                //        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                //        btnEliminar.Disabled = true;
                //        Session["baneli"] = "N";
                //    }
                //}
            }
            return dt;
        }

        protected void f_Seg_Usuario_Modulo()
        {
            try
            {
                DataSourceSelectArguments args = new DataSourceSelectArguments();
                DataView view = (DataView)sqldsModuloInicial.Select(args); //procedimiento almacenado
                DataTable dtInicial = view.ToTable(); //contiene todos los modulos con imgdes

                view = (DataView)sqldsModulo.Select(args); //procedimiento almacenado
                DataTable dt = view.ToTable(); //contiene solo los modulos del codus1selected

                int rowindex = 0;
                foreach (DataRow row in dt.Rows)
                {
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow row2 in dtInicial.Rows)
                            {
                                if (row["codmod"].Equals(row2["codmod"]))
                                {
                                    row2["imagen"] = row["imagen"];
                                }
                            }                            
                        }
                    }
                    rowindex++;
                }
                dt = dtInicial;

                foreach (DataRow row in dt.Rows)
                {
                    string s_imagen = row["imagen"].ToString().Trim();
                    int i_codmod = dt.Rows.IndexOf(row) + 1;
                    ImageButton imgModulo = new ImageButton();
                    imgModulo.ImageUrl = clsGlobalVariablesClass.ImageDirectory + s_imagen;
                    if (s_imagen.Equals("3")) //si retorna 3
                    {
                        imgModulo.Visible = false;
                        imgModulo.Enabled = false;
                    }
                    else
                    {
                        if (s_imagen.Equals("d" + (dt.Rows.IndexOf(row) + 1) + ".png"))
                        {
                            imgModulo.Enabled = false;
                        }
                    }

                    switch (i_codmod)
                    {
                        case 1:
                            imgInventarios.ImageUrl = imgModulo.ImageUrl;
                            imgInventarios.Enabled = imgModulo.Enabled;
                            imgInventarios.Visible = imgModulo.Visible;
                            imgInventarios.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            //divInventarios.Visible = imgModulo.Visible;
                            chkInventarios3.Checked = imgModulo.Visible; //si devolvio 3 desctiva el chkInventarios3, el chkInventarios y no muestra imagen
                            chkInventarios.Checked = imgModulo.Enabled;

                            break;
                        case 2:
                            imgCompras.ImageUrl = imgModulo.ImageUrl;
                            imgCompras.Enabled = imgModulo.Enabled;
                            imgCompras.Visible = imgModulo.Visible;
                            imgCompras.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            //divCompras.Visible = imgModulo.Visible;
                            chkCompras3.Checked = imgModulo.Visible;
                            chkCompras.Checked = imgModulo.Enabled;
                            break;
                        case 3:
                            imgVentas.ImageUrl = imgModulo.ImageUrl;
                            imgVentas.Enabled = imgModulo.Enabled;
                            imgVentas.Visible = imgModulo.Visible;
                            imgVentas.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            //divVentas.Visible = imgModulo.Visible;
                            chkVentas3.Checked = imgModulo.Visible;
                            chkVentas.Checked = imgModulo.Enabled;
                            break;
                        case 4:
                            imgContabilidad.ImageUrl = imgModulo.ImageUrl;
                            imgContabilidad.Enabled = imgModulo.Enabled;
                            imgContabilidad.Visible = imgModulo.Visible;
                            imgContabilidad.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            //divContabilidad.Visible = imgModulo.Visible;
                            chkContabilidad3.Checked = imgModulo.Visible;
                            chkContabilidad.Checked = imgModulo.Enabled;
                            break;
                        case 5:
                            imgNomina.ImageUrl = imgModulo.ImageUrl;
                            imgNomina.Enabled = imgModulo.Enabled;
                            imgNomina.Visible = imgModulo.Visible;
                            imgNomina.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            //divNomina.Visible = imgModulo.Visible;
                            chkNomina3.Checked = imgModulo.Visible;
                            chkNomina.Checked = imgModulo.Enabled;
                            break;
                        case 6:
                            imgActivosFijos.ImageUrl = imgModulo.ImageUrl;
                            imgActivosFijos.Enabled = imgModulo.Enabled;
                            imgActivosFijos.Visible = imgModulo.Visible;
                            imgActivosFijos.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            //divActivosFijos.Visible = imgModulo.Visible;
                            chkActivosFijos3.Checked = imgModulo.Visible;
                            chkActivosFijos.Checked = imgModulo.Enabled;
                            break;
                        case 7:
                            imgImportaciones.ImageUrl = imgModulo.ImageUrl;
                            imgImportaciones.Enabled = imgModulo.Enabled;
                            imgImportaciones.Visible = imgModulo.Visible;
                            imgImportaciones.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            //divImportaciones.Visible = imgModulo.Visible;
                            chkImportaciones3.Checked = imgModulo.Visible;
                            chkImportaciones.Checked = imgModulo.Enabled;
                            break;
                        case 8:
                            imgProduccion.ImageUrl = imgModulo.ImageUrl;
                            imgProduccion.Enabled = imgModulo.Enabled;
                            imgProduccion.Visible = imgModulo.Visible;
                            imgProduccion.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            //divProduccion.Visible = imgModulo.Visible;
                            chkProduccion3.Checked = imgModulo.Visible;
                            chkProduccion.Checked = imgModulo.Enabled;
                            break;
                        case 9:
                            imgConfiguracion.ImageUrl = imgModulo.ImageUrl;
                            imgConfiguracion.Enabled = imgModulo.Enabled;
                            imgConfiguracion.Visible = imgModulo.Visible;
                            imgConfiguracion.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            //divInventarios.Visible = imgModulo.Visible;
                            chkConfiguracion3.Checked = imgModulo.Visible;
                            chkConfiguracion.Checked = imgModulo.Enabled;
                            break;
                        case 10:
                            imgSeguridades.ImageUrl = imgModulo.ImageUrl;
                            imgSeguridades.Enabled = imgModulo.Enabled;
                            imgSeguridades.Visible = imgModulo.Visible;
                            imgSeguridades.CommandArgument = row["codmod"].ToString().Trim() + "@" + row["nommod"].ToString().Trim();
                            //divInventarios.Visible = imgModulo.Visible;
                            chkSeguridades.Checked = imgModulo.Visible;
                            chkSeguridades.Checked = imgModulo.Enabled;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = "❗ *01. " + ex.Message;
            }
        }

        protected void imgModulos_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imgBtn = (ImageButton)sender;

            string[] s_modulo = imgBtn.CommandArgument.Split('@');
            string s_codmod = s_modulo[0];
            //string s_nommod = s_modulo[1];
            Session.Add("gs_CodmodSelected", s_codmod);
            lbxNiv002.Items.Clear();
            f_BloquearCheckbox();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            objUsuarioMenu.Codemp = Session["gs_CodEmp"].ToString();
            objUsuarioMenu.Codus1 = Session["gs_codus1Selected"].ToString();
            objUsuarioMenu.Codmod = int.Parse(Session["gs_CodmodSelected"].ToString());
            objUsuarioMenu.Niv001 = Session["gs_Niv001Selected"].ToString();
            objUsuarioMenu.Niv002 = Session["gs_Niv002Selected"].ToString();
            objUsuarioMenu.Banins = chkBanins.Checked ? "Y" : "N";
            objUsuarioMenu.Bancan = chkBancan.Checked ? "Y" : "N";
            objUsuarioMenu.Baneli = chkBaneli.Checked ? "Y" : "N";
            objUsuarioMenu.Banbus = chkBanbus.Checked ? "Y" : "N";
            objUsuarioMenu.Banpre = chkBanpre.Checked ? "Y" : "N";
            objUsuarioMenu.Banimp = chkBanimp.Checked ? "Y" : "N";
            objUsuarioMenu.Banotr = chkBanotr.Checked ? "Y" : "N";
            objUsuarioMenu.Bangra = chkBangra.Checked ? "Y" : "N";
            objUsuarioMenu.Menvis = chkMenvis.Checked ? "Y" : "N";
            objUsuarioMenu.Menhab = chkMenhab.Checked ? "Y" : "N";
            clsError objError = objUsuarioMenu.f_UsuarioMenu_Actualizar(objUsuarioMenu, Session["gs_UsuIng"].ToString(), "Update");

            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblError.Text = "";
                lblExito.Text = "✔️ " + DateTime.Now + " Guardado Exitoso.";
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }        

        public void Select1_onserverchange(object sender, EventArgs e)
        {
            objUsuarioModulo.Codemp = Session["gs_CodEmp"].ToString();
            objUsuarioModulo.Codus1 = Session["gs_codus1Selected"].ToString();
            HtmlInputCheckBox chk = (HtmlInputCheckBox)sender;
            switch (chk.ID)
            {
                case "chkInventarios": 
                    objUsuarioModulo.Codmod = 1;
                    if (chk.Checked == true) chkInventarios3.Checked = true;
                    break;
                case "chkCompras":
                    objUsuarioModulo.Codmod = 2;
                    if (chk.Checked == true) chkCompras3.Checked = true;
                    break;
                case "chkVentas":
                    objUsuarioModulo.Codmod = 3;
                    if (chk.Checked == true) chkVentas3.Checked = true;
                    break;
                case "chkContabilidad":
                    objUsuarioModulo.Codmod = 4;
                    if (chk.Checked == true) chkContabilidad3.Checked = true;
                    break;
                case "chkNomina":
                    objUsuarioModulo.Codmod = 5;
                    if (chk.Checked == true) chkNomina3.Checked = true;
                    break;
                case "chkActivosFijos":
                    objUsuarioModulo.Codmod = 6;
                    if (chk.Checked == true) chkActivosFijos3.Checked = true;
                    break;
                case "chkImportaciones":
                    objUsuarioModulo.Codmod = 7;
                    if (chk.Checked == true) chkImportaciones3.Checked = true;
                    break;
                case "chkProduccion":
                    objUsuarioModulo.Codmod = 8;
                    if (chk.Checked == true) chkProduccion3.Checked = true;
                    break;
                case "chkConfiguracion":
                    objUsuarioModulo.Codmod = 9;
                    if(chk.Checked == true) chkConfiguracion3.Checked = true;
                    break;
                case "chkSeguridades":
                    objUsuarioModulo.Codmod = 10;
                    if(chk.Checked == true) chkSeguridades3.Checked = true;
                    break;
            }

            if (chk.Checked == true)
                objUsuarioModulo.Estmod = "1";              
            else
                objUsuarioModulo.Estmod = "2";

            f_ActualizarUsuarioModulo(objUsuarioModulo);
        }

        public void Select2_onserverchange(object sender, EventArgs e)
        {
            objUsuarioModulo.Codemp = Session["gs_CodEmp"].ToString();
            objUsuarioModulo.Codus1 = Session["gs_codus1Selected"].ToString();
            HtmlInputCheckBox chk = (HtmlInputCheckBox)sender;
            switch (chk.ID)
            {
                case "chkInventarios3":
                    objUsuarioModulo.Codmod = 1;
                    if (chk.Checked == false) chkInventarios.Checked = false;
                    break;
                case "chkCompras3":
                    objUsuarioModulo.Codmod = 2;
                    if (chk.Checked == false) chkCompras.Checked = false;
                    break;
                case "chkVentas3":
                    objUsuarioModulo.Codmod = 3;
                    if (chk.Checked == false) chkVentas.Checked = false;
                    break;
                case "chkContabilidad3":
                    objUsuarioModulo.Codmod = 4;
                    if (chk.Checked == false) chkContabilidad.Checked = false;
                    break;
                case "chkNomina3":
                    objUsuarioModulo.Codmod = 5;
                    if (chk.Checked == false) chkNomina.Checked = false;
                    break;
                case "chkActivosFijos3":
                    objUsuarioModulo.Codmod = 6;
                    if (chk.Checked == false) chkActivosFijos.Checked = false;
                    break;
                case "chkImportaciones3":
                    objUsuarioModulo.Codmod = 7;
                    if (chk.Checked == false) chkImportaciones.Checked = false;
                    break;
                case "chkProduccion3":
                    objUsuarioModulo.Codmod = 8;
                    if (chk.Checked == false) chkProduccion.Checked = false;
                    break;
                case "chkConfiguracion3":
                    objUsuarioModulo.Codmod = 9;
                    if (chk.Checked == false) chkConfiguracion.Checked = false;
                    break;
                case "chkSeguridades3":
                    objUsuarioModulo.Codmod = 10;
                    if (chk.Checked == false) chkSeguridades.Checked = false;
                    break;
            }

            if (chk.Checked == true) //invisible (3) cuando se desactiva el chk, si lo activa le pone como deshabilitado (2)
            {
                objUsuarioModulo.Estmod = "2";
            }
            else
            {
                objUsuarioModulo.Estmod = "3";
            }
                
            f_ActualizarUsuarioModulo(objUsuarioModulo);
        }

        protected void f_ActualizarUsuarioModulo(cls_seg_Usuario_Modulo objUsuarioModulo)
        {
            clsError objError = new clsError();
            cls_seg_Usuario_Modulo objUsuarioModulo2 = objUsuarioModulo.f_UsuarioModulo_Buscar(Session["gs_CodEmp"].ToString(), Session["gs_codus1Selected"].ToString(), objUsuarioModulo.Codmod.ToString());

            if (String.IsNullOrEmpty(objUsuarioModulo2.Error.Mensaje))
            {
                if (String.IsNullOrEmpty(objUsuarioModulo2.Codus1)) //INSERTA
                {
                    objError = objUsuarioModulo.f_UsuarioModulo_Actualizar(objUsuarioModulo, Session["gs_UsuIng"].ToString(), "Add");
                }
                else //ACTUALIZA
                {
                    objError = objUsuarioModulo.f_UsuarioModulo_Actualizar(objUsuarioModulo, Session["gs_UsuIng"].ToString(), "Update");
                }

                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    lblError.Text = "";
                    lblExito.Text = "✔️ " + DateTime.Now + " Guardado Exitoso.";
                }
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }

                f_Seg_Usuario_Modulo();
            }
            else
            {
                f_ErrorNuevo(objUsuarioModulo.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void lbxNiv001_SelectedIndexChanged(object sender, EventArgs e)
        {
            f_DesbloquearCheckbox();
            Session.Add("gs_Niv001Selected", lbxNiv001.SelectedValue);
            Session.Add("gs_Niv002Selected", "0");
            f_llenarCheckbox(Session["gs_CodEmp"].ToString(), Session["gs_CodmodSelected"].ToString(), Session["gs_codus1Selected"].ToString(), Session["gs_Niv001Selected"].ToString(), Session["gs_Niv002Selected"].ToString());
        }

        protected void lbxNiv002_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session.Add("gs_Niv002Selected", lbxNiv002.SelectedValue);
            f_llenarCheckbox(Session["gs_CodEmp"].ToString(), Session["gs_CodmodSelected"].ToString(), Session["gs_codus1Selected"].ToString(), Session["gs_Niv001Selected"].ToString(), lbxNiv002.SelectedValue);
        }

        protected void f_BloquearCheckbox()
        {
            chkMenvis.Enabled = chkMenhab.Enabled = chkBanins.Enabled = chkBancan.Enabled = chkBaneli.Enabled = chkBanbus.Enabled =
                chkBangra.Enabled = chkBanpre.Enabled = chkBanimp.Enabled = chkBanotr.Enabled = false;
            //btnGuardar.Attributes.Add("disabled", "disabled");
            //btnGuardar.Attributes.Add("class", "btn btn-primary disabled");
            btnGuardar.Disabled = true;
        }

        protected void f_DesbloquearCheckbox()
        {
            chkMenvis.Enabled = chkMenhab.Enabled = chkBanins.Enabled = chkBancan.Enabled = chkBaneli.Enabled = chkBanbus.Enabled =
                chkBangra.Enabled = chkBanpre.Enabled = chkBanimp.Enabled = chkBanotr.Enabled = true;
            //btnGuardar.Attributes.Remove("disabled");
            btnGuardar.Attributes.Add("class", "btn btn-primary");
            btnGuardar.Disabled = false;
        }

        protected void f_llenarCheckbox(string gs_Codemp, string s_codmod, string s_codus1, string s_niv001, string s_niv002)
        {
            objUsuarioMenu = objUsuarioMenu.f_UsuarioMenu_Buscar(gs_Codemp, s_codmod, s_codus1, s_niv001, s_niv002);
            if (String.IsNullOrEmpty(objUsuarioMenu.Error.Mensaje))
            {
                DataTable dt = objUsuarioMenu.dtUsuarioMenu;
                //Session.Add("gdt_UsuarioMenu", dt);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0) //si existe el registro en la tbl llena los chk directamente
                    {
                        chkMenvis.Checked = dt.Rows[0]["menvis"].Equals("Y") ? true : false;
                        chkMenhab.Checked = dt.Rows[0]["menhab"].Equals("Y") ? true : false;
                        chkBanins.Checked = dt.Rows[0]["banins"].Equals("Y") ? true : false;
                        chkBancan.Checked = dt.Rows[0]["bancan"].Equals("Y") ? true : false;
                        chkBaneli.Checked = dt.Rows[0]["baneli"].Equals("Y") ? true : false;
                        chkBanbus.Checked = dt.Rows[0]["banbus"].Equals("Y") ? true : false;
                        chkBangra.Checked = dt.Rows[0]["bangra"].Equals("Y") ? true : false;
                        chkBanpre.Checked = dt.Rows[0]["banpre"].Equals("Y") ? true : false;
                        chkBanimp.Checked = dt.Rows[0]["banimp"].Equals("Y") ? true : false;
                        chkBanotr.Checked = dt.Rows[0]["banotr"].Equals("Y") ? true : false;
                    }
                    else //si no existe el registro en la tbl lo inserta y luego llena los chk
                    {
                        objUsuarioMenu.Codemp = gs_Codemp;
                        objUsuarioMenu.Codus1 = Session["gs_codus1Selected"].ToString();
                        objUsuarioMenu.Codmod = int.Parse(s_codmod);
                        objUsuarioMenu.Niv001 = s_niv001;
                        objUsuarioMenu.Niv002 = s_niv002;
                        objUsuarioMenu.Banins = "N";
                        objUsuarioMenu.Bancan = "N";
                        objUsuarioMenu.Baneli = "N";
                        objUsuarioMenu.Banbus = "N";
                        objUsuarioMenu.Banpre = "N";
                        objUsuarioMenu.Banimp = "N";
                        objUsuarioMenu.Banotr = "N";
                        objUsuarioMenu.Bangra = "N";
                        objUsuarioMenu.Menvis = "N";
                        objUsuarioMenu.Menhab = "N";
                        clsError objError = objUsuarioMenu.f_UsuarioMenu_Actualizar(objUsuarioMenu, Session["gs_UsuIng"].ToString(), "Add");

                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                            lblError.Text = "";
                            lblExito.Text = "✔️ Se creo registro: " + s_codmod + " >> " + s_niv001 + " > " + s_niv002 + " del usuario - " + s_codus1;

                            objUsuarioMenu = objUsuarioMenu.f_UsuarioMenu_Buscar(gs_Codemp, s_codmod, s_codus1, s_niv001, s_niv002);
                            if (String.IsNullOrEmpty(objUsuarioMenu.Error.Mensaje))
                            {
                                dt = objUsuarioMenu.dtUsuarioMenu;
                                chkMenvis.Checked = dt.Rows[0]["menvis"].Equals("Y") ? true : false;
                                chkMenhab.Checked = dt.Rows[0]["menhab"].Equals("Y") ? true : false;
                                chkBanins.Checked = dt.Rows[0]["banins"].Equals("Y") ? true : false;
                                chkBancan.Checked = dt.Rows[0]["bancan"].Equals("Y") ? true : false;
                                chkBaneli.Checked = dt.Rows[0]["baneli"].Equals("Y") ? true : false;
                                chkBanbus.Checked = dt.Rows[0]["banbus"].Equals("Y") ? true : false;
                                chkBangra.Checked = dt.Rows[0]["bangra"].Equals("Y") ? true : false;
                                chkBanpre.Checked = dt.Rows[0]["banpre"].Equals("Y") ? true : false;
                                chkBanimp.Checked = dt.Rows[0]["banimp"].Equals("Y") ? true : false;
                                chkBanotr.Checked = dt.Rows[0]["banotr"].Equals("Y") ? true : false;
                            }
                            else
                            {
                                f_ErrorNuevo(objError);
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                            }
                        }
                        else
                        {
                            f_ErrorNuevo(objError);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                }
            }
            else
            {
                f_ErrorNuevo(objUsuarioMenu.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
    }
}