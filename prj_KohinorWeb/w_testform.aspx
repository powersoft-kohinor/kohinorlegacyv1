﻿<%@ Page Title="" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_testform.aspx.cs" Inherits="prj_KohinorWeb.w_testform" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .skills {
            width: 100%;
            max-width: 600px;
            padding: 0 20px;
        }

        .skill-name {
            font-size: 18px;
            font-weight: 700;
            text-transform: uppercase;
            margin: 20px 0;
        }

        .skill-bar {
            height: 20px;
            background: #cacaca;
            border-radius: 8px;
        }

        .skill-per {
            height: 20px;
            background-color: #0fbcf9;
            border-radius: 8px;
            width: 0;
            transition: 1s linear;
            position: relative;
        }

            .skill-per::before {
                content: attr(per);
                position: absolute;
                padding: 4px 6px;
                background-color: #000;
                color: #fff;
                font-size: 12px;
                border-radius: 4px;
                top: -35px;
                right: 0;
                transform: translateX(50%);
            }

            .skill-per::after {
                content: '';
                position: absolute;
                width: 10px;
                height: 10px;
                background-color: #000;
                top: -16px;
                right: 0;
                transform: translateX(50%) rotate(45deg);
                border-radius: 2px;
            }
    </style>
    <script type="text/javascript">
        function f_ModalXML() {
            $("#btnShowModalXML").click();
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSidebar" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBodyHeader" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="card">
                    <a class="dropdown-item" data-toggle="modal" data-target="#ModalXML">
                        <i class="c-icon cil-account-logout"></i>&nbsp;
                                MODAL XML
                    </a>


                    

                    
                </div>
            </div>
        </div>
    </main>

    <div class="skills">
                        <div class="skill">
                            <div class="skill-name">HTML</div>
                            <div class="skill-bar">
                                <div class="skill-per" per="100"></div>
                            </div>
                        </div>

                        <div class="skill">
                            <div class="skill-name">CSS</div>
                            <div class="skill-bar">
                                <div class="skill-per" per="70"></div>
                            </div>
                        </div>

                        <div class="skill">
                            <div class="skill-name">Javascript</div>
                            <div class="skill-bar">
                                <div class="skill-per" per="60"></div>
                            </div>
                        </div>
                    </div>

                    <script>
                        $('.skill-per').each(function () {
                            var $this = $(this);
                            var per = $this.attr('per');
                            $this.css("width", per + '%');
                            $({ animatedValue: 0 }).animate({ animatedValue: per }, {
                                duration: 1000,
                                step: function () {
                                    $this.attr('per', Math.floor(this.animatedValue) + '%');
                                },
                                complete: function () {
                                    $this.attr('per', Math.floor(this.animatedValue) + '%');
                                }
                            });
                        });
                    </script>

    <!-- Modal XML Load -->
    <%--<button id="btnShowModalXML" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#ModalXML"></button>--%>
    <div class="modal fade" tabindex="-1" role="dialog" id="ModalXML">
        <div class="modal-dialog modal-dark" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
