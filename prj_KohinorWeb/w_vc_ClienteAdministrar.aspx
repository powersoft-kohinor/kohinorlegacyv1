﻿<%@ Page Title="Cliente Administrar" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_vc_ClienteAdministrar.aspx.cs" Inherits="prj_KohinorWeb.w_vc_ClienteAdministrar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtCodcli.ClientID%>").blur(function () {
                f_Validar();
            });
        });


    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtNombre1.ClientID%>").blur(function () {
                f_NombreCompleto();
            });

            $("#<%=txtNombre2.ClientID%>").blur(function () {
                f_NombreCompleto();
            });

            $("#<%=txtApellido1.ClientID%>").blur(function () {
                f_NombreCompleto();
            });

            $("#<%=txtApellido2.ClientID%>").blur(function () {
                f_NombreCompleto();
            });
        });


    </script>

    <script type="text/javascript"> 
        function f_Validar() {
            var s_cod = $("#<%=txtCodcli.ClientID%>").val();
            PageMethods.f_Validar(s_cod, f_SuccessValidacionCod, f_FailValidacionCod);
        }
        function f_SuccessValidacionCod(data) {
            if (data.TipInd == "-" || data.TipEmp == "-") {
                //alert("Solo se puede ingresar RUC = 13 Digitos, CI=10 Digitos, PASAPORTE=9 Digitos");
                alert(data.ErrorCed);
                $("#<%=txtTipind.ClientID%>").val("-");
                $("#<%=txtTipemp.ClientID%>").val("-");
                $("#<%=txtRucced.ClientID%>").val("-");
            }
            else {
                $("#<%=txtTipind.ClientID%>").val(data.TipInd);
                $("#<%=txtTipemp.ClientID%>").val(data.TipEmp);
                $("#<%=txtRucced.ClientID%>").val(data.RucCed);
            }
        }
        function f_FailValidacionCod() {
            alert("ERROR : Método getValues_Success(data) fallo.");
        }



        function f_NombreCompleto() {
            var s_nombre1 = $("#<%=txtNombre1.ClientID%>").val();
            var s_nombre2 = $("#<%=txtNombre2.ClientID%>").val();
            var s_apellido1 = $("#<%=txtApellido1.ClientID%>").val();
            var s_apellido2 = $("#<%=txtApellido2.ClientID%>").val();

            PageMethods.f_UnirNombreCompleto(s_nombre1, s_nombre2, s_apellido1, s_apellido2, f_SucessNombreCompleto, f_FailNombreCompleto);

        }
        function f_SucessNombreCompleto(data) {
            $("#<%=txtNombreCompleto.ClientID%>").val(data);


        }
        function f_FailNombreCompleto(data) {
            $("#<%=txtNombreCompleto.ClientID%>").val("-");

        }


        function f_vistaPreviaImg() {
            var preview = document.querySelector('#<%=imgFotoCliente.ClientID %>');
            var file = document.querySelector('#<%=imgCliente.ClientID %>').files[0];
            var reader = new FileReader();

            reader.onloadend = function () {
                preview.src = reader.result;
            }

            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        }
    </script>

</asp:Content>
<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item"><a href="w_vc_Cliente.aspx">Clientes</a></li>
            <li class="breadcrumb-item active">Administrar Cliente</li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <a class="c-subheader-nav-link">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary" type="button" id="">
                            <i class="cil-filter"></i>
                        </button>
                    </div>

                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
                </div>
                <asp:LinkButton ID="lkbtnSearch" runat="server" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
            </a>

            <%--<a class="c-subheader-nav-link">
                <i class="c-icon cil-speech"></i>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-graph"></i>&nbsp;
                            Escritorio
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-settings"></i>
            </a>--%>
        </div>
    </div>
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item active">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click"><i class="cil-file"></i>Nuevo</button>
                    <button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" onserverclick="btnAbrir_Click"><i class="cil-folder-open"></i>Abrir</button>
                    <button type="button" class="btn btn-outline-dark" id="btnGuardar" runat="server" onserverclick="btnGuardarCliente_Click"><i class="cil-save"></i>Guardar</button>
                    <button type="button" class="btn btn-outline-dark" id="btnCancelar" runat="server" onserverclick="btnCancelar_Click"><i class="cil-grid-slash"></i>Cancelar</button>
                    <button type="button" class="btn btn-outline-danger" id="btnEliminar" runat="server" onserverclick="lkbtnDelete_Click"><i class="cil-trash"></i>Eliminar</button>
                </div>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-dark" id="lkbtnPrincipio" runat="server" onserverclick="lkbtnPrincipio_Click"><i class="cil-media-step-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnAtras" runat="server" onserverclick="lkbtnAtras_Click"><i class="cil-media-skip-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnSiguiente" runat="server" onserverclick="lkbtnSiguiente_Click"><i class="cil-media-skip-forward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnFinal" runat="server" onserverclick="lkbtnFinal_Click"><i class="cil-media-step-forward"></i></button>
                </div>

                <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>

            </li>
        </ol>

    </div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <!-- Begin Page Content -->
                <div id="frmVerificar" class="needs-validation" runat="server" novalidate>
                    <!-- Page Heading -->
                    <div class="card text-white bg-secondary text-black-50">
                        <div class="card-body">

                            <div class="form-row">
                                <div class="col-md-2">
                                    <h2><strong>Gestión Clientes</strong></h2>
                                </div>

                                <div class="form-group col-md-2">
                                    <input type="text" id="txtCodcli" class="form-control" runat="server" required />
                                    <label for="txtCodcli">Código</label>
                                </div>
                                <div class="form-group col-md-3">
                                    <asp:TextBox ID="txtTipemp" runat="server" class="form-control" placeholder="Empresa"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-2">
                                    <asp:TextBox ID="txtTipind" runat="server" class="form-control" placeholder="Indentificación"></asp:TextBox>

                                </div>

                                <div class="form-group col-md-3">
                                    <asp:TextBox ID="txtRucced" runat="server" class="form-control" placeholder="Número"></asp:TextBox>
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-md-2 align-items-center justify-content-center">
                                    <div class="form-row align-items-center justify-content-center">
                                        <asp:Image ID="imgFotoCliente" class="img-thumbnail" runat="server" Width="110px" Height="140px" />
                                        <div class="input-group mb-3 mt-3">
                                            <div class="custom-file">
                                                <input id="imgCliente" type="file" class="custom-file-input" onchange="f_vistaPreviaImg()" runat="server">
                                                <label class="custom-file-label" for="imgCliente" aria-describedby="inputGroupFileAddon02">Foto</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <input type="text" id="txtApellido1" class="form-control" runat="server" ontextchanged="txtApellido1_TextChanged" required />
                                            <label for="txtApellido1">Apellidos</label>
                                            <div class="valid-feedback">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="text" id="txtApellido2" class="form-control" runat="server" required />
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="text" id="txtNombre1" class="form-control" runat="server" required />
                                            <label for="txtNombre1">Nombres</label>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="text" id="txtNombre2" class="form-control" runat="server" required />
                                        </div>

                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-9">
                                            <input type="text" id="txtNombreCompleto" class="form-control" runat="server" />
                                            <label for="txtNombreCompleto">Nombres Completos </label>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <input type="date" id="txtFecnac" class="form-control" runat="server" required />
                                            <label for="txtFecnacE">Fecha Nacimiento</label>
                                        </div>


                                    </div>
                                    <div class="form-row">

                                        <div class="form-group col-md-2">
                                            <asp:DropDownList ID="ddlEstadoCivil" runat="server" class="form-control" Width="150px" required>
                                                <asp:ListItem Value="1">Casado</asp:ListItem>
                                                <asp:ListItem Value="2">Soltero</asp:ListItem>
                                                <asp:ListItem Value="3">Divorciado</asp:ListItem>
                                                <asp:ListItem Value="4">Viudo</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-secondary active">
                                                    <input type="radio" name="options" runat="server" value="1" id="rbtMasculino" autocomplete="off">
                                                    Masculino
                                                </label>
                                                <label class="btn btn-secondary">
                                                    <input type="radio" name="options" runat="server" value="2" id="rbtFemenino" autocomplete="off">
                                                    Femenino
                                                </label>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="card shadow ">
                        <div class="card-body">
                            <h5>Datos de Contacto </h5>
                            <div class="form-row">
                                <div class="form-group col-md-10">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Dirección</strong></span>
                                        </div>
                                        <input id="txtCalle0" runat="server" class="form-control" type="text" required />
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>N°</strong></span>
                                        </div>
                                        <input type="text" id="txtNo" class="form-control" runat="server" required />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Provincia</strong></span>
                                        </div>
                                        <input type="text" id="txtProvincia" class="form-control" runat="server" required />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Ciudad</strong></span>
                                        </div>
                                        <input type="text" id="txtCiudad" class="form-control" runat="server" required />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Parroquia</strong></span>
                                        </div>
                                        <input type="text" id="txtParroquia" class="form-control" runat="server" required />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Nacionalidad</strong></span>
                                        </div>
                                        <input type="text" id="txtNaccli" class="form-control" runat="server" required />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Celular</strong></span>
                                        </div>
                                        <input id="txtTelCel" pattern="[0-9]{10}" runat="server" class="form-control" type="text" required />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Telf.Casa</strong></span>
                                        </div>
                                        <input id="txtTelCasa" pattern="[0-9]{10}" runat="server" class="form-control" type="text" required />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Telf.Trabajo</strong></span>
                                        </div>
                                        <input id="txtTelPrin" pattern="[0-9]{10}" runat="server" class="form-control" type="text" required />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Email</strong></span>
                                        </div>
                                        <input type="email" id="txtEmail" class="form-control" runat="server" required />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Clase</strong></span>
                                        </div>
                                        <asp:DropDownList ID="ddlCodcla" runat="server" class="form-control" DataSourceID="sqldsClaseCliente" DataTextField="nomcla" DataValueField="codcla"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Vendedor</strong></span>
                                        </div>
                                        <asp:DropDownList ID="ddlCodven" runat="server" class="form-control" required DataSourceID="sqldsVendedor" DataTextField="nomven" DataValueField="codven"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Lista</strong></span>
                                        </div>
                                        <asp:DropDownList ID="ddlLispre" runat="server" class="form-control" required>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                            <asp:ListItem Value="6">6</asp:ListItem>
                                            <asp:ListItem Value="7">7</asp:ListItem>
                                            <asp:ListItem Value="8">8</asp:ListItem>
                                            <asp:ListItem Value="9">9</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <button id="btnGuardarCliente" type="submit" value="button" runat="server" class="btn btn-primary float-right" onserverclick="btnGuardarCliente_Click"><i class="cil-save"></i>Guardar</button>
                        </div>

                    </div>
                    <!-- Crear Nuevo Cliente -->
                </div>
            </div>

            <!-- /.container-fluid -->
            <!-- End of Main Content -->

        </div>
    </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsClaseCliente" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codcla])) AS codcla, [nomcla] FROM [vc_cliente_clase] "></asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsVendedor" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codven])) AS codven, [nomven] FROM [vc_vendedor]"></asp:SqlDataSource>

    <!--f SQL DATASOURCES-->


    <!-- Modal Eliminar -->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Cliente</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar al cliente '<asp:Label ID="lblEliCli" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarCliente_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>
</asp:Content>
