﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_vc_ClienteAdministrar : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "3", niv001 = "2", niv002 = "14";
        protected cls_vc_Cliente objCliente = new cls_vc_Cliente();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {       
                Session.Add("gs_Siguiente", ""); //para el lkbtnSiguiente Y lkbtnAtras
                f_Seg_Usuario_Menu();

                if (String.IsNullOrEmpty(Session["gs_codcliSelected"].ToString()))
                    Session.Add("gs_Action", "Add");
                else
                    Session.Add("gs_Action", "Update");

                ddlCodcla.DataBind(); //OJO DEBE IR ANTES DE f_llenarCamposEditar
                ddlCodven.DataBind(); //OJO DEBE IR ANTES DE f_llenarCamposEditar
                objCliente = objCliente.f_Cliente_Buscar(Session["gs_Codemp"].ToString(), Session["gs_codcliSelected"].ToString());
                if (String.IsNullOrEmpty(objCliente.Error.Mensaje))
                {
                    f_llenarCamposEditar(objCliente);
                }
                else
                {
                    f_ErrorNuevo(objCliente.Error);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            if (IsPostBack)
            {
                lblExito.Text = "";
                lblError.Text = "";
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAbrir.Attributes.Add("class", "btn btn-dark");
                btnNuevo.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-dark");
                btnCancelar.Attributes.Add("class", "btn btn-dark");
                btnEliminar.Attributes.Add("class", "btn btn-danger");

                lkbtnPrincipio.Attributes.Add("class", "btn btn-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");

            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
                btnNuevo.Attributes.Add("class", "btn btn-outline-primary");

                btnGuardar.Attributes.Add("class", "btn btn-outline-dark");
                btnCancelar.Attributes.Add("class", "btn btn-outline-dark");
                btnEliminar.Attributes.Add("class", "btn btn-outline-danger ");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");

            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnNuevo.Disabled = true;
                    }
                    if (row["banbus"].Equals("N")) //Abrir
                    {
                        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnAbrir.Disabled = true;
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnGuardar.Disabled = true;
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnCancelar.Disabled = true;

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEliminar.Disabled = true;
                        Session["baneli"] = "N";
                    }
                }
            }
            return dt;
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_codcliSelected"] = "";
            f_limpiarCamposEditar();
           
        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_vc_Cliente.aspx");
        }
        protected void btnGuardarCliente_Click(object sender, EventArgs e)
        {
            frmVerificar.Attributes.Add("class", "needs-validation was-validated");
            //PARA IMAGEN CLIENTE (imgusu)
            string s_imgcli = "";
            //if (imgCliente.PostedFile != null)
            //{
            //    //si hay una archivo.
            //    string nombreArchivo = imgCliente.PostedFile.FileName;
            //    s_imgcli = "~/Modulos/Ventas/ImgCliente/" + nombreArchivo;
            //    imgCliente.PostedFile.SaveAs(Server.MapPath(s_imgcli));
            //}

            string s_prinom = txtNombre1.Value;
            string s_segnom = txtNombre2.Value;
            string s_priape = txtApellido1.Value;
            string s_segape = txtApellido2.Value;
            string s_codusu = Session["gs_CodUs1"].ToString();
            string s_usuing = Session["gs_UsuIng"].ToString();
            string s_nomcli = s_priape + " " + s_segape + " " + s_prinom + " " + s_segnom;
            string s_codcli = txtCodcli.Value.Trim();
            string s_tipind = txtTipind.Text;
            string s_tipemp = txtTipemp.Text;
            string s_rucced = txtRucced.Text;
            string s_fecha = txtFecnac.Value;
            string s_fecnac = "";
            try
            {
                DateTime d_date = DateTime.Parse(s_fecha);
                s_fecnac = d_date.ToString("yyyy-MM-dd");
            }
            catch (Exception ex) //si tiene fecnac en NULL
            {
                s_fecnac = DateTime.Now.Date.ToString();
                //throw;
            }

            string s_sexcli = "";
            if (rbtMasculino.Checked)
                s_sexcli = rbtMasculino.Value;
            else
                s_sexcli = rbtFemenino.Value;

            string s_estciv = ddlEstadoCivil.SelectedItem.Value;
            string s_naccli = txtNaccli.Value;

            //Datos de Contacto
            string s_dircli = txtCalle0.Value;
            string s_dirbar = txtNo.Value;
            string s_codpro = txtProvincia.Value;
            string s_ciucli = txtCiudad.Value;
            string s_codpar = txtParroquia.Value;
            string s_telcli = txtTelPrin.Value;
            string s_telcas = txtTelCasa.Value;
            string s_telcel = txtTelCel.Value;
            string s_email = txtEmail.Value;
            string s_codcla = ddlCodcla.SelectedItem.Value;
            string s_codven = ddlCodven.SelectedItem.Value;
            string s_lispre = ddlLispre.SelectedItem.Value;
            if (!(s_codcli.Equals("") || s_nomcli.Equals("") || s_dircli.Equals("") || s_prinom.Equals("") || s_priape.Equals("") || s_nomcli.Equals("") || s_telcel.Equals("")))
            {
                string gs_Action = Session["gs_Action"].ToString();
                cls_vc_Cliente objCliente = new cls_vc_Cliente();
                clsError objError = objCliente.f_Cliente_Actualizar(Session["gs_Codemp"].ToString(), s_prinom, s_segnom, s_priape, s_segape, s_nomcli, s_codcli,
                s_tipind, s_tipemp, s_rucced, s_fecnac, s_sexcli, s_estciv, s_naccli, s_dircli,
                s_dirbar, s_codpro, s_ciucli, s_codpar, s_telcli, s_telcas, s_telcel, s_email,
                s_codcla, s_codven, s_codusu, s_usuing, s_lispre, s_imgcli, gs_Action);
                
                if (String.IsNullOrEmpty(objError.Mensaje))
                    lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }               
            }
            else
            {
                lblError.Text = "❗ *01. " + DateTime.Now + " Debe llenar todos los campos para crear un cliente.";
            }

        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_codcliSelected"] = "";
            f_limpiarCamposEditar();
        }
        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_codcliSelected"].ToString()))
            {
                Session["gs_codcliSelected"] = txtCodcli.Value;
                lblEliCli.Text = Session["gs_codcliSelected"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar un cliente para eliminar.";
            }
            
        }
        protected void btnEliminarCliente_Click(object sender, EventArgs e)
        {
            cls_vc_Cliente objCliente = new cls_vc_Cliente(); //eliminar
            clsError objError = objCliente.f_Cliente_Eliminar(Session["gs_codcliSelected"].ToString(), Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                f_limpiarCamposEditar();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

        }

        protected void lkbtnPrincipio_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objCliente = objCliente.f_Cliente_Principio(Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objCliente.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codcliSelected"] = objCliente.Codcli;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objCliente.Nomcli;

                f_llenarCamposEditar(objCliente);
            }
            else
            {
                f_ErrorNuevo(objCliente.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

        }
        protected void lkbtnSiguiente_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objCliente = objCliente.f_Cliente_Siguiente(Session["gs_Codemp"].ToString(), Session["gs_Siguiente"].ToString());
            if (String.IsNullOrEmpty(objCliente.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codcliSelected"] = objCliente.Codcli;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objCliente.Nomcli;

                f_llenarCamposEditar(objCliente);
            }
            else
            {
                f_ErrorNuevo(objCliente.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnFinal_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objCliente = objCliente.f_Cliente_Final(Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objCliente.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codcliSelected"] = objCliente.Codcli;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objCliente.Nomcli;

                f_llenarCamposEditar(objCliente);
            }
            else
            {
                f_ErrorNuevo(objCliente.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnAtras_Click(object sender, EventArgs e)
        {
            f_limpiarCamposEditar();

            objCliente = objCliente.f_Cliente_Atras(Session["gs_Codemp"].ToString(), Session["gs_Siguiente"].ToString());
            if (String.IsNullOrEmpty(objCliente.Error.Mensaje))
            {
                //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_codcliSelected"] = objCliente.Codcli;
                //VARIABLE PARA EL lkbtnSiguiente
                Session["gs_Siguiente"] = objCliente.Nomcli;

                f_llenarCamposEditar(objCliente);
            }
            else
            {
                f_ErrorNuevo(objCliente.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        

        [WebMethod]
        public static cls_vc_Cliente f_Validar(string s_codcli) //VALIDA CLIENTES.txt
        {
            cls_vc_Cliente objCliente = new cls_vc_Cliente();
            objCliente.RucCed = s_codcli;

            if (s_codcli.Length == 10) //si es CEDULA
            {
                objCliente.TipInd = "2";
                objCliente.TipEmp = "3"; // NO ESTABA EN VALIDACION ORIGINAL
                if (s_codcli.Substring(2, 1).Equals("0") || s_codcli.Substring(2, 1).Equals("1") ||
                    s_codcli.Substring(2, 1).Equals("2") || s_codcli.Substring(2, 1).Equals("3") ||
                    s_codcli.Substring(2, 1).Equals("4") || s_codcli.Substring(2, 1).Equals("5"))
                {
                    objCliente.TipEmp = "3";
                }
                else
                {
                    objCliente.TipEmp = "-"; //para mandarle al error en f_ValidarCedula
                    objCliente.ErrorCed = "3er digito de cédula incorrecto.";
                }
            }
            else
            {
                if (s_codcli.Length == 13) //si es RUC
                {
                    objCliente.TipInd = "1";
                    if (s_codcli.Substring(2, 1).Equals("9"))
                    {
                        objCliente.TipEmp = "1";
                    }
                    else
                    {
                        if (s_codcli.Substring(2, 1).Equals("6"))
                        {
                            objCliente.TipEmp = "2";
                        }
                        else
                        {
                            objCliente.TipEmp = "3";
                        }
                    }
                }
                else
                {
                    if (s_codcli.Length == 9) //si es PASAPORTE
                    {
                        objCliente.TipInd = "3";
                        objCliente.TipEmp = "3";
                    }
                    else //ERROR, NO ES NI CEDULA NI RUC NI PASAPORTE (tiene digitos de mas o de menos)
                    {
                        objCliente.TipInd = "-";
                        objCliente.TipEmp = "-";
                        objCliente.RucCed = "-";
                    }
                }
            }

            string s_tipemp = objCliente.TipEmp.ToString();

            string s_error = objCliente.f_ValidacionCedula(s_codcli, s_tipemp);
            if (!s_error.Equals("CORRECTO"))
            {
                objCliente.ErrorCed = objCliente.f_ValidacionCedula(s_codcli, s_tipemp);
                objCliente.TipInd = "-";
                objCliente.TipEmp = "-";
                objCliente.RucCed = "-";
            }

            return objCliente;
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod()]
        public static String f_UnirNombreCompleto(string s_nombre1, string s_nombre2, string s_apellido1, string s_apellido2) //VALIDA CLIENTES.txt
        {
            string s_nombreCompleto = s_apellido1 + " " + s_apellido2 + " " + s_nombre1 + " " + s_nombre2;
            return s_nombreCompleto;
        }
        public void f_limpiarCamposEditar()
        {
            frmVerificar.Attributes.Add("class", "needs-validation");
            //Image2.ImageUrl = "~/imgusu/user.png";
            txtCodcli.Value = txtNombre1.Value = txtNombre2.Value = txtApellido1.Value = txtApellido2.Value = txtTipind.Text = txtTipemp.Text = txtRucced.Text =
                txtFecnac.Value = txtNaccli.Value = txtCalle0.Value = txtNo.Value = txtProvincia.Value = txtCiudad.Value = txtParroquia.Value = txtTelPrin.Value =
                txtTelCasa.Value = txtTelCel.Value = txtEmail.Value = txtNombreCompleto.Value = "";

            lblError.Text = lblError.Text = "";
        }
        public void f_llenarCamposEditar(cls_vc_Cliente objCliente)
        {
            string tmpImgCli = objCliente.Imgcli;
            if (!tmpImgCli.Equals(""))
            {
                imgFotoCliente.Attributes.Add("src", tmpImgCli);
            }
            else
            {
                imgFotoCliente.Attributes.Add("src", "~/Modulos/Ventas/ImgCliente/user.png");
            }

            txtNombreCompleto.Value = objCliente.Nomcli;
            Session["gs_Siguiente"] = objCliente.Nomcli;
            txtCodcli.Value = objCliente.Codcli;
            txtRucced.Text = objCliente.RucCed;
            txtCalle0.Value = objCliente.Dircli;
            txtTelPrin.Value = objCliente.Telcli;
            txtCiudad.Value = objCliente.Ciucli;
            txtProvincia.Value = objCliente.Codpro;
            txtParroquia.Value = objCliente.Codpar;
            txtEmail.Value = objCliente.Email;
            txtNombre1.Value = objCliente.Prinom;
            txtNombre2.Value = objCliente.Segnom;
            txtApellido1.Value = objCliente.Priape;
            txtApellido2.Value = objCliente.Segape;

            if (objCliente.Sexcli.Equals("1"))
                rbtMasculino.Checked = true;
            else
                rbtFemenino.Checked = true;

            string s_estciv = objCliente.Estciv;
            string s_lispre = objCliente.Lispre;
            string s_codcla = objCliente.Codcla;
            string s_codven = objCliente.Codven;

            if (String.IsNullOrEmpty(s_estciv))
                ddlEstadoCivil.SelectedValue = ddlEstadoCivil.Items.FindByValue("2").Value;
            else
                ddlEstadoCivil.SelectedValue = ddlEstadoCivil.Items.FindByValue(s_estciv).Value;
            if (String.IsNullOrEmpty(s_lispre))
                ddlLispre.SelectedValue = ddlLispre.Items.FindByValue("1").Value;
            else
                ddlLispre.SelectedValue = ddlLispre.Items.FindByValue(s_lispre).Value;
            if (String.IsNullOrEmpty(s_codcla))
                ddlCodcla.SelectedValue = ddlCodcla.Items.FindByValue("01").Value;
            else
                ddlCodcla.SelectedValue = ddlCodcla.Items.FindByValue(s_codcla).Value;
            if (String.IsNullOrEmpty(s_codven))
                ddlCodven.SelectedValue = ddlCodven.Items.FindByValue("00").Value;
            else
                ddlCodven.SelectedValue = ddlCodven.Items.FindByValue(s_codven).Value;

            txtTipemp.Text = objCliente.TipEmp;
            txtTipind.Text = objCliente.TipInd;

            string s_fecnac = objCliente.Fecnac;
            try
            {
                DateTime d_date = DateTime.Parse(s_fecnac);
                string s_Fecfinal = d_date.ToString("yyyy-MM-dd");
                txtFecnac.Value = s_Fecfinal;
            }
            catch (Exception) //si tiene fecnac en NULL
            {
                txtFecnac.Value = DateTime.Now.Date.ToString();
                //throw;
            }

            txtNo.Value = objCliente.Dirbar;
            txtTelCasa.Value = objCliente.Telcas;
            txtTelCel.Value = objCliente.Telcel;
            txtNaccli.Value = objCliente.Naccli;
        }
    }
}