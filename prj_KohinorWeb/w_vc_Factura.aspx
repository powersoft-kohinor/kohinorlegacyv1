﻿<%@ Page Title="Factura" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_vc_Factura.aspx.cs" Inherits="prj_KohinorWeb.w_vc_Factura" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item active">Facturas</li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <a class="c-subheader-nav-link">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary" type="button" id="">
                            <i class="cil-filter"></i>
                        </button>
                    </div>

                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
                </div>
                <asp:LinkButton ID="lkbtnSearch" runat="server" OnClick="f_GridBuscar" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
            </a>            

            <%--<a class="c-subheader-nav-link">
                <i class="c-icon cil-speech"></i>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-graph"></i>&nbsp;
                            Escritorio
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-settings"></i>
            </a>--%>
        </div>
    </div>
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item active">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click"><i class="cil-file"></i>Nuevo</button>
                    <button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" onserverclick="btnAbrir_Click"><i class="cil-folder-open"></i>Abrir</button>
                    <button type="button" class="btn btn-outline-secondary disabled" id="btnGuardar" runat="server"><i class="cil-save"></i>Guardar</button>
                    <button type="button" class="btn btn-outline-secondary disabled" id="btnCancelar" runat="server"><i class="cil-grid-slash"></i>Cancelar</button>
                    <button type="button" class="btn btn-outline-secondary disabled" id="btnEliminar" runat="server"><i class="cil-trash"></i>Eliminar</button>
                </div>
        <%--        <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-step-backward"></i></button>
                    <button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-skip-backward"></i></button>
                    <button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-skip-forward"></i></button>
                    <button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-step-forward"></i></button>
                </div>--%>

                <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>

            </li>
        </ol>

    </div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="card">
                    <div class="card-header">
                        <i class="c-icon cil-justify-center"></i><b>Factura</b>
                    </div>
                    <div class="card-body">
                        <asp:UpdatePanel ID="updFactura" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                                <div class="table table-responsive" style="overflow-x: auto;">
                                    <div class="thead-dark">
                                        <asp:GridView ID="grvFactura" class="table table-bordered table-active table-active table-hover table-striped" runat="server" AutoGenerateColumns="False" AllowPaging="true" ShowHeaderWhenEmpty="True" AllowSorting="true" OnSorting="OnSorting" OnPageIndexChanging="OnPageIndexChanging" PageSize="10" OnRowDataBound="grvFactura_RowDataBound" OnRowCreated="grvFactura_RowCreated" OnSelectedIndexChanged="grvFactura_SelectedIndexChanged">

                                            <Columns>
                                                <asp:CommandField ButtonType="Image" SelectImageUrl="~/Icon/Menu125/Select-Hand.png" ShowSelectButton="True">
                                                    <ControlStyle Width="30px" />
                                                </asp:CommandField>
                                                <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="1%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="numfac" HeaderText="Factura" SortExpression="numfac" />
                                                <asp:BoundField DataField="nomcli" HeaderText="Nombre" SortExpression="nomcli" />
                                                <asp:BoundField DataField="fecfac" HeaderText="Fecha" SortExpression="fecfac" />
                                                <asp:BoundField DataField="totfac" HeaderText="Total" SortExpression="totfac" />
                                                <asp:BoundField DataField="srie_estado" HeaderText="SRI Recibida" SortExpression="srie_estado" />
                                                <asp:BoundField DataField="srir_estado" HeaderText=" SRI Autorizado" SortExpression="srir_estado" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="updFacturaDelete" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/Icon/Menu125/delete-icon.png" Width="20px" OnClick="lkbtnDelete_Click" />

                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="imgbtnDelete" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle Font-Bold="True" ForeColor="White" />
                                            <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />

                                        </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <!-- End of Main Content -->


    </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsFactura" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [numfac], [nomcli], [fecfac], [totfac], [srie_estado], [srir_estado], [conpag], [codalm], [observ] FROM [vc_factura] WHERE (([codemp] = @codemp) AND ([clicxc] LIKE '%' + @clicxc + '%') AND ([nomcli] LIKE '%' + @nomcli + '%') AND ([fecfac] &gt;= @fecfac) AND ([fecfac] &lt;= @fecfac2)) ORDER BY [fecfac] DESC">
        <SelectParameters>
            <asp:SessionParameter Name="codemp" SessionField="gs_Codemp" Type="String" />
            <asp:SessionParameter Name="clicxc" SessionField="gs_Clicxc" Type="String" />
            <asp:SessionParameter Name="nomcli" SessionField="gs_Nomcxc" Type="String" />
            <asp:SessionParameter Name="fecfac" SessionField="gs_Fecini" Type="DateTime" />
            <asp:SessionParameter Name="fecfac2" SessionField="gs_Fecfin" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCliente" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codcli], [codcla], [nomcli], [rucced], [telcli], [email], [dircli] FROM [vc_cliente]"></asp:SqlDataSource>

    <!--f SQL DATASOURCES-->


    <!-- Modal Eliminar -->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Factura</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar la factura '<asp:Label ID="lblEliFac" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarFactura_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>
</asp:Content>
