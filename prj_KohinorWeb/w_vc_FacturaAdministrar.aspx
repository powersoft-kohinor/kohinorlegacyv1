﻿<%@ Page Title="Factura Administrar" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_vc_FacturaAdministrar.aspx.cs" Inherits="prj_KohinorWeb.w_vc_FacturaAdministrar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function f_ModalCliente() {
            $("#btnShowClienteModal").click();
        }
        function f_ModalClienteNuevo() {
            $("#btnShowClienteNuevoModal").click();
            <%--$('#ClienteNuevoModal').modal('show');
                $('#ClienteNuevoModal').on('shown.bs.modal', function () {
                    $("#<%=txtCodcli.ClientID%>").focus();
                })--%>
        }

        $(document).ready(function () {
            $("#<%=txtNombre1.ClientID%>").blur(function () {
                f_NombreCompleto();
            });

            $("#<%=txtNombre2.ClientID%>").blur(function () {
                f_NombreCompleto();
            });

            $("#<%=txtApellido1.ClientID%>").blur(function () {
                f_NombreCompleto();
            });

            $("#<%=txtApellido2.ClientID%>").blur(function () {
                f_NombreCompleto();
            });
        });

        $(document).ready(function () {
            $("#<%=txtCodcli.ClientID%>").blur(function () {
                f_Validar();
            });
            $("#<%=txtCodcli.ClientID%>").blur(function () {
                getValues();
            });

            $("#<%=txtClicxc.ClientID%>").blur(function () {
                buscarClicxc();
            });

            $("#<%=txtNtardes.ClientID%>").blur(function () {
                buscarNtardes();
            });

            $("#btnSRIBuscar").click(function () {
                var s_codcli = $("#<%=txtCodcli.ClientID%>").val();
                PageMethods.f_buscarCliSRI(s_codcli, buscarCliSRI_Success, buscarCliSRI_Fail);
            });
        });

        function f_Validar() {
            var s_cod = $("#<%=txtCodcli.ClientID%>").val();
            PageMethods.f_Validar(s_cod, f_SuccessValidacionCod, f_FailValidacionCod);
        }
        function f_SuccessValidacionCod(data) {
            if (data.TipInd == "-" || data.TipEmp == "-") {
                //alert("Solo se puede ingresar RUC = 13 Digitos, CI=10 Digitos, PASAPORTE=9 Digitos");
                alert(data.ErrorCed);
                $("#<%=txtTipind.ClientID%>").val("-");
                $("#<%=txtTipemp.ClientID%>").val("-");
                $("#<%=txtRucced.ClientID%>").val("-");
            } else {
                $("#<%=txtTipind.ClientID%>").val(data.TipInd);
                $("#<%=txtTipemp.ClientID%>").val(data.TipEmp);
                $("#<%=txtRucced.ClientID%>").val(data.RucCed);
            }
        }
        function f_FailValidacionCod() {
            alert("ERROR : Método getValues_Success(data) fallo.");
        }

        function f_NombreCompleto() {
            var s_nombre1 = $("#<%=txtNombre1.ClientID%>").val();
            var s_nombre2 = $("#<%=txtNombre2.ClientID%>").val();
            var s_apellido1 = $("#<%=txtApellido1.ClientID%>").val();
            var s_apellido2 = $("#<%=txtApellido2.ClientID%>").val();

            PageMethods.f_UnirNombreCompleto(s_nombre1, s_nombre2, s_apellido1, s_apellido2, f_SucessNombreCompleto, f_FailNombreCompleto);
        }
        function f_SucessNombreCompleto(data) {
            $("#<%=txtNombreCompleto.ClientID%>").val(data);

        }
        function f_FailNombreCompleto(data) {
            $("#<%=txtNombreCompleto.ClientID%>").val("-");
        }

        //Tab en txtClicxc
        function buscarClicxc() {
            var text1 = $("#<%=txtClicxc.ClientID%>").val();
            PageMethods.f_buscarCli(text1, buscarClicxc_Success, buscarClicxc_Fail);
        }
        function buscarClicxc_Success(data) {
            if (data.Clicxc == "noEncontro") {                
                $("#<%=txtCodcli.ClientID%>").val(data.ClicxcModal);
                PageMethods.f_buscarCliSRI(data.ClicxcModal, buscarCliSRI_Success, buscarCliSRI_Fail);
                f_ModalClienteNuevo();
            } else {
                $("#<%=txtClicxc.ClientID%>").val(data.Clicxc);
                $("#<%=txtNomcxc.ClientID%>").val(data.Nomcxc);
                $("#<%=ddlLisprefac.ClientID%>").val(data.Lispre);
                $("#<%=txtDircli.ClientID%>").val(data.Dircli);
                $("#<%=txtObserv.ClientID%>").val(data.Observ);
                $("#<%=txtNtardes.ClientID%>").val(data.Ntardes);
                $("#<%=txtEmailfac.ClientID%>").val(data.Email);
                $("#<%=txtCelular.ClientID%>").val(data.Telcli);
            }
        }
        function buscarClicxc_Fail() {
            alert("ERROR : Método buscarClicxc(data) fallo.");
        }

        //Antes de abrir f_ModalClienteNuevo()
        function buscarCliSRI_Success(data) {
            $("#<%=txtCodcli.ClientID%>").val(data.ClicxcModal);
            if (data.ErrorCed) {
                alert(data.ErrorCed);
            } else {
                $("#<%=txtTipind.ClientID%>").val(data.TipInd);
                $("#<%=txtTipemp.ClientID%>").val(data.TipEmp);
                $("#<%=txtCodcli.ClientID%>").val(data.Codcli);
                $("#<%=txtRucced.ClientID%>").val(data.Codcli);
                $("#<%=txtApellido1.ClientID%>").val(data.Priape);
                $("#<%=txtApellido2.ClientID%>").val(data.Segape);
                $("#<%=txtNombre1.ClientID%>").val(data.Prinom);
                $("#<%=txtNombre2.ClientID%>").val(data.Segnom);
                $("#<%=txtNombreCompleto.ClientID%>").val(data.Nomcli);
                $("#<%=txtFecnac.ClientID%>").val(data.Fecnac);
                $("#<%=ddlEstadoCivil.ClientID%>").val(data.Estciv);
                if (data.Sexcli == "1") {
                    $("#<%=rbtMasculino.ClientID%>").prop("checked", true);
                } else {
                    $("#<%=rbtFemenino.ClientID%>").prop("checked", true);
                }
                $("#<%=txtCalle0.ClientID%>").val(data.Dircli);
                $("#<%=txtNo.ClientID%>").val(data.Dirbar);
                $("#<%=txtProvincia.ClientID%>").val(data.DPA_ProvinDomicilio);
                $("#<%=txtCiudad.ClientID%>").val(data.Ciucli);
                $("#<%=txtParroquia.ClientID%>").val(data.DPA_ParroqDomicilio);
                $("#<%=txtNaccli.ClientID%>").val(data.Naccli);
            }
        }
        function buscarCliSRI_Fail() {
            alert("ERROR : Método f_buscarCliSRI(data) fallo.");
        }

        //Tab en txtNtardes
        function buscarNtardes() {
            var text1 = $("#<%=txtNtardes.ClientID%>").val();
            var text2 = $("#<%=txtFecfac.ClientID%>").val();
            PageMethods.f_buscarNtardes(text1, text2, buscarNtardes_Success, buscarNtardes_Fail);
        }
        function buscarNtardes_Success(data) {
            if (!data.Clicxc == "") {
                $("#<%=txtClicxc.ClientID%>").val(data.Clicxc);
                $("#<%=txtNomcxc.ClientID%>").val(data.Nomcxc);
                $("#<%=ddlLisprefac.ClientID%>").val(data.Lispre);
                $("#<%=txtDircli.ClientID%>").val(data.Dircli);
                $("#<%=txtObserv.ClientID%>").val(data.Observ);
                $("#<%=txtEmailfac.ClientID%>").val(data.Email);
                $("#<%=txtCelular.ClientID%>").val(data.Telcli);
            }
        }
        function buscarNtardes_Fail() {
            alert("ERROR : Método buscarNtardes_Success(data) fallo.");
        }
    </script>
</asp:Content>
<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item"><a href="w_vc_Factura.aspx">Facturas</a></li>
            <li class="breadcrumb-item active">Administrar Factura</li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <a class="c-subheader-nav-link">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary" type="button" id="">
                            <i class="cil-filter"></i>
                        </button>
                    </div>

                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
                </div>
                <asp:LinkButton ID="lkbtnSearch" runat="server" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
            </a>

            <%--<a class="c-subheader-nav-link">
                <i class="c-icon cil-speech"></i>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-graph"></i>&nbsp;
                            Escritorio
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-settings"></i>
            </a>--%>
        </div>
    </div>
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item active">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click"><i class="cil-file"></i>Nuevo</button>
                    <button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" onserverclick="btnAbrir_Click"><i class="cil-folder-open"></i>Abrir</button>
                    <button type="button" class="btn btn-outline-dark" id="btnGuardar" runat="server" onserverclick="btnFormaDePago_Click"><i class="cil-save"></i>Guardar</button>
                    <button type="button" class="btn btn-outline-dark" id="btnCancelar" runat="server" onserverclick="btnCancelar_Click"><i class="cil-grid-slash"></i>Cancelar</button>
                    <button type="button" class="btn btn-outline-danger" id="btnEliminar" runat="server" onserverclick="lkbtnDelete_Click"><i class="cil-trash"></i>Eliminar</button>
                </div>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-dark" id="lkbtnPrincipio" runat="server" onserverclick="lkbtnPrincipio_Click"><i class="cil-media-step-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnAtras" runat="server" onserverclick="lkbtnAtras_Click"><i class="cil-media-skip-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnSiguiente" runat="server" onserverclick="lkbtnSiguiente_Click"><i class="cil-media-skip-forward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnFinal" runat="server" onserverclick="lkbtnFinal_Click"><i class="cil-media-step-forward"></i></button>
                </div>
                <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
            </li>
        </ol>

    </div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div id="div_Factura" runat="server" class="card bg-gradient-secondary">
                    <div class="card-header">
                        <i class="c-icon cil-justify-center"></i><b>Factura</b>
                    </div>

                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-9">
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><strong>Ruc/C.I.</strong></span>
                                            </div>
                                            <input type="text" id="txtClicxc" class="form-control" runat="server" />
                                            <div class="input-group-append">
                                                <asp:LinkButton ID="lkbtnBuscarCliente" data-toggle="modal" data-target="#ClienteModal" runat="server" class="btn btn-primary">
                                                            <i class="cil-search"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <asp:TextBox ID="txtNomcxc" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><strong>Email</strong></span>
                                            </div>
                                            <asp:TextBox ID="txtEmailfac" runat="server" class="form-control" placeholder="ejemplo@email.com" TextMode="Email"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><strong>Dirección</strong></span>
                                            </div>
                                            <asp:TextBox ID="txtDircli" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><strong>Cel.</strong></span>
                                            </div>
                                            <asp:TextBox ID="txtCelular" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><strong>Lista</strong></span>
                                            </div>
                                            <%--<asp:TextBox ID="txtLispre" runat="server" class="form-control"></asp:TextBox>--%>
                                            <asp:DropDownList ID="ddlLisprefac" runat="server" class="form-control">
                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                <asp:ListItem Value="8">8</asp:ListItem>
                                                <asp:ListItem Value="9">9</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-7">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><strong>Observ.</strong></span>
                                            </div>
                                            <asp:TextBox ID="txtObserv" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><strong>Tarjeta.</strong></span>
                                            </div>
                                            <asp:DropDownList ID="ddlTtardes" runat="server" class="form-control" DataSourceID="sqldsTtardes" DataTextField="nomtar" DataValueField="codtar"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <asp:TextBox ID="txtNtardes" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="table table-responsive scroll1 text-black-50" style="overflow-x: auto; height: 70%">
                                        <div class="thead-dark">
                                            <asp:UpdatePanel ID="updFacturaRen" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="grvFacturaRen" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" class="table table-dark ">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex + 1 %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Código" ItemStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="updFacturaRenPuntitos" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <div class="input-group mb-1">
                                                                                <asp:TextBox ID="txtgrvCodart" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                                <div class="input-group-append">
                                                                                    <asp:Button ID="btnPuntitosRen" runat="server" Text="..." class="btn btn-primary btn-sm" OnClick="btnPuntitosRen_Click" />
                                                                                </div>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="btnPuntitosRen" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Descripción" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtgrvNomart" runat="server" htmlencode="false" class="form-control form-control-sm"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Cantidad" ItemStyle-Width="1%">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="updFacturaRenCantid" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:TextBox ID="txtgrvCantid" runat="server" class="form-control form-control-sm" AutoPostBack="True" TextMode="Number" OnTextChanged="txtgrvCantid_TextChanged"></asp:TextBox>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="txtgrvCantid" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="PVP" ItemStyle-Width="3%">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="updFacturaRenPreuni" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:TextBox ID="txtgrvPreuni" class="form-control form-control-sm" runat="server" DataFormatString="{0:n}" AutoPostBack="True" TextMode="Number" OnTextChanged="txtgrvPreuni_TextChanged"></asp:TextBox>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="txtgrvPreuni" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="IVA" ItemStyle-Width="3%">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtgrvIvacal" runat="server" class="form-control form-control-sm" DataFormatString="{0:n}" ReadOnly="True"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="%" ItemStyle-Width="1%">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtgrvPoriva" runat="server" class="form-control form-control-sm" DataFormatString="{0:n}" ReadOnly="True"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Dsco" ItemStyle-Width="1%">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtgrvDesren" class="form-control form-control-sm" runat="server" ReadOnly="True"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="% Des." ItemStyle-Width="2%">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtgrvCoddes" class="form-control form-control-sm" runat="server" ReadOnly="True"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Total" ItemStyle-Width="2%">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtgrvTotren" class="form-control form-control-sm" runat="server" ReadOnly="True"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-Width="1%" ItemStyle-HorizontalAlign="center">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="updFacturaRenBorrar" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:ImageButton ID="lkgrvBorrar" runat="server" OnClick="lkgrvBorrar_Click" ImageUrl="~/Icon/Menu125/delete-icon.png" Width="20px" />
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="lkgrvBorrar" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                    </div>
                                    <div class="form-group col-md-4">

                                        <asp:Button ID="btnAgregar" runat="server" class="btn btn-dark" Text="➕ Agregar" OnClick="btnAgregar_Click" />
                                        <asp:Button ID="btnEliminarTodo" runat="server" class="btn btn-dark" OnClick="btnEliminarTodo_Click" Text="Eliminar Todo" />
                                        <asp:Button ID="btn2x1" runat="server" class="btn btn-dark" Text="2X1" />
                                        <asp:Button ID="btnFormaDePago" runat="server" class="btn btn-facebook" Text="Forma de Pago" OnClick="btnFormaDePago_Click" />
                                    </div>
                                    <div class="form-group col-md-4">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <div class="card text-white bg-gradient-primary ">
                                    <asp:UpdatePanel ID="updEncabezado" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="card-header text-center">
                                                <div class="row">
                                                    <div class="col">
                                                        <h4><label><strong>N° </strong></label></h4>
                                                        <h4><asp:Label ID="txtNumfac" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label></h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <asp:Label ID="Label8" runat="server" Text="Label">Emisión</asp:Label>
                                                    </div>
                                                    <div class="form-group col-md-9">
                                                        <asp:TextBox ID="txtFecfac" runat="server" class="form-control form-control-sm" TextMode="Date"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <div class="input-group input-group-sm">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><strong>Estado</strong></span>
                                                            </div>
                                                            <asp:DropDownList ID="ddlEstado" class="form-control" runat="server">
                                                                <asp:ListItem Value="P">OK</asp:ListItem>
                                                                <asp:ListItem Value="A">ANULADO</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <div class="input-group input-group-sm">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><strong>Almacén</strong></span>
                                                            </div>
                                                            <asp:TextBox ID="txtCodalm" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <asp:Label ID="Label11" runat="server" Text="Label">Vendedor</asp:Label>
                                                    </div>
                                                    <div class="form-group col-md-9">
                                                        <asp:DropDownList ID="ddlCodven" runat="server" class="form-control form-control-sm" DataSourceID="sqldsVendedor" DataTextField="nomven" DataValueField="codven"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <asp:Label ID="Label12" runat="server" Text="Label">AUT.SRI</asp:Label>
                                                    </div>
                                                    <div class="form-group col-md-9">
                                                        <asp:TextBox ID="txtSriNumaut" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="card text-white bg-primary text-center">
                                    <div class="card-body">
                                        <asp:UpdatePanel ID="updTotales" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <asp:Label ID="Label13" runat="server" Text="Label">Total Neto</asp:Label>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <asp:Label ID="txtTotnet" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <asp:Label ID="Label14" runat="server" Text="Label">Base</asp:Label>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <asp:Label ID="txtTotbas" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <asp:Label ID="Label15" runat="server" Text="Label">Descto</asp:Label>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <asp:Label ID="txtTotdes" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-row">

                                                    <div class="form-group col-md-6">
                                                        <asp:Label ID="Label16" runat="server" Text="Label">IVA.</asp:Label>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <asp:Label ID="txtTotiva" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
                                                    </div>
                                                </div>

                                                <hr />

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <h3>
                                                            <label><strong>TOTAL</strong></label></h3>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <h3>
                                                            <asp:Label ID="txtTotfac" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label></h3>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <div class="input-group input-group-sm">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><strong>Recibido</strong></span>
                                                            </div>
                                                            <asp:TextBox ID="txtTotrec" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <div class="input-group input-group-sm">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><strong>Cambio</strong></span>
                                                            </div>
                                                            <asp:TextBox ID="txtCambio" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-row">

                                                    <div class="form-row">
                                                        Clave Acceso
&nbsp;<asp:Label ID="lblUsuing" runat="server" Text="Label"></asp:Label><asp:Label ID="lblFecing" runat="server" Text="Label"></asp:Label><asp:Label ID="lblCodusu" runat="server" Text="Label"></asp:Label><asp:Label ID="lblFecutl" runat="server" Text="Label"></asp:Label>

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <!-- End of Main Content -->
    </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsFacturaExiste" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [numfac] FROM [vc_factura] WHERE (([codemp] = @codemp) AND ([numfac] = @numfac))">
        <SelectParameters>
            <asp:SessionParameter Name="codemp" SessionField="gs_Codemp" Type="String" />
            <asp:SessionParameter Name="numfac" SessionField="gs_numfacSelected" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsVendedor" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codven])) AS codven, [nomven] FROM [vc_vendedor]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsTtardes" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT * FROM [vc_tarjeta]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCliente" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codcli], [codcla], [nomcli], [rucced], [telcli], [email], [dircli] FROM [vc_cliente]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsArticulo" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA  %>" SelectCommand="SELECT TOP 50 [codart], [nomart], [prec01], [prec02], [prec03], [prec04], [eximax], [codiva] FROM [inv_articulo] WHERE ([nomart] LIKE '%' + @nomart + '%')">
        <SelectParameters>
            <asp:SessionParameter Name="nomart" SessionField="gs_BuscarNomart" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsClaseCliente" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codcla])) AS codcla, [nomcla] FROM [vc_cliente_clase] "></asp:SqlDataSource>

    <!--f SQL DATASOURCES-->

    <!-- Modal ClientesNuevo -->
    <button id="btnShowClienteNuevoModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#ClienteNuevoModal">Eliminar modal</button>
    <div class="modal fade" id="ClienteNuevoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Cliente</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <!--i Crear Nuevo Cliente -->
                    <div class="card text-white bg-secondary text-black-50 shadow">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-md-2">
                                    <h4><strong>Gestión Clientes</strong></h4>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Código</strong></span>
                                        </div>
                                        <input type="text" id="txtCodcli" class="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <asp:TextBox ID="txtTipemp" runat="server" class="form-control" placeholder="Empresa"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-2">
                                    <asp:TextBox ID="txtTipind" runat="server" class="form-control" placeholder="Indentificación"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-2">
                                    <asp:TextBox ID="txtRucced" runat="server" class="form-control" placeholder="Número"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-2 align-items-center justify-content-center">
                                    <div class="form-row align-items-center justify-content-center">
                                        <asp:Image ID="imgFotoCliente" class="img-thumbnail" runat="server" Width="95px" Height="125px" />
                                        <div class="input-group my-2">
                                            <div class="custom-file">
                                                <input id="imgCliente" type="file" class="custom-file-input" onchange="f_vistaPreviaImg()" runat="server">
                                                <label class="custom-file-label" for="imgCliente" aria-describedby="inputGroupFileAddon02">Foto</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <input type="text" id="txtApellido1" class="form-control" runat="server" />
                                            <label for="txtApellido1">Apellidos</label>
                                            <div class="valid-feedback">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="text" id="txtApellido2" class="form-control" runat="server" />
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="text" id="txtNombre1" class="form-control" runat="server" />
                                            <label for="txtNombre1">Nombres</label>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="text" id="txtNombre2" class="form-control" runat="server" />
                                        </div>

                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><strong>Nombres Completos</strong></span>
                                                </div>
                                                <input type="text" id="txtNombreCompleto" class="form-control" runat="server" />
                                            </div>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><strong>Fecha Nacimiento</strong></span>
                                                </div>
                                                <input type="date" id="txtFecnac" class="form-control" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <asp:DropDownList ID="ddlEstadoCivil" runat="server" class="form-control" Width="150px">
                                                <asp:ListItem Value="1">Casado</asp:ListItem>
                                                <asp:ListItem Value="2">Soltero</asp:ListItem>
                                                <asp:ListItem Value="3">Divorciado</asp:ListItem>
                                                <asp:ListItem Value="4">Viudo</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group col-md-5">
                                            <asp:RadioButton ID="rbtMasculino" runat="server" GroupName="sexcli" Text="Masculino" />
                                            <asp:RadioButton ID="rbtFemenino" runat="server" GroupName="sexcli" Text="Femenino" />
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="card shadow ">
                        <div class="card-body">
                            <h5>Datos de Contacto </h5>
                            <div class="form-row">
                                <div class="form-group col-md-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Dirección</strong></span>
                                        </div>
                                        <input id="txtCalle0" runat="server" class="form-control" type="text" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>N°</strong></span>
                                        </div>
                                        <input type="text" id="txtNo" class="form-control" runat="server" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Provincia</strong></span>
                                        </div>
                                        <input type="text" id="txtProvincia" class="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Ciudad</strong></span>
                                        </div>
                                        <input type="text" id="txtCiudad" class="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Parroquia</strong></span>
                                        </div>
                                        <input type="text" id="txtParroquia" class="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Nacionalidad</strong></span>
                                        </div>
                                        <input type="text" id="txtNaccli" class="form-control" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Celular</strong></span>
                                        </div>
                                        <input id="txtTelCel" runat="server" class="form-control" type="text" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Telf.Casa</strong></span>
                                        </div>
                                        <input id="txtTelCasa" runat="server" class="form-control" type="text" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Telf.Trabajo</strong></span>
                                        </div>
                                        <input id="txtTelPrin" runat="server" class="form-control" type="text" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Email</strong></span>
                                        </div>
                                        <input type="email" id="txtEmail" class="form-control" runat="server" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Clase</strong></span>
                                        </div>
                                        <asp:DropDownList ID="ddlCodcla" runat="server" class="form-control" DataSourceID="sqldsClaseCliente" DataTextField="nomcla" DataValueField="codcla"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Vendedor</strong></span>
                                        </div>
                                        <asp:DropDownList ID="ddlCodvenCli" runat="server" class="form-control" DataSourceID="sqldsVendedor" DataTextField="nomven" DataValueField="codven"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><strong>Lista</strong></span>
                                        </div>
                                        <asp:DropDownList ID="ddlLispre" runat="server" class="form-control">
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                            <asp:ListItem Value="6">6</asp:ListItem>
                                            <asp:ListItem Value="7">7</asp:ListItem>
                                            <asp:ListItem Value="8">8</asp:ListItem>
                                            <asp:ListItem Value="9">9</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <asp:Label ID="lblErrorCliente" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
                        </div>
                    </div>
                    <!--f Crear Nuevo Cliente -->
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button id="btnGuardarCliente" type="submit" value="button" runat="server" class="btn btn-primary float-right" onserverclick="btnGuardarCliente_Click"><i class="cil-save"></i>Guardar</button>
                    <button id="btnSRIBuscar" type="button" class="btn btn-success">SRI</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal grvClientes -->
    <button id="btnShowClienteModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#ClienteModal"></button>
    <div class="modal fade" id="ClienteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccionar Cliente</h4>
                    <button class="close" id="btnCloseClienteModal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">

                    <div class="form-row">

                        <div class="form-group col-md-12">

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="lkbtnSearchCliente" runat="server" OnClick="f_GridBuscar" class="btn btn-primary"> <span class="cil-search"></span></asp:LinkButton>
                                </div>
                                <asp:TextBox ID="txtSearchCliente" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" Width="50%"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group col-md-12">

                            <div class="table table-responsive" style="overflow-x: auto;">
                                <div class="thead-dark">
                                    <asp:UpdatePanel ID="updCliente" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            <asp:GridView ID="grvCliente" class="table table-bordered table-active table-active table-hover table-striped"
                                                runat="server" AutoGenerateColumns="False" OnRowDataBound="grvCliente_RowDataBound" OnRowCreated="grvCliente_RowCreated" OnSelectedIndexChanged="grvCliente_SelectedIndexChanged"
                                                AllowSorting="true" OnSorting="OnSorting" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging" PageSize="10">
                                                <Columns>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updClienteSelect" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnSelectCliente" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/Select-Hand.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnSelectCliente" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="codcli" HeaderText="Código" SortExpression="codcli" />
                                                    <asp:BoundField DataField="codcla" HeaderText="Clase" SortExpression="codcla" />
                                                    <asp:BoundField DataField="nomcli" HeaderText="Nombres" SortExpression="nomcli" />
                                                    <asp:BoundField DataField="rucced" HeaderText="RUC/Cédula" SortExpression="rucced" />
                                                    <asp:BoundField DataField="telcli" HeaderText="Teléfono" SortExpression="telcli" />
                                                    <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />

                                                </Columns>
                                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                            </asp:GridView>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Articulos Popup -->
    <button id="btnShowArticulosModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#modalPuntitosArt"></button>
    <div id="modalPuntitosArt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccione Artículos</h4>
                    <button class="close" id="btnCloseArticulosModal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <!-- CONTENIDO-->
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="lkbtnBuscarNomart" runat="server" OnClick="f_GridBuscarArt" class="btn btn-primary"> <span class="cil-search"></span></asp:LinkButton>
                                </div>
                                <asp:TextBox ID="txtSearchArt" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" Width="50%"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="form-group col-md-12">
                            <div class="table table-responsive" style="overflow-x: auto;">
                                <div class="thead-dark">
                                    <asp:UpdatePanel ID="updArticulo" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="grvArticulo" runat="server" class="table table-bordered table-active table-active table-hover table-striped"
                                                AutoGenerateColumns="False" OnSelectedIndexChanged="grvArticulo_SelectedIndexChanged" OnRowCreated="grvArticulo_RowCreated"
                                                AllowSorting="true" OnSorting="OnSortingArt" AllowPaging="true" OnPageIndexChanging="OnPageIndexChangingArt" PageSize="10">

                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updArticuloSelect" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnSelectArticulo" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/Select-Hand.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnSelectArticulo" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="codart" HeaderText="codart" SortExpression="codart" />
                                                    <asp:BoundField DataField="nomart" HeaderText="nomart" SortExpression="nomart" />
                                                    <asp:BoundField DataField="prec01" HeaderText="prec01" SortExpression="prec01" />
                                                    <asp:BoundField DataField="prec02" HeaderText="prec02" SortExpression="prec02" />
                                                    <asp:BoundField DataField="prec03" HeaderText="prec03" SortExpression="prec03" />
                                                    <asp:BoundField DataField="prec04" HeaderText="prec04" SortExpression="prec04" />
                                                    <asp:BoundField DataField="eximax" HeaderText="eximax" SortExpression="eximax" />
                                                </Columns>

                                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Eliminar -->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Factura</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar la factura '<asp:Label ID="lblEliFac" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarFactura_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>
</asp:Content>
