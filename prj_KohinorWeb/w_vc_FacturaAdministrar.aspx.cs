﻿using prj_KohinorWeb.clsCedulaSRI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_vc_FacturaAdministrar : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "3", niv001 = "2", niv002 = "1";
        protected cls_vc_Cliente objCliente = new cls_vc_Cliente();
        protected cls_vc_Factura objFactura = new cls_vc_Factura();
        protected cls_vc_Factura_Ren objFacturaRen = new cls_vc_Factura_Ren();
        protected cls_cfg_Secuencia objSecuencia = new cls_cfg_Secuencia();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //***i RENGLONES***//
                Session.Add("gs_BuscarNomart", "%"); //variable para buscar el articulo en btnPuntitosArticulos
                f_SetInitialRow();
                Session.Add("gs_Siguiente", ""); //para el lkbtnSiguiente Y lkbtnAtras

                f_Seg_Usuario_Menu();
                f_BindGrid_Inicial(); //grvCliente
                f_BindGrid_InicialArt();//grvArticulo

                if (Session["gs_numfacSelected"] == null) Response.Redirect("w_vc_Factura.aspx");


                if (String.IsNullOrEmpty(Session["gs_numfacSelected"].ToString()))
                    Session.Add("gs_Action", "Add");
                else
                    Session.Add("gs_Action", "Update");

                ddlCodven.DataBind(); //OJO DEBE IR ANTES DE f_llenarCampos
                ddlCodvenCli.DataBind(); //del Modal Clientes
                ddlTtardes.DataBind(); //OJO DEBE IR ANTES DE f_llenarCampos

                objFactura = objFactura.f_Factura_Buscar(Session["gs_Codemp"].ToString(), Session["gs_numfacSelected"].ToString());
                if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
                {
                    if (!String.IsNullOrEmpty(objFactura.Numfac))//si seleccionó una Factura
                    {
                        Session["gs_numfacSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                        Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                        f_llenarCamposFacturaEnc(objFactura);

                        objFacturaRen = objFacturaRen.f_Factura_Ren_Buscar(Session["gs_Codemp"].ToString(), Session["gs_numfacSelected"].ToString());
                        if (String.IsNullOrEmpty(objFacturaRen.Error.Mensaje))
                        {
                            ViewState["CurrentTable"] = objFacturaRen.dtFacturaRen;
                            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                            if (dtCurrentTable.Rows.Count > 0)
                            {
                                grvFacturaRen.DataSource = dtCurrentTable;
                                grvFacturaRen.DataBind();
                                f_SetPreviousData();
                            }
                            else
                            {
                                f_SetInitialRow();
                                f_SetPreviousData();
                                lblError.Text = "❗ *01. " + DateTime.Now + " Factura no tiene renglones.";
                            }
                        }
                        else
                        {
                            f_ErrorNuevo(objFacturaRen.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                    else //crear factura desde cero
                    {
                        objSecuencia = objSecuencia.f_CalcularSecuencia(Session["gs_Codemp"].ToString(), "VC_FAC", Session["gs_Sersec"].ToString());
                        objFactura.Numfac = objSecuencia.Numero;
                        objFactura.Fecfac = objSecuencia.Fecha;
                        if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
                        {
                            f_llenarCamposFacturaEnc(objFactura);
                        }
                        else
                        {
                            f_ErrorNuevo(objFactura.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }                    
                }
                else
                {
                    f_ErrorNuevo(objFactura.Error);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
                //***f RENGLONES***//
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAbrir.Attributes.Add("class", "btn btn-dark");
                btnNuevo.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-dark");
                btnCancelar.Attributes.Add("class", "btn btn-dark");
                btnEliminar.Attributes.Add("class", "btn btn-danger");
                div_Factura.Attributes.Add("class", "card");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");

            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
                btnNuevo.Attributes.Add("class", "btn btn-outline-primary");
                div_Factura.Attributes.Add("class", "card bg-gradient-secondary");
                btnGuardar.Attributes.Add("class", "btn btn-outline-dark");
                btnCancelar.Attributes.Add("class", "btn btn-outline-dark");
                btnEliminar.Attributes.Add("class", "btn btn-outline-danger ");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");

            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnNuevo.Disabled = true;
                    }
                    if (row["banbus"].Equals("N")) //Abrir
                    {
                        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnAbrir.Disabled = true;
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnGuardar.Disabled = true;
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnCancelar.Disabled = true;

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEliminar.Disabled = true;
                        Session["baneli"] = "N";
                    }
                }
            }
            return dt;
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_numfacSelected"] = "";
            f_limpiarCampos();
        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_vc_Factura.aspx");
        }
        protected void btnGuardarFactura_Click(object sender, EventArgs e)
        {
            // YA NO SE USA, EL BOTON GUARDAR LLAMA A FORMA DE PAGO
            lblError.Text = "";
            Session.Add("g_objFactura", f_generarObjFactura());
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            Session.Add("gdt_FacturaRen", dt);
            //Session.Add("gs_Existe", "Add");

            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsFacturaExiste.Select(args);
            if (view != null) //ya existe la factura (Actualizar)
                Session.Add("gs_Existe", "Update");
            else //no existe la factura (Insertar)
                Session.Add("gs_Existe", "Add");
            if (String.IsNullOrEmpty(txtClicxc.Value)) lblError.Text = "❗ Debe seleccionar un cliente.";
            if (dt.Rows.Count == 1 && String.IsNullOrEmpty(dt.Rows[0]["codart"].ToString())) lblError.Text = "❗ Factura no tiene renglones.";
            if (String.IsNullOrEmpty(lblError.Text)) Response.Redirect("w_vc_FacturaFormaPago.aspx");

            //DataSourceSelectArguments args = new DataSourceSelectArguments();
            //DataView view = (DataView)sqldsFacturaExiste.Select(args);
            //if (view!=null) //ya existe la factura (Actualizar)
            //{
            //    objFactura = f_generarObjFactura();
            //    clsError objError = objFactura.f_Factura_Actualizar(objFactura, "Update");
            //    if (String.IsNullOrEmpty(objError.Mensaje))
            //    {
            //        //ACTUALIZA RENGLON
            //        DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
            //        objFacturaRen.dtFacturaRen = dtRenglones;
            //        objError = objFacturaRen.f_Factura_Ren_Actualizar(objFacturaRen, "Update");
            //        if (String.IsNullOrEmpty(objError.Mensaje))
            //        {
            //            lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
            //        }
            //        else //error en renglones
            //        {
            //            f_ErrorNuevo(objError);
            //            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            //        }
            //    }
            //    else
            //    {
            //        f_ErrorNuevo(objError);
            //        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            //    }
            //}
            //else //no existe la factura (Insertar)
            //{
            //    objFactura = f_generarObjFactura();
            //    clsError objError = objFactura.f_Factura_Actualizar(objFactura, "Add");
            //    if (String.IsNullOrEmpty(objError.Mensaje))
            //    {
            //        //INSERTA RENGLON
            //        DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
            //        objFacturaRen.dtFacturaRen = dtRenglones;
            //        objError = objFacturaRen.f_Factura_Ren_Actualizar(objFacturaRen, "Add");
            //        if (String.IsNullOrEmpty(objError.Mensaje))
            //        {
            //            lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
            //        }
            //        else //error en renglones
            //        {
            //            f_ErrorNuevo(objError);
            //            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            //        }
            //    }
            //    else
            //    {
            //        f_ErrorNuevo(objError);
            //        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            //    }
            //}
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_numfacSelected"] = "";
            f_limpiarCampos();
        }
        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_numfacSelected"].ToString()))
            {
                Session["gs_numfacSelected"] = txtNumfac.Text;
                lblEliFac.Text = Session["gs_numfacSelected"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar una factura para eliminar.";
            }
        }
        protected void btnEliminarFactura_Click(object sender, EventArgs e)
        {
            cls_vc_Factura objFactura = new cls_vc_Factura(); //eliminar
            clsError objError = objFactura.f_Factura_Eliminar(Session["gs_numfacSelected"].ToString(), Session["gs_Codemp"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                f_limpiarCampos();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void lkbtnPrincipio_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objFactura = objFactura.f_Factura_Principio(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
            if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            {
                Session["gs_numfacSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposFacturaEnc(objFactura);
                ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    lblError.Text = "❗ *03. " + DateTime.Now + " Factura no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objFactura.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnSiguiente_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objFactura = objFactura.f_Factura_Siguiente(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString(), Session["gs_Siguiente"].ToString());
            if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            {
                Session["gs_numfacSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposFacturaEnc(objFactura);
                ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    f_SetInitialRow();
                    f_SetPreviousData();
                    lblError.Text = "❗ *04. " + DateTime.Now + " Factura no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objFactura.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnFinal_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objFactura = objFactura.f_Factura_Final(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
            if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            {
                Session["gs_numfacSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposFacturaEnc(objFactura);
                ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    lblError.Text = "❗ *05. " + DateTime.Now + " Factura no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objFactura.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnAtras_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objFactura = objFactura.f_Factura_Atras(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString(), Session["gs_Siguiente"].ToString());
            if (String.IsNullOrEmpty(objFactura.Error.Mensaje))
            {
                Session["gs_numfacSelected"] = objFactura.Numfac; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objFactura.Numfac; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposFacturaEnc(objFactura);
                ViewState["CurrentTable"] = objFactura.FacturaRen.dtFacturaRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    f_SetInitialRow();
                    f_SetPreviousData();
                    lblError.Text = "❗ *06. " + DateTime.Now + " Factura no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objFactura.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }


        //***i CLIENTE ***//
        [WebMethod(enableSession: true)]
        public static cls_vc_Factura f_buscarCli(string s_codcli) //tab en txtClicxc
        {
            cls_vc_Factura objFactura = new cls_vc_Factura();
            if (s_codcli.Equals("")) //tab en txtClicxc y esta vacio
            {
                objFactura.Clicxc = "";
                objFactura.Nomcxc = "";
                objFactura.Dircli = "";
                objFactura.Lispre = "";
                objFactura.Observ = "";
                objFactura.ClicxcModal = "";
                objFactura.Ntardes = "";
                objFactura.Email = "";
                objFactura.Telcli = "";

                return objFactura;
            }
            return objFactura.f_Factura_Buscar_Cliente(HttpContext.Current.Session["gs_Codemp"].ToString(), s_codcli);
        }

        [WebMethod(enableSession: true)]
        public static cls_vc_Cliente f_buscarCliSRI(string s_codcli) //cuando se activa el ModalClienteNuevo se llama automaticamente a este metodo
        {
            cls_AgregarCed objAgregarCed = new cls_AgregarCed();
            cls_vc_Cliente objCliente = objAgregarCed.f_buscarCliSRI(s_codcli);
            return objCliente;
        }

        [WebMethod]
        public static cls_vc_Factura f_buscarNtardes(string s_ntardes, string s_fecha) //tab en txtNtardes
        {
            //BUSCAR CLIENTE QUE COINCIDA CON EL # DE TARJETA
            cls_vc_Factura objFactura = new cls_vc_Factura();

            //Session["gs_CodEmp"]
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_PV_S_CLIENTE_ENCABEZADO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", "01"); //OJO CODEMP SE ESTA MANDANDO QUEMADO
            cmd.Parameters.AddWithValue("@CLICXC", "");
            cmd.Parameters.AddWithValue("@NUMTAR", s_ntardes);

            cmd.Parameters.Add("@s_Clicxc", SqlDbType.Char, 20).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Nomcxc", SqlDbType.Char, 100).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Dircli", SqlDbType.Char, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Lispre", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Observ", SqlDbType.Char, 160).Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string clicxc = cmd.Parameters["@s_Clicxc"].Value.ToString().Trim();

            objFactura.Nomcxc = cmd.Parameters["@s_Nomcxc"].Value.ToString().Trim();
            objFactura.Dircli = cmd.Parameters["@s_Dircli"].Value.ToString().Trim();
            objFactura.Lispre = cmd.Parameters["@s_Lispre"].Value.ToString().Trim();
            objFactura.Observ = cmd.Parameters["@s_Observ"].Value.ToString().Trim();
            objFactura.Clicxc = clicxc;

            //***descuento de tarjeta***//
            HttpContext.Current.Session["gs_Ntardes"] = s_ntardes;
            string s_checktar = "select destar from pv_tarjetas where codtar= '" + s_ntardes + "'";
            SqlCommand com = new SqlCommand(s_checktar, conn);
            if (com.ExecuteScalar() != null)
                HttpContext.Current.Session["gs_Destar"] = com.ExecuteScalar().ToString();
            else
                HttpContext.Current.Session["gs_Destar"] = "0.00";
            //***descuento de tarjeta***//
            string borrar = HttpContext.Current.Session["gs_Destar"].ToString();

            conn.Close();

            return objFactura;
        }

        protected void btnGuardarCliente_Click(object sender, EventArgs e)
        {            
            lblError.Text = "";
            lblErrorCliente.Text = "";
            //PARA IMAGEN CLIENTE (imgusu)
            string s_imgcli = "";
            //if (imgCliente.PostedFile != null)
            //{
            //    //si hay una archivo.
            //    string nombreArchivo = imgCliente.PostedFile.FileName;
            //    s_imgcli = "~/Modulos/Ventas/ImgCliente/" + nombreArchivo;
            //    imgCliente.PostedFile.SaveAs(Server.MapPath(s_imgcli));
            //}
            string s_prinom = txtNombre1.Value;
            string s_segnom = txtNombre2.Value;
            string s_priape = txtApellido1.Value;
            string s_segape = txtApellido2.Value;
            string s_codusu = Session["gs_UsuIng"].ToString();
            string s_usuing = Session["gs_CodUs1"].ToString();
            string s_nomcli = txtNombreCompleto.Value;
            string s_codcli = txtCodcli.Value.Trim();
            string s_tipind = txtTipind.Text;
            string s_tipemp = txtTipemp.Text;
            string s_rucced = txtRucced.Text;
            string s_fecha = txtFecnac.Value;
            string s_fecnac = "";
            try
            {
                DateTime d_date = DateTime.Parse(s_fecha);
                s_fecnac = d_date.ToString("yyyy-MM-dd");
            }
            catch (Exception) //si tiene fecnac en NULL
            {
                s_fecnac = DateTime.Now.Date.ToString();
            }

            string s_sexcli = "";
            if (rbtMasculino.Checked)
                s_sexcli = "1";
            else
                s_sexcli = "2";

            string s_estciv = ddlEstadoCivil.SelectedItem.Value;
            string s_naccli = txtNaccli.Value;

            //Datos de Contacto
            string s_dircli = txtCalle0.Value;
            string s_dirbar = txtNo.Value;
            string s_codpro = txtProvincia.Value;
            string s_ciucli = txtCiudad.Value;
            string s_codpar = txtParroquia.Value;
            string s_telcli = txtTelPrin.Value;
            string s_telcas = txtTelCasa.Value;
            string s_telcel = txtTelCel.Value;
            string s_email = txtEmail.Value;
            string s_codcla = ddlCodcla.SelectedItem.Value;
            string s_codven = ddlCodvenCli.SelectedItem.Value;
            string s_lispre = ddlLispre.SelectedItem.Text;
            if (!(s_codcli.Equals("") || s_nomcli.Equals("") || s_dircli.Equals("") || s_prinom.Equals("") || s_priape.Equals("") || s_telcel.Equals("") || s_email.Equals("")))
            {
                string gs_Action = "Add";
                cls_vc_Cliente objCliente = new cls_vc_Cliente();
                clsError objError = objCliente.f_Cliente_Actualizar(Session["gs_Codemp"].ToString(), s_prinom, s_segnom, s_priape, s_segape, s_nomcli, s_codcli,
                s_tipind, s_tipemp, s_rucced, s_fecnac, s_sexcli, s_estciv, s_naccli, s_dircli,
                s_dirbar, s_codpro, s_ciucli, s_codpar, s_telcel, s_telcas, s_telcel, s_email,
                s_codcla, s_codven, s_codusu, s_usuing, s_lispre, s_imgcli, gs_Action);

                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    txtCodcli.Attributes.Add("class", "form-control");
                    txtNombreCompleto.Attributes.Add("class", "form-control");
                    txtDircli.Attributes.Add("class", "form-control");
                    txtNombre1.Attributes.Add("class", "form-control");
                    txtApellido1.Attributes.Add("class", "form-control");
                    txtTelCel.Attributes.Add("class", "form-control");
                    txtEmail.Attributes.Add("class", "form-control");
                    lblExito.Text = "✔️ Cliente Creado.";
                    f_limpiarModalCliente();

                    txtNomcxc.Text = s_nomcli;
                    txtDircli.Text = s_dircli;
                    ddlLisprefac.SelectedValue = s_lispre;
                    txtEmailfac.Text = s_email;
                    txtCelular.Text = s_telcel;
                }                    
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            else
            {
                if (s_codcli.Equals("")) txtCodcli.Attributes.Add("class", "form-control is-invalid"); else txtCodcli.Attributes.Add("class", "form-control");
                if (s_nomcli.Equals("")) txtNombreCompleto.Attributes.Add("class", "form-control is-invalid"); else txtNombreCompleto.Attributes.Add("class", "form-control");
                if (s_dircli.Equals("")) txtDircli.Attributes.Add("class", "form-control is-invalid"); else txtDircli.Attributes.Add("class", "form-control");
                if (s_prinom.Equals("")) txtNombre1.Attributes.Add("class", "form-control is-invalid"); else txtNombre1.Attributes.Add("class", "form-control");
                if (s_priape.Equals("")) txtApellido1.Attributes.Add("class", "form-control is-invalid"); else txtApellido1.Attributes.Add("class", "form-control");
                if (s_telcel.Equals("")) txtTelCel.Attributes.Add("class", "form-control is-invalid"); else txtTelCel.Attributes.Add("class", "form-control");
                if (s_email.Equals("")) txtEmail.Attributes.Add("class", "form-control is-invalid"); else txtEmail.Attributes.Add("class", "form-control");

                lblError.Text = "❗ *07. " + DateTime.Now + " Debe llenar todos los campos para crear un cliente."; //Error en Factura
                lblErrorCliente.Text = "❗ Debe llenar todos los campos requeridos para crear un cliente."; //Error en Modal Cliente
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalClienteNuevo();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod()]
        public static String f_UnirNombreCompleto(string s_nombre1, string s_nombre2, string s_apellido1, string s_apellido2) //VALIDA CLIENTES.txt
        {
            string s_nombreCompleto = s_apellido1 + " " + s_apellido2 + " " + s_nombre1 + " " + s_nombre2;
            return s_nombreCompleto;
        }

        [WebMethod]
        public static cls_vc_Cliente f_Validar(string s_codcli) //VALIDA CLIENTES.txt
        {
            cls_vc_Cliente objCliente = new cls_vc_Cliente();
            objCliente.RucCed = s_codcli;

            if (s_codcli.Length == 10) //si es CEDULA
            {
                objCliente.TipInd = "2";
                objCliente.TipEmp = "3"; // NO ESTABA EN VALIDACION ORIGINAL
                if (s_codcli.Substring(2, 1).Equals("0") || s_codcli.Substring(2, 1).Equals("1") ||
                    s_codcli.Substring(2, 1).Equals("2") || s_codcli.Substring(2, 1).Equals("3") ||
                    s_codcli.Substring(2, 1).Equals("4") || s_codcli.Substring(2, 1).Equals("5"))
                {
                    objCliente.TipEmp = "3";
                }
                else
                {
                    objCliente.TipEmp = "-"; //para mandarle al error en f_ValidarCedula
                    objCliente.ErrorCed = "3er digito de cédula incorrecto.";
                }
            }
            else
            {
                if (s_codcli.Length == 13) //si es RUC
                {
                    objCliente.TipInd = "1";
                    if (s_codcli.Substring(2, 1).Equals("9"))
                    {
                        objCliente.TipEmp = "1";
                    }
                    else
                    {
                        if (s_codcli.Substring(2, 1).Equals("6"))
                        {
                            objCliente.TipEmp = "2";
                        }
                        else
                        {
                            objCliente.TipEmp = "3";
                        }
                    }
                }
                else
                {
                    if (s_codcli.Length == 9) //si es PASAPORTE
                    {
                        objCliente.TipInd = "3";
                        objCliente.TipEmp = "3";
                    }
                    else //ERROR, NO ES NI CEDULA NI RUC NI PASAPORTE (tiene digitos de mas o de menos)
                    {
                        objCliente.TipInd = "-";
                        objCliente.TipEmp = "-";
                        objCliente.RucCed = "-";
                    }
                }
            }

            string s_tipemp = objCliente.TipEmp.ToString();

            string s_error = objCliente.f_ValidacionCedula(s_codcli, s_tipemp);
            if (!s_error.Equals("CORRECTO"))
            {
                objCliente.ErrorCed = objCliente.f_ValidacionCedula(s_codcli, s_tipemp);
                objCliente.TipInd = "-";
                objCliente.TipEmp = "-";
                objCliente.RucCed = "-";
            }

            return objCliente;
        }

        public void f_limpiarModalCliente()
        {
            txtCodcli.Value = txtNombre1.Value = txtNombre2.Value = txtApellido1.Value = txtApellido2.Value =
                txtNombreCompleto.Value = txtFecnac.Value = txtCalle0.Value = txtNo.Value = txtProvincia.Value =
                txtCiudad.Value = txtParroquia.Value = txtNaccli.Value = txtTelCel.Value = txtTelCasa.Value =
                txtTelPrin.Value = txtEmail.Value = "";

        }

        //***f CLIENTE ***//

        //***i GRVCLIENTE ***//
        protected void grvCliente_RowDataBound(object sender, GridViewRowEventArgs e) //only fires when the GridView's data changes during the postback
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;

            }
        }
        protected void grvCliente_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {

            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }



        }
        protected void grvCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvCliente.SelectedRow;
            string s_codcli = fila.Cells[2].Text.Trim();

            Session["gs_codcliSelected"] = s_codcli; //variable para que al seleccionar uno en el grv se obtenga el codcli (sg_codcliSELECTED ->Editar)

            objCliente = objCliente.f_Cliente_Buscar(Session["gs_Codemp"].ToString(), s_codcli);
            if (String.IsNullOrEmpty(objCliente.Error.Mensaje))
            {
                f_llenarCamposCliente(objCliente);

            }
            else
            {
                f_ErrorNuevo(objCliente.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        private void f_BindGrid_Inicial()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsCliente.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();
            Session.Add("gdt_Cliente", dt);

            grvCliente.DataSource = dt;
            f_BindGrid();
        }
        private void f_BindGrid(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Cliente"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "nomcli LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codcli LIKE " + "'%" + searchText.Trim() + "%'";
                    grvCliente.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirection;
                    grvCliente.DataSource = dv;
                }
                grvCliente.DataBind();
            }
        }
        private string SortDirection
        {
            get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }
        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(null, txtSearchCliente.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalCliente();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCliente.PageIndex = e.NewPageIndex;
            f_BindGrid(null, txtSearchCliente.Text);
        }
        protected void OnSorting(object sender, GridViewSortEventArgs e)
        {
            f_BindGrid(e.SortExpression, txtSearchCliente.Text);
        }
        public void f_llenarCamposCliente(cls_vc_Cliente objCliente)
        {
            txtNomcxc.Text = objCliente.Nomcli;
            //Session["gs_Siguiente"] = objFactura.Nomcli;
            txtClicxc.Value = objCliente.Codcli;
            txtDircli.Text = objCliente.Dircli;
            ddlLisprefac.SelectedValue = objCliente.Lispre;
            txtEmailfac.Text = objCliente.Email;
            txtCelular.Text = objCliente.Telcli;

            //txtObserv.Text = "";
            //txtNtardes.Text = "";
            //txtNumfac.Text = "";
        }

        //***f GRVCLIENTE ***//


        //***i CODIGO DE RENGLONES ***//
        private void f_SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("N°", typeof(string)));
            dt.Columns.Add(new DataColumn("codart", typeof(string)));
            dt.Columns.Add(new DataColumn("nomart", typeof(string)));
            dt.Columns.Add(new DataColumn("cantid", typeof(decimal)));
            dt.Columns.Add(new DataColumn("preuni", typeof(decimal)));
            dt.Columns.Add(new DataColumn("ivacal", typeof(decimal)));
            dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
            dt.Columns.Add(new DataColumn("desren", typeof(decimal)));
            dt.Columns.Add(new DataColumn("coddes", typeof(string)));
            dt.Columns.Add(new DataColumn("totren", typeof(decimal)));

            dr = dt.NewRow();
            dr["N°"] = 1;
            dr["codart"] = string.Empty;
            dr["nomart"] = string.Empty;
            dr["cantid"] = 0.00;
            dr["preuni"] = 0.00;
            dr["ivacal"] = 0.00;
            dr["poriva"] = 0.00;
            dr["desren"] = 0.00;
            dr["coddes"] = string.Empty;
            dr["totren"] = 0.00;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            grvFacturaRen.DataSource = dt;
            grvFacturaRen.DataBind();
        }
        private void f_AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values (OJO el .Cells[2] no importa porq esta buscando el control con rowIndex)
                        TextBox codart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        TextBox cantid = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvCantid");
                        TextBox preuni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvPreuni");
                        TextBox ivacal = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvIvacal");
                        TextBox poriva = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvPoriva");
                        TextBox desren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvDesren");
                        TextBox coddes = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[8].FindControl("txtgrvCoddes");
                        TextBox totren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[9].FindControl("txtgrvTotren");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["codart"] = codart.Text;
                        dtCurrentTable.Rows[i - 1]["nomart"] = nomart.Text;
                        dtCurrentTable.Rows[i - 1]["coddes"] = coddes.Text;
                        if (cantid.Text == "" && preuni.Text == "" && poriva.Text == "" && desren.Text == "" && totren.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["preuni"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["ivacal"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["poriva"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["desren"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["totren"] = 0.00;
                        }
                        else
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = String.IsNullOrEmpty(cantid.Text) ? 0 : Convert.ToDecimal(cantid.Text);
                            dtCurrentTable.Rows[i - 1]["preuni"] = String.IsNullOrEmpty(preuni.Text) ? 0 : Convert.ToDecimal(preuni.Text);
                            dtCurrentTable.Rows[i - 1]["ivacal"] = String.IsNullOrEmpty(ivacal.Text) ? 0 : Convert.ToDecimal(ivacal.Text);
                            dtCurrentTable.Rows[i - 1]["poriva"] = String.IsNullOrEmpty(poriva.Text) ? 0 : Convert.ToDecimal(poriva.Text);
                            dtCurrentTable.Rows[i - 1]["desren"] = String.IsNullOrEmpty(desren.Text) ? 0 : Convert.ToDecimal(desren.Text);
                            dtCurrentTable.Rows[i - 1]["totren"] = String.IsNullOrEmpty(totren.Text) ? 0 : Convert.ToDecimal(totren.Text);
                        }     
                        rowIndex++;
                    }

                    drCurrentRow["ivacal"] = 0.00;
                    drCurrentRow["cantid"] = 0.00;
                    drCurrentRow["preuni"] = 0.00;
                    drCurrentRow["poriva"] = 0.00;
                    drCurrentRow["desren"] = 0.00;
                    drCurrentRow["totren"] = 0.00;
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    
                    ViewState["CurrentTable"] = dtCurrentTable;

                    grvFacturaRen.DataSource = dtCurrentTable;
                    grvFacturaRen.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            f_SetPreviousData();
        }
        private void f_SetPreviousData() //para actualizar el grvFacturaRen con CurrentTable (llenar los textbox del grv)
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox codart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        TextBox cantid = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvCantid");
                        TextBox preuni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvPreuni");
                        TextBox ivacal = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvIvacal");
                        TextBox proiva = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvPoriva");
                        TextBox desren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvDesren");
                        TextBox coddes = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[8].FindControl("txtgrvCoddes");
                        TextBox totren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[9].FindControl("txtgrvTotren");

                        codart.Text = dt.Rows[i]["codart"].ToString();
                        nomart.Text = dt.Rows[i]["nomart"].ToString();                        
                        cantid.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["cantid"]);
                        preuni.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["preuni"]);
                        ivacal.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["ivacal"]);
                        proiva.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["poriva"]);
                        desren.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["desren"]);
                        coddes.Text = dt.Rows[i]["coddes"].ToString();
                        totren.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["totren"]);

                        rowIndex++;
                    }
                }
            }
        }

        //*i BOTONES INFERIOR *//
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            f_AddNewRowToGrid();
        }
        protected void btnEliminarTodo_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];

            for (int i = 0; i < grvFacturaRen.Rows.Count; i++)
            {
                int rowID = grvFacturaRen.SelectedIndex + 1;
                if (ViewState["CurrentTable"] != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (grvFacturaRen.SelectedIndex < dt.Rows.Count - 1)
                        {
                            //Remove the Selected Row data
                            dt.Rows.RemoveAt(i);
                        }
                    }

                    //Store the current data in ViewState for future reference
                    ViewState["CurrentTable"] = dt;
                    //Re bind the GridView for the updated data
                    grvFacturaRen.DataSource = dt;
                    grvFacturaRen.DataBind();
                    i = 0;
                }
            }
            //para borrar los datos de la primera fila....sobra despues de que se ha eliminado todos los demas renglones
            dt.Rows[0]["codart"] = "";
            dt.Rows[0]["nomart"] = "";
            dt.Rows[0]["cantid"] = 0.00;
            dt.Rows[0]["preuni"] = 0.00;
            dt.Rows[0]["ivacal"] = 0.00;
            dt.Rows[0]["poriva"] = 0.00;
            dt.Rows[0]["desren"] = 0.00;
            dt.Rows[0]["coddes"] = "";
            dt.Rows[0]["totren"] = 0.00;

            //Set Previous Data on Postbacks
            f_SetPreviousData();

            f_calcularTblTotales();
        }
        protected void lkgrvBorrar_Click(object sender, EventArgs e)
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox codart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        TextBox cantid = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvCantid");
                        TextBox preuni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvPreuni");
                        TextBox ivacal = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvIvacal");
                        TextBox poriva = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvPoriva");
                        TextBox desren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvDesren");
                        TextBox coddes = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[8].FindControl("txtgrvCoddes");
                        TextBox totren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[9].FindControl("txtgrvTotren");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["codart"] = codart.Text;
                        dtCurrentTable.Rows[i - 1]["nomart"] = nomart.Text;
                        dtCurrentTable.Rows[i - 1]["coddes"] = coddes.Text;
                        if (cantid.Text == "" && preuni.Text == "" && poriva.Text == "" && desren.Text == "" && totren.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["preuni"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["ivacal"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["poriva"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["desren"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["totren"] = 0.00;
                        }
                        else
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = String.IsNullOrEmpty(cantid.Text) ? 0 : Convert.ToDecimal(cantid.Text);
                            dtCurrentTable.Rows[i - 1]["preuni"] = String.IsNullOrEmpty(preuni.Text) ? 0 : Convert.ToDecimal(preuni.Text);
                            dtCurrentTable.Rows[i - 1]["ivacal"] = String.IsNullOrEmpty(ivacal.Text) ? 0 : Convert.ToDecimal(ivacal.Text);
                            dtCurrentTable.Rows[i - 1]["poriva"] = String.IsNullOrEmpty(poriva.Text) ? 0 : Convert.ToDecimal(poriva.Text);
                            dtCurrentTable.Rows[i - 1]["desren"] = String.IsNullOrEmpty(desren.Text) ? 0 : Convert.ToDecimal(desren.Text);
                            dtCurrentTable.Rows[i - 1]["totren"] = String.IsNullOrEmpty(totren.Text) ? 0 : Convert.ToDecimal(totren.Text);
                        }


                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }

            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (!(gvRow.RowIndex == 0 && dt.Rows.Count == 1)) //no permite borrar la primera fila
                    {
                        //Remove the Selected Row data
                        dt.Rows.Remove(dt.Rows[rowID]);
                    }
                }
                else //si selecciona la primera fila
                {
                    if (gvRow.RowIndex == 0 && dt.Rows.Count == 1) //para borrar los datos de la primera fila
                    {
                        dt.Rows[0]["codart"] = "";
                        dt.Rows[0]["nomart"] = "";
                        dt.Rows[0]["cantid"] = 0.00;
                        dt.Rows[0]["preuni"] = 0.00;
                        dt.Rows[0]["ivacal"] = 0.00;
                        dt.Rows[0]["poriva"] = 0.00;
                        dt.Rows[0]["desren"] = 0.00;
                        dt.Rows[0]["coddes"] = "";
                        dt.Rows[0]["totren"] = 0.00;
                    }
                }
                //Store the current data in ViewState for future reference
                ViewState["CurrentTable"] = dt;
                //Re bind the GridView for the updated data
                grvFacturaRen.DataSource = dt;
                grvFacturaRen.DataBind();
            }
            //Set Previous Data on Postbacks
            f_SetPreviousData();
            f_calcularTblTotales();
        }
        //*f BOTONES INFERIOR *//

        //*i SELECCION ARTICULOS *//
        protected void btnPuntitosRen_Click(object sender, EventArgs e) //Abrir Modal Articulos
        {
            Button btn = (Button)sender;
            GridViewRow gvRow = (GridViewRow)btn.NamingContainer;
            int rowIndex = gvRow.RowIndex; //para saber la fila seleccionada
            Session.Add("gs_renSelecxtedRow", rowIndex); //para saber en donde poner el articulo seleccionado del grvArticulos

            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalFacturaArticulos();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void grvArticulo_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }
        }
        protected void grvArticulo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = grvArticulo.SelectedRow;
            string s_codart = row.Cells[2].Text.Trim();
            string s_nomart = Server.HtmlDecode(row.Cells[3].Text.Trim()); //para que acepte caracteres especiales (Ñ)
            string s_preuni = row.Cells[4].Text.Trim();

            objFacturaRen.Codart = s_codart;
            objFacturaRen.Nomart = s_nomart;
            objFacturaRen.Preuni = decimal.Parse(s_preuni);
            objFacturaRen.Cantid = 1;

            int rowIndex = int.Parse(Session["gs_renSelecxtedRow"].ToString());
            f_llenarCamporFacturasRen(objFacturaRen, rowIndex);
        }
        private void f_BindGrid_InicialArt()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsArticulo.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();
            Session.Add("gdt_Articulo", dt);

            grvArticulo.DataSource = dt;
            f_BindGridArt();
        }
        private void f_BindGridArt(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Articulo"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "nomart LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codart LIKE " + "'%" + searchText.Trim() + "%'";
                    grvArticulo.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirectionArt = this.SortDirectionArt == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirectionArt;
                    grvArticulo.DataSource = dv;
                }
                grvArticulo.DataBind();
            }
        }
        private string SortDirectionArt
        {
            get { return ViewState["SortDirectionArt"] != null ? ViewState["SortDirectionArt"].ToString() : "ASC"; }
            set { ViewState["SortDirectionArt"] = value; }
        }
        protected void f_GridBuscarArt(object sender, EventArgs e)
        {
            f_BindGridArt(null, txtSearchArt.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalFacturaArticulos();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void OnPageIndexChangingArt(object sender, GridViewPageEventArgs e)
        {
            grvArticulo.PageIndex = e.NewPageIndex;
            f_BindGridArt(null, txtSearchArt.Text);
        }
        protected void OnSortingArt(object sender, GridViewSortEventArgs e)
        {
            f_BindGridArt(e.SortExpression, txtSearchArt.Text);
        }
        //*f SELECCION ARTICULOS *//


        //*i CALCULOS RENGLON*//
        public void f_recalcularRenglon(int i_rowID)
        {
            GridViewRow row = grvFacturaRen.Rows[i_rowID];

            try
            {
                TextBox txtgrvCantid = (TextBox)row.FindControl("txtgrvCantid");
                if (String.IsNullOrEmpty(txtgrvCantid.Text))
                {
                    txtgrvCantid.Text = "1";
                }
                objFacturaRen.Cantid = decimal.Parse(txtgrvCantid.Text);

                TextBox txtgrvPreuni = (TextBox)row.FindControl("txtgrvPreuni");
                if (String.IsNullOrEmpty(txtgrvPreuni.Text))
                {
                    txtgrvPreuni.Text = "0.00";
                }
                objFacturaRen.Preuni = decimal.Parse(txtgrvPreuni.Text);

                TextBox txtgrvCodart = (TextBox)row.FindControl("txtgrvCodart");
                objFacturaRen.Codart = txtgrvCodart.Text;

                TextBox txtgrvNomart = (TextBox)row.FindControl("txtgrvNomart");
                objFacturaRen.Nomart = txtgrvNomart.Text;

                f_llenarCamporFacturasRen(objFacturaRen, i_rowID);
                
            }
            catch (Exception ex)
            {
                objFacturaRen.Error = new clsError();
                objFacturaRen.Error = objFacturaRen.Error.f_ErrorControlado(ex);
                f_ErrorNuevo(objFacturaRen.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void txtgrvCantid_TextChanged(object sender, EventArgs e)
        {
            TextBox tx = (TextBox)sender;
            GridViewRow gvRow = (GridViewRow)tx.NamingContainer;
            int i_rowID = gvRow.RowIndex;//para saber la fila seleccionada
            f_recalcularRenglon(i_rowID);
        }
        protected void txtgrvPreuni_TextChanged(object sender, EventArgs e)
        {
            TextBox tx = (TextBox)sender;
            GridViewRow gvRow = (GridViewRow)tx.NamingContainer;
            int i_rowID = gvRow.RowIndex;//para saber la fila seleccionada
            f_recalcularRenglon(i_rowID);
        }
        public string f_CalculoPorIva(string s_codart)
        {
            objFacturaRen = objFacturaRen.f_CalculoPorIva(Session["gs_Codemp"].ToString(), s_codart);
            if (!String.IsNullOrEmpty(objFacturaRen.Error.Mensaje)) //si da error muestra f_ModalError() y objFacturaRen.Poriva vale "0"
            {
                f_ErrorNuevo(objFacturaRen.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            return objFacturaRen.Poriva;
        }
        public string f_descuentoArticulo(string s_codart)
        {
            //Session["gs_CodEmp"]
            //string s_codart = "6301132-04";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_PV_M_AUT_DESCUENTO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", "01"); //OJO CODEMP SE ESTA MANDANDO QUEMADO 
            cmd.Parameters.AddWithValue("@CODART", s_codart);
            cmd.Parameters.AddWithValue("@FECHA", "2016-08-08"); //para validar si el decuento aplica

            cmd.Parameters.Add("@s_Coddes", SqlDbType.Char, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@s_Aplica", SqlDbType.Char, 15).Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string descuento = cmd.Parameters["@s_Coddes"].Value.ToString().Trim();
            conn.Close();

            return descuento;


            //realizar operaciones de decuento
            //if (descuento.Equals("3X1"))
            //{
                //campo de descuento = 
            //}

        }
        //*f CALCULOS RENGLON*//


        //*i CALCULOS DERECHA*//
        public void f_calcularTblTotales() //tblTotales es la tabla de la derecha.
        {
            f_renglonesActuales();
            double r_totNeto = f_cantTotal();
            double r_totBas = f_cantBaseImponible();
            double r_totIva = r_totBas * 0.12;
            double r_totFac = r_totNeto + r_totIva;
            txtTotnet.Text = r_totNeto.ToString("N2");
            txtTotbas.Text = r_totBas.ToString("N2");
            txtTotiva.Text = r_totIva.ToString("N2");
            txtTotfac.Text = r_totFac.ToString("N2");
        }
        public double f_cantBaseImponible() //recorre todos los datos de la columna total, sumando solo los del 12% IVA
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            int nrow = dt.Rows.Count;
            int i = 0;
            double r_ctotal = 0;
            try
            {
                while (i < nrow)
                {
                    if (!dt.Rows[i]["totren"].ToString().Equals("") && !dt.Rows[i]["ivacal"].ToString().Equals("12.00")) //si txtgrvTotal NO esta vacio suma
                    {
                        r_ctotal = r_ctotal + double.Parse(dt.Rows[i]["totren"].ToString());
                    }
                    i = i + 1;
                }
            }
            catch (Exception)
            {
                Response.Write("<script>alert('Todos los totales deben estar llenos.');</script>");
            }

            return r_ctotal;
        }
        public double f_cantTotal()//para txtTotnet ...recorre todos los datos de la columna total, sumando los num para sacar la cantidad total
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            int nrow = dt.Rows.Count;
            int i = 0;
            double r_ctotal = 0;
            try
            {
                while (i < nrow)
                {
                    if (!dt.Rows[i]["totren"].ToString().Equals("")) //si txtgrvTotal NO esta vacio suma
                    {
                        r_ctotal = r_ctotal + double.Parse(dt.Rows[i]["totren"].ToString());
                    }
                    i = i + 1;
                }
            }
            catch (Exception)
            {
                Response.Write("<script>alert('Todos los totales deben estar llenos.');</script>");
            }

            return r_ctotal;
        }
        public void f_renglonesActuales() //para guardar en ViewState["CurrentTable"] los renglones mostrados en pantalla
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox codart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        TextBox cantid = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvCantid");
                        TextBox preuni = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[4].FindControl("txtgrvPreuni");
                        TextBox ivacal = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[5].FindControl("txtgrvIvacal");
                        TextBox poriva = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[6].FindControl("txtgrvPoriva");
                        TextBox desren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[7].FindControl("txtgrvDesren");
                        TextBox coddes = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[8].FindControl("txtgrvCoddes");
                        TextBox totren = (TextBox)grvFacturaRen.Rows[rowIndex].Cells[9].FindControl("txtgrvTotren");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["codart"] = codart.Text;
                        dtCurrentTable.Rows[i - 1]["nomart"] = nomart.Text;
                        dtCurrentTable.Rows[i - 1]["coddes"] = coddes.Text;
                        if (cantid.Text == "" && preuni.Text == "" && poriva.Text == "" && desren.Text == "" && totren.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["preuni"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["ivacal"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["poriva"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["desren"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["totren"] = 0.00;
                        }
                        else
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = String.IsNullOrEmpty(cantid.Text) ? 0 : Convert.ToDecimal(cantid.Text);
                            dtCurrentTable.Rows[i - 1]["preuni"] = String.IsNullOrEmpty(preuni.Text) ? 0 : Convert.ToDecimal(preuni.Text);
                            dtCurrentTable.Rows[i - 1]["ivacal"] = String.IsNullOrEmpty(ivacal.Text) ? 0 : Convert.ToDecimal(ivacal.Text);
                            dtCurrentTable.Rows[i - 1]["poriva"] = String.IsNullOrEmpty(poriva.Text) ? 0 : Convert.ToDecimal(poriva.Text);
                            dtCurrentTable.Rows[i - 1]["desren"] = String.IsNullOrEmpty(desren.Text) ? 0 : Convert.ToDecimal(desren.Text);
                            dtCurrentTable.Rows[i - 1]["totren"] = String.IsNullOrEmpty(totren.Text) ? 0 : Convert.ToDecimal(totren.Text);
                        }

                        
                      

                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }
        }
        //*f CALCULOS DERECHA*//

        //***f CODIGO DE RENGLONES ***//

        public void f_llenarCamposFacturaEnc(cls_vc_Factura objFactura)
        {
            txtNomcxc.Text = objFactura.Nomcxc;
            //Session["gs_Siguiente"] = objFactura.Nomcli;

            txtClicxc.Value = objFactura.Clicxc;
            txtDircli.Text = objFactura.Dircli;
            txtObserv.Text = objFactura.Observ;
            txtNtardes.Text = objFactura.Ntardes;
            txtNumfac.Text = objFactura.Numfac;

            objCliente = objCliente.f_Cliente_Buscar(Session["gs_Codemp"].ToString(), objFactura.Clicxc);
            if (String.IsNullOrEmpty(objCliente.Error.Mensaje))
            {
                ddlLisprefac.SelectedValue = objCliente.Lispre;
                txtEmailfac.Text = objCliente.Email;
                txtCelular.Text = objCliente.Telcli;
            }
            else
            {
                f_ErrorNuevo(objCliente.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

            txtTotnet.Text = objFactura.Totnet.ToString();
            txtTotbas.Text = objFactura.Totbas.ToString();
            txtTotdes.Text = objFactura.Totdes.ToString();
            txtTotiva.Text = objFactura.Totiva.ToString();
            txtTotfac.Text = objFactura.Totfac.ToString();

            string s_Ttardes = objFactura.Ttardes;
            string s_codalm = objFactura.Codalm;
            string s_codven = objFactura.Codven;
            string s_estado = objFactura.Estado;
            string s_lispre = objFactura.Lispre;

            //if (String.IsNullOrEmpty(s_Ttardes))
            //    ddlTtardes.SelectedValue = ddlTtardes.Items.FindByValue("00   ").Value;
            //else
            //    ddlTtardes.SelectedValue = ddlTtardes.Items.FindByValue(s_Ttardes).Value;


            if (String.IsNullOrEmpty(s_codven))
            {
                if (ddlCodven.Items.Contains(new ListItem(Session["gs_CodUs1"].ToString()))) //revisa si CODUS1 esta en ddlCodven, lo pone
                    ddlCodven.SelectedValue = ddlCodven.Items.FindByValue(Session["gs_CodUs1"].ToString()).Value;
                else //si no esta pone el 00 por defecto
                    ddlCodven.SelectedValue = ddlCodven.Items.FindByValue("00").Value;
            }                
            else
                ddlCodven.SelectedValue = ddlCodven.Items.FindByValue(s_codven.Trim()).Value;
            if (String.IsNullOrEmpty(s_codalm))
                txtCodalm.Text = Session["gs_CodAlm"].ToString(); 
            else
                txtCodalm.Text = objFactura.Codalm;
            if (String.IsNullOrEmpty(s_estado))
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue("P").Value;
            else
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue(s_estado.Trim()).Value;
            if (String.IsNullOrEmpty(s_lispre))
                ddlLisprefac.SelectedValue = ddlLisprefac.Items.FindByValue("1").Value;
            else
                ddlLisprefac.SelectedValue = ddlLisprefac.Items.FindByValue(s_lispre.Trim()).Value;

            txtFecfac.Text = String.IsNullOrEmpty(objFactura.Fecfac.ToString()) ? DateTime.Now.Date.ToString("yyyy-MM-dd") : objFactura.Fecfac.ToString("yyyy-MM-dd");
        }
        public void f_llenarCamporFacturasRen(cls_vc_Factura_Ren objFacturaRen, int rowIndex)
        {
            GridViewRow row = grvFacturaRen.Rows[rowIndex];

            TextBox txtgrvCodart = (TextBox)row.FindControl("txtgrvCodart");
            TextBox txtgrvNomart = (TextBox)row.FindControl("txtgrvNomart");
            TextBox txtgrvCantid = (TextBox)row.FindControl("txtgrvCantid");
            TextBox txtgrvPreuni = (TextBox)row.FindControl("txtgrvPreuni");
            TextBox txtgrvIvacal = (TextBox)row.FindControl("txtgrvIvacal");
            TextBox txtgrvPoriva = (TextBox)row.FindControl("txtgrvPoriva");
            TextBox txtgrvCoddes = (TextBox)row.FindControl("txtgrvCoddes");
            TextBox txtgrvDesren = (TextBox)row.FindControl("txtgrvDesren");
            TextBox txtgrvTotren = (TextBox)row.FindControl("txtgrvTotren");

            txtgrvCodart.Text = objFacturaRen.Codart;
            txtgrvNomart.Text = objFacturaRen.Nomart;
            txtgrvCantid.Text = objFacturaRen.Cantid.ToString();
            double r_preuni = (double)objFacturaRen.Preuni; //para solo mostrar 2 decimales
            txtgrvPreuni.Text = r_preuni.ToString("N2");
            string s_poriva = !objFacturaRen.Codart.Trim().Equals("*") ? f_CalculoPorIva(objFacturaRen.Codart) : "0";
            txtgrvPoriva.Text = s_poriva;
            double r_poriva = double.Parse(s_poriva);
            double r_totIva = r_preuni + r_preuni * (r_poriva / 100);
            txtgrvIvacal.Text = r_totIva.ToString("N2");
            //string s_coddes = f_descuentoArticulo(s_codart);
            string s_coddes = "0"; //OJO borra esto y descomentar ^^^ al programar lo de decuentos
            txtgrvCoddes.Text = s_coddes;
            double r_desren = 0.00;
            if (!s_coddes.Equals("3X1") && !s_coddes.Equals("-")) //EN Equals("") PONER TODOS LOS DESCUENTOS QUE NO SEAN % Y SE GUARDEN EN LA TABLA....ejm: 3X1
            {
                s_coddes = new string(s_coddes.Where(c => char.IsDigit(c)).ToArray()); //si es 20% devuelve 20
                r_desren = double.Parse(s_coddes);
            }
            //para aplicar descuento de tarjeta            
            if (Session["gs_Destar"] != null)
            {
                r_desren = r_desren + double.Parse(Session["gs_Destar"].ToString());
            }
            txtgrvDesren.Text = r_desren.ToString("N2");
            double r_totRen = (double)objFacturaRen.Cantid * r_preuni;
            r_totRen = r_totRen - (r_totRen * (r_desren / 100)); //resta el % de descuento 
            txtgrvTotren.Text = r_totRen.ToString("N2");

            if (!objFacturaRen.Codart.Trim().Equals("*"))
                f_calcularTblTotales();
                        
        }
        protected void btnFormaDePago_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session.Add("g_objFactura", f_generarObjFactura());
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            Session.Add("gdt_FacturaRen", dt);
            //Session.Add("gs_Existe", "Add");

            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsFacturaExiste.Select(args);
            if (view != null) //ya existe la factura (Actualizar)
                Session.Add("gs_Existe", "Update");
            else //no existe la factura (Insertar)
                Session.Add("gs_Existe", "Add");
            if (String.IsNullOrEmpty(txtClicxc.Value)) lblError.Text = "❗ Debe seleccionar un cliente.";
            if (dt.Rows.Count == 1 && String.IsNullOrEmpty(dt.Rows[0]["codart"].ToString())) lblError.Text = "❗ Factura no tiene renglones.";
            if (String.IsNullOrEmpty(lblError.Text)) Response.Redirect("w_vc_FacturaFormaPago.aspx");
        }
        public void f_limpiarCampos()
        {
            //Image2.ImageUrl = "~/imgusu/user.png";
            txtNomcxc.Text = txtClicxc.Value = txtDircli.Text  = txtObserv.Text = "";
            ddlLisprefac.SelectedIndex = 0;            
            objSecuencia = objSecuencia.f_CalcularSecuencia(Session["gs_Codemp"].ToString(), "VC_FAC", Session["gs_Sersec"].ToString());
            objFactura.Numfac = objSecuencia.Numero;
            objFactura.Fecfac = objSecuencia.Fecha;
            if (String.IsNullOrEmpty(objSecuencia.Error.Mensaje))
            {
                f_llenarCamposFacturaEnc(objFactura);
            }
            else
            {
                f_ErrorNuevo(objFactura.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

            DataTable dt = (DataTable)ViewState["CurrentTable"];
            for (int i = 0; i < grvFacturaRen.Rows.Count; i++)
            {
                int rowID = grvFacturaRen.SelectedIndex + 1;
                if (ViewState["CurrentTable"] != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (grvFacturaRen.SelectedIndex < dt.Rows.Count - 1)
                        {
                            //Remove the Selected Row data
                            dt.Rows.RemoveAt(i);
                        }
                    }

                    //Store the current data in ViewState for future reference
                    ViewState["CurrentTable"] = dt;
                    //Re bind the GridView for the updated data
                    grvFacturaRen.DataSource = dt;
                    grvFacturaRen.DataBind();
                    i = 0;
                }
            }

            //para borrar los datos de la primera fila....sobra despues de que se ha eliminado todos los demas renglones
            if (grvFacturaRen.Rows.Count == 0)
            {
                f_AddNewRowToGrid();
            }
            else
            {
                dt.Rows[0]["codart"] = "";
                dt.Rows[0]["nomart"] = "";
                dt.Rows[0]["cantid"] = 0.00;
                dt.Rows[0]["preuni"] = 0.00;
                dt.Rows[0]["ivacal"] = 0.00;
                dt.Rows[0]["poriva"] = 0.00;
                dt.Rows[0]["desren"] = 0.00;
                dt.Rows[0]["coddes"] = "";
                dt.Rows[0]["totren"] = 0.00;
            }
            //Set Previous Data on Postbacks
            f_SetPreviousData();
            f_calcularTblTotales();

            lblError.Text = lblError.Text =  "";
        }
        public cls_vc_Factura f_generarObjFactura()
        {
            //CAMPOS NORMALES
            objFactura.Codemp = Session["gs_Codemp"].ToString();
            objFactura.Codusu = Session["gs_CodUs1"].ToString();
            objFactura.Codcli = txtClicxc.Value;
            objFactura.Nomcli = txtNomcxc.Text;
            objFactura.Nomcxc = txtNomcxc.Text;
            objFactura.Clicxc = txtClicxc.Value;
            objFactura.Dircli = txtDircli.Text;
            objFactura.Lispre = ddlLisprefac.Text;
            objFactura.Observ = txtObserv.Text;
            objFactura.Ntardes = txtNtardes.Text;
            objFactura.Numfac = txtNumfac.Text;
            objFactura.Fecfac = DateTime.Parse(txtFecfac.Text);
            objFactura.Estado = ddlEstado.Text;
            objFactura.Codalm = txtCodalm.Text;
            objFactura.Codven = ddlCodven.Text;
            objFactura.Totnet = String.IsNullOrEmpty(txtTotnet.Text) ? 0 : decimal.Parse(txtTotnet.Text);
            objFactura.Totbas = String.IsNullOrEmpty(txtTotbas.Text) ? 0 : decimal.Parse(txtTotbas.Text);
            objFactura.Totdes = String.IsNullOrEmpty(txtTotdes.Text) ? 0 : decimal.Parse(txtTotdes.Text);
            objFactura.Totiva = String.IsNullOrEmpty(txtTotiva.Text) ? 0 : decimal.Parse(txtTotiva.Text);
            objFactura.Totfac = String.IsNullOrEmpty(txtTotfac.Text) ? 0 : decimal.Parse(txtTotfac.Text);

            //DATATABLE FACTURA
            DataTable dt = objFactura.f_dtFacturaTableType();
            DataRow dr = null;
            dr = dt.NewRow();
            dr["codemp"] = Session["gs_Codemp"].ToString();
            dr["numfac"] = txtNumfac.Text;
            dr["fecfac"] = txtFecfac.Text;
            dr["estado"] = ddlEstado.Text;
            dr["clicxc"] = txtClicxc.Value;
            dr["nomcxc"] = txtNomcxc.Text;
            dr["codcli"] = txtClicxc.Value;
            dr["nomcli"] = txtNomcxc.Text;
            dr["sersec"] = Session["gs_Sersec"].ToString();
            dr["rucced"] = txtClicxc.Value;
            dr["dircli"] = txtDircli.Text;
            dr["lispre"] = ddlLisprefac.Text;
            dr["codven"] = ddlCodven.Text;
            dr["codalm"] = "001.01"; //txtCodalm.Text;
            dr["observ"] = txtObserv.Text;
            dr["poriva"] = 0;
            dr["pordes"] = 0;
            dr["totnet"] = txtTotnet.Text;
            dr["totdes"] = txtTotdes.Text;
            dr["totbas"] = txtTotbas.Text;
            dr["totiva"] = txtTotiva.Text;
            dr["totfac"] = txtTotfac.Text;
            dr["totcxc"] = txtTotfac.Text;
            dr["usuing"] = Session["gs_UsuIng"].ToString();
            dr["codusu"] = Session["gs_CodUs1"].ToString();
            dt.Rows.Add(dr);
            objFactura.dtFactura = dt;

            return objFactura;
        }
    }
}