﻿<%@ Page Title="Factura VistaPrevia" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_vc_FacturaVistaPrevia.aspx.cs" Inherits="prj_KohinorWeb.w_vc_FacturaVistaPrevia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .skills {
            width: 100%;
            max-width: 600px;
            padding: 0 20px;
        }

        .skill-name {
            font-size: 18px;
            font-weight: 700;
            text-transform: uppercase;
            margin: 20px 0;
        }

        .skill-bar {
            height: 20px;
            background: #cacaca;
            border-radius: 8px;
        }

        .skill-per {
            height: 20px;
            background-color: #0fbcf9;
            border-radius: 8px;
            width: 0;
            transition: 1s linear;
            position: relative;
        }

            .skill-per::before {
                content: attr(per);
                position: absolute;
                padding: 4px 6px;
                background-color: #000;
                color: #fff;
                font-size: 12px;
                border-radius: 4px;
                top: -35px;
                right: 0;
                transform: translateX(50%);
            }

            .skill-per::after {
                content: '';
                position: absolute;
                width: 10px;
                height: 10px;
                background-color: #000;
                top: -16px;
                right: 0;
                transform: translateX(50%) rotate(45deg);
                border-radius: 2px;
            }
    </style>
    <script type="text/javascript">
        function f_ModalXML() {
            $("#btnShowModalXML").click();
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item"><a href="w_vc_Factura.aspx">Facturas</a></li>
            <li class="breadcrumb-item active">Factura Vista Previa - <asp:Label ID="lblNumfacSelected" runat="server" Text=""></asp:Label></li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <%--<a class="c-subheader-nav-link">
                <i class="c-icon cil-speech"></i>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-graph"></i>&nbsp;
                            Escritorio
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-settings"></i>
            </a>--%>
        </div>
    </div>
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item active">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click"><i class="cil-file"></i>Nuevo</button>
                    <button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" onserverclick="btnAbrir_Click"><i class="cil-folder-open"></i>Abrir</button>
                    <button type="button" class="btn btn-outline-secondary disabled" id="btnGuardar" runat="server"><i class="cil-save"></i>Guardar</button>
                    <button type="button" class="btn btn-outline-secondary disabled" id="btnCancelar" runat="server"><i class="cil-grid-slash"></i>Cancelar</button>
                    <button type="button" class="btn btn-outline-secondary disabled" id="btnEliminar" runat="server"><i class="cil-trash"></i>Eliminar</button>
                </div>

                <asp:Button ID="btnImprimir" class="btn btn-outline-primary" runat="server" Text="Imprimir" OnClientClick="printdiv('div_print');" />
                <button id="btnVolver" type="button" runat="server" class="btn btn-outline-primary " onserverclick="btnVolver_Click"><span class="cil-arrow-circle-left"></span>Volver</button>
                <asp:Button ID="btnXML" class="btn btn-outline-success" runat="server" Text="Generar XML" OnClick="btnXML_Click" />
                <asp:Button ID="btnPDF" class="btn btn-outline-warning" runat="server" Text="Generar PDF" OnClick="btnPDF_Click" />
                <asp:Button ID="btnEmail" class="btn btn-outline-info" runat="server" Text="Enviar Email" OnClick="btnEmail_Click" />
                <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click"/>
                
                <%--<div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-step-backward"></i></button>
                    <button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-skip-backward"></i></button>
                    <button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-skip-forward"></i></button>
                    <button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-step-forward"></i></button>
                </div>--%>

                <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
            </li>
        </ol>
    </div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="card">
                    <%--<div class="card-header">
                        <i class="c-icon cil-justify-center"></i><b>Factura</b>
                    </div>--%>
                    <div class="card-body">
                        <div id="wrapper">
                            <script>
                                function printpage() {
                                    var getpanel = document.getElementById("<%= Panel1.ClientID%>");
                                    var MainWindow = window.open('', '', 'height=500,width=800');
                                    MainWindow.document.write('<html><head><title>IMPRIMIR</title>');
                                    MainWindow.document.write('</head><body>');
                                    MainWindow.document.write(getpanel.innerHTML);
                                    MainWindow.document.write('</body></html>');
                                    MainWindow.document.close();
                                    setTimeout(function () {
                                        MainWindow.print();
                                    }, 500);
                                    return false;
                                }

                                function printdiv(printpage) {
                                    var headstr = "<html><head><title></title></head><body>";
                                    var footstr = "</body></html>";
                                    var newstr = document.all.item(printpage).innerHTML;
                                    var oldstr = document.body.innerHTML;
                                    document.body.innerHTML = headstr + newstr + footstr;
                                    window.print();
                                    document.body.innerHTML = oldstr;
                                    return false;
                                }

                            </script>
                            <div class="container">
                                <div class="form-horizontal">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            
                                        </div>
                                    </div>
                                    <div class="row" align="center" id="div_print" style="background-color: #ffffff;">                                        
                                        <asp:Panel ID="Panel1" runat="server" Width="100%" BackColor="White">
                                        <hr style="width:100%;" />
                                            <div class="row align-items-center">
                                                <div class="col-md-7 text-center text-sm-left mb-3 mb-sm-0">
                                                    <img id="logoEmp" src="Icon/Menu125/powersoft.png" width="118" height="46" title="Koice" alt="Koice" />
                                                </div>
                                                <div class="col-sm-5 text-center text-sm-right">
                                                    <h4 class="mb-0">Factura</h4>
                                                </div>
                                            </div>
                                            <hr style="width:100%;" />
                                            <div class="row">
                                                <div class="col-sm-6" style="text-align: left;"><strong>Fecha  Emisión: </strong>
                                                    <asp:Label ID="lblFecfac" runat="server"></asp:Label></div>
                                                <div class="col-sm-6" style="text-align: right;"><strong>N° </strong><asp:Label ID="lblNumfac" runat="server" Font-Bold="True"></asp:Label></div>
                                            </div>
                                            <table class="table mt-2" style="width: 100%; text-align: center; table-layout: fixed; font-family: helvetica">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: left;">
                                                            <div><asp:Label ID="lblNomemp" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>RUC: <asp:Label ID="lblRuc" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Matriz: <asp:Label ID="lblDir01" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Sucursal: <asp:Label ID="lblDirsuc" runat="server" Font-Bold="True">???</asp:Label></div>
                                                            <div>Email: <asp:Label ID="lblEmail" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Teléfono: <asp:Label ID="lblTel01" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>OBLIGADO A LLEVAR CONTABILIDAD: <asp:Label ID="lblContab" runat="server" Font-Bold="True">???</asp:Label></div>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <div>Autorización SRI N° <asp:Label ID="lblNumaut" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Fecha Autorización SRI <asp:Label ID="lblFecaut" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Ambiente <asp:Label ID="Label1" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Emisión <asp:Label ID="Label2" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Clave Acceso</div>
                                                            <img id="logo" src="Icon/Menu125/claveacceso.png" width="250" height="30" />
                                                            <div><small>1103202101179136284500120070330000623490006234917</small></div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="table table-secondary" style="width: 100%; text-align: center; font-family: helvetica">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: left;">
                                                            <div>Razón Social:
                                                                <asp:Label ID="lblRazon" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Dirección:
                                                                <asp:Label ID="lblDircli" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Email:
                                                                <asp:Label runat="server" ID="lblEmail2" Font-Bold="True"></asp:Label></div>
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <div>RUC/C.I.:
                                                                <asp:Label ID="lblRucced" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Ciudad:
                                                                <asp:Label ID="lblCiucli" runat="server" Font-Bold="True"></asp:Label></div>
                                                            <div>Telf.:
                                                                <asp:Label ID="lblTelcli" runat="server" Font-Bold="True"></asp:Label></div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <asp:GridView ID="grvRenglones" runat="server" class="table table-sm table-bordered table-striped" style="text-align: center;" AutoGenerateColumns="False" DataSourceID="sqldsFacturaRenglones" GridLines="None" Width="100%">
                                                <%--<AlternatingRowStyle BackColor="White" ForeColor="Black" />--%>
                                                <Columns>
                                                    <asp:BoundField DataField="codart" HeaderText="Código" />
                                                    <asp:BoundField DataField="nomart" HeaderText="Descripción" />
                                                    <%--<asp:BoundField DataField="coduni" HeaderText="UND" />--%>
                                                    <asp:BoundField DataField="cantid" DataFormatString="{0:0}" HeaderText="Cantidad" />
                                                    <asp:BoundField DataField="preuni" DataFormatString="{0:0.00}" HeaderText="Precio Unitario" />
                                                    <asp:BoundField DataField="totdes" DataFormatString="{0:0.00}" HeaderText="Dscto" />
                                                    <asp:BoundField DataField="totren" DataFormatString="{0:0.00}" HeaderText="Precio Total" />
                                                </Columns>
                                                <%--<EditRowStyle BackColor="#999999"/>
                                                <FooterStyle BackColor="#c7c6c3" Font-Bold="True" ForeColor="Black" />
                                                <HeaderStyle BackColor="#c7c6c3" Font-Bold="True" ForeColor="Black" />
                                                <RowStyle BackColor="#F7F6F3" ForeColor="Black" />--%>
                                            </asp:GridView>
                                            <div class="row">
                                                <div class="col-7">
                                                    <asp:GridView ID="grvFormaPagoFactura" runat="server" class="table table-sm" style="text-align: center;" GridLines="None">

                                                        <%--<AlternatingRowStyle BackColor="White" ForeColor="Black" />
                                                        <FooterStyle BackColor="#c7c6c3" Font-Bold="True" ForeColor="Black" />
                                                        <HeaderStyle BackColor="#c7c6c3" Font-Bold="True" ForeColor="Black" />
                                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#F7F6F3" ForeColor="Black" />--%>

                                                    </asp:GridView>
                                                </div>
                                                <div class="col-5">
                                                    <table border="1" class="table table-sm table-bordered table-striped">
                                                        <tr>
                                                            <td><b>SUBTOTAL</b></td>
                                                            <td>
                                                                <asp:Label ID="lblTotnet" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>SUBTOTAL 12%</b></td>
                                                            <td>
                                                                <asp:Label ID="lblTotbas" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>SUBTOTAL 0%</b></td>
                                                            <td>
                                                                <asp:Label ID="lblSubtotal0" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>DESCUENTO</b></td>
                                                            <td>
                                                                <asp:Label ID="lblTotdes" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>IVA 12%</b></td>
                                                            <td>
                                                                <asp:Label ID="lblTotiva" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>VALOR TOTAL</b></td>
                                                            <td>
                                                                <asp:Label ID="lblTotfac" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>

                                            <br />
                                            <div class="row">
                                                <div class="col">
                                                    <table class="table table-striped table-sm table-active">
                                                        <tr>
                                                            <td align="center">
                                                                <small><b>INFORMACIÓN ADICIONAL</b></small>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <small>EL PAGO CON TRANSFERENCIA BANCARIA O DEPOSITO BANCO: PRODUBANCO<br />
                                                                    CUENTA CORRIENTE NUMERO: 02008009161 A NOMBRE DE POWERSOFT CIA LTDA RUC: 1791865170001<br />
                                                                    REPRESENTANTE LEGAL: EDWARD WLADIMIR DIAZ CORREA CI # 1102738943</small>
                                                                <br />
                                                                <label style="color: red"><small>ACEPTAMOS TODAS LAS TARJETAS DE CREDITO DIFERIDO HASTA 6 MESES SIN INTERESES</small></label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>                                                
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <!-- End of Main Content -->


    </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsFacturaRenglones" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="VC_S_FACTURA_IMPRIMIR" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="NUMFAC" SessionField="gs_numfacSelected" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsEncPuntosventaFull" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT * FROM [vc_factura] WHERE ([numfac] = @numfac)">
        <SelectParameters>
            <asp:SessionParameter Name="numfac" SessionField="gs_numfacSelected" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <!--f SQL DATASOURCES-->

    <!-- Modal XML Load -->
    <button id="btnShowModalXML" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#ModalXML"></button>
    <div class="modal fade" tabindex="-1" role="dialog" id="ModalXML">
        <div class="modal-dialog modal-dark" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                    <div class="skills">
                        <div class="skill">
                            <div class="skill-name">HTML</div>
                            <div class="skill-bar">
                                <div class="skill-per" per="100"></div>
                            </div>
                        </div>

                        <div class="skill">
                            <div class="skill-name">CSS</div>
                            <div class="skill-bar">
                                <div class="skill-per" per="70"></div>
                            </div>
                        </div>

                        <div class="skill">
                            <div class="skill-name">Javascript</div>
                            <div class="skill-bar">
                                <div class="skill-per" per="60"></div>
                            </div>
                        </div>
                    </div>

                    <script>
                        $('.skill-per').each(function () {
                            var $this = $(this);
                            var per = $this.attr('per');
                            $this.css("width", per + '%');
                            $({ animatedValue: 0 }).animate({ animatedValue: per }, {
                                duration: 1000,
                                step: function () {
                                    $this.attr('per', Math.floor(this.animatedValue) + '%');
                                },
                                complete: function () {
                                    $this.attr('per', Math.floor(this.animatedValue) + '%');
                                }
                            });
                        });
                    </script>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>
</asp:Content>

