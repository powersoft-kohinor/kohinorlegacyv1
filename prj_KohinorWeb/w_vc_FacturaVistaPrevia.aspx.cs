﻿using iText.Html2pdf;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
	public partial class w_vc_FacturaVistaPrevia : System.Web.UI.Page
	{
		protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
		protected string codmod = "3", niv001 = "2", niv002 = "1";
		protected cls_vc_Factura objFactura = new cls_vc_Factura();
		protected string s_nomemp, s_ruc, s_dir01, s_dir02, s_oblsri, s_contab, s_numfac, s_fecfac, s_tel01, s_nomcli, s_dircli,
			s_email, s_rucced, s_ciucli, s_telcli, s_totnet, s_totbas, s_subtot0, s_totdes, s_totiva, s_totfac;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				f_Seg_Usuario_Menu();
				if (Session["gs_numfacSelected"] == null) Response.Redirect("w_vc_Factura.aspx");
				lblNumfacSelected.Text = Session["gs_numfacSelected"].ToString();
				f_llenarCamposVistaPrev();
			}			
		}
		protected void Page_LoadComplete(object sender, EventArgs e)
		{
			if (Session["gs_Modo"].ToString() == "D")
			{
				btnAbrir.Attributes.Add("class", "btn btn-dark");
				btnNuevo.Attributes.Add("class", "btn btn-primary");
				btnGuardar.Attributes.Add("class", "btn btn-secondary disabled");
				btnCancelar.Attributes.Add("class", "btn btn-secondary disabled");
				btnEliminar.Attributes.Add("class", "btn btn-secondary disabled");
			}
			if (Session["gs_Modo"].ToString() == "L")
			{
				btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
				btnNuevo.Attributes.Add("class", "btn btn-outline-primary");
				btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
				btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
				btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
			}
		}
		protected void f_ErrorNuevo(clsError objError)
		{
			lblModalFecha.Text = objError.Fecha.ToString();
			txtModalNum.Text = objError.NoError;
			lblModalError.Text = objError.Mensaje;
			lblModalPagina.Text = objError.Donde;
			txtModalObjeto.Text = objError.Objeto;
			txtModalEvento.Text = objError.Evento;
			txtModalLinea.Text = objError.Linea;
		}
		protected void f_Seg_Usuario_Menu()
		{
			if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
			if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
			lblModuloActivo.Text = Session["gs_Nommod"].ToString();
			DataSourceSelectArguments args = new DataSourceSelectArguments();
			DataView view = (DataView)sqldsNivel001.Select(args);
			DataTable dt001 = view.ToTable();
			if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
			rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
			rptNivel001.DataBind();

			int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
			foreach (RepeaterItem item in rptNivel001.Items)
			{
				Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
				Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
				DataSourceSelectArguments args2 = new DataSourceSelectArguments();
				DataView view2 = (DataView)sqldsNivel002.Select(args2);
				DataTable dt002 = view2.ToTable();
				if (dt002.Rows.Count > 0) i_count++;
				foreach (DataRow row in dt002.Rows)
					if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
				rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
				rptNivel002.DataBind();
			}
			//si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
			if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
		}
		protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
		{
			foreach (DataRow row in dt.Rows)
			{
				if (row["menhab"].Equals("N")) //Deshabilitado
					row["menhab"] = "disabled";
				else
					row["menhab"] = "c-sidebar-nav-dropdown-toggle";
			}
			return dt;
		}
		protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
		{
			foreach (DataRow row in dt.Rows)
			{
				if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
					Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

				if (row["menhab"].Equals("N")) //Deshabilitado
					row["menhab"] = "c-sidebar-nav-link disabled";
				else
					row["menhab"] = "c-sidebar-nav-link";

				if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Facturas
				{
					if (row["banins"].Equals("N")) //Nuevo
					{
						btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
						btnNuevo.Disabled = true;
					}
					if (row["banbus"].Equals("N")) //Abrir
					{
						btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
						btnAbrir.Disabled = true;
					}
					if (row["bangra"].Equals("N")) //Guardar
					{
						btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
						btnGuardar.Disabled = true;
					}
					if (row["bancan"].Equals("N")) //Cancelar
					{
						btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
						btnCancelar.Disabled = true;

					}
					if (row["baneli"].Equals("N")) //Eliminiar
					{
						btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
						btnEliminar.Disabled = true;
						Session["baneli"] = "N";
					}
				}
			}
			return dt;
		}

		protected void btnNuevo_Click(object sender, EventArgs e)
		{
			Response.Redirect("w_vc_FacturaAdministrar.aspx");
		}
		protected void btnAbrir_Click(object sender, EventArgs e)
		{
			Response.Redirect("w_vc_Factura.aspx");
		}		

		protected void btnVolver_Click(object sender, EventArgs e)
		{
			Response.Redirect("w_Login.aspx");
		}

		protected DataTable f_llenarCamposVistaPrev()
		{
			//PARA IMPRIMIR SIN FECHA Y NOM DE PAG EN HEADER REVISAR: https://stackoverflow.com/questions/8228088/remove-header-and-footer-from-window-print
			DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
			DataView view = (DataView)sqldsFacturaRenglones.Select(args); //para pasar del SqlDataSource1 a una DataTable
			DataTable dt = view.ToTable(); //en esta dataTable esta el pedido seleccionado... (con los datos de este se llenara el encabezadoegresos)

			lblNomemp.Text = s_nomemp = dt.Rows[0].Field<string>("nomemp");
			lblRuc.Text = s_ruc = dt.Rows[0].Field<string>("ruc");
			lblDir01.Text = s_dir01 = dt.Rows[0].Field<string>("dir01");
			lblEmail.Text = s_dir02 = dt.Rows[0].Field<string>("dir02");
			lblContab.Text = s_oblsri = dt.Rows[0].Field<string>("oblsri");
			lblNumfac.Text = s_numfac = dt.Rows[0].Field<string>("numfac");
			lblFecfac.Text = s_fecfac = dt.Rows[0].Field<DateTime>("fecfac").ToString("dd/MM/yyyy");
			lblTel01.Text = s_tel01 = dt.Rows[0].Field<string>("tel01");
			lblRazon.Text = s_nomcli = dt.Rows[0].Field<string>("nomcli");
			lblDircli.Text = s_dircli = dt.Rows[0].Field<string>("dircli");
			lblEmail2.Text = s_email = dt.Rows[0].Field<string>("email");
			lblRucced.Text = s_rucced = dt.Rows[0].Field<string>("rucced");
			lblCiucli.Text = s_ciucli = dt.Rows[0].Field<string>("ciucli");
			lblTelcli.Text = s_telcli = dt.Rows[0].Field<string>("telcli");
			lblNumaut.Text = s_telcli = dt.Rows[0].Field<string>("numaut");
			lblFecaut.Text = s_telcli = dt.Rows[0].Field<string>("fecaut");

			s_totnet = dt.Rows[0].Field<decimal>("totnet").ToString();
			lblTotnet.Text = s_totnet;
			s_totbas = dt.Rows[0].Field<decimal>("totbas").ToString();
			lblTotbas.Text = s_totbas;
			double r_subtot0 = double.Parse(s_totnet) - double.Parse(s_totbas);
			lblSubtotal0.Text = s_subtot0 = r_subtot0.ToString("N2");
			lblTotdes.Text = s_totdes = dt.Rows[0].Field<decimal>("totdes").ToString();
			lblTotiva.Text = s_totiva = dt.Rows[0].Field<decimal>("totiva").ToString();
			lblTotfac.Text = s_totfac = dt.Rows[0].Field<decimal>("totfac").ToString();

			//PARA LLENAR FORMA DE PAGO
			grvFormaPagoFactura.DataSource = (DataTable)Session["gdt_FormaPagoImprimir"];
			grvFormaPagoFactura.DataBind();
			return dt;
		}

		protected void btnPDF_Click(object sender, EventArgs e)
		{
			DataTable dt = f_llenarCamposVistaPrev();
			string s_renfac = "";
			foreach (DataRow row in dt.Rows)
			{
				s_renfac = s_renfac + "<tr>";
				s_renfac = s_renfac + "<td>" + row.Field<string>("codart").ToString().Trim() + "</td>";
				s_renfac = s_renfac + "<td>" + row.Field<string>("nomart").ToString().Trim() + "</td>";
				s_renfac = s_renfac + "<td>" + row.Field<decimal>("cantid").ToString("N0").Trim() + "</td>";
				s_renfac = s_renfac + "<td>" + row.Field<decimal>("preuni").ToString("N2").Trim() + "</td>";
				s_renfac = s_renfac + "<td>" + row.Field<decimal>("totdes").ToString("N2").Trim() + "</td>";
				s_renfac = s_renfac + "<td>" + row.Field<decimal>("totren").ToString("N2").Trim() + "</td>";
				s_renfac = s_renfac + "</tr>";
			}

			DataTable dtFormaPago = (DataTable)Session["gdt_FormaPagoImprimir"];
			string s_fpa = "<tr><td>01.</td><td>EFECTIVO</td><td>147.32</td></tr><tr><td>01.</td><td>EFECTIVO</td><td>0.00</td></tr>";
			if (dtFormaPago != null)
			{
				s_fpa = "";
				if (dtFormaPago.Rows.Count > 0)
				{
					foreach (DataRow row in dtFormaPago.Rows)
					{
						s_fpa = s_fpa + "<tr>";
						s_fpa = s_fpa + "<td>" + row.Field<string>("Código").ToString().Trim() + "</td>";
						s_fpa = s_fpa + "<td>" + "0" + "</td>";
						s_fpa = s_fpa + "<td>" + row.Field<string>("Forma de Pago").ToString().Trim() + "</td>";
						s_fpa = s_fpa + "<td>" + row.Field<string>("Valor").ToString().Trim() + "</td>";
						s_fpa = s_fpa + "</tr>";
					}
				}
			}

			string html = @"<!DOCTYPE html>
			<html xmlns=""http://www.w3.org/1999/xhtml"">
			<head runat=""server"">
				<title>Kohinor Web - Factura Imprimir</title>
				<meta charset=""utf-8"" />
				<link href=""CoreUI/css/style.css"" rel=""stylesheet"" />
				<link href=""CoreUI/css/style2.css"" rel=""stylesheet"" />
				<link href=""CoreUI/vendors/@coreui/icons/css/brand.min.css"" rel=""stylesheet"" />
				<link href=""CoreUI/vendors/@coreui/icons/css/flag.min.css"" rel=""stylesheet"" />
				<link href=""CoreUI/vendors/@coreui/icons/css/free.min.css"" rel=""stylesheet"" />
			</head>
			<body style=""background-color: #ffffff;"">
				 <table class=""table table-sm mx-0 my-0"" style=""width: 100%; text-align: center; font-family: helvetica"">
				  <thead>
					<tr>
					  <th style=""text-align: left;"">
						<img id=""logo"" src=""Icon/Menu125/powersoft.png"" width=""120"" height=""48"" />
					  </th>
					  <th style=""text-align: right;"">
							<h4>Factura</h4>				
					  </th>
					</tr>
				   </thead>
					<tbody>
						<tr>
						  <td style=""text-align: left;""><label style =""font-weight: bold;"">Fecha  Emisión: </label>" + DateTime.Now.ToString("dd/MM/yyyy") + @"</td>
						  <td style=""text-align: right;""><div style =""font-weight: bold;"">N° - " + s_numfac + @"</div></td>
						</tr>
					  </tbody>
				  </table>
				  <table class=""table"" style=""width: 100%; text-align: center; table-layout: fixed; font-family: helvetica"">
					<tbody>
						<tr>
						  <td style=""text-align: left;"">
							<div style =""font-weight: bold;"">" + s_nomemp + @"</div>
							<div>RUC: " + s_ruc + @"</div>
							<div>Matriz: " + s_dir01 + @"</div>
							<div>Sucursal: " + "???" + @"</div>
							<div>Email: " + s_dir02 + @"</div>
							<div>Teléfono: " + s_tel01 + @"</div>
							<div>OBLIGADO A LLEVAR CONTABILIDAD: " + s_oblsri + @"</div>
						  </td>
						  <td style=""text-align: right;"">
							<div>Autorización SRI N°</div>
							<div>Clave Acceso</div>
							<img id=""logo"" src=""Icon/Menu125/claveacceso.png"" width=""250"" height=""30"" />
							<div><small>1103202101179136284500120070330000623490006234917</small></div>
						  </td>
						</tr>
					  </tbody>
				  </table>
				  <table class=""table table-secondary"" style=""width: 100%; text-align: center; font-family: helvetica"">
					<tbody>
						<tr>
						  <td style=""text-align: left;"">
							<div>Razón Social: " + s_nomcli + @"</div>
							<div>Dirección: " + s_dircli + @"</div>
							<div>Email: " + s_email + @"</div>
						  </td>
						  <td style=""text-align: left;"">
							<div>RUC/C.I.: " + s_rucced + @"</div>
							<div>Ciudad: " + s_ciucli + @"</div>
							<div>Telf.: " + s_telcli + @"</div>
						  </td>
						</tr>
					  </tbody>
				  </table>
				  <table class=""table table-sm table-bordered table-striped"" style=""width: 100%; font-size: smaller; font-family: helvetica"">
				  <thead>
					<tr>
					  <th>Código</th>
					  <th>Descripción</th>
					  <th>Cantidad</th>
					  <th>Precio Unitario</th>
					  <th>Descuento</th>
					  <th>Precio Total</th>
					</tr>
				  </thead>
				  <tbody>
					" + s_renfac + @"
				  </tbody>
				  </table>
				  <table class=""table"" style=""width: 100%; padding: 0; margin: 0; font-family: helvetica"">
					  <tbody>
						<tr>
						  <td>
							<table class=""table table-sm"" style=""width: 100%; padding: 0; margin: 0;"">
							  <thead class=""table-secondary"">
								<tr>
								  <th>Código</th>
								  <th>Plazo</th>
								  <th>Forma de Pago</th>
								  <th>Valor</th>
								</tr>
							  </thead>
							  <tbody>
								" + s_fpa + @"
							  </tbody>
							</table>
					  </td>
					  <td>
						<table class=""table table-sm table-bordered table-striped"" style=""width: 100%; text-align: left; padding: 0; margin: 0;"">
						<tbody>
							<tr>
							  <td colspan=""5""><label style =""font-weight: bold;"">SUBTOTAL</label></td>
							  <td>$" + s_totnet + @"</td>
							</tr>
							<tr>
							  <td colspan=""5""><label style =""font-weight: bold;"">SUBTOTAL 12%</label></td>
							  <td>$" + s_totbas + @"</td>
							</tr>
							<tr>
							  <td colspan=""5""><label style =""font-weight: bold;"">SUBTOTAL 0%</label></td>
							  <td>$" + s_subtot0 + @"</td>
							</tr>
							<tr>
							  <td colspan=""5""><label style =""font-weight: bold;"">DESCUENTO</label></td>
							  <td>$" + s_totdes + @"</td>
							</tr>
							<tr>
							  <td colspan=""5""><label style =""font-weight: bold;"">IVA 12%</label></td>
							  <td>$" + s_totiva + @"</td>
							</tr>
							<tr>
							  <td colspan=""5""><label style =""font-weight: bold;"">VALOR TOTAL</label></td>
							  <td>$" + s_totfac + @"</td>
							</tr>
						</tbody>
						</table>
						  </td>
					</tr>
				  </tbody>
				  </table>
				</div>	
			</body>
			</html>";

			//// Must have write permissions to the path folder
			//PdfWriter writer = new PdfWriter(Server.MapPath("~\\Comprobantes\\demo.pdf"));
			//PdfDocument pdf = new PdfDocument(writer);
			//Document document = new Document(pdf);
			////Document document = new Document(pdf, PageSize.A4, false);
			//Paragraph header = new Paragraph("PRUEBA")
			//   .SetTextAlignment(TextAlignment.CENTER)
			//   .SetFontSize(20);
			//document.Add(header);
			//document.Close();
			String s_FileName = Convert.ToString("PVE_" + s_numfac.Substring(0, 6) + '0' + s_numfac.Substring(7, 8));
			String TARGET = Server.MapPath("~\\Comprobantes\\");
			String DEST = String.Format("{0}" + s_FileName + ".pdf", TARGET);
			ConverterProperties properties = new ConverterProperties();
			properties.SetBaseUri(Server.MapPath("~\\")); //directorio raiz donde se guardan los archivos q debe leer el html

			HtmlConverter.ConvertToPdf(html, new FileStream(DEST, FileMode.Create), properties);
		}

		protected void btnEmail_Click(object sender, EventArgs e)
		{
			SendEmail();
		}

		private void SendEmail()
		{
			//Fetching Settings from WEB.CONFIG file.  
			string emailSender = ConfigurationManager.AppSettings["emailsender"].ToString();
			string emailSenderPassword = ConfigurationManager.AppSettings["password"].ToString();
			string emailSenderHost = ConfigurationManager.AppSettings["smtpserver"].ToString();
			int emailSenderPort = Convert.ToInt16(ConfigurationManager.AppSettings["portnumber"]);
			Boolean emailIsSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSSL"]);
			string html = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
<html xmlns=""http://www.w3.org/1999/xhtml"" lang=""en-GB"">
<head>
  <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
  <title>Demystifying Email Design</title>
  <meta name=""viewport"" content=""width=device-width, initial-scale=1.0""/>

  <style type=""text/css"">
	a[x-apple-data-detectors] {color: inherit !important;}
  </style>

</head>
<body style=""margin: 0; padding: 0;"">
  <table role=""presentation"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
	<tr>
	  <td style=""padding: 20px 0 30px 0;"">

<table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"" style=""border-collapse: collapse; border: 1px solid #cccccc;"">
  <tr>
	<td align=""center"" bgcolor=""#70bbd9"" style=""padding: 40px 0 30px 0;"">
	  <img src=""https://assets.codepen.io/210284/h1_1.gif"" alt=""Creating Email Magic."" width=""300"" height=""230"" style=""display: block;"" />
	</td>
  </tr>
  <tr>
	<td bgcolor=""#ffffff"" style=""padding: 40px 30px 40px 30px;"">
	  <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;"">
		<tr>
		  <td style=""color: #153643; font-family: Arial, sans-serif;"">
			<h1 style=""font-size: 24px; margin: 0;"">Lorem ipsum dolor sit amet!</h1>
		  </td>
		</tr>
		<tr>
		  <td style=""color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;"">
			<p style=""margin: 0;"">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.</p>
		  </td>
		</tr>
		<tr>
		  <td>
			<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;"">
			  <tr>
				<td width=""260"" valign=""top"">
				  <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;"">
					<tr>
					  <td>
						<img src=""https://assets.codepen.io/210284/left_1.gif"" alt="""" width=""100%"" height=""140"" style=""display: block;"" />
					  </td>
					</tr>
					<tr>
					  <td style=""color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 25px 0 0 0;"">
						<p style=""margin: 0;"">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.</p>
					  </td>
					</tr>
				  </table>
				</td>
				<td style=""font-size: 0; line-height: 0;"" width=""20"">&nbsp;</td>
				<td width=""260"" valign=""top"">
				  <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;"">
					<tr>
					  <td>
						<img src=""https://assets.codepen.io/210284/right_1.gif"" alt="""" width=""100%"" height=""140"" style=""display: block;"" />
					  </td>
					</tr>
					<tr>
					  <td style=""color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 25px 0 0 0;"">
						<p style=""margin: 0;"">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.</p>
					  </td>
					</tr>
				  </table>
				</td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
  <tr>
	<td bgcolor=""#ee4c50"" style=""padding: 30px 30px;"">
		<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;"">
		<tr>
		  <td style=""color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;"">
			<p style=""margin: 0;"">&reg; Someone, somewhere 2025<br/>
		   <a href=""#"" style=""color: #ffffff;"">Unsubscribe</a> to this newsletter instantly</p>
		  </td>
		  <td align=""right"">
			<table border=""0"" cellpadding=""0"" cellspacing=""0"" style=""border-collapse: collapse;"">
			  <tr>
				<td>
				  <a href=""http://www.twitter.com/"">
					<img src=""https://assets.codepen.io/210284/tw.gif"" alt=""Twitter."" width=""38"" height=""38"" style=""display: block;"" border=""0"" />
				  </a>
				</td>
				<td style=""font-size: 0; line-height: 0;"" width=""20"">&nbsp;</td>
				<td>
				  <a href=""http://www.twitter.com/"">
					<img src=""https://assets.codepen.io/210284/fb.gif"" alt=""Facebook."" width=""38"" height=""38"" style=""display: block;"" border=""0"" />
				  </a>
				</td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
</table>

	  </td>
	</tr>
  </table>
</body>
</html>";

			try
			{
				MailMessage objMailMsg = new MailMessage();
				objMailMsg.From = new MailAddress(emailSender, "KohinorWeb");
				objMailMsg.To.Add("davidabad081998@gmail.com");
				objMailMsg.Subject = "Factura Electrónica";
				objMailMsg.Body = html;
				objMailMsg.IsBodyHtml = true;
				Attachment attPDF = new Attachment(Server.MapPath("~/Comprobantes/AutorizadosPVE_003002000000016.pdf"));
				objMailMsg.Attachments.Add(attPDF);
				Attachment attXML = new Attachment(Server.MapPath("~/Comprobantes/AutorizadosPVE_003002000000016.xml"));
				objMailMsg.Attachments.Add(attXML);
				objMailMsg.Priority = MailPriority.Normal;

				SmtpClient sc = new SmtpClient(emailSenderHost, emailSenderPort); //Try port 587 instead of 465. Port 465 is technically deprecated.
				sc.Credentials = new NetworkCredential(emailSender, emailSenderPassword);
				sc.EnableSsl = emailIsSSL;
				sc.Send(objMailMsg);
				lblExito.Text = "✔️ Email enviado. " + DateTime.Now;
			}
			catch (Exception ex)
			{
				clsError objError = new clsError();
				objError = objError.f_ErrorControlado(ex);
				f_ErrorNuevo(objError);
				ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
			}
		}

		protected void btnXML_Click(object sender, EventArgs e)
		{
			clsError objError = new clsError();
			DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
			DataView view = (DataView)sqldsEncPuntosventaFull.Select(args);            //para pasar del SqlDataSource1 a una DataTable
			DataTable dt_EncPV = view.ToTable(); //en esta dataTable esta el pedido seleccionado... (con los datos de este se llenara el encabezadoegresos)
			cls_vc_XML objXML = new cls_vc_XML(Session["gs_SerSec"].ToString(), Session["gs_CodEmp"].ToString(), Session["gs_CodAlm"].ToString(), dt_EncPV, (DataTable)Session["gdt_FacturaRen"], (DataTable)Session["gdt_FormaPago"]);
			objError = objXML.f_ProcesoXML_SRI();
			if (String.IsNullOrEmpty(objError.Mensaje))
			{
				lblExito.Text = "✔️ Archivo XML Creado. " + DateTime.Now;
			}
			else
			{
				f_ErrorNuevo(objError);
				ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
			}
		}

		protected void Button1_Click(object sender, EventArgs e)
		{			
			ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalXML();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
		}		
	}
}