﻿<%@ Page Title="Pedido Administrar" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_vc_PedidoAdministrar.aspx.cs" Inherits="prj_KohinorWeb.w_vc_PedidoAdministrar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtClicxc.ClientID%>").blur(function () {
                buscarClicxc();
            });
        });

        //Tab en txtClicxc
        function buscarClicxc() {
            var text1 = $("#<%=txtClicxc.ClientID%>").val();
            PageMethods.f_buscarCli(text1, buscarClicxc_Success, buscarClicxc_Fail);
        }
        function buscarClicxc_Success(data) {
            if (data.Clicxc == "noEncontro") {
                alert("No existe el cliente ingresado.");
            }
            else {
                $("#<%=txtClicxc.ClientID%>").val(data.Clicxc);
                $("#<%=txtNomcxc.ClientID%>").val(data.Nomcxc);
                $("#<%=ddlLispre.ClientID%>").val(data.Lispre);
                $("#<%=txtObserv.ClientID%>").val(data.Observ);
            }
        }
        function buscarClicxc_Fail() {
            alert("ERROR : Método buscarClicxc_Success(data) fallo.");
        }

    </script>
</asp:Content>
<asp:Content ID="ContenSidebar" ContentPlaceHolderID="cphSidebar" runat="server">
    <%-- Modulos Kohinor Web --%>
    <li class="c-sidebar-nav-title">Menu</li>
    <asp:Repeater ID="rptNivel001" runat="server">
        <ItemTemplate>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link <%# Eval("menhab") %>" href="#"><i class='<%# Eval("img001") %>'></i><%# Eval("nom001") %></a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <asp:Repeater ID="rptNivel002" runat="server">
                        <ItemTemplate>
                            <li class="c-sidebar-nav-item">
                                <asp:LinkButton ID="lkbtnNiv002" runat="server" class='<%# Eval("menhab") %>' PostBackUrl='<%# Eval("ref002") %>'><span class="c-sidebar-nav-icon"></span><%# Eval("nom002") %></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
    <div class="c-subheader justify-content-between px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">
                <asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>
            <li class="breadcrumb-item"><a href="w_vc_Pedido.aspx">Pedido</a></li>
            <li class="breadcrumb-item active">Administrar Pedido
            </li>
            <!-- Breadcrumb Menu-->
        </ol>
        <div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <a class="c-subheader-nav-link">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary" type="button" id="">
                            <i class="cil-filter"></i>
                        </button>
                    </div>

                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" data-toggle="tooltip" data-placement="bottom" title="Buscar por N° o Nombre"></asp:TextBox>
                </div>
                <asp:LinkButton ID="lkbtnSearch" runat="server" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
            </a>

            <%--<a class="c-subheader-nav-link">
                <i class="c-icon cil-speech"></i>
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-graph"></i>&nbsp;
                            Escritorio
            </a>
            <a class="c-subheader-nav-link">
                <i class="c-icon cil-settings"></i>
            </a>--%>
        </div>
    </div>
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item active">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click"><i class="cil-file"></i>Nuevo</button>
                    <button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" onserverclick="btnAbrir_Click"><i class="cil-folder-open"></i>Abrir</button>
                    <button type="button" class="btn btn-outline-dark" id="btnGuardar" runat="server" onserverclick="btnGuardarPedido_Click"><i class="cil-save"></i>Guardar</button>
                    <button type="button" class="btn btn-outline-dark" id="btnCancelar" runat="server" onserverclick="btnCancelar_Click"><i class="cil-grid-slash"></i>Cancelar</button>
                    <button type="button" class="btn btn-outline-danger" id="btnEliminar" runat="server" onserverclick="lkbtnDelete_Click"><i class="cil-trash"></i>Eliminar</button>
                </div>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-dark" id="lkbtnPrincipio" runat="server" onserverclick="lkbtnPrincipio_Click"><i class="cil-media-step-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnAtras" runat="server" onserverclick="lkbtnAtras_Click"><i class="cil-media-skip-backward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnSiguiente" runat="server" onserverclick="lkbtnSiguiente_Click"><i class="cil-media-skip-forward"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="lkbtnFinal" runat="server" onserverclick="lkbtnFinal_Click"><i class="cil-media-step-forward"></i></button>
                </div>

                <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
            </li>
        </ol>

    </div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
                <main class="c-main">
                    <div class="container-fluid">
                        <div class="fade-in">
                            <div id="div_Pedido" runat="server" class="card bg-gradient-secondary">
                                <div class="card-header">
                                    <i class="c-icon cil-justify-center"></i><b>Pedido</b>
                                </div>

                                <div class="card-body">
                                    <div class="form-row">
                                        <div class="form-group col-md-9">
                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>Ruc/C.I.</strong></span>
                                                        </div>
                                                        <input type="text" id="txtClicxc" class="form-control" runat="server" />
                                                        <div class="input-group-append">
                                                            <asp:LinkButton ID="lkbtnBuscarCliente" data-toggle="modal" data-target="#ClienteModal" runat="server" class="btn btn-primary">
                                                            <i class="cil-search"></i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-9">
                                                    <asp:TextBox ID="txtNomcxc" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>Observ.</strong></span>
                                                        </div>
                                                        <asp:TextBox ID="txtObserv" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>Lista</strong></span>
                                                        </div>
                                                        <asp:DropDownList ID="ddlLispre" runat="server" class="form-control" Enabled="False">
                                                            <asp:ListItem Value="1">1</asp:ListItem>
                                                            <asp:ListItem Value="2">2</asp:ListItem>
                                                            <asp:ListItem Value="3">3</asp:ListItem>
                                                            <asp:ListItem Value="4">4</asp:ListItem>
                                                            <asp:ListItem Value="5">5</asp:ListItem>
                                                            <asp:ListItem Value="6">6</asp:ListItem>
                                                            <asp:ListItem Value="7">7</asp:ListItem>
                                                            <asp:ListItem Value="8">8</asp:ListItem>
                                                            <asp:ListItem Value="9">9</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>Vendedor</strong></span>
                                                        </div>
                                                        <asp:DropDownList ID="ddlCodven" runat="server" class="form-control" DataSourceID="sqldsVendedor" DataTextField="nomven" DataValueField="codven"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>Entrega</strong></span>
                                                        </div>
                                                        <asp:TextBox ID="txtFecent" runat="server" class="form-control" TextMode="Date"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>Valido</strong></span>
                                                        </div>
                                                        <asp:TextBox ID="txtValido" runat="server" class="form-control" TextMode="Date"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>Orden Compra</strong></span>
                                                        </div>
                                                        <asp:TextBox ID="txtNumpro" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>F. Pago</strong></span>
                                                        </div>
                                                        <asp:TextBox ID="txtForpag" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                     <div class="form-group col-md-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>Ciudad</strong></span>
                                                        </div>
                                                        <asp:TextBox ID="txtCiucli" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                            <div class="form-row">                                                
                                                <div class="table table-responsive scroll1 text-black-50" style="overflow-x: auto; height: 70%">
                                                    <div class="thead-dark">
                                                        <asp:UpdatePanel ID="updPedidoRen" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="grvPedidoRen" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" class="table table-bordered table-dark table-hover table-striped">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <%# Container.DataItemIndex + 1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Código" ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updPedidoRenPuntitos" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <div class="input-group mb-1">
                                                                                            <asp:TextBox ID="txtgrvCodart" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                                            <div class="input-group-append">
                                                                                                <asp:Button ID="btnPuntitosRen" runat="server" Text="..." class="btn btn-primary btn-sm" OnClick="btnPuntitosRen_Click" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="btnPuntitosRen" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Descripción" ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtgrvNomart" runat="server" htmlencode="false" class="form-control form-control-sm"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Alm." ItemStyle-Width="3%">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddlgrvCodalm" runat="server" class="form-control form-control-sm" DataSourceID="sqldsCodalm" DataTextField="codalm" DataValueField="codalm"></asp:DropDownList>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Uni." ItemStyle-Width="2%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtgrvCoduni" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updPedidoRenCantid" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtgrvCantid" runat="server" class="form-control form-control-sm" AutoPostBack="True" TextMode="Number" OnTextChanged="txtgrvCantid_TextChanged"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="txtgrvCantid" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="PVP" ItemStyle-Width="3%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updPedidoRenPreuni" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtgrvPreuni" class="form-control form-control-sm" runat="server" DataFormatString="{0:n}" AutoPostBack="True" TextMode="Number" OnTextChanged="txtgrvPreuni_TextChanged"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="txtgrvPreuni" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="IVA" ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtgrvPoriva" runat="server" class="form-control form-control-sm" DataFormatString="{0:n}" ReadOnly="True"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Desc." ItemStyle-Width="1%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updPedidoRenDesren" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtgrvDesren" class="form-control form-control-sm" runat="server" DataFormatString="{0:n}" AutoPostBack="True" TextMode="Number" OnTextChanged="txtgrvDesren_TextChanged"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="txtgrvDesren" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Total" ItemStyle-Width="2%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtgrvTotren" class="form-control form-control-sm" runat="server" ReadOnly="True"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-Width="1%" ItemStyle-HorizontalAlign="center">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="updPedidoRenBorrar" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:ImageButton ID="lkgrvBorrar" runat="server" OnClick="lkgrvBorrar_Click" ImageUrl="~/Icon/Menu125/delete-icon.png" Width="20px"/>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lkgrvBorrar" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <asp:Button ID="btnAgregar" runat="server" class="btn btn-dark" Text="➕ Agregar" OnClick="btnAgregar_Click" />
                                                    <asp:Button ID="btnEliminarTodo" runat="server" class="btn btn-dark" OnClick="btnEliminarTodo_Click" Text="Eliminar Todo" />

                                                </div>
                                                <div class="form-group col-md-4">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <div class="card text-white bg-gradient-primary">                                                
                                                    <asp:UpdatePanel ID="updEncabezado" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="card-header text-center">
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <h4>
                                                                            <label><strong>N° </strong></label>
                                                                        </h4>
                                                                        <h4><asp:Label ID="txtNumtra" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label></h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="form-row">
                                                                    <div class="form-group col-md-3">
                                                                        <asp:Label ID="Label1" runat="server" Text="Label">Emisión</asp:Label>
                                                                    </div>
                                                                    <div class="form-group col-md-9">
                                                                        <asp:TextBox ID="txtFectra" runat="server" class="form-control form-control-sm" TextMode="Date"></asp:TextBox>
                                                                    </div>
                                                                </div>

                                                                <div class="form-row">
                                                                    <div class="form-group col-md-6">
                                                                        <div class="input-group input-group-sm">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><strong>Estado</strong></span>
                                                                            </div>
                                                                            <asp:DropDownList ID="ddlEstado" class="form-control" runat="server">
                                                                                <asp:ListItem Value="P">OK</asp:ListItem>
                                                                                <asp:ListItem Value="A">ANULADO</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <div class="input-group input-group-sm">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><strong>Almacén</strong></span>
                                                                            </div>
                                                                            <asp:TextBox ID="txtCodalm" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>                                                
                                            </div>


                                            <div class="card text-white bg-primary text-center">
                                                <div class="card-body">
                                                    <asp:UpdatePanel ID="updTotales" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <asp:Label ID="Label13" runat="server" Text="Label">Total Neto</asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <asp:Label ID="txtTotnet" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <asp:Label ID="Label14" runat="server" Text="Label">Base</asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <asp:Label ID="txtTotbas" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="form-row">
                                                                 <div class="form-group col-md-6">
                                                                     <div class="input-group input-group-sm">
                                                                         <div class="input-group-prepend">
                                                                            <span class="input-group-text"><strong>Descto</strong></span>
                                                                        </div>
                                                                         <asp:TextBox ID="txtPordes" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                          <div class="input-group-append">
                                                                            <button class="btn btn-light btn-sm" style="cursor: default;" disabled><strong>%</strong></button>
                                                                          </div>
                                                                     </div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <asp:Label ID="txtTotdes" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
                                                                </div>                                                                
                                                            </div>                                                          

                                                            <div class="form-row">     
                                                                <div class="form-group col-md-6">
                                                                    <div class="input-group input-group-sm">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text"><strong>IVA.</strong></span>
                                                                        </div>
                                                                        <asp:TextBox ID="txtPoriva" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                        <div class="input-group-append">
                                                                            <button class="btn btn-light btn-sm" style="cursor: default;" disabled><strong>%</strong></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <asp:Label ID="txtTotiva" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <h3>
                                                                        <label><strong>TOTAL</strong></label></h3>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <h3>
                                                                        <asp:Label ID="txtTotfac" runat="server" Text="" ForeColor="White" Font-Bold="true"></asp:Label></h3>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                    <!-- End of Main Content -->
                </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsPedidoExiste" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [numfac] FROM [vc_factura] WHERE (([codemp] = @codemp) AND ([numfac] = @numfac))">
        <SelectParameters>
            <asp:SessionParameter Name="codemp" SessionField="gs_Codemp" Type="String" />
            <asp:SessionParameter Name="numfac" SessionField="gs_numfacSelected" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsNivel001" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV001" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter Name="CODMOD" SessionField="gs_Codmod" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsNivel002" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SEG_S_MENU_NIV002" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="Action" SessionField="gs_SuperAdmin" Type="String" />
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODUS1" SessionField="gs_CodUs1" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="CODMOD" SessionField="gs_Codmod" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="NIV001" SessionField="gs_Niv001" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsVendedor" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codven])) AS codven, [nomven] FROM [vc_vendedor]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCodalm" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codalm], [nomalm] FROM [vc_almacen]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsTtardes" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT * FROM [vc_tarjeta]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCliente" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [codcli], [codcla], [nomcli], [rucced], [telcli], [email], [dircli] FROM [vc_cliente]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsArticulo" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA  %>" SelectCommand="SELECT TOP 50 [codart], [nomart], [prec01], [exiact], [coduni], ([prec01]+[prec01]*[poriva]/100) as pvpiva FROM [vw_inv_articulo_vc] WHERE ([nomart] LIKE '%' + @nomart + '%')">
        <SelectParameters>
            <asp:SessionParameter Name="nomart" SessionField="gs_BuscarNomart" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsArticuloDetalle" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="VC_S_EXISTENCIA_ALMACEN" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="codemp" SessionField="gs_CodEmp" />
            <asp:SessionParameter Name="codart" SessionField="gs_DetalleArt" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsClaseCliente" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT LTRIM(RTRIM([codcla])) AS codcla, [nomcla] FROM [vc_cliente_clase] "></asp:SqlDataSource>
    
    <!--f SQL DATASOURCES-->


    <!-- Modal grvClientes -->
    <button id="btnShowClienteModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#ClienteModal"></button>
    <div class="modal fade" id="ClienteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccionar Cliente</h4>
                    <button class="close" id="btnCloseClienteModal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="lkbtnSearchCliente" runat="server" OnClick="f_GridBuscar" class="btn btn-primary"> <span class="cil-search"></span></asp:LinkButton>
                                </div>
                                <asp:TextBox ID="txtSearchCliente" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" Width="50%"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="table table-responsive" style="overflow-x: auto;">
                                <div class="thead-dark">
                                    <asp:UpdatePanel ID="updCliente" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="grvCliente" class="table table-bordered table-active table-active table-hover table-striped"
                                                runat="server" AutoGenerateColumns="False" OnRowDataBound="grvCliente_RowDataBound" OnRowCreated="grvCliente_RowCreated" OnSelectedIndexChanged="grvCliente_SelectedIndexChanged"
                                                AllowSorting="true" OnSorting="OnSorting" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging" PageSize="10">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updClienteSelect" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnSelectCliente" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/Select-Hand.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnSelectCliente" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="codcli" HeaderText="Código" SortExpression="codcli" />
                                                    <asp:BoundField DataField="codcla" HeaderText="Clase" SortExpression="codcla" />
                                                    <asp:BoundField DataField="nomcli" HeaderText="Nombres" SortExpression="nomcli" />
                                                    <asp:BoundField DataField="rucced" HeaderText="RUC/Cédula" SortExpression="rucced" />
                                                    <asp:BoundField DataField="telcli" HeaderText="Teléfono" SortExpression="telcli" />
                                                    <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
                                                </Columns>
                                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Articulos Popup -->
    <button id="btnShowArticulosModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#modalPuntitosArt"></button>
    <div id="modalPuntitosArt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccione Artículos</h4>
                    <button class="close" id="btnCloseArticulosModal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <!-- CONTENIDO-->
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="lkbtnBuscarNomart" runat="server" OnClick="f_GridBuscarArt" class="btn btn-primary"> <span class="cil-search"></span></asp:LinkButton>
                                </div>
                                <asp:TextBox ID="txtSearchArt" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" Width="50%"></asp:TextBox>
                            </div>                            
                        </div>
                        <br />
                        <div class="form-group col-md-12">
                            <div class="table table-responsive" style="overflow-x: auto;">
                                <div class="thead-dark">
                                    <asp:UpdatePanel ID="updArticulo" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="grvArticulo" runat="server" class="table table-bordered table-active table-active table-hover table-striped"
                                                AutoGenerateColumns="False" OnSelectedIndexChanged="grvArticulo_SelectedIndexChanged" OnRowCreated="grvArticulo_RowCreated"
                                                AllowSorting="true" OnSorting="OnSortingArt" AllowPaging="true" OnPageIndexChanging="OnPageIndexChangingArt" PageSize="5">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updArticuloSelect" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnSelectArticulo" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/Select-Hand.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnSelectArticulo" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%"></ItemStyle>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Img">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updArticuloImg" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnImgArticulo" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/catUser.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnImgArticulo" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="codart" HeaderText="Código" SortExpression="codart" />
                                                    <asp:BoundField DataField="nomart" HeaderText="Nombre" SortExpression="nomart" />
                                                    <asp:BoundField DataField="exiact" HeaderText="Existencia" SortExpression="exiact" DataFormatString="{0:n}" />
                                                    <asp:BoundField DataField="coduni" HeaderText="Uni." SortExpression="coduni"/>
                                                    <asp:BoundField DataField="prec01" HeaderText="PVP" SortExpression="prec01" DataFormatString="{0:n}" />
                                                    <asp:BoundField DataField="pvpiva" HeaderText="PVP+IVA" SortExpression="pvpiva" DataFormatString="{0:n}" />
                                                    
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updArticuloDetalle" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton ID="lkbtnDetArticulo" runat="server" class="btn btn-pill btn-sm btn-warning" OnClick="lkbtnDetArticulo_Click"><i class="cil-pencil"></i>
                                                                        &nbsp;Detalle</asp:LinkButton>
                                                                    <%--<asp:LinkButton ID="lkbtnAlmArticulo" runat="server" class="btn btn-pill btn-sm btn-warning" OnClick="lkbtnDetArticulo_Click"><i class="cil-clipboard"></i>
                                                                        &nbsp;Almacén</asp:LinkButton>--%>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="lkbtnDetArticulo" />
                                                                    <%--<asp:PostBackTrigger ControlID="lkbtnAlmArticulo" />--%>
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>

                                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                            </asp:GridView>

                                            <%--<asp:DataList ID="DataList1" runat="server" DataSourceID="sqldsArticuloDetalle" Width="100%">
                                                <ItemTemplate>
                                                    <div class="row">
                                                        <div class="col">
                                                            <label>Almacén: '<%# Eval("codalm") %>' - '<%# Eval("nomalm") %>'</label>
                                                            <label>Existencia: '<%# Eval("exiact") %>'</label>
                                                            <label>Ult. Compra: '<%# Eval("ultcom") %>'</label>
                                                            <label>Ult. Venta: '<%# Eval("ultven") %>'</label>
                                                            <label>% Desc.: '<%# Eval("coddes") %>'</label>
                                                            <label>Dias: '<%# Eval("diaper") %>'</label>
                                                        </div>
                                                        <div class="col">
                                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Icon/Menu125/catUser.png" />
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:DataList>  --%>
                                            
                                            <div id="divGridAlmacen" class="card card-accent-primary bg-gradient-light" style="display: none;" runat="server">
                                                <div class="card-header"><strong>Artículo - <asp:Label ID="lblDetArtcodart" runat="server" Text=""></asp:Label></strong></div>
                                                <div class="card-body">
                                                    <asp:GridView ID="grvArticuloAlmacen" runat="server" class="table table-sm table-bordered table-active table-active table-hover table-striped"
                                                        AutoGenerateColumns="False" DataSourceID="sqldsArticuloDetalle">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Almacén">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCodNomAlm" runat="server" Text=""><%# Eval("codalm") %> - <%# Eval("nomalm") %></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="exiact" HeaderText="Existencia" SortExpression="exiact" DataFormatString="{0:n}" />
                                                            <asp:BoundField DataField="ultcom" HeaderText="Ult Compra" SortExpression="ultcom" />
                                                            <asp:BoundField DataField="ultven" HeaderText="Ult Venta" SortExpression="ultven" />                                                            
                                                            <asp:BoundField DataField="coddes" HeaderText="% Desc." SortExpression="coddes" />
                                                            <asp:BoundField DataField="diaper" HeaderText="Días" SortExpression="diaper" DataFormatString="{0:n}" />
                                                        </Columns>
                                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                        <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                    </asp:GridView>                                                    
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Eliminar -->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Pedido</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar el Pedido '<asp:Label ID="lblEliFac" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarPedido_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>
</asp:Content>
