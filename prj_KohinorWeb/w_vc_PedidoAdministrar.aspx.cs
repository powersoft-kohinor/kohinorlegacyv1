﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_vc_PedidoAdministrar : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected string codmod = "3", niv001 = "2", niv002 = "6";
        protected cls_vc_Cliente objCliente = new cls_vc_Cliente();
        protected cls_vc_Pedido objPedido = new cls_vc_Pedido();
        protected cls_vc_Pedido_Ren objPedidoRen = new cls_vc_Pedido_Ren();
        protected cls_cfg_Secuencia objSecuencia = new cls_cfg_Secuencia();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //***i RENGLONES***//
                Session.Add("gs_BuscarNomart", "%"); //variable para buscar el articulo en btnPuntitosArticulos
                f_SetInitialRow();
                Session.Add("gs_Siguiente", ""); //para el lkbtnSiguiente Y lkbtnAtras

                f_Seg_Usuario_Menu();
                f_BindGrid_Inicial(); //grvCliente
                f_BindGrid_InicialArt();//grvArticulo

                if (Session["gs_tiptraSelected"] == null || Session["gs_numtraSelected"] == null) Response.Redirect("w_vc_Pedido.aspx");

                if (String.IsNullOrEmpty(Session["gs_tiptraSelected"].ToString()) && String.IsNullOrEmpty(Session["gs_numtraSelected"].ToString()))
                    Session.Add("gs_Action", "Add");
                else
                    Session.Add("gs_Action", "Update");

                ddlCodven.DataBind(); //OJO DEBE IR ANTES DE f_llenarCampos

                objPedido = objPedido.f_Pedido_Buscar(Session["gs_Codemp"].ToString(), Session["gs_tiptraSelected"].ToString(), Session["gs_numtraSelected"].ToString());
                if (String.IsNullOrEmpty(objPedido.Error.Mensaje))
                {
                    if (!String.IsNullOrEmpty(objPedido.Numtra))//si seleccionó un Pedido
                    {
                        Session["gs_tiptraSelected"] = objPedido.Tiptra; //VARIABLE PARA EL saber cual es el seleccionado
                        Session["gs_numtraSelected"] = objPedido.Numtra; //VARIABLE PARA EL saber cual es el seleccionado
                        Session["gs_Siguiente"] = objPedido.Numtra; //VARIABLE PARA EL lkbtnSiguiente
                        f_llenarCamposPedidoEnc(objPedido);

                        objPedidoRen = objPedidoRen.f_Pedido_Ren_Buscar(Session["gs_Codemp"].ToString(), Session["gs_tiptraSelected"].ToString(), Session["gs_numtraSelected"].ToString());
                        if (String.IsNullOrEmpty(objPedidoRen.Error.Mensaje))
                        {
                            ViewState["CurrentTable"] = objPedidoRen.dtPedidoRen;
                            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                            if (dtCurrentTable.Rows.Count > 0)
                            {
                                grvPedidoRen.DataSource = dtCurrentTable;
                                grvPedidoRen.DataBind();
                                f_SetPreviousData();
                            }
                            else
                            {
                                f_SetInitialRow();
                                f_SetPreviousData();
                                lblError.Text = "❗ *01. " + DateTime.Now + " Pedido no tiene renglones.";
                            }
                        }
                        else
                        {
                            f_ErrorNuevo(objPedidoRen.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                    else //crear Pedido desde cero
                    {
                        objSecuencia = objSecuencia.f_CalcularSecuencia(Session["gs_Codemp"].ToString(), "VC_PED", Session["gs_Sersec"].ToString());
                        objPedido.Numtra = objSecuencia.Numero;
                        objPedido.Fectra = objSecuencia.Fecha;
                        if (String.IsNullOrEmpty(objPedido.Error.Mensaje))
                        {
                            f_llenarCamposPedidoEnc(objPedido);
                        }
                        else
                        {
                            f_ErrorNuevo(objPedido.Error);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                }
                else
                {
                    f_ErrorNuevo(objPedido.Error);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
                //***f RENGLONES***//
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (Session["gs_Modo"].ToString() == "D")
            {
                btnAbrir.Attributes.Add("class", "btn btn-dark");
                btnNuevo.Attributes.Add("class", "btn btn-primary");
                btnGuardar.Attributes.Add("class", "btn btn-dark");
                btnCancelar.Attributes.Add("class", "btn btn-dark");
                btnEliminar.Attributes.Add("class", "btn btn-danger");
                div_Pedido.Attributes.Add("class", "card");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-primary my-2 my-sm-0 ");

            }
            if (Session["gs_Modo"].ToString() == "L")
            {
                btnAbrir.Attributes.Add("class", "btn btn-outline-dark");
                btnNuevo.Attributes.Add("class", "btn btn-outline-primary");
                div_Pedido.Attributes.Add("class", "card bg-gradient-secondary");
                btnGuardar.Attributes.Add("class", "btn btn-outline-dark");
                btnCancelar.Attributes.Add("class", "btn btn-outline-dark");
                btnEliminar.Attributes.Add("class", "btn btn-outline-danger ");
                lkbtnPrincipio.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnAtras.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSiguiente.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnFinal.Attributes.Add("class", "btn btn-outline-dark");
                lkbtnSearch.Attributes.Add("class", "btn btn-outline-primary my-2 my-sm-0 ");

            }
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        protected void f_Seg_Usuario_Menu()
        {
            if (Session["gs_CodUs1"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
            if (codmod != Session["gs_Codmod"].ToString()) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Ir a módulo (" + codmod + ") primero.");
            lblModuloActivo.Text = Session["gs_Nommod"].ToString();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsNivel001.Select(args);
            DataTable dt001 = view.ToTable();
            if (dt001.Rows.Count == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. Navegación por menú desactivada.");
            rptNivel001.DataSource = f_Seg_BanNiv001(dt001);
            rptNivel001.DataBind();

            int i_count = 0, i_existe = 0;//cuando no haya registro de nivel en tbl (ejm>no existe renglon niv001=1 && niv002=2), saber que no puede acceder a la pag, se le envia a Escritorio.aspx
            foreach (RepeaterItem item in rptNivel001.Items)
            {
                Repeater rptNivel002 = (Repeater)item.FindControl("rptNivel002");
                Session["gs_Niv001"] = dt001.Rows[item.ItemIndex]["niv001"].ToString();
                DataSourceSelectArguments args2 = new DataSourceSelectArguments();
                DataView view2 = (DataView)sqldsNivel002.Select(args2);
                DataTable dt002 = view2.ToTable();
                if (dt002.Rows.Count > 0) i_count++;
                foreach (DataRow row in dt002.Rows)
                    if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) i_existe++;
                rptNivel002.DataSource = f_Seg_BanNiv002(dt002);
                rptNivel002.DataBind();
            }
            //si no existe el resigitro de esta pag en la tbl lo envia al Escritorio
            if (i_count == 0 || i_existe == 0) Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos para acceder a la página solicitada.");
        }
        protected DataTable f_Seg_BanNiv001(DataTable dt) //metodo para validar banderas de nivel001
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "disabled";
                else
                    row["menhab"] = "c-sidebar-nav-dropdown-toggle";
            }
            return dt;
        }
        protected DataTable f_Seg_BanNiv002(DataTable dt) //metodo para validar banderas de nivel002
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002) && (row["menhab"].ToString().Equals("N") || row["menvis"].ToString().Equals("N"))) //si intenta acceder por URL y no tiene acceso a la pag
                    Response.Redirect("~/w_Escritorio.aspx?gs_RedirectError=❗ Acceso denegado. No tiene permisos o la página solicitada no esta habilitada.");

                if (row["menhab"].Equals("N")) //Deshabilitado
                    row["menhab"] = "c-sidebar-nav-link disabled";
                else
                    row["menhab"] = "c-sidebar-nav-link";

                if (row["niv001"].ToString().Equals(niv001) && row["niv002"].ToString().Equals(niv002)) //Gestiones --> Cliente
                {
                    if (row["banins"].Equals("N")) //Nuevo
                    {
                        btnNuevo.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnNuevo.Disabled = true;
                    }
                    if (row["banbus"].Equals("N")) //Abrir
                    {
                        btnAbrir.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnAbrir.Disabled = true;
                    }
                    if (row["bangra"].Equals("N")) //Guardar
                    {
                        btnGuardar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnGuardar.Disabled = true;
                    }
                    if (row["bancan"].Equals("N")) //Cancelar
                    {
                        btnCancelar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnCancelar.Disabled = true;

                    }
                    if (row["baneli"].Equals("N")) //Eliminiar
                    {
                        btnEliminar.Attributes.Add("class", "btn btn-outline-secondary disabled");
                        btnEliminar.Disabled = true;
                        Session["baneli"] = "N";
                    }
                }
            }
            return dt;
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_tiptraSelected"] = "";
            Session["gs_numtraSelected"] = "";
            f_limpiarCampos();
        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_vc_Pedido.aspx");
        }
        protected void btnGuardarPedido_Click(object sender, EventArgs e)
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsPedidoExiste.Select(args);
            if (view != null) //ya existe el pedido (Actualizar)
            {
                objPedido = f_generarObjPedido();
                clsError objError = objPedido.f_Pedido_Actualizar(objPedido, "Update");
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    //ACTUALIZA RENGLON
                    DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
                    objPedidoRen.dtPedidoRen = dtRenglones;
                    objError = objPedidoRen.f_Pedido_Ren_Actualizar(objPedidoRen, "Update");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                    }
                    else //error en renglones
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
            else //no existe el pedido (Insertar)
            {
                objPedido = f_generarObjPedido();
                clsError objError = objPedido.f_Pedido_Actualizar(objPedido, "Add");
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    //INSERTA RENGLON
                    DataTable dtRenglones = (DataTable)ViewState["CurrentTable"];
                    objPedidoRen = f_generarObjPedidoRen();
                    objPedidoRen.dtPedidoRen = dtRenglones;
                    objError = objPedidoRen.f_Pedido_Ren_Actualizar(objPedidoRen, "Add");
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        objError = objSecuencia.f_ActualizarSecuencia(Session["gs_Codemp"].ToString(), "VC_PED", Session["gs_Sersec"].ToString());
                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                            lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                        }
                        else //error en actualizar secuencia
                        {
                            f_ErrorNuevo(objError);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                    else //error en renglones
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Session["gs_Action"] = "Add";
            Session["gs_tiptraSelected"] = "";
            Session["gs_numtraSelected"] = "";
            f_limpiarCampos();
        }
        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_tiptraSelected"].ToString()) && !String.IsNullOrEmpty(Session["gs_numtraSelected"].ToString()))
            {
                Session["gs_numtraSelected"] = txtNumtra.Text;
                lblEliFac.Text = Session["gs_numtraSelected"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar una Pedido para eliminar.";
            }
        }
        protected void btnEliminarPedido_Click(object sender, EventArgs e)
        {
            cls_vc_Pedido objPedido = new cls_vc_Pedido(); //eliminar
            clsError objError = objPedido.f_Pedido_Eliminar(Session["gs_Codemp"].ToString(), Session["gs_tiptraSelected"].ToString(), Session["gs_numtraSelected"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                lblExito.Text = "✔️ Guardado Exitoso. " + DateTime.Now;
                f_limpiarCampos();
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void lkbtnPrincipio_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objPedido = objPedido.f_Pedido_Principio(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
            if (String.IsNullOrEmpty(objPedido.Error.Mensaje))
            {
                Session["gs_numtraSelected"] = objPedido.Numtra; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objPedido.Numtra; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposPedidoEnc(objPedido);
                ViewState["CurrentTable"] = objPedido.PedidoRen.dtPedidoRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvPedidoRen.DataSource = dtCurrentTable;
                    grvPedidoRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    lblError.Text = "❗ *01. " + DateTime.Now + " Pedido no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objPedido.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnSiguiente_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objPedido = objPedido.f_Pedido_Siguiente(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString(), Session["gs_Siguiente"].ToString());
            if (String.IsNullOrEmpty(objPedido.Error.Mensaje))
            {
                Session["gs_numtraSelected"] = objPedido.Numtra; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objPedido.Numtra; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposPedidoEnc(objPedido);
                ViewState["CurrentTable"] = objPedido.PedidoRen.dtPedidoRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvPedidoRen.DataSource = dtCurrentTable;
                    grvPedidoRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    f_SetInitialRow();
                    f_SetPreviousData();
                    lblError.Text = "❗ *01. " + DateTime.Now + " Pedido no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objPedido.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnFinal_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objPedido = objPedido.f_Pedido_Final(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString());
            if (String.IsNullOrEmpty(objPedido.Error.Mensaje))
            {
                Session["gs_numtraSelected"] = objPedido.Numtra; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objPedido.Numtra; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposPedidoEnc(objPedido);
                ViewState["CurrentTable"] = objPedido.PedidoRen.dtPedidoRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvPedidoRen.DataSource = dtCurrentTable;
                    grvPedidoRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    lblError.Text = "❗ *01. " + DateTime.Now + " Pedido no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objPedido.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void lkbtnAtras_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();

            objPedido = objPedido.f_Pedido_Atras(Session["gs_Codemp"].ToString(), Session["gs_Sersec"].ToString(), Session["gs_Siguiente"].ToString());
            if (String.IsNullOrEmpty(objPedido.Error.Mensaje))
            {
                Session["gs_numtraSelected"] = objPedido.Numtra; //VARIABLE PARA EL saber cual es el seleccionado
                Session["gs_Siguiente"] = objPedido.Numtra; //VARIABLE PARA EL lkbtnSiguiente
                f_llenarCamposPedidoEnc(objPedido);
                ViewState["CurrentTable"] = objPedido.PedidoRen.dtPedidoRen;
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    grvPedidoRen.DataSource = dtCurrentTable;
                    grvPedidoRen.DataBind();
                    f_SetPreviousData();
                }
                else
                {
                    f_SetInitialRow();
                    f_SetPreviousData();
                    lblError.Text = "❗ *01. " + DateTime.Now + " Pedido no tiene renglones.";
                }
            }
            else
            {
                f_ErrorNuevo(objPedido.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }


        //***i CLIENTE ***//
        [WebMethod(enableSession: true)]
        public static cls_vc_Pedido f_buscarCli(string s_codcli) //tab en txtClicxc
        {
            cls_vc_Pedido objPedido = new cls_vc_Pedido();
            if (s_codcli.Equals("")) //tab en txtClicxc y esta vacio
            {
                objPedido.Clicxc = "";
                objPedido.Nomcxc = "";
                objPedido.Lispre = "";
                objPedido.Observ = "";

                return objPedido;
            }
            return objPedido.f_Pedido_Buscar_Cliente(HttpContext.Current.Session["gs_Codemp"].ToString(), s_codcli);
        }
        //***f CLIENTE ***//

        //***i GRVCLIENTE ***//
        protected void grvCliente_RowDataBound(object sender, GridViewRowEventArgs e) //only fires when the GridView's data changes during the postback
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;

            }
        }
        protected void grvCliente_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {

            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }
        }
        protected void grvCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvCliente.SelectedRow;
            string s_codcli = fila.Cells[2].Text.Trim();

            Session["gs_codcliSelected"] = s_codcli; //variable para que al seleccionar uno en el grv se obtenga el codcli (sg_codcliSELECTED ->Editar)

            objCliente = objCliente.f_Cliente_Buscar(Session["gs_Codemp"].ToString(), s_codcli);
            if (String.IsNullOrEmpty(objCliente.Error.Mensaje))
            {
                f_llenarCamposCliente(objCliente);

            }
            else
            {
                f_ErrorNuevo(objCliente.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        private void f_BindGrid_Inicial()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsCliente.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();
            Session.Add("gdt_Cliente", dt);

            grvCliente.DataSource = dt;
            f_BindGrid();
        }
        private void f_BindGrid(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Cliente"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "nomcli LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codcli LIKE " + "'%" + searchText.Trim() + "%'";
                    grvCliente.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirection;
                    grvCliente.DataSource = dv;
                }
                grvCliente.DataBind();
            }
        }
        private string SortDirection
        {
            get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }
        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(null, txtSearchCliente.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalCliente();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCliente.PageIndex = e.NewPageIndex;
            f_BindGrid(null, txtSearchCliente.Text);
        }
        protected void OnSorting(object sender, GridViewSortEventArgs e)
        {
            f_BindGrid(e.SortExpression, txtSearchCliente.Text);
        }
        public void f_llenarCamposCliente(cls_vc_Cliente objCliente)
        {
            txtNomcxc.Text = objCliente.Nomcli;
            //Session["gs_Siguiente"] = objFactura.Nomcli;
            txtClicxc.Value = objCliente.Codcli;
            ddlLispre.SelectedValue = objCliente.Lispre;
            txtCiucli.Text = objCliente.Ciucli;

            Session.Add("gs_Numpag", objCliente.Numpag);
            Session.Add("gs_Plapag", objCliente.Plapag);



            //txtObserv.Text = "";
            //txtNtardes.Text = "";
            //txtNumfac.Text = "";

            //txtEstado.Text = "";
            //txtCodalm.Text = "";
            //txtTotnet.Text = "";
            //txtTotbas.Text = "";
            //txtTotdes.Text = "";
            //txtTotiva.Text = "";
            //txtTotfac.Text = "";
        }

        //***f GRVCLIENTE ***//


        //***i CODIGO DE RENGLONES ***//
        private void f_SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("N°", typeof(string)));
            dt.Columns.Add(new DataColumn("codart", typeof(string)));
            dt.Columns.Add(new DataColumn("nomart", typeof(string)));
            dt.Columns.Add(new DataColumn("codalm", typeof(string)));
            dt.Columns.Add(new DataColumn("coduni", typeof(string)));
            dt.Columns.Add(new DataColumn("cantid", typeof(decimal)));
            dt.Columns.Add(new DataColumn("preuni", typeof(decimal)));
            dt.Columns.Add(new DataColumn("poriva", typeof(decimal)));
            dt.Columns.Add(new DataColumn("desren", typeof(decimal)));
            dt.Columns.Add(new DataColumn("totren", typeof(decimal)));

            dr = dt.NewRow();
            dr["N°"] = 1;
            dr["codart"] = string.Empty;
            dr["nomart"] = string.Empty;
            dr["codalm"] = string.Empty;
            dr["coduni"] = string.Empty;
            dr["cantid"] = 0.00;
            dr["preuni"] = 0.00;
            dr["poriva"] = 0.00;
            dr["desren"] = 0.00;
            dr["totren"] = 0.00;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            grvPedidoRen.DataSource = dt;
            grvPedidoRen.DataBind();
        }
        private void f_AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values (OJO el .Cells[2] no importa porq esta buscando el control con rowIndex)
                        TextBox codart = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        DropDownList codalm = (DropDownList)grvPedidoRen.Rows[rowIndex].Cells[4].FindControl("ddlgrvCodalm");
                        TextBox coduni = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[5].FindControl("txtgrvCoduni");
                        TextBox cantid = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[6].FindControl("txtgrvCantid");
                        TextBox preuni = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[7].FindControl("txtgrvPreuni");
                        TextBox poriva = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[8].FindControl("txtgrvPoriva");
                        TextBox desren = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[9].FindControl("txtgrvDesren");
                        TextBox totren = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[10].FindControl("txtgrvTotren");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["codart"] = codart.Text;
                        dtCurrentTable.Rows[i - 1]["nomart"] = nomart.Text;
                        dtCurrentTable.Rows[i - 1]["codalm"] = codalm.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["coduni"] = coduni.Text;
                        if (cantid.Text == "" && preuni.Text == "" && poriva.Text == "" && desren.Text == "" && totren.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["preuni"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["poriva"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["desren"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["totren"] = 0.00;
                        }
                        else
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = String.IsNullOrEmpty(cantid.Text) ? 0 : Convert.ToDecimal(cantid.Text);
                            dtCurrentTable.Rows[i - 1]["preuni"] = String.IsNullOrEmpty(preuni.Text) ? 0 : Convert.ToDecimal(preuni.Text);
                            dtCurrentTable.Rows[i - 1]["poriva"] = String.IsNullOrEmpty(poriva.Text) ? 0 : Convert.ToDecimal(poriva.Text);
                            dtCurrentTable.Rows[i - 1]["desren"] = String.IsNullOrEmpty(desren.Text) ? 0 : Convert.ToDecimal(desren.Text);
                            dtCurrentTable.Rows[i - 1]["totren"] = String.IsNullOrEmpty(totren.Text) ? 0 : Convert.ToDecimal(totren.Text);
                        }
                        rowIndex++;
                    }

                    drCurrentRow["cantid"] = 0.00;
                    drCurrentRow["preuni"] = 0.00;
                    drCurrentRow["poriva"] = 0.00;
                    drCurrentRow["desren"] = 0.00;
                    drCurrentRow["totren"] = 0.00;
                    dtCurrentTable.Rows.Add(drCurrentRow);

                    ViewState["CurrentTable"] = dtCurrentTable;

                    grvPedidoRen.DataSource = dtCurrentTable;
                    grvPedidoRen.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            f_SetPreviousData();
        }
        private void f_SetPreviousData() //para actualizar el grvFacturaRen con CurrentTable (llenar los textbox del grv)
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox codart = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        DropDownList codalm = (DropDownList)grvPedidoRen.Rows[rowIndex].Cells[4].FindControl("ddlgrvCodalm");
                        TextBox coduni = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[5].FindControl("txtgrvCoduni");
                        TextBox cantid = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[6].FindControl("txtgrvCantid");
                        TextBox preuni = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[7].FindControl("txtgrvPreuni");
                        TextBox proiva = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[8].FindControl("txtgrvPoriva");
                        TextBox desren = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[9].FindControl("txtgrvDesren");
                        TextBox totren = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[10].FindControl("txtgrvTotren");

                        codart.Text = dt.Rows[i]["codart"].ToString();
                        nomart.Text = dt.Rows[i]["nomart"].ToString();
                        codalm.SelectedValue = dt.Rows[i]["codalm"].ToString();
                        coduni.Text = dt.Rows[i]["coduni"].ToString();
                        cantid.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["cantid"]);
                        preuni.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["preuni"]);
                        proiva.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["poriva"]);
                        desren.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["desren"]);
                        totren.Text = String.Format("{0:0.##}", (Decimal)dt.Rows[i]["totren"]);

                        rowIndex++;
                    }
                }
            }
        }

        //*i BOTONES INFERIOR *//
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            f_AddNewRowToGrid();
        }
        protected void btnEliminarTodo_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];

            for (int i = 0; i < grvPedidoRen.Rows.Count; i++)
            {
                int rowID = grvPedidoRen.SelectedIndex + 1;
                if (ViewState["CurrentTable"] != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (grvPedidoRen.SelectedIndex < dt.Rows.Count - 1)
                        {
                            //Remove the Selected Row data
                            dt.Rows.RemoveAt(i);
                        }
                    }

                    //Store the current data in ViewState for future reference
                    ViewState["CurrentTable"] = dt;
                    //Re bind the GridView for the updated data
                    grvPedidoRen.DataSource = dt;
                    grvPedidoRen.DataBind();
                    i = 0;
                }
            }
            //para borrar los datos de la primera fila....sobra despues de que se ha eliminado todos los demas renglones
            dt.Rows[0]["codart"] = "";
            dt.Rows[0]["nomart"] = "";
            dt.Rows[0]["codalm"] = "";
            dt.Rows[0]["coduni"] = "";
            dt.Rows[0]["cantid"] = 0.00;
            dt.Rows[0]["preuni"] = 0.00;
            dt.Rows[0]["poriva"] = 0.00;
            dt.Rows[0]["desren"] = 0.00;
            dt.Rows[0]["totren"] = 0.00;

            //Set Previous Data on Postbacks
            f_SetPreviousData();

            f_calcularTblTotales();
        }
        protected void lkgrvBorrar_Click(object sender, EventArgs e)
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox codart = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        DropDownList codalm = (DropDownList)grvPedidoRen.Rows[rowIndex].Cells[4].FindControl("ddlgrvCodalm");
                        TextBox coduni = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[5].FindControl("txtgrvCoduni");
                        TextBox cantid = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[6].FindControl("txtgrvCantid");
                        TextBox preuni = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[7].FindControl("txtgrvPreuni");
                        TextBox poriva = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[8].FindControl("txtgrvPoriva");
                        TextBox desren = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[9].FindControl("txtgrvDesren");
                        TextBox totren = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[10].FindControl("txtgrvTotren");


                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["codart"] = codart.Text;
                        dtCurrentTable.Rows[i - 1]["nomart"] = nomart.Text;
                        dtCurrentTable.Rows[i - 1]["codalm"] = codalm.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["coduni"] = coduni.Text;
                        if (cantid.Text == "" && preuni.Text == "" && poriva.Text == "" && desren.Text == "" && totren.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["preuni"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["poriva"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["desren"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["totren"] = 0.00;
                        }
                        else
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = String.IsNullOrEmpty(cantid.Text) ? 0 : Convert.ToDecimal(cantid.Text);
                            dtCurrentTable.Rows[i - 1]["preuni"] = String.IsNullOrEmpty(preuni.Text) ? 0 : Convert.ToDecimal(preuni.Text);
                            dtCurrentTable.Rows[i - 1]["poriva"] = String.IsNullOrEmpty(poriva.Text) ? 0 : Convert.ToDecimal(poriva.Text);
                            dtCurrentTable.Rows[i - 1]["desren"] = String.IsNullOrEmpty(desren.Text) ? 0 : Convert.ToDecimal(desren.Text);
                            dtCurrentTable.Rows[i - 1]["totren"] = String.IsNullOrEmpty(totren.Text) ? 0 : Convert.ToDecimal(totren.Text);
                        }
                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }

            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (!(gvRow.RowIndex == 0 && dt.Rows.Count == 1)) //no permite borrar la primera fila
                    {
                        //Remove the Selected Row data
                        dt.Rows.Remove(dt.Rows[rowID]);
                    }
                }
                else //si selecciona la primera fila
                {
                    if (gvRow.RowIndex == 0 && dt.Rows.Count == 1) //para borrar los datos de la primera fila
                    {
                        dt.Rows[0]["codart"] = "";
                        dt.Rows[0]["nomart"] = "";
                        dt.Rows[0]["codalm"] = "";
                        dt.Rows[0]["coduni"] = "";
                        dt.Rows[0]["cantid"] = 0.00;
                        dt.Rows[0]["preuni"] = 0.00;
                        dt.Rows[0]["poriva"] = 0.00;
                        dt.Rows[0]["desren"] = 0.00;
                        dt.Rows[0]["totren"] = 0.00;
                    }
                }
                //Store the current data in ViewState for future reference
                ViewState["CurrentTable"] = dt;
                //Re bind the GridView for the updated data
                grvPedidoRen.DataSource = dt;
                grvPedidoRen.DataBind();
            }
            //Set Previous Data on Postbacks
            f_SetPreviousData();
            f_calcularTblTotales();
        }
        //*f BOTONES INFERIOR *//

        //*i SELECCION ARTICULOS *//
        protected void btnPuntitosRen_Click(object sender, EventArgs e) //Abrir Modal Articulos
        {
            Button btn = (Button)sender;
            GridViewRow gvRow = (GridViewRow)btn.NamingContainer;
            int rowIndex = gvRow.RowIndex; //para saber la fila seleccionada
            Session.Add("gs_renSelecxtedRow", rowIndex); //para saber en donde poner el articulo seleccionado del grvArticulos
            divGridAlmacen.Attributes.Add("style", "display: none;");

            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalFacturaArticulos();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void grvArticulo_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }
        }
        protected void grvArticulo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = grvArticulo.SelectedRow;
            string s_codart = row.Cells[3].Text.Trim();
            string s_nomart = Server.HtmlDecode(row.Cells[4].Text.Trim()); //para que acepte caracteres especiales (Ñ)
            string s_coduni = row.Cells[6].Text.Trim();
            string s_preuni = row.Cells[7].Text.Trim();            

            objPedidoRen.Codart = s_codart;
            objPedidoRen.Nomart = s_nomart;
            objPedidoRen.Coduni = s_coduni;
            objPedidoRen.Preuni = decimal.Parse(s_preuni);
            objPedidoRen.Cantid = 1;

            int rowIndex = int.Parse(Session["gs_renSelecxtedRow"].ToString());
            f_llenarCamposPedidoRen(objPedidoRen, rowIndex);
        }
        private void f_BindGrid_InicialArt()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsArticulo.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();
            Session.Add("gdt_Articulo", dt);

            grvArticulo.DataSource = dt;
            f_BindGridArt();
        }
        private void f_BindGridArt(string sortExpression = null, string searchText = null)
        {
            //Para consultar a DB si selecciona lkbtnBuscarNomart
            DataTable dt = new DataTable();
            if (searchText != null)
            {
                Session["gs_BuscarNomart"] = searchText;
                DataSourceSelectArguments args = new DataSourceSelectArguments();
                DataView view = (DataView)sqldsArticulo.Select(args); //procedimiento almacenado
                dt = view.ToTable();
                Session["gdt_Articulo"] = dt;
            }
            else
            {
                dt = (DataTable)Session["gdt_Articulo"];
            }
            
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "nomart LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codart LIKE " + "'%" + searchText.Trim() + "%'";
                    grvArticulo.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirectionArt = this.SortDirectionArt == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirectionArt;
                    grvArticulo.DataSource = dv;
                }
                grvArticulo.DataBind();
            }
        }
        private string SortDirectionArt
        {
            get { return ViewState["SortDirectionArt"] != null ? ViewState["SortDirectionArt"].ToString() : "ASC"; }
            set { ViewState["SortDirectionArt"] = value; }
        }
        protected void f_GridBuscarArt(object sender, EventArgs e)
        {
            f_BindGridArt(null, txtSearchArt.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalFacturaArticulos();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void OnPageIndexChangingArt(object sender, GridViewPageEventArgs e)
        {
            grvArticulo.PageIndex = e.NewPageIndex;
            f_BindGridArt(null, txtSearchArt.Text);
        }
        protected void OnSortingArt(object sender, GridViewSortEventArgs e)
        {
            f_BindGridArt(e.SortExpression, txtSearchArt.Text);
        }
        protected void lkbtnDetArticulo_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)btn.NamingContainer;
            string s_codart = gvRow.Cells[3].Text.Trim();
            lblDetArtcodart.Text = s_codart;
            Session.Add("gs_DetalleArt", s_codart);
            divGridAlmacen.Attributes.Add("style", "display: normal;");
            grvArticuloAlmacen.DataBind();
            //DataList1.DataBind();
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalFacturaArticulos();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        //*f SELECCION ARTICULOS *//


        //*i CALCULOS RENGLON*//
        public void f_recalcularRenglon(int i_rowID)
        {
            GridViewRow row = grvPedidoRen.Rows[i_rowID];

            try
            {
                TextBox txtgrvCantid = (TextBox)row.FindControl("txtgrvCantid");
                if (String.IsNullOrEmpty(txtgrvCantid.Text)) txtgrvCantid.Text = "1";
                objPedidoRen.Cantid = decimal.Parse(txtgrvCantid.Text);

                TextBox txtgrvPreuni = (TextBox)row.FindControl("txtgrvPreuni");
                if (String.IsNullOrEmpty(txtgrvPreuni.Text)) txtgrvPreuni.Text = "0.00";                
                objPedidoRen.Preuni = decimal.Parse(txtgrvPreuni.Text);

                TextBox txtgrvDesren = (TextBox)row.FindControl("txtgrvDesren");
                if (String.IsNullOrEmpty(txtgrvDesren.Text)) txtgrvDesren.Text = "0.00";
                objPedidoRen.Desren = decimal.Parse(txtgrvDesren.Text);

                TextBox txtgrvCodart = (TextBox)row.FindControl("txtgrvCodart");
                objPedidoRen.Codart = txtgrvCodart.Text;
                TextBox txtgrvNomart = (TextBox)row.FindControl("txtgrvNomart");
                objPedidoRen.Nomart = txtgrvNomart.Text;
                DropDownList ddlgrvCodalm = (DropDownList)row.FindControl("ddlgrvCodalm");
                objPedidoRen.Codalm = ddlgrvCodalm.SelectedValue;
                TextBox txtgrvCoduni = (TextBox)row.FindControl("txtgrvCoduni");
                objPedidoRen.Coduni = txtgrvCoduni.Text;

                f_llenarCamposPedidoRen(objPedidoRen, i_rowID);

            }
            catch (Exception ex)
            {
                objPedidoRen.Error = new clsError();
                objPedidoRen.Error = objPedidoRen.Error.f_ErrorControlado(ex);
                f_ErrorNuevo(objPedidoRen.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        protected void txtgrvCantid_TextChanged(object sender, EventArgs e)
        {
            TextBox tx = (TextBox)sender;
            GridViewRow gvRow = (GridViewRow)tx.NamingContainer;
            int i_rowID = gvRow.RowIndex;//para saber la fila seleccionada
            f_recalcularRenglon(i_rowID);
        }
        protected void txtgrvPreuni_TextChanged(object sender, EventArgs e)
        {
            TextBox tx = (TextBox)sender;
            GridViewRow gvRow = (GridViewRow)tx.NamingContainer;
            int i_rowID = gvRow.RowIndex;//para saber la fila seleccionada
            f_recalcularRenglon(i_rowID);
        }
        protected void txtgrvDesren_TextChanged(object sender, EventArgs e)
        {
            TextBox tx = (TextBox)sender;
            GridViewRow gvRow = (GridViewRow)tx.NamingContainer;
            int i_rowID = gvRow.RowIndex;//para saber la fila seleccionada
            f_recalcularRenglon(i_rowID);
        }
        public string f_CalculoPorIva(string s_codart)
        {
            objPedidoRen = objPedidoRen.f_CalculoPorIva(Session["gs_Codemp"].ToString(), s_codart);
            if (!String.IsNullOrEmpty(objPedidoRen.Error.Mensaje)) //si da error muestra f_ModalError() y objFacturaRen.Poriva vale "0"
            {
                f_ErrorNuevo(objPedidoRen.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

            return objPedidoRen.Poriva.ToString();
        }

        //*f CALCULOS RENGLON*//


        //*i CALCULOS DERECHA*//
        public void f_calcularTblTotales() //tblTotales es la tabla de la derecha.
        {
            f_renglonesActuales();
            double r_totNeto = f_cantTotal();
            double r_totBas = f_cantBaseImponible();
            double r_totIva = r_totBas * 0.12;
            double r_totFac = r_totNeto + r_totIva;
            txtTotnet.Text = r_totNeto.ToString("N2");
            txtTotbas.Text = r_totBas.ToString("N2");
            txtTotiva.Text = r_totIva.ToString("N2");
            txtTotfac.Text = r_totFac.ToString("N2");

        }
        public double f_cantBaseImponible() //recorre todos los datos de la columna total, sumando solo los del 12% IVA
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            int nrow = dt.Rows.Count;
            int i = 0;
            double r_ctotal = 0;
            try
            {
                while (i < nrow)
                {
                    if (!dt.Rows[i]["totren"].ToString().Equals("") && !dt.Rows[i]["poriva"].ToString().Equals("12.00")) //si txtgrvTotal NO esta vacio suma
                    {
                        r_ctotal = r_ctotal + double.Parse(dt.Rows[i]["totren"].ToString());
                    }
                    i = i + 1;
                }
            }
            catch (Exception)
            {
                Response.Write("<script>alert('Todos los totales deben estar llenos.');</script>");
            }

            return r_ctotal;
        }
        public double f_cantTotal()//para txtTotnet ...recorre todos los datos de la columna total, sumando los num para sacar la cantidad total
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            int nrow = dt.Rows.Count;
            int i = 0;
            double r_ctotal = 0;
            try
            {
                while (i < nrow)
                {
                    if (!dt.Rows[i]["totren"].ToString().Equals("")) //si txtgrvTotal NO esta vacio suma
                    {
                        r_ctotal = r_ctotal + double.Parse(dt.Rows[i]["totren"].ToString());
                    }
                    i = i + 1;
                }
            }
            catch (Exception)
            {
                Response.Write("<script>alert('Todos los totales deben estar llenos.');</script>");
            }

            return r_ctotal;
        }
        public void f_renglonesActuales() //para guardar en ViewState["CurrentTable"] los renglones mostrados en pantalla
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox codart = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[2].FindControl("txtgrvCodart");
                        TextBox nomart = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[3].FindControl("txtgrvNomart");
                        DropDownList codalm = (DropDownList)grvPedidoRen.Rows[rowIndex].Cells[4].FindControl("ddlgrvCodalm");
                        TextBox coduni = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[5].FindControl("txtgrvCoduni");
                        TextBox cantid = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[6].FindControl("txtgrvCantid");
                        TextBox preuni = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[7].FindControl("txtgrvPreuni");
                        TextBox poriva = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[8].FindControl("txtgrvPoriva");
                        TextBox desren = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[9].FindControl("txtgrvDesren");
                        TextBox totren = (TextBox)grvPedidoRen.Rows[rowIndex].Cells[10].FindControl("txtgrvTotren");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["N°"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["codart"] = codart.Text;
                        dtCurrentTable.Rows[i - 1]["nomart"] = nomart.Text;
                        dtCurrentTable.Rows[i - 1]["codalm"] = codalm.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["coduni"] = coduni.Text;
                        if (cantid.Text == "" && preuni.Text == "" && poriva.Text == "" && desren.Text == "" && totren.Text == "")
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["preuni"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["poriva"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["desren"] = 0.00;
                            dtCurrentTable.Rows[i - 1]["totren"] = 0.00;
                        }
                        else
                        {
                            dtCurrentTable.Rows[i - 1]["cantid"] = String.IsNullOrEmpty(cantid.Text) ? 0 : Convert.ToDecimal(cantid.Text);
                            dtCurrentTable.Rows[i - 1]["preuni"] = String.IsNullOrEmpty(preuni.Text) ? 0 : Convert.ToDecimal(preuni.Text);
                            dtCurrentTable.Rows[i - 1]["poriva"] = String.IsNullOrEmpty(poriva.Text) ? 0 : Convert.ToDecimal(poriva.Text);
                            dtCurrentTable.Rows[i - 1]["desren"] = String.IsNullOrEmpty(desren.Text) ? 0 : Convert.ToDecimal(desren.Text);
                            dtCurrentTable.Rows[i - 1]["totren"] = String.IsNullOrEmpty(totren.Text) ? 0 : Convert.ToDecimal(totren.Text);
                        }
                        rowIndex++;
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }
        }
        //*f CALCULOS DERECHA*//

        //***f CODIGO DE RENGLONES ***//

        public void f_llenarCamposPedidoEnc(cls_vc_Pedido objPedido)
        {
            txtNomcxc.Text = objPedido.Nomcxc;
            //Session["gs_Siguiente"] = objFactura.Nomcli;
            txtClicxc.Value = objPedido.Clicxc;
            txtObserv.Text = objPedido.Observ;
            txtFecent.Text = objPedido.Fecent.ToString("yyyy-MM-dd");
            txtValido.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtNumtra.Text = objPedido.Numtra;
            txtPoriva.Text = objPedido.Poriva.ToString();
            txtPordes.Text = objPedido.Pordes.ToString();
            txtTotnet.Text = objPedido.Totnet.ToString();
            txtTotbas.Text = objPedido.Totbas.ToString();
            txtTotdes.Text = objPedido.Totdes.ToString();
            txtTotiva.Text = objPedido.Totiva.ToString();
            txtTotfac.Text = objPedido.Totfac.ToString();

            string s_codalm = objPedido.Codalm;
            string s_codven = objPedido.Codven;
            string s_estado = objPedido.Estado;
            string s_lispre = objPedido.Lispre;

            if (String.IsNullOrEmpty(s_codven))
                ddlCodven.SelectedValue = ddlCodven.Items.FindByValue("00").Value;
            else
                ddlCodven.SelectedValue = ddlCodven.Items.FindByValue(s_codven.Trim()).Value;
            if (String.IsNullOrEmpty(s_codalm))
                txtCodalm.Text = Session["gs_CodAlm"].ToString();
            else
                txtCodalm.Text = objPedido.Codalm;
            if (String.IsNullOrEmpty(s_estado))
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue("P").Value;
            else
                ddlEstado.SelectedValue = ddlEstado.Items.FindByValue(s_estado.Trim()).Value;
            if (String.IsNullOrEmpty(s_lispre))
                ddlLispre.SelectedValue = ddlLispre.Items.FindByValue("1").Value;
            else
                ddlLispre.SelectedValue = ddlLispre.Items.FindByValue(s_lispre.Trim()).Value;

            txtFectra.Text = String.IsNullOrEmpty(objPedido.Fectra.ToString()) ? DateTime.Now.Date.ToString("yyyy-MM-dd") : objPedido.Fectra.ToString("yyyy-MM-dd");
        }
        public void f_llenarCamposPedidoRen(cls_vc_Pedido_Ren objPedidoRen, int rowIndex)
        {
            GridViewRow row = grvPedidoRen.Rows[rowIndex];

            TextBox txtgrvCodart = (TextBox)row.FindControl("txtgrvCodart");
            TextBox txtgrvNomart = (TextBox)row.FindControl("txtgrvNomart");
            DropDownList ddlgrvCodalm = (DropDownList)row.FindControl("ddlgrvCodalm");
            TextBox txtgrvCoduni = (TextBox)row.FindControl("txtgrvCoduni");
            TextBox txtgrvCantid = (TextBox)row.FindControl("txtgrvCantid");
            TextBox txtgrvPreuni = (TextBox)row.FindControl("txtgrvPreuni");
            TextBox txtgrvPoriva = (TextBox)row.FindControl("txtgrvPoriva");
            TextBox txtgrvDesren = (TextBox)row.FindControl("txtgrvDesren");
            TextBox txtgrvTotren = (TextBox)row.FindControl("txtgrvTotren");

            txtgrvCodart.Text = objPedidoRen.Codart;
            txtgrvNomart.Text = objPedidoRen.Nomart;
            ddlgrvCodalm.SelectedValue = objPedidoRen.Codalm;
            txtgrvCoduni.Text = objPedidoRen.Coduni;
            txtgrvCantid.Text = objPedidoRen.Cantid.ToString();
            double r_preuni = (double)objPedidoRen.Preuni; //para solo mostrar 2 decimales
            txtgrvPreuni.Text = r_preuni.ToString("N2");
            string s_poriva = !objPedidoRen.Codart.Trim().Equals("*") ? f_CalculoPorIva(objPedidoRen.Codart) : "0";
            txtgrvPoriva.Text = s_poriva;
            double r_poriva = double.Parse(s_poriva);
            double r_desren = (double)objPedidoRen.Desren;
            txtgrvDesren.Text = r_desren.ToString("N2");
            double r_totRen = (double)objPedidoRen.Cantid * r_preuni;
            r_totRen = r_totRen - (r_totRen * (r_desren / 100)); //resta el % de descuento 
            txtgrvTotren.Text = r_totRen.ToString("N2");

            if (!objPedidoRen.Codart.Trim().Equals("*"))
                f_calcularTblTotales();

        }
        public void f_limpiarCampos()
        {
            //Image2.ImageUrl = "~/imgusu/user.png";
            txtNomcxc.Text = txtClicxc.Value = txtObserv.Text = txtNumpro.Text = txtForpag.Text = "";
            ddlLispre.SelectedIndex = 0;
            txtFecent.Text = txtValido.Text = DateTime.Now.ToString("yyyy-MM-dd");
            objSecuencia = objSecuencia.f_CalcularSecuencia(Session["gs_Codemp"].ToString(), "VC_PED", Session["gs_Sersec"].ToString());
            objPedido.Numtra = objSecuencia.Numero;
            objPedido.Fectra = objSecuencia.Fecha;
            if (String.IsNullOrEmpty(objSecuencia.Error.Mensaje))
            {
                f_llenarCamposPedidoEnc(objPedido);
            }
            else
            {
                f_ErrorNuevo(objPedido.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

            DataTable dt = (DataTable)ViewState["CurrentTable"];
            for (int i = 0; i < grvPedidoRen.Rows.Count; i++)
            {
                int rowID = grvPedidoRen.SelectedIndex + 1;
                if (ViewState["CurrentTable"] != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (grvPedidoRen.SelectedIndex < dt.Rows.Count - 1)
                        {
                            //Remove the Selected Row data
                            dt.Rows.RemoveAt(i);
                        }
                    }

                    //Store the current data in ViewState for future reference
                    ViewState["CurrentTable"] = dt;
                    //Re bind the GridView for the updated data
                    grvPedidoRen.DataSource = dt;
                    grvPedidoRen.DataBind();
                    i = 0;
                }
            }

            //para borrar los datos de la primera fila....sobra despues de que se ha eliminado todos los demas renglones
            if (grvPedidoRen.Rows.Count == 0)
            {
                f_AddNewRowToGrid();
            }
            else
            {
                dt.Rows[0]["codart"] = "";
                dt.Rows[0]["nomart"] = "";
                dt.Rows[0]["codalm"] = "";
                dt.Rows[0]["coduni"] = "";
                dt.Rows[0]["cantid"] = 0.00;
                dt.Rows[0]["preuni"] = 0.00;
                dt.Rows[0]["poriva"] = 0.00;
                dt.Rows[0]["desren"] = 0.00;
                dt.Rows[0]["totren"] = 0.00;
            }
            //Set Previous Data on Postbacks
            f_SetPreviousData();
            f_calcularTblTotales();

            lblError.Text = lblError.Text = "";
        }
        public cls_vc_Pedido f_generarObjPedido()
        {
            //CAMPOS NORMALES
            objPedido.Codemp = Session["gs_Codemp"].ToString();
            objPedido.Nomcxc = txtNomcxc.Text;
            objPedido.Clicxc = txtClicxc.Value;
            objPedido.Lispre = ddlLispre.Text;
            objPedido.Observ = txtObserv.Text;
            objPedido.Numtra = txtNumtra.Text;
            objPedido.Fectra = DateTime.Parse(txtFectra.Text);
            objPedido.Estado = ddlEstado.Text;
            objPedido.Codalm = txtCodalm.Text;
            objPedido.Codven = ddlCodven.SelectedValue;
            objPedido.Totnet = String.IsNullOrEmpty(txtTotnet.Text) ? 0 : decimal.Parse(txtTotnet.Text);
            objPedido.Totbas = String.IsNullOrEmpty(txtTotbas.Text) ? 0 : decimal.Parse(txtTotbas.Text);
            objPedido.Totdes = String.IsNullOrEmpty(txtTotdes.Text) ? 0 : decimal.Parse(txtTotdes.Text);
            objPedido.Totiva = String.IsNullOrEmpty(txtTotiva.Text) ? 0 : decimal.Parse(txtTotiva.Text);
            objPedido.Totfac = String.IsNullOrEmpty(txtTotfac.Text) ? 0 : decimal.Parse(txtTotfac.Text);

            //DATATABLE FACTURA
            DataTable dt = objPedido.f_dtPedidoTableType();
            DataRow dr = null;
            dr = dt.NewRow();
            dr["codemp"] = Session["gs_Codemp"].ToString();
            dr["tiptra"] = 3;
            dr["numtra"] = txtNumtra.Text;
            dr["estado"] = ddlEstado.Text;
            dr["codalm"] = txtCodalm.Text;
            dr["codsuc"] = Session["gs_Codsuc"].ToString();
            dr["sersec"] = Session["gs_Sersec"].ToString();
            dr["fectra"] = txtFectra.Text;
            dr["fecven"] = txtFectra.Text;
            dr["clicxc"] = txtClicxc.Value;
            dr["nomcxc"] = txtNomcxc.Text;
            dr["lispre"] = ddlLispre.Text;
            dr["codcen"] = "";
            dr["nomcen"] = "";
            dr["codven"] = ddlCodven.SelectedValue;
            dr["codeje"] = "";
            dr["nomeje"] = "";
            dr["fecent"] = DateTime.Now;
            dr["forpag"] = txtForpag.Text;
            dr["valido"] = "";
            dr["numpro"] = "";
            dr["observ"] = txtObserv.Text;
            dr["totnet"] = String.IsNullOrEmpty(txtTotnet.Text) ? 0 : decimal.Parse(txtTotnet.Text);
            dr["totbas"] = String.IsNullOrEmpty(txtTotbas.Text) ? 0 : decimal.Parse(txtTotbas.Text);
            dr["totiv0"] = 0;
            dr["poriva"] = 0;
            dr["totiva"] = String.IsNullOrEmpty(txtTotiva.Text) ? 0 : decimal.Parse(txtTotiva.Text);
            dr["pordes"] = 0;
            dr["totdes"] = objPedido.Totdes = String.IsNullOrEmpty(txtTotdes.Text) ? 0 : decimal.Parse(txtTotdes.Text);
            dr["totfac"] = objPedido.Totfac = String.IsNullOrEmpty(txtTotfac.Text) ? 0 : decimal.Parse(txtTotfac.Text);
            dr["tiporg"] = "";
            dr["numorg"] = "";
            dr["codmon"] = "01";
            dr["valcot"] = 1;
            dr["usuing"] = Session["gs_UsuIng"].ToString();
            dr["fecing"] = DateTime.Now;
            dr["codusu"] = Session["gs_CodUs1"].ToString();
            dr["fecult"] = DateTime.Now;
            dr["numpag"] = Session["gs_Numpag"];
            dr["plapag"] = Session["gs_Plapag"];
            dt.Rows.Add(dr);
            objPedido.dtPedido = dt;

            return objPedido;
        }
        public cls_vc_Pedido_Ren f_generarObjPedidoRen()
        {
            objPedidoRen.Codemp = Session["gs_Codemp"].ToString();
            objPedidoRen.Tiptra = "3";
            objPedidoRen.Numtra = txtNumtra.Text;
            objPedidoRen.Codalm = txtCodalm.Text;
            objPedidoRen.Codiva = "1";
            objPedidoRen.Ubifis = "?";

            return objPedidoRen;
        }
    }
}